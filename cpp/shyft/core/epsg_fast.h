// Boost.Geometry

// Copyright (c) 2017, Oracle and/or its affiliates.
// Contributed and/or modified by Adam Wulkiewicz, on behalf of Oracle

// Use, modification and distribution is subject to the Boost Software License,
// Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#pragma once

// NOTICE: This file is a light mod of boost geometry epsg, due to ref. https://github.com/boostorg/geometry/issues/1006

#include <vector>
#include <boost/geometry/algorithms/buffer.hpp> // minimal (alt. is  boost/geometry.hpp) must be included here to pull in some requirements for algo/variants
#include <boost/geometry/srs/projection.hpp>
#include <boost/geometry/srs/projections/dpar.hpp>
#include <boost/geometry/srs/spheroid.hpp>
#include <boost/geometry/srs/projections/code.hpp>

namespace boost::geometry{

    namespace srs {
        struct epsg_fast{
            explicit epsg_fast(int c): code(c){}
            int code;
        };
    }

    namespace projections {

        namespace detail {
            srs::dpar::parameters<> epsg_to_parameters_fast(int code);// make parameters from epsg_code, using table
            std::vector<int> all_epsg_codes(); // for test purposes
        }

        template <>
        struct dynamic_parameters<srs::epsg_fast> {
            static const bool is_specialized = true;
            static inline srs::dpar::parameters<> apply(srs::epsg_fast const& params){
                return projections::detail::epsg_to_parameters_fast(params.code);
            }
        };
    }
} // namespace boost::geometry

