#include <shyft/dtss/dtss.h>
#include <shyft/core/dlib_utils.h>
#include <shyft/dtss/ts_subscription.h>
#include <shyft/dtss/master_slave_sync.h>
#include <shyft/version.h>

namespace shyft::dtss {
    
    using shyft::time_series::dd::ts_as;
    using shyft::time_series::dd::ats_vector;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_nvp;
    using shyft::core::core_arch_flags;
    using shyft::core::from_seconds;
    using shyft::core::utcperiod_hasher;

    void server::on_connect(
        std::istream & in,
        std::ostream & out,
        const std::string & foreign_ip,
        const std::string & local_ip,
        unsigned short foreign_port,
        unsigned short local_port,
        dlib::uint64 /*connection_id*/
    ) {

        using shyft::core::core_iarchive;
        using shyft::core::core_oarchive;
        using shyft::time_series::dd::expression_decompressor;
        using shyft::time_series::dd::compressed_ts_expression;
        using shyft::core::scoped_count;
        using shyft::core::subscription::manager;
        using shyft::dtss::subscription::ts_observer;

        scoped_count _{alive_connections};// count the alive connections
        ts_observer subs(sm, foreign_ip + std::to_string(foreign_port));
        // local fx to read the changed subs
        const auto read_changed_subs=[this,&subs]()->vector<apoint_ts> {
            auto rr = subs.find_changed_ts();
            vector<apoint_ts> r;r.reserve(rr.size());
            for(auto const& i: rr) {
                auto pr = do_slave_read(i.second, i.first, true);
                for(size_t j=0; j<pr.size(); ++j) {
                    r.emplace_back(apoint_ts(i.second[j],pr[j]));
                }
            }
            return r;
        };
        
        try {
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try { // scoping the binary-archive could be ok, since it forces destruction time (considerable) to taken immediately, reduce memory foot-print early
                    //  at the cost of early& fast response. I leave the commented scopes in there for now, and aim for fastest response-time
                    switch (msg_type) { // currently switch, later maybe table[msg_type]=msg_handler
                    case message_type::EVALUATE_TS_VECTOR:
                    case message_type::EVALUATE_TS_VECTOR_CLIP:
                    case message_type::EVALUATE_EXPRESSION:
                    case message_type::EVALUATE_EXPRESSION_CLIP:{
                        utcperiod bind_period;bool use_ts_cached_read,update_ts_cache;utcperiod clip_period;
                        ts_vector_t rtsv;
                        core_iarchive ia(in,core_arch_flags);
                        ia>>bind_period;
                        if(msg_type==message_type::EVALUATE_EXPRESSION || msg_type==message_type::EVALUATE_EXPRESSION_CLIP) {
                            compressed_ts_expression c_expr;
                            ia>>c_expr;
                            rtsv=expression_decompressor::decompress(c_expr);
                        } else {
                            ia>>rtsv;
                        }
                        ia>>use_ts_cached_read>>update_ts_cache;
                        if(msg_type==message_type::EVALUATE_TS_VECTOR_CLIP || msg_type==message_type::EVALUATE_EXPRESSION_CLIP) {
                            ia>>clip_period;
                        }
                        auto result=do_evaluate_ts_vector(bind_period, rtsv,use_ts_cached_read,update_ts_cache,clip_period);//first get result
                        msg::write_type(message_type::EVALUATE_TS_VECTOR,out);// then send
                        core_oarchive oa(out,core_arch_flags);
                        oa<<result;

                    } break;
                    case message_type::EVALUATE_EXPRESSION_PERCENTILES:
                    case message_type::EVALUATE_TS_VECTOR_PERCENTILES: {
                        utcperiod bind_period;bool use_ts_cached_read,update_ts_cache;
                        ts_vector_t rtsv;
                        std::vector<int64_t> percentile_spec;
                        gta_t ta;
                        core_iarchive ia(in,core_arch_flags);
                        ia >> bind_period;
                        if(msg_type==message_type::EVALUATE_EXPRESSION_PERCENTILES) {
                            compressed_ts_expression c_expr;
                            ia>>c_expr;
                            rtsv=expression_decompressor::decompress(c_expr);
                        } else {
                            ia>>rtsv;
                        }

                        ia>>ta>>percentile_spec>>use_ts_cached_read>>update_ts_cache;

                        auto result = do_evaluate_percentiles(bind_period, rtsv,ta,percentile_spec,use_ts_cached_read,update_ts_cache);//{
                        msg::write_type(message_type::EVALUATE_TS_VECTOR_PERCENTILES, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << result;
                    } break;
                    case message_type::FIND_TS: {
                        std::string search_expression; //{
                        search_expression = msg::read_string(in);// >> search_expression;
                        auto find_result = do_find_ts(search_expression);
                        msg::write_type(message_type::FIND_TS, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << find_result;
                    } break;
                    case message_type::GET_TS_INFO: {
                        std::string ts_url;
                        ts_url = msg::read_string(in);
                        auto result = do_get_ts_info(ts_url);
                        msg::write_type(message_type::GET_TS_INFO, out);
                        core_oarchive oa(out, core_arch_flags);
                        oa << result;
                    } break;
                    case message_type::STORE_TS: {
                        ts_vector_t rtsv;
                        bool overwrite_on_write{ true };
                        bool cache_on_write{ false };
                        core_iarchive ia(in,core_arch_flags);
                        ia >> rtsv >> overwrite_on_write >> cache_on_write;
                        do_store_ts(rtsv, overwrite_on_write, cache_on_write);
                        msg::write_type(message_type::STORE_TS, out);
                    } break;
                    case message_type::MERGE_STORE_TS: {
                        ts_vector_t rtsv;
                        bool cache_on_write{ false };
                        core_iarchive ia(in,core_arch_flags);
                        ia >> rtsv >> cache_on_write;
                        do_merge_store_ts(rtsv, cache_on_write);
                        msg::write_type(message_type::MERGE_STORE_TS, out);
                    } break;
                    case message_type::REMOVE_TS: {
                        std::string ts_url = msg::read_string(in);
                        do_remove_ts(ts_url);
                        msg::write_type(message_type::REMOVE_TS, out);
                    } break;
                    case message_type::CACHE_FLUSH: {
                        flush_cache();
                        clear_cache_stats();
                        msg::write_type(message_type::CACHE_FLUSH,out);
                    } break;
                    case message_type::CACHE_STATS: {
                        auto cs = get_cache_stats();
                        msg::write_type(message_type::CACHE_STATS,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<cs;
                    } break;
                    case message_type::GET_GEO_INFO: {
                        auto r = do_get_geo_info();
                        msg::write_type(message_type::GET_GEO_INFO,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;
                    case message_type::EVALUATE_GEO: {
                        geo::eval_args geo_args;
                        bool use_cache;
                        bool update_cache;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>geo_args>>use_cache>>update_cache;
                        auto result=do_geo_evaluate(geo_args,use_cache,update_cache);
                        msg::write_type(message_type::EVALUATE_GEO, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << result;
                    }break;
                    
                    case message_type::STORE_GEO: {
                        geo::ts_matrix tsm;
                        bool cache,replace;
                        string geo_db_name;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>geo_db_name>>tsm>>replace>>cache;
                        do_geo_store(geo_db_name,tsm,replace,cache);
                        msg::write_type(message_type::STORE_GEO, out);
                    } break;
                    
                    case message_type::ADD_GEO_DB: {
                        geo::ts_db_config_ gdb;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>gdb;
                        add_geo_ts_db(gdb);
                        msg::write_type(message_type::ADD_GEO_DB, out);
                    } break;

                    case message_type::REMOVE_GEO_DB: {
                        string geo_db_name;
                        core_iarchive ia(in,core_arch_flags);
                        ia >>geo_db_name;
                        remove_geo_ts_db(geo_db_name);
                        msg::write_type(message_type::REMOVE_GEO_DB, out);
                    } break;

                    case message_type::GET_CONTAINERS: {
                        auto container_names = do_get_container_names();
                        msg::write_type(message_type::GET_CONTAINERS, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << container_names;
                    } break;

                    case message_type::SLAVE_READ: {
                        id_vector_t ids;
                        utcperiod p;
                        bool use_ts_cached_read;
                        bool subscribed;

                        core_iarchive ia(in,core_arch_flags);
                        ia >> ids >> p >> use_ts_cached_read >> subscribed;

                        if(subscribed) {
                            subs.subscribe_to(ids, p);
                        }
                        auto result = do_slave_read(ids, p, use_ts_cached_read);
                        auto updates= read_changed_subs();// also read changed subs to piggyback to the response

                        msg::write_type(message_type::SLAVE_READ, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << result<<updates; // consider piggyback read_changed_subs here.
                    } break;

                    case message_type::SLAVE_READ_SUBSCRIPTION: {
                        auto r=read_changed_subs();
                        msg::write_type(message_type::SLAVE_READ_SUBSCRIPTION, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << r;
                    } break;

                    case message_type::GET_VERSION: {
                        msg::write_type(message_type::GET_VERSION, out);
                        core_oarchive oa(out,core_arch_flags);
                        oa << shyft::_version_string();
                    } break;
                    
                    case message_type::SLAVE_UNSUBSCRIBE: {
                        id_vector_t ids;

                        core_iarchive ia(in,core_arch_flags);
                        ia >> ids;
                        subs.unsubscribe(ids);

                        msg::write_type(message_type::SLAVE_UNSUBSCRIBE, out);
                    } break;

                    //-- queue related
                    case message_type::Q_LIST:{
                        auto r=queue_manager.queue_names();
                        msg::write_type(message_type::Q_LIST,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;

                    case message_type::Q_INFO:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name,msg_id;
                        ia>>q_name>>msg_id;
                        auto r=queue_manager(q_name)->get_msg_info(msg_id);
                        msg::write_type(message_type::Q_INFO,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;

                    case message_type::Q_INFOS:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name;
                        ia>>q_name;
                        auto r=queue_manager(q_name)->get_msg_infos();
                        msg::write_type(message_type::Q_INFOS,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;

                    case message_type::Q_PUT:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name,msg_id, descript;utctime ttl;
                        ts_vector_t tsv;
                        ia>>q_name>>msg_id>>descript>>ttl>>tsv;
                        queue_manager(q_name)->put(msg_id, descript,ttl, tsv);
                        msg::write_type(message_type::Q_PUT,out);
                    } break;

                    case message_type::Q_GET:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name;utctime max_wait;
                        ia>>q_name>>max_wait;
                        auto q=queue_manager(q_name);
                        auto r = q->try_get();
                        if(r==nullptr && max_wait> utctime(0)) {
                            auto dt=std::chrono::milliseconds(5);
                            auto t_exit=core::utctime_now()+max_wait;
                            do {
                                std::this_thread::sleep_for(dt);
                                r=q->try_get();
                            } while (r==nullptr && core::utctime_now()<t_exit);
                        }
                        msg::write_type(message_type::Q_GET,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;

                    case message_type::Q_ACK:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name,msg_id, diag;
                        ia>>q_name>>msg_id>>diag;
                        queue_manager(q_name)->done(msg_id, diag);
                        msg::write_type(message_type::Q_ACK,out);
                    } break;

                    case message_type::Q_SIZE:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name;
                        ia>>q_name;
                        auto r=queue_manager(q_name)->size();
                        msg::write_type(message_type::Q_SIZE,out);
                        core_oarchive oa(out,core_arch_flags);
                        oa<<r;
                    } break;

                    case message_type::Q_ADD:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name;
                        ia>>q_name;
                        queue_manager.add(q_name);
                        msg::write_type(message_type::Q_ADD,out);
                    } break;

                   case message_type::Q_REMOVE:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name;
                        ia>>q_name;
                        queue_manager.remove(q_name);
                        msg::write_type(message_type::Q_REMOVE,out);
                    } break;

                   case message_type::Q_MAINTAIN:{
                        core_iarchive ia(in,core_arch_flags);
                        string q_name;bool keep_ttl_items,flush_all;
                        ia>>q_name>>keep_ttl_items>>flush_all;
                        if(flush_all)
                            queue_manager(q_name)->reset();
                        else
                            queue_manager(q_name)->flush_done_items(keep_ttl_items);
                        msg::write_type(message_type::Q_MAINTAIN,out);
                    } break;

                    default:
                        throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string(static_cast<int>(msg_type)));
                    }
                } catch(dlib::socket_error const &) {
                    // silent ignore, later log. It means we got trouble read/write socket, we do not crash on that, just 
                    // continue, as its most likely the connection to remote that went away
                    // either because connection was broken, or client was killed
                }  catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        } catch(...) { // when we reach here.. we are going to close down the socket
            // so we just log to std err, then return.
            // the dlib thread, will forcibly close the socket
            // but survive the server it self.
            std::cerr<< "dtss: failed and cleanup connection from '"<<foreign_ip<<"'@"<<foreign_port<<", served at local '"<< local_ip<<"'@"<<local_port<<"\n";
        }
    }
}
