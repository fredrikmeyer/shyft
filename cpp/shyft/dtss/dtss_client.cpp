/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <algorithm>
#include <memory>
#include <utility>
#include <functional>
#include <cstring>
#include <regex>
#include <future>
#include <utility>
#include <chrono>

#include  <shyft/dtss/dtss_client.h>
#include  <shyft/dtss/dtss_url.h>
#include  <shyft/dtss/dtss_msg.h>

#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>
#include <shyft/time_series/expression_serialization.h>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace shyft::dtss {

    using std::runtime_error;
    using std::vector;
    using std::string;
    using std::int64_t;
    using std::exception;
    using std::future;
    using std::min;
    using std::chrono::milliseconds;
    using std::this_thread::sleep_for;

    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_arch_flags;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::gta_t;
    using shyft::time_series::dd::expression_compressor;
    using shyft::time_series::statistics_property;
    using dlib::socket_error;
    namespace {
        /** @brief Helper-class to enable 'autoconnect', lazy connect.
        * @details This operates on the client connection(s), that might
        * be one or more (recall that dtss client can utilize several servers)
        */
        struct client_scoped_connect {
            client& c;
            client_scoped_connect (client& c):c(c){
                bool rethrow=false;
                runtime_error rt_re("");
                for(auto&sc:c.srv_con) {
                    if (!sc.is_open ) { // auto-connect, and put some effort into succeeding
                        bool attempt_more=false;
                        int retry_count=5;
                        do {
                            try {
                                sc.open(); // either we succeed and finishes, or we get an exception
                                attempt_more=false;
                            } catch(const socket_error&se) {// capture socket error that might go away if we stay alive a little bit more
                                if(--retry_count>0 && strstr(se.what(),"unable to connect") ) {
                                    attempt_more=true;
                                    sleep_for(milliseconds(100));
                                } else {
                                    rt_re=runtime_error(se.what());
                                    rethrow=true;
                                    attempt_more=false;
                                }
                            } catch(const exception&re) { // just give up this type of error,
                                    rt_re=runtime_error(re.what());
                                    rethrow=true;
                                    attempt_more=false;
                            }
                        } while(attempt_more);
                    }
                }
                if(rethrow)
                    throw rt_re;
            }
            ~client_scoped_connect() noexcept(false) {
                // we don't disconnect, rather we try to fix broken connects.
            }
            client_scoped_connect (const client_scoped_connect&) = delete;
            client_scoped_connect ( client_scoped_connect&&) = delete;
            client_scoped_connect& operator=(const client_scoped_connect&) = delete;
            client_scoped_connect()=delete;
            client_scoped_connect& operator=(client_scoped_connect&&)=delete;
        };

        // automate message exchange to one-liners.

        struct r_void{};// just to fix void fx's

        template<message_type mtype,class R, class ...A>
        R exchange_msg(client&c, A&& ...args) {
            client_scoped_connect scs(c);
            auto &sc = c.srv_con[0];

            R r{};
            for (int retry = 0; retry < 3; ++retry) {
                try {
                    auto& io = *( sc.io);
                    msg::write_type(mtype,io);
                    if constexpr (sizeof ...(args) >0) { // if there are any args to forward to server...
                        core_oarchive oa(io, core_arch_flags);// only then create the io stream
                        ( (oa<< std::forward<decltype(args)>(args) ),...); // unpack the args, use std:: forward to ensure we do not tamper with type of args
                    }
                    auto response_type = msg::read_type(io);
                    if (response_type == message_type::SERVER_EXCEPTION) {
                        auto re = msg::read_exception(io);
                        throw re;
                    } else if (response_type == mtype) {
                        if constexpr (!std::is_same<R,r_void>::value){
                            core_iarchive ia(io, core_arch_flags);
                            ia >> r;
                        }
                        return r;

                    } else {
                        sc.close();//close the stream to avoid reading more clutter, it will reopen
                        throw runtime_error(string("Got unexpected response:") + std::to_string((int)response_type));
                    }

                } catch (const dlib::socket_error&) {
                    sc.reopen();
                }
            }
            throw runtime_error("Failed to establish connection with " + sc.host_port);
        }
    }


    client::client ( const string& host_port, bool auto_connect, int timeout_ms )
        :auto_connect(auto_connect)
    {
        srv_con.emplace_back(host_port,timeout_ms);
    }
    size_t client::reconnect_count() const {
        size_t s{0u};
        for(auto const&c:srv_con)
            s+=c.reconnect_count;
        return s;
    }

    client::client(const vector<string>& host_ports,bool auto_connect,int timeout_ms):auto_connect(auto_connect) {
        if(host_ports.size()==0)
            throw runtime_error("host_ports must contain at least one element");
        for(const auto &hp:host_ports) {
            srv_con.emplace_back(hp,timeout_ms);
        }
    }

    void client::reopen(int timeout_ms) {
        for(auto&sc:srv_con)
            sc.reopen(timeout_ms);
    }

    void client::close(int timeout_ms) {
        bool rethrow = false;
        runtime_error rt_re("");
        for(auto&sc:srv_con) {
            try {
                sc.close(timeout_ms);
            } catch (const exception &re) { // ensure we try to close all
                rt_re=runtime_error(re.what());
                rethrow=true;
            }
        }
        if(rethrow)
            throw rt_re;
    }

    msync_read_result
    client::read(id_vector_t const& tsv, utcperiod p,bool use_ts_cached_read, bool subscribe) {
        if (tsv.size() == 0)
            throw std::runtime_error("(slave)read requires a source ts-vector with more than 0 time-series");
        if (!p.valid())
            throw std::runtime_error("evaluate require a valid period-specification");
        client_scoped_connect ac(*this);
        msync_read_result r;
        do_io_with_repair_and_retry(srv_con[0],
                [&r,&tsv,p,use_ts_cached_read,subscribe](srv_connection &sc) {
                    auto& io = *sc.io;
                    msg::write_type(message_type::SLAVE_READ, io);
                    core_oarchive oa(io, core_arch_flags);
                    oa << tsv << p << use_ts_cached_read << subscribe;

                    auto response_type = msg::read_type(io);
                    if (response_type == message_type::SERVER_EXCEPTION) {
                        auto re = msg::read_exception(io);
                        throw re;
                    } else if (response_type == message_type::SLAVE_READ) {
                        core_iarchive ia(io, core_arch_flags);
                        ia >> r.result>>r.updates;
                    } else {
                        throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                    }
                }
        );
        return r;
    }

    void
    client::unsubscribe(id_vector_t const& tsv) {
        if (tsv.size() == 0)
            return;// noop
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0],
            [&tsv](srv_connection &sc) {
                auto& io = *sc.io;
                msg::write_type(message_type::SLAVE_UNSUBSCRIBE, io);
                core_oarchive oa(io, core_arch_flags);
                oa << tsv;

                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type != message_type::SLAVE_UNSUBSCRIBE) {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));

                }
            }
        );
    }

    std::vector<apoint_ts>
    client::read_subscription() {
        client_scoped_connect ac(*this);
        std::vector<apoint_ts> r;
        do_io_with_repair_and_retry(srv_con[0],
            [&r](srv_connection &sc) {
                auto& io = *sc.io;
                msg::write_type(message_type::SLAVE_READ_SUBSCRIPTION, io);

                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::SLAVE_READ_SUBSCRIPTION) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> r;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") +
                                                std::to_string(
                                                        static_cast<int>(response_type)));
                }
            }
        );
        return r;
    }

    vector<apoint_ts>
    client::percentiles(ts_vector_t const& tsv, utcperiod p, gta_t const&ta, const std::vector<int64_t>& percentile_spec,bool use_ts_cached_read,bool update_ts_cache) {
        if (tsv.size() == 0)
            throw runtime_error("percentiles requires a source ts-vector with more than 0 time-series");
        if (percentile_spec.size() == 0)
            throw std::runtime_error("percentile function require more than 0 percentiles specified");
        if (!p.valid())
            throw std::runtime_error("percentiles require a valid period-specification");
        if (ta.size() == 0)
            throw std::runtime_error("percentile function require a time-axis with more than 0 steps");


        if(srv_con.size()==1 || tsv.size()< srv_con.size()) {
            client_scoped_connect ac(*this);
            ts_vector_t r;
            do_io_with_repair_and_retry(srv_con[0],
                [this,&r,&ta,&tsv,&percentile_spec,&use_ts_cached_read,&update_ts_cache,&p](srv_connection&sc) {
                    auto& io = *sc.io;
                    msg::write_type(compress_expressions ? message_type::EVALUATE_EXPRESSION_PERCENTILES : message_type::EVALUATE_TS_VECTOR_PERCENTILES, io);
                    core_oarchive oa(io, core_arch_flags);
                    oa << p;
                    if (compress_expressions) {
                        oa << expression_compressor::compress(tsv);
                    } else {
                        oa << tsv;
                    }
                    oa << ta << percentile_spec << use_ts_cached_read << update_ts_cache;
                    auto response_type = msg::read_type(io);
                    if (response_type == message_type::SERVER_EXCEPTION) {
                        auto re = msg::read_exception(io);
                        throw re;
                    } else if (response_type == message_type::EVALUATE_TS_VECTOR_PERCENTILES) {
                        core_iarchive ia(io, core_arch_flags);
                        ia >> r;
                    } else {
                        throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                    }
                }
            );
            return r;
        } else {
            vector<int64_t> p_spec;
            bool can_do_server_side_average=true; // in case we are searching for min-max extreme, we can not do server-side average
            for(size_t i=0;i<percentile_spec.size();++i) {
                p_spec.push_back(int(percentile_spec[i]));
                if(percentile_spec[i] == statistics_property::MIN_EXTREME || percentile_spec[i]==statistics_property::MAX_EXTREME)
                    can_do_server_side_average=false;
            }
            auto atsv = evaluate(can_do_server_side_average?tsv.average(ta):tsv,p,use_ts_cached_read,update_ts_cache); // get the result we can do percentiles on
            return shyft::time_series::dd::percentiles(atsv, ta, p_spec);
        }
    }

    std::vector<apoint_ts>
    client::evaluate(ts_vector_t const& tsv, utcperiod p,bool use_ts_cached_read,bool update_ts_cache,utcperiod result_clip) {
        if (tsv.size() == 0)
            throw std::runtime_error("evaluate requires a source ts-vector with more than 0 time-series");
        if (!p.valid())
            throw std::runtime_error("evaluate require a valid period-specification");
        client_scoped_connect ac(*this);
        // local lambda to ensure one definition of communication with the server
        auto eval_io = [this] (srv_connection &sc,const ts_vector_t& tsv,const utcperiod& p,bool use_ts_cached_read,bool update_ts_cache,const utcperiod result_clip) ->ts_vector_t {
            ts_vector_t r;
            do_io_with_repair_and_retry(sc,
                [this,&r,&tsv,&p,&use_ts_cached_read,&update_ts_cache,&result_clip](srv_connection &sc) {
                    auto& io = *sc.io;
                    bool rc=result_clip.valid();
                    msg::write_type(compress_expressions ? (rc?message_type::EVALUATE_EXPRESSION_CLIP:message_type::EVALUATE_EXPRESSION) :
                                                            (rc?message_type::EVALUATE_TS_VECTOR_CLIP:message_type::EVALUATE_TS_VECTOR), io); {
                        core_oarchive oa(io, core_arch_flags);
                        oa << p;
                        if (compress_expressions) { // notice that we stream out all in once here
                            // .. just in case the destruction of the compressed expr take time (it could..)
                            oa << expression_compressor::compress(tsv) << use_ts_cached_read << update_ts_cache;
                        } else {
                            oa << tsv << use_ts_cached_read << update_ts_cache;
                        }
                        if(rc) {
                            oa<<result_clip;
                        }
                    }
                    auto response_type = msg::read_type(io);
                    if (response_type == message_type::SERVER_EXCEPTION) {
                        auto re = msg::read_exception(io);
                        throw re;
                    } else if (response_type == message_type::EVALUATE_TS_VECTOR) {
                        core_iarchive ia(io, core_arch_flags);
                        ia >> r;
                    } else {
                        throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                    }
                }
            );
            return r;
        };

        if(srv_con.size()==1 || tsv.size() == 1) { // one server, or just one ts, do it easy
            return eval_io(srv_con[0],tsv,p,use_ts_cached_read,update_ts_cache,result_clip);
        } else {
            ts_vector_t rt(tsv.size()); // make place for the result contributions from threads
            // lamda to eval partition on server
            auto eval_partition= [&rt,&tsv,&eval_io,p,use_ts_cached_read,update_ts_cache,result_clip]
                (srv_connection& sc,size_t i0, size_t n) {
                    ts_vector_t ptsv;ptsv.reserve(tsv.size());
                    for(size_t i=i0;i<i0+n;++i) ptsv.push_back(tsv[i]);
                    auto pt = eval_io(sc,ptsv,p,use_ts_cached_read,update_ts_cache,result_clip);
                    for(size_t i=0;i<pt.size();++i)
                        rt[i0+i]=pt[i];// notice how se stash data into common result variable, safe because each thread using it's own preallocated space
            };
            size_t n_ts_pr_server = tsv.size()/srv_con.size();
            size_t remainder = tsv.size() - n_ts_pr_server*srv_con.size();
            vector<future<void>> calcs;
            size_t b=0;
            for(size_t i=0;i<srv_con.size() && b<tsv.size();++i) {
                size_t e = b+n_ts_pr_server;
                if(remainder>0) {
                    e+=1;// spread remainder equally between servers
                    --remainder;
                } else if (e>tsv.size()) {
                    e=tsv.size();
                }

                size_t n = e-b;
                calcs.push_back(std::async(
                                std::launch::async,
                                [this,i,b,n,&eval_partition] () {
                                    eval_partition(srv_con[i],b,n);
                                }
                               )
                      );
                b=e;
            }
            for (auto &f : calcs)
                f.get();

            return rt;
        }
    }

    void
    client::store_ts(const ts_vector_t &tsv, bool overwrite_on_write, bool cache_on_write) {
        if (tsv.size() == 0)
            return; //trivial and considered valid case
                    // verify that each member of tsv is a gpoint_ts
        for (auto const &ats : tsv) {
            auto rts = dynamic_cast<aref_ts const *>(ats.ts.get());
            if (!rts) throw std::runtime_error(std::string("attempt to store a null ts"));
            if (rts->needs_bind()) throw std::runtime_error(std::string("attempt to store unbound ts:") + rts->id);
        }
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0],
            [&tsv,overwrite_on_write,cache_on_write](srv_connection&sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::STORE_TS, io);
                {
                    core_oarchive oa(io, core_arch_flags);
                    oa << tsv << overwrite_on_write << cache_on_write;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::STORE_TS) {
                    return;
                }
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        );
    }

    void
    client::merge_store_ts(const ts_vector_t &tsv,bool cache_on_write) {
        if (tsv.size() == 0)
            return; //trivial and considered valid case
                    // verify that each member of tsv is a gpoint_ts
        for (auto const &ats : tsv) {
            auto rts = dynamic_cast<aref_ts const *>(ats.ts.get());
            if (!rts) throw std::runtime_error(std::string("attempt to store a null ts"));
            if (rts->needs_bind()) throw std::runtime_error(std::string("attempt to store unbound ts:") + rts->id);
        }
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0],
            [&tsv, cache_on_write](srv_connection&sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::MERGE_STORE_TS, io);
                {
                    core_oarchive oa(io, core_arch_flags);
                    oa << tsv << cache_on_write;
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::MERGE_STORE_TS) {
                    return;
                }
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        );
    }

    ts_info_vector_t
    client::find(const std::string& search_expression) {
        client_scoped_connect ac(*this);
        ts_info_vector_t r;
        do_io_with_repair_and_retry(srv_con[0],
            [&r, &search_expression](srv_connection&sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::FIND_TS, io); {
                    msg::write_string(search_expression, io);
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::FIND_TS) {
                        core_iarchive ia(io, core_arch_flags);
                        ia >> r;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                }
            }
        );
        return r;
    }

    ts_info
    client::get_ts_info(const std::string & ts_url) {
        client_scoped_connect ac(*this);
        ts_info tsi;
        do_io_with_repair_and_retry(srv_con[0],
            [&tsi, &ts_url](srv_connection & sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::GET_TS_INFO, io); {
                    msg::write_string(ts_url, io);
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::GET_TS_INFO) {
                    core_iarchive ia(io, core_arch_flags);
                    ia >> tsi;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                }
            }
        );
        return tsi;
    }

    void
    client::remove(const string & ts_url) {
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0],
            [&ts_url](srv_connection&sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::REMOVE_TS, io);{
                    msg::write_string(ts_url, io);
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == message_type::REMOVE_TS) {
                    return;
                }
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        );
    }

    void
    client::cache_flush() {
        client_scoped_connect ac(*this);
        for(auto& sc:srv_con) {
            do_io_with_repair_and_retry(sc,
                [](srv_connection&sc) {
                    auto& io = *(sc.io);
                    msg::write_type(message_type::CACHE_FLUSH, io);
                    auto response_type = msg::read_type(io);
                    if (response_type == message_type::SERVER_EXCEPTION) {
                        auto re = msg::read_exception(io);
                        throw re;
                    } else if (response_type != message_type::CACHE_FLUSH) {
                        throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                    }
                }
            );
        }
    }



    cache_stats
    client::get_cache_stats() {
        client_scoped_connect ac(*this);
        cache_stats s;
        for(auto& sc:srv_con) {
            do_io_with_repair_and_retry(sc, [&s](srv_connection&sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::CACHE_STATS, io);
                auto response_type = msg::read_type(io);
                if (response_type == message_type::CACHE_STATS) {
                    cache_stats r;
                    core_iarchive oa(io, core_arch_flags);
                    oa >> r;
                    s = s + r;
                } else if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                }
            }
            );
        }
        return s;
    }
    string
    client::get_server_version() {
        client_scoped_connect ac(*this);
        auto &sc = srv_con[0];
        string r;
        do_io_with_repair_and_retry(sc,
            [&r](srv_connection&sc) {

                auto &io = *(sc.io);
                msg::write_type(message_type::GET_VERSION, io);
                auto response_type = msg::read_type(io);

                if (response_type == message_type::GET_VERSION) {
                    core_iarchive oa(io, core_arch_flags);
                    oa >> r;
                } else if(response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                }
            }
        );
        return r;
    }
    
    id_vector_t client::get_container_names() {
        client_scoped_connect ac(*this);
        auto &sc = srv_con[0];
        id_vector_t r;
        do_io_with_repair_and_retry(sc,
            [&r](srv_connection&sc) {

                auto &io = *(sc.io);
                msg::write_type(message_type::GET_CONTAINERS, io);
                auto response_type = msg::read_type(io);

                if (response_type == message_type::GET_CONTAINERS) {
                    core_iarchive oa(io, core_arch_flags);
                    oa >> r;
                } else if(response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                }
            }
        );
        return r;
    }

    geo::geo_ts_matrix client::geo_evaluate(
            geo::eval_args const&ea,
            bool use_cache,
            bool update_cache
        ) {
        client_scoped_connect ac(*this);
        geo::geo_ts_matrix r;
        do_io_with_repair_and_retry(srv_con[0], [&r,&ea,use_cache,update_cache](srv_connection&sc) {
            auto& io = *(sc.io);
            msg::write_type(message_type::EVALUATE_GEO, io);
            {
                core_oarchive oa(io, core_arch_flags);
                oa << ea<<use_cache<<update_cache;
            }
            auto response_type = msg::read_type(io);
            if (response_type == message_type::EVALUATE_GEO) {
                // all ok, no exception, core_iarchive oa(io, core_arch_flags);
                core_iarchive oa(io, core_arch_flags);
                oa >> r;
            } else if (response_type == message_type::SERVER_EXCEPTION) {
                auto re = msg::read_exception(io);
                throw re;
            } else {
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        }
        );
        return r;
            
    }
    void client::geo_store(
            const string& geo_db_name,
            geo::ts_matrix const&tsm,
            bool replace,
            bool update_cache
        ) {
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0], [&geo_db_name,&tsm,replace,update_cache](srv_connection&sc) {
            auto& io = *(sc.io);
            msg::write_type(message_type::STORE_GEO, io);
            {
                core_oarchive oa(io, core_arch_flags);
                oa << geo_db_name<<tsm<<replace<<update_cache;
            }
            auto response_type = msg::read_type(io);
            if (response_type == message_type::STORE_GEO) {
                // all ok, no exception, core_iarchive oa(io, core_arch_flags);
                
            } else if (response_type == message_type::SERVER_EXCEPTION) {
                auto re = msg::read_exception(io);
                throw re;
            } else {
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        }
        );
    }

    

    vector<geo::ts_db_config_> client::get_geo_ts_db_info() {
        client_scoped_connect ac(*this);
        vector<geo::ts_db_config_> s;
        for(auto& sc:srv_con) {
            do_io_with_repair_and_retry(sc, [&s](srv_connection&sc) {
                auto& io = *(sc.io);
                msg::write_type(message_type::GET_GEO_INFO, io);
                auto response_type = msg::read_type(io);
                if (response_type == message_type::GET_GEO_INFO) {
                    core_iarchive oa(io, core_arch_flags);
                    oa >> s;
                } else if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else {
                    throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
                }
            }
            );
        }
        return s;
        
    }
    
    void client::add_geo_ts_db(geo::ts_db_config_ const &gdb) {
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0], [&gdb](srv_connection&sc) {
            auto& io = *(sc.io);
            msg::write_type(message_type::ADD_GEO_DB, io);
            {
                core_oarchive oa(io, core_arch_flags);
                oa << gdb;
            }
            auto response_type = msg::read_type(io);
            if (response_type == message_type::ADD_GEO_DB) {
               
            } else if (response_type == message_type::SERVER_EXCEPTION) {
                auto re = msg::read_exception(io);
                throw re;
            } else {
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        }
        );
    }
        
    void client::remove_geo_ts_db(const string & geo_db_name) {
        client_scoped_connect ac(*this);
        do_io_with_repair_and_retry(srv_con[0], [&geo_db_name](srv_connection&sc) {
            auto& io = *(sc.io);
            msg::write_type(message_type::REMOVE_GEO_DB, io);
            {
                core_oarchive oa(io, core_arch_flags);
                oa << geo_db_name;
            }
            auto response_type = msg::read_type(io);
            if (response_type == message_type::REMOVE_GEO_DB) {
               
            } else if (response_type == message_type::SERVER_EXCEPTION) {
                auto re = msg::read_exception(io);
                throw re;
            } else {
                throw std::runtime_error(std::string("Got unexpected response:") + std::to_string(static_cast<int>(response_type)));
            }
        }
        );
    }
    
    vector<string> 
    client::q_list() {
        return exchange_msg<message_type::Q_LIST,vector<string> >(*this);
    }

    queue::msg_info 
    client::q_msg_info(string const&q_name,string const&msg_id) {
        return exchange_msg<message_type::Q_INFO,queue::msg_info>(*this,q_name,msg_id);
    }
    
    vector<queue::msg_info> 
    client::q_msg_infos(string const&q_name) {
        return exchange_msg<message_type::Q_INFOS,vector<queue::msg_info> >(*this,q_name);
    }

    void 
    client::q_put(string const &q_name,string const&msg_id, string const&descript, utctime ttl,ts_vector_t const&tsv) {
        exchange_msg<message_type::Q_PUT,r_void>(*this,q_name,msg_id,descript,ttl,tsv);
    }
    
    queue::tsv_msg_ 
    client::q_get(string const &q_name, utctime max_wait) {
        return exchange_msg<message_type::Q_GET,queue::tsv_msg_ >(*this,q_name,max_wait);
    }

    void 
    client::q_ack(string const&q_name, string const&msg_id,string const&diag) {
        exchange_msg<message_type::Q_ACK,r_void>(*this,q_name,msg_id,diag);
    }

    size_t
    client::q_size(string const&q_name) {
        return exchange_msg<message_type::Q_SIZE,size_t>(*this,q_name);
    }

    void
    client::q_add(string const &q_name) {
        exchange_msg<message_type::Q_ADD,r_void>(*this,q_name);
    }

    void
    client::q_remove(string const &q_name) {
        exchange_msg<message_type::Q_REMOVE,r_void>(*this,q_name);
    }

    void
    client::q_maintain(string const&q_name,bool keep_ttl_items,bool flush_all) {
        exchange_msg<message_type::Q_MAINTAIN,r_void>(*this,q_name,keep_ttl_items,flush_all);
    }

    
}

