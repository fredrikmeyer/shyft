#include <algorithm>
#include <shyft/dtss/geo.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/multi_point.hpp>
#include <boost/geometry/algorithms/distance.hpp>
#include <boost/geometry/algorithms/convex_hull.hpp>


namespace shyft::dtss::geo {

namespace bg=boost::geometry;
using bg::degree;
using bg::cs::cartesian;
using bg::cs::geographic;
using bg::distance;
using bg::srs::projection;
using bg::srs::proj4;
using bg::srs::epsg_fast;
using point_xy=bg::model::d2::point_xy<double>;
using polygon=bg::model::polygon<point_xy>;
using shyft::core::to_seconds64;
using shyft::core::detail::epsg_map;
using shyft::time_series::dd::apoint_ts;

slice::slice(ix_vector const&v,ix_vector const& g,ix_vector const&e, vector<utctime> const&t,utctime ts_dt)
: v{v},g{g},e{e},t{t},ts_dt{ts_dt}{}

eval_args::eval_args(string const &geo_ts_db_id, vector<string> const & variables,vector<int64_t> const & ens,gta_t  const &ta,utctime ts_dt, query const & geo_range, bool concat,utctime cc_dt0)
:geo_ts_db_id{geo_ts_db_id}, variables{variables},ens{ens},ta{ta},ts_dt{ts_dt},geo_range{geo_range},cc_dt0{cc_dt0},concat{concat}
{}



ix_vector grid_spec::find_geo_match_ix(query const&geo)const {
    //  find those x,y in grid that is contained in geo_scope using boost::polygon
    ix_vector px;// captures the interesting points.
    if(geo.polygon.size()>2) {
        polygon geo_scope;// this is the geo scope that we are to find
        // transform/adapt geo args into boost::geometry
        for(auto const& gp:geo.polygon) {
            geo_scope.outer().emplace_back(epsg_map(point_xy{gp.x,gp.y},geo.epsg,epsg));
        }
        auto p0=geo.polygon.front();
        if(p0 != geo.polygon.back()) { // make a closed polygon
            geo_scope.outer().emplace_back(epsg_map(point_xy{p0.x,p0.y},geo.epsg,epsg));// close the polygon
        }
        for(size_t i=0;i<points.size();++i) {
            auto const &p=points[i];
            point_xy pxy{p.x,p.y};
            if(bg::covered_by(pxy,geo_scope))
                px.emplace_back(i);
        }
    } else {
        px.reserve(points.size());for(size_t i=0;i<points.size();++i) px.emplace_back(i);
    }
    return px;
}
gta_t ts_db_config::t0_time_axis() const {
    return t0_times.size()>0?
        gta_t{t0_times,t0_times.back()+dt}
        :
        gta_t{};
    ;

}

utctime ts_db_config::get_associated_t0(utctime t0,bool concat) const {
    if(!concat)
        return t0;
    if(t0_times.size()==0)
        return t0;
    if(t0<=t0_times.front())
        return t0;
    if(t0>=t0_times.back())
        return t0_times.back();
    // we are in-between
    auto r = lower_bound( t0_times.cbegin(), t0_times.cend(), t0,[]( utctime pt, utctime val ) { return pt <= val; } );
    return t0_times[static_cast<size_t>( r - t0_times.cbegin() ) - 1];// mayb just return *--r
}


slice ts_db_config::compute(eval_args const &ea) const {
    slice gx;//note that all gx.v, gx.e etc. are gdb.cfg referenced with respect to index values
    for(auto const&v:ea.variables) {
        size_t i=0;
        for(;i<variables.size();++i) {
            if(variables[i]==v)
                break;
        }
        if(i==variables.size())
            throw runtime_error("geo::ts_db:"+name+" do not have variable named "+ v);
        gx.v.push_back(i);
    }
    if(gx.v.size()==0) // empty means all
        for(size_t i=0;i<variables.size();++i) gx.v.push_back(i);
            
    // validate ensembles
    gx.e.reserve(n_ensembles);
    if(ea.ens.size()==0) {
        for(decltype(n_ensembles) i=0;i<n_ensembles;++i)
            gx.e.emplace_back(i);
    } else {
        for(auto const& e:ea.ens) {
            gx.e.emplace_back(e);
            if(e >= n_ensembles)
                throw runtime_error("geo::ts_db:"+name+" out of range ens member specified: "+ to_string(e) );
        }
    }
    if(t0_times.size()==0)
        return gx;//empty db, return empty result
            
    // validate ta, or t0_times,
    // empty ta should indicate all available
    // then we require exact match for all t0 in fc-times ?
    auto t0_ta=t0_time_axis();
    if(ea.ta.size()) { // if specified, then validate precicely
        for(size_t i=0;i<ea.ta.size();++i) {
            auto it=t0_ta.index_of(ea.ta.time(i));
            if(it==std::string::npos)
                throw runtime_error("geo::ts_db:"+name+" out of range time member specified at t0 position:"+ to_string(i) );
            if(t0_ta.time(it)!=ea.ta.time(i))
                throw runtime_error("geo_ts_db:"+name+" miss-match range time member specified at t0 position:"+ to_string(i) );
            gx.t.emplace_back(ea.ta.time(i));//hmm, maybe ix here as well
        }
    } else {
        gx.t=t0_times;// empty means all
    }
    // gx.g .. the index-set that we are looking for.
    gx.g=find_geo_match_ix(ea.geo_range);// to polygon match and fill gx.g with those that are within
    gx.ts_dt=ea.ts_dt>utctime(0)?ea.ts_dt:dt; // use supplied ts_dt, or if not specified, use the configured length
    return gx;
}

geo_ts_matrix ts_db_config::create_geo_ts_matrix(slice const& gx) const {
    geo_ts_matrix rv(gx.t.size(),gx.v.size(),gx.e.size(),gx.g.size());
    for(size_t t0_ix=0;t0_ix<gx.t.size();++t0_ix) {
        for(size_t v_ix=0;v_ix<gx.v.size();++v_ix) {
            for(size_t e_ix=0;e_ix<gx.e.size();++e_ix) {
                for(size_t g_ix=0;g_ix<gx.g.size();++g_ix) {
                    rv._set_geo_point(t0_ix,v_ix,e_ix,g_ix,grid.points[gx.g[g_ix]]);
                }
            }
        }
    }
    return rv;
}

ts_matrix ts_db_config::create_ts_matrix(slice const& gx) const {
    return ts_matrix (gx.t.size(),gx.v.size(),gx.e.size(),gx.g.size());
}

ts_matrix ts_db_config::create_ts_matrix_t0(size_t n_t0) const {
    return ts_matrix (n_t0,variables.size(),n_ensembles,grid.points.size());
}


vector<geo_point> ts_db_config::bounding_box(slice const& gx) const {
    using std::min;
    using std::max;
    geo_point p0,p1;
    if(gx.g.size()==0)
        return vector<geo_point>{p0,p1};
    
    p0=p1=grid.points[gx.g.front()];
    for(auto i:gx.g) {
        auto p=grid.points[i];
        p0.x = min(p0.x,p.x);p0.y = min(p0.y,p.y);p0.z=min(p0.z,p.z);
        p1.x = max(p1.x,p.x);p1.y = max(p1.y,p.y);p1.z=max(p0.z,p.z);
    }
    return vector<geo_point>{p0,p1};
}

vector<geo_point> ts_db_config::convex_hull(slice const& gx) const {
    if(gx.g.size()==0)
        return vector<geo_point>{};

    using multi_point_xy=bg::model::multi_point<point_xy>;
    multi_point_xy points; points.reserve(gx.g.size());
    for(size_t g_ix=0;g_ix<gx.g.size();++g_ix) {
        auto const &p=grid.points[gx.g[g_ix]];
        point_xy pxy{p.x,p.y};
        points.emplace_back(pxy);
    }

    // Compute the hull
    polygon hull_poly;
    bg::convex_hull(points, hull_poly);

    vector<geo_point> hull_points;
    vector<point_xy>::iterator it;
    for(it=hull_poly.outer().begin(); it!=hull_poly.outer().end(); ++it) {
        geo_point gp{bg::get<0>(*it), bg::get<1>(*it)};
        hull_points.emplace_back(gp);
    }
    return hull_points;
}

query::query(int64_t epsg,vector<geo_point> const&gpv)
    :epsg{epsg},polygon{gpv} {
    if(gpv.size()<3)
        throw runtime_error("geo::query: needs 3 or more points to form a polygon, supplied only "+to_string(gpv.size()));
    if(this->epsg < 0)
        throw runtime_error("geo::query: epsg must be 0-positive number "+to_string(this->epsg));
    // we could do more checks here, to valididate it's complete polygon
}

grid_spec::grid_spec(int64_t epsg,vector<geo_point> const&gpv)
    :epsg{epsg},points{gpv} {
    if(this->epsg < 0)
        throw runtime_error("geo::query: epsg must be 0-positive number "+to_string(this->epsg));
    // we could do more checks here, to valididate it's complete polygon
}

ts_db_config::ts_db_config(string const&prefix,string const&name,string const&descr, grid_spec const& grid,vector<utctime> const&t0_times,utctime dt,int64_t n_ensembles,vector<string> const&v) 
    :prefix{prefix},name{name},descr{descr},grid{grid},t0_times{t0_times},dt{dt},n_ensembles{n_ensembles},variables{v} {
    if(n_ensembles<=0)
        throw runtime_error("geo_ts_db_config: n_ensembles must be >0: "+to_string(n_ensembles));
    if(variables.size()==0)
        throw runtime_error("geo_ts_db_config: variables list can not be empty");
    if(dt <= utctime(0))
        throw runtime_error("geo_ts_db_config: dt specified must be >0");
    if(prefix.size() && !boost::algorithm::ends_with(prefix,"://"))
        throw runtime_error("geo_ts_db_config: prefix, if specified must end with ://, was:"+prefix);
}

template <class M, class GP>
M fx_concatenate(M const& m, GP && geo_point_copy,utctime cc_dt, utctime fc_interval) {
    if(cc_dt <utctime(0))
        throw runtime_error("geo_matrix.concatenate: require cc_dt >= 0s");
    if(fc_interval <= utctime(0))
        throw runtime_error("geo_matrix.concatenate: require fc_interval > 0s");
    if(m.shape.size()==0)
        throw runtime_error("geo_matrix.concatenate: require more than 0 forecasts to concatenate");
    
    M ccv(1,m.shape.n_v,m.shape.n_e,m.shape.n_g);// create result, that has only 1 in t0 dimension as result of concat operation
    
    for(size_t v=0; v<m.shape.n_v;++v) {
        for(size_t e=0;e<m.shape.n_e;++e) {
            ats_vector g_cc;g_cc.reserve(m.shape.n_g);// collect stuff into this one that we are going to multicore reduce
            for(size_t g=0;g<m.shape.n_g;++g) {
                ats_vector cc;cc.reserve(m.shape.n_t0);
                for(size_t t=0;t<m.shape.n_t0;++t) {
                    cc.push_back(m._ts(t,v,e,g));
                }
                g_cc.push_back(cc.forecast_merge(cc_dt,fc_interval));
            }
            auto r_g_cc=shyft::time_series::dd::deflate_ts_vector<apoint_ts>(g_cc);// multicore concat for all the points and t0
            for(size_t g=0;g<m.shape.n_g;++g){
                ccv._set_ts(0,v,e,g,std::move(r_g_cc[g]));// put the concatenated series in place
                geo_point_copy(ccv,m,0,v,e,g);// and the geo point along with it.
            }
        }
    }
    return ccv;
    
}

geo_ts_matrix geo_ts_matrix::concatenate(utctime cc_dt,utctime fc_interval) const {
    return fx_concatenate<geo_ts_matrix>(*this,
        [](geo_ts_matrix&ccv, geo_ts_matrix const &m,size_t t,size_t v,size_t e,size_t g)->void{
            ccv._set_geo_point(t,v,e,g,m._get_geo_point(t,v,e,g));
        },
        cc_dt,
        fc_interval);

}
ts_matrix ts_matrix::concatenate(utctime cc_dt,utctime fc_interval) const {
    return fx_concatenate<ts_matrix>(*this,
        [](ts_matrix& /*ccv*/, ts_matrix const &/*m*/,size_t /*t*/,size_t /*v*/,size_t /*e*/,size_t /*g*/)->void{
            ;
        },
        cc_dt,
        fc_interval);

}

}
