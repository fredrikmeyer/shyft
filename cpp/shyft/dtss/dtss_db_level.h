#pragma once


#include <shyft/dtss/ts_db_interface.h>
#include <shyft/time/utctime_utilities.h>



namespace shyft::dtss {

    namespace detail {
        struct ts_db_level_impl; // fwd. detailed impl.
    }

    /**
     * @brief configuration parameters for backend.
     * @details
     * Each parameter have reasonable defaults, have a look at google level db documentation for the effect
     * of max_file_size, write_buffer_size and compression.
     * The ppf remains constant once db is created (any changes will be ignored).
     * The other can be changed on persisted/existing databases.
     *
     * About compression:
     * Turns out although very effective for a lot of time-series, it have a single thread performance cost of 2..3x
     * native read/write performance due to compression/decompression.
     *
     * However, for geo dtss we are using multithreaded writes, so performance is limited to  the io-capacity,
     * so it might be set to true for those kind of scenarios.
     *
     */
    struct db_cfg {
        uint64_t ppf{1024} ;                     ///< 1 k points pr. key/value fragment, could be important for series that are updated at head(measurements)
        bool     compression{false};             ///< if true, snappy compression to the cost of 3x native speed single thread speed.
        uint64_t max_file_size{100*1024*1024};   ///< level db will keep internal files up to this size. choose a value that keeps reasonable number of files
        uint64_t write_buffer_size{10*1024*1024};///< internal write buffer, larger buffer, less io
        int64_t  log_level{200};                 ///< 400=fatal, 300=error, 200=warn, 100=info, 0= debug -1000=trace
        uint64_t test_mode{0};                   ///< impl specific, for testing errorhandling
        uint64_t ix_cache{0};                    ///< index cache size 0 means default size by level db.
        uint64_t ts_cache{0};                    ///< ts data cache, usually 0, since we do cache ts it self
        // py support ==
        bool operator==(db_cfg const&o) const {
            return ppf==o.ppf && compression==o.compression && max_file_size==o.max_file_size
            && write_buffer_size==o.write_buffer_size && log_level==o.log_level
            && test_mode==o.test_mode && ix_cache==o.ix_cache && ts_cache==o.ts_cache;
        }
        bool operator!=(db_cfg const&o) const { return !operator==(o);}
    };

    /**
     * @brief The shyft backend ts store using google level db
     */
    struct ts_db_level:its_db {

        unique_ptr<detail::ts_db_level_impl> impl;
        ts_db_level(const string& root_dir,db_cfg const& cfg);
        ~ts_db_level();

        string root_dir() const override;

        void save(const string& fn, const gts_t& ts, bool overwrite = true, const queries_t & queries = queries_t{}) override;
        void save(size_t n, fx_ts_item_t const& fx_item,bool overwrite, const queries_t & queries = queries_t{}) override;
        gts_t read(const string& fn, utcperiod p, const queries_t & queries = queries_t{}) override;
        void remove(const string& fn, const queries_t & queries = queries_t{}) override ;
        ts_info get_ts_info(const string& fn, const queries_t & queries = queries_t{}) override;
        vector<ts_info> find(const string& match, const queries_t & queries = queries_t{}) override;

    };

}
