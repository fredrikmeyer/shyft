#pragma once
/** This file is part of Shyft. Copyright 2015-2019 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <map>
#include <future>
#include <atomic>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/core/subscription.h>



namespace shyft::dtss::subscription {
    using std::vector;
    using std::make_shared;
    using std::forward;
    using std::string;
    //using std::string_view;
    using std::to_string;
    using std::runtime_error;
    using std::end;
    using std::unordered_map;
    using std::array;
    using std::recursive_mutex;

    using std::scoped_lock;
    using std::begin;
    using std::end;
    using std::atomic_int64_t;
    using namespace shyft::core;
    using namespace shyft::time_axis;
    using namespace shyft::time_series::dd;
    using shyft::core::subscription::observer_base;
    using shyft::core::subscription::manager_;
    
    /**  
     * @brief observer for ts-expressions
     *
     * @details
     * Given :
     * 
     *   1.  a subscription_manager (provided by the dtss, or the em-server ),
     *   2.  and unbound expression (provided by the end -user/web_api)
     *   
     * Then:
     *   1. create subscriptions for the terminals of the expression.
     *   2. keep and maintain the published version number as sum of expression terminal version number
     *   3. when changes are detected (separate thread, or event), then recompute expression and
     *       update the published version number.
     * 
     * Typically, this is used in the web-api (part of web-socket context) 
     * to keep track of changes to expression/time-series.
     * 
     */
    struct ts_expression_observer: observer_base {
        using Fx=std::function<ats_vector(ats_vector)>;// evaluation function takes a fresh expr and return results
        //--- inputs, as passed on by the constructor:
        ats_vector ts_expression_template;///< unbound untampered ts -expression vector
        
        //-- needed internal stuff to ensure subscription logic works
        ats_vector published_view;///< since we have a strong promise to only send if it is changed we need to keep the last values
        Fx fx; ///< callable fx(ats_vector clone_of_expr)->ats_vector, the result.

        template<class T, class F>
        ts_expression_observer(manager_ const &sm, string const&  request_id,T &&tsv, F&&fx ): observer_base(sm, request_id), ts_expression_template{tsv.clone_expr()},fx{fx} {
            subscribe_to(ts_expression_template);
        }
                
        void subscribe_to(ats_vector const &tsv) {
            vector<string> ts_ids;
            for(auto const&ts:tsv) {
                auto biv=ts.find_ts_bind_info();
                for (auto const&bi:biv )
                    ts_ids.emplace_back(bi.reference);
            }
            terminals= sm->add_subscriptions(ts_ids);
        }
                
        /** 
         * @brief recalculate the expression
         *
         * @details
         * Capture the terminal_version prior to re-calculate,
         * then execute the function that recalculate the expression.
         * IF the result is different from previous published_view
         * then update the published view and published version equal to captured terminal_version,
         * otherwise, just update the published version to the new terminal-version,
         * so that we are done with the change.
         * 
         * Typically, this routine is called by the observer thread, after it get
         * notified from the subscription manger that something did happen.
         * 
         */
        virtual bool recalculate() {
            auto cv=terminal_version();// capture version early here, (it could be updated)
            auto r=fx(ts_expression_template.clone_expr());// important, use clone expr from template
            if( r != published_view) {
                published_view=move(r);
                published_version=cv;// we can not safely capture terminal_version here, use conservative version in case of update glitch
                //worst case scenario, we have to evaluate once more.
                return true;
            } else 
                published_version=cv;
            return false;
        }
        
        ats_vector const& recompute_result() {
            (void)recalculate();
            return published_view;
        }
    };
    using ts_expression_observer_=shared_ptr<ts_expression_observer>;///<short hand for shared_ptr to 
    
}
