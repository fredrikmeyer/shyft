#include <shyft/dtss/dtss.h>
#include <shyft/web_api/web_api_generator.h> // looking for the geo ts url generator
#include <boost/dynamic_bitset.hpp>
#include <boost/thread/sync_bounded_queue.hpp>
#include <boost/algorithm/string.hpp>

namespace shyft::dtss {
    using shyft::time_series::dd::ts_as;
    using shyft::web_api::generator::geo_ts_url_generator;
    using shyft::time_series::dd::ats_vector;
    using std::ifstream;
    using std::ofstream;
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_nvp;
    using shyft::core::core_arch_flags;
    using shyft::core::from_seconds;
    using shyft::core::utcperiod_hasher;

       
    /** detects if a geo db is using internal time-series, using the prefix shyft:// */
    static bool is_internal_geo(string const& prefix) {
        return boost::starts_with(prefix,"shyft://");
    }
    
    geo::geo_ts_matrix server::do_geo_evaluate(geo::eval_args const &ea, bool use_cache, bool update_cache ) {
        using geo::detail::ix_calc;
        using geo::detail::geo_ix;
        if(msync) {
            return msync->geo_evaluate(ea,use_cache,update_cache);
        }
        

        std::shared_ptr<geo::ts_db_config> gdb;
        
        // copy gdb, consider: make multiread-singlewrite lock
        /** protected access on lockup, but should we take a reader-lock, or just copy the gdb-structure ? */{
            unique_lock<mutex> sl(c_mx);
            auto f=geo.find(ea.geo_ts_db_id);
            if( f==geo.end()) throw std::runtime_error("geo_evaluate: no geo-db configured for " + ea.geo_ts_db_id);
            gdb=std::make_shared<geo::ts_db_config>(*f->second);// make a copy we keep during the evaluation, considered cheap relative  other work
        }
        auto internal_gdb=is_internal_geo(gdb->prefix);
        if(!internal_gdb && !geo_read_cb) 
            throw runtime_error("dtss: no geo read callback set, geo-extensions not available");
        
        // validate variables and build the geo_ts_set
        // refactor to function taking query params, gdb.cfg -> geo_ts_set or similar
        auto gx=gdb->compute(ea);// gx is a fully specified slice into the geo ts-db
        if(gx.size()==0)
            return {};
        
        //
        //-- (1) create the indexed result structure, with empty time-series (to be filled in later)
        //

        auto rv=gdb->create_geo_ts_matrix(gx);// keep this as result structure, supporting pure-indexing 
        //
        //-- (2) do the cache-lookups and fill in references from ts-cache, while
        //       we at the same time build the __residual gross query__
        //
        //       that we will use to query the backend-store to fill in
        //       those not matched.
        //
        //       the cache.get(...) uses a linear indexing approach, 
        //       doing a callback to us for each of the
        //         ts_id(i),period(i) we search
        //       and for after lookup, it will call us with
        //         found_cb(i,bool found, ts_t const&ts_found_ref)
        //
        //       so we need a fast computed size_t ix -> tuple of (t0,v,e,g) indicies matching the above structure
        //        (lets generate it in the loop above)
        //    -- then
        //       our plan is to use the
        //         found_cb(i,found=false,ts=none) -> extend the __residual gross query__
        //        and
        //         found_cb(i,found=true,ts=found_ts) -> i->tuple of(t0,v,e,g) rv[t0][v][e][g].ts=found_ts
        //
        // Invoke the ts_cache with suitable callbacks to us.
        
        ix_calc rx(gx.t.size(),gx.v.size(),gx.e.size(),gx.g.size());//rx result index flat<-->ndim conversions by math
        auto t0_ta=gdb->t0_time_axis();
        vector<size_t> cache_misses;cache_misses.reserve(rx.size());
        auto t0_span=ea.ts_dt>utctime(0)?ea.ts_dt:gdb->dt;// get out the expected/defined range of 'forecasts'
        string ts_url;ts_url.reserve(500);// assumed reasonable max-limit(not hard, only performance)
        auto  ts_url_sink=std::back_inserter(ts_url);
        geo_ts_url_generator<decltype(ts_url_sink)> geo_ts_url_(gdb->prefix);
        geo::ts_id url_ix;url_ix.geo_db=ea.geo_ts_db_id;//assign only once, invariant for this query
        if(use_cache) {
            ts_cache.get(
                rx.size(),//  this is the number of items we request,
                [&gx,&rx,&geo_ts_url_,&ts_url,&ts_url_sink,&url_ix,&t0_ta](size_t i)->string { // this will be how we generate the ts-url
                    auto const ix=rx.ix(i);
                    ts_url.resize(0);//resize,keep memory
                    url_ix.v=gx.v[ix.v];// important, translate to cfg.referenced indexes
                    url_ix.e=gx.e[ix.e];//  --!--
                    url_ix.g=gx.g[ix.g];//  --!--
                    url_ix.t=t0_ta.time(ix.t0);// get the real-time from ix.t0, so no translate to cfg needed
                    boost::spirit::karma::generate(ts_url_sink,geo_ts_url_,url_ix);
                    return ts_url;// here we want a single copy string op(it has to be one)
                    
                },
                [&rx,&t0_ta,t0_span](size_t i)->utcperiod {// this will return the wanted (minimum) period requested from cache
                    auto t0=t0_ta.time(rx.ix(i).t0);// the start of this 'forecast', using the t0_ta as the correct reference
                    return utcperiod(t0,t0+t0_span);// the total covering period for this 'forecast'
                },
                [&rx,&rv,&cache_misses](size_t i, bool found,apoint_ts const& f_ts)->void { // we get called back for each item here.
                    if(found) {
                        auto const  ix=rx.ix(i);
                        rv._set_ts(ix.t0,ix.v,ix.e,ix.g,f_ts); // notice that here we use ix. directly to correctly address the resulting structure
                    } else {
                        cache_misses.emplace_back(i);
                    }
                }
            );
        } else {
            for(size_t i=0;i<rx.size();++i)//fill in all misses
                cache_misses.emplace_back(i);
        }
        
        // -- (3) Analyze the result(hits,misses) from above, and if needed call to backend to refill the missing.
        //    
        //    pre:The call to backend should return exactly what is requested,
        //        so the request need to be precise, and let's say we 
        //        use indicies referenced to the geo cfg (so that t0-idx is i'th t0 =cfg.t0[t0_idx],
        //            and v-idx is ref to the i'th variables as cfg.variables[v_idx] etc.)
        //        then the request is minimal, precise, and well defined.
        //        
        //        For the returned result-type, In python, it could be convinient to adress this as
        //        GeoTsRequestResult that maps to c++ geo_ts_request_result
        //        E.g:
        //        
        //          r=GeoTsRequestResult(n_t0,n_var,n_ens,n_geo)
        //          then
        //          r.assign_ts(t0,var,ens,geo,ts), where t0,var,ens,geo are all indexes as pr. result-indexing space
        //          r.ts(t0,var,ens,geo,ts) -> gives the TimeSeries at specified result index.
        //                 .. so we can range-check t0,var,ens,geo against the specified range at construction.
        //                    to minimize mixing up indexes-errors.
        //
        //        internally rep as ats_vector[n_t0*n_var*n_ens*n_geo]
        //          ats_vector[i], where i is computed as (n_t0*n_var*n_ens*n_geo)*t0 + (n_var*n_ens*n_geo)*v + (n_ens*n_geo)*e + g
        //                          reversed compute is g= i%n_geo, .. e=.., v=.., t0=..
        // 
        //   post:When handling the result of the callback request, we need to map
        //        the python GeoTsResult (alias c++ geo_ts_request_result)
        //        back into the result structure as provided/returned by this routine.
        //
        //        recall r[t0-ix][var-ix][ens_idx]::type geo_ts_vector, where the i'th member of geo_ts-vector represents ith point'
        //
        //        The most efficient manner is index-lookup-translations:
        //          r_map_t0[t0_request_idx] gives directly ix that goes into the rv[ix]
        //          r_map_v[v_request_idx] gives directly the ix that goes into the rv[t0_ix][v_ix]
        //
        if(cache_misses.size()>0) {
            // prep the call to backend assume we should read all:
            auto b_gx=gx;
            auto b_ta=ea.ta.size()?ea.ta:gdb->t0_time_axis();// empty means all
            // for speed, we need a direct lookup map from callback indicies to result-set indicies
            vector<size_t> t_map;// if all timepoints, then t_map[i] would be 0,1,.. n-1  etc.
            vector<size_t> v_map;// if all  variables then v_map[i] would be 0,1,..n-1 etc. 
            vector<size_t> e_map;
            vector<size_t> g_map;
            
            if(cache_misses.size()<rx.size()) { // optimize away if all missing, in this case: reduce what we read into strictly needed
                using boost::dynamic_bitset;
                t_map.reserve(gx.t.size());// if all timepoints, then t_map[i] would be 0,1,.. n-1  etc.
                v_map.reserve(gx.v.size());// if all  variables then v_map[i] would be 0,1,..n-1 etc. 
                e_map.reserve(gx.e.size());
                g_map.reserve(gx.g.size());
                b_gx.size_to_zero();
                
                // scan and register missing, using bit-map approach, note ix positions refers to rx indexing space (result, not cfg)
                // TODO: we could keep these, and be 100% exact on fetch/store, but
                // for now we accept we get the union of the slices missing(.e.g if one var is missing tx, and another missing ty, then we read both vars at tx,ty)
                dynamic_bitset<> xvar{gdb->variables.size()};
                dynamic_bitset<> xgeo{gdb->grid.points.size()};
                dynamic_bitset<> xens{(size_t)gdb->n_ensembles};
                dynamic_bitset<> xt0{t0_ta.size()};
                for(auto cm:cache_misses) {
                    auto const x=rx.ix(cm);
                    xvar[x.v]=true;
                    xens[x.e]=true;// i'th index ref to arg ens_ix[i]
                    xgeo[x.g]=true;
                    xt0[x.t0]=true;// i'th index referring to t0_ta time-points
                }
                // construct arguments to pass to geo read callback routine
                // they are either by value, or by ix-ref to cfg. tables
                for(size_t i=0;i<xvar.size();++i) if(xvar[i]) {b_gx.v.push_back(gx.v[i]);v_map.push_back(i);}//gx.v[i] ref to cfg.variables[i]
                for(size_t i=0;i<xens.size();++i) if(xens[i]) {b_gx.e.push_back(gx.e[i]);e_map.push_back(i);}//gx.e[i] ref to arg ens_idx, by value.
                for(size_t i=0;i<xgeo.size();++i) if(xgeo[i]) {b_gx.g.push_back(gx.g[i]);g_map.push_back(i);}//gx.g[i] ref. to positions found by  geo-query
                for(size_t i=0;i<xt0.size();++i) if(xt0[i]) {b_gx.t.push_back(t0_ta.time(i));t_map.push_back(i);}
                b_ta=gta_t(b_gx.t,t0_ta.total_period().end);// the end-point is not important
            } 
            //
            // here we perform the call to backend, getting the missing results back in a dense ats_vector format
            //                           cfg.v    cfg.points   by val   ta
            
            auto br= internal_gdb?do_internal_geo_read(gdb,b_gx): geo_read_cb(gdb, b_gx);// backend result
            
            // unwind the result from  the backend, and put it into the missing entries of the result structure
            // br[var_idx][geo_idx][ens_idx]::type ats_vector, keeping all results, ordered by t0
            // __x means final result-index reference
            // _i  means cb result reference (they might differ)
            
            vector<string> c_id;// cache these ts urls with ts below
            vector<apoint_ts> c_ts;// cache these ts
            if(update_cache) {// ensure to make room for result to cache
                size_t c_size=b_gx.size();
                c_id.reserve(c_size);
                c_ts.reserve(c_size);
            }
            for(size_t ti=0;ti<b_ta.size();++ti) {
                size_t tix=t_map.size()?t_map[ti]:ti;// if all t, then 1-1 map, else pick result-index from t_map.
                for(size_t vi=0;vi<b_gx.v.size();++vi) {
                    size_t vix=v_map.size()?v_map[vi]:vi;// if all v, then 1-1 map, else pick result index from v_map.
                    for(size_t ei=0;ei<b_gx.e.size();++ei) {
                        size_t eix=e_map.size()?e_map[ei]:ei;// if all e, then 1-1 map, else pick result index from e_map.
                        for(size_t gi=0;gi<b_gx.g.size();++gi) {
                           size_t gix=g_map.size()?g_map[gi]:gi;// if all e, then 1-1 map, else pick result index from e_map.
                           auto const & rts=br._ts(ti,vi,ei,gi);
                           rv._set_ts(tix,vix,eix,gix,rts);
                           if(update_cache) {// collect stuff to cache here.(avoid multiple calls for mutex)
                               ts_url.resize(0);//resize,keep memory
                               url_ix.v=gx.v[vix];// important, translate to cfg.referenced indexes
                               url_ix.e=gx.e[eix];//  --!--
                               url_ix.g=gx.g[gix];//  --!--
                               url_ix.t=t0_ta.time(tix);// get the real-time from ix.t0, so no translate to cfg needed
                               boost::spirit::karma::generate(ts_url_sink,geo_ts_url_,url_ix);
                               c_id.push_back(ts_url);
                               c_ts.emplace_back(rts);
                           }
                        }
                    }
                }
            }
            if(update_cache &&  c_id.size()) {
                ts_cache.add(c_id,c_ts);
            }
        }
            
        if(!ea.concat)
            return rv;//we are done
        //    
        // (4) if concat, execute that here
        //
        return rv.concatenate(ea.cc_dt0,ea.ta.period(0).timespan());
    }



    vector<geo::ts_db_config_> server::do_get_geo_info() {
        if(msync) { 
            return msync->get_geo_ts_db_info();
        } else {
            unique_lock<mutex> sl(c_mx);// ensure we got geo stable
            vector<geo::ts_db_config_> r;r.reserve(geo.size());
            for(auto const &kv:geo)
                r.emplace_back(make_shared<geo::ts_db_config>(*kv.second));
            return r;
        }
    }
    
    void server::add_geo_ts_db(geo::ts_db_config_ const &cfg) {
        if(!cfg) throw runtime_error("add_gs_ts_db must have a non-null argument");
        if(msync) {
            msync->add_geo_ts_db(cfg);
        } else {
            unique_lock<mutex> sl(c_mx);// ensure we got geo stable
            if(is_internal_geo(cfg->prefix)) {
                auto f=container_find(cfg->name,string(""));
                if(f->first == ""){ // create a new container
                    auto nc_root = f->second->root_dir() + "/" + cfg->name; // new container path is relative to root container
                    container[cfg->name] = std::make_unique<ts_db_level>(nc_root, default_geo_db_cfg);
                    geo[cfg->name]=cfg;
                    f=container_find(cfg->name,string(""));
                }
                else if(f == container.end()) // container not configured also no root container found
                    throw runtime_error("dtss.add_geo_ts_db: could not find or create ts-store container for name="+cfg->name);
                do_geo_ts_cfg_store(f->second->root_dir(),cfg);
            }
            geo[cfg->name] = std::make_shared<geo::ts_db_config>(*cfg);
        }
    }
    
    void server::remove_geo_ts_db(std::string const& geo_db_name) {
        if(msync) {
            msync->remove_geo_ts_db(geo_db_name);
        } else {
            unique_lock<mutex> sl(c_mx);// ensure we got geo stable
            auto f=geo.find(geo_db_name);
            if( f!=geo.end()) {
                // remove any cache-entries
                flush_cache();// for now just empty all, later be specific, remove items selective
                auto gdb=f->second;//keep a ref
                geo.erase(geo_db_name);// get rid of the entry
                if(is_internal_geo(gdb->prefix)) { // possibly cleanup internal stuff, could take a loong time if a lot of files!
                    its_db & ts_db=internal(f->second->name);
                    fs::path gdb_root= fs::path(ts_db.root_dir());
                    container.erase(geo_db_name);
                    fs::remove_all(gdb_root);// the last thing, otherwise win will fail on locked files
                }
            }// silently ignore attemt to remove non-existing geo_db_name ? The post-condition is the same
        }
    }
    
    
    void server::do_geo_ts_cfg_store(std::string const&root,geo::ts_db_config_ const &gdb) {
        auto cfg_path= fs::path(root)/geo_cfg_file; // configuration file is stored in its container root
        ofstream o(cfg_path.string(),std::ios::binary);
        core_oarchive oa(o,core_arch_flags);
        oa<<core_nvp("cfg",gdb);
    }
    
    void server::do_geo_store(std::string const& geo_db_name,geo::ts_matrix const& tsm, bool replace, bool cache) {
        using std::to_string;
        
        if(tsm.shape.size()==0)
            throw runtime_error("dtss geo store: no geo matrix data supplied");
        if(geo_db_name.size()==0)
            throw runtime_error("dtss geo store: no geo db-name is specified");
        if(msync) {
            msync->geo_store(geo_db_name,tsm,replace,cache);
        } else {
            shared_ptr<geo::ts_db_config> gdb;
            /** protected access on lockup,  copy the gdb-structure */{
                unique_lock<mutex> sl(c_mx);
                auto f=geo.find(geo_db_name);
                if( f==geo.end()) // here we could scan & load internal geo-db, instead of failing
                    throw std::runtime_error("geo_evaluate: no geo-db configured for " + geo_db_name);
                
                gdb=make_shared<geo::ts_db_config>(*f->second);// make a copy we keep during the evaluation, considered cheap relative  other work
            }
            auto internal_gdb=is_internal_geo(gdb->prefix);
            if(!internal_gdb && !geo_store_cb) 
                throw runtime_error("dtss: no geo store callback set, geo-extensions not available");
            // verify correct shape, we need it twice, so a local lambda helps us doing that:
            auto verify_shape_vs_gdb=[](geo::detail::ix_calc const&shape,geo::ts_db_config const&gdb) {
                if(shape.n_v != gdb.variables.size())
                    throw runtime_error("dtss geo store: variable dimension " + to_string(shape.n_v) + " differs from required " + to_string(gdb.variables.size()));
                if(int64_t(shape.n_e) != gdb.n_ensembles)
                    throw runtime_error("dtss geo store: ensemble dimension " + to_string(shape.n_e) + " differs from required " + to_string(gdb.n_ensembles));
                if(shape.n_g != gdb.grid.points.size())
                    throw runtime_error("dtss geo store: geo point  dimension " + to_string(shape.n_g) + " differs from required " + to_string(gdb.grid.points.size()));
            };
            verify_shape_vs_gdb(tsm.shape,*gdb);
            if(internal_gdb) do_internal_geo_store(gdb,tsm,replace);
            else             geo_store_cb(gdb,tsm,replace);// do the store operation here
            // figure out if we got more t0-versions, and update accordingly
            vector<utctime> t0x;t0x.reserve(tsm.shape.n_t0);
            auto t0_ta=gdb->t0_time_axis();
            for(size_t i=0;i<tsm.shape.n_t0;++i) {
                auto t0= gdb->get_associated_t0( tsm._ts(i,0,0,0).time(0),!replace); // important if concat (alias !replace)
                size_t t0_idx=t0_ta.index_of(t0);
                if(t0_idx==string::npos || t0_ta.time(t0_idx)!= t0) {
                    t0x.push_back(t0);
                }
            }

            unique_lock<mutex> sl(c_mx);
            if(t0x.size()) { // new points arrived, we update the t0 of  geo-db using scoped lock
                auto f=geo.find(geo_db_name);
                if( f==geo.end()) throw std::runtime_error("geo_evaluate: geo-db removed during write for " + geo_db_name);
                // here we could compare f->second vs gdb  to ensure the defs is consistent
                verify_shape_vs_gdb(tsm.shape,*f->second);// relax requirement, just  verify equal in v,e,g dimensions, allow t0 changes.
                auto need_sort= f->second->t0_times.size()? t0x.front()<f->second->t0_times.back():false;
                for(auto t:t0x) f->second->t0_times.push_back(t);
                if(need_sort) {
                    auto &t0v=f->second->t0_times;
                    std::sort(t0v.begin(),t0v.end());
                    auto last=std::unique(t0v.begin(),t0v.end());
                    if(last!=t0v.end()) {
                        t0v.erase(last,t0v.end()); // get rid of possible duplicates when updating existing t0
                    }
                }
                if(internal_gdb) {// if it's internal we take responsibility to store the most recent geo_ts_db_config
                    do_geo_ts_cfg_store(internal(f->second->name).root_dir(),f->second);
                }
            }
            if(cache ||replace) { // if replace, flush old items.
                // update/replace cache items stored. 
                //  this might take some time for large payloads (like million)
                string ts_url;ts_url.reserve(500);// assumed reasonable max-limit(not hard, only performance)
                auto  ts_url_sink=std::back_inserter(ts_url);
                geo_ts_url_generator<decltype(ts_url_sink)> geo_ts_url_(gdb->prefix);
                geo::ts_id url_ix;url_ix.geo_db=geo_db_name;//assign only once, invariant for this query

                ts_cache.update(
                    tsm.shape.size(),//  this is the number of items we request,
                    [&tsm,&geo_ts_url_,&ts_url,&ts_url_sink,&url_ix,replace,&gdb](size_t i)->string { // this will be how we generate the ts-url
                        auto ix=tsm.shape.ix(i);
                        ts_url.resize(0);//resize,keep memory
                        url_ix.v=ix.v;
                        url_ix.e=ix.e;
                        url_ix.g=ix.g;
                        url_ix.t=gdb->get_associated_t0(tsm.tsv[i].time(0),!replace); // again .. ensure we use correct t0 in case of concat
                        boost::spirit::karma::generate(ts_url_sink,geo_ts_url_,url_ix);
                        return ts_url;// here we want a single copy string op(it has to be one)
                        
                    },
                    [&tsm](size_t i)->apoint_ts const& {// 
                        return tsm.tsv[i];
                    },
                    cache,
                    false // hmm. we use replace as concat, so  let's just add to cache for now
                );

            }
        }
    }

    /** simple pod */
    namespace {
    struct ts_store_pkg {
        string ts_name;
        gpoint_ts const * gts{nullptr};// not owning, scope-life time keept by store-worker
    };
    }
    static size_t compute_n_workers(size_t n_ts) {
        auto hw_threads=std::thread::hardware_concurrency();
        if(hw_threads <2) hw_threads=2;
        return n_ts<hw_threads?n_ts:hw_threads;
    }
    
    void server::do_internal_geo_store(geo::ts_db_config_ cfg, geo::ts_matrix const&tsm,bool replace) {
        string ts_path;ts_path.reserve(100);
        auto  sink=std::back_inserter(ts_path);
        geo::ts_id tidx{cfg->name,0,0,0,utctime(0)};//reuse this in loop, reset ts_path
        geo_ts_url_generator<decltype(sink)> url_gen("");//drop the prefix, we want naked urls in this case
        its_db & ts_db=internal(cfg->name);//kept ref. hmm. could consider shared something here
        calendar utc;
        using boost::queue_op_status;
        using ts_store_pkgs=vector<ts_store_pkg>;
        using s_queue=boost::sync_bounded_queue<ts_store_pkgs>;
        using std::async;
        using std::launch;
        size_t batch_size=10000u;// assume this is suitable for underlying db.(Or at least minimizes the overhead substantially)
        size_t n_in_flight=compute_n_workers(1+(tsm.shape.size()/batch_size));
        s_queue wq(n_in_flight*2);// add some slack in the queue 
        vector<std::future<int>> wt;
        for(size_t i=0;i<n_in_flight;++i) {
            wt.emplace_back(
                async(launch::async,
                    [&wq,&ts_db,replace]()->int {// note, this is the logic for the queue worker thread
                        int error_count=0;
                        bool got_exception{false};
                        runtime_error re{""};
                        ts_store_pkgs pkg;
                        while (queue_op_status::closed != wq.wait_pull_front(pkg)) {
                            
                            if(pkg.size()) {
                                try {// avoid harakiri and writer wait-forever on run-time errors, continue as long as possible
                                    ts_db.save(pkg.size(),[&](size_t i) {return ts_item_t{pkg[i].ts_name,pkg[i].gts->rep};},replace);
                                } catch(runtime_error const &e) {
                                    ++error_count;
                                    re=e;
                                }
                            } else
                                ++error_count;// not very likely,
                        }
                        if(got_exception)
                            throw re;
                        return error_count;
                    }
                )
            );
        }
        ts_store_pkgs pkgs;pkgs.reserve(batch_size);
        for(size_t i=0;i<tsm.shape.n_t0;++i) {
            for(size_t v=0;v<tsm.shape.n_v;++v) {
                tidx.v=v;
                for(size_t e=0;e<tsm.shape.n_e;++e) {
                    tidx.e=e;
                    for(size_t g=0;g<tsm.shape.n_g;++g) {
                        ts_path.resize(0);// reset to 0, keep mem
                        tidx.g=g;
                        apoint_ts const &ts=tsm._ts(i,v,e,g);
                        auto gts = dynamic_cast<gpoint_ts const *>(ts.ts.get());
                        tidx.t=cfg->get_associated_t0(ts.time(0),!replace);// pr. def
                        generate(std::back_inserter(ts_path),url_gen,tidx);
                        pkgs.emplace_back(ts_store_pkg{ts_path,gts});
                        if(pkgs.size()==batch_size) {
                            wq.push_back(pkgs);
                            pkgs.resize(0);//keep mem
                        }
                    }
                }
            }
        }
        if(pkgs.size())
            wq.push_back(pkgs);// push last of batch that might be less than batch size
        wq.close();// signal end of story
        // ensure to collect all errors/threads
        int sum_errors{0};
        bool got_exception{false};
        runtime_error re{""};// We accept that this one might be overwritten, last exception counts
        for(auto &w:wt) {
            try {
                sum_errors+=w.get();
            } catch( runtime_error const &e) { // is this ok for now?
                re=e;
                got_exception=true;
            } catch( std::exception const &e) {// if we get another exception.. should we just collect it, and 
                got_exception=true;
                re=runtime_error(e.what());
            } catch(...) {
                got_exception=true;
                re=runtime_error("unknown exception from storage-worker");
            }
        }
        if(got_exception)
            throw re;
        if(sum_errors) 
            throw runtime_error("Failed to store all series, number of failures is "+ std::to_string(sum_errors));
        
        
    }
    namespace {
        /** simple pod for read queue operation */
        struct ts_read_pkg {
            string ts_name;
            apoint_ts * ats{nullptr};// target read assign, like *ats=read-result 
            utcperiod read_period;
        };
    }
    
    geo::ts_matrix server::do_internal_geo_read(geo::ts_db_config_ const &cfg,geo::slice const&gs) {
        geo::ts_matrix r=cfg->create_ts_matrix(gs);// create the result.
        string ts_path;ts_path.reserve(100);// we will regenerate this mill times, avoid malloc between
        auto  sink=std::back_inserter(ts_path);
        geo::ts_id tidx{cfg->name,0,0,0,utctime(0)};//reuse this in loop, reset ts_path
        geo_ts_url_generator<decltype(sink)> url_gen("");//drop the prefix, we want naked urls in this case
        its_db &ts_db=internal(cfg->name);// assume it succeeds here
        
        
        using boost::queue_op_status;
        using r_queue=boost::sync_bounded_queue<ts_read_pkg>;
        using std::async;
        using std::launch;
        size_t n_in_flight=compute_n_workers(gs.size());
        r_queue rq(n_in_flight*2);// add some slack in the queue 
        vector<std::future<int>> wt;
        for(size_t i=0;i<n_in_flight;++i) {
            wt.emplace_back(
                async(launch::async,
                    [&rq,&ts_db]()->int {
                        int error_count=0;
                        bool got_exception{false};
                        runtime_error re{""};
                        ts_read_pkg pkg;
                        while (queue_op_status::closed != rq.wait_pull_front(pkg)) {
                            
                            if(pkg.ats) {
                                try {// no harakiri on run-time errors, continue as long as possible
                                    apoint_ts rts(make_shared<const gpoint_ts>(ts_db.read(pkg.ts_name,pkg.read_period)));
                                    *pkg.ats = rts;// maybe direct inline expression above, or use move.
                                } catch(runtime_error const &e) {
                                    ++error_count;
                                    re=e;
                                }
                            } else
                                ++error_count;// not very likely,
                        }
                        if(got_exception)
                            throw re;
                        return error_count;
                    }
                )
            );
        }
        auto ts_dt=gs.ts_dt>utctime(0)?gs.ts_dt:cfg->dt;
        for(size_t i=0;i<gs.t.size();++i) {
            utcperiod read_period{gs.t[i],gs.t[i]+ts_dt};//maybe utc.add(ti,cfg->dt,1) to support yearly/monthly?
            // v,e,g dimensions share read-period
            tidx.t=gs.t[i];
            for(size_t v=0;v<gs.v.size();++v) {
                tidx.v=gs.v[v];
                for(size_t e=0;e<gs.e.size();++e) {
                    tidx.e=gs.e[e];
                    for(size_t g=0;g<gs.g.size();++g) {
                        ts_path.resize(0);// reset to 0, keep mem
                        tidx.g=gs.g[g];
                        generate(std::back_inserter(ts_path),url_gen,tidx);
                        rq.push_back(ts_read_pkg{ts_path,r._ts_pointer(i,v,e,g),read_period});
                    }
                }
            }
        }
        rq.close();// signal end of story, so that our read-workers quits the loop
        // ensure to collect all errors/threads
        int sum_errors{0};
        bool got_exception{false};
        runtime_error re{""};// We accept that this one might be overwritten, last exception counts
        for(auto &w:wt) {
            try {
                sum_errors+=w.get();
            } catch( runtime_error const &e) { // is this ok for now?
                re=e;
                got_exception=true;
            } catch( std::exception const &e) {// if we get another exception.. should we just collect it, and 
                got_exception=true;
                re=runtime_error(e.what());
            } catch(...) {
                got_exception=true;
                re=runtime_error("unknown exception from read-worker");
            }
        }
        if(got_exception)
            throw re;
        if(sum_errors)
            throw runtime_error("geo read: read failure for a number of requests " + std::to_string(sum_errors));

        return r;
    }
    
    std::optional<geo::ts_db_config_ > server::geo_ts_db_scan(fs::path root) const {
        if(fs::exists(root) && fs::is_directory(root)) {
                auto cfg_file=root/geo_cfg_file;
                if(fs::exists(cfg_file) && fs::is_regular_file(cfg_file)) {
                    try {
                        ifstream is(cfg_file.c_str(),std::ios::binary);
                        core_iarchive ia(is,core_arch_flags);
                        geo::ts_db_config_ cfg;
                        ia>>cfg;
                        if(cfg)
                            return cfg;
                    } catch(...) {
                        return {};
                    }
                }
        }
        return {};
    }

}
