/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <cstdint>
#include <cstdio>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <utility>
#include <functional>
#include <cstring>
#include <regex>
#include <variant>
#include <atomic>

#include <shyft/core/core_serialization.h>
#include <shyft/time_series/expression_serialization.h>
#include <shyft/core/core_archive.h>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <shyft/srv/fast_server_iostream.h>
#include <shyft/srv/fast_iosockstream.h>
#include <dlib/logger.h>
#include <dlib/misc_api.h>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/dtss_url.h>
#include <shyft/dtss/dtss_msg.h>
#include <shyft/dtss/dtss_db.h>
#include <shyft/dtss/dtss_db_level.h>
#include <shyft/dtss/dtss_krls.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/dtss/master_slave_sync.h>
#include <shyft/dtss/queue.h>

#include <shyft/dtss/geo.h>
#include <shyft/core/fs_compat.h>

namespace shyft::dtss {

    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::utctimespan;
    using shyft::core::no_utctime;
    using shyft::core::calendar;
    using shyft::core::deltahours;

    using gta_t = shyft::time_axis::generic_dt;
    using gts_t = shyft::time_series::point_ts<gta_t>;

    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::gts_t;
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::ts_as;
    using shyft::time_series::dd::geo_ts;
    using shyft::time_series::dd::geo_ts_vector;
    using shyft::time_series::dd::geo_ts_t0_var_ens_vector;// result of geo-eval

    struct master_slave_sync;
    // ========================================


    using ts_vector_t = shyft::time_series::dd::ats_vector;
    using ts_info_vector_t = std::vector<ts_info>;
    using id_vector_t = std::vector<std::string>;
    using read_call_back_t = std::function<ts_vector_t(const id_vector_t& ts_ids, utcperiod p)>;
    using store_call_back_t = std::function<void(const ts_vector_t&)>;
    using find_call_back_t = std::function<ts_info_vector_t(std::string search_expression)>;

    using std::unique_ptr;
    using std::make_unique;
    using std::unique_lock;
    using std::string;
    using server_iostream_t=shyft::srv::fast_server_iostream;
    //==============================


    /**
     * @brief A dtss server with time-series server-side functions
     * @details
     * The dtss server listens on a port, receives messages, interpret them
     * and ship the response back to the client.
     *
     * Callbacks are provided for extending/delegating find/read_ts/store_ts,
     * as well as internal implementation of storing time-series
     * using plain binary files stored in containers(directory).
     *
     * Time-series are named with url's, and all request involving 'shyft://'
     * like
     *   shyft://<container>/<local_ts_name>
     * resolves to the internal implementation.
     * 
     * geo-evaluate calls goes through the geo_evaluate engine,
     *   that uses the ordinary cache to get cached responses.
     *   the geo-ts do follow this url pattern
     *    geo://<geo_ts_id>/...
     * 
     * planned later: We can inverse map these to the time-series space
     *   e.g. TimeSeries('geo://arome/t/1/1/1')
     *   can automagically be mapped to the
     *   geo_ts_db 'arome' geo_ts_db, in case it is not found in the cache.
     *
     */
    
    struct server : server_iostream_t {


        using ts_cache_t = cache<apoint_ts_frag, apoint_ts>;
        using cwrp_t = unique_ptr<its_db>;
        using container_t =std::unordered_map<std::string, cwrp_t>; ///< type for keeping internal ts-db-containers
        
        // callbacks for extensions
        read_call_back_t bind_ts_cb;    ///< called to read non shyft:// unbound ts
        find_call_back_t find_ts_cb;    ///< called for all non shyft:// find operations
        store_call_back_t store_ts_cb;  ///< called for all non shyft:// store operations
        //-- geo extensions
        geo::read_call_back_t geo_read_cb;   ///< geo_read hook to supply ts not found in cache
        geo::store_call_back_t geo_store_cb; ///< geo_store hook to update the geo_ts_db with  more data
        
        // shyft-internal ts container implementation
        inline static const std::string geo_cfg_file{"geo.cfg"}; ///< geo_cfg_file are stored in the root of internal geo-db containers.
        
        inline static const std::string container_query{ "container" };///< Query key used to specify container types
        inline static const std::array<std::string, 1> remove_queries{{ container_query }};///< Sequence of query keys to be removed before passing the query map to the containers.
        mutex c_mx;///< container mutex
        
        container_t container;  ///< mapping of internal shyft <container>
        std::unordered_map<std::string,geo::ts_db_config_ > geo;///< mapping of geo_evaluate requests goes through this
        db_cfg default_geo_db_cfg;  ///< the level db cfg for defaulted geo databases, created by clients.
        ts_cache_t ts_cache{ 1000000 };  ///< default 1 mill ts in cache
        bool cache_all_reads{ false };
        bool can_remove{ false };
        std::atomic_bool terminate { false };
        
        std::future<void> msync_worker;///< used to wait on termination
        unique_ptr<master_slave_sync> msync; ///< Connection to dtss master, all read requests are forwared, if dtss in slave modus
        shyft::core::subscription::manager_ sm=make_shared<shyft::core::subscription::manager>();///< the subscription_manager so that web-api can support observable expression-vectors
        std::atomic_size_t alive_connections{0};///< counts live connections, ref on_connect and scoped_count
        
        //--  queue support system
        queue::q_mgr queue_manager;///< the queue manager, that have toplevel fx, tsv_queue_ () operator to provide individual queue functionality
        
        // constructors
        server()=default;
        server(server&&)=delete;
        server(const server&) =delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;

        template < class CB >
        explicit server(CB && cb)
            : bind_ts_cb{ std::forward<CB>(cb) } {
        }

        template < class RCB, class FCB>
        server(RCB && rcb, FCB && fcb)
            : bind_ts_cb{ std::forward<RCB>(rcb) }, find_ts_cb{ std::forward<FCB>(fcb) } {
        }

        template < class RCB, class FCB, class SCB >
        server(RCB && rcb, FCB && fcb, SCB && scb)
            : bind_ts_cb{ std::forward<RCB>(rcb) }, find_ts_cb{ std::forward<FCB>(fcb) },
              store_ts_cb{ std::forward<SCB>(scb) } {
        }
        template < class RCB, class FCB, class SCB, class GRCB,class GSCB>
        server(RCB && rcb, FCB && fcb, SCB && scb, GRCB && grcb, GSCB && gscb)
            : bind_ts_cb{ std::forward<RCB>(rcb) }, find_ts_cb{ std::forward<FCB>(fcb) },
              store_ts_cb{ std::forward<SCB>(scb) },
              geo_read_cb{ std::forward<GRCB>(grcb)},
              geo_store_cb{std::forward<GSCB>(gscb)}
              {}

        ~server();

        //-- container management, that you can optionally override, 
        virtual void add_container(
            const std::string & container_name, const std::string & root_dir,
            std::string container_type = std::string{},
            db_cfg cfg=db_cfg{}
        );

        container_t::iterator container_find(const std::string & container_name, const std::string & container_query);
        virtual its_db & internal(const std::string & container_name, const std::string & container_query = std::string{});

        /** start the server in background, return the listening port used in case it was set unspecified */
        int start_server() {
            if(get_listening_port()==0) {
                start_async();
                while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
                    std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
            } else {
                start_async();
            }
            return get_listening_port();
        }
        //-- expose cache functions

        void add_to_cache(id_vector_t&ids, ts_vector_t& tss) { ts_cache.add(ids,tss);}
        void remove_from_cache(id_vector_t &ids) { ts_cache.remove(ids);}
        cache_stats get_cache_stats() { return ts_cache.get_cache_stats();}
        void clear_cache_stats() { ts_cache.clear_cache_stats();}
        void flush_cache() { return ts_cache.flush();}
        void set_cache_size(std::size_t max_size) { ts_cache.set_capacity(max_size);}
        void set_cache_memory_target_size(std::size_t max_size) { ts_cache.set_mem_max(max_size);}
        void set_ts_size(std::size_t ts_average_size) { ts_cache.set_ts_size(ts_average_size);}
        void set_auto_cache(bool active) { cache_all_reads=active;}
        std::size_t get_cache_size() const {return ts_cache.get_capacity();}
        std::size_t get_ts_size() const {return ts_cache.get_ts_size();}
        std::size_t get_cache_memory_target_size() const {return ts_cache.get_mem_max();}
        void set_can_remove(bool can_remove) { this->can_remove = can_remove; }

        ts_info_vector_t do_find_ts(const std::string& search_expression);
        ts_info do_get_ts_info(const std::string & ts_url);

        std::string extract_url(const apoint_ts&ats) const {
            auto rts = ts_as<aref_ts>(ats.ts);
            if(rts)
                return rts->id;
            throw runtime_error("dtss store.extract_url:supplied type must be of type ref_ts");
        }

        void do_cache_update_on_write(const ts_vector_t&tsv,bool overwrite_on_write);

        void do_store_ts(const ts_vector_t & tsv, bool overwrite_on_write, bool cache_on_write);

        void do_merge_store_ts(const ts_vector_t & tsv, bool cache_on_write);
        /** @brief Read the time-series from providers for specified period
        *
        * @param ts_ids identifiers, url form, where shyft://.. is specially filtered
        * @param p the period to read
        * @param use_ts_cached_read allow reading results from already existing cached results
        * @param update_ts_cache when reading, also update the ts-cache with the results
        * @return read ts-vector in the order of the ts_ids
        */
        ts_vector_t do_read(const id_vector_t& ts_ids,utcperiod p,bool use_ts_cached_read,bool update_ts_cache);
        void do_remove_ts(const std::string & ts_url);
        void do_bind_ts(utcperiod bind_period, ts_vector_t& atsv,bool use_ts_cached_read,bool update_ts_cache);
        ts_vector_t do_evaluate_ts_vector(utcperiod bind_period, ts_vector_t& atsv,bool use_ts_cached_read,bool update_ts_cache,utcperiod clip_period);
        ts_vector_t do_evaluate_percentiles(utcperiod bind_period, ts_vector_t& atsv, gta_t const&ta,std::vector<int64_t> const& percentile_spec,bool use_ts_cached_read,bool update_ts_cache);
        ts_vector_t do_slave_read(const id_vector_t& ts_ids,utcperiod p,bool use_ts_cached_read);
        
        /** 
         * @brief assign master dtss for this dtss
         * @details
         * This changes the mode of operation for this dtss instance.
         * 
         * All physical IO requests, and GEO requests are forwarded to the 
         * designated master dtss as specified with the ip and port number.
         * 
         * The local dtss cache and subscriptions are kept in sync with the master
         * with the given parameters.
         * 
         * @param ip          the ip address of the master dtss
         * @param port        the port number of the master dtss instance
         * @param master_poll_time the maximum number of seconds to wait between polling the master for relevant changes(subscriptions)
         * @param unsubscribe_min_threshold the minimum number of unsubscription events before propagating it to unsubscribe from master
         * @param unsubscribe_max_delay the maximum time to wait, regardless count, before unsubscribeing wasted items 
         */
        void set_master(string ip, int port, double master_poll_time,size_t unsubscribe_min_threshold,double unsubscribe_max_delay);
        /** @brief geo_evaluate 
         *
         * Peforms the geo-evaluate server side work
         * 
         * @param eval_args specifies the scope of geo-evaluation
         * @param use_cache use the dtss cache, if available
         * @param update_cache stash any new reads into cache
         * @return a vector of the results, of ta.size() length, one for each t0, or in case of concat just one
         */
        geo::geo_ts_matrix do_geo_evaluate(geo::eval_args const &eval_args, bool use_cache, bool update_cache );
        
        /** @brief do_geo_store 
         *
         * @details
         * Stores the supplied ts-matrix to the backend.
         * 
         * @param geo_db_name the name of the geo data-base
         * @param tsm 1 or more t0 dimensions of complete variable,ens, geo dimensions to store
         * @param replace if there already exis forecasts/time-series, this will replace, merge with previous
         * @param cache if true, also put the time-series to in-memory cache for fast retrieval
         * 
         */
        void do_geo_store(std::string const& geo_db_name,geo::ts_matrix const& tsm, bool replace, bool cache);
        void do_internal_geo_store(geo::ts_db_config_ cfg, geo::ts_matrix const&tsm,bool replace);
        geo::ts_matrix do_internal_geo_read(geo::ts_db_config_ const &cfg,geo::slice const&gs);
        
        /** @brief get geo_ts info available on the server */
        vector<geo::ts_db_config_> do_get_geo_info();

        /** @brief get name of all containers set on the server */
        id_vector_t do_get_container_names();

        
        /** @brief add a geo_ts db container/service
         * 
         * @details
         * adds a geo-ts db according to the 
         * instance passed.
         * If there is already registered, exception is thrown.
         * We require that user first call remove_geo_ts_db
         * 
         */
        void add_geo_ts_db(geo::ts_db_config_ const &cfg);
        
        /** @brief remove a geo_ts_db, unregister/flush cache */
        void remove_geo_ts_db(std::string const& geo_db_name);
        
        /** @brief scan container for and read a geo-ts-db configuration
         * @details scan a container directory for a geo.cfg file and attempts to load it.
         * @param root points to the container directory.
         * @return optionally returns a  geo-ts-db config if file found and load successful
         */
        std::optional<geo::ts_db_config_> geo_ts_db_scan(fs::path root) const;
        
        /** @brief store current state of internal geo_ts_cfg
         * @details store the geo::ts_db_config to specified root dir so that
         * it can be found later, and used to bootstrap the geo-part of the dtss.
         * It overwrites/replaces any other file at the location.
         * 
         * @param root the root of the ts_db container, the full filename is formed using root/name/geo_cfg_file
         * @param cfg the geo::ts_db_config to store
         */
        
        void do_geo_ts_cfg_store(std::string const&root,geo::ts_db_config_ const &cfg);
        
        // ref. dlib, all connection calls are directed here
        void on_connect(
            std::istream & in,
            std::ostream & out,
            const std::string & foreign_ip,
            const std::string & local_ip,
            unsigned short foreign_port,
            unsigned short local_port,
            dlib::uint64 connection_id
        );
    };
    
         

} // shyft::dtss


