#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/grammar_type_rules.h>

namespace shyft::web_api::grammar {

    void insert_element(json& j, std::pair<string, value_type> kvp) {
        j.m.emplace(kvp.first, kvp.second);
    }

    template<typename Iterator, typename Skipper>
    json_grammar<Iterator, Skipper>::json_grammar():
    json_grammar::base_type(json_, "json") {

        json_list_ = lit('[') >> -(json_ % ',') >> lit(']');
        json_table_ = lit('[') >> -(json_list_ % ',') >> lit(']');
        string_list_ = lit('[') >> -(quoted_string_ % ',') >> lit(']');
        value_ = (strict_double_ | int_ | integer_list_ | quoted_string_  |
            bool_ | period_ | time_axis_ | // Period must come before string_list to remove ambiguity.
            string_list_ | mi_ |
            stm_session_ | stm_case_ | model_ref_ |
            proxy_ | proxy_list_ | // Proxy- and proxy_list should also be late in the parser, as it contains for instance string.
			json_ | json_list_  | json_table_); // Json- and json-list have to be last in these conditionals to handle grammar ambiguities correctly.

        pair_ = (quoted_string_ >> ":" >> value_);

        json_ =
            ( lit("null") |
            (lit("{")
                >> -(pair_[phx::bind(insert_element, _val, _1)] % ",")
                >> lit("}") )
            );

        json_list_.name("json list");
        json_table_.name("json table");
        value_.name("json value type");
        pair_.name("key-value pair");
        on_error<fail>(json_, error_handler(_4, _3, _2));
    }

    template struct json_grammar<request_iterator_t, request_skipper_t>;
}
