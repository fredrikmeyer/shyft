#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    inline read_ts_request mk_read_ts_request(string const&request_id,utcperiod const&r,utcperiod const&c,bool cache,vector<string>const& ids,boost::optional<bool> opt_sub, boost::optional<bool> ts_fmt) {
        return read_ts_request{request_id,r,c,cache,ids, opt_sub.get_value_or(false),ts_fmt.get_value_or(false)};
    }

    template<typename Iterator,typename Skipper>
    read_ts_request_grammar<Iterator,Skipper>::read_ts_request_grammar() : read_ts_request_grammar::base_type(start,"read_ts_request") {

        start = (
                lit("read") > lit('{')
                > lit("\"request_id\"")   > ':' > quoted_string > ','
                > lit("\"read_period\"" ) > ':' > p_ > ','
                > lit("\"clip_period\"")  > ':' > p_ > ','
                > lit("\"cache\"")  > ':' > bool_ > ','
                > lit("\"ts_ids\"")       > ':' > '[' > (quoted_string % ',') > lit(']') 
                > -(','>lit("\"subscribe\"") > ':' > bool_ )
                > -(','>lit("\"ts_fmt\"") > ':' > bool_ )>

                lit('}')
        )
        [ _val = phx::bind(mk_read_ts_request,_1,_2,_3,_4,_5,_6,_7) ];
        start.name("read_request");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct read_ts_request_grammar<request_iterator_t,request_skipper_t>;

}
