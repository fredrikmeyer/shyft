#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/grammar_type_rules.h>


namespace shyft::web_api::grammar {
    template<typename Iterator, typename Skipper>
    xy_point_grammar<Iterator, Skipper>::xy_point_grammar():
        xy_point_grammar::base_type(start, "xy_point") {
        start = lit("(")
            >> double_ [phx::bind(&point::x, _val) = _1]
            >> lit(",") >> double_[phx::bind(&point::y, _val) = _1]
            >> lit(")");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    template<typename Iterator, typename Skipper>
    xy_point_list_grammar<Iterator, Skipper>::xy_point_list_grammar():
        xy_point_list_grammar::base_type(start, "xy_point_list") {
        start = lit("[")
              >> (point_[phx::push_back( phx::bind(&xy_point_curve::points, _val), _1)] % ",")
              >> lit("]");
        point_.name("point");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    template<typename Iterator, typename Skipper>
    xyz_curve_grammar<Iterator, Skipper>::xyz_curve_grammar():
        xyz_curve_grammar::base_type(start, "xyz_point_list") {
        start = lit("{") >> lit("\"z\":") 
                >> double_[phx::bind(&xy_point_curve_with_z::z, _val) = _1]
                >> lit(",") >> lit("\"points\":")
                >> xy_list_[phx::bind(&xy_point_curve_with_z::xy_curve, _val) = _1]
                >> lit("}");
        xy_list_.name("List of points");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    template<typename Iterator, typename Skipper>
    xyz_list_grammar<Iterator, Skipper>::xyz_list_grammar():
        xyz_list_grammar::base_type(start, "xyz_curve_list") {
        start = lit("[")
              >> (curve_[phx::push_back(_val, _1)] % ",")
              >> lit("]");
        curve_.name("xy_point_curve_with_z");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    
    template<typename Iterator, typename Skipper>
    turbine_efficiency_grammar<Iterator, Skipper>::turbine_efficiency_grammar():
        turbine_efficiency_grammar::base_type(start, "turbine_efficiency") {
        start = lit("{")
                >> lit("\"production_min\":") >> double_[phx::bind(&turbine_efficiency::production_min, _val) = _1] >> ","
                >> lit("\"production_max\":") >> double_[phx::bind(&turbine_efficiency::production_max, _val) = _1] >> ","
                >> lit("\"production_nominal\":") >> double_[phx::bind(&turbine_efficiency::production_nominal, _val) = _1] >> ","
                >> lit("\"fcr_min\":") >> double_[phx::bind(&turbine_efficiency::fcr_min, _val) = _1] >> ","
                >> lit("\"fcr_max\":") >> double_[phx::bind(&turbine_efficiency::fcr_max, _val) = _1] >> ","
                >> lit("\"efficiency_curves\":")
                    >> "[" >> -(xyz_curve_[phx::push_back( phx::bind(&turbine_efficiency::efficiency_curves, _val), _1)] % ",") >> "]"
                >> lit("}");
        xyz_curve_.name("xy_point_curve_with_z");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    template<typename Iterator, typename Skipper>
    turbine_description_grammar<Iterator, Skipper>::turbine_description_grammar():
        turbine_description_grammar::base_type(start, "turbine_description") {
        start = lit("{")
                >> lit("\"turbine_efficiencies\":")
                >> "[" >> -(turbine_efficiency_[phx::push_back(phx::bind(&turbine_description::efficiencies, _val), _1)] % ",") >> "]"
                >> lit("}");
        turbine_efficiency_.name("Turbine efficiency");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    
    template<typename T>
    void insert_t_value_pair(shared_ptr< map< utctime, shared_ptr<T> > >& vmap, const utctime& t, const T& value){
        if (!vmap){
            vmap = std::make_shared< map<utctime, shared_ptr<T> >>();
        }
        vmap->insert({t, std::make_shared<T>(value)});        
    }
    
    template<typename Iterator, typename T, typename TGrammar, typename Skipper>
    t_map_grammar<Iterator, T, TGrammar, Skipper>::t_map_grammar():
        t_map_grammar::base_type(start, "t_map"){
        start = lit("{")
              >> -((time_ >> ":" >> range_)[phx::bind(&insert_t_value_pair<T>, _val, _1, _2)] % ",")
              >> "}";
        time_.name("time");
        range_.name("value");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }


    template struct xy_point_grammar<request_iterator_em, request_skipper_em>;
    template struct xy_point_list_grammar<request_iterator_em, request_skipper_em>;
    template struct xyz_curve_grammar<request_iterator_em, request_skipper_em>;
    template struct xyz_list_grammar<request_iterator_em, request_skipper_em>;
    template struct turbine_efficiency_grammar<request_iterator_em, request_skipper_em>;
    template struct turbine_description_grammar<request_iterator_em, request_skipper_em>;

    //template struct absolute_constraint_grammar<request_iterator_em, request_skipper_em>;
    //template struct penalty_constraint_grammar<request_iterator_em, request_skipper_em>;
    // Parsers for value types of attribute_valueibutes:
    template struct t_map_grammar<request_iterator_em, xy_point_curve, xy_point_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    template struct t_map_grammar<request_iterator_em, xy_point_curve_with_z, xyz_curve_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    template struct t_map_grammar<request_iterator_em, vector<xy_point_curve_with_z>, xyz_list_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;
    template struct t_map_grammar<request_iterator_em, turbine_description, turbine_description_grammar<request_iterator_em, request_skipper_em>, request_skipper_em>;


    template<typename Iterator, typename Skipper>
    attribute_value_list_grammar<Iterator, Skipper>::attribute_value_list_grammar():
        attribute_value_list_grammar::base_type(start, "attribute_value_list") {
        start %= lit("[")
            >> -(proxy_ % ",") 
            >> lit("]");
        proxy_.name("Proxy attribute");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    //template struct attribute_value_type_grammar<request_iterator_em, request_skipper_em>;
    template struct attribute_value_list_grammar<request_iterator_em, request_skipper_em>;
}
