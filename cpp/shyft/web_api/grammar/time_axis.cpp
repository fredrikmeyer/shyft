#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {


    inline auto mk_fixed_dt(utctime t,utctime dt, size_t n) {
        if(dt== utctime(0)) throw std::runtime_error("fixed dt time-axis must have dt>0");
        return shyft::time_axis::generic_dt(t,dt,n);
    }
    inline auto mk_cal_dt(std::string const& tz,utctime t,utctime dt, size_t n) {
        if(dt== utctime(0)) throw std::runtime_error("calendar dt time-axis must have dt>0");
        return shyft::time_axis::generic_dt(std::make_shared<shyft::core::calendar>(tz),t,dt,n);
    }
    inline auto mk_pt_dt(std::vector<utctime> const&t) {return shyft::time_axis::generic_dt(t);}



    template<typename Iterator,typename Skipper>
    time_axis_grammar<Iterator,Skipper>::time_axis_grammar():time_axis_grammar::base_type(start,"time_axis_dt") {
            // one of the following syntaxes are valid
            start = lit('{')>>
                     ( ((lit("\"t0\"")>':'>t_>','>lit("\"dt\"")>':'>t_>','>lit("\"n\"")>':'>qi::uint_)[_val=phx::bind(mk_fixed_dt,_1,_2,_3)])
                   | ((lit("\"calendar\"")>':'>quoted_string>','>lit("\"t0\"")>':'>t_>','>lit("\"dt\"")>':'>t_>','>lit("\"n\"")>':'>qi::uint_)[_val=phx::bind(mk_cal_dt,_1,_2,_3,_4)])
                   | ((lit("\"time_points\"")>>':'>>time_points_)[_val=phx::bind(mk_pt_dt,_1)])
                ) >> lit('}')
            ;
            start.name("time_axis");
            on_error<fail>(start, error_handler(_4, _3, _2));
        }

    template struct time_axis_grammar<request_iterator_t,request_skipper_t>;

}
