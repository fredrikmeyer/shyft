#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {
    
    template<typename Iterator,typename Skipper>
    web_request_grammar<Iterator,Skipper>::web_request_grammar() : web_request_grammar::base_type(start,"web_request") {
        start = ( find_ts_ | read_ts_ | average_ts_ | percentile_ts_| store_ts_ | info_ |unsubscribe_
            | q_list_ | q_info_ | q_infos_ | q_size_ | q_maintain_ | q_put_ | q_get_ | q_done_
        ); // one of these constructs are accepted
        start.name("web_request");
        on_error<fail>(start, error_handler(_4, _3, _2));
    }

    template struct web_request_grammar<request_iterator_t,request_skipper_t>;

}
