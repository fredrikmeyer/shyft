#include <shyft/web_api/web_api_grammar.h>

namespace shyft::web_api::grammar {

    template<typename Iterator, typename Skipper>
    find_reply_grammar<Iterator,Skipper>::find_reply_grammar():find_reply_grammar::base_type(start,"find_reply_grammar") {
        constexpr auto mk_find_response=[](string const&r, vector<ts_info> const&tsi)->find_reply {
            return find_reply{r,tsi};
        };
        start= (
            lit('{')
                >> lit("\"request_id\"") >>lit(':')  >>quoted_string_   >> lit(',')
                >> lit("\"result\"")   >>lit(':')  >>ts_infos_
            >> lit('}')
        )[_val=phx::bind(mk_find_response, _1,_2)]
        ;

        on_error<fail>(start, error_handler(_4, _3, _2));
    }
    template struct find_reply_grammar<request_iterator_t,request_skipper_t>;


}
