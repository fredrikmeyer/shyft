/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>

namespace shyft::web_api::generator {

    template<class OutputIterator>
    utctime_generator<OutputIterator>::utctime_generator()
        : utctime_generator::base_type(pg)
    {
        using ka::true_;
        using ka::bool_;
        pg =  
            (
            &bool_(true)[ka::_1= ka::_val != no_utctime ] 
                << time_[ka::_1=phx::bind(shyft::core::to_seconds,ka::_val)]
            )
            |
            (
            &true_ << ka::lit("null")
            )
        ;
        pg.name("utctime");
    }

    template struct utctime_generator<generator_output_iterator>;
}
