/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/web_api/web_api_generator.h>


namespace shyft::web_api::generator {
    template<class OutputIterator>
    point_generator<OutputIterator>::point_generator() : point_generator::base_type(pg) {
        using ka::true_;
        using ka::bool_;
        using ka::_val;
        using ka::_1;
        static constexpr auto  is_num=[](point const&x){return std::isfinite(x.v);};
        pg =
              &bool_(true)[_1= phx::bind(is_num,_val) ] << ( '[' <<  time_[_1=phx::bind(&point::t,_val)]<< ',' << d_[_1=phx::bind(&point::v,_val)] << ']')
                |          // notice that we emit null here instead of NaN, we can adjust to fit!
             &true_ << ( '[' <<  time_[ka::_1=phx::bind(&point::t,_val)]<< ',' << "null" << ']')

        ;
        pg.name("point");
    }

    template struct point_generator<generator_output_iterator>;
}

