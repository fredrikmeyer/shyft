#pragma once
#include <memory>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <cstdint>

#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/karma_real.hpp>

namespace shyft::web_api::generator {

using std::vector;
using std::string;
using std::string_view;
using std::map;
using std::int64_t;
using std::int16_t;
using std::uint16_t;


namespace ka=boost::spirit::karma;
namespace phx=boost::phoenix;

using ka::int_;
using ka::long_long;
using ka::double_;
using ka::bool_;    
using boost::spirit::karma::generate; 

constexpr char quote='"';
constexpr char colon=':';
constexpr char obj_begin='{';
constexpr char obj_end='}';
constexpr char arr_begin='[';
constexpr char arr_end=']';
constexpr char comma=',';
constexpr char esc = '\\';


/** 
 * @module json-emit
 * 
 * @description
 *
 * What we try to achieve
 *
 * 1. speed/efficiency:
 *     hpc          : re-using boost::spirit::karma generators for time,period,time-series etc. since they do carry the
 *                    volume/performance critical parts
 *     zero-copy    : do not copy strings,etc, to emit (consider friend
 *
 * 2. ease of use/extension
 *     few lines    : we would like to have like one line code for each attribute exposed (for a given context).
 *     composition  : compose aggregates
 *     easy reading : direct code,
 *     parameterize : send parameters to emitter to control form/amount of emitted code.
 *     testing      : by composition
 *     !intrusive   : should be possible to use existing classes with no mods (maybe a friend class access, is allowed in case we start with private members).
 *
 * 3. what we do not yet need
 *     general framework : unless it satisfies the above requirements, including reusing boost::spirit::karma for the hpc parts.
 *
 */


/**  @brief emit a 'null' */
template <class OutputIterator> void 
emit_null(OutputIterator& oi) {*oi++='n';*oi++='u';*oi++='l';*oi++='l';}

/**
 * @brief emit, the base class for emit functionality 
 * 
 * @details
 * The template to be overloaded/specialized for each type T.
 * @note that it is the constructor(OutputIterator&oi, const T&t) that should be specialized.
 * 
 * @tparam OutputIterator Iterator for stream type to emit to.
 * @tparam T Class to emit
 */
template<class OutputIterator, class T>
struct emit {
    /** 
     * @brief Constructor, so it can be called as emit(oi,t);
     *
     * @param oi: Output iterator to write to.
     * @param t: instance of T to emit to stream.
     */
    emit(OutputIterator& oi, const T& t) {
        auto value = string("ERROR: ") + typeid(t).name();
        copy(begin(value), end(value), oi);
        //throw std::runtime_error(typeid(T).name());
    }
};


/**
 * @brief Helper class for partial specialization to handle shared_ptr<T>
 * @details
 * Produces 'null' or derefd object {}
 * 
 * Lets say you have a shared_ptr<T> and you would like the json_emit to produce a 'null', \
 * or the shared pointer is not null then
 * produce the json object, like { .. } etc.
 * @note use x_emit_shared_ptr(T) to automate it.
 * 
 * @tparam OutputIterator
 * @tparam T
 */
template<class OutputIterator, class T>
void emit_shared_ptr(OutputIterator& oi, std::shared_ptr<T> const &t) {
    if(t==nullptr) emit_null(oi);
    else emit<OutputIterator,T>(oi,*t); // Here we assume that emit<OutputIterator,T> has been specialized
}
#define x_emit_shared_ptr(T) template<class OutputIterator> struct emit<OutputIterator, std::shared_ptr<T>> { emit(OutputIterator& oi, std::shared_ptr<T> const& t){emit_shared_ptr(oi,t);}}

/** 
 * @brief emit a vector of values
 * @details
 * Produces [ t1..tn ] output
 * @note  use x_emit_vec(T) to automate the generation of the class
 */
template<class OutputIterator, class T>
void emit_vector(OutputIterator& oi, std::vector<T> const& t){
    *oi++=arr_begin;
    bool first=true;
    for(auto const&v : t) {
        if(!first) *oi++=comma; else first=false;
        emit<OutputIterator, T>(oi,v);
    }
    *oi++=arr_end;
}
#define x_emit_vec(T) template<class OutputIterator> struct emit<OutputIterator, std::vector<T>> {emit(OutputIterator& oi, std::vector<T> const& t) {emit_vector(oi,t);}}

/**
 * @brief emit a vector of values, where what do generate at each
 * entry is provided through a provided function.
 *
 * @tparam OutputIterator: Type of iterator to write to
 * @tparam T: Type vector contains
 * @tparam Fx: Callback function type. Should be equivalent to void(*fx)(OutputIterator&, T const&)
 *
 * @param oi: Iterator to generate output to.
 * @param t: Vector of instances to generate output from
 * @param fx: Callback function to apply to each entry.
 *
 * If t is a vector with elements t1,t2,...,tN
 * emit_vector_fx writes to oi a list like
 *   [fx(oi,t1),fx(oi,t2),...,fx(oi,tN)]
 */
template<class OutputIterator, typename T, class Fx>
void emit_vector_fx(OutputIterator& oi, T const& t, Fx&& fx) {
    *oi++ = arr_begin;
    for(auto it = t.begin(); it != t.end(); it++) {
        if (it != t.begin()) *oi++ = comma;
        fx(oi, *it);
    }
    *oi++ = arr_end;
}

/** 
 * @brief emit a map<Key,T> value
 * @details
 * This resolves typical time-map (time-dependent attributes required in the stm9
 *
 * as [ [key,value],...] sequence
 * assuming that emit(oi,utctime) and emit(oi,T) exists.
 * @tparam OutputIterator the sink type
 * @tparam Key key type of the map
 * @tparam T   value type of the map
 */
template<class OutputIterator, class Key, class T>
void emit_map(OutputIterator&oi,std::map<Key,T> const &value) {
    *oi++=obj_begin;
    bool first=true;
    for(auto const&kv:value) {
        if(!first) *oi++=comma; else first=false;
        *oi++='"';
        emit(oi,kv.first);
        *oi++='"';
        *oi++=colon;
        emit(oi,kv.second);
    }
    *oi++=obj_end;
}
#define x_emit_map(K,T) template<class OutputIterator> struct emit<OutputIterator, std::map<K,T>> {emit(OutputIterator& oi, std::map<K,T> const& t){emit_map(oi, t);}};

/** 
 * @brief Visitor for emitting a boost::variant.
 * @details
 * Usage:
 * @code
 * typedef boost::variant<type1, type2,...,typeN> mytypes;
 *
 * // Partial template specialization of emit for mytypes:
 * 
 * template<class OutputIterator>
 * struct emit<OutputIterator, mytypes> {
 *     emit(OutputIterator& oi, mytypes const& m) {
 *          boost::apply_visitor(emit_visitor(oi),m);
 *     }
 * }
 * @endcode
 * @tparam OutputIterator
 */
template<class OutputIterator>
class emit_visitor: public boost::static_visitor<> {
    OutputIterator *oi{nullptr};
public:
    explicit emit_visitor(OutputIterator* a_oi): oi(a_oi) {}

    template<class T>
    void operator()(T const& t) const{
        emit<OutputIterator, T>(*oi, t);
    }
};

//-- HERE WE BEGIN PARTIAL SPECIALIZATION FOR VARIOUS BASIS TYPES
//   e.g. for double, int, etc. that are common for all json emitters.

/**
 * @brief emit_type(T,T_)
 * @details
 * This class instantiates a specialization of the struct emit<OutputIterator,T>
 * where the T_ is the generator for the type T.
 * @param T the type to be emitted
 * @param T_ the generator class that takes care of emitting T
 */
#define emit_type(T,T_) template<class OutputIterator> struct emit<OutputIterator,T> {emit(OutputIterator& oi, T const & t) {static T_<OutputIterator> t_; generate(oi,t_,t);}}

/** 
 * @brief decimal policy for json emitter,
 * @details
 * We have so far found that fixed floatingpoint works well for the current frontend frameworks.
 * 
 */
template <class Num>
struct decimal_policy : ka::real_policies<Num> {
    static int floatfield(Num /*n*/) { return ka::real_policies<Num>::fmtflags::fixed; }

    static unsigned precision(Num) {
        return std::numeric_limits<Num>::digits10;
    }
    //ensure to map nan->null
    template <class CharEncoding,class Tag,class OutputIterator>
    static bool nan(OutputIterator&sink,Num,bool) {
        return ka::string_inserter<CharEncoding,Tag>::call(sink,"null");
    }

    template <class CharEncoding,class Tag,class OutputIterator>
    static bool inf(OutputIterator&sink,Num,bool) {
        return ka::string_inserter<CharEncoding,Tag>::call(sink,"null");
    }
};

template<class OutputIterator>
struct emit<OutputIterator, double> {
    emit(OutputIterator& oi, double d) {
        generate(oi, fixed_double_, d);
    }

private:
    ka::real_generator<double, decimal_policy<double>> fixed_double_;
};

template<class OutputIterator>
struct emit<OutputIterator, bool> {
    emit(OutputIterator& oi, bool b) {
        generate(oi, bool_, b);
    }
};

template<class OutputIterator>
struct emit<OutputIterator, int64_t> {
    emit(OutputIterator& oi, int64_t i) {
        generate(oi, long_long, i);
    }
};
x_emit_vec(int64_t);

template<class OutputIterator>
struct emit<OutputIterator, int> {
    emit(OutputIterator& oi, int i){
        generate(oi, int_, i);
    }
};
x_emit_vec(int);

template<class OutputIterator>
struct emit<OutputIterator, std::uint16_t> {
    emit(OutputIterator& oi, std::uint16_t i){
        generate(oi, int_, i);
    }
};
x_emit_vec(std::uint16_t);


template<class OutputIterator>
inline void output_json_char(OutputIterator& oi, const char& c) {
    // Append a single character of a string to an output iterator, handling reserved characters in json.
    switch (c) {
    case '\n': *oi++ = esc; *oi++ = 'n'; return;
    case '\r': *oi++ = esc; *oi++ = 'r'; return;
    case '\t': *oi++ = esc; *oi++ = 't'; return;
    case '\f': *oi++ = esc; *oi++ = 'f'; return;
    case '\b': *oi++ = esc; *oi++ = 'b'; return;
    case quote: case esc: *oi++ = esc;
    }
    *oi++ = c;
}

template<class OutputIterator>
struct emit<OutputIterator, const char*> {
    emit(OutputIterator& oi, const char* t) {
        *oi++ = quote;
        while (t && *t)
            output_json_char(oi, *t++);
        *oi++ = quote;
    }
};

template<class OutputIterator>
struct emit<OutputIterator, char> {
    emit(OutputIterator& oi, const char& t) {
        *oi++ = quote;
        output_json_char(oi, t);
        *oi++ = quote;
    }
};


//template <class OutputIterator> void emit(OutputIterator&oi,std::string const& value) {*oi++=quote;copy(begin(value),end(value),oi);*oi++=quote;}
//TODO: for the string handling, ensure we deal with proper quoting/encoding
template<class OutputIterator>
struct emit<OutputIterator, string_view> {
    static constexpr const char* reserved = "\"\\\n\r\t\f\b"; // Reserved characters in json (see output_json_char)
    emit(OutputIterator& oi, string_view value) {
        *oi++ = quote;
        auto ita = begin(value);
        const auto itend = end(value);
        while (ita != itend) {
            auto itb = std::find_if(ita, itend, [](const auto& c){return strchr(reserved,c)?true:false;});
            copy(ita, itb, oi);
            if (itb != itend) {
                output_json_char(oi, *itb);
                ++itb;
            }
            ita = itb;
        }
        *oi++=quote;
    }
};

template<class OutputIterator>
struct emit<OutputIterator, string> {
    emit(OutputIterator& oi, string const & t) {
        emit<OutputIterator, string_view>(oi, t);
    }
};
x_emit_vec(string);


/** 
 * @brief json object emitter
 *
 * @details
 * This class takes care of emitting a properly formattet
 * json-object, using curly braces, square bracets etc.
 * as needed.
 * It allow a syntax similar to boost::python,
 * e.g.
 * @code {.cpp}
 * emit_object<sink_type> o{sink};
 * o
 * .def("name",my_name) 
 * .def("got_it",yes_got_it)
 * .def_fx("fx_works",[](){return"hello";})
 * ;
 * @endcode
 * 
 * It supports 
 *   std::vector<T>  -> [ t1,..,tn ] (use x_emit_vec(T))
 *   std::map<K,T>   -> { k1:v1,..,kn:vn} 
 * @tparam OutputIterator like back_inserter, or char* etc.  
 */
template <class OutputIterator>
struct emit_object {
    OutputIterator&oi;
    bool first{true};
    explicit emit_object(OutputIterator&oi):oi{oi}{*oi++=obj_begin;}
    inline ~emit_object() {
        *oi++=obj_end;
    }

    inline void sep() {if(first) first=false;else *oi++=comma;}

    /** 
     * @brief emit name-value pair
     * @details
     * note that we have to defer the trivial def of it until _after_
     * the vector-def. is seen, so that compiler succeed resolving
     * emit( vector<T>..)
     *
     * TO CONSIDER: Do better decomposition of the template library tools
     *
     */
    template <class Value>
    emit_object& def(std::string_view name,Value const& value) {
        sep();
        emit(oi,name);
        *oi++ = colon;
        emit<OutputIterator,Value>(oi,value); // value here could be a vector<T>, so with the current approach it must have been seen at the point of template def (no ADL/scope that can help us.).
        return *this;
    }


    template <class Fx>
    emit_object& def_fx(std::string_view name,Fx&& fx) {
        sep();
        emit(oi,name);
        *oi++ = colon;
        fx(oi);
        return *this;
    }

    emit_object(const emit_object&)=delete;
    emit_object(emit_object&&)=delete;
    emit_object & operator=(emit_object const&) =delete;
    emit_object & operator=(emit_object &&) =delete;
};


}
