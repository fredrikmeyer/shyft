#include "targetver.h"
#include <iostream>
#include <string>
#include <boost/beast/version.hpp>
#include <boost/beast/core/string.hpp>
#include <boost/system/error_code.hpp>
#include <shyft/web_api/beast_server.h>

#if BOOST_BEAST_VERSION >= 248
#ifdef BOOST_BEAST_SEPARATE_COMPILATION
#include <boost/beast/src.hpp>
#endif
#include <boost/beast/ssl.hpp>
#include <boost/asio/error.hpp>
namespace net = boost::asio;
#endif

namespace shyft::web_api {
    using boost::system::error_code;
    using boost::beast::string_view;
    using std::string;


    /**  Return a reasonable mime type based on the extension of a file.*/
    string_view
    mime_type(string_view path) {
        using boost::beast::iequals;
        auto const ext = [&path] {
            auto const pos = path.rfind(".");
            if(pos == string_view::npos)
                return string_view{};
            return path.substr(pos);
        }();
        if(iequals(ext, ".htm"))  return "text/html";
        if(iequals(ext, ".html")) return "text/html";
        if(iequals(ext, ".php"))  return "text/html";
        if(iequals(ext, ".css"))  return "text/css";
        if(iequals(ext, ".txt"))  return "text/plain";
        if(iequals(ext, ".js"))   return "application/javascript";
        if(iequals(ext, ".json")) return "application/json";
        if(iequals(ext, ".xml"))  return "application/xml";
        if(iequals(ext, ".swf"))  return "application/x-shockwave-flash";
        if(iequals(ext, ".flv"))  return "video/x-flv";
        if(iequals(ext, ".png"))  return "image/png";
        if(iequals(ext, ".jpe"))  return "image/jpeg";
        if(iequals(ext, ".jpeg")) return "image/jpeg";
        if(iequals(ext, ".jpg"))  return "image/jpeg";
        if(iequals(ext, ".gif"))  return "image/gif";
        if(iequals(ext, ".bmp"))  return "image/bmp";
        if(iequals(ext, ".ico"))  return "image/vnd.microsoft.icon";
        if(iequals(ext, ".tiff")) return "image/tiff";
        if(iequals(ext, ".tif"))  return "image/tiff";
        if(iequals(ext, ".svg"))  return "image/svg+xml";
        if(iequals(ext, ".svgz")) return "image/svg+xml";
        return "application/text";
    }

    /**
    *Append an HTTP rel-path to a local filesystem path.
    * The returned path is normalized for the platform.
    */
    string
    path_cat(
        string_view base,
        string_view path) {
        if(base.empty())
            return path.to_string();
        string result = base.to_string();
    #if BOOST_MSVC
        char constexpr path_separator = '\\';
        if(result.back() == path_separator)
            result.resize(result.size() - 1);
        result.append(path.data(), path.size());
        for(auto& c : result)
            if(c == '/')
                c = path_separator;
    #else
        char constexpr path_separator = '/';
        if(result.back() == path_separator)
            result.resize(result.size() - 1);
        result.append(path.data(), path.size());
    #endif
        return result;
    }

    void
    fail(error_code ec, char const* what) {
        #if BOOST_BEAST_VERSION >= 248
        // ssl::error::stream_truncated, also known as an SSL "short read",
        // indicates the peer closed the connection without performing the
        // required closing handshake (for example, Google does this to
        // improve performance). Generally this can be a security issue,
        // but if your communication protocol is self-terminated (as
        // it is with both HTTP and WebSocket) then you may simply
        // ignore the lack of close_notify.
        //
        // https://github.com/boostorg/beast/issues/38
        //
        // https://security.stackexchange.com/questions/91435/how-to-handle-a-malicious-ssl-tls-shutdown
        //
        // When a short read would cut off the end of an HTTP message,
        // Beast returns the error beast::http::error::partial_message.
        // Therefore, if we see a short read here, it has occurred
        // after the message has been completed, so it is safe to ignore it.

        if(ec == net::ssl::error::stream_truncated)
            return;
        if(ec==net::error::bad_descriptor && what==string("shutdown.https"))
            return;//We get this during shutdown https, it's not clear how we could/should handle it
        #endif
        std::cerr << what << ": ("<<ec.value()<<") " << ec.message() << "\n";
    }

 using std::string;
 using std::shared_ptr;
 using std::make_shared;
 using std::optional;

  int run_web_server(base_request_handler& bg_server, string address_s,unsigned short port,shared_ptr<string const> doc_root,int threads, int bg_threads, bool tls_only) {
        auto const address = boost::asio::ip::make_address(address_s);
        // shorthand for now, using web-api request handler.
        using bg_work=bg_worker<base_request_handler>;
        // The io_context is required for all I/O
        net::io_context ioc{threads};
        net::io_context bg_ioc{bg_threads};
        optional<net::io_context::work> bg_work_lock;
        bg_work_lock.emplace(std::ref(bg_ioc)); // keep alive the bg_work io-service until we zero out the work flag
        bg_work bgw{bg_server,bg_ioc};// bacground worker consist of a server and an ioc that we can post on

        // The SSL context is required, and holds certificates
        ssl::context ctx{ssl::context::tls};
        ctx.set_default_verify_paths();

        // This holds the self-signed certificate used by the server
        load_server_certificate(ctx);

        // Create and launch a listening port
        // it needs to carry all the stuff
        // that needs to passed on to
        // the down-stream classes to handle
        // incoming connections, including the background worker
        make_shared<listener<bg_work>>(
            ioc,
            ctx,
            tcp::endpoint{address, port},
            doc_root,
            bgw,tls_only)->run(); // will hopefully post work to the ioc, that will get alive later when we start the run.

        // Capture SIGINT to perform a clean shutdown
        boost::asio::signal_set signals(ioc, SIGINT);
        signals.async_wait(
            [&](beast::error_code const&, int)
            {
                // Stop the `io_context`. This will cause `run()`
                // to return immediately, eventually destroying the
                // `io_context` and all of the sockets in it.
                ioc.stop();
                bgw.ioc.stop();//ok stopping this loop
            });

        // Run the I/O service on the requested number of threads
        std::vector<std::thread> v;
        v.reserve(threads - 1);
        for(auto i = threads - 1; i > 0; --i)
            v.emplace_back( [&ioc] { ioc.run(); });
        for(auto i = bg_threads; i > 0; --i)
            v.emplace_back( [&bg_ioc] { bg_ioc.run(); });
        bg_server.running=true;
        ioc.run();
        bg_server.running=false;

        // (If we get here, it means we got a SIGXXX )
        bg_work_lock=std::nullopt;// remove the motivation for the bg.io_context to stay alive (but we did actually stop above..)
        // Block until all the threads exit
        for(auto& t : v)
            t.join();

        return 0;
    }
}
