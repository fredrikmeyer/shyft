/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_real.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include <boost/spirit/include/karma.hpp>

#include <string_view>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>
#include <shyft/web_api/dtss_web_api.h>
#include <shyft/core/subscription.h>
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/dtss/geo.h>

namespace  shyft::web_api::grammar {

    using std::vector;
    using std::string;

    namespace qi = boost::spirit::qi;
    namespace phx = boost::phoenix;
    namespace fu = boost::fusion;

    using qi::double_;
    using qi::char_;
    using qi::bool_;
    using qi::int_;
    using qi::_1;
    using qi::_2;
    using qi::_3;
    using qi::_4;
    using qi::_5;
    using qi::_6;
    using qi::_7;
    using qi::_8;

    using qi::_val;
    using qi::lexeme;
    using qi::on_error;
    using qi::fail;
    using phx::val;
    using phx::construct;
    using qi::lit;
    using qi::uint_parser;
    using qi::_a;
    using qi::real_parser;
    using qi::strict_real_policies;

    using core::calendar;
    using core::utctime;
    using core::utcperiod;
    using core::from_seconds;
    using time_series::dd::apoint_ts;
    using ts_points=vector<std::tuple<utctime,double>>;
    using geo_ts_id=dtss::geo::ts_id;
    using dtss::ts_info;

    /** @brief parse a phrase using any grammar or rule.
     * Optional to verify whether the match is on the full phrase or not.
     */
    template <typename P,typename V>
    inline bool parser(char const* input, P const& p,V &v, bool full_match = true){
        using boost::spirit::qi::parse;
        char const* f(input);
        char const* l(f + strlen(f));
        return parse(f, l, p,v) && (!full_match || (f == l));
    }
    
    template <typename P,typename V>
    inline bool phrase_parser(char const* input, P const& p,V &v, bool full_match = true){
        using boost::spirit::qi::phrase_parse;
        using boost::spirit::qi::ascii::space;
        char const* f(input);
        char const* l(f + strlen(f));
        return phrase_parse(f, l, p,space,v) && (!full_match || (f == l));
    }
    /** @brief Commonly used error handler for grammars
    *
    * Error handler, that needs to bound to phx function to be used in qi grammar on_error
    */
    struct error_handler_ {
        template <typename, typename, typename>
        struct result { typedef void type; };

        template <typename Iterator>
        void operator()(qi::info const& what , Iterator err_pos, Iterator last) const ;
    };

    /** @brief commonly used quoted string for json identifiers and strings */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct quoted_string_grammar : public qi::grammar< Iterator, string(),Skipper> {
        quoted_string_grammar() ;
        qi::rule<Iterator, string()> content;
        qi::rule<Iterator, char()> escape_char;
        qi::rule<Iterator, string(),Skipper> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    /** Grammar for utctime, as number or ~iso8601 spec.
     *
     * like 3600.5, or "2018-03-04T05:06:07Z""
     *
     */
    template<typename Iterator>
    struct utctime_grammar:public qi::grammar<Iterator, utctime()> {
        utctime_grammar() ;
        qi::rule<Iterator, utctime()> start;
        uint_parser<unsigned,10,4,4> d4_;
        uint_parser<unsigned,10,2,2> d2_;
        phx::function<error_handler_> const error_handler = error_handler_{};

    };


    /** Grammar for utcperiod, using utctime grammar
     *
     * like ["2018-02-04T06:07:08Z", "2018-02-04T06:07:08Z"] or [10.0,20.0]
     */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct utcperiod_grammar : public qi::grammar< Iterator, utcperiod(),Skipper> {
        utcperiod_grammar() ;
        qi::rule<Iterator, utcperiod(),Skipper> start;
        utctime_grammar<Iterator> t_; // the grammar/rule for specifying time.
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief read ts request grammar
    *
    * Specifies a grammar sufficient to do a read/evaluate request, including
    * caching.
    * In this first version, the list of time-series is ts-urls. Later we could easily
    * make this a list of ts-expressions and then provide similar functionality over
    * the web-api as for the raw socket servers.
    *
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct read_ts_request_grammar : public qi::grammar< Iterator, read_ts_request(),Skipper> {
        read_ts_request_grammar() ;
        qi::rule<Iterator, read_ts_request(),Skipper> start;
        utcperiod_grammar<Iterator,Skipper> p_; // the grammar/rule for specifying time.
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief info request
    *
    * Just a place holder for query the web_api server for information, like
    * version, statistics etc.
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct info_request_grammar : public qi::grammar< Iterator, info_request(),Skipper> {
        info_request_grammar() ;
        qi::rule<Iterator, info_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    
    /** @brief geo_ts_url like  geo://<geo_db>/v/g/e/t  
     */
    template<typename Iterator>
    struct geo_ts_url_grammar:public qi::grammar<Iterator, geo_ts_id()> {
        geo_ts_url_grammar(std::string prefix="geo://");
        qi::rule<Iterator, geo_ts_id()> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
        string const prefix;
    };
    
    /** @brief unsubscribe request
    *
    * Used to stop subscription on read/average/percentile or similar request
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct unsubscribe_request_grammar : public qi::grammar< Iterator, unsubscribe_request(),Skipper> {
        unsubscribe_request_grammar() ;
        qi::rule<Iterator, unsubscribe_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief a grammar for find time-series
    *
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct find_ts_request_grammar : public qi::grammar< Iterator, find_ts_request(),Skipper> {
        find_ts_request_grammar();
        qi::rule<Iterator, find_ts_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    /** @brief nans as symbols
     *
     * Helps us create a grammar where we easily map null to nan,
     * giving the correct attribute type of double.
     * Json does not allow nan, so we have to use other solutions.
     */
    struct nan_symbols_ : qi::symbols<char, double>{
        nan_symbols_(){
            add("null", shyft::nan);
        }
    };

    /** list of ts_points
     *
     * part of grammar dealing with web-client updating a time-series,
     * sending ts-fragments back.
     *
     * One of the forms are +(time,value)
     * other forms could be more formal like
     *  pfx time_axis points,
     * where
     *   pfx bool_ is true for stair-case,
     *   time_axis -> (fixed_dt | calendar_dt | point_dt)
     *   values -> [ value ]
     *
     */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct ts_points_grammar : public qi::grammar< Iterator, ts_points(),Skipper> {
        ts_points_grammar() ;
        qi::rule<Iterator, ts_points(),Skipper> start;
        nan_symbols_ nan_symbol;
        utctime_grammar<Iterator> t_; // the grammar/rule for specifying time.;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief time_points grammar
    *
    * Used for building flexible interval time-axis
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct time_points_grammar:public qi::grammar<Iterator,vector<utctime>(),Skipper> {
        time_points_grammar();
        qi::rule<Iterator,vector<utctime>(),Skipper> start;
        utctime_grammar<Iterator> t_; // the grammar/rule for specifying time.;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief ts_values grammar
    *
    * Used for building list of doubles when creating time-series
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct ts_values_grammar:public qi::grammar<Iterator,vector<double>(),Skipper> {
        ts_values_grammar();
        qi::rule<Iterator, vector<double>(),Skipper> start;
        nan_symbols_ nan_symbol;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief a complete time-axis grammar
    *
    * Supporting fixed,calendar and point time-axis.
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct time_axis_grammar:public qi::grammar<Iterator,shyft::time_axis::generic_dt(),Skipper> {
        time_axis_grammar();
        qi::rule<Iterator,shyft::time_axis::generic_dt(),Skipper> start;
        utctime_grammar<Iterator> t_; // the grammar/rule for specifying time.;
        time_points_grammar<Iterator,Skipper> time_points_; // the grammar/rule for specifying time.;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief a complete point time-series grammar
    *
    * Covers the case of point time-series including point-interpretation(pfx), time-axis,  and
    * corresponding values.
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct apoint_ts_grammar:public qi::grammar<Iterator,shyft::time_series::dd::apoint_ts(),Skipper> {
        apoint_ts_grammar();
        qi::rule<Iterator,shyft::time_series::dd::apoint_ts(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        time_axis_grammar<Iterator,Skipper> time_axis_; // the grammar/rule for specifying time.;
        ts_values_grammar<Iterator,Skipper> values_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief a grammar for a list of time-series */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct ats_vector_grammar:public qi::grammar<Iterator,shyft::time_series::dd::ats_vector(),Skipper> {
        ats_vector_grammar();
        qi::rule<Iterator,shyft::time_series::dd::ats_vector(),Skipper> start;
        apoint_ts_grammar<Iterator,Skipper> ts_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief grammar for average ts request
    *
    * Works in the same way as the read_ts_request, except that
    * as an average over a specified time-axis is added as
    * a post processing step.
    * @see read_ts_request_grammar
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct average_ts_request_grammar : public qi::grammar< Iterator, average_ts_request(),Skipper> {
        average_ts_request_grammar() ;
        qi::rule<Iterator, average_ts_request(),Skipper> start;
        utcperiod_grammar<Iterator,Skipper> p_; // the grammar/rule for specifying time.
        time_axis_grammar<Iterator,Skipper> time_axis_;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief grammar for a percentile ts request
    *
    * It is similar as for the read-ts-request grammar, except that
    * as post-processing, it evaluates the percentiles of the result.
    * Typical use-case when you have a ts-vector that represents n different outcomes
    * and want to provide the percentiles-plot over a suitable resolution in stead of
    * over-plotting the canvas with 10+ more time-series.
    * @see read_ts_request_grammar,average_ts_request_grammar
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct percentile_ts_request_grammar : public qi::grammar< Iterator, percentile_ts_request(),Skipper> {
        percentile_ts_request_grammar() ;
        qi::rule<Iterator, percentile_ts_request(),Skipper> start;
        utcperiod_grammar<Iterator,Skipper> p_; // the grammar/rule for specifying time.
        time_axis_grammar<Iterator,Skipper> time_axis_;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    /** @brief store time-series request
    *
    * A complete grammar for storing time-series as list of fully specified time-series
    * including the ts-urls and point-interpretation,pfx, that allows backend to
    * create new time-series if needed.
    */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct store_ts_request_grammar : public qi::grammar< Iterator, store_ts_request(),Skipper> {
        store_ts_request_grammar();
        qi::rule<Iterator, store_ts_request(),Skipper> start;
        ats_vector_grammar<Iterator,Skipper> tsv_;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };



    /** @brief ts_info grammar */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct ts_info_grammar : public qi::grammar< Iterator, ts_info(),Skipper> {
        ts_info_grammar();
        qi::rule<Iterator, ts_info(),Skipper> start;
        utctime_grammar<Iterator> time_;
        utcperiod_grammar<Iterator> period_;
        quoted_string_grammar<Iterator,Skipper> quoted_string_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief vector<ts_info>, ts_infos,  grammar */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct ts_infos_grammar : public qi::grammar< Iterator, std::vector<ts_info>(),Skipper> {
        ts_infos_grammar();
        qi::rule<Iterator, vector<ts_info>(),Skipper> start;
        ts_info_grammar<Iterator,Skipper> ts_info_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    /** @brief reply_request grammar */
    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct request_reply_grammar : public qi::grammar< Iterator, request_reply(),Skipper> {
        request_reply_grammar();
        qi::rule<Iterator, request_reply(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief find_response grammar */
    template <typename Iterator, typename Skipper=boost::spirit::qi::ascii::space_type>
    struct find_reply_grammar:public qi::grammar<Iterator,find_reply(),Skipper> {
        find_reply_grammar();

        qi::rule<Iterator, find_reply(), Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string_;
        ts_infos_grammar<Iterator,Skipper> ts_infos_;
        phx::function<error_handler_> const error_handler=error_handler_{};
    };

    /** @brief find_response grammar */
    template <typename Iterator, typename Skipper=boost::spirit::qi::ascii::space_type>
    struct tsv_reply_grammar:public qi::grammar<Iterator,tsv_reply(),Skipper> {
        tsv_reply_grammar();

        qi::rule<Iterator, tsv_reply(), Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string_;
        ats_vector_grammar<Iterator,Skipper> tsv_;
        phx::function<error_handler_> const error_handler=error_handler_{};
    };



    // -- queue related grammars

    using namespace  dtss;

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_msg_info_grammar : public qi::grammar< Iterator, queue::msg_info(),Skipper> {
        q_msg_info_grammar();
        qi::rule<Iterator, queue::msg_info(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        utctime_grammar<Iterator> t_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_list_request_grammar : public qi::grammar< Iterator, q_list_request(),Skipper> {
        q_list_request_grammar();
        qi::rule<Iterator, q_list_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_list_response_grammar : public qi::grammar< Iterator, q_list_response(),Skipper> {
        q_list_response_grammar();
        qi::rule<Iterator, q_list_response(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_info_request_grammar : public qi::grammar< Iterator, q_info_request(),Skipper> {
        q_info_request_grammar();
        qi::rule<Iterator, q_info_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_info_response_grammar : public qi::grammar< Iterator, q_info_response(),Skipper> {
        q_info_response_grammar();
        qi::rule<Iterator, q_info_response(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        q_msg_info_grammar<Iterator,Skipper> msg_info_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_infos_request_grammar : public qi::grammar< Iterator, q_infos_request(),Skipper> {
        q_infos_request_grammar();
        qi::rule<Iterator, q_infos_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_infos_response_grammar : public qi::grammar< Iterator, q_infos_response(),Skipper> {
        q_infos_response_grammar();
        qi::rule<Iterator, q_infos_response(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        q_msg_info_grammar<Iterator,Skipper> msg_info_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_size_request_grammar : public qi::grammar< Iterator, q_size_request(),Skipper> {
        q_size_request_grammar();
        qi::rule<Iterator, q_size_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_size_response_grammar : public qi::grammar< Iterator, q_size_response(),Skipper> {
        q_size_response_grammar();
        qi::rule<Iterator, q_size_response(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_maintain_request_grammar : public qi::grammar< Iterator, q_maintain_request(),Skipper> {
        q_maintain_request_grammar();
        qi::rule<Iterator, q_maintain_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_put_request_grammar : public qi::grammar< Iterator, q_put_request(),Skipper> {
        q_put_request_grammar();
        qi::rule<Iterator, q_put_request(),Skipper> start;
        utctime_grammar<Iterator> t_;
        ats_vector_grammar<Iterator,Skipper> tsv_;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_get_request_grammar : public qi::grammar< Iterator, q_get_request(),Skipper> {
        q_get_request_grammar();
        qi::rule<Iterator, q_get_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_done_request_grammar : public qi::grammar< Iterator, q_done_request(),Skipper> {
        q_done_request_grammar();
        qi::rule<Iterator, q_done_request(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_tsv_msg_grammar : public qi::grammar< Iterator, queue::tsv_msg(),Skipper> {
        q_tsv_msg_grammar();
        qi::rule<Iterator, queue::tsv_msg(),Skipper> start;
        q_msg_info_grammar<Iterator,Skipper> info_;
        ats_vector_grammar<Iterator,Skipper> tsv_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct q_get_response_grammar : public qi::grammar< Iterator, q_get_response(),Skipper> {
        q_get_response_grammar();
        qi::rule<Iterator, q_get_response(),Skipper> start;
        quoted_string_grammar<Iterator,Skipper> quoted_string;
        q_tsv_msg_grammar<Iterator,Skipper> q_tsv_msg_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };


    /**
     * @brief The complete web_api_request grammar
     *
     * @details
     * Using boost spirit qi to parse the request into a variant,
     * and then use get<request_type> to do further dispatching,
     * and finally generate the response using similar
     * constructs from boost spirit karma generator.
     */
    using web_request=boost::variant<
        find_ts_request,read_ts_request,info_request,average_ts_request,percentile_ts_request,
        store_ts_request,unsubscribe_request,
        q_list_request,q_info_request,q_infos_request,q_size_request,q_maintain_request,
        q_put_request,q_get_request,q_done_request
        >;

    template<typename Iterator,typename Skipper=boost::spirit::qi::ascii::space_type>
    struct web_request_grammar : public qi::grammar< Iterator, web_request(),Skipper> {
        web_request_grammar() ;

        qi::rule<Iterator, web_request(),Skipper> start;

        // grammar for each of the requests-->
        find_ts_request_grammar<Iterator,Skipper> find_ts_;
        read_ts_request_grammar<Iterator,Skipper> read_ts_;
        average_ts_request_grammar<Iterator,Skipper> average_ts_;
        percentile_ts_request_grammar<Iterator,Skipper> percentile_ts_;
        store_ts_request_grammar<Iterator,Skipper> store_ts_;
        info_request_grammar<Iterator,Skipper> info_;
        unsubscribe_request_grammar<Iterator,Skipper> unsubscribe_;
        // queue:
        q_list_request_grammar<Iterator,Skipper> q_list_;
        q_info_request_grammar<Iterator,Skipper> q_info_;
        q_infos_request_grammar<Iterator,Skipper> q_infos_;
        q_size_request_grammar<Iterator,Skipper> q_size_;
        q_maintain_request_grammar<Iterator,Skipper> q_maintain_;
        q_put_request_grammar<Iterator,Skipper> q_put_;
        q_get_request_grammar<Iterator,Skipper> q_get_;
        q_done_request_grammar<Iterator,Skipper> q_done_;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };

    /** @brief web_reply for the dtss
     * @details
     * Given one of the requests, the dtss will reply with one of the following
     * informal formats.
     */
    using web_reply=boost::variant<
        request_reply,find_reply,tsv_reply,
        q_list_response,q_info_response,q_infos_response,q_size_response,
        q_get_response
        >;

    // and then 'or' grammar for those constructs
    template <typename Iterator, typename Skipper=boost::spirit::qi::ascii::space_type>
    struct web_reply_grammar:qi::grammar<Iterator,web_reply(),Skipper> {
        web_reply_grammar();

        qi::rule<Iterator,web_reply(),Skipper> start;
        request_reply_grammar<Iterator,Skipper> request_reply_;
        find_reply_grammar<Iterator,Skipper> find_;
        tsv_reply_grammar<Iterator,Skipper> tsv_reply_;
        q_list_response_grammar<Iterator,Skipper> q_list_;
        q_info_response_grammar<Iterator,Skipper> q_info_;
        q_infos_response_grammar<Iterator,Skipper> q_infos_;
        q_size_response_grammar<Iterator,Skipper> q_size_;
        q_get_response_grammar<Iterator,Skipper> q_get_;
        phx::function<error_handler_> const error_handler=error_handler_{};
    };


    /** fwd declare the templates we need for request_iterator_t
     *  
     * Current approach is to use a const char* iterator type,
     * but we could also change it to allow us work more directly
     * on the boost beast buffer streams, to save one copy.
     * Currently, we think this is not a performance issue,
     * so we keep it 'simple' for now.
     * 
     * Using extern template ensures that the templates are only
     * expanded once, in their in respective compilation units.
     * 
     * Basically, we tell the c++ compiler that somewhere, there
     * is an instantiation of the template with response_iterator.
     * And then we take care in the grammar.cpp files to ensure that
     * we at least instantiate the templates for the reqest_iterator type.
     * 
     */
    using request_iterator_t= const char*;
    using request_skipper_t= qi::ascii::space_type;
    extern template struct apoint_ts_grammar<request_iterator_t,request_skipper_t>;
    extern template struct ats_vector_grammar<request_iterator_t,request_skipper_t>;
    extern template struct average_ts_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct find_ts_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct info_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct percentile_ts_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct quoted_string_grammar<request_iterator_t,request_skipper_t>;
    extern template struct read_ts_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct store_ts_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct utctime_grammar<request_iterator_t>;
    extern template struct time_axis_grammar<request_iterator_t,request_skipper_t>;
    extern template struct time_points_grammar<request_iterator_t,request_skipper_t>;
    extern template struct ts_points_grammar<request_iterator_t,request_skipper_t>;
    extern template struct ts_values_grammar<request_iterator_t,request_skipper_t>;
    extern template struct utcperiod_grammar<request_iterator_t,request_skipper_t>;
    extern template struct web_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct unsubscribe_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct geo_ts_url_grammar<request_iterator_t>;
    extern template struct ts_info_grammar<request_iterator_t,request_skipper_t>;
    extern template struct ts_infos_grammar<request_iterator_t,request_skipper_t>;
    extern template struct request_reply_grammar<request_iterator_t,request_skipper_t>;
    extern template struct find_reply_grammar<request_iterator_t,request_skipper_t>;
    extern template struct tsv_reply_grammar<request_iterator_t,request_skipper_t>;
    extern template struct web_reply_grammar<request_iterator_t,request_skipper_t>;

    //-- queue related
    extern template struct q_list_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_list_response_grammar<request_iterator_t,request_skipper_t>;

    extern template struct q_info_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_info_response_grammar<request_iterator_t,request_skipper_t>;

    extern template struct q_infos_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_infos_response_grammar<request_iterator_t,request_skipper_t>;

    extern template struct q_size_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_size_response_grammar<request_iterator_t,request_skipper_t>;

    extern template struct q_msg_info_grammar<request_iterator_t,request_skipper_t>;

    extern template struct q_maintain_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_tsv_msg_grammar<request_iterator_t,request_skipper_t>;


    extern template struct q_get_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_get_response_grammar<request_iterator_t,request_skipper_t>;

    extern template struct q_put_request_grammar<request_iterator_t,request_skipper_t>;
    extern template struct q_done_request_grammar<request_iterator_t,request_skipper_t>;

}
