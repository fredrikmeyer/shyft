#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/json_struct.h>

#include <shyft/energy_market/constraints.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>

#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/attribute_types.h>

#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/srv/dstm/context_enums.h>

#include <shyft/mp.h>
#include <shyft/energy_market/a_wrap.h>
#ifdef SHYFT_WITH_SHOP
#include <shyft/web_api/energy_market/stm/shop/generators.h>
#endif
#include <shyft/energy_market/stm/srv/dstm/context_enums.h>

namespace shyft::web_api::energy_market {
    using namespace shyft::web_api::generator;
    namespace mp = shyft::mp;
    namespace hana = boost::hana;

    using shyft::energy_market::stm::srv::dstm::srv_shared_lock;// for read access
    using shyft::energy_market::stm::srv::dstm::srv_unique_lock;// for exclusive write access to models
    using shyft::srv::model_info;
    using shyft::energy_market::stm::stm_hps;
    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::power_plant;
    using shyft::energy_market::stm::waterway;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::unit_group;
    using shyft::energy_market::stm::unit_group_member;
    using shyft::energy_market::stm::transmission_line;
    using shyft::energy_market::stm::busbar;
    using shyft::energy_market::stm::energy_market_area;
    using shyft::energy_market::stm::catchment;
    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::run_parameters;
    using shyft::energy_market::stm::srv::dstm::model_state;

    using shyft::energy_market::core::constraint_base;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using shyft::energy_market::stm::subscription::proxy_attr_observer_;

    using shyft::web_api::grammar::phrase_parser;

    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::gpoint_ts;
    using shyft::time_series::dd::abin_op_ts;
    using shyft::time_series::dd::ts_as;
    using shyft::energy_market::attr_traits::exists;

    map<string, model_info> request_handler::get_model_infos() { return srv->do_get_model_infos(); }

    bg_work_result request_handler::handle_request(const request &req) {
        if (req.keyword == "read_model") {
            return handle_read_model_request(req.request_data);
        } else if (req.keyword == "read_attributes") {
            return handle_read_attribute_request(req.request_data);
        } else if (req.keyword == "get_model_infos") {
            return handle_get_model_infos_request(req.request_data);
        } else if (req.keyword == "get_hydro_components") {
            return handle_get_hydro_components_request(req.request_data);
        } else if (req.keyword == "set_attributes") {
            return handle_set_attribute_request(req.request_data);
        } else if (req.keyword == "unsubscribe") {
            return handle_unsubscribe_request(req.request_data);
        } else if (req.keyword == "fx") {
            return handle_fx_request(req.request_data);
        } else if (req.keyword == "run_params") {
            return handle_run_params_request(req.request_data);
        } else if (req.keyword == "get_log") {
            return handle_get_log_request(req.request_data);
        }else if (req.keyword == "opt_summary") {
            return handle_opt_summary_request(req.request_data);
        } else if (req.keyword == "get_state") {
            return handle_get_state_request(req.request_data);
        }
        return bg_work_result("Unknown keyword: " + req.keyword);
    }

    /** called by the web-server timer serving the web-socket connections at regular intervals */
    bg_work_result request_handler::do_subscription_work(observer_base_ const&o) {
        // We support subscription on proxy-type expressions
        // so ts_expression_observer_ , url, looks different for the in-memory em server
        // instead of 'ts-url', we use request_id (assumed to be unique between clients, so that's a possible problem!),
        // ...a map o->request_id -> request_json, (from the original request)
        // and then we can simply do
        //  handle_read_attribute_request(request_json)
        //
        if(o->recalculate()) { // ok, is there a difference(version-number updated, ref. the last sent)
            auto proxy_observer = std::dynamic_pointer_cast<proxy_attr_observer>(o);
            return proxy_observer->re_emit_response();
        } else {
            return bg_work_result{};
        }
    }

    bg_work_result request_handler::do_the_work(const string &input) {
        // 1: Parse into a request
        // 2: Error handling: On success, do a switch on keyword
        // 3: Handle every case, and emit a response string

        bg_work_result b;
        // 1) Parse the request:
        request arequest;
        shyft::web_api::grammar::request_grammar<const char *> request_;
        bool ok_parse = false;
        bg_work_result response;
        try {
            ok_parse = phrase_parser(input.c_str(), request_, arequest);
            if (ok_parse) {
                response = handle_request(arequest);
            } else {
                response = bg_work_result{string("not understood: ") + input};
            }
        } catch (std::runtime_error const &re) {
            response = bg_work_result{string("request_parse:") + re.what()};
        }
        return response;
    }

    shared_ptr<stm_system_context> request_handler::get_system(const string &mid) {
        return srv->do_get_context(mid);
    }

    
    /** @brief visitor class for dispatching how to read attributes based on type
     * 
     */
    struct read_proxy_handler : public boost::static_visitor<attribute_value_type> {
        struct extract_period: boost::static_visitor<utcperiod>{
            utcperiod def;
            utcperiod operator() (utcperiod const&p) const { return p;}
            utcperiod operator() (vector<int> const&v) const {
                if(v.size()!=2)
                    throw std::runtime_error("parsing read_period failed, needs exact format of [from,to]");

                return utcperiod{from_seconds(v[0]),from_seconds(v[1])};
            }
            template<typename T>
            utcperiod operator() (T const&) const {return def;}
        };

        read_proxy_handler(const json& data, server *const srv, std::function<bg_work_result(json const&)>&& cb): srv{srv} {
            // Set up for subscription:
            auto osub = boost::get_optional_value_or(data.optional<bool>("subscribe"), false);
            use_cache = boost::get_optional_value_or(data.optional<bool>("cache"), true);
            if (osub) {
                json sub_data = data;
                sub_data.m.erase("subscribe");
                subscription = std::make_shared<proxy_attr_observer>(srv, data.required<string>("request_id"), sub_data,
                    std::move(cb));
                subscription->recalculate();//important: since we emit version 0, set initial value of terminals to 0
            }
            
            // Setting up read_type, -period and time_axis;
            read_type = boost::get_optional_value_or(data.optional<string>("read_type"), "read");
            if (read_type == "percentiles")
                percentiles = data.required<vector<int>>("percentiles");
            ta = data.required<generic_dt>("time_axis");
            //read_period = boost::get_optional_value_or(data.optional<utcperiod>("read_period"), ta.total_period());
            auto rp=data.optional("read_period");
            if(rp) {
                extract_period epv;//epv.def=ta.total_period();
                read_period = boost::apply_visitor(epv,*rp);
                if(read_period == epv.def)
                    throw std::runtime_error("dstm: failed to parse read_period attribute from the request");
            } else {
                read_period=ta.total_period();
            }
        }
        
        attribute_value_type operator()(apoint_ts const& ts) {
            apoint_ts nts;
            if (ts.needs_bind()) { // Case 1: The series is unbound:
                if ( !srv || !(srv->dtss) )
                    throw std::runtime_error("Dtss has to be set to read unbound time series.");
                dtss::ts_vector_t tsv;
                tsv.emplace_back(ts.clone_expr());
                tsv = srv->dtss->do_evaluate_ts_vector(read_period, tsv, use_cache, false, read_period);// use_cache as pr. request(default true) if available, no to update cache, because we want readback from dstm
                nts = tsv[0];
            } else {
                nts = ts;
            }
            // TODO: the percentiles, or should we say percentile, ..
            //       could be used, if set, accepting one percentile
            //       combined with time-axis
            if(ta.size()==0) {
                return nts;// just return the time-series as is, no average, or resample
            }
            if(nts.point_interpretation()==time_series::POINT_AVERAGE_VALUE) { // we promise to deliver points to valid for the time-axis
                return nts.average(ta); // stair-case -> use true average, makes a lot of sense
            } else { // linear, point-in-time state-variables, we use resampling to the exact provided time-axis (hmm. maybe plus one timepoint?? or should user supply resample ta?)
                apoint_ts rts(ta,1.0,time_series::POINT_INSTANT_VALUE);
                return nts.use_time_axis_from(rts);
            }

        }

        template<typename V>
        attribute_value_type operator()(shared_ptr<map<utctime,V>> const& tv) {
            // We only read the values that are in the read_period:
            auto res = make_shared<map<utctime, V>>();
            utctime tx=min_utctime; // keeps track of entries <= read_period.start
            V x; // the one entry that was found closest to read_period.start
            for (auto const& kv : *tv) {
                if (kv.first>read_period.start && kv.first< read_period.end){
                    res->insert(res->end(),kv);//within the period, add it.
                } else if(kv.first <= read_period.start) { //add entry just at start, or first before start
                    if(kv.first > tx) { // better candidate found
                        tx=kv.first;// record the time of the candidate
                        x=kv.second;// and the value, to be inserted when we are done with the loop.
                    }
                }
            }
            if(tx != min_utctime) {
                (*res)[tx]=x;//insert the most recent entry before the read period
            }
            return res;
        }

        template<typename V>
        attribute_value_type operator()(V const& v) {
            return v;

        }
        //attribute_value_type operator()(uint16_t v) {
        //    return int(v);
        //}
        
        proxy_attr_observer_ subscription = nullptr;
        string read_type;
        vector<int> percentiles;
        generic_dt ta;
        utcperiod read_period;
        server *const srv;
        bool use_cache{true};

    };


    template<class T>
    vector<json> get_proxy_attributes(
        T const & t,
        vector<string> const& attr_ids,
        read_proxy_handler& rph)
    {
        // Set up result:
        vector<json> attr_vals;
        vector<bool> found(attr_ids.size(),false);// keep track of which attributes we actually found so we an report error
        size_t count=0;// count number we find, so we can return quickly
        auto has_id_x=[&attr_ids,&found,&count](const char*id) {
            for(auto i=0u;i<attr_ids.size();++i) {
                if(!strcmp(attr_ids[i].c_str(),id)) {
                    found[i]=true; // mark it as found
                    ++count;
                    return true;// and return
                }
            }
            return false;// not found/wanted.
        };
        // Iterate over each attribute, except ts map
        auto constexpr attribute_paths = mp::leaf_accessors(hana::type_c<T>);
        hana::for_each(
            attribute_paths, // The sequence
            [&](auto m) {
                // 0. Check if current id is in the specified list:
                if (has_id_x(mp::leaf_accessor_id_str(m))) {
                    json attr_struct;
                    attr_struct["attribute_id"] = std::string(mp::leaf_accessor_id_str(m));
                    auto val = mp::leaf_access(t,m);
                    if (exists(val)) {
                        attr_struct["data"] = rph(mp::leaf_access(t,m));
                    } else {
                        attr_struct["data"] = string("not found");
                    }
                    if (rph.subscription) {
                        rph.subscription->add_subscription(t, m);
                    }
                    attr_vals.emplace_back(attr_struct);
                }
            }
        );
        // iterate over the .tsm[].. attributes, unless, we already did find all we searched for
        if(count!=attr_ids.size() ) { // we lack some, let's check the .tsm[].. if they are there.
            if constexpr (!std::is_same<unit_group_member, T>::value) { // only if T has .tsm[]
                //std::cout<<"search for missing members "<<t.id<<",name="<<t.name<< std::endl;
                for(auto i=0u;!t.tsm.empty() && i<found.size();++i) {
                    if(!found[i]  ) {
                        //std::cout<<" search for "<< attr_ids[i]<<std::endl;
                        if(attr_ids[i].rfind("ts.",0)==0 ) {
                            //std::cout<<" ts.xxx "<< attr_ids[i].substr(3)<<std::endl;
                            
                            auto f=t.tsm.find(attr_ids[i].substr(3));
                            if( f!= t.tsm.end()) {
                                found[i]=true;++count;
                                //std::cout<<" ts.xxx "<< attr_ids[i].substr(3)<<".. found"<<std::endl;

                                json attr_struct;
                                attr_struct["attribute_id"] = attr_ids[i];
                                apoint_ts val=f->second;
                                if(exists(val)) {
                                    attr_struct["data"] = rph(val);
                                } else {
                                    attr_struct["data"] = string("not found");
                                }
                                if (rph.subscription) {
                                    rph.subscription->add_ts_map_subscription(t,attr_ids[i]);
                                }
                                attr_vals.emplace_back(attr_struct);
                            } 
                        }
                    }
                }
            }
        }
        
        if(count!=attr_ids.size()) { // we got a request that ask for not existent attributes.
            for(auto i=0u;i<found.size();++i) {
                if (!found[i]) {
                    json r;
                    r["attribute_id"] = attr_ids[i];// just pretend we have it
                    r["data"] = string("attribute not found");//
                    attr_vals.emplace_back(r);
                }
            }
        }
        return attr_vals;
    }


    /** @brief Reads a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @tparam Fx a callable (int cid, shared_ptr<Tc> const&el)->bool if el->id matches id
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     * @param rph: instance of handler, used in innermost loop to handle the different value types.
     * @param fx: the callable that compares component-id, cid, to the 'id' of the Tc.'id' (usually ->id)
     * @return
     */
    template<class T, class Tc, class Fx>
    vector<json> get_attribute_value_table_fx(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, read_proxy_handler& rph,Fx&&fx){
        vector<json> result;
        // Get out component_ids and attribute_ids:
        for(auto const &cdata:data) {
            auto cid = cdata.required<int>("component_id");
            auto attr_ids = cdata.required<vector<string>>("attribute_ids");
            shared_ptr<T> comp;
            // For each attribute container:
            {
                json comp_struct;
                comp_struct["component_id"] = cid;
                // Find container in vector:
                auto it = find_if(vecvals.begin(), vecvals.end(), [&cid,&fx](auto const& el) {
                    return fx(cid,el);//->id == cid;
                });
                if (it == vecvals.end()) {
                    comp_struct["component_data"] = string("Unable to find component");
                } else {
                    comp = dynamic_pointer_cast<T>(*it);
                    comp_struct["component_data"] = get_proxy_attributes(*comp,attr_ids, rph);
                }
                result.push_back(comp_struct);
            }
        }
        return result;
    }

    /** ref to get_attribute_value_table_fx, this is with a prefabricated fx that compares the id of objects to cid */
    template<class T, class Tc>
    vector<json> get_attribute_value_table(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, read_proxy_handler& rph){
        return get_attribute_value_table_fx<T,Tc>(vecvals,data,rph,[](int cid,shared_ptr<Tc> const& c){return c->id==cid;});
    }

    

    /** @brief generate a response for a read_attributes request */
    bg_work_result request_handler::handle_read_attribute_request(const json &data) {
        // Get model and HPS:
        // we need to have a shared-lock on the model in order to read the model.
        auto mid = data.required<string>("model_key");
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// grab shared lock here
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        
        // We use a read_proxy_handler (visitor) to hold subscription and read data,
        // and pass that along to the innermost loop over components and attributes
        read_proxy_handler vis(data, srv,
            [this](json const& data) {
                return handle_read_attribute_request(data);
            }
        );
        
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        size_t n_sections=0;// count that the request do contain a section.
        using hana::type_c;
        //---- EMIT DATA: ----//
        {
            emit_object<decltype(sink)> oo(sink);
            //tools for oo emitters,
            auto oo_def=[&n_sections,&vis](auto &oo,json const&data,std::string const& name,auto const& dv, auto tp) {
                using d_type= typename decltype(tp)::type;
                auto j_data=data.template optional<vector<json>>(name); // check if there is a json section with the given name
                if(j_data) {// if yes, then
                    ++n_sections;//just count so we keep track
                    oo.def(name,get_attribute_value_table<d_type>(dv,*j_data,vis)); // does all needed magic rendering json
                }
            };
            // convinience: auto deduce type for  vectors in the stm namespace,
            auto oo_defx=[&oo_def](auto &oo,json const&data,std::string const& name,auto const& dv) {
                using d_type= typename std::remove_cvref_t<decltype(dv)>::value_type::element_type;// flip out the type T from vector<shared_ptr<T>>
                oo_def(oo,data,name,dv,type_c<d_type>);
            };

            oo.def("model_key", mid);
            auto hps_data=data.optional<vector<json>>("hps");

            if(hps_data && (*hps_data).size()) {
                n_sections++;
                oo.def_fx("hps",[&vis,&mdl,mid,&hps_data,&oo_def](auto sink) {
                    emit_vector_fx(sink,*hps_data,[&vis,&mdl,mid,&oo_def](auto oi, json const&hpsd) {
                        auto hps_id = hpsd.required<int>("hps_id");
                        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),[&hps_id](auto ihps) { return ihps->id == hps_id; });
                        if (it == mdl->hps.end()) {
                            throw runtime_error( string("Unable to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
                        }
                        auto hps = *it;
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("hps_id", hps->id);
                        oo_def(oo,hpsd,"reservoirs"  ,hps->reservoirs,type_c<reservoir>);
                        oo_def(oo,hpsd,"units"       ,hps->units,type_c<unit>);
                        oo_def(oo,hpsd,"power_plants",hps->power_plants,type_c<power_plant>);
                        oo_def(oo,hpsd,"waterways"   ,hps->waterways,type_c<waterway>);
                        auto gts=hps->gates();// important.. gates are return by value, and get_attr.. takes const ref.
                        oo_def(oo,hpsd,"gates"       ,gts,type_c<gate>);
                        oo_def(oo,hpsd,"catchments"  ,hps->catchments,type_c<catchment>);
                    });
                });
            }

            oo_defx(oo,data,"markets",mdl->market);
            oo_defx(oo,data,"contracts",mdl->contracts);
            oo_defx(oo,data,"contract_portfolios",mdl->contract_portfolios);
            oo_defx(oo,data,"power_modules",mdl->power_modules);

            auto network_data=data.optional<vector<json>>("networks"); // check if there is a json section with the given name
            if (network_data && (*network_data).size()) {
                ++n_sections;//just count so we keep track
                oo.def_fx("networks",[&vis,&mdl,mid,&network_data](auto sink) {
                    emit_vector_fx(sink,*network_data,[&vis,&mdl,mid](auto oi, json const& networkd) {
                        auto network_id = networkd.required<int>("component_id");
                        auto it = std::find_if(mdl->networks.begin(), mdl->networks.end(),[&network_id](auto inet) { return inet->id == network_id; });
                        if (it == mdl->networks.end()) {
                            throw runtime_error( string("Unable to find Network ") + std::to_string(network_id) + string(" in model '") + mid + "'");
                        }
                        auto network = *it;
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("component_id", network->id);
                        if (auto tl_attrs = networkd.optional<vector<json>>("transmission_lines"); tl_attrs) {
                            oo.def("transmission_lines", get_attribute_value_table_fx<transmission_line>(network->transmission_lines, *tl_attrs, vis,
                                                                            [](int tid,shared_ptr<transmission_line> const&tl){return tid==tl->id;}));
                        }
                        if (auto b_attrs = networkd.optional<vector<json>>("busbars"); b_attrs) {
                            oo.def("busbars", get_attribute_value_table_fx<busbar>(network->busbars, *b_attrs, vis,
                                                                            [](int bid,shared_ptr<busbar> const&b){return bid==b->id;}));
                        }
                    });
                });
            }

            auto ug_data=data.optional<vector<json>>("unit_groups");
            if(ug_data && (*ug_data).size()) {
                n_sections++;
                oo.def_fx("unit_groups",[&vis,&mdl,mid,&ug_data](auto sink) {
                    emit_vector_fx(sink,*ug_data,[&vis,&mdl,mid](auto oi, json const&ugd) {
                        auto ug_id = ugd.required<int>("component_id");
                        auto it = std::find_if(mdl->unit_groups.begin(), mdl->unit_groups.end(),[&ug_id](auto iug) { return iug->id == ug_id; });
                        if (it == mdl->unit_groups.end()) {
                            throw runtime_error( string("Unable to find unit_groups ") + std::to_string(ug_id) + string(" in model '") + mid + "'");
                        }
                        auto ug = *it;
                        
                        // first emit unit-group attribute (if any)
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("component_id", ug->id);// ensure to identify the unit group regardless.
                        auto attr_ids = ugd.optional<vector<string>>("attribute_ids");
                        if( attr_ids ) {//any local attribute, like obligation.schedule etc.
                            oo.def("component_data",get_proxy_attributes(*ug,*attr_ids, vis));
                        }                        
                        // then, while in this unit-group, also check if there are member attributes requested: members 
                        auto comp_attrs = ugd.optional<vector<json>>("members");// from members we can retrieve is_active, maybe later some more..
                        if (comp_attrs) oo.def("members",// notice that we use group-member->unit->id as member id
                            get_attribute_value_table_fx<unit_group_member>(ug->members, *comp_attrs, vis,
                                                                            [](int cid,shared_ptr<unit_group_member> const&ugm){return cid==ugm->unit->id;}));
                    });
                });
            }
            if(n_sections==0) {
                throw runtime_error( string("Currently we require the hps or markets sections in the request: request_id= ") +  req_id);
            }
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    }

    /** @brief Helper function for disambiguating which array to put
     * subscription in, based on value type
     * @tparam T1
     * @tparam Ts
     * @return
     */
    template<class T1, class... Ts>
    constexpr bool is_one_of() noexcept {
        return (std::is_same_v<T1, Ts> || ...);
    }


    /** @brief handler class for setting attributes on a model.
     *
     */
    class set_attribute_handler {
            // Attributes:
            server* const srv; ///<Pointer to the server we need access to to handle time series correctly
            map<string, vector<string>> subs;///<Keep track of what attributes have been changed
            bool merge; ///<Whether to merge values by the "shotgun method", i.e. keep all values that don't directly overlap.
            bool recreate;///<Remove the old value entirely
            bool cache_on_write;///< if true(default), also cache shyft-time-series.
            string mkey;///<Model key that will need to be prefixed to all subscription updates.
        public:
            set_attribute_handler(server* const srv, bool merge=false, bool recreate=false, const string& mid="", bool cache_on_write=true)
                : srv{srv}, merge{merge}, recreate{recreate},cache_on_write{cache_on_write}, mkey{mid}
            {
                subs["time-series"] = {};
                subs["other"] = {};
            }

            /** @brief Need a override for attribute_value_type-variant, to safely unpack:
             *
             */
            template<class V>
            string apply(V& old_val, attribute_value_type const& new_val) {
                auto f = [this,&old_val](auto arg) { return this->apply(old_val, arg); };
                return boost::apply_visitor(f, new_val);
            }

            // Apply new_val to old_val based on the state of this, i.e. the values of merge, recreate and the server
            // to communicate with.
            template<class V>
            string apply(V& old_val, V const& new_val) {
                 // Update with new values:
                string res;
                if (exists(old_val)) {
                    res = merge_values(old_val, new_val);
                } else {
                    old_val = new_val;
                    res = "OK";
                }
                return res;
            }

            template<class V, class U>
            string apply(V& /*old_val*/, U const& /*new_val*/) {
                return "type mismatch";
            }


            // Notify accrued changes to the subscription managers:
            void notify_changes() {
                // Propagate/notify changes due to write of attributes (e.g. time-series)
                if (srv->sm) srv->sm->notify_change(subs["other"]);
                srv->dtss->sm->notify_change(subs["time_series"]);// notify the dtss sm about time-series changes
            }

            template <class T>
            string make_subscription_url(T const&t, std::string_view attr_id) const {
                string sub_id ="dstm://M" + mkey;
                sub_id.reserve(30);
                auto rbi = std::back_inserter(sub_id);
                t.generate_url(rbi);
                *rbi++ = '.';
                sub_id+=attr_id;
                return sub_id;
            }

            template<class T>
            void add_tsm_subscription(T const&t,std::string_view attr_id) {
                subs["time_series"].push_back(make_subscription_url(t,attr_id));
            }

            template <class Struct, class LeafAccessor>
            void add_sub_id(Struct const& t, LeafAccessor&& la) {
                auto sub_id=make_subscription_url(t, mp::leaf_accessor_id_str(la));
                // Get value type of leaf_accessor:
                //auto value_type = mp::leaf_accessor_type(la);
                using V = typename decltype(+mp::leaf_accessor_type(std::declval<LeafAccessor>()))::type;
                // Send to correct subscription manager based on type:
                if constexpr (is_one_of<V, apoint_ts, absolute_constraint, penalty_constraint>()) {
                    subs["time_series"].push_back(sub_id);
                } else {
                    subs["other"].push_back(sub_id);
                }
            }

    private:
        string merge_values(uint16_t &old_v, const  uint16_t new_v) {
            old_v=new_v;// just assign it.
            return "OK";
        }
        string merge_values(apoint_ts& old_val, const apoint_ts& new_val) {
            if (ts_as<gpoint_ts>(old_val.ts)) { // Case 1: Treated as a local variable
                if (!recreate || merge) {
                    old_val.merge_points(new_val);
                } else
                    old_val = new_val;
            } else if (auto sts = ts_as<aref_ts>(old_val.ts)) { // Case 2: Reference to a time series
                // Case 2.0: In the case that the underlying time-series is unbounded:
                if (sts->needs_bind() && !(sts->id.rfind("dstm://", 0) == 0)) {
                    if (srv && srv->dtss) {
                        dtss::ts_vector_t tsv;
                        tsv.push_back(apoint_ts(old_val.id(), new_val));
                        if (merge) srv->dtss->do_merge_store_ts(tsv, cache_on_write);
                        else srv->dtss->do_store_ts(tsv, recreate, cache_on_write);
                        // NO need to update subs here. They are incremented in the do_store_ts function
                        return "stored to dtss";
                    } else {
                        return "Cannot set dtss time series without dtss.";
                    }
                } else {
                    if (!recreate || merge) {
                        old_val.merge_points(new_val);
                    } else {
                        old_val = new_val;
                    }
                }
            } else { // Case 3: The time series is an expression and should not be set
                return "Time series is an expression. Cannot be set.";
            }
            return "OK";
        }

        string merge_values(absolute_constraint& old_val, const absolute_constraint& new_val) {
            merge_values(old_val.limit, new_val.limit);
            merge_values(old_val.flag, old_val.flag);
            return "OK";
        }

        string merge_values(penalty_constraint& old_val, const penalty_constraint& new_val) {
            merge_values(old_val.limit, new_val.limit);
            merge_values(old_val.flag, new_val.flag);
            merge_values(old_val.cost, new_val.cost);
            merge_values(old_val.penalty, new_val.penalty);
            return "OK";
        }


        template <class T>
        string merge_values(shared_ptr<map<utctime, shared_ptr<T>>>& old_val, const shared_ptr<map<utctime, shared_ptr<T>>>& new_val) {
            // We want to overwrite, so insert doesn't do the trick:
            if (!recreate || merge) {
                for (auto const &kv : *new_val) {
                    (*old_val)[kv.first] = kv.second;
                }
            } else {
                old_val = new_val;
            }
            return "OK";
        }

        template <class T>
        string merge_values(T& old_val, T const & new_val) {
            old_val = new_val;
            return "NOT OK";
        }
    };

    auto get_attr_struct(vector<json> const& attr_vals, string const& attr_id) {
        return std::find_if(attr_vals.begin(), attr_vals.end(),
            [&attr_id](json const& element) -> bool {
                // Here we check if the json structure e contains the key "attribute_id",
                // and if so, if it compares equal to the provided attr_id.
                auto opt = element.optional<string>("attribute_id"); // Get compiler error: "invalid use of 'class std::optional<std::__cxx11::basic_string<char>>" if not cast
                if (opt) {
                    return *opt == attr_id;
                } else {
                    return false;
                }
            }
        );
    }
    /** @brief
     *
     * @tparam T : Type whose attributes we want to change. Assumed to be a hana-struct
     * @param t : The instance of type T whose attribute values we want to change
     * @param attr_vals : The values to set or merge in t's attributes. Each entry of the vector requires the
     *   following:
     *      * "attribute_id" : <string> (ID of the attribute to change)
     *      * "value" : The value to be inserted/merged in.
     * @return a list of json. Each entry is the attribute_id that has been processed together with a "status",
     *  stating whether the change was successful or if it  encountered an error.
     */
    template<class T>
    vector<json> set_attribute_values(T& t, vector<json> const& attr_vals, set_attribute_handler& handler) {
        // Initialize result:
        vector<json> result;
        result.reserve(attr_vals.size());

        // Go through each attribute:
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        hana::for_each(attr_paths,
            [&t, &attr_vals, &result, &handler](auto m) {
                auto attr_id = string(mp::leaf_accessor_id_str(m));
                auto it = get_attr_struct(attr_vals, attr_id);
                if (it != attr_vals.end()) { // If we found the current attribute amongst the ones to change
                    // Initialize structure to be inserted into result
                    json attr_res;
                    attr_res["attribute_id"] = attr_id;
                    // Get out value to be inserted/merged:
                    auto value = it->required("value"); // value is now of type value_type -- a boost::variant
                    // Set value
                    auto f = [&handler, &t, &m](auto arg) {
                        auto& attr = mp::leaf_access(t,m);
                        return handler.apply(attr, arg);
                    };
                    // Alternatively, we could have used hana::partial like:
                    // auto f = hana::partial([&handler](auto& attr, auto arg) { return handler.apply(attr, arg); }, mp::leaf_access(t,m));
                    attr_res["status"] = boost::apply_visitor(f, value);
                    // Add subscription ID to notify change later:
                    handler.add_sub_id(t, m);
                    // Insert into result vector:
                    result.push_back(attr_res);
                }
            }
        );
        if constexpr (!std::is_same_v<T,shyft::energy_market::stm::unit_group_member>) {
            for(auto const& a:attr_vals) {
                auto attr_id=a.required<string>("attribute_id");
                if(attr_id.rfind("ts.",0)==0) {
                    auto value=a.required<attribute_value_type>("value");
                    json attr_res;
                    attr_res["attribute_id"] = attr_id;
                    auto tsi= t.tsm.find(attr_id.substr(3));
                    if(tsi==t.tsm.end()) {
                        attr_res["status"]="not found";
                    } else {
                        attr_res["status"] = handler.apply(tsi->second,value);
                        handler.add_tsm_subscription(t,attr_id);// could work.
                    }
                    result.push_back(attr_res);
                } // else alraedy handled
            }
        }
        return result;
    }
    
    struct check_empty_visitor : public boost::static_visitor<bool> {
        template<typename T>
        bool operator()(const vector<T>& t) const {
            return t.size() == 0;
        }
        
        template<typename T>
        bool operator()(const T& ) const {
            return true;
        }
    };
    /** @brief Sets a list of proxy attributes from a list of T specified by data.
     *
     * @tparam T container type for proxy attributes (reservoir, unit, waterway &c.)
     * @tparam Tc container type of base type (So we can do dynamic casting)
     *      This template parameters is required in this solution due to invariation,
     *      i.e. even if T extends Tc, vector<T> DOES NOT extend vector<Tc>
     * @tparam Fx a callable(int,ptr_type const&)->bool used to locate the wanted component
     * 
     * @param srv ref to server so that we can notify on subscription (server.sm subscription manager)
     * @param vecvals the vector with a series of references to instances of T.
     * @param data: JSON struct specifying which components, and which attributes to retrieve
     *      Requires keys:
     *          @key component_ids: vector<int> of IDs of which containers to retrieve from.
     *          @key attribute_ids: vector<int> of IDs of which attributes to retrieve for each container.
     *          @key values: vector<attribute_value_type> of values to set for each component and attribute
     *              Must have length equal to |component_ids| * |attribute_ids|.
     * @param handler: the handler we use to record/repeat the request (incase subscribe)
     * @param fx: The callable(int cid,ptr_type const&c)->bool, that should return true if cid matches id of comp
     * @return vector with json
     */
    template<class T, class Tc,class Fx>
    vector<json> set_attribute_values_vector_fx(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, set_attribute_handler& handler, Fx&& fx){
        vector<json> result;

        shared_ptr<T> comp;
        // Iterate over each element of the json-list:
        for (auto &comp_data : data) {
            // Each element of the vector is required to have:
            // int component_id
            // vector<json> attribute_data
            auto cid = comp_data.required<int>("component_id");
            auto temp = comp_data.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
            vector<json> attr_vals;
            if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                attr_vals = comp_data.required<vector<json>>("attribute_data");
            }
            // Initialize json to store results for current component:
            json comp_struct;
            comp_struct["component_id"] = cid;
            // Get out component:
            auto it = find_if(vecvals.begin(), vecvals.end(), [cid,&fx](auto e) { return fx(cid,e); });
            if (it != vecvals.end()) {
                // We need to downcast the component because of inheritance.
                if ( (comp = dynamic_pointer_cast<T>(*it)) ) {
                    comp_struct["status"] = set_attribute_values(*comp, attr_vals, handler); // set_attribute_values(comp, attr_vals....)
                } else {
                    comp_struct["status"] = "Unable to cast hydro component";
                }
            } else {
                comp_struct["status"] = "Unable to find component";
            }
            // Add component's json to result list:
            result.push_back(comp_struct);
        }
        return result;
    }

    /** ref set_attribute_values_vector_fx, this one with a ready-shipped fx that compares to c->id */
    template<class T, class Tc>
    vector<json> set_attribute_values_vector(vector<shared_ptr<Tc>> const& vecvals, vector<json> const& data, set_attribute_handler& handler){
        return set_attribute_values_vector_fx<T,Tc>(vecvals,data,handler,[](int cid,shared_ptr<Tc>const&c){return cid==c->id;});
    }    
        
    bg_work_result request_handler::handle_set_attribute_request(const json &data) {
        // Get model and HPS:
        auto mid = data.required<string>("model_key");
        auto ctx = get_system(mid);
        srv_unique_lock ul(ctx->mtx);// grab unique lock for modification here
        auto mdl=ctx->mdl;//for bw compat code below, and we have a mutable stm_system

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");

        auto merge = boost::get_optional_value_or(data.optional<bool>("merge"), false);
        auto recreate = boost::get_optional_value_or(data.optional<bool>("recreate"), false);
        auto cache_on_write=boost::get_optional_value_or(data.optional<bool>("cache_on_write"), true);
        // Set up set_attribute_handler;
        set_attribute_handler handler(srv, merge, recreate, mid,cache_on_write);

        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        {

            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid);
            auto n_sections=0u;
            // some tools to auto roll out the types:
            using hana::type_c;
            /**
             * oo_def checks if the json data have the specified key,
             * then if it is there, get the specs from it, and apply them to
             * the corresponding energy market model object.
             */
            auto oo_def=[&n_sections,&handler](auto &oo,json const&data, std::string const& name,auto const& dv, auto tp) {
                using d_type= typename decltype(tp)::type;//std::remove_cvref_t<decltype(dv)>::value_type::element_type;
                auto j_data=data.template optional<vector<json>>(name); // check if there is a json section with the given name
                if(j_data && (*j_data).size()) {// if yes, then
                    ++n_sections;//just count so we keep track
                    oo.def(name,set_attribute_values_vector<d_type>(dv,*j_data,handler)); // does all needed magic rendering json
                }
            };
            /// short hand when the type to process is the same as vector type (e.g.stm only)
            auto oo_defx=[&oo_def](auto & oo, auto const&data, std::string const& name,auto const& dv) {
                using d_type=typename std::remove_cvref_t<decltype(dv)>::value_type::element_type;// pull out the type from vector<shared_ptr<T>>
                return oo_def(oo,data,name,dv,type_c<d_type>);
            };

            auto hps_data=data.optional<vector<json>>("hps");
            if(hps_data && (*hps_data).size()) {
                n_sections++;
                oo.def_fx("hps",[&mdl,mid,&hps_data,&oo_def](auto sink) {
                    emit_vector_fx(sink,*hps_data,[&mdl,mid,&oo_def](auto oi,json const&hpsd) {
                        auto hps_id = hpsd.required<int>("hps_id");
                        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(), [&hps_id](auto ihps) { return ihps->id == hps_id; });
                        if (it == mdl->hps.end()) {
                            throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
                        }
                        auto hps = *it;
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("hps_id", hps->id);
                        oo_def(oo,hpsd,"reservoirs"  , hps->reservoirs,type_c<reservoir>);
                        oo_def(oo,hpsd,"units"       , hps->units,type_c<unit>);
                        oo_def(oo,hpsd,"power_plants", hps->power_plants,type_c<power_plant>);
                        oo_def(oo,hpsd,"waterways"   , hps->waterways,type_c<waterway>);
                        auto gts=hps->gates();// needed as a instance here, inline in next line will fail(would require by value, move, etc.
                        oo_def(oo,hpsd,"gates"       , gts,type_c<gate>);
                        oo_def(oo,hpsd,"catchments", hps->catchments, type_c<catchment>);
                    });
                });
            }


            oo_defx(oo,data,"markets",mdl->market);
            oo_defx(oo,data,"contracts",mdl->contracts);
            oo_defx(oo,data,"contract_portfolios",mdl->contract_portfolios);
            oo_defx(oo,data,"power_modules",mdl->power_modules);

            auto network_data=data.optional<vector<json>>("networks"); // check if there is a json section with the given name
            if (network_data && (*network_data).size()) {
                ++n_sections;//just count so we keep track
                oo.def_fx("networks",[&handler,&mdl,mid,&network_data](auto sink) {
                    emit_vector_fx(sink,*network_data,[&handler,&mdl,mid](auto oi, json const& networkd) {
                        auto network_id = networkd.required<int>("component_id");
                        auto it = std::find_if(mdl->networks.begin(), mdl->networks.end(),[&network_id](auto inet) { return inet->id == network_id; });
                        if (it == mdl->networks.end()) {
                            throw runtime_error( string("Unable to find Network ") + std::to_string(network_id) + string(" in model '") + mid + "'");
                        }
                        auto network = *it;
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("component_id", network->id);
                        if (auto tl_attrs = networkd.optional<vector<json>>("transmission_lines"); tl_attrs) {
                            oo.def("transmission_lines", set_attribute_values_vector_fx<transmission_line>(network->transmission_lines, *tl_attrs, handler,
                                                                            [](int tid,shared_ptr<transmission_line> const&tl){return tid==tl->id;}));
                        }
                        if (auto b_attrs = networkd.optional<vector<json>>("busbars"); b_attrs) {
                            oo.def("busbars", set_attribute_values_vector_fx<busbar>(network->busbars, *b_attrs, handler,
                                                                            [](int bid,shared_ptr<busbar> const&b){return bid==b->id;}));
                        }
                    });
                });
            }

            auto ug_data=data.optional<vector<json>>("unit_groups");
            if(ug_data && (*ug_data).size()) {
                n_sections++;
                oo.def_fx("unit_groups",[&handler,&mdl,mid,&ug_data](auto sink) {
                    emit_vector_fx(sink,*ug_data,[&handler,&mdl,mid](auto oi,json const& ugd) {
                        auto ug_id = ugd.required<int>("component_id");
                        emit_object<decltype(oi)> oo(oi);
                        oo.def("component_id", ug_id); // we can tell which component it is,

                        auto it = std::find_if(mdl->unit_groups.begin(), mdl->unit_groups.end(), [&ug_id](auto iug) { return iug->id == ug_id; });
                        if (it == mdl->unit_groups.end()) {
                            oo.def("status","Unable to find component");// and we did not find it, just a message, no throw
                        } else {
                            auto ug = *it;
                            // set unit_group attributes here
                            auto temp = ugd.required("attribute_data"); // This pattern is required because an empty list would not parse successfully to vector<json>
                            vector<json> attr_vals;
                            if (!boost::apply_visitor(check_empty_visitor(), temp)) {
                                attr_vals = ugd.required<vector<json>>("attribute_data");
                            }
                            oo.def("status",set_attribute_values(*ug, attr_vals, handler)); // set_attribute_values(comp, attr_vals....)
                            // Set unit-group member attributes(is_active, and we could consider unit attribute propagation) 
                            auto comp_attrs = ugd.optional<vector<json>>("members");
                            if (comp_attrs) oo.def("members",
                                set_attribute_values_vector_fx<unit_group_member>(
                                    ug->members, *comp_attrs, handler,
                                    [](int cid,shared_ptr<unit_group_member> const&ugm){return ugm->unit->id==cid;}
                                )
                            );
                        }
                    });
                });
            }
            
            
            if(n_sections==0) {
                throw runtime_error( string("Currently we require the hps or market attribute: request_id= ") +  req_id);
            }
        }
        handler.notify_changes();
        //---- RETURN RESPONSE: ------//
        response += "}";
        return bg_work_result{response};
    }

    template<class OutputIterator,class V>
    void emit_system_elements(OutputIterator &oi, V const& elements) {
        emit_vector_fx(oi, elements, [](OutputIterator &oi, auto el) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", el->id)
                .def("name", el->name);
        });
    }

    /** @brief handle requests of the form:
     * 
     * @param data: {
     *          "model_key": <string>
     *      } 
     * @return 
     */
    bg_work_result request_handler::handle_read_model_request(const json &data) {
        // Get model:
        auto mid = data.required<string>("model_key");
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// grab a shared lock while fiddling model with output
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
        // Get request id:
        auto req_id = data.required<string>("request_id");
        std::string response = string("{\"request_id\":\"") + req_id + string("\",\"result\":");
        auto sink = std::back_inserter(response);
        // Emit bare-bones model structure:
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
                .def("id", mdl->id)
                .def("name", mdl->name)
                .def("json", mdl->json)
                .def_fx("hps", [&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->hps);})
                .def_fx("unit_groups", [&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->unit_groups);})
                .def_fx("markets", [&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->market);})
                .def_fx("contracts",[&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->contracts);})
                .def_fx("contract_portfolios",[&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->contract_portfolios);})
                .def_fx("networks",[&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->networks);})
                .def_fx("power_modules",[&mdl](decltype(sink) oi) {emit_system_elements(oi, mdl->power_modules);})
                ;
        }
        response += "}";
        return bg_work_result{response};
    }

    bg_work_result request_handler::handle_get_model_infos_request(const json &data) {
        // Communicate with server
        auto model_infos = get_model_infos();

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        emit_vector_fx(sink, model_infos, [](auto oi, auto mi) {
            emit_object<decltype(oi)> oo(oi);
            oo.def("model_key", mi.first)
                .def("id", mi.second.id)
                .def("name", mi.second.name);
        });
        response += "}";
        return bg_work_result{response};
    }


    /** @brief Get out a list of IDs for which attributes have values attached.
     *
     * @tparam T : Hana-struct type, e.g. reservoir, powerplant &c.
     * @param t  : instance to check which attributes are set for.
     * @return : vector of attribute-ID's in string format.
     */
    template<class T>
    vector<string> available_attributes(T const &t) {
        // Accessor functions for every attribute in the struct T (recursive, so gives attributes of nested structs as well)
        auto constexpr attr_paths = mp::leaf_accessors(hana::type_c<T>);

        vector<string> attr_ids{};
        // Iterate over every attribute:
        hana::for_each(attr_paths, // The sequence
            [&attr_ids, &t](auto m) {
                // m is here a pair (attr_id, attr_accessor_function);
                if (exists(mp::leaf_access(t, m))) {
                    attr_ids.push_back(string(mp::leaf_accessor_id_str(m)));
                }
            }
        );
        return attr_ids;
    }

    template<class OutputIterator>
    void emit_power_plant_skeleton(OutputIterator &oi, const power_plant &pp, bool get_data = false) {
        emit_object <OutputIterator> oo(oi);
        oo.def("id", pp.id)
            .def("name", pp.name)
            .def_fx("units", [&pp](auto oi) {
                *oi++ = arr_begin;
                for (auto it = pp.units.begin(); it != pp.units.end(); ++it) {
                    if (it != pp.units.begin()) *oi++ = comma;
                    emit(oi, (*it)->id);
                }
                *oi++ = arr_end;
            });
        if (get_data) oo.def("set_attrs", available_attributes(pp));
    }

    template<class OutputIterator>
    void emit_waterway_skeleton(OutputIterator &oi, const waterway &wr, bool get_data = false) {
        emit_object <OutputIterator> oo(oi);
        oo.def("id", wr.id)
            .def("name", wr.name)
            .def("upstreams", wr.upstreams)
            .def("downstreams", wr.downstreams);
        if (get_data) oo.def("set_attrs", available_attributes(wr));
    }


    template<class OutputIterator>
    void emit_hps_reservoirs(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.reservoirs, [&get_data](auto oi, auto res_) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", res_->id)
                .def("name", res_->name);
            if (get_data) {
                oo.def("set_attrs", available_attributes(*dynamic_pointer_cast<reservoir>(res_)));
            }
        });
    }

    template<class OutputIterator>
    void emit_hps_units(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.units, [&get_data](auto oi, auto unit_) {
            emit_object <OutputIterator> oo(oi);
            oo.def("id", unit_->id)
                .def("name", unit_->name);
            if (get_data) {
                oo.def("set_attrs", available_attributes(*dynamic_pointer_cast<unit>(unit_)));
            }
        });
    }

    template<class OutputIterator>
    void emit_hps_power_plants(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.power_plants, [&get_data](auto oi, auto pp_) {
            emit_power_plant_skeleton(oi, *dynamic_pointer_cast<power_plant>(pp_), get_data);
        });
    }

    template<class OutputIterator>
    void emit_hps_waterways(OutputIterator &oi, const stm_hps &hps, bool get_data = false) {
        emit_vector_fx(oi, hps.waterways, [&get_data](auto oi, auto wr_) {
            emit_waterway_skeleton(oi, *dynamic_pointer_cast<waterway>(wr_), get_data);
        });
    }

    bg_work_result request_handler::handle_get_hydro_components_request(const json &data) {
        // Get model ID and HPS ID:
        auto mid = data.required<string>("model_key");
        auto hps_id = data.required<int>("hps_id");

        // Find if whether to find available data as well:
        auto avail_data_kwarg = data.optional("available_data");
        bool avail_data = false;
        if (avail_data_kwarg) {
            avail_data = boost::get<bool>(*avail_data_kwarg);
        }
        // Get model and search for requested HPS in model:
        auto ctx = get_system(mid);
        srv_shared_lock sl(ctx->mtx);// shared-lock because we are reading.
        auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
        auto it = std::find_if(mdl->hps.begin(), mdl->hps.end(),
                               [&hps_id](auto ihps) { return ihps->id == hps_id; });
        if (it == mdl->hps.end()) {
            throw runtime_error( string("Uanble to find HPS ") + std::to_string(hps_id) + string(" in model '") + mid + "'");
        }
        auto hps = *it;

        // Prepare response:
        auto req_id = data.required<string>("request_id");
        std::string response = string(R"_({"request_id":")_") + req_id + string(R"_(","result":)_");
        auto sink = std::back_inserter(response);
        //---- EMIT DATA: ----//
        { // Has to be in scope so destructor is called appropriately.
            emit_object<decltype(sink)> oo(sink);
            oo.def("model_key", mid)
                .def("hps_id", hps->id)
                .def_fx("reservoirs", [&hps, &avail_data](auto oi) {
                    emit_hps_reservoirs(oi, *hps, avail_data);
                })
                .def_fx("units", [&hps, &avail_data](auto oi) {
                    emit_hps_units(oi, *hps, avail_data);
                })
                .def_fx("power_plants", [&hps, &avail_data](auto oi) {
                    emit_hps_power_plants(oi, *hps, avail_data);
                })
                .def_fx("waterways", [&hps, &avail_data](auto oi) {
                    emit_hps_waterways(oi, *hps, avail_data);
                });
        }
        //---- RETURN RESPONSE: ------//
        response += "}";
        return bg_work_result{response};
    }
    
    bg_work_result request_handler::handle_unsubscribe_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto unsub_id = data.required<string>("subscription_id");
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("subscription_id", unsub_id)
                .def("diagnostics", string{});
        }
        
        return bg_work_result{response, unsub_id};
    }
    /** @brief handle fx(mid,fx_arg) request
     *
     * In the first approach, just call the server callback, 
     * doing no claim to the model/nor server.
     * Assume the callback will do proper claim on shared resources
     * when needed.
     */
    bg_work_result request_handler::handle_fx_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        auto fx_arg = data.required<string>("fx_arg");
        auto success= srv->fx_cb ? srv->fx_cb(model_key,fx_arg):false;
        
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("diagnostics", string{(success? "":"Failed")});
        }        
        return bg_work_result{response};
    }

    template<class T>
    vector<json> get_attribute_values(T const& t, read_proxy_handler& rph) {
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        vector<json> result{};
        result.reserve(mp::leaf_accessor_count(hana::type_c<T>));
        hana::for_each(
            attr_paths,
            [&result, &t, &rph](auto m) {
                json attr_struct;
                attr_struct["attribute_id"] = string(mp::leaf_accessor_id_str(m));
                attr_struct["data"] = mp::leaf_access(t,m);
                if (rph.subscription) {
                    rph.subscription->add_subscription(t, m);
                }
                result.push_back(attr_struct);
            }
        );

        return result;
    }
    /** @brief handle run_params request
     *
     */
    bg_work_result request_handler::handle_run_params_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        auto ctx = get_system(model_key);
        srv_shared_lock sl(ctx->mtx);
        // Generate result:
        json ndata = data;
        ndata["time_axis"] = generic_dt(); // Time_axis is required
        read_proxy_handler vis(ndata, srv, [this](json const& data) { return handle_run_params_request(data); });
        json result;
        result["model_key"] = model_key;
        result["values"] = get_attribute_values((ctx->mdl->run_params), vis);
        // Generate response:
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("result", result);
        }
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    }
    bg_work_result request_handler::handle_opt_summary_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");
        auto ctx = get_system(model_key);
        srv_shared_lock sl(ctx->mtx);
        // Generate result:
        json ndata = data;
        ndata["time_axis"] = generic_dt(); // Time_axis is required
        read_proxy_handler vis(ndata, srv, [this](json const& data) { return handle_opt_summary_request(data); });
        json result;
        result["model_key"] = model_key;
        result["values"] = get_attribute_values(*(ctx->mdl->summary), vis);
        // Generate response:
        std::string response = "";
        auto sink = std::back_inserter(response);
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("request_id", req_id)
                .def("result", result);
        }
        if (vis.subscription)
            return bg_work_result{response, std::move(vis.subscription)};
        else
            return bg_work_result{response};
    }

    /** @brief handle get_log request */
    bg_work_result request_handler::handle_get_log_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");

        // Generate response
        std::string response = R"_({"request_id":")_" + req_id + R"_(","result":)_";
        auto sink = std::back_inserter(response);
#ifdef SHYFT_WITH_SHOP
        auto log = srv->do_get_log(model_key); // Needs to be in guards because shop_log_entry isn't exposed otherwise.
        shop::shop_log_entry_generator<decltype(sink)> msg_;
        ka::rule<decltype(sink), vector<shyft::energy_market::stm::shop::shop_log_entry>()> g = ka::lit('[') << -(msg_ % ',') << ka::lit(']');
#else
        std::string log = "\"Shyft is not built with SHOP, and therefore unable to get shop log\"";
        auto g = ka::string;
#endif
        {
            generate(sink, g, log);
        }
        *sink++ = '}';
        return bg_work_result{response};
    }
    namespace {
        static string convert_model_state_to_string(const model_state& state) {
            switch (state) {
                case model_state::idle:
                    return "idle";
                case model_state::setup:
                    return "setup";
                case model_state::running:
                    return "running";
                case model_state::finished:
                    return "finished";
                case model_state::failed:
                    return "failed";
            };
            return "unknown";
        }
    }
    bg_work_result request_handler::handle_get_state_request(const json& data) {
        auto req_id = data.required<string>("request_id");
        auto model_key = data.required<string>("model_key");

        auto state = srv->do_get_state(model_key);

        // Prepare response:
        std::string response = string("{\"request_id\":\"") + req_id + string("\",\"result\":");
        auto sink = std::back_inserter(response);
        // Emit bare-bones model structure:
        {
            emit_object<decltype(sink)> oo(sink);
            oo.def("state", convert_model_state_to_string(state));
        }
        response += "}";
        return bg_work_result{ response };
    }
}
