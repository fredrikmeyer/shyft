/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <map>
#include <iosfwd>

#include <shyft/srv/model_info.h>
#include <shyft/energy_market/stm/attribute_types.h>

#include <shyft/web_api/bg_work_result.h> // For bg_work_result


namespace shyft::energy_market::stm {
    struct stm_system;//fwd
    namespace srv::dstm {
        struct server;//fwd here
        struct stm_system_context;
    }
}

namespace shyft::web_api::energy_market {
    using std::vector;
    using std::string;
    using std::map;
    
    template<typename T>
    inline bool is_equal(vector<T> const& v1, vector<T> const& v2){
        if (v1.size() != v2.size()) return false;
        for (const auto& el1 : v1) {
            if (std::none_of(begin(v2), end(v2), [&el1](const auto el2)->bool {
                return el1 == el2;
            })) return false;
        }
        return true;
    }


    using shyft::energy_market::stm::srv::dstm::server;
    using shyft::web_api::bg_work_result;
    using shyft::core::subscription::observer_base_;

    using shyft::energy_market::stm::stm_system;
    using shyft::energy_market::stm::srv::dstm::stm_system_context;// that have the required lock mechanism
    using shyft::srv::model_info;
    using std::shared_ptr;
    using std::dynamic_pointer_cast;
    using std::string;
    using std::vector;

    struct json;//fwd decl
    struct request;//fwd decl

    // responses

    // background service that
    // have a shared_ptr<stm_server>
    // the boost beast foreground io-service receives ws messages
    // posts those to the bg thread for processing
    // the bg worker does following:
    // parses the request using boost.spirit.qi
    // forward the request to the server
    // using boost.spirit.karma, generate the json-like response
    // emit the result to the output buffer
    // forward it to the boost beast io-services
    struct request_handler:base_request_handler {
        server* srv{nullptr};
        //std::atomic_bool running;///< true when starting to serve, false before and after serving
        /** @brief callback function to be used in beast server.
         * Parses request into a request struct.
         * On successful parse passes it on to handler function
         * and passes on response to server.
         * On failure passes on error message as string.
         * @param input string to parse and process.
         * @return bg_work_result for server to handle.
         **/
         bg_work_result do_the_work(const string& input) override;
         bg_work_result do_subscription_work(observer_base_ const&o) override;

        /** @brief main dispatch for messages
         *
         * @param req with keyword and json struct of request data
         * @return response string
         **/
        bg_work_result handle_request(const request& req);
    private:
        // Various specialized functions for handling each specific request type:
        // The dispatch is done in handle_request
        bg_work_result handle_read_attribute_request(const json& data);
        bg_work_result handle_read_model_request(const json& data);
        bg_work_result handle_get_model_infos_request(const json& data);
        bg_work_result handle_get_hydro_components_request(const json& data);
        bg_work_result handle_get_attribute_mapping_request(const json& data);
        bg_work_result handle_set_attribute_request(const json& data);
        bg_work_result handle_unsubscribe_request(const json& data);
        bg_work_result handle_fx_request(const json& data);
        bg_work_result handle_run_params_request(const json& data);
        bg_work_result handle_opt_summary_request(const json& data);
        bg_work_result handle_get_log_request(const json& data);
        bg_work_result handle_get_state_request(const json& data);

        // Various helper functions for communicating with the server.
        // Easier if interface to backend changes.
        shared_ptr<stm_system_context> get_system(const string& mid);
        map<string, model_info> get_model_infos();

    };
}
