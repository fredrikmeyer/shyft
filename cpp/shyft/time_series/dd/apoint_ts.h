/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once

#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/convolve_policy.h>
#include <shyft/time_series/ice_packing_policy.h>
#include <shyft/time_series/rating_curve_parameters.h>
#include <shyft/time_series/ice_packing_parameters.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
using namespace shyft::core;
using std::shared_ptr;
using std::vector;
using std::string;
using std::runtime_error;
using std::size_t;
using std::move;
using std::forward;

struct average_ts;  // fwd api
struct accumulate_ts;  // fwd api
struct integral_ts;  // fwd api
struct time_shift_ts;  // fwd api
struct aglacier_melt_ts;  // fwd api
struct aref_ts;  // fwd api
struct ts_bind_info;  // fwd
struct ats_vector;  // fwd
struct abs_ts;  // fwd
struct ice_packing_recession_parameters;  // fwd
struct inside_parameter; // fwd
struct bit_decoder;//fwd
struct derivative_ts; // fwd api
struct use_time_axis_from_ts; // fwd api
struct qac_parameter;
struct transform_spline_ts;
struct spline_parameter;
struct xy_point_curve;
enum interpolation_scheme : uint8_t;


/** @brief  apoint_ts, a value-type conceptual ts.
*
*  This is the class that we expose to python, with operations, expressions etc.
*  and that we build all the exposed semantics on.
*  It holds a shared_ptr to some ipoint_ts, that could be a concrete point timeseries or
*  an expression.
*
*/
struct apoint_ts {
    /** a ref to the real implementation, could be a concrete point ts, or an expression */
    ipoint_ts_ref ts;// consider unique pointer instead,possibly public, to ease transparency in python

    typedef gta_t ta_t;///< this is the generic time-axis type for apoint_ts, needed by timeseries namespace templates
    friend struct average_ts;
    friend struct integral_ts;
    friend struct time_shift_ts;
    friend struct accumulate_ts;
    friend struct aglacier_melt_ts;
    friend struct abs_ts;
    friend struct abin_op_ts;
    // constructors that we want to expose
    // like

    // these are for the python exposure
    apoint_ts(const time_axis::fixed_dt& ta,double fill_value,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    apoint_ts(const time_axis::fixed_dt& ta,const vector<double>& values,ts_point_fx point_fx=POINT_INSTANT_VALUE);

    apoint_ts(const time_axis::point_dt& ta,double fill_value,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    apoint_ts(const time_axis::point_dt& ta,const vector<double>& values,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    explicit apoint_ts(const rts_t & rts);// ct for result-ts at cell-level that we want to wrap.
    apoint_ts(const vector<double>& pattern, utctimespan dt, time_axis::generic_dt const& ta);
    apoint_ts(const vector<double>& pattern, utctimespan dt, utctime pattern_t0,const time_axis::generic_dt& ta);
    // these are the one we need.
    apoint_ts(const gta_t& ta,double fill_value,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    apoint_ts(const gta_t& ta,const vector<double>& values,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    apoint_ts(const gta_t& ta,vector<double>&& values,ts_point_fx point_fx=POINT_INSTANT_VALUE);

    apoint_ts(gta_t&& ta,vector<double>&& values,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    apoint_ts(gta_t&& ta,double fill_value,ts_point_fx point_fx=POINT_INSTANT_VALUE);
    explicit apoint_ts(const shared_ptr<ipoint_ts>& c):ts(c) {}
    explicit apoint_ts(ipoint_ts_ref const& c):ts(c) {}
    explicit apoint_ts(ipoint_ts_ref && c):ts(std::move(c)) {}
    explicit apoint_ts(string ref_ts_id);
    apoint_ts(string ref_ts_id, const apoint_ts& x);
    // some more exotic stuff like average_ts

    apoint_ts()=default;
    bool needs_bind() const {
        if(!ts)
            return false;// empty ts, counts as terminal, can't be bound
        return ts->needs_bind();
    }
    string id() const;///< if the ts is aref_ts return it's id (or maybe better url?)
    void set_id(string const&);///< if ts is aref_ts and not bound, allow changing the id
    void do_bind() {
        if(ts) dref(ts).do_bind();
    }
    void do_unbind() {
        if(ts) dref(ts).do_unbind();
    }
    ipoint_ts_ref const& sts() const {
        if(!ts)
            throw runtime_error("TimeSeries is empty");
        if(ts->needs_bind())
            throw runtime_error("TimeSeries, or expression unbound, please bind sym-ts before use.");
        return ts;
    }
    ipoint_ts_  sts() {
        if(!ts)
            throw runtime_error("TimeSeries is empty");
        if(ts->needs_bind())
            throw runtime_error("TimeSeries, or expression unbound, please bind sym-ts before use.");
        return wref(ts);
    }

    /** support operator! bool  to let an empty ts evaluate to */
    bool operator !() const { // can't expose it as op, due to math promotion
        if(ts && needs_bind())
            throw runtime_error("TimeSeries, or expression unbound, please bind sym-ts before use.");
        return !(  ts && ts->size() > 0);
    }
    /**@brief Easy to compare for equality, but tricky if performance needed */
    bool operator==(const apoint_ts& other) const;
    bool operator!=(const apoint_ts& other) const { return !operator==(other);}
    // interface we want to expose
    // the standard ipoint-ts stuff:
    ts_point_fx point_interpretation() const {return sts()->point_interpretation();}
    void set_point_interpretation(ts_point_fx point_interpretation) { sts()->set_point_interpretation(point_interpretation); };
    const gta_t& time_axis() const { return sts()->time_axis();};
    utcperiod total_period() const {return ts&& !ts->needs_bind()?ts->total_period():utcperiod();};   ///< Returns period that covers points, given
    size_t index_of(utctime t) const {return ts&&!ts->needs_bind()?ts->index_of(t):string::npos;};
    size_t index_of(utctime t,size_t ix_hint) const { return ts&&!ts->needs_bind() ? ts->time_axis().index_of(t,ix_hint) : string::npos; };
    size_t open_range_index_of(utctime t, size_t ix_hint = string::npos) const {
        return ts && !ts->needs_bind() ? ts->time_axis().open_range_index_of(t, ix_hint):string::npos; }
    size_t size() const {return ts?sts()->size():0;};        ///< number of points that descr. y=f(t) on t ::= period
    utctime time(size_t i) const {return sts()->time(i);};///< get the i'th time point
    double value(size_t i) const {return sts()->value(i);};///< get the i'th value
    double operator()(utctime t) const  {return sts()->value_at(t);}; // maybe nan for empty ts?
    vector<double> values() const;

    //-- then some useful functions/properties
    apoint_ts extend( const apoint_ts & ts,
        extend_ts_split_policy split_policy, extend_ts_fill_policy fill_policy,
        utctime split_at, double fill_value ) const;
    apoint_ts average(const gta_t& ta) const;
    apoint_ts integral(gta_t const &ta) const;
    apoint_ts derivative(derivative_method dm=derivative_method::default_diff) const;
    apoint_ts accumulate(const gta_t& ta) const;
    apoint_ts time_shift(utctimespan dt) const;
    apoint_ts pow(double a) const;
    apoint_ts pow(const apoint_ts& other) const;
    apoint_ts max(double a) const;
    apoint_ts min(double a) const;
    apoint_ts max(const apoint_ts& other) const;
    apoint_ts min(const apoint_ts& other) const;
    apoint_ts log() const;
    apoint_ts statistics(const gta_t ta,int64_t p) const;
    static apoint_ts max(const apoint_ts& a, const apoint_ts& b);
    static apoint_ts min(const apoint_ts& a, const apoint_ts& b);
    static apoint_ts pow(const apoint_ts& a, const apoint_ts& b);
    static apoint_ts log(const apoint_ts& a);
    ats_vector stack_ts(const calendar& cal, utctime t, size_t n_dt, utctimespan dt, size_t n_partitions, utctime common_t0, utctimespan dt_snap) const;
    apoint_ts convolve_w(const vector<double>& w, shyft::time_series::convolve_policy conv_policy) const;
    apoint_ts abs() const;
    apoint_ts rating_curve(const rating_curve_parameters & rc_param) const;
    apoint_ts ice_packing(const ice_packing_parameters & ip_param, ice_packing_temperature_policy ipt_policy) const;
    apoint_ts ice_packing_recession(const apoint_ts & ice_packing_ts, const ice_packing_recession_parameters & ipr_param) const;
    apoint_ts bucket_to_hourly(int start_hour_utc,double empty_bucket_event_limit) const;
    apoint_ts krls_interpolation(utctimespan dt, double rbf_gamma, double tol, size_t size) const;

    apoint_ts min_max_check_linear_fill(double min_x,double max_x,utctimespan max_dt) const;
    apoint_ts min_max_check_ts_fill(double min_x,double max_x,utctimespan max_dt,const apoint_ts& cts) const;
    apoint_ts min_max_check_linear_fill(double min_x,double max_x,int64_t max_dt) const{return min_max_check_linear_fill(min_x,max_x,seconds(max_dt));};
    apoint_ts min_max_check_ts_fill(double min_x,double max_x,int64_t max_dt,const apoint_ts& cts) const {return min_max_check_ts_fill(min_x,max_x,seconds(max_dt),cts);};
    apoint_ts quality_and_self_correction(qac_parameter const&p) const;
    apoint_ts quality_and_ts_correction(qac_parameter const&p,const apoint_ts& cts) const;

    apoint_ts inside(double min_v,double max_v,double nan_v,double inside_v,double outside_v) const;
    apoint_ts transform_spline(const spline_parameter& p) const;
    apoint_ts transform_spline(const std::vector<double>& knots, const std::vector<double>& coeff, std::size_t degree) const;
    apoint_ts transform(const xy_point_curve& xy, interpolation_scheme scheme) const;
    apoint_ts decode(int start_bit,int n_bits) const;

    apoint_ts slice(int i0, int n) const;

    apoint_ts merge_points(const apoint_ts& o);
    apoint_ts use_time_axis_from(const apoint_ts&o) const;
    apoint_ts repeat(gta_t const&rta) const;
    apoint_ts compress(double accuracy) const;
    size_t    compress_size(double accuracy ) const;
    //-- in case the underlying ipoint_ts is a gpoint_ts (concrete points)
    //   we would like these to be working (exception if it's not possible,i.e. an expression)
    point get(size_t i) const {return point(time(i),value(i));}
    void set(size_t i, double x) ;
    void fill(double x) ;
    void scale_by(double x) ;
    string stringify() const;
    /** given that this ts is a bind-able ts (aref_ts)
    * and that bts is a gpoint_ts, make
    * a *copy* of gpoint_ts and use it as representation
    * for the values of this ts
    * @parameter bts time-series of type point that will be applied to this ts.
    * @throw runtime_error if any of preconditions is not true.
    */
    void bind(const apoint_ts& bts);

    /** recursive search through the expression that this ts represents,
    *  and return a list of bind_ts_info that can be used to
    *  inspect and possibly 'bind' to values @ref bind.
    * @return a vector of ts_bind_info
    */
    vector<ts_bind_info> find_ts_bind_info() const;

    /** evaluate the expression, into a concrete ts, require a bounded ts */
    apoint_ts evaluate() const;

    apoint_ts clone_expr() const {
        if(needs_bind())
            return apoint_ts{ts->clone_expr()};
        return *this;
    }

    string serialize() const;
    static apoint_ts deserialize(const string&ss);
    vector<char> serialize_to_bytes() const;
    static apoint_ts deserialize_from_bytes(const vector<char>&ss);
    x_serialize_decl();
};


/** @brief clip a concrete time-series to period
*
* Rely on the apoint_ts.slice(i,n) algo.
* Which in effect *will* to a memcpy of the slice (values, potentially also the time-axis points).
* -> cost = memcpy the slice of ts.
*
* The usage context for this function is currently *only* at the dtss final stage before
* serializing values back to the client. User might want to set a clip period to remove
* excessive amount of data for non-expressional request
* where time-axis usually determines precicely what should be returned.
*
* @note Not valid for expression time-series (yet).
*       It will throw at the first point where it dive into slice function.
*       When .slice() supports expressions (not useful for now), - this will also work.
*
*
*/
apoint_ts clip_to_period(apoint_ts const& ts, utcperiod p) ;

    // add operators and functions to the apoint_ts class, of all variants that we want to expose
apoint_ts average(const apoint_ts& ts,const gta_t& ta/*fx-type */) ;
apoint_ts average(apoint_ts&& ts,const gta_t& ta) ;

apoint_ts integral(const apoint_ts& ts, const gta_t& ta/*fx-type */);
apoint_ts integral(apoint_ts&& ts, const gta_t& ta);

apoint_ts accumulate(const apoint_ts& ts, const gta_t& ta/*fx-type */);
apoint_ts accumulate(apoint_ts&& ts, const gta_t& ta);

apoint_ts create_glacier_melt_ts_m3s(const apoint_ts & temp,const apoint_ts& sca_m2,double glacier_area_m2,double dtf);

double nash_sutcliffe(const apoint_ts& observation_ts, const apoint_ts& model_ts, const gta_t &ta);

double kling_gupta(const apoint_ts& observation_ts, const apoint_ts&  model_ts, const gta_t& ta, double s_r, double s_a, double s_b);

apoint_ts create_periodic_pattern_ts(const vector<double>& pattern, utctimespan dt,utctime t0, const gta_t& ta);

apoint_ts operator+(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts operator+(const apoint_ts& lhs,double           rhs) ;
apoint_ts operator+(double           lhs,const apoint_ts& rhs) ;

apoint_ts operator-(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts operator-(const apoint_ts& lhs,double           rhs) ;
apoint_ts operator-(double           lhs,const apoint_ts& rhs) ;
apoint_ts operator-(const apoint_ts& rhs) ;

apoint_ts operator/(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts operator/(const apoint_ts& lhs,double           rhs) ;
apoint_ts operator/(double           lhs,const apoint_ts& rhs) ;

apoint_ts operator*(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts operator*(const apoint_ts& lhs,double           rhs) ;
apoint_ts operator*(double           lhs,const apoint_ts& rhs) ;


apoint_ts max(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts max(const apoint_ts& lhs,double           rhs) ;
apoint_ts max(double           lhs,const apoint_ts& rhs) ;

apoint_ts min(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts min(const apoint_ts& lhs,double           rhs) ;
apoint_ts min(double           lhs,const apoint_ts& rhs) ;

apoint_ts pow(const apoint_ts& lhs,const apoint_ts& rhs) ;
apoint_ts pow(const apoint_ts& lhs,double           rhs) ;
apoint_ts pow(double           lhs,const apoint_ts& rhs) ;

apoint_ts log(const apoint_ts& lhs);


///< time_shift i.e. same ts values, but time-axis is time-axis + dt
apoint_ts time_shift(const apoint_ts &ts, utctimespan dt);

apoint_ts extend(
    const apoint_ts & lhs_ts, const apoint_ts & rhs_ts,
    extend_ts_split_policy split_policy, extend_ts_fill_policy fill_policy,
    utctime split_at, double fill_value );


/** @brief ts_bind_info gives information about the timeseries and it's binding
* 
* It represented by encoded string reference
* Given that you have a concrete ts,
* you can bind that the bind_info.ts
* using bind_info.ts.bind().
* Used by the dtss to find time-series in an expression tree
* and then to resolve the symbols, by reading/providing a concrete 
* time-series to put in its place.
*/
struct ts_bind_info {
    ts_bind_info(const string& id, const apoint_ts&ts) :reference(id), ts(ts) {}
    ts_bind_info() =default;
    bool operator==(const ts_bind_info& o) const { return reference == o.reference; }
    string reference;
    apoint_ts ts;
};
}

namespace shyft::time_series {
    inline size_t hint_based_search(const dd::apoint_ts& source, const utcperiod& p, size_t i) {
        return source.index_of(p.start, i);
    }
}
x_serialize_export_key_nt(shyft::time_series::dd::apoint_ts);
