/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <cmath>
#include <dlib/statistics.h>
#include <memory>
#include <shyft/time_series/time_series_dd.h>
#include <shyft/time_series/fx_merge.h>
#include <shyft/time_series/fx_quantile_mapping.h>
#include <shyft/time_series/ts_point_merge.h>
#include <shyft/time_series/fx_average.h>
#include <shyft/time_series/stack_ts.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <shyft/time_series/bin_op_eval.h>
#include <shyft/time_series/goal_function.h>
#include <shyft/time_series/ts_compress.h>

namespace shyft::time_series {

/*
    Some comments:
    The api-type ts, time-axis gives some challenges when it comes to performance on large amount of data.
    The computation tends to be memory bound, - and the .values() approach kind of does not help in that matter.
    On the other side, a zillion virtual-dispatches down to the terminal-values (when evaluating expressions)
    also have higher cost (tested), although one could argue that in an ideal case, it should be more core-bound
    doing just the math on fewer memory-refs.

    As a common approach here, we say that if the set of time-series do have common time-axis (same-resolution/range),
    we try to perform the operations as fast as vector vs vector operations.

    Tests comparing with armadillo, indicates that we succeeds with this approach.

    The system is still fast, when time-axis are not aligned, but it takes the penalty of aligning the accesses (which is needed!).
*/

/** ts_src template helps using the average_value/accumulate value template functions
*
*  During the evaluation of apoint_ts, .average /.integral functions, the ts at hand
*  can be anything from a complex expression, up to a terminal-value (a concrete point ts).
*  To avoid making a copy of terminal-value ts, we check if the argument is a terminal-value type of ts
*  and instead of copy, just reference the terminal-ts values (if the time-axis are aligned).
*  This saves us a copy of the terminal-value ts (which is significant part of computation),
*/
template<class TA>
struct ts_src {
    const TA& ta;
    const std::vector<double>& v;
    ts_src(const TA& ta,const std::vector<double>&v):ta{ta},v{v} {}
    size_t index_of(const utctime t) const {return ta.index_of(t);}
    utctime time(size_t i) const {return ta.time(i);}
    utcperiod total_period() const {return ta.total_period();}
    point get(size_t i ) const {return point{ta.time(i),v[i]};}
    size_t size() const {return v.size();}
};
/* specialization of the hint-based searches,
    * SiH: really questionable if we should use open_range_index here.
    */
template<>
inline size_t hint_based_search<ts_src<time_axis::fixed_dt>>(const ts_src<time_axis::fixed_dt>& source, const utcperiod& p, size_t /*i*/) {
    return source.ta.index_of(p.start);
}
template<>
inline size_t hint_based_search<ts_src<time_axis::calendar_dt>>(const ts_src<time_axis::calendar_dt>& source, const utcperiod& p, size_t /*i*/) {
    return source.ta.index_of(p.start);
}
template<>
inline size_t hint_based_search<ts_src<time_axis::point_dt>>(const ts_src<time_axis::point_dt>& source, const utcperiod& p, size_t i) {
    return source.ta.index_of(p.start,i);
}
template<>
inline size_t hint_based_search<ts_src<time_axis::generic_dt>>(const ts_src<time_axis::generic_dt>& source, const utcperiod& p, size_t i) {
    return source.ta.index_of(p.start,i);
}

template<> struct is_ts<dd::apoint_ts> {static const bool value=true;};// enable use of is_ts enabled algos.

    
}

namespace shyft::time_series::dd {

static inline double do_op(double a, iop_t op, double b) {
    switch (op) {
    case iop_t::OP_ADD:return a + b;
    case iop_t::OP_SUB:return a - b;
    case iop_t::OP_DIV:return a / b;
    case iop_t::OP_MUL:return a * b;
    case iop_t::OP_MAX:return std::max(a, b);
    case iop_t::OP_MIN:return std::min(a, b);
    case iop_t::OP_POW:return std::pow(a, b);
    case iop_t::OP_LOG:return std::log(a);
    case iop_t::OP_NONE:break;// just fall to exception
    }
    throw std::runtime_error("unsupported shyft::api::iop_t");
}
// add operators and functions to the apoint_ts class, of all variants that we want to expose
apoint_ts average(const apoint_ts& ts, const gta_t& ta/*fx-type */) { return apoint_ts(std::make_shared<const average_ts>(ta, ts)); }
apoint_ts average(apoint_ts&& ts, const gta_t& ta) { return apoint_ts(std::make_shared<const average_ts>(ta, std::move(ts))); }
apoint_ts integral(const apoint_ts& ts, const gta_t& ta/*fx-type */) { return apoint_ts(std::make_shared<const integral_ts>(ta, ts)); }
apoint_ts integral(apoint_ts&& ts, const gta_t& ta) { return apoint_ts(std::make_shared<const integral_ts>(ta, std::move(ts))); }

apoint_ts accumulate(const apoint_ts& ts, const gta_t& ta/*fx-type */) { return apoint_ts(std::make_shared<const accumulate_ts>(ta, ts)); }
apoint_ts accumulate(apoint_ts&& ts, const gta_t& ta) { return apoint_ts(std::make_shared<const accumulate_ts>(ta, std::move(ts))); }

apoint_ts create_periodic_pattern_ts(const vector<double>& pattern, utctimespan dt, utctime pattern_t0, const gta_t& ta) { return apoint_ts(make_shared<const periodic_ts>(pattern, dt, pattern_t0, ta)); }

apoint_ts operator+(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts       >(lhs, iop_t::OP_ADD, rhs)); }
apoint_ts operator+(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_ADD, rhs)); }
apoint_ts operator+(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_ADD, rhs)); }

apoint_ts operator-(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts       >(lhs, iop_t::OP_SUB, rhs)); }
apoint_ts operator-(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_SUB, rhs)); }
apoint_ts operator-(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_SUB, rhs)); }
apoint_ts operator-(const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(-1.0, iop_t::OP_MUL, rhs)); }

apoint_ts operator/(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts       >(lhs, iop_t::OP_DIV, rhs)); }
apoint_ts operator/(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_DIV, rhs)); }
apoint_ts operator/(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_DIV, rhs)); }

apoint_ts operator*(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts       >(lhs, iop_t::OP_MUL, rhs)); }
apoint_ts operator*(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_MUL, rhs)); }
apoint_ts operator*(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_MUL, rhs)); }


apoint_ts max(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts       >(lhs, iop_t::OP_MAX, rhs)); }
apoint_ts max(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_MAX, rhs)); }
apoint_ts max(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_MAX, rhs)); }

apoint_ts min(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts>(lhs, iop_t::OP_MIN, rhs)); }
apoint_ts min(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_MIN, rhs)); }
apoint_ts min(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_MIN, rhs)); }

apoint_ts pow(const apoint_ts& lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_ts>(lhs, iop_t::OP_POW, rhs)); }
apoint_ts pow(const apoint_ts& lhs, double           rhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_POW, rhs)); }
apoint_ts pow(double           lhs, const apoint_ts& rhs) { return apoint_ts(std::make_shared<const abin_op_scalar_ts>(lhs, iop_t::OP_POW, rhs)); }

apoint_ts log(const apoint_ts& lhs) { return apoint_ts(std::make_shared<const abin_op_ts_scalar>(lhs, iop_t::OP_LOG, 1.0)); }

double abin_op_ts::value_at(utctime t) const {
    if (!time_axis().total_period().contains(t))
        return nan;
    return do_op(lhs(t), op, rhs(t));// this might cost a 2xbin-search if not the underlying ts have smart incremental search (at the cost of thread safety)
}

static vector<double> ts_op_ts_values(const vector<double>& l, iop_t op, const vector<double>& r) {
    vector<double> x; x.reserve(l.size());
    switch (op) {
    case OP_ADD:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(l[i] + r[i]); return x;
    case OP_SUB:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(l[i] - r[i]); return x;
    case OP_MUL:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(l[i] * r[i]); return x;
    case OP_DIV:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(l[i] / r[i]); return x;
    case OP_MAX:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(std::max(l[i], r[i])); return x;
    case OP_MIN:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(std::min(l[i], r[i])); return x;
    case OP_POW:for (size_t i = 0; i < r.size(); ++i) x.emplace_back(std::pow(l[i], r[i])); return x;
    [[fallthrough]];
    case OP_LOG:  // only exposed through for abin_op_ts_scalar
    default: break;
    }
    throw runtime_error("Unsupported operation " + to_string(int(op)));
}

static void lhs_in_place_ts_op_ts_values(vector<double>& l, iop_t op, const vector<double>& r) {
    switch (op) {
    case OP_ADD:for (size_t i = 0; i < r.size(); ++i) l[i] += r[i]; return;
    case OP_SUB:for (size_t i = 0; i < r.size(); ++i) l[i] -= r[i]; return;
    case OP_MUL:for (size_t i = 0; i < r.size(); ++i) l[i] *= r[i]; return;
    case OP_DIV:for (size_t i = 0; i < r.size(); ++i) l[i] /= r[i]; return;
    case OP_MAX:for (size_t i = 0; i < r.size(); ++i) l[i] = std::max(l[i], r[i]); return;
    case OP_MIN:for (size_t i = 0; i < r.size(); ++i) l[i] = std::min(l[i], r[i]); return;
    case OP_POW:for (size_t i = 0; i < r.size(); ++i) l[i] = std::pow(l[i], r[i]); return;
    [[fallthrough]];
    case OP_LOG:  // only exposed through for abin_op_ts_scalar
    default: break;
    }
    throw runtime_error("Unsupported operation " + to_string(int(op)));
}
static void rhs_in_place_ts_op_ts_values(const vector<double>& l, iop_t op, vector<double>& r) {
    switch (op) {
    case OP_ADD:for (size_t i = 0; i < r.size(); ++i) r[i] += l[i]; return;
    case OP_SUB:for (size_t i = 0; i < r.size(); ++i) r[i] = l[i] - r[i]; return;
    case OP_MUL:for (size_t i = 0; i < r.size(); ++i) r[i] *= l[i]; return;
    case OP_DIV:for (size_t i = 0; i < r.size(); ++i) r[i] = l[i] / r[i]; return;
    case OP_MAX:for (size_t i = 0; i < r.size(); ++i) r[i] = std::max(l[i], r[i]); return;
    case OP_MIN:for (size_t i = 0; i < r.size(); ++i) r[i] = std::min(l[i], r[i]); return;
    case OP_POW:for (size_t i = 0; i < r.size(); ++i) r[i] = std::pow(l[i], r[i]); return;
    [[fallthrough]];
    case OP_LOG:  // only exposed through for abin_op_ts_scalar
    default: break;
    }
    throw runtime_error("Unsupported operation " + to_string(int(op)));
}

static inline const vector<double>* terminal_values(ipoint_ts_ref const& ts) {
    if (auto ats=ts_as<aref_ts>(ts))
        return &(ats->core_ts().v);
    if (auto gts=ts_as<gpoint_ts>(ts))
        return &(gts->core_ts().v);
    return nullptr;
}

static inline const vector<double>* terminal_values(const apoint_ts&ats) {
    return terminal_values(ats.ts);

}

/** implementation of apoint_ts op apoint_ts
*
*  Try to optimize the evaluation by considering
*  equal time-axis.
*  Then also check  rhs and lhs.
*  If one is a terminal-value (concrete point ts)
*  then just reference the values, otherwise get the computed values.
*
* If time-axis not aligned, just compute value-by-value.
*
*/
std::vector<double> abin_op_ts::values() const {
    if (lhs.time_axis() == rhs.time_axis()) {
        const vector<double>* lhs_v{ terminal_values(lhs) };
        const vector<double>* rhs_v{ terminal_values(rhs) };
        if (lhs_v && rhs_v) {
            return ts_op_ts_values(*lhs_v, op, *rhs_v);
        } else if (lhs_v) {
            auto r{ rhs.values() };
            rhs_in_place_ts_op_ts_values(*lhs_v, op, r);
            return r;
        } else if (rhs_v) {
            auto l{ lhs.values() };
            lhs_in_place_ts_op_ts_values(l, op, *rhs_v);
            return l;
        } else {
            auto l{ lhs.values() };
            auto r{ rhs.values() };
            lhs_in_place_ts_op_ts_values(l, op, r);
            return l;
        }
    } else {
        // old code: 
        //std::vector<double> r; r.reserve(time_axis().size());
        //for (size_t i = 0; i < time_axis().size(); ++i) {
        //    r.push_back(value(i));//TODO: improve speed using accessors with ix-hint for lhs/rhs stepwise traversal
        //}
        using shyft::time_series::detail::bin_op_resolve_a;
        switch (op) {
            case OP_ADD:return bin_op_resolve_a(time_axis(),lhs,std::plus<double>(),rhs);
            case OP_SUB:return bin_op_resolve_a(time_axis(),lhs,std::minus<double>(),rhs);
            case OP_MUL:return bin_op_resolve_a(time_axis(),lhs,std::multiplies<double>(),rhs);
            case OP_DIV:return bin_op_resolve_a(time_axis(),lhs,std::divides<double>(),rhs);
            case OP_MAX:return bin_op_resolve_a(time_axis(),lhs,[](double const&a,double const&b) noexcept { return std::max(a,b);},rhs);
            case OP_MIN:return bin_op_resolve_a(time_axis(),lhs,[](double const&a,double const&b) noexcept { return std::min(a,b);},rhs);
            case OP_POW:return bin_op_resolve_a(time_axis(),lhs,[](double const&a,double const&b) noexcept { return std::pow(a,b);},rhs);
            //[[fallthrough]];
            case OP_LOG:  // only exposed through for abin_op_ts_scalar
            default: break;
        }
        throw runtime_error("Unsupported operation " + to_string(int(op)));
    }
}

/** implementation of apoint_ts op ... op apoint_ts
*
*  Try to optimize the evaluation by considering equal time-axis.
*  Then also check args.
*  If one is a terminal-value (concrete point ts)
*  then just reference the values, otherwise get the computed values.
*
* If time-axis not aligned, just compute value-by-value.
*
*/

std::vector<double> anary_op_ts::values() const {
    vector<double> tv;
    if(op==nary_op_t::OP_MERGE) {
        tv.reserve(args.size()*fc_interval/seconds(3600));
        forecast_merge_fx(args, lead_time, fc_interval, [&tv](utctime, double v){tv.push_back(v);}, [](utctime){});
    } else if( op==nary_op_t::OP_ADD) {
        tv.reserve(ta.size());
        if(args.size()) {
            auto r=args[0];
            for(size_t i=1;i<args.size();++i)
                r = bin_op_eval<apoint_ts>(r,std::plus<double>(),args[i]);
            return r.values();
        }
    }
    return tv;
}

double anary_op_ts::value_at(utctime t) const {
    auto i = ta.index_of(t);
    return values()[i];
}
double anary_op_ts::value(size_t i) const {
    return values()[i];
}

void anary_op_ts::local_do_bind() {
    if(!bound) {
        if(op==nary_op_t::OP_MERGE) {
            vector<utctime> tv;
            fx_policy = args[0].point_interpretation(); //Pick first
            tv.reserve(args.size()*fc_interval/seconds(3600));
            shyft::time_series::forecast_merge_fx(args, lead_time, fc_interval, [&tv](utctime t, double){tv.push_back(t);}, [&tv](utctime t){tv.push_back(t);});
            // simplify time axis (it is most likely fixed dt)
            if( tv.size() > 1 ) {
                auto dt=tv[1]-tv[0];
                size_t i=2;
                for(auto t=tv[1];tv[i]-t == dt&& i <tv.size();++i)
                    t=tv[i];
                if(i==tv.size())
                    ta=gta_t(tv[0],dt,tv.size()-1);
                else
                    ta=gta_t(move(tv));
            } else {
                ta=gta_t(move(tv));
            }
        } else if( op==nary_op_t::OP_ADD) {
            if(args.size()) { // hmm. maybe we should drop pre-compute of time-axis ?
                ta=args[0].time_axis();
                fx_policy=args[0].point_interpretation();
                for(size_t i=1;i<args.size();++i) {
                    ta=combine(ta,args[i].time_axis());
                    fx_policy=time_series::result_policy(fx_policy,args[i].point_interpretation());
                }
            }
        }
        bound=true;
    }
}
void anary_op_ts::local_do_unbind() {
    if(bound) {
        bound=false;// drop to do anything more
    }
}
anary_op_ts::anary_op_ts()=default;
bool anary_op_ts::needs_bind() const {
    if(bound)
        return false;
    for(auto const & a: args) if(a.needs_bind()) return true;
    return false;
}
void anary_op_ts::do_bind() {
    for(auto & i: args) i.do_bind();
    local_do_bind();
}
void anary_op_ts::do_unbind() {
    for(auto & i: args) i.do_unbind();
    local_do_unbind();
}
anary_op_ts::anary_op_ts(const vector<apoint_ts> &args,nary_op_t op, utctimespan lead_time, utctimespan fc_interval)
    :args{args},op{op},lead_time{lead_time},fc_interval{fc_interval},bound{false} {
    if( !needs_bind() )
        local_do_bind();
}

anary_op_ts::anary_op_ts(vector<apoint_ts> args, nary_op_t op,gta_t const & ta)
    :args(move(args)),op(op),ta(ta),bound(true){
}


/** make_interval template
*
* Help creating implementation of interval type of results (average/integral value)
* at highest possible speed
*/
template < bool avg, class TA, class TS, class ATA >
static std::vector<double> make_interval( const TA &ta, const TS& ts, const ATA& avg_ta) {
    auto pfx = ts->point_interpretation();
    const bool linear_interpretation = pfx == ts_point_fx::POINT_INSTANT_VALUE;
    const vector<double>* src_v{ terminal_values(ts) };//fetch terminal value
    if (src_v) {// if values are already in place, use reference
        const ts_src<TA> rts{ ta,*src_v };
        return linear_interpretation?accumulate_linear(avg_ta,rts,avg):accumulate_stair_case(avg_ta,rts,avg);
    } else {
        auto tsv{ ts->values() };// pull out values from underlying expression.
        const ts_src<TA> rts{ ta,tsv };// note that ->values() deflates any underlying expression, this could be sub optimal in some cases.
        return linear_interpretation?accumulate_linear(avg_ta,rts,avg):accumulate_stair_case(avg_ta,rts,avg);
    }
}

/** Implementation of the average_ts::values
*
*  Important here is to ensure that accessing the time-axis is
*  direct into the core-type implementation of the generic_dt.
*  (avoid switch every single lookup during integration)
*/
std::vector<double> average_ts::values() const {
    if (ts->time_axis() == ta && ts->point_interpretation() == ts_point_fx::POINT_AVERAGE_VALUE) {
        return ts->values(); // elide trivial average_ts cases
    }
    switch (ts->time_axis().gt) { // pull out the real&fast time-axis before doing computations here:
    case time_axis::generic_dt::FIXED:    return make_interval<true>(ts->time_axis().f, ts, ta);
    case time_axis::generic_dt::CALENDAR: return make_interval<true>(ts->time_axis().c, ts, ta);
    case time_axis::generic_dt::POINT:    return make_interval<true>(ts->time_axis().p, ts, ta);
    }
    return make_interval<true>(ts->time_axis(), ts, ta);
}

std::vector<double> integral_ts::values() const {
    switch (ts->time_axis().gt) { // pull out the real&fast time-axis before doing computations here:
    case time_axis::generic_dt::FIXED:    return make_interval<false>(ts->time_axis().f, ts, ta);
    case time_axis::generic_dt::CALENDAR: return make_interval<false>(ts->time_axis().c, ts, ta);
    case time_axis::generic_dt::POINT:    return make_interval<false>(ts->time_axis().p, ts, ta);
    }
    return make_interval<false>(ts->time_axis(), ts, ta);
}
/** helpers */
static inline utctimespan fixed_timestep(const time_axis::fixed_dt& ta) {
    return ta.dt;
}
static inline utctimespan fixed_timestep(const time_axis::point_dt& /*ta*/) {
    return utctimespan{0};
}
static inline utctimespan fixed_timestep(const time_axis::calendar_dt& ta) {
    return ta.dt<time_axis::calendar_dt::dt_tz_semantics?ta.dt:utctimespan{0};
}
static inline utctimespan fixed_timestep(const time_axis::generic_dt& ta) {
    if(ta.gt==time_axis::generic_dt::FIXED) {
        return ta.f.dt;
    } else if(ta.gt==time_axis::generic_dt::CALENDAR && ta.c.dt<time_axis::calendar_dt::dt_tz_semantics){
        return ta.c.dt;
    }
    return utctimespan{0};
}

/**We need a temporary slice of a time-axis */
template <class TA>
struct ta_slice {
    const TA& ta;
    size_t i0;
    size_t n;
    ta_slice(const TA&ta,size_t i0,size_t n):ta(ta),i0(i0),n(n){}
    size_t size() const {return n;}
    utcperiod period(size_t i) const {return ta.period(i0+i);}
    utctime time(size_t i) const {return ta.time(i0+i);}
};

template <class TA>
static inline utctimespan fixed_timestep(const ta_slice<TA>& ta) {
    return fixed_timestep(ta.ta);
}

/** */
template <class TA>
void derivative_fx(const TA& ta,vector<double>& v,derivative_method dm) {
    if(v.size()<2) {
        if(v.size())
            v.front()=isfinite(v.front())?0.0:nan;// flat or nan
    } else {
        auto dt=fixed_timestep(ta);                 // if fixed interval(really) ->highspeed
        if(dt>utctimespan{0}) {
            switch(dm) {
                case derivative_method::default_diff:
                case derivative_method::center_diff:{// maybe average_diff is better name
                double v0=v[0];
                v[0]= isfinite(v0)?(isfinite(v[1])? (v[1]-v0)/to_seconds(2*dt) : 0.0) : nan;// first value special case
                for(size_t i=1;i+1<v.size();++i) {
                    double v1=v[i];
                    if(isfinite(v1)) {
                        if(isfinite(v0)) {
                            if(isfinite(v[i+1])) {
                                v[i] = (v[i+1] - v0)/to_seconds(2*dt);
                            } else {
                                v[i] = (v1 - v0)/to_seconds(2*dt);
                            }
                        } else {
                            if(isfinite(v[i+1])) {
                                v[i] = (v[i+1] - v1)/to_seconds(2*dt);
                            } else {
                                v[i] = 0.0;
                            }
                        }
                    } else {
                        v[i]=nan;
                    }
                    v0=v1;
                }
                v.back()=isfinite(v.back())?(isfinite(v0)?(v.back() - v0)/to_seconds(2*dt):0.0):nan;// last value special case

                }break;
                case derivative_method::forward_diff:{// center forward diff
                for(size_t i=0;i+1<v.size();++i) {
                    v[i]= isfinite(v[i])?(isfinite(v[i+1])?(v[i+1]-v[i])/to_seconds(dt):0.0):nan;
                }
                v.back()=isfinite(v.back())?0.0:nan;
                } break;
                case derivative_method::backward_diff:{// center backward diff
                for(size_t i=v.size()-1;i>0;--i) {
                    v[i]= isfinite(v[i])?(isfinite(v[i-1])?(v[i]-v[i-1])/to_seconds(dt):0.0):nan;
                }
                v.front()=isfinite(v.front())?0.0:nan;
                } break;
            }
        } else { // variable intervals
            switch(dm) {
                case derivative_method::default_diff:
                case derivative_method::center_diff:{// maybe average_diff is better name
                double v0=v[0];
                auto p0=ta.period(0);
                auto p1=ta.period(1);
                v[0]= isfinite(v0)?(isfinite(v[1])?(v[1]-v[0])/to_seconds(p1.end - p0.start):0.0):nan;// first value special case
                for(size_t i=1;i+1<v.size();++i) {
                    p1=ta.period(i);
                    auto v1=v[i];
                    auto p2=ta.period(i+1);
                    auto v2=v[i+1];
                    if(isfinite(v1)){
                        if(isfinite(v0)) {// got left valid value
                            if(isfinite(v2)) { // got right valid value
                                v[i] = 2*(v2-v0)/to_seconds((p2.start-p0.start) +(p2.end-p0.end));
                            } else { // right is nan, finish flat last half
                                v[i] = (v1-v0)/to_seconds(p1.end - p0.start);
                            }
                        } else if(isfinite(v2)) {// valid right side, nan on the left
                            v[i] = (v2-v1)/to_seconds(p2.end - p1.start);
                        } else { // both sides nan, flat
                            v[i] = 0.0;
                        }
                    } else {
                        v[i] = nan;
                    }
                    v0=v1;
                    p0=p1;
                }
                // we got p0,v0,
                p1=ta.period(v.size()-1);
                auto v1=v.back();
                if(isfinite(v1)) {
                    if(isfinite(v0)) {
                        v.back() = (v1-v0)/to_seconds(p1.end-p0.start);
                    } else {
                        v.back() = 0.0;
                    }
                } else {
                    v.back()=nan;
                }

                }break;
                case derivative_method::forward_diff:{// center forward diff
                    auto p0 = ta.period(0);
                    auto v0 = v[0];
                    for(size_t i=0;i<v.size()-1;++i) {
                        auto p1=ta.period(i+1);
                        auto v1=v[i+1];
                        if(isfinite(v0)) {
                            if(isfinite(v1)) {
                                v[i] = 2*(v1 - v0)/ to_seconds((p1.start-p0.start) + (p1.end-p0.end));
                            } else {
                                v[i] = 0.0; // flat out
                            }
                        } else {
                            v[i]=nan;
                        }
                        p0=p1;
                        v0=v1;
                    }
                    v.back()=isfinite(v.back())?0.0:nan;// flat out last step.
                } break;
                case derivative_method::backward_diff:{// center backward diff
                    auto p0 = ta.period(0);
                    auto v0 = v[0];
                    v.front()=isfinite(v.front())?0.0:nan;
                    for(size_t i=1;i<v.size();++i) {
                        auto p1=ta.period(i);
                        auto v1=v[i];
                        if(isfinite(v1)) {
                            if(isfinite(v0)) {
                                v[i] = 2*(v1 - v0)/ to_seconds((p1.start-p0.start) + (p1.end-p0.end));
                            } else {
                                v[i] = 0.0; // flat out
                            }
                        } else {
                            v[i]=nan;
                        }
                        p0=p1;
                        v0=v1;
                    }
                } break;
            }
        }
    }
}

std::vector<double> derivative_ts::values() const {
    if(!ts) throw runtime_error("derivative of null ts attempted");
    auto v = ts->values();
    if(ts->point_interpretation()==POINT_INSTANT_VALUE) {
        for(size_t i=0;i+1<v.size();++i) {
            v[i]= (v[i+1]-v[i])/to_seconds(ts->time(i+1)-ts->time(i));
        }
        if(v.size())
            v[v.size()-1] = nan;
    } else {  // implement dm for stair-case
        derivative_fx(ts->time_axis(),v,dm);
    }
    return v;
}
double derivative_ts::value(size_t i) const {
    if(!ts) throw runtime_error("derivative of null ts attempted");
    if(ts->point_interpretation()==POINT_INSTANT_VALUE) {
        if(i+1<ts->size()) {
            return (ts->value(i+1)-ts->value(i))/to_seconds(ts->time_axis().period(i).timespan());
        } else {
            return nan;
        }
    } else {
        vector<double> v;v.reserve(3);
        size_t i0=i;
        if(i>0){ v.push_back(ts->value(i-1));i0=i-1;}
        v.push_back(ts->value(i));
        if(i+1<ts->size()) v.push_back(ts->value(i+1));
        //const auto & ta = ts->time_axis();
        ta_slice<decltype(ts->time_axis())> ta(ts->time_axis(),i0,v.size());
        derivative_fx(ta,v,dm);
        return v[i>0?1:0];
    }
    return nan;
}

double derivative_ts::value_at(utctime t) const {
    if(!ts) throw runtime_error("derivative of null ts attempted");
    size_t ix=ts->index_of(t); // easy, just figure out the ix of the period
    if(ix == string::npos)
        return nan;
    return value(ix);// and forward it to the value method.
}

apoint_ts apoint_ts::derivative(derivative_method dm) const {
    return apoint_ts(make_shared<const derivative_ts>(ts,dm));
}
// implement popular ct for apoint_ts to make it easy to expose & use
apoint_ts::apoint_ts(const time_axis::generic_dt& ta, double fill_value, ts_point_fx point_fx)
    :ts(std::make_shared<const gpoint_ts>(ta, fill_value, point_fx)) {
}
apoint_ts::apoint_ts(const time_axis::generic_dt& ta, const std::vector<double>& values, ts_point_fx point_fx)
    : ts(std::make_shared<const gpoint_ts>(ta, values, point_fx)) {
}
apoint_ts::apoint_ts(const time_axis::generic_dt& ta, std::vector<double>&& values, ts_point_fx point_fx)
    : ts(std::make_shared<const gpoint_ts>(ta, std::move(values), point_fx)) {
}

apoint_ts::apoint_ts(std::string ref_ts_id)
    : ts(std::make_shared<const aref_ts>(ref_ts_id)) {
}
apoint_ts::apoint_ts(std::string ref_ts_id, const apoint_ts&bts)
    : ts(std::make_shared<const aref_ts>(ref_ts_id)) {
    bind(bts);// bind the symbolic ts directly
}



void apoint_ts::bind(const apoint_ts& bts) {
    auto rts=ts_as_mutable<aref_ts>(ts);
    if (!rts)
        throw runtime_error("this time-series is not bindable");
    
    if (ts_as<gpoint_ts>(bts.ts)) {
        rts->rep = dynamic_pointer_cast<const gpoint_ts>(bts.ts);
    } else if (!bts.needs_bind()) {
        rts->rep = make_shared<const gpoint_ts>(bts.time_axis(), bts.values(), bts.point_interpretation());
    } else {
        throw runtime_error("bind:"+id()+", the supplied argument time-series must be a point ts or something that directly resolves to one");
    }
}

string apoint_ts::id() const {
    if (auto cts=ts_as<aref_ts>(ts))
        return cts->id;
    return string{};
}

void apoint_ts::set_id(string const&idv) {
    if (auto cts=ts_as_mutable<aref_ts>(ts)) {
        if(cts->needs_bind()) {
            cts->id=idv;
        } else{
            throw std::runtime_error("a reference ts can not be change when bound");
        }
    } else {
        throw std::runtime_error("attempt to set id on a non reference time-series");
    }
}

// and python needs these:
apoint_ts::apoint_ts(const time_axis::fixed_dt& ta, double fill_value, ts_point_fx point_fx)
    :apoint_ts(time_axis::generic_dt(ta), fill_value, point_fx) {
}
apoint_ts::apoint_ts(const time_axis::fixed_dt& ta, const std::vector<double>& values, ts_point_fx point_fx)
    : apoint_ts(time_axis::generic_dt(ta), values, point_fx) {
}

// and python needs these:
apoint_ts::apoint_ts(const time_axis::point_dt& ta, double fill_value, ts_point_fx point_fx)
    : apoint_ts(time_axis::generic_dt(ta), fill_value, point_fx) {
}
apoint_ts::apoint_ts(const time_axis::point_dt& ta, const std::vector<double>& values, ts_point_fx point_fx)
    : apoint_ts(time_axis::generic_dt(ta), values, point_fx) {
}
apoint_ts::apoint_ts(const rts_t &rts) :
    apoint_ts(time_axis::generic_dt(rts.ta), rts.v, rts.point_interpretation()) {
}

apoint_ts::apoint_ts(const vector<double>& pattern, utctimespan dt, const time_axis::generic_dt& ta) :
    apoint_ts(make_shared<const periodic_ts>(pattern, dt, ta)) {
}
apoint_ts::apoint_ts(const vector<double>& pattern, utctimespan dt, utctime pattern_t0, const time_axis::generic_dt& ta) :
    apoint_ts(make_shared<const periodic_ts>(pattern, dt, pattern_t0, ta)) {
}

apoint_ts::apoint_ts(time_axis::generic_dt&& ta, std::vector<double>&& values, ts_point_fx point_fx)
    : ts(std::make_shared<const gpoint_ts>(std::move(ta), std::move(values), point_fx)) {
}
apoint_ts::apoint_ts(time_axis::generic_dt&& ta, double fill_value, ts_point_fx point_fx)
    : ts(std::make_shared<const gpoint_ts>(std::move(ta), fill_value, point_fx)) {
}
apoint_ts apoint_ts::average(const gta_t &ta) const {
    return shyft::time_series::dd::average(*this, ta);
}
apoint_ts apoint_ts::integral(const gta_t &ta) const {
    return shyft::time_series::dd::integral(*this, ta);
}
apoint_ts apoint_ts::accumulate(const gta_t &ta) const {
    return shyft::time_series::dd::accumulate(*this, ta);
}
apoint_ts apoint_ts::time_shift(utctimespan dt) const {
    return shyft::time_series::dd::time_shift(*this, dt);
}
apoint_ts apoint_ts::extend(
    const apoint_ts & ts,
    extend_ts_split_policy split_policy, extend_ts_fill_policy fill_policy,
    utctime split_at, double fill_value
) const {
    return shyft::time_series::dd::extend(
        *this, ts,
        split_policy, fill_policy,
        split_at, fill_value
    );
}

/** recursive function to dig out bind_info */
void find_ts_bind_info(const std::shared_ptr<const ipoint_ts>&its, std::vector<ts_bind_info>&r) {
    using namespace shyft;
    if (its == nullptr)
        return;
    if (auto rts=ts_as<aref_ts>(its)) {
        //HMM: if(!rts->rep) // only bind if not already bound ?
            r.push_back(ts_bind_info(rts->id, apoint_ts(its)));
    } else if (auto ts=ts_as<average_ts>(its))    {find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<integral_ts>(its))   {find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<accumulate_ts>(its)) {find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<time_shift_ts>(its)) {find_ts_bind_info(ts->ts, r);
    } else if (auto bin_op=ts_as<abin_op_ts>(its)) {
        find_ts_bind_info(bin_op->lhs.ts, r);
        find_ts_bind_info(bin_op->rhs.ts, r);
    } else if (auto bin_op=ts_as<abin_op_scalar_ts>(its)) {
        find_ts_bind_info(bin_op->rhs.ts, r);
    } else if (auto bin_op=ts_as<abin_op_ts_scalar>(its)) {
        find_ts_bind_info(bin_op->lhs.ts, r);
    } else if (auto nary_op=ts_as<anary_op_ts>(its)) {
        nary_op->find_ts_bind_info(r);
    } else if (auto ts=ts_as<abs_ts>(its)) { find_ts_bind_info(ts->ts, r);
    } else if (auto ext=ts_as<extend_ts>(its)) {
        find_ts_bind_info(ext->lhs.ts, r);
        find_ts_bind_info(ext->rhs.ts, r);
    } else if (auto ext=ts_as<use_time_axis_from_ts>(its)) {
        find_ts_bind_info(ext->lhs.ts, r);
        find_ts_bind_info(ext->rhs.ts, r);
    } else if (auto ts=ts_as<ice_packing_ts>(its)) {find_ts_bind_info(ts->ts.temp_ts.ts, r);
    } else if (auto iprt=ts_as<ice_packing_recession_ts>(its)) {
        find_ts_bind_info(iprt->flow_ts.ts, r);
        find_ts_bind_info(iprt->ice_packing_ts.ts, r);
    } else if (auto ts=ts_as<rating_curve_ts>(its)) {find_ts_bind_info(ts->ts.level_ts.ts, r);
    } else if (auto ts=ts_as<krls_interpolation_ts>(its)) {
        find_ts_bind_info(ts->ts.ts, r);
    } else if (auto ts=ts_as<qac_ts>(its)) {
        find_ts_bind_info(ts->ts, r);
        find_ts_bind_info(ts->cts, r);
    } else if (auto ts=ts_as<inside_ts>(its))    {find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<derivative_ts>(its)){find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<convolve_w_ts>(its)){find_ts_bind_info(ts->ts_impl.ts.ts, r);
    } else if (auto ts=ts_as<bucket_ts>(its))    {find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<repeat_ts>(its))    {find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<statistics_ts>(its)){find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<decode_ts>(its)){find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<transform_spline_ts>(its)){find_ts_bind_info(ts->ts, r);
    } else if (auto ts=ts_as<aglacier_melt_ts>(its)) {
        find_ts_bind_info(ts->gm.temperature,r);
        find_ts_bind_info(ts->gm.sca_m2,r);
    }

}

std::vector<ts_bind_info> apoint_ts::find_ts_bind_info() const {
    std::vector<ts_bind_info> r;
    shyft::time_series::dd::find_ts_bind_info(ts, r);
    return r;
}

ats_vector apoint_ts::stack_ts(const calendar& cal, utctime t, size_t n_dt, utctime dt, size_t n_partitions, utctime common_t0,utctimespan dt_snap) const {
    // some very rudimentary argument checks:
    if (n_partitions < 1)
        throw std::runtime_error("n_partitions should be > 0");
    if (dt.count() <= 0)
        throw std::runtime_error("dt should be > 0, typically Calendar::YEAR|MONTH|WEEK|DAY");
    if (n_dt <1)
        throw std::runtime_error("n_dt should be > 0");

    auto mk_raw_time_shift = [](const apoint_ts& ts, utctimespan dt)->apoint_ts {
        return apoint_ts(std::make_shared<const time_shift_ts>(ts, dt));
    };
    auto r = shyft::time_series::stack_ts<apoint_ts>(*this, cal, t,n_dt,dt, n_partitions, common_t0,dt_snap, mk_raw_time_shift);
    return ats_vector(r.begin(), r.end());
}

void apoint_ts::set(size_t i, double x) {
    gpoint_ts *gpts = ts_as_mutable<gpoint_ts>(ts);
    if (!gpts)
        throw std::runtime_error("apoint_ts::set(i,x) only allowed for ts of non-expression types");
    gpts->set(i, x);
}
void apoint_ts::fill(double x) {
    gpoint_ts *gpts = ts_as_mutable<gpoint_ts>(ts);
    if (!gpts)
        throw std::runtime_error("apoint_ts::fill(x) only allowed for ts of non-expression types");
    gpts->fill(x);
}
void apoint_ts::scale_by(double x) {
    gpoint_ts *gpts = ts_as_mutable<gpoint_ts>(ts);
    if (!gpts)
        throw std::runtime_error("apoint_ts::scale_by(x) only allowed for ts of non-expression types");
    gpts->scale_by(x);
}

apoint_ts apoint_ts::compress(double accuracy) const {
    if(needs_bind()) 
        throw std::runtime_error("apoint_ts::compress: does not support lazy/unbound expressions");
    auto tmp=this->evaluate();// incase it's an expression
    return ts_compress<apoint_ts,double,gta_t>(tmp,accuracy);
}

size_t apoint_ts::compress_size(double accuracy) const {
    if(needs_bind()) 
        throw std::runtime_error("apoint_ts::compress: does not support lazy/unbound expressions");
    return ts_compress_size(values(),accuracy);
}


bool apoint_ts::operator==(const apoint_ts& other) const {
    if (ts.get() == other.ts.get()) // equal by reference
        return true;
    if( !ts || !other.ts)
        return false;// different, one is nullptr, other is not nullptr
    if( needs_bind() || other.needs_bind()) {
            throw runtime_error("TimeSeries, or expression unbound, equal op require bound ts, please bind sym-ts before use.");
    }
    if (ts->size() != other.ts->size())
        return false;
    for (size_t i = 0; i < ts->size(); ++i) {
        if (ts->time_axis().period(i) != other.ts->time_axis().period(i))
            return false;
        if (!shyft::core::nan_equal(ts->value(i) , other.ts->value(i)))
            return false;
    }
    return true;
}

apoint_ts apoint_ts::max(double a) const { return shyft::time_series::dd::max(*this, a); }
apoint_ts apoint_ts::min(double a) const { return shyft::time_series::dd::min(*this, a); }

apoint_ts apoint_ts::max(const apoint_ts& other) const { return shyft::time_series::dd::max(*this, other); }
apoint_ts apoint_ts::min(const apoint_ts& other) const { return shyft::time_series::dd::min(*this, other); }

apoint_ts apoint_ts::max(const apoint_ts &a, const apoint_ts&b) { return shyft::time_series::dd::max(a, b); }
apoint_ts apoint_ts::min(const apoint_ts &a, const apoint_ts&b) { return shyft::time_series::dd::min(a, b); }

apoint_ts apoint_ts::pow(const apoint_ts &a, const apoint_ts&b) { return shyft::time_series::dd::pow(a, b); }
apoint_ts apoint_ts::pow(double a) const { return shyft::time_series::dd::pow(*this, a); }
apoint_ts apoint_ts::pow(const apoint_ts& other) const { return shyft::time_series::dd::pow(*this, other); }

apoint_ts apoint_ts::log() const { return shyft::time_series::dd::log(*this); }
apoint_ts apoint_ts::log(const apoint_ts & a) { return shyft::time_series::dd::log(a); }

apoint_ts apoint_ts::convolve_w(const std::vector<double> &w, shyft::time_series::convolve_policy conv_policy) const {
    return apoint_ts(std::make_shared<const convolve_w_ts>(*this, w, conv_policy));
}
apoint_ts apoint_ts::slice(int i0, int n) const {
    gpoint_ts *gpts = ts_as_mutable<gpoint_ts>(ts);
    if (!gpts)
        throw std::runtime_error("apoint_ts::slice() only allowed for ts of non-expression types");
    return apoint_ts(make_shared<const gpoint_ts>(gpts->slice(i0, n)));
}

apoint_ts apoint_ts::rating_curve(const rating_curve_parameters & rc_param) const {
    return apoint_ts(std::make_shared<const rating_curve_ts>(*this, rc_param));
}

apoint_ts apoint_ts::ice_packing(const ice_packing_parameters & ip_param, ice_packing_temperature_policy ipt_policy) const {
    return apoint_ts(std::make_shared<const ice_packing_ts>(*this, ip_param, ipt_policy));
}

apoint_ts apoint_ts::ice_packing_recession(
    const apoint_ts & ice_packing_ts,
    const ice_packing_recession_parameters & ipr_param
) const {
    return apoint_ts(std::make_shared<const ice_packing_recession_ts>(
        *this, ice_packing_ts, ipr_param
    ));
}

apoint_ts apoint_ts::krls_interpolation(core::utctimespan dt, double rbf_gamma, double tol, std::size_t size) const {
    return apoint_ts(std::make_shared<const krls_interpolation_ts>(*this, dt, rbf_gamma, tol, size));
}
apoint_ts apoint_ts::merge_points(const apoint_ts& o) {
    if(!o.ts)
        return *this;
    if(!ts) { // we are empty, create a new point ts
        auto a=make_shared<gpoint_ts>();
        ts_point_merge(a->rep,o);
        ts=a;
    } else {// we have existing ts, verify type:
        if(auto a=ts_as_mutable<gpoint_ts>(ts)) {
            ts_point_merge(a->rep,o);// merge
        } else if(auto a=ts_as_mutable<aref_ts>(ts)) {
            if(a->rep) { // its already bound
                ts_point_merge(ts_as_mutable<gpoint_ts>(a->rep)->rep,o);
            } else {
                auto r=make_shared<gpoint_ts>();
                ts_point_merge(r->rep,o);
                a->rep=r;
            }
        } else {
            throw runtime_error("self.merge_points_from:self ts must be a concrete point ts");
        }
    }
    return *this;
}

template<class TSV>
static bool all_same_generic_time_axis_type(const TSV&tsv) {
    if (tsv.size() == 0) return true;
    auto gt = tsv[0].time_axis().gt;
    for (const auto&ts : tsv) {
        if (ts.time_axis().gt != gt)
            return false;
    }
    return true;
}

std::vector<apoint_ts> percentiles(const std::vector<apoint_ts>& tsv1, const gta_t& ta, const intv_t& percentile_list) {
    std::vector<apoint_ts> r; r.reserve(percentile_list.size());
    auto tsvx = deflate_ts_vector<gts_t>(tsv1);
    // check of all tsvx.time_axis is of same type
    //  make that vector type (move values), and run percentile calc for that
    //  the objective is to avoid traffic over the 'switch' in generic_dt
    //  and provide direct access to representative time-axis instead
    if (all_same_generic_time_axis_type(tsvx)) {
        if (tsvx.size()) {
            auto gt = tsvx[0].time_axis().gt;
            switch (gt) {
            case time_axis::generic_dt::FIXED: {
                std::vector<point_ts<time_axis::fixed_dt>> tsv; tsv.reserve(tsvx.size());
                for (auto&ts : tsvx) tsv.emplace_back(move(ts.ta.f), move(ts.v), ts.fx_policy);
                auto rp = shyft::time_series::calculate_percentiles(ta, tsv, percentile_list);
                for (auto&ts : rp) r.emplace_back(ta, std::move(ts.v), POINT_AVERAGE_VALUE);
            } break;
            case time_axis::generic_dt::CALENDAR: {
                std::vector<point_ts<time_axis::calendar_dt>> tsv; tsv.reserve(tsvx.size());
                for (auto&ts : tsvx) tsv.emplace_back(move(ts.ta.c), move(ts.v), ts.fx_policy);
                auto rp = shyft::time_series::calculate_percentiles(ta, tsv, percentile_list);
                for (auto&ts : rp) r.emplace_back(ta, std::move(ts.v), POINT_AVERAGE_VALUE);
            } break;
            case time_axis::generic_dt::POINT: {
                std::vector<point_ts<time_axis::point_dt>> tsv; tsv.reserve(tsvx.size());
                for (auto&ts : tsvx) tsv.emplace_back(move(ts.ta.p), move(ts.v), ts.fx_policy);
                auto rp = shyft::time_series::calculate_percentiles(ta, tsv, percentile_list);
                for (auto&ts : rp) r.emplace_back(ta, std::move(ts.v), POINT_AVERAGE_VALUE);
            } break;
            }
        } else {
            for (size_t i = 0; i < percentile_list.size(); ++i)
                r.emplace_back(ta, shyft::nan, POINT_AVERAGE_VALUE);
        }
    } else {
        auto rp = shyft::time_series::calculate_percentiles(ta, tsvx, percentile_list);
        for (auto&ts : rp) r.emplace_back(ta, std::move(ts.v), POINT_AVERAGE_VALUE);
    }
    return r;
}

std::vector<apoint_ts> percentiles(const std::vector<apoint_ts>& ts_list, const time_axis::fixed_dt& ta, const intv_t& percentile_list) {
    return percentiles(ts_list, time_axis::generic_dt(ta), percentile_list);
}

double abin_op_ts::value(size_t i) const {
    if (i == std::string::npos || i >= time_axis().size())
        return nan;
    return value_at(time_axis().time(i));
}
double abin_op_scalar_ts::value_at(utctime t) const {
    bind_check();
    return do_op(lhs, op, rhs(t));
}
double abin_op_scalar_ts::value(size_t i) const {
    bind_check();
    return do_op(lhs, op, rhs.value(i));
}

std::vector<double> abin_op_scalar_ts::values() const {
    bind_check();
    const vector<double> *rhs_v{ terminal_values(rhs) };
    if (rhs_v) {
        const auto& r_v = *rhs_v;
        std::vector<double> r; r.reserve(r_v.size());
        auto l = lhs;
        switch (op) {
        case OP_ADD:for (const auto&v : r_v) r.emplace_back(v + l); return r;
        case OP_SUB:for (const auto&v : r_v) r.emplace_back(l - v); return r;
        case OP_MUL:for (const auto&v : r_v) r.emplace_back(l*v); return r;
        case OP_DIV:for (const auto&v : r_v) r.emplace_back(l / v); return r;
        case OP_MAX:for (const auto&v : r_v) r.emplace_back(std::max(v, l)); return r;
        case OP_MIN:for (const auto&v : r_v) r.emplace_back(std::min(v, l)); return r;
        case OP_POW:for (const auto&v : r_v) r.emplace_back(std::pow(l, v)); return r;
        [[fallthrough]];
        case OP_LOG:  // only exposed through for abin_op_ts_scalar
        default: throw runtime_error("Unsupported operation " + to_string(int(op)));
        }
    } else {
        std::vector<double> r(rhs.values());
        auto l = lhs;
        switch (op) {
        case OP_ADD:for (size_t i = 0; i < r.size(); ++i) r[i] += l; return r;
        case OP_SUB:for (size_t i = 0; i < r.size(); ++i) r[i] = l - r[i]; return r;
        case OP_MUL:for (size_t i = 0; i < r.size(); ++i) r[i] *= l; return r;
        case OP_DIV:for (size_t i = 0; i < r.size(); ++i) r[i] = l / r[i]; return r;
        case OP_MAX:for (size_t i = 0; i < r.size(); ++i) r[i] = std::max(r[i], l); return r;
        case OP_MIN:for (size_t i = 0; i < r.size(); ++i) r[i] = std::min(r[i], l); return r;
        case OP_POW:for (size_t i = 0; i < r.size(); ++i) r[i] = std::pow(l,r[i]); return r;
        [[fallthrough]];
        case OP_LOG:  // only exposed through for abin_op_ts_scalar
        default: throw runtime_error("Unsupported operation " + to_string(int(op)));
        }
    }
}

double abin_op_ts_scalar::value_at(utctime t) const {
    bind_check();
    return do_op(lhs(t), op, rhs);
}
double abin_op_ts_scalar::value(size_t i) const {
    bind_check();
    return do_op(lhs.value(i), op, rhs);
}
std::vector<double> abin_op_ts_scalar::values() const {
    bind_check();
    const vector<double>* lhs_v{ terminal_values(lhs) };
    if (lhs_v) { // avoid a copy, but does not help much..
        std::vector<double> r; r.reserve(lhs_v->size());
        auto rv = rhs;
        switch (op) {
        case OP_ADD:for (const auto&lv : *lhs_v) r.emplace_back(lv + rv); return r;
        case OP_SUB:for (const auto&lv : *lhs_v) r.emplace_back(lv - rv); return r;
        case OP_MUL:for (const auto&lv : *lhs_v) r.emplace_back(lv*rv); return r;
        case OP_DIV:for (const auto&lv : *lhs_v) r.emplace_back(lv / rv); return r;
        case OP_MAX:for (const auto&lv : *lhs_v) r.emplace_back(std::max(lv, rv)); return r;
        case OP_MIN:for (const auto&lv : *lhs_v) r.emplace_back(std::min(lv, rv)); return r;
        case OP_POW:for (const auto&lv : *lhs_v) r.emplace_back(std::pow(lv, rv)); return r;
        case OP_LOG:for (const auto&lv : *lhs_v) r.emplace_back(std::log(lv)); return r;
        default: throw runtime_error("Unsupported operation " + to_string(int(op)));
        }
    } else {
        std::vector<double> l(lhs.values());
        auto r = rhs;
        switch (op) {
        case OP_ADD:for (size_t i = 0; i < l.size(); ++i) l[i] += r; return l;
        case OP_SUB:for (size_t i = 0; i < l.size(); ++i) l[i] -= r; return l;
        case OP_MUL:for (size_t i = 0; i < l.size(); ++i) l[i] *= r; return l;
        case OP_DIV:for (size_t i = 0; i < l.size(); ++i) l[i] /= r; return l;
        case OP_MAX:for (size_t i = 0; i < l.size(); ++i) l[i] = std::max(l[i], r); return l;
        case OP_MIN:for (size_t i = 0; i < l.size(); ++i) l[i] = std::min(l[i], r); return l;
        case OP_POW:for (size_t i = 0; i < l.size(); ++i) l[i] = std::pow(l[i], r); return l;
        case OP_LOG:for (size_t i = 0; i < l.size(); ++i) l[i] = std::log(l[i]); return l;
        default: throw runtime_error("Unsupported operation " + to_string(int(op)));
        }
    }
}

apoint_ts time_shift(const apoint_ts& ts, utctimespan dt) {
    return apoint_ts(std::make_shared<const time_shift_ts>(ts, dt));
}

apoint_ts apoint_ts::abs() const {
    return apoint_ts(std::make_shared<const abs_ts>(ts));
}

apoint_ts apoint_ts::min_max_check_linear_fill(double min_x, double max_x, utctimespan max_dt) const {
    return apoint_ts(make_shared<const qac_ts>(*this, qac_parameter{ max_dt,min_x,max_x }));
}

apoint_ts apoint_ts::min_max_check_ts_fill(double min_x, double max_x, utctimespan max_dt, const apoint_ts& cts) const {
    return apoint_ts(make_shared<const qac_ts>(*this, qac_parameter{ max_dt,min_x,max_x }, cts));
}

apoint_ts apoint_ts::inside(double min_v,double max_v,double nan_v,double inside_v,double outside_v) const {
    return apoint_ts(make_shared<const inside_ts>(*this, inside_parameter{ min_v,max_v,nan_v,inside_v,outside_v }));
}

apoint_ts apoint_ts::transform_spline(const spline_parameter& p) const {
    return apoint_ts(make_shared<const transform_spline_ts>(*this, p));
}

apoint_ts apoint_ts::transform_spline(const std::vector<double>& knots, const std::vector<double>& coeff, std::size_t degree) const {
    return apoint_ts(make_shared<const transform_spline_ts>(*this, spline_parameter{knots, coeff, degree}));
}

apoint_ts apoint_ts::transform(const xy_point_curve& xy, interpolation_scheme scheme) const {
    spline_parameter p = spline_interpolator::interpolate(xy, scheme);
    return apoint_ts(make_shared<const transform_spline_ts>(*this, p));
}


apoint_ts apoint_ts::decode(int start_bit,int n_bits) const {
    if(start_bit <0 || start_bit >51)
        throw runtime_error("start_bit must be in range [0..51], was " + to_string(start_bit));
    if(n_bits < 1 || start_bit + n_bits > 51)
        throw runtime_error("n_bits must be > 0 and start_bit+n_bits <= 51: n_bits =" + to_string(n_bits) + ", start_bit="+to_string(start_bit));
    return apoint_ts(make_shared<const decode_ts>(*this, bit_decoder{(unsigned int)start_bit,(unsigned int)n_bits }));
}

double nash_sutcliffe(const apoint_ts& observation_ts, const apoint_ts& model_ts, const gta_t &ta) {
    average_accessor<apoint_ts, gta_t> o(observation_ts, ta);
    average_accessor<apoint_ts, gta_t> m(model_ts, ta);
    return 1.0 - shyft::time_series::nash_sutcliffe_goal_function(o, m);
}

double kling_gupta(const apoint_ts & observation_ts, const apoint_ts &model_ts, const gta_t & ta, double s_r, double s_a, double s_b) {
    average_accessor<apoint_ts, gta_t> o(observation_ts, ta);
    average_accessor<apoint_ts, gta_t> m(model_ts, ta);
    return 1.0 - shyft::time_series::kling_gupta_goal_function<dlib::running_scalar_covariance<double>>(o, m, s_r, s_a, s_b);
}
apoint_ts apoint_ts::statistics(const gta_t ta,int64_t p) const {
    return apoint_ts(make_shared<const statistics_ts>(apoint_ts(ts),ta,p));
}

void statistics_ts::check_percentile_range_or_throw() {
    if(!( (p>=0 && p<=100) || p==statistics_property::MIN_EXTREME || p==statistics_property::MAX_EXTREME || p==statistics_property::AVERAGE))
    throw runtime_error("statistics_ts: invalid percentile argument "+to_string(p));
}
statistics_ts::statistics_ts(const apoint_ts& ats, gta_t const& ta, int64_t p):ts(ats.ts),ta(ta),p(p){
    check_percentile_range_or_throw();
}
statistics_ts::statistics_ts(apoint_ts&& ats, gta_t const& ta, int64_t p):ts(move(ats.ts)),ta(ta),p(p){
    check_percentile_range_or_throw();        
}

double statistics_ts::value(size_t i) const {
    if(i>=ta.size()  || ts==nullptr)
        return shyft::nan;
    auto px=ta.period(i);
    return ts_percentile_values(*ts,time_axis::fixed_dt(px.start,px.timespan(),1),p)[0];
}

double statistics_ts::value_at(utctime t) const {
    if(!ta.total_period().contains(t) || ts==nullptr)
        return shyft::nan;
    auto ix=ta.index_of(t);
    auto px=ta.period(ix);
    return ts_percentile_values(*ts,time_axis::fixed_dt(px.start,px.timespan(),1),p)[0];
}

vector<double> statistics_ts::values() const {
    return ts_percentile_values(*ts,ta,p);
}

// methods for binding and symbolic ts
bool statistics_ts::needs_bind() const { return ts?ts->needs_bind():false;}
    
void statistics_ts::do_bind() {if(ts) dref(ts).do_bind();}
void statistics_ts::do_unbind() {if(ts) dref(ts).do_unbind();}

apoint_ts create_glacier_melt_ts_m3s(const apoint_ts & temp, const apoint_ts& sca_m2, double glacier_area_m2, double dtf) {
    return apoint_ts(make_shared<const aglacier_melt_ts>(temp, sca_m2, glacier_area_m2, dtf));
}



std::vector<char> apoint_ts::serialize_to_bytes() const {
    auto ss = serialize();
    return std::vector<char>(std::begin(ss), std::end(ss));
}
apoint_ts apoint_ts::deserialize_from_bytes(const std::vector<char>&ss) {
    return deserialize(std::string(ss.data(), ss.size()));
}

//--ats_vector impl.
// multiply operators
ats_vector operator*(ats_vector const &a, double b) { ats_vector r; r.reserve(a.size()); for (auto const&ts : a) r.push_back(ts*b); return r; }
ats_vector operator*(double a, ats_vector const &b) { return b * a; }
ats_vector operator*(ats_vector const &a, ats_vector const& b) {
    if (a.size() != b.size()) throw runtime_error(string("ts-vector multiply require same sizes: lhs.size=") + std::to_string(a.size()) + string(",rhs.size=") + std::to_string(b.size()));
    ats_vector r; r.reserve(a.size()); for (size_t i = 0; i < a.size(); ++i) r.push_back(a[i] * b[i]);
    return r;
}
ats_vector operator*(ats_vector::value_type const &a, ats_vector const& b) { ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back(a*b[i]); return r; }
ats_vector operator*(ats_vector const& b, ats_vector::value_type const &a) { return a * b; }


// divide operators
ats_vector operator/(ats_vector const &a, double b) { return a * (1.0 / b); }
ats_vector operator/(double a, ats_vector const &b) { ats_vector r; r.reserve(b.size()); for (auto const&ts : b) r.push_back(a / ts); return r; }
ats_vector operator/(ats_vector const &a, ats_vector const& b) {
    if (a.size() != b.size()) throw runtime_error(string("ts-vector divide require same sizes: lhs.size=") + std::to_string(a.size()) + string(",rhs.size=") + std::to_string(b.size()));
    ats_vector r; r.reserve(a.size()); for (size_t i = 0; i < a.size(); ++i) r.push_back(a[i] / b[i]);
    return r;
}
ats_vector operator/(ats_vector::value_type const &a, ats_vector const& b) { ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back(a / b[i]); return r; }
ats_vector operator/(ats_vector const& b, ats_vector::value_type const &a) { ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back(b[i] / a); return r; }

// add operators
ats_vector operator+(ats_vector const &a, double b) { ats_vector r; r.reserve(a.size()); for (auto const&ts : a) r.push_back(ts + b); return r; }
ats_vector operator+(double a, ats_vector const &b) { return b + a; }
ats_vector operator+(ats_vector const &a, ats_vector const& b) {
    // additional rule nice for reduce(add.. )
    // if one of tsv is 0, result is a tsv.
    if (a.size()==0 && b.size()!=0)
        return b;
    if (a.size()!=0 && b.size()==0)
        return a;
    if (a.size() != b.size()) throw runtime_error(string("ts-vector add require same sizes: lhs.size=") + std::to_string(a.size()) + string(",rhs.size=") + std::to_string(b.size()));
    ats_vector r; r.reserve(a.size()); for (size_t i = 0; i < a.size(); ++i) r.push_back(a[i] + b[i]);
    return r;
}
ats_vector operator+(ats_vector::value_type const &a, ats_vector const& b) { ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back(a + b[i]); return r; }
ats_vector operator+(ats_vector const& b, ats_vector::value_type const &a) { return a + b; }

// sub operators
ats_vector operator-(const ats_vector& a) { ats_vector r; r.reserve(a.size()); for (auto const&ts : a) r.push_back(-ts); return r; }

ats_vector operator-(ats_vector const &a, double b) { ats_vector r; r.reserve(a.size()); for (auto const&ts : a) r.push_back(ts - b); return r; }
ats_vector operator-(double a, ats_vector const &b) { ats_vector r; r.reserve(b.size()); for (auto const&ts : b) r.push_back(a - ts); return r; }
ats_vector operator-(ats_vector const &a, ats_vector const& b) {
    if (a.size()==0 && b.size()!=0) {
        ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back( - b[i]);
        return r;
    }
    if (a.size()!=0 && b.size()==0)
        return a;

    if (a.size() != b.size()) throw runtime_error(string("ts-vector sub require same sizes: lhs.size=") + std::to_string(a.size()) + string(",rhs.size=") + std::to_string(b.size()));
    ats_vector r; r.reserve(a.size()); for (size_t i = 0; i < a.size(); ++i) r.push_back(a[i] - b[i]);
    return r;
}
ats_vector operator-(ats_vector::value_type const &a, ats_vector const& b) { ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back(a - b[i]); return r; }
ats_vector operator-(ats_vector const& b, ats_vector::value_type const &a) { ats_vector r; r.reserve(b.size()); for (size_t i = 0; i < b.size(); ++i) r.push_back(b[i] - a); return r; }

// max/min operators
ats_vector ats_vector::min(ats_vector const& x) const {
    if (size() != x.size()) throw runtime_error(string("ts-vector min require same sizes: lhs.size=") + std::to_string(size()) + string(",rhs.size=") + std::to_string(x.size()));
    ats_vector r; r.reserve(size()); for (size_t i = 0; i < size(); ++i) r.push_back((*this)[i].min(x[i]));
    return r;
}
ats_vector ats_vector::max(ats_vector const& x) const {
    if (size() != x.size()) throw runtime_error(string("ts-vector max require same sizes: lhs.size=") + std::to_string(size()) + string(",rhs.size=") + std::to_string(x.size()));
    ats_vector r; r.reserve(size()); for (size_t i = 0; i < size(); ++i) r.push_back((*this)[i].max(x[i]));
    return r;
}

ats_vector ats_vector::pow(ats_vector const& x) const {
    if (size() != x.size()) throw runtime_error(string("ts-vector pow require same sizes: lhs.size=") + std::to_string(size()) + string(",rhs.size=") + std::to_string(x.size()));
    ats_vector r; r.reserve(size()); for (size_t i = 0; i < size(); ++i) r.push_back((*this)[i].pow(x[i]));
    return r;
}

ats_vector ats_vector::inside(double min_v,double max_v,double nan_v, double inside_v, double outside_v) const {
    ats_vector r; r.reserve(size()); for (size_t i = 0; i < size(); ++i) r.push_back((*this)[i].inside(min_v,max_v,nan_v,inside_v,outside_v));
    return r;
}

ats_vector ats_vector::transform_spline(const vector<double>& knots, const vector<double>& coeff, size_t degree) const {
    ats_vector r; r.reserve(size()); for (size_t i = 0; i < size(); ++i) r.push_back((*this)[i].transform_spline(knots, coeff, degree));
    return r;
}

ats_vector ats_vector::transform(const xy_point_curve& xy, interpolation_scheme scheme) const {
    spline_parameter p = spline_interpolator::interpolate(xy, scheme);
    ats_vector r;
    r.reserve(size());
    for (size_t i = 0; i < size(); ++i)
        r.push_back((*this)[i].transform_spline(p));
    return r;
}

ats_vector min(ats_vector const &a, double b) { return a.min(b); }
ats_vector min(double b, ats_vector const &a) { return a.min(b); }
ats_vector min(ats_vector const &a, apoint_ts const& b) { return a.min(b); }
ats_vector min(apoint_ts const &b, ats_vector const& a) { return a.min(b); }
ats_vector min(ats_vector const &a, ats_vector const &b) { return a.min(b); }

ats_vector max(ats_vector const &a, double b) { return a.max(b); }
ats_vector max(double b, ats_vector const &a) { return a.max(b); }
ats_vector max(ats_vector const &a, apoint_ts const & b) { return a.max(b); }
ats_vector max(apoint_ts const &b, ats_vector const &a) { return a.max(b); }
ats_vector max(ats_vector const &a, ats_vector const & b) { return a.max(b); }

ats_vector pow(ats_vector const &a, double b) { return a.pow(b); }
ats_vector pow(double b, ats_vector const &a) {
    ats_vector r;
    r.reserve(a.size());
    for(const auto&ts:a)
        r.push_back(pow(b,ts));
    return r;
}
ats_vector pow(ats_vector const &a, apoint_ts const & b) { return a.pow(b); }
ats_vector pow(apoint_ts const &b, ats_vector const &a) {
    ats_vector r;
    r.reserve(a.size());
    for(const auto&ts:a)
        r.push_back(pow(b,ts));
    return r;
}
ats_vector pow(ats_vector const &a, ats_vector const & b) { return a.pow(b); }

ats_vector log(ats_vector const & a) { return a.log(); }
bool ats_vector::operator==(ats_vector const&o ) const {
    if(size() ==0 && o.size()==0) return true;
    if(size() != o.size()) return false;
    for(size_t i=0;i<size();++i) {
        if (  (*this)[i] != o[i] )
            return false;
    }
    return true;
}
apoint_ts  ats_vector::forecast_merge(utctimespan lead_time, utctimespan fc_interval) const {
    //verify arguments
    if (lead_time.count() < 0)
        throw runtime_error("lead_time parameter should be 0 or a positive number giving number of seconds into each forecast to start the merge slice");
    if (fc_interval.count() <= 0)
        throw runtime_error("fc_interval parameter should be positive number giving number of seconds between first time point in each of the supplied forecast");
    return apoint_ts(std::make_shared<const anary_op_ts>(*this, nary_op_t::OP_MERGE, lead_time, fc_interval));

}

apoint_ts ats_vector::sum() const {
    return apoint_ts(std::make_shared<const anary_op_ts>(*this, nary_op_t::OP_ADD));            
}

double ats_vector::nash_sutcliffe(apoint_ts const &obs, utctimespan t0_offset, utctimespan dt, int n) const {
    if (n < 0)
        throw runtime_error("n, number of intervals, must be specified as > 0");
    if (dt <= utctimespan{0})
        throw runtime_error("dt, average interval, must be specified as > 0 s");
    if (t0_offset < utctimespan{0})
        throw runtime_error("lead_time,t0_offset,must be specified  >= 0 s");
    return time_series::nash_sutcliffe(*this, obs, t0_offset, dt, (size_t)n);
}

ats_vector ats_vector::average_slice(utctimespan t0_offset, utctimespan dt, int n) const {
    if (n < 0)
        throw runtime_error("n, number of intervals, must be specified as > 0");
    if (dt <= utctimespan{0})
        throw runtime_error("dt, average interval, must be specified as > 0 s");
    if (t0_offset < utctimespan{0})
        throw runtime_error("lead_time,t0_offset,must be specified  >= 0 s");
    ats_vector r;
    for (size_t i = 0; i < size(); ++i) {
        auto const& ts = (*this)[i];
        if (ts.size()) {
            gta_t ta(ts.time_axis().time(0) + t0_offset, dt, n);
            r.push_back((*this)[i].average(ta));
        } else {
            r.push_back(ts);
        }
    }
    return r;
}
/** @see shyft::qm::quantile_map_forecast */
ats_vector quantile_map_forecast(vector<ats_vector> const & forecast_sets,
    vector<double> const& set_weights,
    ats_vector const& historical_data,
    gta_t const&time_axis,
    utctime interpolation_start,
    utctime interpolation_end,
    bool interpolated_quantiles
) {
    // since this is scripting access, verify all parameters here
    if (forecast_sets.size() < 1)
        throw runtime_error("forecast_set must contain at least one forecast");
    if (historical_data.size() < 2)
        throw runtime_error("historical_data should have more than one time-series");
    if (forecast_sets.size() != set_weights.size())
        throw runtime_error(string("The size of weights (") + to_string(set_weights.size()) + string("), must match number of forecast-sets (") + to_string(forecast_sets.size()) + string(""));
    if (time_axis.size() == 0)
        throw runtime_error("time-axis should have at least one step");
    if (core::is_valid(interpolation_start)) {
        if (!time_axis.total_period().contains(interpolation_start)) {
            calendar utc;
            auto ts = utc.to_string(interpolation_start);
            auto ps = utc.to_string(time_axis.total_period());
            throw runtime_error("interpolation_start " + ts + " is not within time_axis period " + ps);
        }
        if (core::is_valid(interpolation_end) && !time_axis.total_period().contains(interpolation_end)) {
            calendar utc;
            auto ts = utc.to_string(interpolation_end);
            auto ps = utc.to_string(time_axis.total_period());
            throw runtime_error("interpolation_end " + ts + " is not within time_axis period " + ps);
        }
    }
    return qm::quantile_map_forecast<time_series::average_accessor<apoint_ts, gta_t> >(forecast_sets, set_weights, historical_data, time_axis, interpolation_start, interpolation_end, interpolated_quantiles);

}

apoint_ts extend(
    const apoint_ts & lhs_ts,
    const apoint_ts & rhs_ts,
    extend_ts_split_policy split_policy, extend_ts_fill_policy fill_policy,
    utctime split_at, double fill_value
) {
    return apoint_ts(std::make_shared<const extend_ts>(
        lhs_ts, rhs_ts,
        split_policy, fill_policy,
        split_at, fill_value
        ));
}

std::vector<double> extend_ts::values() const {
    bind_check();

    const utctime split_at = get_split_at();
    const auto lhs_p = lhs.time_axis().total_period();
    const auto rhs_p = rhs.time_axis().total_period();

    // get values
    std::vector<double> lhs_values{}, rhs_values{};
    if (split_at >= lhs_p.start)  lhs_values = lhs.values();
    if (split_at <= rhs_p.end)   rhs_values = rhs.values();

    // possibly to long, but not too short, all values default to nan
    std::vector<double> result;
    result.reserve(ta.size());// exact size

    auto res_oit = std::back_inserter(result);  // output iterator

    // values from the lhs
    // we  should at least include all values strictly left of split_at
    if (split_at >= lhs_p.end) {  // use all of lhs
        res_oit = std::copy(lhs_values.begin(), lhs_values.end(), res_oit);
        // if rhs starts _after_ split_at, we append a fill value.. given there are something to the left. ref issue #876
        if(lhs_p.end < max(split_at,rhs_p.start) && lhs_values.size()) {
            double fill_v=nan;
            switch (ets_fill_p) {
                case EPF_NAN:  fill_v=nan;break;
                case EPF_FILL: fill_v=fill_value;break;
                case EPF_LAST: fill_v= lhs_values.back();break;// ref above. lhs.values size>0
            }
            *res_oit++ = fill_v;
        }
    } else if (split_at >= lhs_p.start) {  // split inside lhs
        size_t lhs_i = lhs.time_axis().index_of(split_at);
        auto lhs_end_it = lhs_values.begin();
        if(lhs.time_axis().time(lhs_i) < rhs_p.start)
            lhs_i++; // inside a, filler is a
        std::advance(lhs_end_it, lhs_i);
        res_oit = std::copy(lhs_values.begin(), lhs_end_it, res_oit);
    }


    // values from the rhs
    if (split_at <= rhs_p.start) {  // use all of rhs
        std::copy(rhs_values.begin(), rhs_values.end(), res_oit);
    } else if (split_at < rhs_p.end) {  // split inside rhs
        size_t rhs_i = this->rhs.time_axis().index_of(split_at);
        auto rhs_start_it = rhs_values.begin();
        std::advance(rhs_start_it, rhs_i);
        std::copy(rhs_start_it, rhs_values.end(), res_oit);
    }

    return result;
}

double extend_ts::value_at(utctime t) const {
    // bind_check();  // done in time_axis()
    if (!time_axis().total_period().contains(t)) {
        return nan;
    }
    utctime split_at = get_split_at();
    if (t < split_at) {  // lhs
        if (lhs.time_axis().total_period().contains(t)) {
            if(!lhs.time_axis().total_period().contains(split_at))
                return lhs(t);
            //-- a bit hard to get: we do not split exactly at t, we use split_at to find the interval in lhs where rhs should contribute,
            auto lhs_split_ix=lhs.time_axis().index_of(split_at);
            auto t_step_split=lhs.time_axis().time(lhs_split_ix);
            if(t < t_step_split) { // ok, we are to the left of the real split, so just return lhs(t)
                return lhs(t);
            } else { // we are on th left of split, but inside the split period that should have rhs-contrib
                if (rhs.time_axis().total_period().contains(t)) {
                    return rhs(t);// rhs do have contrib
                } else {
                    // ok, rhs can't give anything, but we are inside lhs, so use lhs value.
                    return lhs(t);
                }
            }
        } else {
            // greater than lhs.end -> use policy
            switch (ets_fill_p) {
            default:
            case EPF_NAN:  return nan;
            case EPF_FILL: return fill_value;
            case EPF_LAST: return lhs.size()>0?lhs.value(lhs.size() - 1):nan;
            }
        }
    } else {  // rhs
        if (rhs.time_axis().total_period().contains(t)) {
            return rhs(t);
        } else {
            // less than rhs.start -> use policy
            switch (this->ets_fill_p) {
            default:
            case EPF_NAN:  return nan;
            case EPF_FILL: return fill_value;
            case EPF_LAST: return lhs.time_axis().total_period().contains(t)? lhs(t):(lhs.size()>0?lhs.value(lhs.size()-1):shyft::nan);// use *last* value from lhs(t), or keep lhs.last value!
            }
        }
    }
}

double extend_ts::value(size_t i) const {
    //this->bind_check();  // done in value_at()
    if (i == std::string::npos || i >= time_axis().size()) {
        return nan;
    }
    return value_at(time_axis().time(i));
}

//--
// fx_time_axis_ts impl
//
vector<double> use_time_axis_from_ts::values() const {
    bind_check();
    if(lhs.time_axis()==ta) { // optimize away trivial case
        return lhs.values();
    } else {
        vector<double> r;r.reserve(ta.size());
        for(size_t i=0;i<ta.size();++i)
            r.push_back(lhs.ts->value_at(ta.time(i)));
        return r;
    }
}

double use_time_axis_from_ts::value_at(utctime t) const {
    //bind_check();  // done in time_axis()
    if (!time_axis().total_period().contains(t)) {
        return nan;// time-axis is from rhs, so we need this check.
    }
    return lhs.ts->value_at(t);//sih: could be wrong if lhs is linear, and there are several points in between.. hmm!
}

double use_time_axis_from_ts::value(size_t i) const {
    if (i == std::string::npos || i >= time_axis().size()) {
        return nan;
    }
    return value_at(time_axis().time(i));
}

apoint_ts apoint_ts::use_time_axis_from(const apoint_ts&o) const {
    return apoint_ts{make_shared<const use_time_axis_from_ts>(*this,o)};
}

/** QAC stuff  */
apoint_ts apoint_ts::quality_and_self_correction(qac_parameter const&p) const {
    return apoint_ts{make_shared<const qac_ts>(*this,p)};
}
apoint_ts apoint_ts::quality_and_ts_correction(qac_parameter const&p,const apoint_ts& cts) const {
    return apoint_ts{make_shared<const qac_ts>(*this,p,cts)};
}

qac_ts::qac_ts(const apoint_ts& ats):ts(ats.ts) {do_early_bind();}
qac_ts::qac_ts(apoint_ts&& ats):ts(move(ats.ts)) {do_early_bind();}
qac_ts::qac_ts(const apoint_ts& ats, const qac_parameter& qp,const apoint_ts& cts):ts(ats.ts),cts(cts.ts),p(qp) {do_early_bind();}
qac_ts::qac_ts(const apoint_ts& ats, const qac_parameter& qp):ts(ats.ts),p(qp) {do_early_bind();}
            // methods for binding and symbolic ts
bool qac_ts::needs_bind() const {
    if(bound)
        return false;
    return ts->needs_bind() || (cts && cts->needs_bind());
}
void qac_ts::do_bind() {
    if(ts && !bound) {
        dref(ts).do_bind();
        if(cts)
            dref(cts).do_bind();
        local_do_bind();
    }
}
void qac_ts::do_unbind() {
    if(bound) {
        dref(ts).do_unbind();
        if(cts)
            dref(cts).do_unbind();
        local_do_unbind();
    }
}
void qac_ts::local_do_unbind() {
    if(bound) {
        bound=false;
    }
}
void qac_ts::local_do_bind() {
    if(!bound && ts) {
        // if dt_max < all gaps, use ts->time_axis()
        // else run through,
        ta=ts->time_axis();// copy it
        if(p.max_timespan!=utctimespan{0} && p.max_timespan<ts->time_axis().total_period().timespan()) {
            //-- ok we have to scan the time-axis to see if there
            //   is a gap >max_timespan,
            //   if yes, we have to build a new time-axis
            //   where we insert points at ti+ x
            //   where x is epsilon_time for linear and max_timespan for stair-case
            size_t n_patches=0;
            for(size_t i=0;i<ta.size();++i) {
                if(ta.period(i).timespan()>p.max_timespan) {//TODO: verify > or  >= here
                    ++n_patches;
                }
            }
            if(n_patches) {
                vector<utctime> tp;tp.reserve(ta.size()+n_patches);// ensure we have one alloc
                auto x= ts->point_interpretation()==POINT_AVERAGE_VALUE?p.max_timespan:utctime{1};// linear->epsion, stair=max_timespan
                for(size_t i=0;i<ta.size();++i) {
                    tp.push_back(ta.time(i));
                    if(ta.period(i).timespan()>p.max_timespan) {
                        tp.push_back(ta.time(i)+x);
                    }
                }
                ta=gta_t(move(tp),ta.total_period().end);
            }
        }
        bound=true;
    }
}

double qac_ts::_fill_value(size_t i) const {
    auto t = ta.time(i);
    if (cts) // if user have made all the effort of providing a cts, use that
        return cts->value_at(t); // we do not check this value, assume ok!(could to that, and fallback to filler/linear)
    // try linear|stair-case interpolation between previous--next *valid* value
    if(p.max_timespan == utctimespan{0})
        return p.constant_filler;// interpolation is disabled, return constant_filler, whatever value that is
    const size_t n = ts->size();
    if(n!=ta.size()) { // the result time-axis have injected nan-points due to p.max_timespan
        i=ts->time_axis().index_of(t);// so remap to the original ts indexing order.
    }
    auto const pfx=point_interpretation();
    if (i == 0 ||((pfx==ts_point_fx::POINT_INSTANT_VALUE) && i + 1 >= n ))
        return shyft::nan; // lack possible previous.. next value-> nan, or
    size_t j = i;
    while (j--) { // find previous *valid* point
        utctime t0 = ts->time(j);
        if (t - t0 > p.max_timespan)
            return shyft::nan;// exceed configured max timespan,->nan
        double x0 = ts->value(j);//
        if (qac::is_ok_quality(p,x0)) { // got a previous point
            // here we are at a point where t0,x0 is valid ,(or at the beginning)
            if(pfx== ts_point_fx::POINT_AVERAGE_VALUE)
                return x0;// OK! time-series is stair-case, and we found a previous ok value, ->keep it!
            for (size_t k = i + 1; k < n; ++k) { // then find next ok point
                utctime t1 = ts->time(k);
                if (t1 - t0 > p.max_timespan)
                    return shyft::nan;// exceed configured max time span ->nan
                double x1 = ts->value(k);//
                if (qac::is_ok_quality(p,x1))  // got a next  point
                    return qac::linear_fx(t,t0,x0,t1,x1);
            }
        }
    }
    return shyft::nan; // if we reach here, we failed to find substitute
}
double qac_ts::value(size_t i) const {
    return value_at(time(i));
}
double qac_ts::_value(size_t i) const {
    // ok.. i is relative to the new time-axis..
    size_t i_ta=i;//keep original i since we will use fill-value from that
    if(ta.size()!=ts->time_axis().size()) {// so we need to translate i from ta.relative to ts->..
        i =ts->time_axis().index_of(ta.time(i)); //figure out the index of src ts time-axis.
        if(ts->time_axis().time(i) != ta.time(i_ta))
            return shyft::nan;// because this is a nan-insert point, to large gap.
    }

    double x = ts->value(i);
    if (qac::is_ok_quality(p,x)){
        if(i==0 || p.repeat_timespan.count()==0)
            return x;
        if(!qac::is_repeated_once(ts,i,x,p))
            return x;
        auto l=qac::find_last_valid_repeat(ts,i,x,p);
        if(l==i) // i is within the valid repeated sequence
            return x;
        //-- value is not ok, we have to replace it if possible
        if(cts)
            return cts->value_at(ts->time(i));
        if(p.max_timespan.count()==0)
            return p.constant_filler;
        // interpolate or extend based on own values
        // l is the index/value to the left,
        //
        if(point_interpretation()==ts_point_fx::POINT_AVERAGE_VALUE)
            return shyft::nan;

        auto t0=ts->time(l);
        size_t r= qac::find_first_ok_value_right(ts,i,x,t0,p);//also stretch as far as possible away from repeated
        if(r==i)
            return shyft::nan;
        return qac::linear_fx(ts->time(i),t0,ts->value(l),ts->time(r),ts->value(r));
    } else {
        //-- value is not ok, we have to replace it if possible
        if(cts)
            return cts->value_at(ts->time(i));
        if(p.max_timespan.count()==0)
            return p.constant_filler;
        if(i==0)
            return shyft::nan;// can't fix
        const size_t n= ts->size();
        const auto linear =point_interpretation()==ts_point_fx::POINT_INSTANT_VALUE;
        if(linear && i+1==n)
            return shyft::nan;// also can't fix.

        // hard work, replace by interpolate/extend it self:
        // if stair case, only left is needed,
        // but the left could be a sequence of repeated values,
        // so we need the rightmost value of those.
        if(p.repeat_timespan.count()==0) {
            return _fill_value(i_ta);
        } else {
            // left:find  last ok value in a possibly repeated sequence
            size_t l=qac::find_left_ok_value(ts,i,p); // take possible repeated sequence into account
            if(l==i)
                return shyft::nan;
            if(!linear) {
                double vl=ts->value(l);
                if(l>0) { // check that l is not a repeated value, because then we can not extend!
                    if(qac::is_repeated_once(ts,i,vl,p))
                        return shyft::nan;
                }
                return (ts->time(i)-ts->time(l))>p.max_timespan?shyft::nan:vl;
            }
            auto t0=ts->time(l);
            size_t r=qac::find_first_ok_value_right_no_repeat(ts,i,t0,p);
            auto t1=ts->time(r);
            if(r==i || (t1-t0)>p.max_timespan)
                return shyft::nan;
            return qac::linear_fx(ts->time(i),t0,ts->value(l),t1,ts->value(r));
        }
    }
}

double qac_ts::value_at(utctime t) const {
    size_t i = index_of(t);
    if (i == string::npos)
        return shyft::nan;
    double x0 = _value(i);
    if(!isfinite(x0))
        return x0;// no hope for repair,it's nan, return early
    utctime t0 = time(i);
    if (ts->point_interpretation() == ts_point_fx::POINT_AVERAGE_VALUE) {
        if(p.max_timespan.count() && (t-t0)>p.max_timespan) // stretching too long ?
            return shyft::nan;
        return x0;
    }
    //
    if (t0 == t ) {// value at point is exactly the point-value, but it is a dilemma for linear between points.
        // because this will return value for the singular point
        // given that there is no lhs to t, and no rhs to t,
        // then one definition could say that it should return nan
        // that raises another dilemma:
        //  value(i) -> the value stored at i't point (ok, because that's what iẗ́'s supposed to do)
        //  value_at(time(i)) -> fx(t) could be nan, because it lacks both rhs and lhs.(could surprise some).
        return x0;// just return x0, singular or not..
    }

    // linear interpolation between points(and we are a + eps right of t0)
    if (i + 1 >= size())
        return shyft::nan;// no next point, ->nan
    double x1 = _value(i + 1);
    utctime t1 = ta.time(i + 1);
    if (!isfinite(x1)) { // no value to the right
        if(p.max_timespan.count() && ((t-t0)>p.max_timespan)) // ! if to long linear span fail it..
            return shyft::nan;
        return x0;//no timespan limit, extend flat out to the nan-point, from ts source, or inserted as max_timespan stretch failure
    }
    
    if(p.max_timespan.count() && ((t1-t0)>p.max_timespan)) // ! if to long linear span fail it..
        return shyft::nan;
    return qac::linear_fx(t,t0,x0,t1,x1); // otherwise interpolation
}

vector<double> qac_ts::values() const {
    assert_bound();
    vector<double> r;
    if(!ta.size())
        return r;

    //-- here we need speed
    //-- but also correctness regarding time-axis
    if(ta.size()!=ts->time_axis().size()) {// in this case p.max_timespan have injected more point to the time-axis
        //-- so we need to repeat this for the values
        r.reserve(ta.size());// this is what we need in space
        auto rx=ts->values();// take acopy, so we can iterate fast while copy
        auto const & ts_ta=ts->time_axis();
        // if linear ts, we have to watch out for singular points
        //   like nan_a value nan_b , where a and b are injected due to to large gaps.
        //   in this case, we should 'nan' the singular point as well.
        //bool linear=point_interpretation()== POINT_INSTANT_VALUE;
        //bool singular;
        for(size_t i=0;i<rx.size();++i) {
            r.push_back(rx[i]);
            if(ts_ta.period(i).timespan()>p.max_timespan) {
                //if(singular)// breaking change if removed,singular will remain
                //    r.back()=shyft::nan;//linear, -ensure to kill singular value
                r.push_back(shyft::nan);// inject nan value
                //singular=linear;// only linear series can have singularities that we want to kill, so stair-case gives nooop here
            } else {
                //singular=false;
            }
        }
    } else {
        r=ts->values();
    }
    if(point_interpretation()==ts_point_fx::POINT_AVERAGE_VALUE) { // single scan and fix
        if(cts) {
            if(cts->point_interpretation()==ts_point_fx::POINT_AVERAGE_VALUE) {
                // optimize cts computations, using ix-hint for lookup, avoid bin-search
                auto ix=cts->index_of(ta.time(0));// get the initial hint
                auto const& cta=cts->time_axis();// grab the cts ta
                qac::fill_fx_for_bad_values(ta,r,p,
                    [this,&ix,&cta](size_t,size_t i)->double {
                        ix=cta.index_of(ta.time(i), ix);// returns back new ix-hint
                        return ix!=string::npos?cts->value(ix):shyft::nan;
                    }
                );
            } else {
                // TODO: optimizer, in this case speculative cts, would have to do aX +b
                // remembering ix, as ix-hint, and use ix.. ix+1 to compute a,b
                //
                qac::fill_fx_for_bad_values(ta,r,p,[this](size_t,size_t i)->double {return cts->value_at(ta.time(i));});
            }
        } else if(p.max_timespan.count()==0) {
            qac::fill_fx_for_bad_values(ta,r,p,[this](size_t,size_t)->double {return p.constant_filler;});
        } else {
            qac::fill_fx_for_bad_values(ta,r,p,[this,&r](size_t l,size_t i)->double {return (l==string::npos || (ta.time(i)-ta.time(l)>=p.max_timespan))?shyft::nan:r[l];});
        }
    } else { // two-pass, scan and set nan's, then fixup linear
        qac::fill_fx_for_bad_values(ta,r,p,[](size_t,size_t)->double {return shyft::nan;});
        if(cts) {
            if(cts->point_interpretation()==ts_point_fx::POINT_AVERAGE_VALUE) {
                auto ix=cts->index_of(ta.time(0));// get the initial hint
                auto const& cta=cts->time_axis();// grab the cts ta
                for(size_t i=0;i<ta.size();++i) {
                    if(!isfinite(r[i])) {
                        ix=cta.index_of(ta.time(i),ix);
                        r[i]= ix!=string::npos? cts->value(ix) : shyft::nan;
                    }
                }
            } else {
                //TODO: optimize: ref above, do smart lookup of cts, recompute aX + b only when needed etc.
                qac::fill_ts_for_missing_values(ta,r,cts);
            }

        } else if(p.max_timespan.count()==0) {
            qac::fill_constant_for_missing_values(r,p.constant_filler);
        } else {
            qac::fill_linear_for_missing_values(ta,r,p);
        }
    }
    return r;
}

double inside_ts::value(size_t i) const {
    return p.inside_value(ts->value(i));
}

double inside_ts::value_at(utctime t) const {
    size_t i = index_of(t);
    if (i == string::npos)
        return shyft::nan;
    return value(i);
}

vector<double> inside_ts::values() const {
    vector<double> r{ts->values()};
    for(auto&x:r) x=p.inside_value(x);
    return r;
}

spline_parameter::spline_parameter(const vector<double>& knots, const vector<double>& coeff, size_t degree)
    : knots(knots), coeff(coeff), degree(degree) {
    // Check that vector sizes are matching
    if (knots.size() != coeff.size() + degree + 1)
        throw runtime_error("Invalid knot and coefficiecient vectors");

    // Check that there is at least one open knot span in the knot-vector
    auto nonempty = [] (const auto& x0, const auto& x1) -> bool { return x0 != x1; };
    first = std::distance(knots.begin(), std::adjacent_find(knots.begin(), knots.end(), nonempty));
    last = knots.size() - 2 - std::distance(knots.rbegin(), std::adjacent_find(knots.rbegin(), knots.rend(), nonempty));
    if (first > last)
        throw runtime_error("At least one knot span must be non-empty");
}


vector<double>::difference_type spline_parameter::get_interval(double x) const {
    auto m = std::distance(knots.begin(), std::find_if(knots.begin(), knots.end(), [&x](const auto& x0) -> bool { return x0 > x; })) - 1;
    return std::clamp(m, first, last);
}

template<>
spline_parameter spline_interpolator::interpolate<spline_interpolator::method::LINEAR>(const xy_point_curve& pc, spline_interpolator::method::LINEAR) {
    int degree = 1;
    vector<double> knots;
    vector<double> coeff;

    knots.reserve(pc.points.size() + 2);
    coeff.reserve(pc.points.size());

    // First and last knots are repeated
    knots.push_back(pc.points.front()[0]);
    for (const auto& p: pc.points) {
        knots.push_back(p[0]);
        coeff.push_back(p[1]);
    }
    knots.push_back(pc.points.back()[0]);

    return spline_parameter(knots, coeff, degree);
}

template<>
spline_parameter spline_interpolator::interpolate<spline_interpolator::method::CATMULL_ROM>(const xy_point_curve& pc, spline_interpolator::method::CATMULL_ROM) {
    int degree = 3;
    const auto& pts = pc.points;
    const auto m = pts.size();

    vector<double> knots;
    vector<double> coeff;

    knots.reserve(pts.size() * 2 + 2);
    knots.push_back(pts.front()[0]);
    knots.push_back(pts.front()[0]);
    for (const auto& p: pts) {
        // Internal knots are repeated once
        knots.push_back(p[0]);
        knots.push_back(p[0]);
    }
    knots.push_back(pts.back()[0]);
    knots.push_back(pts.back()[0]);

    // Helper functions for computing approximate derivative
    auto delta_x = [&pts, &m] (size_t i) { return (0 < i && i < m) ? pts[i][0] - pts[i-1][0] : 0.0; };
    auto delta_y = [&pts, &m] (size_t i) { return (0 < i && i < m) ? pts[i][1] - pts[i-1][1] : 0.0; };
    auto dydx = [&m, &delta_x, &delta_y] (size_t i) -> double {
        if (i == 0)
            return delta_y(1) / delta_x(1);
        if (i == m-1)
            return  delta_y(m-1) / delta_x(m-1);
        return 0.5 * (delta_y(i)/delta_x(i) + delta_y(i+1)/delta_x(i+1));
    };

    for (size_t i = 0; i < m; ++i) {
        coeff.push_back(pts[i][1] - dydx(i)/3 * delta_x(i));
        coeff.push_back(pts[i][1] + dydx(i)/3 * delta_x(i+1));
    }

    return spline_parameter(knots, coeff, degree);
}

template<>
spline_parameter spline_interpolator::interpolate<spline_interpolator::method::POLYNOMIAL>(const xy_point_curve& pc, spline_interpolator::method::POLYNOMIAL) {
    const auto& pts = pc.points;
    const auto m = pts.size();
    const auto& a = pts.front()[0];
    const auto& b = pts.back()[0];

    auto degree = int(m - 1);

    vector<double> knots;
    vector<double> coeff;

    // single open knot span
    for (auto i = 0; i < degree + 1; ++i)
        knots.push_back(a);
    for (auto i = 0; i < degree + 1; ++i)
        knots.push_back(b);

    // Function for evaluation Bernstein basis on the interval [a, b]
    auto basis = [&degree, &a, &b] (int k, double x) -> double {
        // Compute number of permutations
        int r = (2*k < degree) ? k : degree - k;
        long ncr = 1;
        for (int i = 1; i <= r; ++i) {
            ncr *= degree - i + 1;
            ncr /= i;
        }

        auto t = std::max((x-a) / (b-a), 0.0);
        return ncr * std::pow(1.0 - t, degree - k) * std::pow(t, k);
    };

    // Formulate the equation system
    for (const auto& p: pts)
        coeff.push_back(p[1]);

    vector<double> mat;
    mat.reserve(m * m);
    for (size_t i = 0; i < m; ++i)
        for (size_t j = 0; j < m; ++j)
            mat[m*i + j] = basis(j, pc.points[i][0]);

    // forward elimination
    for (size_t k = 1; k < m-1; ++k) {
        for (size_t i = k + 1; i < m-1; ++i) {
            auto c = mat[m*i + k] / mat[m*k + k];
            for (size_t j = k+1; j < m; ++j)
                mat[m*i + j] -= c * mat[m*k + j];
            coeff[i] -= c * coeff[k];
        }
    }
    // backwards substitution
    for (int i = m - 2; i > 0; --i) {
        for (size_t j = i+1; j < m; ++j)
            coeff[i] -= mat[m*i + j] * coeff[j];
        coeff[i] /= mat[m*i + i];
    }

    return spline_parameter(knots, coeff, degree);
}

// Implementation selection; must after specialising implementation templates
spline_parameter spline_interpolator::interpolate(const xy_point_curve& xy, interpolation_scheme scheme) {
    switch (scheme) {
        case interpolation_scheme::SCHEME_POLYNOMIAL:
            return interpolate(xy, method::POLYNOMIAL());
        case interpolation_scheme::SCHEME_LINEAR:
            return interpolate(xy, method::LINEAR());
        case interpolation_scheme::SCHEME_CATMULL_ROM:
            return interpolate(xy, method::CATMULL_ROM());
    }
    throw std::runtime_error("Valid methods are {'polynomial', 'linear', 'catmull-rom'}.");
}

static double spline_ts_value(spline_parameter const&p, double x) {
    if(!isfinite(x))
        return shyft::nan;

    auto m = p.get_interval(x);
    // De Boor algorithm
    vector<double> c(p.degree + 1);
    for (size_t j = 0; j < p.degree + 1; ++j) {
        c[j] = p.coeff[m + j - p.degree];
    }

    for (size_t k = 1; k <= p.degree; ++k) {
        for (size_t j = p.degree; j >= k; --j) {
            double a = (x - p.knots[m + j - p.degree]) / (p.knots[m + j + 1 - k] - p.knots[m + j - p.degree]);
            c[j] = (1.0 - a) * c[j-1] + a * c[j];
        }
    }
    return c[p.degree];
}

double transform_spline_ts::value(size_t i) const {
    assert_ts();
    return spline_ts_value(p,ts->value(i));
}

double transform_spline_ts::value_at(utctime t) const {
    assert_ts();
    return spline_ts_value(p,ts->value_at(t));
}

vector<double> transform_spline_ts::values() const {
    assert_ts();
    vector<double> r {ts->values()};
    vector<double> c(p.degree + 1);
    // Using  de Boor algorithm to compute values
    for (auto& x : r) {
        auto m = p.get_interval(x);
        for (size_t j = 0; j < p.degree + 1; ++j)
            c[j] = p.coeff[m + j - p.degree];

        // loop of over basis degrees
        for (size_t k = 1; k <= p.degree; ++k) {
            for (size_t j = p.degree; j >= k; --j) {
                double a = (x - p.knots[m + j - p.degree]) / (p.knots[m + j + 1 - k] - p.knots[m + j - p.degree]);
                c[j] = (1.0 - a) * c[j-1] + a * c[j];
            }
        }
        x = c[p.degree];
    }
    return r;
}

double decode_ts::value(size_t i) const {
    return p.decode(ts->value(i));
}

double decode_ts::value_at(utctime t) const {
    size_t i = index_of(t);
    if (i == string::npos)
        return shyft::nan;
    return value(i);
}

vector<double> decode_ts::values() const {
    vector<double> r{ts->values()};
    for(auto&x:r) x=p.decode(x);
    return r;
}

apoint_ts clip_to_period(apoint_ts const& ts, utcperiod p) {
    //sanity checks:
    if(ts.size()==0)
        return ts;

    auto ts_tp=ts.total_period();
    if(p.contains(ts_tp))
        return ts; //easy, nothing to clip
    if(!ts_tp.overlaps(p))
        return apoint_ts(gta_t{},shyft::nan,ts.point_interpretation());//easy!
    // it's overlap, and we have to take a serious look into it.
    auto const& ta=ts.time_axis();
    size_t ix_hint=0;
    auto ix_left=ta.open_range_index_of(p.start,ix_hint);// get left ix, less than p.start
    if(ix_left==string::npos) ix_left=0;// possible scenario, then we start at i0
    auto ix_right=ta.open_range_index_of(p.end,ix_hint);
    assert(ix_right != string::npos);// pr.def. we should **always** have ix_right if it's overlap.
    auto t_right=ta.time(ix_right);
    if(ts.point_interpretation()==ts_point_fx::POINT_INSTANT_VALUE) {// linear between point
        if(t_right < p.end && ix_right+1<ta.size())// tricky end-definition, we need point to the right
            ++ix_right;// add end point, so we can do f(t) when t in range [p.start..p.end>
    } else if(t_right==p.end && ix_right>ix_left) {
        --ix_right;//drop off this point, since we do not need it.
    }

    return ts.slice(ix_left,1u+ix_right-ix_left);
}

ats_vector clip_to_period(ats_vector const& tsv, utcperiod p) {
    ats_vector r;r.reserve(tsv.size());
    for(auto const &ts:tsv)
        r.emplace_back(clip_to_period(ts,p));
    return r;
}
apoint_ts apoint_ts::evaluate() const {
    if(!ts) return apoint_ts{};
    if(ts->needs_bind())
        throw runtime_error("This time-series expression contains unbound time-series, please resolve before evaluate");
    if(ts_as<gpoint_ts>(ts))
        return *this;
    eval_ctx c;
    ts->prepare(c);
    return apoint_ts(c.evaluate(ts));
}

vector<double> apoint_ts::values()const {
    if(!ts)
        return vector<double>{};
    if(ts->needs_bind())
        throw runtime_error("This time-series expression contains unbound time-series, please resolve before evaluate");
    if(auto gts=ts_as<gpoint_ts>(ts))
        return gts->values();
    else if(auto rts=ts_as<aref_ts>(ts))
        return rts->rep->values();
    eval_ctx c;
    ts->prepare(c);
    auto ets=c.evaluate(ts);
    return ets->values();
}

apoint_ts apoint_ts::bucket_to_hourly(int start_hour_utc,double bucket_empty_limit) const {
    if( start_hour_utc<0 || start_hour_utc>23)
        throw runtime_error("start_hour_utc must be in range [0..23]");
    if(bucket_empty_limit>= 0.0)
        throw runtime_error("the bucket_empty_limit should be less than 0.0, typically -10.0 mm");
    return apoint_ts(make_shared<const bucket_ts>(*this,bucket_parameter{deltahours(start_hour_utc),bucket_empty_limit}));
}

vector<double> bucket_fix(vector<double> const& v, size_t i0, size_t n,double bucket_empty_limit) {
    vector<double> dv_p(n, shyft::nan);
    vector<double> dv(n, shyft::nan);
    vector<double> hi(n, 0);
    if (n == 0 || i0 >= n)
        return hi;
    for (size_t i = i0; i < n - 1; ++i) {
        double tmp_dv = v[i+1] - v[i];
        dv[i] = tmp_dv > bucket_empty_limit ? tmp_dv : shyft::nan;
    }
    dv[n - 1] = 0.0;
    // dv contains unfiltered hourly precip in the bucket
    for (size_t d = 0; d < n/24; ++d) {
        double dv_sum = 0;
        double dv_psum = 0;
        for (size_t i = i0 + d*24; i < i0 + (d + 1)*24 && i<n; ++i) {
            // daily sum
            if (isfinite(dv[i])) {
                dv_sum += dv[i];
                if (dv[i] > 0) {
                    dv_p[i] = dv[i];// save positive derivatives
                    dv_psum += dv[i];// daily positive sum
                } else {
                    dv_p[i] = 0;
                }
            }
        }
        // calculate hourly intensity where positive diff
        double p_diff = dv_sum > 0 ? dv_sum : 0;
        if (p_diff > 0) {
            for (size_t i = i0 + d*24; i < i0 + (d + 1)*24 && i<(n-1); ++i)
                hi[i+1] = dv_p[i]*p_diff / dv_psum;
        } else {
            for (size_t i = i0 + d*24; i < i0 + (d + 1)*24&& i<(n-1); ++i)
                hi[i+1] = 0;
        }
    }
    return hi;
}

vector<double> bucket_ts::values() const {
    assert_bound();
    vector<double> v;
    if (!hour_time_axis(ts->time_axis())) {
        v = apoint_ts(ts).average(ta).values();
    }
    else {
        v = ts->values();
    }
    auto t0 = floor(ta.total_period().start + calendar::DAY - utctime(1ul) - p.hour_offset, calendar::DAY) + p.hour_offset;
    if (!ta.total_period().contains(t0))
        return vector<double>(ta.size(), shyft::nan);
    return bucket_fix(v, ta.index_of(t0), ta.size(),p.bucket_empty_limit);
}

double bucket_ts::value(size_t i) const {
    if(i>=ta.size())
        return shyft::nan;
    auto t0 = floor(ta.time(i) - p.hour_offset, calendar::DAY) + p.hour_offset;
    //auto t1 = t0 + calendar::DAY;
    if (!ta.total_period().contains(t0) )// we allow trailing ts, || !ta.total_period().contains(t1))
        return shyft::nan;
    auto v = apoint_ts(ts).average(time_axis::generic_dt(t0, seconds(3600), 24)).values();
    auto r = bucket_fix(v, 0, 24,p.bucket_empty_limit);
    auto hour_idx = (ta.time(i) - t0)/seconds(3600);
    return r[hour_idx];
}

double bucket_ts::value_at(utctime t) const {
    assert_bound();
    if (!ta.total_period().contains(t))
        return shyft::nan;
    return value(ta.index_of(t));
}

//----------------------------------------------------------------------------------------
//-- repeat_ts impl.
apoint_ts apoint_ts::repeat(gta_t const&rta) const {
        return apoint_ts(make_shared<const repeat_ts>(ts,rta));
}

double repeat_ts::value(size_t i) const  {
    using time_axis::generic_dt;
    assert_bound();
    if(i>=ta.size())// sanity, and covers case when ts is nullptr, ta will be empty/zero
        return shyft::nan;
    const auto&src=ts->time_axis();
    auto t=ta.time(i);
    auto t0 = rta.gt==generic_dt::CALENDAR? rta.c.cal->trim(src.time(0),rta.c.dt):utctime_floor(src.time(0),rta.f.dt); // fast compute, invariant.
    auto rp=rta.index_of(t);//repeat period index, fast computed,since r is calendar_dt or fixed_dt
    auto tb=rta.time(rp);// start of repeat period., fast computed, ...
    auto t_src= t0+ (t-tb);//offset into repeat-pattern.
    auto src_ix=src.index_of(t_src); // will return n_pos for t_src < src.t0, -kind of ok. and also npos for t_src > src.end, which is also ok, since we plan nan-for both.
    return src_ix!=std::string::npos?ts->value(src_ix):shyft::nan;
}

double repeat_ts::value_at(utctime t) const  {
    using time_axis::generic_dt;
    assert_bound();
    if(!ta.total_period().contains(t)) // try to resolve trivial case fast.
        return shyft::nan;
    // hard work starts here
    const auto&src=ts->time_axis();
    auto t0 = rta.gt==generic_dt::CALENDAR? rta.c.cal->trim(src.time(0),rta.c.dt):utctime_floor(src.time(0),rta.f.dt); // fast compute, invariant.
    auto rp=rta.index_of(t);//repeat period index, fast computed,since r is calendar_dt or fixed_dt
    auto tb=rta.time(rp);// start of repeat period., fast computed, ...
    auto t_src= t0+ (t-tb);//offset into repeat-pattern.
    auto src_ix= src.index_of(t_src); // will return n_pos for t_src < src.t0, -kind of ok. and also npos for t_src > src.end, which is also ok, since we plan nan-for both.
    auto v1 = src_ix!=std::string::npos?ts->value(src_ix):shyft::nan;
    bool linear = point_interpretation()==POINT_INSTANT_VALUE;
    if(!linear) return v1;
    if(!isfinite(v1)) return v1;// nan because lhs is nan
    // get next point of src.. hmm. could wrap.
    if(src_ix+1 < src.size()) {// we are lucky, just interpolated to next point
        auto v2= ts->value(src_ix+1);
        auto t2 = src.time(src_ix+1);
        auto t1= src.time(src_ix);
        return v1 + to_seconds(t_src-t1)*(v2-v1)/to_seconds(t2-t1);//interpolate
    } else { // we are at the end of src ts. If total period ends before next period, this is a nan, otherwise, wrap around take first value.
        if( tb + (src.total_period().end - t0 ) >= rta.period(rp).end ) { // the repeat ts do have enough data for period,  try to wrap to first value.hmm.
            if(src.time(0)== t0) {
                auto v2= ts->value(0);// then interpolate. but what if there is a gap between t0 and the first value of the src ts. (always wrap,or always nan..)
                auto t2 =t0+(rta.period(rp).end - tb);// yes, this point exactly
                auto t1= src.time(src_ix);
                return v1 + to_seconds(t_src-t1)*(v2-v1)/to_seconds(t2-t1);//interpolate
            } // else wrap around into a nan-area.
        }
    }
    return shyft::nan;
}

vector<double> repeat_ts::values() const {
    using time_axis::generic_dt;
    assert_bound();
    vector<double> r;r.reserve(ta.size());
    const auto&src=ts->time_axis();
    auto t0 = rta.gt==generic_dt::CALENDAR? rta.c.cal->trim(src.time(0),rta.c.dt):utctime_floor(src.time(0),rta.f.dt); // fast compute, invariant.
    // the need for speed, optimize between pure pattern-concrete source, or expression. The first one is always the case on the server-side eval
    double const *ts_value;
    vector<double> src_ts_v;
    if(auto pts=ts_as<gpoint_ts>(ts)) {
        ts_value=pts->rep.v.data();//avoid the struggle, point directly to memory
    } else {
        src_ts_v=ts->values();// first get the src ts evaluated (might be cost)
        ts_value=src_ts_v.data();// then point into local copy of it.
    }
    for(size_t i=0;i<ta.size();++i) {
        auto t=ta.time(i);
        auto rp=rta.index_of(t);//repeat period index, fast computed,since r is calendar_dt or fixed_dt
        auto tb=rta.time(rp);// start of repeat period., fast computed, ...
        auto t_src= t0+ (t-tb);//offset into repeat-pattern.
        auto src_ix=src.index_of(t_src); // will return n_pos for t_src < src.t0, -kind of ok. and also npos for t_src > src.end, which is also ok, since we plan nan-for both.
        r.push_back( src_ix!=std::string::npos?ts_value[src_ix]:shyft::nan);
    }
    return r;
}

// ds for binding and symbolic ts
bool repeat_ts::needs_bind() const {
    return bound?false:(ts?ts->needs_bind():false);// allow null ts repeat.
}
void repeat_ts::do_bind() {
    if(!bound) {
        if(ts) dref(ts).do_bind();
        local_do_bind();
        bound=true;
    }
}
void repeat_ts::local_do_bind() {
    if(ts)
        ta=time_axis::repeat_time_axis(ts->time_axis(),rta);
    bound=true;
}
void repeat_ts::do_unbind() {
    if(bound) {
        if(ts) dref(ts).do_unbind();
        local_do_unbind();
        bound=false;
    }
}
void repeat_ts::local_do_unbind() {
    bound=true;
}
time_shift_ts::time_shift_ts(const apoint_ts& ats,utctimespan adt):ts(ats.ts),dt(adt) {
    if(!ts->needs_bind())
        local_do_bind();

}

time_shift_ts::time_shift_ts(apoint_ts&& ats, utctimespan adt):ts(move(ats.ts)),dt(adt) {
    if(!ts->needs_bind())
        local_do_bind();
}
repeat_ts::repeat_ts(const apoint_ts& ats,gta_t const&rta)
    :ts(ats.ts),rta(rta) {
        do_early_bind();
}
repeat_ts::repeat_ts(apoint_ts&& ats, gta_t && rta)
    :ts(move(ats.ts)),rta(move(rta)) {
        do_early_bind();
}

ats_vector ats_vector::percentiles(gta_t const &ta,intv_t const& percentile_list) const {
    ats_vector r;r.reserve(percentile_list.size());
    auto rp= shyft::time_series::calculate_percentiles(ta,deflate_ts_vector<gts_t>(*this),percentile_list);
    for(auto&ts:rp) r.emplace_back(ta,move(ts.v),POINT_AVERAGE_VALUE);
    return r;
}
bucket_ts::bucket_ts(const apoint_ts& ats, const bucket_parameter& qp) :ts(ats.ts), p{qp} { do_early_bind(); }

vector<double> ats_vector::value_range(utcperiod p) const {
    using time_axis::generic_dt;
    if(! p.valid()) throw std::runtime_error("You must specify a valid utcperiod.");

    // ignore empty ts
    const shyft::time_axis::generic_dt ta(p.start, p.timespan(), 1);
    ats_vector tsv; tsv.reserve(size());
    for(auto&ts: *this){
        if (ts.needs_bind()) throw runtime_error("value_range:attemt to use method on unbound ts");
        if (ts.size()==0) continue;
        tsv.emplace_back(ts);
    }

    if(tsv.size()==0) return vector<double> {nan, nan};
    // values begining / end of interval
    auto v_t0 = tsv.values_at_time(p.start);
    auto v_t1 = tsv.values_at_time(p.end);

    // in between, i.e. where ats has bound values
    auto r1 = tsv.percentiles(ta, {statistics_property::MIN_EXTREME, statistics_property::MAX_EXTREME});

    const auto [min_t0, max_t0] = std::minmax_element(v_t0.begin(), v_t0.end());
    const auto [min_t1, max_t1] = std::minmax_element(v_t1.begin(), v_t1.end());
    double min = std::fmin(std::fmin(*min_t0, r1[0].value(0)), *min_t1);
    double max = std::fmax(std::fmax(*max_t0, r1[1].value(0)), *max_t1);

    return vector<double> {min, max};
}

void aref_ts::do_bind()  {}
void aref_ts::do_unbind() {
    rep=nullptr;//questionable? what if it was created with id,ts?
}

}

