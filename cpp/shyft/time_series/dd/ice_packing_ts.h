/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/ice_packing.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
struct apoint_ts;
/** @brief ice-packing detection time-series
*
* The purpose of this time-series is to provide 1.0 if
* ice-packing is detected, according to specified parameters,
* otherwise evaluate to 0.0 if no ice-packing occurs.
*
* Temperature time-series is the input to this algorithm,
* and the time-axis etc. are just reflected through this class.
*
*
* To signal error conditions, the ice-packing ts returns nan
* according to specified policy flags.
*
*/
struct ice_packing_ts : ipoint_ts {

    using ipt_t = shyft::time_series::ice_packing_ts<apoint_ts>;
    using ip_param_t = shyft::time_series::ice_packing_parameters;

    ipt_t ts; ///< the implementation time-series fetched from the core layer, with temp

    ice_packing_ts() = default;
    template < class TS, class IPP >
    ice_packing_ts(
        TS && ts, IPP && ipp,
        ice_packing_temperature_policy ipt_policy = ice_packing_temperature_policy::DISALLOW_MISSING
    ) : ts{ forward<TS>(ts), forward<IPP>(ipp), ipt_policy } { }
    // -----
    virtual ~ice_packing_ts() = default;
    // -----
    ice_packing_ts(const ice_packing_ts &) = default;
    ice_packing_ts & operator= (const ice_packing_ts &) = default;
    // -----
    ice_packing_ts(ice_packing_ts &&) = default;
    ice_packing_ts & operator= (ice_packing_ts &&) = default;

    bool needs_bind() const override { return ts.needs_bind(); }
    void do_bind() override { ts.do_bind(); }
    void do_unbind() override {ts.do_unbind();}
    // -----
    ts_point_fx point_interpretation() const override { return ts.point_interpretation(); }
    void set_point_interpretation(ts_point_fx policy) override { return ts.set_point_interpretation(policy); }
    // -----
    size_t size() const override { return ts.size(); }
    utcperiod total_period() const override { return ts.total_period(); }
    const gta_t & time_axis() const override { return ts.time_axis(); }
    // -----
    size_t index_of(utctime t) const override { return ts.index_of(t); }
    double value_at(utctime t) const override { return ts(t); }
    // -----
    utctime time(size_t i) const override { return ts.time(i); }
    double value(size_t i) const override { return ts.value(i); }
    // -----
    vector<double> values() const override {
        size_t dim = size();
        vector<double> ret; ret.reserve(dim);
        for ( size_t i = 0; i < dim; ++i ) {
            ret.emplace_back(value(i));
        }
        return ret;
    }
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    string stringify() const override;
    x_serialize_decl();

};
    
}
x_serialize_export_key(shyft::time_series::ice_packing_ts<shyft::time_series::dd::apoint_ts>);
x_serialize_export_key(shyft::time_series::dd::ice_packing_ts);
