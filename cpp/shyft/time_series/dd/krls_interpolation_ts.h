/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/predictions.h>

namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
struct apoint_ts;
struct krls_interpolation_ts : ipoint_ts {
    using krls_p = prediction::krls_rbf_predictor;

    apoint_ts ts;
    krls_p predictor;

    bool bound=false;

    template <class TS_, class PRED_>
    krls_interpolation_ts(TS_&&ts, PRED_&& p):ts(forward<TS_>(ts)),predictor(forward<PRED_>(p)) {
        if (!needs_bind())
            local_do_bind();
    }

    krls_interpolation_ts() = default;

    krls_interpolation_ts(apoint_ts && ts,
            utctimespan dt, double rbf_gamma, double tol, size_t size
        ) : ts{ move(ts) }, predictor{ dt, rbf_gamma, tol, size }
    {
        if( ! needs_bind() )
            local_do_bind();
    }
    krls_interpolation_ts(const apoint_ts & ts,
        utctimespan dt, double rbf_gamma, double tol, size_t size
    ) : ts{ ts }, predictor{ dt, rbf_gamma, tol, size }
    {
        if( ! needs_bind() )
            local_do_bind();
    }
    // -----
    virtual ~krls_interpolation_ts() = default;
    // -----
    krls_interpolation_ts(const krls_interpolation_ts &) = default;
    krls_interpolation_ts & operator= (const krls_interpolation_ts &) = default;
    // -----
    krls_interpolation_ts(krls_interpolation_ts &&) = default;
    krls_interpolation_ts & operator= (krls_interpolation_ts &&) = default;

    bool needs_bind() const override { return ts.needs_bind(); }
    void do_bind() override { ts.do_bind(); local_do_bind(); }
    void do_unbind() override { ts.do_unbind();local_do_unbind();}
    void local_do_bind() {
        if ( ! bound ) {
            predictor.train(ts);
            bound=true;
        }
    }
    void local_do_unbind() {
        if ( bound ) {
            //predictor.train(ts);
            bound=false;
        }
    }
    void bind_check() const {
        if ( ! bound ) {
            throw runtime_error("attempting to use unbound timeseries, context krls_interpolation_ts");
        }
    }
    // -----
    ts_point_fx point_interpretation() const override { return ts.point_interpretation(); }
    void set_point_interpretation(ts_point_fx policy) override { ts.set_point_interpretation(policy); }
    // -----
    size_t size() const override { return ts.size(); }
    utcperiod total_period() const override { return ts.total_period(); }
    const gta_t & time_axis() const override { return ts.time_axis(); }
    // -----
    size_t index_of(utctime t) const override { return ts.index_of(t); }
    double value_at(utctime t) const override { bind_check(); return predictor.predict(t); }
    // -----
    utctime time(size_t i) const override { return ts.time(i); }
    double value(size_t i) const override { bind_check(); return predictor.predict(ts.time(i)); }
    // -----
    vector<double> values() const override {
        bind_check();
        return predictor.predict_vec(ts.time_axis());
    }
    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    string stringify() const override;

    x_serialize_decl();

};

}
x_serialize_export_key(shyft::time_series::dd::krls_interpolation_ts);
