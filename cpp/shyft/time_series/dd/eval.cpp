#include <shyft/time_series/time_series_dd.h>
#include <shyft/time_series/bin_op_eval.h>

namespace shyft::time_series::dd {
using std::make_shared;

/** helper template for ::prepare to keep code short */

static void safe_prepare(ipoint_ts_ref const& ts,eval_ctx& c) {
    if(ts) // nullptr can happen(is valid) for some operators
        ts->prepare(c);
}


template<class TS> 
static void prepare_ts(TS const*this_ts,eval_ctx& c) {
    if(c.ref_counting(this_ts)>1u) return;
    safe_prepare(this_ts->ts,c);
}

void abin_op_scalar_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(rhs.ts,c);
}
ipoint_ts_ref abin_op_scalar_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_rhs= c.evaluate(rhs.ts);
    abin_op_scalar_ts tmp;
    tmp.lhs=lhs;tmp.op=op;tmp.rhs=apoint_ts(eval_rhs);tmp.ta=ta;tmp.fx_policy=fx_policy;tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),fx_policy);
    c.register_ts(this,r);
    return r;
}

/** unwrap a terminal_ts from ipoint_ts or throw */
static inline const gts_t& terminal_ts(ipoint_ts_ref const& ts) {
    if (auto ats=ts_as<aref_ts>(ts))
        return ats->core_ts();
    if (auto gts=ts_as<gpoint_ts>(ts))
        return gts->core_ts();
    throw runtime_error("terminal_ts called for unsupported type.");
}

void abin_op_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(rhs.ts,c);
    safe_prepare(lhs.ts,c);
}
ipoint_ts_ref abin_op_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    using shyft::time_series::detail::bin_op_resolve_a;
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto x_rhs=c.evaluate(rhs.ts);// note: keep real reference here in case it's a temporary with 1-ref == this-ref
    auto x_lhs=c.evaluate(lhs.ts);
    auto const& e_rhs= terminal_ts(x_rhs);
    auto const& e_lhs= terminal_ts(x_lhs);

    vector<double> rv;
    switch (op) {
        case OP_ADD:rv=bin_op_resolve_a(ta,e_lhs,std::plus<double>(),e_rhs);break;
        case OP_SUB:rv=bin_op_resolve_a(ta,e_lhs,std::minus<double>(),e_rhs);break;
        case OP_MUL:rv=bin_op_resolve_a(ta,e_lhs,std::multiplies<double>(),e_rhs);break;
        case OP_DIV:rv=bin_op_resolve_a(ta,e_lhs,std::divides<double>(),e_rhs);break;
        case OP_MAX:rv=bin_op_resolve_a(ta,e_lhs,[](double const&a,double const&b) noexcept { return std::max(a,b);},e_rhs);break;
        case OP_MIN:rv=bin_op_resolve_a(ta,e_lhs,[](double const&a,double const&b) noexcept { return std::min(a,b);},e_rhs);break;
        case OP_POW:rv=bin_op_resolve_a(ta,e_lhs,[](double const&a,double const&b) noexcept { return std::pow(a,b);},e_rhs);break;
        //[[fallthrough]];
        case OP_LOG:  // only exposed through for abin_op_ts_scalar
        default: throw runtime_error("ERROR: Illegal bin-op type in abin_op_ts::evaluate");
            break;
    }
    auto r= make_shared<gpoint_ts>(ta,move(rv),fx_policy);
    c.register_ts(this,r);
    return r;
}

void abin_op_ts_scalar::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(lhs.ts,c);
}
ipoint_ts_ref abin_op_ts_scalar::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_lhs =c.evaluate(lhs.ts);
    abin_op_ts_scalar tmp;
    tmp.lhs=apoint_ts(eval_lhs);tmp.op=op;tmp.rhs=rhs;tmp.ta=ta;tmp.fx_policy=fx_policy;tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),fx_policy);
    c.register_ts(this,r);
    return r;
}

void anary_op_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    for(auto const& a: args) safe_prepare(a.ts,c);
}
ipoint_ts_ref anary_op_ts::evaluate(eval_ctx& c, ipoint_ts_ref const& /*shared_this*/) const {
     if(c.is_evaluated(this))
        return c.evaluated[this];
    vector<double> tv;
    if(op==nary_op_t::OP_MERGE) {
        vector<apoint_ts> tmp_args;tmp_args.reserve(args.size());
        for(auto const& a: args)
            tmp_args.emplace_back(c.evaluate(a.ts));
        tv.reserve(ta.size());
        forecast_merge_fx(tmp_args, lead_time, fc_interval, [&tv](utctime, double v){tv.push_back(v);}, [](utctime){});
    } else if(op==nary_op_t::OP_ADD) {
        if(args.size()) {
            auto a0=c.evaluate(args[0].ts);
            gts_t a{terminal_ts(a0)};//first approach algo
            for(size_t i=1;i<args.size();++i) {
                auto ai=c.evaluate(args[i].ts);//keep ref. alive here in case it's a single expression with only this ref.
                auto const &b=terminal_ts(ai);//unwrap terminal_ts raw data.
                if(b.time_axis()==a.time_axis()) {
                    a.add(b);//fast-track, inplace add.
                } else {
                    a=bin_op_eval<gts_t>(a,std::plus<double>(),b);
                }
            }
            tv=a.v;
        }
    } else {
        for(size_t i=0;i<ta.size();++i)
            tv.push_back(shyft::nan);
    }
    auto r= make_shared<gpoint_ts>(ta, move(tv), fx_policy);
    c.register_ts(this,r);
    return r;
}

void abs_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref abs_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    abs_ts tmp;
    tmp.ts=eval_ts;tmp.ta=ta;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),ts->point_interpretation());
    c.register_ts(this,r);
    return r;
}

void accumulate_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref accumulate_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    accumulate_ts tmp;
    tmp.ts=eval_ts;tmp.ta=ta;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void aref_ts::prepare(eval_ctx&/*c*/) const {
    return;
}
ipoint_ts_ref aref_ts::evaluate(eval_ctx&/*c*/, ipoint_ts_ref const& /*shared_this*/) const {
    return rep;
}

void average_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref average_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    average_ts tmp;
    tmp.ts=eval_ts;tmp.ta=ta;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void bucket_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref bucket_ts::evaluate(eval_ctx& c, ipoint_ts_ref const&) const {
    if (c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts = c.evaluate(ts);
    bucket_ts tmp;
    tmp.ts = eval_ts; tmp.ta = ta;tmp.p=p; tmp.bound = bound;
    auto r = make_shared<gpoint_ts>(ta, tmp.values(), point_interpretation());
    c.register_ts(this, r);
    return r;
}

void decode_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref decode_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    decode_ts tmp;
    tmp.ts=eval_ts;tmp.p=p;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void convolve_w_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(ts_impl.ts.ts,c);
}
ipoint_ts_ref convolve_w_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts_impl.ts.ts);
    convolve_w_ts tmp;
    tmp.ts_impl.ts=apoint_ts(eval_ts);tmp.ts_impl.w=ts_impl.w;tmp.ts_impl.policy=ts_impl.policy;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void derivative_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref derivative_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    derivative_ts tmp;
    tmp.ts=eval_ts;tmp.dm=dm;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void extend_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(lhs.ts,c);
    safe_prepare(rhs.ts,c);
}
ipoint_ts_ref extend_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_rhs= c.evaluate(rhs.ts);
    auto eval_lhs =c.evaluate(lhs.ts);
    extend_ts tmp;
    tmp.lhs=apoint_ts(eval_lhs);
    tmp.rhs=apoint_ts(eval_rhs);
    tmp.ets_split_p=ets_split_p;
    tmp.split_at=split_at;
    tmp.ets_fill_p=ets_fill_p;
    tmp.fill_value=fill_value;
    tmp.ta=ta;
    tmp.fx_policy=fx_policy;
    tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),fx_policy);
    c.register_ts(this,r);
    return r;
}

void gpoint_ts::prepare(eval_ctx& /* c */) const {
    return;
}
ipoint_ts_ref gpoint_ts::evaluate(eval_ctx&/*c*/, ipoint_ts_ref const& shared_this) const {
    return shared_this;// trival case, evaluate to shared_this always
}

void ice_packing_recession_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(flow_ts.ts,c);
    safe_prepare(ice_packing_ts.ts,c);
}

ipoint_ts_ref ice_packing_recession_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_flow_ts=c.evaluate(flow_ts.ts);
    auto eval_ice_packing_ts =c.evaluate(ice_packing_ts.ts);
    ice_packing_recession_ts tmp;
    tmp.flow_ts=apoint_ts(eval_flow_ts);
    tmp.ice_packing_ts=apoint_ts(eval_ice_packing_ts);
    tmp.fx_policy=fx_policy;
    tmp.bound=bound;
    tmp.ipr_param=ipr_param;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),fx_policy);
    c.register_ts(this,r);
    return r;
}

void ice_packing_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(ts.temp_ts.ts,c);
}
ipoint_ts_ref ice_packing_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts=c.evaluate(ts.temp_ts.ts);
    ice_packing_ts tmp;
    tmp.ts.temp_ts=apoint_ts(eval_ts);
    tmp.ts.ip_param=ts.ip_param;
    tmp.ts.ipt_policy=ts.ipt_policy;
    tmp.ts.bound=ts.bound;
    tmp.ts.fx_policy=ts.fx_policy;

    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void inside_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}

ipoint_ts_ref inside_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    inside_ts tmp;
    tmp.ts=eval_ts;tmp.p=p;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void transform_spline_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}

ipoint_ts_ref transform_spline_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    transform_spline_ts tmp;
    tmp.ts=eval_ts;tmp.p=p;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void integral_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}

ipoint_ts_ref integral_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    integral_ts tmp;
    tmp.ts=eval_ts;tmp.ta=ta;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void krls_interpolation_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(ts.ts,c);
}
ipoint_ts_ref krls_interpolation_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts.ts);
    krls_interpolation_ts tmp;
    tmp.ts=apoint_ts(eval_ts);tmp.predictor=predictor;tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void periodic_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
}
ipoint_ts_ref periodic_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto r= make_shared<gpoint_ts>(time_axis(),ts.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void qac_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(cts,c);
    safe_prepare(ts,c);
}
ipoint_ts_ref qac_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    ipoint_ts_ref eval_cts= cts?c.evaluate(cts):cts;
    qac_ts tmp;
    tmp.ts=eval_ts;
    tmp.cts=eval_cts; // important! use the eval-cts to avoid recompute partial values on refs.
    tmp.p=p;
    tmp.ta=ta;
    tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void rating_curve_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(ts.level_ts.ts,c);
}
ipoint_ts_ref rating_curve_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts.level_ts.ts);
    rating_curve_ts tmp;
    tmp.ts.level_ts=apoint_ts(eval_ts);
    tmp.ts.rc_param=ts.rc_param;
    tmp.ts.fx_policy=ts.fx_policy;
    tmp.ts.bound=ts.bound;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void time_shift_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref time_shift_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    time_shift_ts tmp;
    tmp.ts=eval_ts;
    tmp.dt=dt;
    tmp.ta=ta;
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void use_time_axis_from_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(lhs.ts,c);
    safe_prepare(rhs.ts,c);
}
ipoint_ts_ref use_time_axis_from_ts::evaluate(eval_ctx&c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_lhs =c.evaluate(lhs.ts);
    use_time_axis_from_ts tmp;
    tmp.lhs=apoint_ts(eval_lhs);
    // faster:
    tmp.rhs=apoint_ts(rhs.ts->time_axis(),1.0,ts_point_fx::POINT_AVERAGE_VALUE);// just fake a ts, with the right ta.
    // slower(when expr. is more complex):
    //auto eval_rhs= c.evaluate(rhs.ts);//avoid doing eval, we have the time-axis..
    //tmp.rhs=apoint_ts(eval_rhs);
    tmp.ta=ta;
    tmp.fx_policy=fx_policy;
    tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),fx_policy);
    c.register_ts(this,r);
    return r;
}

void repeat_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref repeat_ts::evaluate(eval_ctx& c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    repeat_ts tmp;
    tmp.ts=c.evaluate(ts);
    tmp.ta=ta;
    tmp.rta=rta;
    tmp.bound=bound;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void aglacier_melt_ts::prepare(eval_ctx&c) const {
    if(c.ref_counting(this)>1u) return;
    safe_prepare(gm.sca_m2,c);
    safe_prepare(gm.temperature,c);
}
ipoint_ts_ref aglacier_melt_ts::evaluate(eval_ctx&c, ipoint_ts_ref const&) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto sca_m2 = c.evaluate(gm.sca_m2);
    auto temperature=c.evaluate(gm.temperature);
    aglacier_melt_ts tmp(apoint_ts(temperature),apoint_ts(sca_m2),gm.glacier_area_m2,gm.dtf);
    auto r= make_shared<gpoint_ts>(time_axis(),tmp.values(),point_interpretation());
    c.register_ts(this,r);
    return r;
}

void statistics_ts::prepare(eval_ctx&c) const {
    prepare_ts(this,c);
}
ipoint_ts_ref statistics_ts::evaluate(eval_ctx& c, ipoint_ts_ref const& /*shared_this*/) const {
    if(c.is_evaluated(this))
        return c.evaluated[this];
    auto eval_ts =c.evaluate(ts);
    statistics_ts tmp;
    tmp.ts=eval_ts;tmp.ta=ta;tmp.p=p;
    auto r= make_shared<gpoint_ts>(ta,tmp.values(),ts->point_interpretation());
    c.register_ts(this,r);
    return r;
}
    
}
