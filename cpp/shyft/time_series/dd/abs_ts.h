/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS See file COPYING for more details **/
#pragma once
#include <cmath>
#include <shyft/time_series/dd/ipoint_ts.h>
namespace shyft::time_series::dd {// dd= dynamic_dispatch version of the time_series library, aiming at python api
struct apoint_ts;
using std::abs;

/** @brief abs_ts as  abs(ts)
*
* The time-axis as source, values are abs of source
*
*
*/
struct abs_ts :ipoint_ts {
    ipoint_ts_ref ts;
    gta_t ta;

    abs_ts() = default;

    //-- useful ct goes here
    explicit abs_ts(const apoint_ts& ats)
        :ts(ats.ts) {
        if (!ts->needs_bind())
            local_do_bind();

    }
    explicit abs_ts(apoint_ts&& ats)
        :ts(move(ats.ts)){
        if (!ts->needs_bind())
            local_do_bind();
    }
    explicit abs_ts(ipoint_ts_ref const& ts)
        :ts(ts) {
        if (!ts->needs_bind())
            local_do_bind();
    }
    void local_do_bind() {
        if (ta.size() == 0) {//TODO: introduce bound flag, and use that, using the ta.size() is a problem if ta *is* size 0.
            ta = ts->time_axis();
        }
    }
    void local_do_unbind() {
        ta=gta_t{};
    }
    // implement ipoint_ts contract:
    ts_point_fx point_interpretation() const override { return ts->point_interpretation(); }
    void set_point_interpretation(ts_point_fx point_interpretation) override { wref(ts)->set_point_interpretation(point_interpretation); }
    const gta_t& time_axis() const override { return ta; }
    utcperiod total_period() const override { return ta.total_period(); }
    size_t index_of(utctime t) const override { return ts->index_of(t); }
    size_t size() const override { return ta.size(); }
    utctime time(size_t i) const override { return ta.time(i); };
    double value(size_t i) const override { return abs(ts->value(i)); }
    double value_at(utctime t) const override { return abs(ts->value_at(t)); }
    vector<double> values() const override {
        auto vv=ts->values();
        for (auto &v : vv) v = abs(v);
        return vv;
    }
    bool needs_bind() const override { return ts->needs_bind(); }
    void do_bind() override { dref(ts).do_bind(); local_do_bind(); }
    void do_unbind() override { dref(ts).do_unbind(); local_do_unbind(); }

    ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
    shared_ptr<ipoint_ts> clone_expr() const override;
    void prepare(eval_ctx&ctx) const override;
    string stringify() const override;
    x_serialize_decl();

};

}
x_serialize_export_key(shyft::time_series::dd::abs_ts);
