/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
    using namespace shyft::core;
    using std::string;
    using std::vector;
    using std::runtime_error;

    /** @brief point time-series, a concrete time-series
     *
     * @details
     * It is defined by its templated time-axis, ta
     * the values corresponding periods in time-axis (same size)
     * the ts_point_fx that determine how to compute the
     * f(t) on each interval of the time-axis (linear or stair-case)
     * and
     * value of the i'th interval of the time-series.
     * @tparam TA a time-axis type that satisfies requirement for a time-axis
     */
    template <class TA>
    struct point_ts {
        typedef TA ta_t;

        TA ta;
        vector<double> v;
        ts_point_fx fx_policy = POINT_INSTANT_VALUE;
        bool operator==(const point_ts&o) const { return  ta == o.ta && fx_policy == o.fx_policy && nan_equal(v, o.v); }
        ts_point_fx point_interpretation() const { return fx_policy; }
        void set_point_interpretation(ts_point_fx point_interpretation) { fx_policy=point_interpretation;}

        point_ts() = default;

        point_ts(const TA & ta, double fill_value, ts_point_fx fx_policy = POINT_INSTANT_VALUE)
            : ta{ ta }, v(ta.size(), fill_value), fx_policy{ fx_policy } { }

        point_ts(const TA & ta, const vector<double> & vx, ts_point_fx fx_policy = POINT_INSTANT_VALUE)
            : ta{ ta }, v{ vx }, fx_policy{ fx_policy }
        {
            if(ta.size() != v.size())
                throw runtime_error("point_ts: time-axis size is different from value-size");
        }

        point_ts(TA && tax, vector<double> && vx, ts_point_fx fx_policy= POINT_INSTANT_VALUE )
            : ta{ std::move(tax) }, v{ std::move(vx) }, fx_policy{ fx_policy }
        {
            if(ta.size() != v.size())
                throw runtime_error("point_ts: time-axis size is different from value-size");
        }

        point_ts(const TA & tax, vector<double> && vx, ts_point_fx fx_policy= POINT_INSTANT_VALUE )
            : ta{ tax }, v{ std::move(vx) }, fx_policy{ fx_policy }
        {
            if(ta.size() != v.size())
                throw runtime_error("point_ts: time-axis size is different from value-size");
        }

        // TA ta, ta is expected to provide 'time_axis' functions as needed
        // so we do not re-pack and provide functions like .size(), .index_of etc.

        const TA & time_axis() const noexcept { return ta; }

        /**@brief the function value f(t) at time t, fx_policy taken into account */
        double operator()(utctime t) const noexcept {
            size_t i = ta.index_of(t);
            if(i == string::npos) return nan;
            if( fx_policy==ts_point_fx::POINT_INSTANT_VALUE && i+1<ta.size() && isfinite(v[i+1])) {
                utctime t1=ta.time(i);
                utctime t2=ta.time(i+1);
                double f= to_seconds(t2-t)/to_seconds(t2-t1);
                return v[i]*f + (1.0-f)*v[i+1];
            }
            return v[i]; // just keep current value flat to +oo or nan
        }

        /**@brief i'th value of the value,
         */
        double value(size_t i) const noexcept { return v[i]; }
        const vector<double>& values() const noexcept {return v;}
        // BW compatiblity ?
        size_t size() const noexcept { return ta.size();}
        size_t index_of(utctime t) const noexcept {return ta.index_of(t);}
        utcperiod total_period() const noexcept {return ta.total_period();}
        utctime time(size_t i ) const {return ta.time(i);}

        // to help average_value method for now!
        point get(size_t i) const {return point(ta.time(i),value(i));}

        // Additional write/modify interface to operate directly on the values in the time-series
        void set(size_t i,double x) {v[i]=x;}
        void add(size_t i, double value) { v[i] += value; }
        void add(const point_ts<TA>& other) {
            std::transform(begin(v), end(v), other.v.cbegin(), begin(v), std::plus<double>());
        }
        void add_scale(const point_ts<TA>&other,double scale) {
            std::transform(begin(v), end(v), other.v.cbegin(), begin(v), [scale](double a, double b) {return a + b*scale; });
        }
        void fill(double value) { std::fill(begin(v), end(v), value); }
        void fill_range(double value, int start_step, int n_steps) { if (n_steps == 0)fill(value); else std::fill(begin(v) + start_step, begin(v) + start_step + n_steps, value); }
        void scale_by(double value) { std::for_each(begin(v), end(v), [value](double&v){v *= value; }); }
        point_ts<TA> slice(size_t first, size_t n) const {
            if(first >= size() || first + n > size() || n == 0)
                throw runtime_error("slice: invalid slice parameters");
            return point_ts<TA>(this->ta.slice(first, n),
                                vector<double>(this->v.begin() + first, this->v.begin() + (first + n)),
                                this->fx_policy);
        }

        x_serialize_decl();
    };

    template<class T> struct is_ts<point_ts<T>> {static const bool value=true;};
    template<class T> struct is_ts<shared_ptr<point_ts<T>>> {static const bool value=true;};

    /** specialization of hint-based search for all time-axis that merely can calculate the index from time t
     */

    inline size_t hint_based_search(const point_ts<time_axis::fixed_dt>& source, const utcperiod& p, size_t ) {
        return source.ta.open_range_index_of(p.start);
    }
    inline size_t hint_based_search(const point_ts<time_axis::calendar_dt>& source, const utcperiod& p, size_t ) {
        return source.ta.open_range_index_of(p.start);
    }

    inline size_t hint_based_search(const point_ts<time_axis::point_dt>& source, const utcperiod& p, size_t i) {
        return source.ta.open_range_index_of(p.start,i);
    }
    inline size_t hint_based_search(const point_ts<time_axis::generic_dt>& source, const utcperiod& p, size_t i) {
        return source.ta.open_range_index_of(p.start,i);
    }
}
x_serialize_export_key(shyft::time_series::point_ts<shyft::time_axis::fixed_dt>);
x_serialize_export_key(shyft::time_series::point_ts<shyft::time_axis::calendar_dt>);
x_serialize_export_key(shyft::time_series::point_ts<shyft::time_axis::point_dt>);
x_serialize_export_key(shyft::time_series::point_ts<shyft::time_axis::generic_dt>);
