/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once



#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/gpoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/average_ts.h>
#include <shyft/time_series/dd/integral_ts.h>
#include <shyft/time_series/dd/derivative_ts.h>
#include <shyft/time_series/dd/accumulate_ts.h>
#include <shyft/time_series/dd/time_shift_ts.h>
#include <shyft/time_series/dd/repeat_ts.h>

#include <shyft/time_series/dd/use_time_axis_from_ts.h>
#include <shyft/time_series/dd/abs_ts.h>
#include <shyft/time_series/dd/periodic_ts.h>
#include <shyft/time_series/dd/convolve_w_ts.h>
#include <shyft/time_series/dd/extend_ts.h>
#include <shyft/time_series/dd/aglacier_melt_ts.h>
#include <shyft/time_series/dd/rating_curve_ts.h>
#include <shyft/time_series/dd/ice_packing_ts.h>
#include <shyft/time_series/dd/ice_packing_recession_ts.h>
#include <shyft/time_series/dd/krls_interpolation_ts.h>
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/time_series/dd/inside_ts.h>
#include <shyft/time_series/dd/decode_ts.h>
#include <shyft/time_series/dd/bucket_ts.h>
#include <shyft/time_series/dd/statistics_ts.h>
#include <shyft/time_series/dd/abin_op_ts.h>
#include <shyft/time_series/dd/anary_op_ts.h>
#include <shyft/time_series/dd/transform_spline_ts.h>
#include <shyft/time_series/dd/ats_vector.h>


namespace shyft::time_series {



}

