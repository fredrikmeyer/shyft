/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <boost/math/special_functions/relative_difference.hpp>
#include <shyft/time_series/common.h>
#include <shyft/time_series/rating_curve_parameters.h>

namespace shyft::time_series {
    using namespace shyft::core;
    using std::string;
    using std::vector;
    using std::runtime_error;


    template <class TS>
    class rating_curve_ts {

    public:  // type api
        using ts_t = TS;
        using ta_t = typename TS::ta_t;

    public:  // data
        ts_t level_ts;
        rating_curve_parameters rc_param;
        // -----
        ts_point_fx fx_policy = ts_point_fx::POINT_INSTANT_VALUE;
        // -----
        bool bound = false;

    public:  // con/de-struction, copy & move
        rating_curve_ts() = default;
        rating_curve_ts(ts_t && ts, rating_curve_parameters && rc,
                        ts_point_fx fx_policy = ts_point_fx::POINT_INSTANT_VALUE)
            : level_ts{ std::forward<ts_t>(ts) },
              rc_param{ std::forward<rating_curve_parameters>(rc) },
              fx_policy{ fx_policy }
        {
            if( ! e_needs_bind(d_ref(level_ts)) )
                local_do_bind();
        }
        rating_curve_ts(const ts_t & ts, const rating_curve_parameters & rc,
                        ts_point_fx fx_policy = ts_point_fx::POINT_INSTANT_VALUE)
            : level_ts{ ts }, rc_param{ rc },
              fx_policy{ fx_policy }
        {
            if( ! e_needs_bind(d_ref(level_ts)) ) {
                local_do_bind();
            }
        }
        // -----
        ~rating_curve_ts() = default;
        // -----
        rating_curve_ts(const rating_curve_ts &) = default;
        rating_curve_ts & operator= (const rating_curve_ts &) = default;
        // -----
        rating_curve_ts(rating_curve_ts &&) = default;
        rating_curve_ts & operator= (rating_curve_ts &&) = default;

    public:  // usage
        bool needs_bind() const {
            return ! bound;
        }
        void do_bind() {
            if ( !bound ) {
                level_ts.do_bind();  // bind water levels
                local_do_bind();
            }
        }
        void do_unbind() {
            if(bound) {
                level_ts.do_unbind();
                local_do_unbind();
            }
        }
        void local_do_bind() {
            fx_policy = d_ref(level_ts).point_interpretation();
            bound = true;
        }
        void local_do_unbind() {
            bound = false;
        }
        void ensure_bound() const {
            if ( ! bound ) {
                throw runtime_error("rating_curve_ts: access to not yet bound attempted");
            }
        }
        // -----
        ts_point_fx point_interpretation() const {
            return fx_policy;
        }
        void set_point_interpretation(ts_point_fx policy) {
            fx_policy = policy;
        }

    public:  // api
        std::size_t size() const {
            return level_ts.size();
        }
        utcperiod total_period() const {
            return level_ts.total_period();
        }
        const ta_t & time_axis() const {
            return level_ts.time_axis();
        }
        // -----
        std::size_t index_of(utctime t) const {
            return level_ts.index_of(t);
        }
        double operator()(utctime t) const {
            ensure_bound();
            return rc_param.flow(t, level_ts(t));
        }
        // -----
        utctime time(std::size_t i) const {
            return level_ts.time(i);
        }
        double value(std::size_t i) const {
            ensure_bound();
            return rc_param.flow(time(i), level_ts.value(i));
        }
        point get(size_t i) const {return point{time(i),value(i)};}

        x_serialize_decl();
    };
    template<class TS> struct is_ts<rating_curve_ts<TS>> { static const bool value = true; };

}
