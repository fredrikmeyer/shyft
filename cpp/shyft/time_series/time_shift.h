/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <shyft/time_series/common.h>
#include <shyft/time_series/time_axis.h>

namespace shyft::time_series {
    using namespace shyft::core;
    using std::string;
    using std::vector;
    using std::runtime_error;

    /** @brief time_shift ts do a time-shift dt on the supplied ts
     *
     * @details
     * The values are exactly the same as the supplied ts argument to the constructor
     * but the time-axis is shifted utctimespan dt to the left.
     * e.g.: t_new = t_original + dt
     *
     *       lets say you have a time-series 'a'  with time-axis covering 2015
     *       and you want to time-shift so that you have a time- series 'b'data for 2016,
     *       then you could do this to get 'b':
     *
     *           utc = calendar() // utc calendar
     *           dt  = utc.time(2016,1,1) - utc.time(2015,1,1)
     *            b  = timeshift_ts(a, dt)
     *
     * @tparam Ts a time-series like object
     */
    template<class Ts>
    struct time_shift_ts {
        typedef typename Ts::ta_t ta_t;
        Ts ts;
        // need to have a time-shifted time-axis here:
        // TA, ta.timeshift(dt) -> a clone of ta...
        ta_t ta;
        ts_point_fx fx_policy=POINT_AVERAGE_VALUE; // inherited from ts
        utctimespan dt{0};// despite ta time-axis, we need it
        bool bound=false;
        ts_point_fx point_interpretation() const { return fx_policy; }
        void set_point_interpretation(ts_point_fx point_interpretation) { fx_policy=point_interpretation;}

        //-- default stuff, ct/copy etc goes here
        time_shift_ts()=default;

        //-- useful ct goes here
        template<class A_>
        time_shift_ts(A_ && ts,utctimespan dt)
            :ts(std::forward<A_>(ts)),dt(dt) {
            // we have to wait with time-axis until we know underlying stuff are ready:
            if( !(needs_bind< typename d_ref_t<Ts>::type>::value && e_needs_bind(ts))) {
                do_bind();//if possible do it now
            }
        }

        void do_bind() {
            if(!bound) {
                fx_policy = d_ref(ts).point_interpretation();
                ta = time_axis::time_shift(d_ref(ts).time_axis(),dt);
                bound=true;
            }
        }
        const ta_t& time_axis() const { return ta;}

        point get(size_t i) const {return point(ta.time(i),ts.value(i));}

        // BW compatiblity ?
        size_t size() const { return ta.size();}
        size_t index_of(utctime t) const {return ta.index_of(t);}
        utcperiod total_period() const {return ta.total_period();}
        utctime time(size_t i ) const {return ta.time(i);}

        //--
        double value(size_t i) const { return ts.value(i);}
        double operator()(utctime t) const { return ts(t-dt);} ///< just here we needed the dt
        x_serialize_decl();
    };
    template<class T> struct is_ts<time_shift_ts<T>> {static const bool value=true;};
    template<class T> struct is_ts<shared_ptr<time_shift_ts<T>>> {static const bool value=true;};
    template<class Ts>
    time_shift_ts<typename std::decay<Ts>::type > time_shift( Ts &&ts, utctimespan dt) {return time_shift_ts< typename std::decay<Ts>::type >(std::forward<Ts>(ts),dt);}

}
