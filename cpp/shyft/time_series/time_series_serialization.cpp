/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wsign-compare"
#endif

#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/base_object.hpp>

//
// 1. first include std stuff and the headers for
// files with serializeation support
//

#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/time_series.h>
#include <shyft/time_series/time_series_dd.h>
#include <shyft/dtss/time_series_info.h>
#include <shyft/time_series/predictions.h>
#include <shyft/dtss/dtss_cache.h>
#include <shyft/dtss/geo.h>
#include <shyft/time_series/dd/geo_ts.h>

#include <dlib/serialize.h>
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
//
// 2. Then implement each class serialization support
//

using namespace boost::serialization;
using namespace shyft::core;


//-- time-series serialization
template <class Ta>
template <class Archive>
void shyft::time_series::point_ts<Ta>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("time_axis", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("values", v)
		;
}
template <class TS>
template <class Archive>
void shyft::time_series::ref_ts<TS>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ref", ref)
		& core_nvp("ts", ts)
		;
}

template <class Ts>
template <class Archive>
void shyft::time_series::time_shift_ts<Ts>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ts", ts)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("dt", dt)
		& core_nvp("bound", bound)
		;
}
template <class Ts, class Ta>
template <class Archive>
void shyft::time_series::average_ts<Ts, Ta>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ts", ts)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		;
}

template <class Ts, class Ta>
template <class Archive>
void shyft::time_series::accumulate_ts<Ts, Ta>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ts", ts)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		;
}

template <class Archive>
void shyft::time_series::profile_description::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("t0", t0)
		& core_nvp("dt", dt)
		& core_nvp("profile", profile)
		;
}
template <class TA>
template <class Archive>
void shyft::time_series::profile_accessor<TA>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ta", ta)
		& core_nvp("profile", profile)
		& core_nvp("fx_policy", fx_policy)
		;
}

template <class TA>
template <class Archive>
void shyft::time_series::periodic_ts<TA>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ta", ta)
		& core_nvp("pa", pa)
		& core_nvp("fx_policy", fx_policy)
		;
}

template <class TS_A, class TS_B>
template <class Archive>
void shyft::time_series::glacier_melt_ts<TS_A, TS_B>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("temperature", temperature)
		& core_nvp("sca_m2", sca_m2)
		& core_nvp("glacier_area_m2", glacier_area_m2)
		& core_nvp("dtf", dtf)
		& core_nvp("fx_policy", fx_policy)
		;
}

template <class Ts>
template <class Archive>
void shyft::time_series::convolve_w_ts<Ts>::serialize(Archive & ar, const unsigned int version) {
	bool b = bound;
	ar
		& core_nvp("ts", ts)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("w", w)
		& core_nvp("convolve_policy", policy)
		& core_nvp("bound", b)
		;
	bound = b;
}

template <class A, class B, class O, class TA>
template<class Archive>
void shyft::time_series::bin_op<A, B, O, TA>::serialize(Archive & ar, const unsigned int version) {
	bool bd = bind_done;
	ar
		//& core_nvp("op",o.op) // not needed yet, needed when op starts to carry data
		& core_nvp("lhs", lhs)
		& core_nvp("rhs", rhs)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bind_done", bind_done)
		;
	bind_done = bd;
}

template<class Archive>
void shyft::time_series::rating_curve_segment::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("lower", lower)
		& core_nvp("a", a)
		& core_nvp("b", b)
		& core_nvp("c", c)
		;
}

template<class Archive>
void shyft::time_series::rating_curve_function::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("segments", segments)
		;
}

template<class Archive>
void shyft::time_series::rating_curve_parameters::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("curves", curves)
		;
}

template <class TS>
template<class Archive>
void shyft::time_series::rating_curve_ts<TS>::serialize(Archive & ar, const unsigned int version) {
	bool bd = bound;
	ar
		& core_nvp("level_ts", level_ts)
		& core_nvp("rc_param", rc_param)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bd)
		;
	bound = bd;
}

template<class Archive>
void shyft::time_series::ice_packing_parameters::serialize(Archive & ar, const unsigned int version) {
    ar
        & core_nvp("window", window)
        & core_nvp("threshold_temp", threshold_temp)
        ;
}

template <class TS>
template<class Archive>
void shyft::time_series::ice_packing_ts<TS>::serialize(Archive & ar, const unsigned int version) {
    bool bd = bound;
    ar
        & core_nvp("temp_ts", temp_ts)
        & core_nvp("ip_param", ip_param)
        & core_nvp("ipt_policy", ipt_policy)
        & core_nvp("fx_policy", fx_policy)
        & core_nvp("bound", bd)
        ;
    bound = bd;
}

template <class Archive>
void shyft::dtss::ts_info::serialize(Archive& ar, const unsigned int file_version) {
	ar
		& core_nvp("name", name)
		& core_nvp("point_fx", point_fx)
		& core_nvp("delta_t", delta_t)
		& core_nvp("olson_tz_id", olson_tz_id)
		& core_nvp("data_period", data_period)
		& core_nvp("created", created)
		& core_nvp("modified", modified)
		;
}

//-- predictor serialization
template < typename A, typename K >
inline void serialize_helper(A & ar, const dlib::krls<K> & krls) {

	using namespace dlib;

	std::ostringstream ostream{ std::ios_base::out | std::ios_base::binary };
	serialize(krls, ostream);
	auto blob = ostream.str();
	ar & core_nvp("krls_data", blob);
}

template < typename A, typename K >
inline void deserialize_helper(A & ar, dlib::krls<K> & krls) {

	using namespace dlib;

	std::string tmp;
	ar & core_nvp("krls_data", tmp);
	std::istringstream istream{ tmp, std::ios_base::in | std::ios_base::binary };
	deserialize(krls, istream);
}

template <class Archive>
void shyft::prediction::krls_rbf_predictor::serialize(Archive& ar, const unsigned int file_version) {
	ar & core_nvp("_dt", _dt) &core_nvp("_predicted_point_fx", _predicted_point_fx);

	if (Archive::is_saving::value) {
		serialize_helper(ar, this->_krls);
	} else {
		deserialize_helper(ar, this->_krls);
	}
}
template <class Arcive>
void shyft::dtss::cache_stats::serialize(Arcive& ar, const unsigned int file_version) {
	ar
		& core_nvp("hits", hits)
		& core_nvp("misses", misses)
		& core_nvp("coverage_misses", coverage_misses)
		& core_nvp("id_count", id_count)
		& core_nvp("point_count", point_count)
		& core_nvp("fragment_count", fragment_count)
		;
}

/* api time-series serialization (dyn-dispatch) */

template <class Archive>
void shyft::time_series::dd::ipoint_ts::serialize(Archive & ar, const unsigned) {
}

template<class Archive>
void shyft::time_series::dd::gpoint_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("rep", rep)
		;
}


template<class Archive>
void shyft::time_series::dd::aref_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("id", id)
		& core_nvp("rep", rep)

		;
}

template<class Archive>
void shyft::time_series::dd::average_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ta", ta)
		& core_nvp("ts", ts)
		;
}

template<class Archive>
void shyft::time_series::dd::integral_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ta", ta)
		& core_nvp("ts", ts)
		;
}

template<class Archive>
void shyft::time_series::dd::derivative_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		;
}

template<class Archive>
void shyft::time_series::dd::accumulate_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ta", ta)
		& core_nvp("ts", ts)
		;
}

template<class Archive>
void shyft::time_series::dd::statistics_ts::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
    & core_nvp("ta", ta)
    & core_nvp("ts", ts)
    & core_nvp("p",p)
    ;
}

template<class Archive>
void shyft::time_series::dd::time_shift_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ta", ta)
		& core_nvp("ts", ts)
		& core_nvp("dt", dt)
		;
}

template<class Archive>
void shyft::time_series::dd::periodic_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		;
}

template<class Archive>
void shyft::time_series::dd::convolve_w_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts_impl", ts_impl)
		;
}

template<class Archive>
void shyft::time_series::dd::rating_curve_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		;
}
template<class Archive>
void shyft::time_series::dd::repeat_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
        & core_nvp("rta",rta)
        & core_nvp("ta",ta)
        & core_nvp("bound",bound)
		;
}

template<>
template<class Archive>
void shyft::time_series::rating_curve_ts<shyft::time_series::dd::apoint_ts>::serialize(Archive & ar, const unsigned int version) {
	bool bd = bound;
	ar
		& core_nvp("level_ts", level_ts)
		& core_nvp("rc_param", rc_param)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bd)
		;
	bound = bd;
}

template<class Archive>
void shyft::time_series::dd::ice_packing_ts::serialize(Archive & ar, const unsigned int version) {
    ar
        & core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
        & core_nvp("ts", ts)
        ;
}

template<>
template<class Archive>
void shyft::time_series::ice_packing_ts<shyft::time_series::dd::apoint_ts>::serialize(Archive & ar, const unsigned int version) {
    bool bd = bound;
    ar
        & core_nvp("temp_ts", temp_ts)
        & core_nvp("ip_param", ip_param)
        & core_nvp("ipt_policy", ipt_policy)
        & core_nvp("fx_policy", fx_policy)
        & core_nvp("bound", bd)
        ;
    bound = bd;
}

template<class Archive>
void shyft::time_series::dd::ice_packing_recession_parameters::serialize(Archive & ar, const unsigned int version) {
    ar
        & core_nvp("alpha", alpha)
        & core_nvp("recession_minimum", recession_minimum)
        ;
}

template<class Archive>
void shyft::time_series::dd::ice_packing_recession_ts::serialize(Archive & ar, const unsigned int version) {
    bool bd = bound;
    ar
        & core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
        & core_nvp("flow_ts", flow_ts)
        & core_nvp("ice_packing_ts", ice_packing_ts)
        & core_nvp("ipr_param", ipr_param)
        & core_nvp("fx_policy", fx_policy)
        & core_nvp("bound", bd)
        ;
    bound = bd;
}

template<class Archive>
void shyft::time_series::dd::krls_interpolation_ts::serialize(Archive & ar, const unsigned int version) {
	bool bd = bound;
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		& core_nvp("predictor", predictor)
		& core_nvp("bound", bd)
		;
	bound = bd;
}

template<class Archive>
void shyft::time_series::dd::ats_vector::serialize(Archive& ar, const unsigned int version) {
	ar
		& core_nvp("ats_vec", base_object<shyft::time_series::dd::ats_vec>(*this))
		;
}

// kind of special, mix core and api, hmm!
template<>
template <class Archive>
void shyft::time_series::convolve_w_ts<shyft::time_series::dd::apoint_ts>::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ts", ts)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("w", w)
		& core_nvp("convolve_policy", policy)
		& core_nvp("bound", bound)
		;
}

template<class Archive>
void shyft::time_series::dd::extend_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("lhs", lhs)
		& core_nvp("rhs", rhs)
		& core_nvp("split_at", split_at)
		& core_nvp("ets_split_p", ets_split_p)
		& core_nvp("fill_value", fill_value)
		& core_nvp("ets_fill_p", ets_fill_p)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bound)
		;
}

template<class Archive>
void shyft::time_series::dd::use_time_axis_from_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("lhs", lhs)
		& core_nvp("rhs", rhs)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bound)
		;
}

template<class Archive>
void shyft::time_series::dd::abin_op_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("lhs", lhs)
		& core_nvp("op", op)
		& core_nvp("rhs", rhs)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bound)
		;
}

template<class Archive>
void shyft::time_series::dd::abin_op_scalar_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("lhs", lhs)
		& core_nvp("op", op)
		& core_nvp("rhs", rhs)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bound)
		;
}

template<class Archive>
void shyft::time_series::dd::abin_op_ts_scalar::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("lhs", lhs)
		& core_nvp("op", op)
		& core_nvp("rhs", rhs)
		& core_nvp("ta", ta)
		& core_nvp("fx_policy", fx_policy)
		& core_nvp("bound", bound)
		;
}

template<class Archive>
void shyft::time_series::dd::anary_op_ts::serialize(Archive & ar, const unsigned int version) {
    ar
    & core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
    & core_nvp("args", args)
    & core_nvp("op", op)
    & core_nvp("lead_time", lead_time)
    & core_nvp("fc_interval", fc_interval)
    & core_nvp("ta", ta)
    & core_nvp("fx_policy", fx_policy)
    & core_nvp("bound", bound)
    ;
}

template<class Archive>
void shyft::time_series::dd::abs_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		;
}

template<class Archive>
void shyft::time_series::dd::qac_parameter::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("max_timespan", max_timespan)
    & core_nvp("min_x", min_x)
    & core_nvp("max_x", max_x)
    & core_nvp("repeat_timespan",repeat_timespan)
    & core_nvp("repeat_tolerance",repeat_tolerance)
    & core_nvp("repeat_allowed",repeat_allowed)
    & core_nvp("constant_filler",constant_filler)
    ;
}

template<class Archive>
void shyft::time_series::dd::qac_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		& core_nvp("cts", cts)
		& core_nvp("p", p)
        & core_nvp("ta",ta)
        & core_nvp("bound",bound)
		;
}

template<class Archive>
void shyft::time_series::dd::inside_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		& core_nvp("p", p)
		;
}

template<class Archive>
void shyft::time_series::dd::decode_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		& core_nvp("p", p)
		;
}

template<class Archive>
void shyft::time_series::dd::bucket_ts::serialize(Archive & ar, const unsigned int version) {
	ar
		& core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
		& core_nvp("ts", ts)
		& core_nvp("p", p)
        & core_nvp("bound",bound)
        & core_nvp("ta",ta)
		;
}

template<class Archive>
void shyft::time_series::dd::apoint_ts::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("ts", ts)
    ;
}

//-- geo ts related serialization needs to go here:

template<class Archive>
void shyft::dtss::geo::query::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("epsg", epsg)
    & core_nvp("polygon", polygon)
    ;
}

template<class Archive>
void shyft::time_series::dd::geo_ts::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("mid_point", mid_point)
    & core_nvp("ts",ts)
    ;
}

template<class Archive>
void shyft::dtss::geo::grid_spec::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("epsg", epsg)
    & core_nvp("points",points)
    ;
}

template<class Archive>
void shyft::dtss::geo::ts_db_config::serialize(Archive & ar, const unsigned int version) {
	ar
	& core_nvp("prefix",prefix)
    & core_nvp("name", name)
    & core_nvp("descr",descr)
    & core_nvp("grid", grid)
    & core_nvp("t0",t0_times)
    & core_nvp("dt",dt)
    & core_nvp("n_ensemples",n_ensembles)
    & core_nvp("variables",variables)
    ;
}

template<class Archive>
void shyft::dtss::geo::eval_args::serialize(Archive & ar, const unsigned int version) {
	ar
    & core_nvp("geo_ts_db_id", geo_ts_db_id)
    & core_nvp("variables", variables)
    & core_nvp("ens",ens)
    & core_nvp("ta",ta)
    & core_nvp("ts_dt",ts_dt)
    & core_nvp("geo_range",geo_range)
    & core_nvp("concat",concat)
    & core_nvp("cc_dt0",cc_dt0)
    ;
}

template<class Archive>
void shyft::dtss::geo::detail::ix_calc::serialize(Archive & ar, const unsigned int version) {

	ar
	& core_nvp("n_t0",n_t0)
    & core_nvp("n_v", n_v)
    & core_nvp("n_e", n_e)
    & core_nvp("n_g", n_g)
    
    ;
}


template<class Archive>
void shyft::dtss::geo::ts_matrix::serialize(Archive & ar, const unsigned int version) {

	ar
	& core_nvp("shape",shape)
    & core_nvp("tsv", tsv)
    
    ;
}
template<class Archive>
void shyft::dtss::geo::geo_ts_matrix::serialize(Archive & ar, const unsigned int version) {

	ar
	& core_nvp("shape",shape)
    & core_nvp("tsv", tsv)
    
    ;
}

template<class Archive>
void shyft::time_series::dd::transform_spline_ts::serialize(Archive &ar, const unsigned int version) {
	ar
    & core_nvp("ipoint_ts", base_object<shyft::time_series::dd::ipoint_ts>(*this))
    & core_nvp("ts", ts)
    & core_nvp("p", p)
    ;
}

template<class Archive>
void shyft::time_series::dd::spline_parameter::serialize(Archive &ar, const unsigned int version) {
	ar
    & core_nvp("knots",knots)
    & core_nvp("coeff", coeff)
    & core_nvp("degree",degree)
    & core_nvp("first",first)
    & core_nvp("last",last)
    ;
}


x_serialize_instantiate_and_register(shyft::time_series::dd::transform_spline_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::spline_parameter);

x_serialize_instantiate_and_register(shyft::dtss::geo::detail::ix_calc);
x_serialize_instantiate_and_register(shyft::dtss::geo::ts_matrix);
x_serialize_instantiate_and_register(shyft::dtss::geo::geo_ts_matrix);


x_serialize_instantiate_and_register(shyft::dtss::geo::eval_args);
x_serialize_instantiate_and_register(shyft::dtss::geo::ts_db_config);
x_serialize_instantiate_and_register(shyft::dtss::geo::grid_spec);
x_serialize_instantiate_and_register(shyft::dtss::geo::query);
x_serialize_instantiate_and_register(shyft::dtss::ts_info);
x_serialize_instantiate_and_register(shyft::dtss::cache_stats);
x_serialize_instantiate_and_register(shyft::time_series::point_ts<shyft::time_axis::fixed_dt>);
x_serialize_instantiate_and_register(shyft::time_series::point_ts<shyft::time_axis::calendar_dt>);
x_serialize_instantiate_and_register(shyft::time_series::point_ts<shyft::time_axis::point_dt>);
x_serialize_instantiate_and_register(shyft::time_series::point_ts<shyft::time_axis::generic_dt>);
x_serialize_instantiate_and_register(shyft::time_series::ref_ts<shyft::time_series::point_ts<shyft::time_axis::fixed_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ref_ts<shyft::time_series::point_ts<shyft::time_axis::calendar_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ref_ts<shyft::time_series::point_ts<shyft::time_axis::point_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ref_ts<shyft::time_series::point_ts<shyft::time_axis::generic_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::profile_description);
x_serialize_instantiate_and_register(shyft::time_series::profile_accessor<shyft::time_axis::fixed_dt>);
x_serialize_instantiate_and_register(shyft::time_series::profile_accessor<shyft::time_axis::calendar_dt>);
x_serialize_instantiate_and_register(shyft::time_series::profile_accessor<shyft::time_axis::point_dt>);
x_serialize_instantiate_and_register(shyft::time_series::profile_accessor<shyft::time_axis::generic_dt>);
x_serialize_instantiate_and_register(shyft::time_series::convolve_w_ts<shyft::time_series::point_ts<shyft::time_axis::fixed_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::convolve_w_ts<shyft::time_series::point_ts<shyft::time_axis::generic_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::periodic_ts<shyft::time_axis::fixed_dt>);
x_serialize_instantiate_and_register(shyft::time_series::periodic_ts<shyft::time_axis::calendar_dt>);
x_serialize_instantiate_and_register(shyft::time_series::periodic_ts<shyft::time_axis::point_dt>);
x_serialize_instantiate_and_register(shyft::time_series::periodic_ts<shyft::time_axis::generic_dt>);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_segment);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_function);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_parameters);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_ts<shyft::time_series::point_ts<shyft::time_axis::fixed_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_ts<shyft::time_series::point_ts<shyft::time_axis::calendar_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_ts<shyft::time_series::point_ts<shyft::time_axis::point_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_ts<shyft::time_series::point_ts<shyft::time_axis::generic_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ice_packing_parameters);
x_serialize_instantiate_and_register(shyft::time_series::ice_packing_ts<shyft::time_series::point_ts<shyft::time_axis::fixed_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ice_packing_ts<shyft::time_series::point_ts<shyft::time_axis::calendar_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ice_packing_ts<shyft::time_series::point_ts<shyft::time_axis::point_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::ice_packing_ts<shyft::time_series::point_ts<shyft::time_axis::generic_dt>>);
x_serialize_instantiate_and_register(shyft::time_series::dd::ipoint_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::gpoint_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::aref_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::average_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::integral_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::accumulate_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::abs_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::time_shift_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::periodic_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::repeat_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::statistics_ts);
x_serialize_instantiate_and_register(shyft::time_series::convolve_w_ts<shyft::time_series::dd::apoint_ts>);
x_serialize_instantiate_and_register(shyft::time_series::dd::convolve_w_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::rating_curve_ts);
x_serialize_instantiate_and_register(shyft::time_series::rating_curve_ts<shyft::time_series::dd::apoint_ts>);
x_serialize_instantiate_and_register(shyft::time_series::dd::ice_packing_ts);
x_serialize_instantiate_and_register(shyft::time_series::ice_packing_ts<shyft::time_series::dd::apoint_ts>);
x_serialize_instantiate_and_register(shyft::time_series::dd::ice_packing_recession_parameters);
x_serialize_instantiate_and_register(shyft::time_series::dd::ice_packing_recession_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::extend_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::use_time_axis_from_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::abin_op_scalar_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::abin_op_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::abin_op_ts_scalar);
x_serialize_instantiate_and_register(shyft::time_series::dd::anary_op_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::apoint_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::krls_interpolation_ts);
x_serialize_instantiate_and_register(shyft::prediction::krls_rbf_predictor);
x_serialize_instantiate_and_register(shyft::time_series::dd::ats_vector);
x_serialize_instantiate_and_register(shyft::time_series::dd::qac_parameter);
x_serialize_instantiate_and_register(shyft::time_series::dd::qac_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::inside_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::decode_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::bucket_ts);
x_serialize_instantiate_and_register(shyft::time_series::dd::geo_ts);


std::string shyft::time_series::dd::apoint_ts::serialize() const {
	using namespace std;
	std::ostringstream xmls;
	core_oarchive oa(xmls, core_arch_flags);
	oa << core_nvp("ats", *this);
	xmls.flush();
	return xmls.str();
}
shyft::time_series::dd::apoint_ts shyft::time_series::dd::apoint_ts::deserialize(const std::string&str_bin) {
	std::istringstream xmli(str_bin);
	core_iarchive ia(xmli, core_arch_flags);
	shyft::time_series::dd::apoint_ts ats;
	ia >> core_nvp("ats", ats);
	return ats;
}
