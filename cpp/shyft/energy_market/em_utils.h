/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <map>

namespace shyft::energy_market {    

    using std::map;
    using std::vector;

    template <class T>// assume map<key,shared_ptr<x>>
    bool equal_map_content(const T&a, const T&b) {
        if (a.size() != b.size()) return false;
        for (const auto&kv : a) {
            auto f = b.find(kv.first);
            if (f == b.end())
                return false;
            if (!((*f->second) == (*kv.second)))
                return false;
        }
        return true;
    }


    /** @brief compare two vector<ptr<T>> for equality */
    template<typename T,typename RT>
    inline bool equal_vector_ptr_content(const std::vector<std::shared_ptr<RT>>& lhs, const std::vector<std::shared_ptr<RT>>& rhs) {
        return std::is_permutation(begin(lhs),end(lhs),begin(rhs),end(rhs),
                [](std::shared_ptr<RT> const&ca, std::shared_ptr<RT> const& cb) {
                    if constexpr ( std::is_polymorphic<T>::value && !std::is_same_v<T,RT> ) {
                        auto a=std::dynamic_pointer_cast<T>(ca);
                        auto b=std::dynamic_pointer_cast<T>(cb);
                        // equal if both are same ref, or null, or if both valid, and content equal
                        return (ca==cb) || (a && b &&( *a == *b));
                    } else { // not polymorphic, or same type
                        auto a = static_cast<const T*>(ca.get());
                        auto b = static_cast<const T*>(cb.get());
                        return (ca==cb) || (a && b &&( *a == *b));
                    }
                }
        );
    };


        
}
