/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

//-- notice that boost serialization require us to
//   include shared_ptr/vector .. etc.. wherever it's needed

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>

#include <sstream>
namespace shyft::energy_market::market {
    using std::string;
    using std::ostringstream;
    using std::istringstream;
    using std::to_string;
    using std::runtime_error;

    string model::to_blob() const {
        using namespace std;
        ostringstream xmls;
        {
            boost::archive::binary_oarchive oa(xmls);
            oa << boost::serialization::make_nvp("model", *this);
        }
        xmls.flush();
        return xmls.str();
    }
            
    shared_ptr<model> model::from_blob(const string& xml) {
        auto m = std::make_shared<model>();
        {istringstream xmli(xml); {
            boost::archive::binary_iarchive ia(xmli);
            ia >> boost::serialization::make_nvp("model", *m);
        }
        }
        //-- now we have to patch into place all
        //   weak-refs that we use.
        //   we *could* serialize the weak-refs, but
        //   this is tricky (and does not work to well on boost 1.63)
        //   so a practical approach is just to make fixes here
        for (auto&pl : m->power_lines) 
            pl->mdl = m;
        for (auto&ak : m->area) {
            ak.second->mdl = m;
            for (auto&pk : ak.second->power_modules)
                pk.second->area = ak.second;
            if (auto const& hps = ak.second->detailed_hydro)
                hps->mdl_area = ak.second;
        }

        return m;
    }

    bool model::operator==(const model& o) const {
        return id_base::operator==(o) 
            && created == o.created 
            && equal_content(o);
    }

    bool model::equal_structure(const model& b) const {
        if(area.size()!=b.area.size())
            return false;
        for(auto const&kv:area) {
            auto f=b.area.find(kv.first);
            if(f== b.area.end())
                return false;
            if(!kv.second->equal_structure(*(f->second)) )
                return false;
        }
                
        if (!std::is_permutation(begin(power_lines),end(power_lines),begin(b.power_lines),end(b.power_lines),
                [](auto const& ca,auto const& cb) {return ca==cb || (ca->equal_structure(*cb));}
                )
            )
            return false;
        return true;
    }

    bool model::equal_content(const model& b) const {
        //skip comparing model id,/name etc. and compare the equal
        if(area.size()!=b.area.size())
            return false;
        for(auto const&kv:area) {
            auto f=b.area.find(kv.first);
            if(f== b.area.end())
                return false;
            if( *(kv.second) != *(f->second)  )
                return false;
        }

        if (!std::is_permutation(begin(power_lines),end(power_lines),begin(b.power_lines),end(b.power_lines),
                [](auto const& ca,auto const& cb) {return ca==cb || ( ca&&cb &&(*ca == *cb));}
                )
            )
            return false;
        return true;
    }

    void  model_builder::validate_create_model_area(int area_id, const string& area_name){
        if (area_name.size() == 0)
            throw runtime_error("supplied area_name must be specified as a valid non-empty string");
        for (const auto& a : m->area) {
            if (a.second->name == area_name)
                throw runtime_error("the supplied area_id already exists, please supply unique area-ids within a model");
        }
        if (m->area.find(area_id) != m->area.end())
            throw runtime_error("the supplied area_name already exists, please supply unique area-names within a model");
        if (area_id <= 0)
            throw runtime_error("supplied area_id must be a positive integer");
    }

    model_area_ model_builder::create_model_area(int area_id, const string& area_name,const string& json) {
        validate_create_model_area(area_id,area_name);
        auto a= make_shared<model_area>(m,area_id, area_name,json);
        m->area[area_id] = a;
        return a;
    }

    void model_builder::validate_create_power_line( int power_line_id, const string& power_line_name,model_area_& a, model_area_& b) {
        if ((a == nullptr || b == nullptr) || (a->name == b->name && a->id == b->id))
            throw runtime_error("two non-null, and different model area objects must be supplied");
        auto fa = m->area.find(a->id);
        if (fa == m->area.end() || fa->second != a)
            throw runtime_error("The supplied area a, is not part of existing model, please add area to model before using it to establish power lines");
        auto fb = m->area.find(b->id);
        if (fb == m->area.end() || fb->second != b)
            throw runtime_error("The supplied area b, is not part of existing model, please add area to model before using it to establish power lines");
        if (power_line_name == "")
            throw runtime_error("The supplied power_line_name must be a non empty string");
        for (const auto& pl : m->power_lines) {
            if (pl->name == power_line_name)
                throw runtime_error("The supplied power_line_name already exists, , please supply unique power-line-names within a model");
        }
        if (power_line_id == 0)
            throw runtime_error("The supplied power_line_id must be a positive integer");
        for (const auto& pl : m->power_lines) {
            if (pl->id == power_line_id)
                throw runtime_error("The supplied power_line_id already exists, , please supply unique power-line-ids within a model");
        }
    }

    power_line_ model_builder::create_power_line( int power_line_id, const string& power_line_name,const string& json,model_area_& a, model_area_& b) {
        validate_create_power_line(power_line_id,power_line_name,a,b);
        auto pl = make_shared<power_line>(m,a, b, power_line_id,power_line_name,json);
        m->power_lines.push_back(pl);
        return pl;
    }

    void  model_builder::validate_create_power_module(int pm_id,const string& name,model_area_& a) {
        if (name.size() == 0)
            throw runtime_error("The power module needs a valid name, the supplied name was null");

        for (auto const &x : a->power_modules) {
            if (x.second->name == name)
                throw runtime_error(string("There already exist a power module with name in this area:") + name);
        }
        if (a->power_modules.find(pm_id) != a->power_modules.end())
                throw runtime_error(string("There already exist a power module id in this area") + to_string(pm_id));
    }

    power_module_ model_builder::create_power_module(int pm_id,const string& name,const string& json,model_area_& a) {
        validate_create_power_module(pm_id,name,a);
        auto pm = make_shared<power_module>(a,  pm_id,name,json);
        a->power_modules[pm_id] = pm;
        return pm;
    }


}
