/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>
#include <cmath>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::market {

    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::weak_ptr;
    using std::make_shared;
    using std::fabs;
        
    struct model_area;
    typedef shared_ptr<model_area> model_area_;
    typedef weak_ptr<model_area> model_area__;

    /** @brief a power-module consumes/produces power within a model-area
    *
    * Represents a consumption, production, or even outer-boundary sink/source.
    * Have certain characteristics, suitable for describing for example a
    * a nuclear power-plant. 
    * 
    * The unit have a price/volume profile = f(t, ..other time-dependent functionals)
    * so that it can participate as an actor/element in a power-market.
    *
    * Consumption, is typically based on historical data profiles, that is environment dependent( e.g. temperature),
    * as well as price-dependent sensitivity (people will switch to other non-electric energy sources if electricity is to expensive).
    *
    * A power unit is also used to describe exchange with areas external to the model, e.g. the external area is not
    * modeled, so it is described with properties suitable to match typical exchange patterns.
    *
    * For the LTM/EMPS system, the power-modules is also used as mathematical building blocks in order to build up something
    * that resembles the wanted properties of one actor within the area.
    * 
    * This introduces 'cross-power-module' references, mostly to show the user how the basic building blocks
    * forms a production/demand within the region.
    *
    */
    struct power_module:id_base {
        power_module() =default;
        power_module(model_area_ const& area, int power_module_id, const string& power_module_name, const string& other_json_data="")
            : id_base{power_module_id,power_module_name,other_json_data,{},{}},area(area){}

        bool equal_structure(const power_module& b) const;

        bool operator==(const power_module& o) const { return id_base::operator==(o); }
        bool operator!=(const power_module& o) const { return !operator==(o); }
        bool operator<(const power_module& o) const { return id < o.id ; }
        model_area_ get_area() { return area.lock(); }
        model_area__ area;/// weak ref to (parent) area for this power module
        x_serialize_decl();
    };

    typedef shared_ptr<power_module> power_module_;
    typedef shared_ptr<power_module const> power_module_c;

}

x_serialize_export_key(shyft::energy_market::market::power_module);
