/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/srv/model_info.h>
namespace shyft::energy_market::market {

    using std::string;
    using std::vector;
    using std::map;
    using std::shared_ptr;
    using std::make_shared;
    using std::weak_ptr;
    using shyft::core::utctime;
    using shyft::core::no_utctime;
    using model_info=shyft::srv::model_info;

    // fwd:
    struct model_area;
    typedef shared_ptr<model_area> model_area_;
    typedef weak_ptr<model_area> model_area__;
    typedef shared_ptr<model_area const> model_area_c;

    struct power_line;
    typedef shared_ptr<power_line> power_line_;
    typedef shared_ptr<power_line const> power_line_c;

    struct power_module;
    typedef shared_ptr<power_module> power_module_;

    /** @brief The energy market model
    *
    *  Describes the long-term-model (LTM), consisting of
    *  model-areas with power-modules, and detailed_hydro, plus power-lines, 
    *  for exchange between the model-areas (bottlenecs).
    *
    */
    struct model:id_base {
        model() = default;
        model(int id, const string& model_name, const string& json="")
            : id_base{id,model_name,json,{},{}} {}

        utctime created{no_utctime};///< when it was created
        map<int, model_area_> area; ///< area unique by id
        vector<power_line_> power_lines;///< tranmission capacity between areas

        //-- methods, - for scripting support
        bool operator==(const model& o) const;
        bool operator!=(const model&o) const { return !operator==(o); }
        bool equal_structure(const model& other) const;
        bool equal_content(const model& other) const;

        //-- xml-serialization
        string to_blob() const;
        static shared_ptr<model> from_blob(const string& xml);
        x_serialize_decl();
    };
    typedef shared_ptr<model> model_;

    /** @brief Utility to ease building a correct model 
    *
    * Provide a build-semantic layer that allows us to 
    * do some magic, and ensure the user build
    * models that are ok.
    * It ensures that the owner-ship, unique-ness of names/id's, etc. 
    * are maintained one place.
    *
    */
    struct model_builder {
        model_ m;
        template<class M>
        model_builder(M &&m) :m{std::forward<M>(m)} {}
        
        void validate_create_model_area(int area_id,const string& area_name);
        void validate_create_power_line( int id, const string& name, model_area_& a, model_area_& b);
        void validate_create_power_module(int module_id,const string& name,model_area_& a);
        
        model_area_ create_model_area(int area_id,const string& area_name,const string& json);
        power_line_ create_power_line( int id, const string& name,const string& json, model_area_& a, model_area_& b);
        power_module_ create_power_module(int module_id,const string& name,const string& json,model_area_& a);
    };

}

x_serialize_export_key(shyft::energy_market::market::model);

