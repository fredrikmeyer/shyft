/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/model.h>


namespace shyft::energy_market::market {

    power_line::power_line(shared_ptr<model>const& m,shared_ptr<model_area>& a1, shared_ptr<model_area>& a2, int id, const string& name, const string& json)
        : id_base{id,name,json,{},{}},mdl(m),area_1(a1),area_2(a2) {
        if (a1 == a2 || a1 == nullptr || a2 == nullptr)
            throw std::runtime_error("A power line must connect two different ModelAreas");
    }

    bool power_line::operator==(const power_line &o) const {
        return id_base::operator==(o) && equal_structure(o);
            // equal operator: only private equality, does not check topology/connections
    }

    bool power_line::equal_structure(const power_line&o) const {
        if(id!=o.id)
            return false;// we insist objects id's are the same
        vector<model_area_> a {get_area_1(),get_area_2()};// and that the connectivity is the same
        vector<model_area_> b {o.get_area_1(),o.get_area_2()};// .. but order or direction of connectivity is not significant
        return std::is_permutation(begin(a),end(a),begin(b),end(b),[](auto const &ca,auto const &cb){return (ca==cb)||(ca && cb && (ca->id == cb->id));});
    }

}
