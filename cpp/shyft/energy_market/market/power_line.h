/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <memory>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::market {

    using std::string;
    using std::shared_ptr;
    using std::weak_ptr;
            
    struct model_area;
    struct model;

    /** @brief a power-line interconnects the model-areas
    *
    * The power-line can be concrete physical line, or represent the effective capacity of several
    * transmission lines between two areas.
    * 
    * Most LTMs simplify the  electric transmission system to the extent that within an area, there is
    * no limitation on energy-flow, the limits are only visible between connected areas.
    *
    * A full implementation of the energy transmission-lines would require a lot more work, and details,
    * including switches, transformers between different voltage levels etc. This is done in other system
    * where the business processes are on the transmission-lines rather than the energy and market exchange.
    *
    * @see model_area, model
    *
    */
    struct power_line:id_base {
        power_line()=default;
        power_line(shared_ptr<model> const&m, shared_ptr<model_area>& a1, shared_ptr<model_area>& a2, int id, const string& name, const string& json="");

        weak_ptr<model> mdl;
        weak_ptr<model_area> area_1;
        weak_ptr<model_area> area_2;

        shared_ptr<model> get_model() { return mdl.lock(); }
        shared_ptr<model_area> get_area_1() const { return area_1.lock(); }
        shared_ptr<model_area> get_area_2() const { return area_2.lock(); }
        void set_area_1(shared_ptr<model_area>&a) { area_1 = a; }
        void set_area_2(shared_ptr<model_area>&a) { area_2 = a; }
        bool equal_structure(const power_line&o) const;
        bool operator==(const power_line& o) const;
        bool operator!=(const power_line& o) const { return !operator==(o); }
        bool operator<(const power_line& o) const { return id < o.id || (id == o.id && name < o.name); }
        x_serialize_decl();
    };

}
x_serialize_export_key(shyft::energy_market::market::power_line);
