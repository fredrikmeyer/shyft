/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
//
// 1. first include std stuff and the headers for
// files with serializeation support
//

#include <shyft/energy_market/id_base.h>

#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <shyft/srv/model_info.h>
#include <shyft/energy_market/srv/run.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time/utctime_utilities.h>
// then include stuff you need like vector,shared, base_obj,nvp etc.
#include <boost/format.hpp>

#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/unique_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/nvp.hpp>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
//
// 2. Then implement each class serialization support
//

namespace shyft::energy_market{
    
    em_handle::destroy_t em_handle::destroy=nullptr; 
    
}

using namespace boost::serialization;
using namespace shyft::core;

template <class Archive>
void shyft::energy_market::srv::run::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("created",created)
    & make_nvp("json",json)
    & make_nvp("mid",mid)
    & make_nvp("state",state)
    ;
}


template <class Archive>
void shyft::energy_market::id_base::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("json",json)
    ;
    if (file_version > 0) {
        ar
        & make_nvp("tsm",tsm)
        ;
    }
}


template <class Archive>
void shyft::energy_market::market::model::serialize(Archive & ar, const unsigned int file_version) {
    ar
        & make_nvp("id",id)
        & make_nvp("name",name)
        & make_nvp("json",json)
        & make_nvp("created",created)
        & make_nvp("area",area)
        & make_nvp("power_lines",power_lines)
        //& make_nvp("power_type_map",power_type_map)
        //& make_nvp("load_type_map",load_type_map)
        ;
}

template <class Archive>
void shyft::energy_market::market::model_area::serialize(Archive & ar, const unsigned int file_version) {
    ar
        & make_nvp("id", id)
        & make_nvp("name", name)
        & make_nvp("json",json)
        & make_nvp("power_modules", power_modules)
        & make_nvp("hps",detailed_hydro)
        ;
}

template <class Archive>
void shyft::energy_market::market::power_line::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json",json)
    & make_nvp("area_1", area_1)
    & make_nvp("area_2", area_2)
    ;
}

template <class Archive>
void shyft::energy_market::market::power_module::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json",json)
    ;
}


template <class Archive>
void shyft::energy_market::hydro_power::point::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("x", x)
    & make_nvp("y",y)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xy_point_curve::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("points", points)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::xy_point_curve_with_z::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("z", z)
    & make_nvp("xy_curve", xy_curve)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::turbine_efficiency::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("production_min", production_min)
    & make_nvp("production_max", production_max)
    & make_nvp("efficiency_curves", efficiency_curves)
    ;
    if(file_version ==0 && !Archive::is_saving::value) {
        production_nominal=production_max;
        fcr_max=production_max;//bw compat, use max/min as values for fcr
        fcr_min=production_min;
    } else {
        ar
        & make_nvp("production_nominal",production_nominal)
        & make_nvp("fcr_min",fcr_min)
        & make_nvp("fcr_max",fcr_max)
        ;
    }
}

template <class Archive>
void shyft::energy_market::hydro_power::turbine_description::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("efficiencies", efficiencies)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_power_system::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id", id)
    & make_nvp("name",name)
    & make_nvp("created",created)
    & make_nvp("reservoirs",reservoirs)
    & make_nvp("aggregates",units )
    & make_nvp("water_routes",waterways )
    & make_nvp("catchments", catchments)
    & make_nvp("power_stations",power_plants )
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_connection::serialize(Archive & ar, const unsigned int file_version) {
    ar
        & make_nvp("role", role)
        & make_nvp("target", target)
        ;
}

template <class Archive>
void shyft::energy_market::hydro_power::hydro_component::serialize(Archive & ar, const unsigned int file_version) {
    ar 
        & make_nvp("hps", hps)
        & make_nvp("id",id)
        & make_nvp("name", name)
        & make_nvp("ds", downstreams)
        & make_nvp("us", upstreams)
        ;
}

template <class Archive>
void shyft::energy_market::hydro_power::reservoir::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
    ;
}

#include <iostream>
template <class Archive>
void shyft::energy_market::hydro_power::unit::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
    ;
    if(file_version ==0 && !Archive::is_saving::value) { 
        shyft::energy_market::hydro_power::power_plant_ pp;// first version was a shared ptr.
        ar & make_nvp("station",pp);
        pwr_station=pp;// now it's a weak ptr.
    } else {
        ar & make_nvp("station",pwr_station);
    }
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::power_plant::serialize(Archive & ar, const unsigned int file_version) {
    ar
    & make_nvp("id_base",base_object<shyft::energy_market::id_base>(*this))
    & make_nvp("hps", hps)
//    & make_nvp("id", id)
//    & make_nvp("name", name)
//    & make_nvp("json", json)
    & make_nvp("aggregates",units)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::waterway::serialize(Archive& ar, const unsigned int file_version) {
    ar
    & make_nvp("hc", base_object<shyft::energy_market::hydro_power::hydro_component>(*this))
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
	& make_nvp("gates",gates)
    ;
}

template <class Archive>
void shyft::energy_market::hydro_power::gate::serialize(Archive& ar, const unsigned int file_version) {
    if(file_version ==0 && !Archive::is_saving::value) { 
        shyft::energy_market::hydro_power::waterway_ ww;// first version was a shared ptr.
        ar & make_nvp("wtr",ww);
        wtr=ww;// now it's a weak ptr.
    } else {
        ar & make_nvp("wtr",wtr);
    }
	ar 	& make_nvp("id", id)
		& make_nvp("name", name)
		& make_nvp("json", json)
		;
}

template <class Archive>
void shyft::energy_market::hydro_power::catchment::serialize(Archive& ar, const unsigned int file_version) {
	ar
    & make_nvp("id", id)
    & make_nvp("name", name)
    & make_nvp("json", json)
    & make_nvp("hps", hps)
    ;
}

//-- fix the rest:
x_serialize_instantiate_and_register(shyft::energy_market::id_base);
//x_serialize_instantiate_and_register(shyft::srv::model_info);
x_serialize_instantiate_and_register(shyft::energy_market::srv::run);
x_serialize_instantiate_and_register(shyft::energy_market::market::model);
x_serialize_instantiate_and_register(shyft::energy_market::market::model_area);
x_serialize_instantiate_and_register(shyft::energy_market::market::power_line);
x_serialize_instantiate_and_register(shyft::energy_market::market::power_module);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::point);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::xy_point_curve);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::xy_point_curve_with_z);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::turbine_efficiency);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::turbine_description);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::reservoir);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::hydro_connection);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::hydro_component);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::hydro_power_system);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::unit);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::power_plant );
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::waterway );
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::gate);
x_serialize_instantiate_and_register(shyft::energy_market::hydro_power::catchment);
