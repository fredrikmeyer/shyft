/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#pragma once
#include <string>
#include <shyft/time/utctime_utilities.h>

    namespace shyft::energy_market::srv {

    using std::string;
    using shyft::core::utctime;
    using shyft::core::no_utctime;

    enum class run_state:int8_t {
        created,
        prepare_input,
        running,
        finished_run,
        reading_results,
        frozed,
        failed
    };

    /** @brief run info, keeps info about a run
    *
    * The model information contains some mandatory fields
    * like 
    * id, a unique id for the model, and some other useful
    * or optional stuff
    * 
    * This can be shared between models, and the json part of it
    * is there to provide easy scripting flexibility.
    * 
    */
    struct run {
        int64_t id{0};///< the unique id of the run
        string name{};///< optional, often useful name
        utctime created{no_utctime};///< or modified, we might be able to keep track of this
        string json{};///< optional, for scripting support
        int64_t mid{0};///< model-id attached to the run(could be zero initially)
        run_state state{run_state::created};///< for now,  
        // py cts
        run()=default;
        run(int64_t id, string const&name,utctime created, string json=string{},int64_t mid=0,run_state state=run_state::created)
        :id{id},name{name},created{created},json{json},mid{mid},state{state}{}
        bool operator==(run const& o) const noexcept {return name==o.name && id==o.id && created == o.created && json == o.json && mid==o.mid && state==o.state;}
        bool operator!=(run const&o) const noexcept {return !operator==(o);}
        x_serialize_decl();
    };
}

x_serialize_export_key(shyft::energy_market::srv::run);
