
add_library(
    em_ui ${shyft_lib_type}  
    ui_core.cpp
    ui_serialization.cpp
    ../../web_api/ui/request_handler.cpp
)

set_target_properties(
    em_ui PROPERTIES
    ARCHIVE_OUTPUT_NAME em_ui.${SHYFT_VERSION}
    INSTALL_RPATH "$ORIGIN"
    VISIBILITY_INLINES_HIDDEN TRUE
    VERSION ${SHYFT_VERSION}
    SOVERSION ${SHYFT_VERSION_MAJOR}.${SHYFT_VERSION_MINOR}.${SHYFT_VERSION_PATCH}
)

target_link_libraries(
    em_ui
    shyft_core
    em_model_core
    stm_core
    Qt5::Widgets
    Qt5::Charts
)

target_compile_definitions(em_ui PUBLIC QT_NO_EMIT)

install( # python support
    TARGETS em_ui ${shyft_runtime}
    LIBRARY DESTINATION ${SHYFT_PYTHON_DIR}/shyft/lib NAMELINK_SKIP # installing .so on linux
    ARCHIVE DESTINATION $<TARGET_FILE_DIR:em_ui> # workaround to avoid win import lib being installed to main install prefix
    RUNTIME DESTINATION ${SHYFT_PYTHON_DIR}/shyft/lib # installing .dll on win
)

install(TARGETS em_ui   # c++ development support
    COMPONENT development
    EXPORT shyft-targets
    EXCLUDE_FROM_ALL
    LIBRARY DESTINATION ${SHYFT_INSTALL_LIBDIR} NAMELINK_SKIP
    ARCHIVE DESTINATION ${SHYFT_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${SHYFT_INSTALL_BINDIR}
)
