/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <vector>
#include <memory>
#include <stdexcept>

#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>




namespace shyft::energy_market::graph {
    using namespace shyft::energy_market::hydro_power;
    using shyft::energy_market::hydro_power::connection_role;
    using shyft::energy_market::hydro_power::reservoir;
    using shyft::energy_market::hydro_power::unit;
    using shyft::energy_market::hydro_power::waterway;

    /*
     * Functor used to restrict searches to upstream only
     */
    struct upstream_predicate {

        bool operator()(const traits::edge_descriptor e) const {
            connection_role role = e.second.role;
            return role == connection_role::input ? true : false;
        }
    };

    /*
     * Functor used to restrict searches to downstream only
     */
    struct downstream_predicate {

        bool operator()(const traits::edge_descriptor e) const {
            connection_role role = e.second.role;
            return role == connection_role::input ? false : true;
        }
    };

    class hydro_weight_map: public boost::put_get_helper<int, hydro_weight_map> {
    public:
        using category = boost::readable_property_map_tag;
        using value_type = size_t;
        using reference = int;
        using key_type = traits::edge_descriptor;

        size_t w_rsv = 1;
        size_t w_unit = 1;
        size_t w_wtr = 0;

        hydro_weight_map() = default;

        value_type operator[](const key_type& e) const {
            if (std::dynamic_pointer_cast<waterway>(e.first)) {
                return w_wtr;
            }
            if (std::dynamic_pointer_cast<unit>(e.first)) {
                return w_unit;
            }
            if (std::dynamic_pointer_cast<reservoir>(e.first)) {
                return w_rsv;
            }
            throw std::runtime_error("Cannot determine weight for this component type");
        }
    };

    template<typename C, typename EP>
    inline auto find_components_from(traits::vertex_descriptor src, int amax_dist = 0) {
        auto hps = src->hps_();
        if (!hps)
            throw std::runtime_error("Component is not part of a hydro power system");

        // explicitly casting from int to allow max_dist=-1 in Python
        using distance_t = hydro_weight_map::value_type;
        distance_t max_dist = static_cast<distance_t>(amax_dist);

        // make sure we never return unreachable vertices,
        // unreachable vertices have at least distance max_dist -1 relative to origin
        max_dist = std::min(max_dist, std::numeric_limits<distance_t>::max() - 2);

        std::vector<distance_t> dist_vec(num_vertices(*hps));
        auto index_map = boost::get(boost::vertex_index, *hps);
        auto distances = boost::make_iterator_property_map(dist_vec.begin(), index_map);
        hydro_weight_map wm;

        // we count distance as the number of reservoirs/units crossed to reach target,
        // excluding the starting point if it is a reservoir or unit
        distance_t dist_0 = 0;
        if (std::dynamic_pointer_cast<unit>(src)) dist_0 = wm.w_unit;
        if (std::dynamic_pointer_cast<reservoir>(src)) dist_0 = wm.w_rsv;


        // filter graph edges for directed search
        EP edge_predicate;
        auto fg = boost::filtered_graph(*hps, edge_predicate);

        boost::dijkstra_shortest_paths(fg, src, boost::distance_map(distances).weight_map(wm));

        // Exclude starting point from returned set;
        distances[src] = std::numeric_limits<distance_t>::max();

        std::vector<std::shared_ptr<C>> result;
        for (auto[vi, vertices_end] = vertices(fg); vi != vertices_end; ++vi) {
            if (auto c = std::dynamic_pointer_cast<C>(*vi)) {
                if (distances[c] - dist_0 <= max_dist) {
                    result.push_back(c);
                }
            }
        }
        return result;
    }

    inline auto upstream_reservoirs(traits::vertex_descriptor src, int amax_dist = 0) {
        return find_components_from<reservoir, upstream_predicate>(std::forward<traits::vertex_descriptor>(src), amax_dist);
    }
    inline auto downstream_reservoirs(traits::vertex_descriptor src, int amax_dist = 0) {
        return find_components_from<reservoir, downstream_predicate>(std::forward<traits::vertex_descriptor>(src), amax_dist);
    }
    inline auto upstream_units(traits::vertex_descriptor src, int amax_dist = 0) {
        return find_components_from<unit, upstream_predicate>(std::forward<traits::vertex_descriptor>(src), amax_dist);
    }
    inline auto downstream_units(traits::vertex_descriptor src, int amax_dist = 0) {
        return find_components_from<unit, downstream_predicate>(std::forward<traits::vertex_descriptor>(src), amax_dist);
    }
}


