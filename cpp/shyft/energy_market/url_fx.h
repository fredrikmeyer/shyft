/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <string_view>
#include <iterator>
#include <functional>
#include <shyft/mp.h>

namespace shyft::energy_market { //-- candidate for url_fx.h
    namespace hana=boost::hana;
    using mp::has_accessors;
    using mp::accessor_ptr_type;
    using mp::accessor_ptr;
    using mp::accessor_id;
    
    using sbi_t=std::back_insert_iterator<std::string>; ///< string back_inserter_iterator sbi_t for short
    using url_fx_t=std::function<void ( sbi_t& o,int levels, int template_levels,std::string_view)>; ///< function type for url generation
    
    namespace detail {
        /** has_url_fx traits defaults to false */
        template<typename T, typename = void>
        struct has_url_fx : std::false_type { };

        /** has_url_fx traits is true for members that do have .url_fx */
        template<typename T>
        struct has_url_fx<T, decltype(std::declval<T>().url_fx, void())> : std::true_type { };
        
        /** _mk_url_fx recursively patches url_fx pointers
        * 
        * @detail Ensure that all url_fx functions are initialized to functions that provide the url from the root object.
        * 
        * 
        * @tparam R the root class, like reservoir that is required to have the .generate_url(sbi_t&o, int level, int template_level)
        * @tparam A the class of the data-member somewhere owned by R (could be reservoir.volume.constraint..)
        * @param r_this the non-null pointer to the root object
        * @param a_this the non-null pointer to the member in the interior of R,
        * @param pth  the attribute path fromm root object down to here, like .volume, or volume.constraint
        * 
        */
        
        template <class R, class A> 
        void _mk_url_fx(R *r_this, A* a_this,std::string pth) { // r_this = reservoir(require .generate_url), - a_this member address within r_this, pth= path to member, like ".volume" (consider to make pth constexpr char*)
            using A_=std::remove_cv_t<A>;
            if constexpr (has_accessors(hana::type_c<A_>)) { //  stop on terminal/basic types, dive into struct-types
                if constexpr (has_url_fx<A_>::value) { // if there is url_fx on this level, ensure to patch it (we can levels that exposes no terminal attributes)
                    (*a_this).url_fx= [r_this,a_path=pth]( sbi_t&o,int level, int template_levels,std::string_view xtra)->void {
                        if(level)
                            r_this->generate_url(o,level-1,template_levels?template_levels-1:template_levels);
                        if(template_levels!=0) {
                            auto xpath=a_path+std::string(xtra);
                            std::copy(std::begin(xpath),std::end(xpath),o);//  r_this capture by value(ok, ptr), a_path, also by value (kind of ok, could be constexpr. ..)
                        }
                    };
                }
                hana::for_each(hana::accessors<A_>(), [r_this,a_this,pth](auto acc) { // now iterate over members to see if there is more levels to dive into
                    if constexpr (has_accessors(accessor_ptr_type(acc))) {
                        auto aa_this=&accessor_ptr(acc)(*a_this); // could consider second arg by ref, and drop this.
                        auto a_path=pth+hana::to<const char*>(hana::string_c<'.'>+accessor_id(acc)); // the path to this attribute is the attribute path from root object, plus the .member.
                        _mk_url_fx(r_this,aa_this,a_path);// recurse into child-aggregate
                    }
                });
            }
        }
    }
    
    /** mk_url_fx ensures (nested) members of root class R have proper url_fx functions patched in
     * 
     * @detail You insert a call to this function in _all_ constructors, mk_url_fx(this) for all main stm-objects, like reservoir, unit etc.
     * 
     * @tparam R root level object, like reservoir, unit etc. with nested structures
     * @param r_this the this pointer of the R, root object
     * 
     */
    template<class R>
    void  mk_url_fx(R*r_this) { // to be used by root-objects, to instrument contained, nested, types. require R to have .generate_url(...), 
        using R_=std::remove_cv_t<R>;
        if constexpr (has_accessors(hana::type_c<R_>)) { //  ok, just safe-guarding,
            hana::for_each(hana::accessors<R_>(), [r_this](auto acc) { // iterate on top-level aggregates members
                if constexpr (has_accessors(accessor_ptr_type(acc))) { // the member is a hana-struct, so it (most likely) need to be instrumented with url_fx
                    auto a_this=&accessor_ptr(acc)(*r_this); // could consider second arg by ref, and drop this.
                    std::string a_path=hana::to<const char*>(hana::string_c<'.'>+accessor_id(acc));
                    detail::_mk_url_fx(r_this,a_this,a_path);
                }
            });
        }
    };
}
