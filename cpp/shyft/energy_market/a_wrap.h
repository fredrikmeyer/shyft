#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <optional>
#include <shyft/energy_market/url_fx.h>
#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/core/math_utilities.h>

namespace shyft::energy_market {
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_axis::generic_dt;
    namespace attr_traits {

        // int8_t
        inline bool equal(std::int8_t a, std::int8_t b) {return a==b;}
        inline bool exists(std::int8_t) {return true;} // always!
        inline void reset(std::int8_t& a) {a=0;} // default value 0

        // int16_t
        inline bool equal(std::int16_t a, std::int16_t b) {return a==b;}
        inline bool exists(std::int16_t) {return true;} // always!
        inline void reset(std::int16_t& a) {a=0;} // default value 0

        // uint16_t
        inline bool equal(std::uint16_t a, std::uint16_t b) {return a==b;}
        inline bool exists(std::uint16_t) {return true;} // always!
        inline void reset(std::uint16_t& a) {a=0;} // default value 0

        // int32_t
        inline bool equal(std::int32_t a, std::int32_t b) {return a==b;}
        inline bool exists(std::int32_t) {return true;} // always!
        inline void reset(std::int32_t& a) {a=0;} // default value 0

        // int64_t
        inline bool equal(std::int64_t a, std::int64_t b) {return a==b;}
        inline bool exists(std::int64_t) {return true;} // always!
        inline void reset(std::int64_t& a) {a=0;} // default value 0

        // double
        inline bool equal(double a, double b) {return shyft::core::nan_equal(a,b);}
        inline bool exists(double) {return true;} // always!
        inline void reset(double& a) {a=shyft::nan;} // default value nan

        // bool
        inline bool equal(bool a, bool b) {return a==b;}
        inline bool exists(bool) {return true;} // always!
        inline void reset(bool& a) {a=false;} // default value false

        // string
        inline bool equal(std::string a, std::string b) {return a==b;}
        inline bool exists(std::string const& a) {return !a.empty();}
        inline void reset(std::string& a) {a=std::string();}

        // apoint_ts
        inline bool equal(apoint_ts const& a, apoint_ts const& b) {return a==b;}
        inline bool exists(apoint_ts const& a) {return a.ts.get();}
        inline void reset(apoint_ts& a) {a=apoint_ts();}

        // time_axis::generic_dt
        inline bool equal(generic_dt const& a, generic_dt const& b) {return a==b;}
        inline bool exists(generic_dt const& a) {return a != generic_dt{};}
        inline void reset(generic_dt& a) {a=generic_dt{};}

        // any shared_ptr
        template <class T>
        inline bool equal(std::shared_ptr<T> const& a, std::shared_ptr<T> const& b) {return (!a&&!b)||(a&&b&&*a==*b);}
        template <class T>
        inline bool exists(std::shared_ptr<T> const& a) {return a.get();}
        template <class T>
        inline void reset(std::shared_ptr<T>& a) {a.reset();}

        // any vector
        template <class T>
        inline bool equal(std::vector<T> const& a, std::vector<T> const& b) {return a==b;}
        template <class T>
        inline bool exists(std::vector<T> const& a) {return a.size()>0;}
        template <class T>
        inline void reset(std::vector<T>& a) {a.clear();}

        // any optional
        template <class T>
        inline bool equal(std::optional<T> const &a, std::optional<T> const &b) {return a==b;}
        template <class T>
        inline bool exists(std::optional<T> const &a) {return a.has_value();}
        template <class T>
        inline void reset(std::optional<T>& a) {a.reset();}

        // any boost optional, since we struggle with boost::serialization//
        template <class T>
        inline bool equal(boost::optional<T> const &a, boost::optional<T> const &b) {return a==b;}
        template <class T>
        inline bool exists(boost::optional<T> const &a) {return a.has_value();}
        template <class T>
        inline void reset(boost::optional<T>& a) {a.reset();}
    }

    /** attr_wrap provides means of providing attributes that are capable of providing the 'url' 
     * 
     * @details
     * The url of an attribute is used to make expressions/templates/data-bindings.
     * E.g. the user can uses python expressions like this
     *  reservoir.volume.result.url(), and result is apoint_ts.
     * So we need to provide something to python that appears as an apoint_ts, but do have extra
     * properties to support the other stuff.
     * 
     * We keep the function<> url_fx_t that knows in a type-erased manner how to  produce
     * the parent url for the given temporary a_wrap object
     * The attribute value is kept by value ref, so that assignment works as if it was the attribute itself.
     * Since the only template-parameter type is A, the need for python exposed classes
     * is equal to value-type, e.g. apoint_ts, xy-curve etc.
     * 
     * @tparam A the type of the attribute
     * 
     */
    template <typename A>
    struct a_wrap {
        url_fx_t url_fx; ///< the callable url_fx, provided by the python attribute expose of the a_wrap<T>.. 
        std::string a_name;///< the visible attribute name in python.
        A& a;///< attribute object reference, to location within the  owning object(might be nested aggregate in e.g. unit,)

        a_wrap()=delete;///< don't allow default construct.
        a_wrap(url_fx_t &&url_fx,std::string a_name,A& a):url_fx{url_fx},a_name{a_name},a{a} {}
        a_wrap(const a_wrap &c):url_fx{c.url_fx},a_name{c.a_name},a{c.a} {}
        a_wrap(a_wrap&&c):url_fx{std::move(c.url_fx)},a_name{c.a_name},a{c.a} {}

        /** @brief generate an almost unique, url-like string for a proxy exposed attribute.
         * 
         * @param prefix: What the resulting string will start with
         * @param levels: How many levels of the url to include.
         *      levels== 0 includes only this level. (Use levels < 0 to get all levels)
         * @param placeholders: The last element of the vector states whether to use the attribute ID or place_holder
         *  "{attr_id}". The remaining vector will be used in subsequent levels of the url.
         *  If the vector is empty, the function defaults to not using placeholder in the url
         */
        std::string url(std::string prefix="",int levels=-1,int template_levels=-1) const {
            std::string s;
            auto sbi=std::back_inserter(s);
            std::copy(begin(prefix),end(prefix),sbi);
            url_fx(sbi,levels,template_levels,""); // assume it's an internal nested type with the url_fx member available
            std::string attr_part= template_levels==0?std::string("{attr_id}"):a_name;
            return (boost::format("%1%.%2%")%s%attr_part).str(); 
        }

        /** @brief exits returns true if the attribute is kind of non-null, false otherwise */
        bool exists() const {
            return attr_traits::exists(a);
        }
        /** @brief remove() nullfies the attribute, so that after call .exists() ==false */
        void remove() {
            attr_traits::reset(a);
        }
        /** @brief allow assignment to held attribute */
        a_wrap& operator=(A const&v) {a=v;return *this;}

        /** @brief allow auto cast from held attribute to it's type */
        operator A() const {return a;};

        /** @brief equal operator, propagate to the attribute type */
        bool operator==(a_wrap const&b) const { 
            return attr_traits::equal(a,b.a);
        }
        bool operator!=(a_wrap const&other) const {return !operator==(other);};
    };

    /** creates a a_wrap aka proxy-attribute 
     * 
     * To be used in closed/short contexts, like stack auto variables etc.
     * 
     *  typical use: like
     * auto a=proxy_attr(rsv.level,"schedule",rsv.level.schedule);
     * a.url(...) etc.
     * 
     * @note that owner ref should always outlive the life-time of the returned object!
     * 
     * @param owner_ref reference to the closest owning struct of the attribute
     * @param attr_name attribute name of the property
     * @param attr_ref attribute reference.
     * @return a_wrap<decltype(attr_ref)> as proxy attribute.
     */
    constexpr auto proxy_attr=[](auto& owner_ref, std::string const & attr_name, auto& attr_ref) {
        return a_wrap<decltype(attr_ref)>(
            [&owner_ref](sbi_t& so,int levels,int template_levels,std::string_view sv) {
                if constexpr (detail::has_url_fx<decltype(owner_ref)>::value) {
                    owner_ref.url_fx(so,levels,template_levels,sv);
                } else {
                    if(levels) 
                        owner_ref.generate_url(so,levels-1,template_levels>0?template_levels-1:template_levels);
                }
            },
            attr_name,
            attr_ref
        );
    };

}
