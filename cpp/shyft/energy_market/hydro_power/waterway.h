/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/core/core_serialization.h>

#include <shyft/energy_market/hydro_power/hydro_component.h>

namespace shyft::energy_market::hydro_power {

    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::weak_ptr;
    using std::make_shared;

    struct gate;
    using gate_ = shared_ptr<gate>;

    /** @brief waterway - a river or a tunnel.
    *
    * Hydrology:
    *    If multiple inputs, it's a junction.
    *    If multiple outputs:
    *          For distributing flow to several turbines inside
    *          the power-house (tunnel scenario).
    *          For rivers, - could be supported, but
    *          currently it is not supported, workaround is reservoir in between.
    *    If it's a river, time-delays, volume, and routing characteristics might apply
    *    If it's a tunnel, friction-loss coefficients might apply
    *
    *
    *    In a power system, the *tunnels* can split, but then it's always pressure-balance
    *    and the water-flow might go both ways depending on the hydrostatic pressure.
    *    On the inputs, there might be hatches/gates that controls the amount of flow.
    *
    *    If the water_route_(river) input is connected to a reservoir, the amount of flow could
    *    be dependent on the gate-opening/characteristics, the reservoir level, as well as the
    *    reservoir level at the output side. The gate-opening might be variable to allow for
    *    a certain amount of flow related to restrictions/concession requirements(fish-water).
    *
    *    If the water_route_ is a tunnel, then it might still be gates/hatches at the input that limits the flow.
    *    Usually (assume always), it's either open or closed.
    *
    *   @ref http://www.ivt.ntnu.no/ept/fag/tep4195/innhold/Forelesninger/forelesninger%202006/5%20-%20Hydro%20Power%20Plants.pdf
    */
    struct waterway :hydro_component {
        waterway()=default;
        waterway(int id,const string& name,const string& json="", hydro_power_system_ const& hps=nullptr) : hydro_component{id,name,json,hps}{};
        ~waterway();
        vector<gate_> gates; ///< gates control amount of water into this water_route
        hydro_component_ downstream() const { return downstreams.size() ? downstreams[0].target_() : nullptr; }
        hydro_component_ upstream() const { return upstreams.size() ? upstreams[0].target_() : nullptr; }
        static void add_gate(waterway_ const&w, gate_ const& g);
        void remove_gate_ptr(gate* g);
        void remove_gate(gate_ const& g) { return remove_gate_ptr(g.get()); }
        shared_ptr<waterway> shared_from_this() const;
        bool operator==(waterway const& o)const;
        bool operator!=(waterway const& o) const { return !operator==(o); }
        static waterway_ const& input_from(waterway_ const&me, waterway_ const& w);
        static waterway_ const& input_from(waterway_ const&me, unit_ const& p);
        static waterway_ const& input_from(waterway_ const&me, reservoir_ const& r, connection_role role);
        static waterway_ const& input_from(waterway_ const&me, reservoir_ const& r);
        static waterway_ const& output_to(waterway_ const&me, waterway_ const& w);
        static waterway_ const& output_to(waterway_ const&me, reservoir_ const& r);
        static waterway_ const& output_to(waterway_ const&me, unit_ const& p);
        connection_role upstream_role();///< the role the water way has relative to the component above
        x_serialize_decl();
    };
    using waterway_ = shared_ptr<waterway>;
    using waterway__ = weak_ptr<waterway>;

    /** @brief A gate controls the amount of flow into the water-route
    *
    * Gates in tunnels are usually open/closed, since leaving it half-open would
    * lead to fall-height losses and less optimal utiliziation of water.
    *
    * Gates releated to reservoirs, bypass, or flood water-routes,
    * is usually controlled, using gate-opening, to
    * give a certain amount of flow, or to keep a certain water-level.
    *
    * The gate-flow in such cases can be quite complex, and depend on:
    *  1. gate-opening ( that leads to a friction-loss component)
    *  2. reservoir level of the reservoir in the 'upstream'-direction
    *  3. reservoir level of the reservoir in the 'downstream'-direction
    *
    * Modelling cases
    *
    *  uni-directional:
    *    gate to water-route(river), where downstream reservoir is lower, representing no effect on flow
    *
    *  bidirectional:
    *    gate to water-route(river), where downstream is reservoir, potentially overlapping levels
    *    gate to water-route(tunnel), junction,  where closure reservoirs, potentially overlapping levels
    *
    * @note computing correct gate-flow accurately can be complex, e.g. given multiple gates that could
    *       have mutual effects. In case they are significant, special care is needed.
    */
    struct gate:id_base {
        waterway__ wtr; ///< waterroute this gate belongs to

        gate();
        virtual ~gate();
        gate(int id, const string& name, const string& json = "");

        waterway_ wtr_() const { return wtr.lock(); }
        hydro_power_system_ hps_() const {
            auto ww=wtr_();
            return ww?ww->hps_():nullptr;
        }

        gate_ shared_from_this()  const; ///< shared from this using hps.gates shared pointers.

        //waterway_ & wtr_() { return wtr; }
        // compute_flow(opening...)->m3/s,
        //or
        // compute_friction_loss(opening...)-> m /(m3/s)^2
        bool operator==(gate const&o) const { return id_base::operator==(o); }
        bool operator!=(gate const&o) const { return !operator==(o); }
        x_serialize_decl();
    };

    /** hydro_connect for easy readable type-safe connections
    *
    * allow us to write hydro_connect(rsv).input_from(wtr).output_to(wtr2) etc.
    *
    */

    template <class T>
    struct hydro_connect {
        shared_ptr<T> me;
        hydro_connect(shared_ptr<T> const& me) :me(me) {}
        template <class I>
        hydro_connect &input_from(shared_ptr<I> const&i) { T::input_from(me, i); return *this; }
        template <class I>
        hydro_connect &input_from(shared_ptr<I> const&i, connection_role cr) { T::input_from(me, i, cr); return *this; }

        template <class O>
        hydro_connect &output_to(shared_ptr<O> const& o) { T::output_to(me, o); return *this; }
        template <class O>
        hydro_connect &output_to(shared_ptr<O> const& o, connection_role cr) { T::output_to(me, o, cr); return *this; }
    };
    template <class T>
    hydro_connect<T> connect(shared_ptr<T> const& me) { return hydro_connect<T>(me); }

}

//x_serialize_export_key(shyft::energy_market::hydro_power::waterway);
BOOST_CLASS_EXPORT_KEY2(shyft::energy_market::hydro_power::waterway, BOOST_PP_STRINGIZE(shyft::energy_market::hydro_power::water_route));
x_serialize_export_key(shyft::energy_market::hydro_power::gate);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::gate,1);// shared to weak ptr.
