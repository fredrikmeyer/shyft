/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <sstream>
#include <stdexcept>
#include <algorithm>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/catchment.h>
#include <boost/format.hpp>
// serialization for hydro power system
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/nvp.hpp>
#include <shyft/energy_market/em_utils.h>



namespace shyft::energy_market::hydro_power {

    using std::runtime_error;
    using std::map;
    using std::istringstream;
    using std::ostringstream;
    using std::dynamic_pointer_cast;
            
    hydro_power_system::hydro_power_system(string a_name) :name(a_name) {}
    
    void hydro_power_system::populate(set<hydro_component_> &collection) {
        for (auto &component : collection) {
            auto res = dynamic_pointer_cast<reservoir>(component);
            auto wr = dynamic_pointer_cast<waterway>(component);
            auto agg = dynamic_pointer_cast<unit>(component);
            if (res) {
                reservoirs.push_back(res);
                continue;
            }
            if (wr) {
                waterways.push_back(wr);
                continue;
            }
            if (agg) {
                units.push_back(agg);
            }
        }

        bool found;
        for (auto &agg : units) {
            found = false;
            auto ps = agg->pwr_station_();
            for (auto &p : power_plants) {
                if (ps->id == p->id) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                power_plants.push_back(ps);
            }
        }

        for (auto &ps : power_plants) {
            for (auto &agg : ps->units) {
                auto el = std::find(units.begin(), units.end(), agg);
                if (el == units.end()) {
                    units.push_back(agg);
                }
            }
        }
    }

    void hydro_power_system::populate(set<hydro_component_> &collection, hydro_power_system_builder &hpsb) {
        map<int, reservoir_> nres; 
        map<int, unit_> nagg;
        map<int, waterway_> nwr;
        for (auto &comp : collection) {
            auto res = std::dynamic_pointer_cast<reservoir>(comp);
            auto wr = std::dynamic_pointer_cast<waterway>(comp);
            auto agg = std::dynamic_pointer_cast<unit>(comp);
            if (res) {
                if (res->name.compare("havet") != 0) {
                    auto _res = hpsb.create_reservoir(res->id, res->name, res->json);
                    nres.insert(std::pair<int, reservoir_>(_res->id, _res));
                }
                continue;
            }
            if (wr) {
                auto _wr = hpsb.create_waterway(wr->id, wr->name, wr->json);
                nwr.insert(std::pair<int, waterway_>(_wr->id, _wr));
                continue;
            }
            if (agg) {
                auto ps = agg->pwr_station_();
                if (!any_of(begin(power_plants), end(power_plants), [&ps](const auto& p)->bool {return p->name == ps->name; })) {
                    auto _ps = hpsb.create_power_plant(ps->id, ps->name, ps->json);
                    for (auto &_agg : ps->units) {
                        if (_agg->name != agg->name) {
                            auto agg_ = hpsb.create_unit(_agg->id, _agg->name, _agg->json);
                            _ps->add_unit(_ps, agg_);
                        }
                    }
                    auto agg_ = hpsb.create_unit(agg->id, agg->name, agg->json);
                    _ps->add_unit(_ps, agg_);
                    nagg.insert(std::pair<int, unit_>(agg_->id, agg_));
                }
            }
        }
        auto havet = hpsb.create_reservoir(0, "havet");
        nres.insert(std::pair<int, reservoir_>(havet->id, havet));

        for (auto &comp : collection) {
            auto wr = std::dynamic_pointer_cast<waterway>(comp);
            if (wr) {
                for (auto &connection : wr->upstreams) {
                    //auto role = connection.role;
                    auto target = connection.target;

                    auto res = std::dynamic_pointer_cast<reservoir>(target);
                    auto _wr = std::dynamic_pointer_cast<waterway>(target);
                    auto agg = std::dynamic_pointer_cast<unit>(target);
                    if (res) {
                        nwr[wr->id]->input_from(nwr[wr->id], nres[res->id], wr->upstream_role());
                        continue;
                    }
                    if (_wr) {
                        nwr[wr->id]->input_from(nwr[wr->id], nwr[_wr->id]);
                        continue;
                    }
                    if (agg) {
                        nwr[wr->id]->input_from(nwr[wr->id], nagg[agg->id]);
                    }
                }
                for (auto &connection : wr->downstreams) {
                    //auto role = connection.role;
                    auto target = connection.target;

                    auto res = std::dynamic_pointer_cast<reservoir>(target);
                    auto _wr = std::dynamic_pointer_cast<waterway>(target);
                    auto agg = std::dynamic_pointer_cast<unit>(target);
                    if (res) {
                        nwr[wr->id]->output_to(nwr[wr->id], nres[res->id]);
                        continue;
                    }
                    if (_wr) {
                        nwr[wr->id]->output_to(nwr[wr->id], nwr[_wr->id]);
                        continue;
                    }
                    if (agg) {
                        nwr[wr->id]->output_to(nwr[wr->id], nagg[agg->id]);
                    }
                }
            }
        }
    }

    void hydro_power_system::clear() {
        for (auto&c : reservoirs)
            c->clear();
        for (auto&c : waterways)
            c->clear();
        for (auto&c : units )
            c->clear();
        for (auto&c : catchments)
            c->clear();
        power_plants.clear();
        reservoirs.clear();
        waterways.clear();
        units.clear();
        catchments.clear();
    }
    hydro_power_system::~hydro_power_system() {
        clear();
    }
    vector<gate_> hydro_power_system::gates() const {
        vector<gate_> r;
        for(auto const&w:waterways)
            for(auto const&g:w->gates)
                r.push_back(g);
        return r;
    }
    reservoir_ hydro_power_system_builder::create_reservoir(int id,const string& name, const string& json) {
        if (any_of(begin(hps->reservoirs), end(hps->reservoirs), [&name](const auto&r)->bool {return r->name == name;}))
            throw runtime_error((boost::format("reservoir name must be unique within a hydro_power_system:%1%")%name).str());//is that reasonable ?
        auto r = make_shared<reservoir>(id,name,json,hps);
        hps->reservoirs.push_back(r);
        return r;
    }
	
    unit_ hydro_power_system_builder::create_unit (int id,const string& name, const string& json) {
        if (any_of(begin(hps->units ), end(hps->units ), [&name](const auto& p)->bool {return p->name == name;}))
            throw runtime_error((boost::format("unit_ name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto p = make_shared<unit>(id,name, json,hps);
        hps->units.push_back(p);
        return p;
    }
    power_plant_ hydro_power_system_builder::create_power_plant (int id,const string& name, const string& json) {
        if (any_of(begin(hps->power_plants ), end(hps->power_plants ), [&name](const auto& p)->bool {return p->name == name;}))
            throw runtime_error((boost::format("power_station name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto p = make_shared<power_plant>(id,name, json,hps);
        hps->power_plants.push_back(p);
        return p;
    }

    waterway_ hydro_power_system_builder::create_waterway(int id,const string& name, const string& json) {
        if (any_of(begin(hps->waterways), end(hps->waterways), [&name](const auto&w)->bool {return w->name == name;}))
            throw runtime_error((boost::format("waterway_ name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto w = make_shared<waterway>(id,name, json,hps);
        hps->waterways.push_back(w);
        return w;
    }

    gate_ hydro_power_system_builder::create_gate(int id,const string& name, const string& json) {
        auto gates=hps->gates();
        if (any_of(begin(gates), end(gates), [&name](const auto&w)->bool {return w->name == name;}))
            throw runtime_error((boost::format("gate_ name must be unique within a HydroPowerSystem:%1%")% name).str());//is that reasonable ?
        auto w = make_shared<gate>(id,name, json);
        return w;
    }

    catchment_ hydro_power_system_builder::create_catchment(int id,const string& name, const string& json) {
        if (any_of(begin(hps->catchments), end(hps->catchments), [&name](const auto&w)->bool {return w->name == name; }))
            throw runtime_error((boost::format("catchment name must be unique within a HydroPowerSystem:%1%") % name).str());//is that reasonable ?
        auto w = make_shared<catchment>(id,name, json, hps);
        hps->catchments.push_back(w);
        return w;
    }

    waterway_ hydro_power_system_builder::create_tunnel(int id,const string& name, const string& json) {
        return create_waterway(id,name,json);
    }

    waterway_ hydro_power_system_builder::create_river(int id,const string& name, const string& json) {
        return create_waterway(id,name,json);
    }

    reservoir_ hydro_power_system::find_reservoir_by_name(const string& name) const {return find_by_name(reservoirs, name);}
    unit_ hydro_power_system::find_unit_by_name (const string& name) const {return find_by_name( units, name);}
    power_plant_ hydro_power_system::find_power_plant_by_name (const string& name) const {return find_by_name( power_plants, name);}
    waterway_ hydro_power_system::find_waterway_by_name(const string& name) const {return find_by_name(waterways, name);}
    gate_ hydro_power_system::find_gate_by_name(const string& name) const {
        auto gts=gates();
        return find_by_name(gts, name);
    }
    catchment_ hydro_power_system::find_catchment_by_name(const string& name) const {return find_by_name(catchments, name);}

    reservoir_ hydro_power_system::find_reservoir_by_id(int64_t id) const { return find_by_id(reservoirs, id); }
    unit_ hydro_power_system::find_unit_by_id (int64_t id) const { return find_by_id( units, id); }
    power_plant_ hydro_power_system::find_power_plant_by_id (int64_t id) const { return find_by_id( power_plants, id); }
    waterway_ hydro_power_system::find_waterway_by_id(int64_t id) const { return find_by_id(waterways, id); }
    gate_ hydro_power_system::find_gate_by_id(int64_t id) const { 
        auto gts=gates();
        return find_by_id(gts, id); 
        
    }
    catchment_ hydro_power_system::find_catchment_by_id(int64_t id) const { return find_by_id(catchments, id); }


    /* Verify that system a and b are structurally 
    * equal.
    * This means almost all attributes& relations, 
    * exceptions are typically db-identifiers/created/modified time.
    */
    bool hydro_power_system::equal_structure(hydro_power_system const & b) const {
        auto const & a=*this;

        /** returns true if the vector<ptr_obj>s are same set, and have equal structure */
        auto is_equal_structure=[](auto const&av, auto const &bv) {
            return std::is_permutation(begin(av),end(av),begin(bv),end(bv),[](auto const&ca,auto const &cb) {return ca->equal_structure(*cb);});
        };

        return
            is_equal_structure(a.units,b.units) &&
            is_equal_structure(a.reservoirs,b.reservoirs) &&
            is_equal_structure(a.waterways,b.waterways) &&
            is_equal_structure(a.power_plants,b.power_plants) &&
            is_equal_structure(a.catchments,b.catchments) ;

    }

    bool hydro_power_system::equal_content(hydro_power_system const& b) const {
        auto const & a=*this;

        /** returns true if the vector<ptr_obj>s are same set, including each object equal to each other */
        auto is_same_set_of_objects=[](auto const&av, auto const &bv) {
            return std::is_permutation(begin(av),end(av),begin(bv),end(bv),[](auto const&ca,auto const &cb) {return *ca == *cb;});
        };

        return
            is_same_set_of_objects(a.units,b.units) &&
            is_same_set_of_objects(a.reservoirs,b.reservoirs) &&
            is_same_set_of_objects(a.waterways,b.waterways) &&
            is_same_set_of_objects(a.power_plants,b.power_plants) &&
            is_same_set_of_objects(a.catchments,b.catchments) ;

    }

    bool hydro_power_system::operator==(hydro_power_system const&o) const {
        if(id!=o.id || name!=o.name || created != o.created)
            return false;
        return equal_content(o);
    }

    std::string hydro_power_system::to_blob(hydro_power_system_ const &hps) {
        using namespace std;
        ostringstream xmls;
        {
            boost::archive::binary_oarchive oa(xmls);
            oa << boost::serialization::make_nvp("hps", hps);
        }
        xmls.flush();
        return xmls.str();
    }

    hydro_power_system_ hydro_power_system::from_blob(std::string xmls) {
        shared_ptr<hydro_power_system> hps;
        istringstream xmli(xmls); {
        boost::archive::binary_iarchive ia(xmli);
        ia >> boost::serialization::make_nvp("hps", hps);
        }
        return hps;
    }

}
