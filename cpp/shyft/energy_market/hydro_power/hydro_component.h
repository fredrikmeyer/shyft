/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <memory>
#include <string>
#include <vector>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/id_base.h>

namespace shyft::energy_market::hydro_power {

    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using std::weak_ptr;
            
    using shyft::core::utctime;

    /** connection role for the first segment of a possibly longer list water-routes*/
    enum connection_role {
        main, ///< a main waterroute, usually a production water-route
        bypass,///< a bypass, in the meaning of bypass relative a hydro-aggregate/or production unit
        flood,///< water escaping a reservoir uncontrolled, at high water-levels
        input///< input, to a hydro-component
    };

    // fwd decl, 
    // conventions: 
    // _ for shared ptr,  -due to python, we use shared_ptr to ease interface and shared ownership/ref-counting
    // __ for weak_ptr
    // _c a shared pointer to a const object (hmm)
    struct hydro_power_system;
    typedef shared_ptr<hydro_power_system> hydro_power_system_;
    typedef weak_ptr<hydro_power_system> hydro_power_system__;
    typedef shared_ptr<hydro_power_system const > hydro_power_system_c;

    struct hydro_component;
    typedef shared_ptr<hydro_component> hydro_component_;
    typedef weak_ptr<hydro_component> hydro_component__;
    typedef shared_ptr<hydro_component const > hydro_component_c;
            
    struct waterway;
    typedef shared_ptr<waterway> waterway_;
    typedef shared_ptr<waterway const> waterway_c;

    struct gate;
    typedef shared_ptr<gate> gate_;
    typedef shared_ptr<gate const> gate_c;

    struct reservoir;
    typedef shared_ptr<reservoir> reservoir_;
    typedef shared_ptr<reservoir const > reservoir_c;
    
    struct unit;
    typedef shared_ptr<unit> unit_;
    typedef shared_ptr<unit const> unit_c;

    struct power_plant;
    typedef shared_ptr<power_plant> power_plant_;
    typedef weak_ptr<power_plant> power_plant__;
    typedef shared_ptr<power_plant const> station_c;

    /** @brief A hydro connection to connect hydro-components
    *
    * A hydro-component have a flexible number of upstream and downstream connections.
    * each of them describes the intended role that connections plays.
    * 
    * A connection consists of a reference to the target component 
    * and the connection-role that connection plays relative 'this' object.
    *
    * The connection objects are always two-way, and mutual, so if there is a path from 
    * a reservoir 'R' to a a tunnel 'T',
    * The reservoir.downstream points to tunnel, and the tunnel.upstream points to 
    * reservoir.
    * 
    * \see hydro_component
    *
    */
    struct hydro_connection {
        hydro_connection(connection_role a_role, const hydro_component_& a_target):role(a_role),target(a_target) { }
        hydro_connection() =default;
        connection_role role{connection_role::main};// e.g. : You are at a reservoir, downstream bypass to..
        hydro_component_ target;// weak reference to  reservoir|aggregate_|water_route_
        //const hydro_component_& target_()  const { return target; }
        //hydro_component_& target_()  { return target; }
        hydro_component_ target_()  const { return target; }
        //hydro_component_ target_() const { return target.lock(); } // weak->shared target, could be nullptr | None in python
        //bool has_target() const { return !target.expired();}
        bool has_target() const { return target != nullptr;}
        bool operator==(const hydro_connection&o) const {
            return role == o.role && target_() == o.target_();// equal by ptr. 
        }
        bool operator!=(const hydro_connection&o) const { return !operator==(o); }
        bool operator<(const hydro_connection&o)const { return int(role) < int(o.role); }
        x_serialize_decl();
    };

    /** @brief Base for hydro connectable components
    *
    * Contains the functionality and rules for hydro components, reservoir, water-routes and aggregates(turbine..)
    * A hydro component have a unique id, a name (could be unique), up-stream and down-stream connections.
    * 
    * Notice that connections are mutual pairs, so if 'A' is upstream 'B', then 'B' is downstream 'A'.
    *
    * like this:
    *
    *   A
    *      .downstream[0] (main)--> B
    *
    *   B
    *      .upstream[0](input) --> A
    *
    *
    * About up-stream/down-stream:
    *
    *  -# It's a convention, - water can flow in both directions(reservoir ..tunn.. reservoir, determined by levels).
    *  -# It's questionable if we really need up/down, or we could simplify structure with just connection(and use current state to determine up/down..)
    *  -# The convention is 'major-upstream/down-stream' where water can flow bidirectional.
    *  -# It's used to traverse from hydro-component in some 'known'/intended direction.
    * 
    */
    struct hydro_component:id_base {
        protected:
            hydro_component() = default;//serialization only
        public:
        hydro_component(int id, const string& a_name, const string& json="", hydro_power_system_ a_hps=nullptr)
            : id_base{id,a_name,json,{},{}},hps{a_hps} {}
        void clear();
        virtual ~hydro_component() ;
        hydro_component(const hydro_component&) = delete;
        hydro_component(hydro_component&&) = delete;
        hydro_component& operator=(const hydro_component&) = delete;

        //python access hydro_power_system 
        hydro_power_system_ hps_() const { return hps.lock(); }
        hydro_power_system__ hps; ///< reference up to the 'owning' hydro-power system.
        vector<hydro_connection> upstreams; ///< upstream connections
        vector<hydro_connection> downstreams; ///< downstream connections.

        /** returns true if the other hydro component have the same hydro-connects,name,id as this*/
        virtual bool equal_structure(hydro_component const& o) const;

        //-- connection rules that enforces correct mutual connects/disconnects
        static void connect(const hydro_component_& upstream, connection_role u_role, const hydro_component_& downstream, connection_role d_role);
        static void connect(const reservoir_& upstream, connection_role u_role, const waterway_& downstream);
        static void connect(const unit_& upstream, const waterway_& downstream);
        static void connect(const waterway_& upstream_tunnel, const unit_& power_station);
        static void connect(const waterway_& upstream, const reservoir_& downstream);
        static void connect(const waterway_& upstream, const waterway_& junction);
        static void disconnect(const hydro_component_& c1, const hydro_component_& c2);
        void disconnect_from(hydro_component&o);
        x_serialize_decl();
    };
    
    /** locate a shared_ptr<T>  from T* m searching through v, or return nullptr */
    template <class T>
    shared_ptr<T> shared_from_me(T const *me,vector<shared_ptr<T>> const&v) {
        for(auto const& p:v) if(p.get()==me) return p;
        return nullptr;
    }

} 
x_serialize_export_key(shyft::energy_market::hydro_power::hydro_connection);
x_serialize_export_key(shyft::energy_market::hydro_power::hydro_component);
