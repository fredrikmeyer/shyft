/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <boost/format.hpp>
#include <stdexcept>
#include <algorithm>
#include <typeinfo>

namespace shyft::energy_market::hydro_power {

    using std::runtime_error;
        
    void hydro_component::connect(const reservoir_& upstream, connection_role u_role, const waterway_& downstream) {
        if (u_role == connection_role::input)
            throw runtime_error((boost::format("Legal water_route reservoir roles are main,bypass,flood, supplied role was illegal %1%")%u_role).str());
        if (downstream->upstreams.size() > 0)
            throw runtime_error((boost::format("water_route take only input from one reservoir: The %1% input is already connected to %2%")% downstream->name% downstream->upstreams[0].target_()->name).str());
        connect(upstream, u_role, downstream, connection_role::input);
    }
    void hydro_component::connect(const unit_& upstream, const waterway_& downstream) {
        // We allow this: the output from a turbine can be connected to the same downstream waterroute
        //                        or.. we could disallow it and require a draft-segment, that again is allowed to go into the same waterroute.
        //                        for now, we allow multiple aggregates(turbines) to have a common outlet path.
        //if (downstream->upstreams.size() > 0)
        //    throw runtime_error((boost::format("water_route_ take only input from one aggregate_: The %1% input is already connected to %2%")% downstream->name% downstream->upstreams[0].target_()->name).str());
        if (upstream->downstreams.size() > 0)
            throw runtime_error((boost::format("aggregate can only have one output connected: The %1% output is already connected to %2%")% upstream->name% upstream->downstreams[0].target_()->name).str());
        connect(upstream, connection_role::main, downstream, connection_role::input);
    }
    void hydro_component::connect(const waterway_& upstream_tunnel, const  unit_& power_station) {
        if (power_station->upstreams.size() > 0)
            throw runtime_error((boost::format("aggregate take only input from one water_route_: The %1% input is already connected to %2%")% power_station->name% power_station->upstreams[0].target_()->name).str());
        if (upstream_tunnel->downstreams.size() > 0)
            throw runtime_error((boost::format("water_route_ have only one main output: The %1% output is already connected to %2%")% upstream_tunnel->name% upstream_tunnel->downstreams[0].target_()->name).str());
        connect(upstream_tunnel, connection_role::main, power_station, connection_role::input);
    }
    void hydro_component::connect(const waterway_& upstream, const  reservoir_& downstream) {
        if (upstream->downstreams.size() > 0)
            throw runtime_error((boost::format("water-route output can only be connected to one object: The %1% output is already connected to %2%")% upstream->name % upstream->downstreams[0].target_()->name).str());
        connect(upstream, connection_role::main, downstream, connection_role::input);
    }
    void hydro_component::connect(const waterway_& upstream, const waterway_& junction) {
        // This is allowed: a tunnel is allowed to split into several tunnel-segments downstream, typical penstock-split inside a power-station.
        // if (upstream->downstreams.size() > 0)
        //    throw runtime_error((boost::format("water-route have only one main output: The %1% output is already connected to %2%")% upstream->name%upstream->downstreams[0].target_()->name).str());
        connect(upstream, connection_role::main, junction, connection_role::input);
    }

    void hydro_component::disconnect(const hydro_component_& c1, const hydro_component_& c2) {
        c1->disconnect_from(*c2);
        /*
        c1->downstreams.erase(remove_if(c1->downstreams.begin(), c1->downstreams.end(), [&c2](const auto&c)->bool { return c.target_() == c2;}),c1->downstreams.end());
        c2->downstreams.erase(remove_if(c2->downstreams.begin(), c2->downstreams.end(), [&c1](const auto&c)->bool { return c.target_() == c1;}),c2->downstreams.end());
        c1->upstreams.erase(remove_if(c1->upstreams.begin()  , c1->upstreams.end()  , [&c2](const auto&c)->bool { return c.target_() == c2;}),c1->upstreams.end());
        c2->upstreams.erase(remove_if(c2->upstreams.begin()  , c2->upstreams.end()  , [&c1](const auto&c)->bool { return c.target_() == c1;}),c2->upstreams.end());
        */
    }

    bool hydro_component::equal_structure(hydro_component const& o) const {
        // Check that local structure is the same
        // 1: check that basic own attributes are the same
        if(this == &o) return true;// same ref, always equal
        if(id != o.id) return false;// because we insist on same object id's
        // 1: Check that we have the same component type
        if (typeid(*this) != typeid(o))
            return false;


        // 2: Check that the up- and downstream connections have the same role and target, and possibly target type
        constexpr auto equal_hydro_connection_predicate = [] (const auto& c1, const auto& c2) -> bool {
            constexpr auto equal_targets=[](auto const &a, auto const&b) {
                return (a==nullptr && b== nullptr) ||((a && b && typeid(*a)==typeid(*b)) && a->id == b->id);//not checking type-id..might be ok, it is the same structure.
            };
            return c1.role == c2.role && equal_targets(c1.target,c2.target);
        };

        if (!std::is_permutation(upstreams.begin(), upstreams.end(),
                                 o.upstreams.begin(), o.upstreams.end(),
                                 equal_hydro_connection_predicate))
            return false;


        if (!std::is_permutation(downstreams.begin(), downstreams.end(),
                                 o.downstreams.begin(), o.downstreams.end(),
                                 equal_hydro_connection_predicate))
            return false;

        return true;
    }

    void hydro_component::connect(const hydro_component_& upstream, connection_role u_role, const hydro_component_& downstream, connection_role d_role) {
        if (upstream == nullptr || downstream == nullptr)
            throw runtime_error("Only connect to non-nullptr components are allowed");

        if (upstream->hps_() != downstream->hps_()) {
            throw runtime_error((boost::format("Only components within the same system are allowed to interconnect,\t component1 system=%1%\n\t component2 system %2%") %
                (upstream->hps_() != nullptr ? upstream->hps_()->name : string("none")) % (downstream->hps_() != nullptr ? downstream->hps_()->name : string("none"))).str());
        }
        upstream->downstreams.push_back(hydro_connection(u_role, downstream));
        downstream->upstreams.push_back(hydro_connection(d_role, upstream));
    }
    /** get rid of all references between this and the other object o */
    void hydro_component::disconnect_from(hydro_component& c2) {
        auto& c1=*this;
        c1.downstreams.erase(remove_if(c1.downstreams.begin(), c1.downstreams.end(), [&c2](const auto&c)->bool { return c.target_().get() == &c2;}),c1.downstreams.end());
        c2.downstreams.erase(remove_if(c2.downstreams.begin(), c2.downstreams.end(), [&c1](const auto&c)->bool { return c.target_().get() == &c1;}),c2.downstreams.end());
        c1.upstreams.erase(remove_if(c1.upstreams.begin()  , c1.upstreams.end()  , [&c2](const auto&c)->bool { return c.target_().get() == &c2;}),c1.upstreams.end());
        c2.upstreams.erase(remove_if(c2.upstreams.begin()  , c2.upstreams.end()  , [&c1](const auto&c)->bool { return c.target_().get() == &c1;}),c2.upstreams.end());
    }
    void hydro_component::clear() {
        while(upstreams.size()) {
            disconnect_from(*(upstreams.back().target_()));
        }
        while(downstreams.size()) {
            disconnect_from(*(downstreams.back().target_()));
        }
        //for (auto&us : upstreams)
            //  disconnect(us.target, shared_from_this());
        //for (auto&ds : downstreams)
            //  disconnect(ds.target, shared_from_this());
        upstreams.clear();
        downstreams.clear();
        hps.reset();
    }
    hydro_component::~hydro_component() {
        clear();
    }


}
