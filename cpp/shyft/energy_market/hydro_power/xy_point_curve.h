/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <vector>
#include <cmath>
#include <memory>
#include <shyft/core/core_serialization.h>
#include <boost/math/special_functions/relative_difference.hpp>

#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/transform_spline_ts.h>

namespace shyft::energy_market::hydro_power {

    using std::vector;
    using std::shared_ptr;
    using std::fabs;
    using boost::math::epsilon_difference;

    using shyft::time_series::dd::apoint_ts;
    //using shyft::time_series::dd::ats_vector;
    using shyft::time_series::dd::interpolation_scheme;

    constexpr double max_epsilon=2.0;// double less than 2 eps are equal

    /** practically equal to max_epsilon relative difference, including both nans */
    inline bool nan_equal(double a,double b) noexcept {
        if(!std::isfinite(a) && !std::isfinite(b))
            return true;
        if(std::isfinite(a) &&std::isfinite(b))
            return epsilon_difference(a,b)<max_epsilon;
        return false;
    }

    /** point as in context of hydro-power, describing curve-shapes,
    * 
    * it's used as building block for the xy- or z-xy curves.
    *
    */
    struct point {
        point(double x, double y) : x(x),y(y) {}
        point()=default;
        point(const point&) = default;
        point(point&&) = default;
        point& operator=(const point&c) {x=c.x;y=c.y;return *this;}
        bool operator==(const point& o) const { return nan_equal(x,o.x) && nan_equal(y,o.y); }
        bool operator != (const point&o) const { return !operator==(o); }
        bool operator < (const point&o) const {
            if(nan_equal(x,o.x)) {
                return y<o.y;
            } else {
                return x<o.x;
            }
        }
        double x{ 0.0 };
        double y{ 0.0 };
        x_serialize_decl();
    };

    /** @brief a curve described using points, piecewise linear
    *
    *  xy_point_curve,  conceptually y=F(x), and x=Finv(y)
    *  represented as two or more points, piecewise linear between points
    *   + extrapolated values at the end.
    *  Used as base building block for
    *    a) Reservoir Volume description (and extra requirement is asc masl/Mm3 here..)
    *    b) Pure Overflow description, corresponding to classic overflow gate ( extra requirement,masl/m3/s is ascending)
    *    c) Generator effiency curve ( and now it's now the Finv(y) does not map to one value..)
    *  List of xy_point_curves, 3D functions, x=f(z,y)
    *    d) A set of xy_point_curves is used for turbine effiency curves, each curve for a referernce height. eff= f(reference_height,m3/s)
    *    e) A set of xy_point_curves is used for complex flow curve tables between reservoirs flow= f(gateopening, difference_in_water_levels)
    *    f) A set of xy_point_curves is used for to describe flow between reservoirs, R1,R2 , flow= f( R1.level, (R1-R2).level)
    *  The class needs points with X-ascending & unique (sorting points at the input).
    *  
    */
    struct xy_point_curve {
        vector<point> points;///< point in a point based description(could be functional..)
        explicit xy_point_curve(const vector<point>& points);
        xy_point_curve(const vector<double>&x, const vector<double>& y);
        xy_point_curve() = default;

        /** @return true if we have two ordered points or more,
        *  and X,Y are increasing, suitable for reservoir curves check..
        */
        bool is_xy_mono_increasing() const;
    
        /** @return true if we have two ordered points or more,
        *  and y is strictly increasing or decreasing.
        *  This property is required for invertibility.
        */
        bool is_xy_invertible() const;

        /** @return true if we have two ordered points or more,
        *  and every point (x, y) is on the convex boundary.
        */
        bool is_xy_convex() const;

        /** Interpreting the points, figure out Y, given X
        * note1:extrapolating the points if asking outside range.
        *
        * @param from_x the x-coordinate
        * @return the calculated y-value
        */
        double calculate_y(double from_x) const;
        apoint_ts calculate_y(const apoint_ts& x, interpolation_scheme = interpolation_scheme::SCHEME_LINEAR) const;

        /** Inverse of calculate_y
        *
        * @note clip input to 0.0 (hmm!)
        * @param from_y y-value
        * @return the calculated x-value
        */
        double calculate_x(double from_y) const;
        apoint_ts calculate_x(const apoint_ts& y, interpolation_scheme = interpolation_scheme::SCHEME_LINEAR) const;
        bool operator==(const xy_point_curve& o) const { return points == o.points; }
        bool operator!=(const xy_point_curve& o) const { return !operator==(o); }
        x_serialize_decl();
    };
    using xy_point_curve_ = shared_ptr<xy_point_curve>;

    /** Represents a 3d curve, suitable for variable gate etc.
    * To build A 3D curve, x=f(z,y)
    *  we have a list if these,
    * but the interpretation of them are
    * different, gate flow, deltaMeter flow etc.
    */

    struct xy_point_curve_with_z {

        xy_point_curve xy_curve;
        xy_point_curve_with_z() = default;// for boost::py
        xy_point_curve_with_z(const xy_point_curve& c, double z) : xy_curve(c), z(z) {}
    
        /** The Z (or Reference) value is used when this xy_point_curve
        * plays as a member of a set of curves that describes a 3D
        * function space, like
        * x= f(z,y)
        */
        double z{ 0.0 };

        bool operator==(const xy_point_curve_with_z& o) const {
            return xy_curve==o.xy_curve && nan_equal(z,o.z);
        }
        bool operator!=(const xy_point_curve_with_z& o) const { return !operator==(o); }
        x_serialize_decl();
    };
    using xyz_point_curve_ = shared_ptr<xy_point_curve_with_z>;

    /**
     * @brief turbine efficiency
     *
     * @details
     * Defined by a set of efficiency curves, one for each net head, with an optional production min/max limit.
     * For Pelton turbines there is one turbine efficiency object for each needle combination.
     * For other turbines there is only one turbine effieciency object, describing the entire turbine.
     * The production min/max limits are only used for turbines that have 'illegal working areas', as well as
     * helping shop out modelling pelton turbines (needle combination).
     *
     */
    struct turbine_efficiency {
        turbine_efficiency() = default;
        explicit turbine_efficiency(const vector<xy_point_curve_with_z>& c) : efficiency_curves{c} {}
        turbine_efficiency(const vector<xy_point_curve_with_z>& c, double pmin, double pmax) : efficiency_curves{c}, production_min{pmin}, production_max{pmax}, production_nominal{pmax}, fcr_min{pmin}, fcr_max{pmax} {}
        turbine_efficiency(const vector<xy_point_curve_with_z>& c, double pmin, double pmax, double pnom, double fmin, double fmax) : efficiency_curves{c}, production_min{pmin}, production_max{pmax}, production_nominal{pnom}, fcr_min{fmin}, fcr_max{fmax} {}
        vector<xy_point_curve_with_z> efficiency_curves; ///< one or more z .. eff (xy)
        double production_min{ 0.0 };///< min production, might be more strict than eff-curve
        double production_max{ 0.0 };///< max production, might be more strict than eff-curve
        double production_nominal{ 0.0 };///< nominal production, the installed/rated/nameplate capacity
        double fcr_min{0.0};///< fcr reserve min production, might be less strict than eff-curve prod_min (lower allowed)
        double fcr_max{0.0};///< fcr reserve max production, might be less strict than eff-curve prod_max (higher allowed)
        bool operator==(const turbine_efficiency& o) const {
            return efficiency_curves == o.efficiency_curves
                && nan_equal(production_min, o.production_min) && nan_equal(production_max, o.production_max)
                && nan_equal(production_nominal, o.production_nominal)
                && nan_equal(fcr_min, o.fcr_min) && nan_equal(fcr_max, o.fcr_max)
                ;
        }
        bool operator!=(const turbine_efficiency& o) const { return !operator==(o); }
        x_serialize_decl();
    };

    /** @brief turbine description
    *
    * Complete description of all turbine efficiencies.
    * For Pelton turbines there are multiple sets of efficiency curves; one set for each needle combination,
    * where each of them may contain multiple efficiency curves; one for each net head.
    * For other turbines there is only one set of efficiency curves, describing the entire turbine;
    * one for each net head.
    *
    */
    struct turbine_description {
        turbine_description() = default;
        explicit turbine_description(const vector<turbine_efficiency>& c) : efficiencies(c) {}
        vector<turbine_efficiency> efficiencies; ///< one or more z .. eff for each needle-comb
        bool operator==(const turbine_description& o) const { return efficiencies == o.efficiencies; }
        bool operator!=(const turbine_description& o) const { return !operator==(o); }
        x_serialize_decl();
    };
    using turbine_description_ = shared_ptr<turbine_description>;

    double x_min(const xy_point_curve& xy);
    double x_max(const xy_point_curve& xy);
    double y_min(const xy_point_curve& xy);
    double y_max(const xy_point_curve& xy);

    double x_min(const xy_point_curve_with_z& xyz);
    double x_max(const xy_point_curve_with_z& xyz);
    double y_min(const xy_point_curve_with_z& xyz);
    double y_max(const xy_point_curve_with_z& xyz);

    double x_min(const vector<xy_point_curve_with_z>& xyz);
    double x_max(const vector<xy_point_curve_with_z>& xyz);
    double y_min(const vector<xy_point_curve_with_z>& xyz);
    double y_max(const vector<xy_point_curve_with_z>& xyz);
    double z_min(const vector<xy_point_curve_with_z>& xyz);
    double z_max(const vector<xy_point_curve_with_z>& xyz);
}

x_serialize_export_key(shyft::energy_market::hydro_power::point);
x_serialize_export_key(shyft::energy_market::hydro_power::xy_point_curve);
x_serialize_export_key(shyft::energy_market::hydro_power::xy_point_curve_with_z);
x_serialize_export_key(shyft::energy_market::hydro_power::turbine_efficiency);
x_serialize_export_key(shyft::energy_market::hydro_power::turbine_description);
BOOST_CLASS_VERSION(shyft::energy_market::hydro_power::turbine_efficiency,1);
