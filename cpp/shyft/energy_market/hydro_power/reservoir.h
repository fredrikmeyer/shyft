/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/core/core_serialization.h>

#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>

namespace shyft::energy_market::hydro_power {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::weak_ptr;
    using std::make_shared;
    
    struct reservoir_aggregate;
    using reservoir_aggregate_=shared_ptr<reservoir_aggregate>;
    using reservoir_aggregate__=weak_ptr<reservoir_aggregate>;

    /** @brief A reservoir, stores water 
    *
    * Any reservoir of any size, including those practically 0 (open tunnel inlets, creeks).
    *
    *  Hydrology: It takes multiple inputs from WaterRoutes, and have unlimited
    *            outputs to WaterRoutes via roles main,flood, bypass
    *              each of these WaterRoutes might have hatches/gates on their input-side that controls the amount of flow
    *            It do have a volume-curve, - mas -> Mm3
    */
    struct reservoir : hydro_component {
        reservoir() = default;
                
        // copy and assign is problematic
        // from a semantic point of view
        // due to hydro-connections.
        // -- maybe provide a limited copy ?
        reservoir(const reservoir&) = delete;
        reservoir& operator=(const reservoir&) = delete;

        //--
        reservoir(int id, const string& name, const string& json="", hydro_power_system_ hps=nullptr)
            : hydro_component{id,name,json,hps}{};
        reservoir_ shared_from_this() const;
        static reservoir_ const& input_from(reservoir_ const& me, waterway_ const& r);
        static reservoir_ const& output_to(reservoir_ const& me, waterway_ const& w, connection_role role);

        //bool equal_structure(const reservoir& o) const;
        bool operator==(const reservoir& o) const;
        bool operator!=(const reservoir&o)const { return !operator==(o); }
        x_serialize_decl();
    };
    
}

x_serialize_export_key(shyft::energy_market::hydro_power::reservoir);
