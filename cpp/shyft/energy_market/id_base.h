/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <map>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/em_handle.h>

namespace shyft::energy_market {

    using std::string;
    using std::map;
    using shyft::time_series::dd::apoint_ts;

    /**
    * @brief identity and common stuff for python enabled components
    * 
    * Usually the id is unique within the scope (e.g. area, hydro-power-system).
    * The name could also be unique, depending on context.
    * The tsm is for storing additinal time series, extending the regular set
    * of attributes exposed as individual members on specialized class.
    * The json is for storing arbitrary data, that is primarly handled in python.
    */
    struct id_base {
        int64_t id{0};///< unique, in the scope of context (type/level) etc.
        string name;///< the name of the object
        string json;///< json, to be used by the python-side
        map<string,apoint_ts> tsm;///< custom time series
        em_handle h;///< handle to external python objects
        bool operator==(const id_base&o) const {return id==o.id && name==o.name && json==o.json && tsm==o.tsm;}
        bool operator!=(const id_base&o) const {return !operator==(o);}
        bool operator <(const id_base&o) const {return name < o.name;}
        
        //--- python support
        int64_t get_id() const {return id;}
        void set_id(int64_t v) {id=v;}
        
        const string& get_name() const {return name;}
        void set_name(const string& v) {name=v;}

        const string& get_json() const {return json;}
        void set_json(const string& v) {json=v;}

        const map<string,apoint_ts>& get_tsm() const {return tsm;}
        void set_tsm(const map<string,apoint_ts>& v) {tsm=v;}

        x_serialize_decl();
    };
    
};
x_serialize_export_key(shyft::energy_market::id_base);
BOOST_CLASS_VERSION(shyft::energy_market::id_base, 1); // tsm
