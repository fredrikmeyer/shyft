#pragma once
/** This file is part of Shyft. Copyright 2015-2021 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <string>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/web_api/energy_market/tp_id.h>

namespace shyft::energy_market::stm {
struct stm_system;//fwd
namespace srv::dstm {
    struct server;//fwd for the dstm
    using shyft::time_series::dd::apoint_ts;
    using shyft::web_api::grammar::tp_id;

    /** @brief Provides resolving url to attributes time_series on the dstm model
     * @details
     * This class is used in the dstm server in to resolve/find time_series
     * within the stm system model, based on specified url.
     * The dstm_ts_url_grammar requires a resolver function as input,
     * and this particular class is used within the 
     * dstm server to help the dtss getting access to the stm model time_series.
     * This allow us to use expressions that involves attribute-references on the
     * model.
     */
    struct ts_url_resolver {
        server * srv{nullptr};///< reference to the server that is using this resolver, no ownership etc.
        ts_url_resolver(server * srv):srv(srv){}
        apoint_ts operator() ( const std::string& mid, std::vector<tp_id> const &cpth, const std::string& attr_id);
    };

    /** @brief Provides enforcing scoped read-only ts_url_resolver
     * @details
     */
    struct scoped_ts_url_resolver {
        stm_system const * mdl{nullptr};///< reference to the resolved/locked stm-system for the specified mid
        std::string mid; ///< the model id, as, in dstm://Mmymodel, matching the mdl above
        scoped_ts_url_resolver(stm_system const * mdl,std::string const&mid):mdl{mdl},mid{mid}{}
        apoint_ts operator() ( const std::string& mid, std::vector<tp_id> const &cpth, const std::string& attr_id);
    };

    /** @brief Provides enforcing scoped read-only ts_url_resolver
     * @details
     */
    struct scoped_ts_url_resolver_setter {
        stm_system * mdl{nullptr};///< reference to the resolved/locked stm-system for the specified mid
        std::string mid; ///< the model id, as, in dstm://Mmymodel, matching the mdl above
        apoint_ts v;///<the time-series to assign to the resolved attribute
        scoped_ts_url_resolver_setter(stm_system* mdl,std::string const&mid):mdl{mdl},mid{mid}{}
        apoint_ts operator() ( const std::string& mid, std::vector<tp_id> const &cpth, const std::string& attr_id);
    };

}
}
