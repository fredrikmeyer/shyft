#include <shyft/energy_market/stm/srv/dstm/server.h>

#include <signal.h> // For custom handling of SIGSEGV
#include <setjmp.h> // For longjmp and setjmp. See https://stackoverflow.com/questions/8401689/best-practices-for-recovering-from-a-segmentation-fault
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/hana.hpp>
#include <shyft/core/core_archive.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>

#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_resolver.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_generator.h>

#include <shyft/mp.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/compute_node.h>
#include <shyft/version.h>


namespace shyft::energy_market::stm::srv::dstm {
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_arch_flags;

    namespace mp = shyft::mp;
    namespace hana = boost::hana;

    dlib::logger server::slog{"dstm"};

    //atomic_uint shop_segfault_handler::curr_opt = 0;
    //bool shop_segfault_handler::sigsegv_received = false;
    thread_local bool shop_segfault_handler::shop_call = false;

    static jmp_buf buf;
    static void(*old_handler)(int sig)=nullptr;
    /** @brief Any shop call from server, on any thread, should now be enclosed in 
     * 
     * if (!setjmp(buf)) {
     *   seg_handler.shop_call = true;
     *   ...Do stuff with shop...
     *   seg_handler.shop_call = false;
     * } else {
     *   ...This code block is run if SIGSEGV is received from shop...
     * }
     */
    void shop_segfault_handler::sigsegv_handler(int signum) {
            if (shop_call) {
                if (!sigsegv_received) // We haven't encountered a SIGSEGV from SHOP before
                    sigsegv_received = true;
                longjmp(buf, curr_opt);
            } else {
                if(old_handler)
                    (*old_handler)(signum);
            }
    }
    
    shop_segfault_handler::shop_segfault_handler() {
        curr_opt++;
        if (curr_opt > 0) {
            old_handler=signal(SIGSEGV, sigsegv_handler);
        }
    }

    server::~server() {}

    shop_segfault_handler::~shop_segfault_handler() {
        if (curr_opt) curr_opt--;
        if (curr_opt == 0) {
            signal(SIGSEGV, SIG_DFL); //we could use old_handler here, but choose to just use default.
        }
    }

    server::server() {
        setup_dtss();
    }
    
    /** start the server in background, return the listening port used in case it was set unspecified */
    int server::start_server() {
        if(get_listening_port()==0) {
            
            start_async();
            while(is_running()&& get_listening_port()==0) //because dlib do not guarantee that listening port is set
                std::this_thread::sleep_for(std::chrono::milliseconds(10)); // upon return, so we have to wait until it's done
        } else {
            start_async();
        }
        auto port_num = get_listening_port();
        // for debug/trace in c++ : slog.set_level(dlib::LINFO);
        slog << dlib::LINFO << "Started server on port " << port_num;
        return port_num;
    }

    /** Set up the dtss */
    void server::setup_dtss() {
        // in slave mode, all non dstm reads are passed to the master
        dtss = make_unique<ts_server>([this](const id_vector_t& ts_ids, utcperiod p) -> ts_vector_t { return this->dtss_read_callback(ts_ids, p); });
    }
    
    /** add container to dtss */
    void server::add_container(const string& container_name, const string& root_dir) {
        if (!dtss)
            throw runtime_error("Dtss hasn't been set. Call server::setup_dtss() before adding container.");
        dtss->add_container(container_name, root_dir);
    }

    
    /** @brief callback function for reading dstm:// time series */
    ts_vector_t server::dtss_read_callback(const id_vector_t& ts_ids, utcperiod /*p*/) {
        ts_vector_t r;r.reserve(ts_ids.size());
        shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{ts_url_resolver(this)};
        for (auto const&ts_id : ts_ids) {
            apoint_ts ts;
            auto ok_parse = shyft::web_api::grammar::phrase_parser(ts_id.c_str(), pth_, ts);
            if (ok_parse) {
                auto rts=std::dynamic_pointer_cast<aref_ts const>(ts.ts);
                if(rts) {
                    ts=apoint_ts(rts->rep);//strip off the ref.name, only return ts.
                }
                r.push_back(ts);
            } else
                throw std::runtime_error(string("Unable to parse ts_id '") + ts_id + "'");
        }
        return r;
    }

    void server::do_set_ts(string const&mid,ats_vector const&tsv) {
        auto ctx = do_get_context(mid);// assuming
        // srv_unique_lock ul(ctx->mtx);// lock model while reading, we trust user to have heterogen set ts-urls refs to mid..
        // note: assume the context mid, is already write-locked.
        scoped_ts_url_resolver_setter resolve(ctx->mdl.get(),mid);
        for (auto const& tsx : tsv) {
            apoint_ts target;// not used actually, but needed for the phrase_parser arg.
            string ts_id=tsx.id();
            // here we could consider checking that tsx was of type aref_ts etc.
            auto rts=std::dynamic_pointer_cast<time_series::dd::aref_ts const>(tsx.ts);
            if(rts ) {
                resolve.v= rts->rep?apoint_ts{rts->rep}:tsx;//bw compat:allow pure unbound series to be set., otherwise strip off sym part.
            } else {
                resolve.v=tsx;// this is the value to assign, allowing to put into place unbound symrefs.
            }
            shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{resolve};
            auto ok_parse = shyft::web_api::grammar::phrase_parser(ts_id.c_str(), pth_, target);
            if (ok_parse) {
                // if target is concrete ts, then assign values
                // else if it's empty/null.. assign the arg.
                // if it's an expression, fail it..
                if(dtss) dtss->sm->notify_change(ts_id);// we notify on each item, because of possible exception
            } else {
                throw std::runtime_error(string("Unable to parse ts_id '") + ts_id + "'");
            }
        }
    }
    void server::do_lock_set_ts(string const&mid,ats_vector const&tsv) {
        auto ctx = do_get_context(mid);
        srv_unique_lock ul(ctx->mtx);// lock model while reading, we trust user to have heterogen set ts-urls refs to mid..
        do_set_ts(mid,tsv);
    }

    ats_vector server::do_get_ts(const string &mid,const vector<string> &ts_ids) {
        auto ctx = do_get_context(mid);// assuming
        srv_shared_lock ul(ctx->mtx);// lock model while reading, we trust user to have heterogen set ts-urls refs to mid..
        utcperiod p;// not yet used.
        ts_vector_t r;r.reserve(ts_ids.size());
        shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{scoped_ts_url_resolver{ctx->mdl.get(),mid}};
        for (auto const& ts_id : ts_ids) {
            apoint_ts ts;
            auto ok_parse = shyft::web_api::grammar::phrase_parser(ts_id.c_str(), pth_, ts);
            if (ok_parse)
                r.push_back(ts);
            else
                throw std::runtime_error(string("Unable to parse ts_id '") + ts_id + "'");
        }
        return r;
    }

    void server::set_kill_flag(const std::string& mid) {
        auto ctx = do_get_context(mid);
        if(ctx->state >= model_state::setup && ctx->state <=model_state::finished) {
            ctx->killed = true;
            slog << dlib::LINFO << "setting kill signal for " << mid;
        }
    }

    bool server::has_kill_flag(const std::string& mid) {
        auto ctx = do_get_context(mid);
        return ctx->killed;
    }

    void server::clear_kill_flag(const std::string& mid) {
        auto ctx = do_get_context(mid);
        ctx->killed=false;
    }

    void server::raise_sigterm() {
        slog << dlib::LINFO << "Killing process by raising sigterm...";
        unique_lock<mutex> sl(srv_mx);
        model_map.clear();// to better support testing..at some risk, just clear model_map, as if we was dying.
        raise(SIGTERM);// in test mode, this is captured, and counted.., in real mode, the process is terminated and k8s/pod restart it
    }

    void server::set_master(string ip, int port,double master_poll_time,size_t unsubscribe_min_threshold,double unsubscribe_max_delay) {
        if (!dtss)
            throw runtime_error("Dtss hasn't been set. Call server::setup_dtss() before adding container.");
        dtss->set_master(ip,port,master_poll_time, unsubscribe_min_threshold, unsubscribe_max_delay);
    }

    
    /** @brief get current api version */
    string server::do_get_version_info(){
        return std::to_string(shyft::_version.major)+'.'+std::to_string(shyft::_version.minor)+'.' +std::to_string(shyft::_version.patch);
    }

    optimization_summary_
    server::do_get_optimization_summary(const string &mid) {
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if (i == model_map.end()){
            slog << dlib::LERROR << "do_get_optimization_summary: Unable to find model '" << mid << "'";
            throw runtime_error("dstm: not able to find model '" + mid + "'");
        }

        return i->second->mdl->summary;
    }

    /** @brief create a new model with id */
    bool server::do_create_model(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "create_model: Model with name '" << mid << "' already exists.";
            throw runtime_error("dstm: model with specified name '"+mid+"' already exists, please remove it before (re)create");
        }
        model_map[mid]=make_context(model_state::idle, make_shared<stm_system>());
        slog << dlib::LINFO << "Successfully created model '" << mid << "'";
        return true;
    }

    /** @brief add existing model with id */
    bool server::do_add_model(string const& mid, stm_system_ mdl){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "add_model: Model with name '" << mid << "' already exists";
            throw runtime_error("dstm: model with specified name '"+mid+"' already exists, please remove it before (re)add");
        }
        model_map[mid]=make_context(model_state::idle, mdl);
        slog << dlib::LINFO << "Successfully added model '" << mid << "'";
        return true;
    }

    /** @brief remove (free up mem etc) model by id */
    bool server::do_remove_model(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if(i==model_map.end()) {
            slog << dlib::LERROR << "remove_model: No model with name '" << mid << "'";
            throw runtime_error("dstm: no model with specified name '"+mid +"'");
        }
        model_map.erase(mid);
        slog << dlib::LINFO << "Successfully removed model '" << mid << "'";
        return true;
    }
    

    /** @brief rename a model by id */
    bool server::do_rename_model(string old_mid, string new_mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(new_mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "rename_model: Model with name '" << new_mid << "' already exists";
            throw runtime_error("dstm: model with specified name '"+new_mid+"' already exists");
        }
        i=model_map.find(old_mid);
        if(i==model_map.end()) {
            slog << dlib::LERROR << "rename_model: Unable to find model '" << old_mid << "'";
            throw runtime_error("dstm: not able to find model '"+old_mid+"'");
        }
        auto ctx_old = (*i).second;
        model_map.erase(old_mid);
        model_map[new_mid] = ctx_old;
        slog << dlib::LINFO << "Successfully renamed '" << old_mid << "' --> '" << new_mid << "'";
        return true;
    }

    bool rebind_expression(stm_system&mdl,string const& newm_key);
    /** @brief clone existing model with id */
    bool server::do_clone_model(string const& old_mid, string new_mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(new_mid);
        if(i!=model_map.end()) {
            slog << dlib::LERROR << "clone_model: Model with name '" << new_mid << "' already exists";
            throw runtime_error("dstm: model with specified name '"+new_mid+"' already exists");
        }
        i=model_map.find(old_mid);
        if(i==model_map.end()) {
            slog << dlib::LERROR << "clone_model: Unable to find model '" << old_mid << "'";
            throw runtime_error("dstm: not able to find model '"+old_mid+"'");
        }
        auto old_mdl = (*i).second->mdl;
        auto new_mdl = stm_system::clone_stm_system(old_mdl);
        model_map[new_mid] = make_context(model_state::idle, new_mdl);
        rebind_ts(*new_mdl, new_mid); ///< Wherever there are references to attributes in old_mdl, replace with new_mdl.
        slog << dlib::LINFO << "Successfully cloned model '" << old_mid << "' == '" << new_mid << "'";
        return true;
    }

    /** @brief get models, returns a string list with model identifiers */
    vector<string> server::do_get_model_ids() {
        vector<string> r;
        unique_lock<mutex> sl(srv_mx);
        for(auto e=model_map.begin();e!=model_map.end();++e)
            r.push_back(e->first);
        slog << dlib::LTRACE << "Returning all model IDs";
        return r;
    }

    map<string, model_info> server::do_get_model_infos() {
        map<string, model_info> mis;
        unique_lock<mutex> sl(srv_mx);
        for(auto e=model_map.begin(); e != model_map.end(); ++e){
            auto mdl = e->second->mdl;
            auto key = e->first;
            mis[key] = model_info(mdl->id, mdl->name, no_utctime, mdl->json);
        }
        slog << dlib::LTRACE << "Returning info for all models";
        return mis;
    }

    
    /** @brief get model with its context */
    stm_system_context_ server::do_get_context(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if (i == model_map.end()){
            slog << dlib::LERROR << "get_context: Unable to find model '" << mid << "'";
            throw runtime_error("dstm: not able to find model '" + mid + "'");
        }
        return i->second;
    }

    void server::do_set_state(string const& mid, model_state const& state){
        auto ctx = do_get_context(mid);
        //shared_lock sl(*((ctx->mtx_))); // Thread safety is here ensured by state attribute being atomic
        ctx->state = state;
        slog << dlib::LTRACE << "set_state: State of '" << mid << "' is now " << (int)state;
    }
    
    model_state server::do_get_state(string const& mid){
        unique_lock<mutex> sl(srv_mx);
        auto i=model_map.find(mid);
        if (i == model_map.end()){
            slog << dlib::LERROR << "get_state: Unable to find model '" << mid << "'";
            throw runtime_error("dstm: not able to find model '" + mid + "'");
        }
        return i->second->state;
    }
    stm_system_ server::do_get_model(string const & mid){
        auto ctx = do_get_context(mid);
        return ctx->mdl;
    }

    using client_=shared_ptr<shyft::energy_market::stm::srv::dstm::client>;
    

    
    bool server::is_master() const {
        return cn_mgr!=nullptr;
    }

    bool server::is_optimization_running(const std::string& mid) {
        try {
            auto ctx = do_get_context(mid);
            boost::shared_lock<srv_shared_mutex> sl(ctx->mtx);
            return ctx->state == model_state::running;
        } catch (const std::exception& e) {
            slog << dlib::LWARN << e.what();
        }
        return false;
    }


    
    bool server::do_slave_optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd){
        auto ctx = do_get_context(mid);
        srv_upgradable_lock sl(ctx->mtx);
        if (stm_system_needs_bind(*(ctx->mdl))){
            slog << dlib::LERROR << "optimize('" << mid << "'): Cannot run optimization on model with unbound attributes.\n"
                << "\tConsider evaluating model before running SHOP optimization.";
            return false;
        }
        
        if (ctx->state == model_state::running) {
            slog << dlib::LWARN << "optimize: Optimization is already running on '" << mid << "'";
            return false;
        }
        
        scoped_compute_node remote{*cn_mgr,mid};
        auto slave = make_shared<client>(remote.client_host_port());
        ctx->state = model_state::setup;// about to optimize
        slog<<dlib::LINFO<<"optimize('"<<mid<<"') using computenode "<< slave->c.host_port;
        /* scope a unique lock here to ensure proper modify access to local variables*/ {
            srv_upgrade_lock ul(sl);// pass in the upgradable lock here.
            //TODO: slave->remove_model(mid); or consider using mid+ something to uniquely identify the computational models.
            slave->add_model(mid, ctx->mdl);
            ctx->state = model_state::running;
            remote.register_contact(utctime_now());
        }

        // We can now release the unique lock, keep the shared lock so that we keep model consistent(unchanged) while optimizing
        ctx->current_run = std::async(std::launch::async,
            [this, ctx, slave, remotei{std::move(remote)}, sli{std::move(sl)}, cmd, ta, mid]() mutable -> shop_flag {
                auto sl = std::move(sli); // ensure lock is released on return
                auto remote=std::move(remotei);
                //auto clear_on_make_and_exit = local::on_make_and_exit([this, mid] { clear_kill_flag(mid); }); // ensure flag is cleared on return

                slog << dlib::LINFO << "Starting slave "<<slave->c.host_port<< " optimization on '" << mid << "'";
                try {
                    if(!slave->optimize(mid,ta,cmd,true)) {// optimize only, minimize job done on slave
                        remote.register_failure();
                        slave->remove_model(mid);
                        throw std::runtime_error("dstm: failed to run optimize "+mid+" on "+slave->c.host_port);
                    }
                    slog << dlib::LINFO << "Waiting/polling on "<<slave->c.host_port<< " optimization on '" << mid << "'";
                    const auto t_exit = utctime_now() + calc_suggested_timelimit(cmd);
                    bool hard_timeout { false };
                    size_t logpos=0;
                    while(slave->get_state(mid)== model_state::running && !hard_timeout && !ctx->killed) {
                        std::this_thread::sleep_for(std::chrono::milliseconds(10));
                        auto msgs=slave->get_log(mid);
                        if(std::size(msgs) > logpos) {
                            ctx->add_shop_log(std::next(std::cbegin(msgs),logpos), std::cend(msgs));
                            logpos = std::size(msgs);
                        }
                        remote.register_contact(utctime_now());
                        hard_timeout = utctime_now() >= t_exit;
                    }
                    if(slave->get_state(mid) != model_state::finished) {
                        remote.register_failure();

                        if (hard_timeout || ctx->killed) {
                            remote.register_kill();// this means, we are going to put it on freezer.
                            string e = "dstm: optimize " + mid + (hard_timeout ? " hard timeout. " : " received kill-signal. ");
                            ctx->killed = ctx->killed || hard_timeout;// regardless how, we are really done with this
                            if (slave->kill_optimization(mid)) {
                                e += "Kill signal sent to slave: " + slave->c.host_port;
                            } else {
                                e += "Failed to send kill signal to slave: " + slave->c.host_port;
                            }

                            throw std::runtime_error(e);
                        }

                        throw std::runtime_error("dstm: Failed to complete optimize "+mid+" on "+slave->c.host_port);

                    } else {
                        auto msgs=slave->get_log(mid);
                        if(std::size(msgs) > logpos) {
                            ctx->add_shop_log(std::next(std::cbegin(msgs),logpos), std::cend(msgs));
                        }
                    }
                } catch(std::exception const&e) {
                    slog << dlib::LWARN << "optimize('" << mid << "): threw exception " << e.what();
                    ctx->state=model_state::failed;// important
                    if(!ctx->killed) {// incase of hard timeout/kill, we can not remove on remote, it is assumed dead.
                        slog << dlib::LINFO << "slave optimize('" << mid << "'): soft failure,  removing slave model";
                        slave->remove_model(mid);
                    }
                    return ctx->shop_result=shop_flag::other;
                }

                slog << dlib::LINFO << "slave optimize('" << mid << "'): SHOP run completed, upgrade to write-lock on model.";

                // We now upgrade the lock-type and collect results
                srv_upgrade_lock ul(sl);
                slog << dlib::LINFO << "slave optimize('" << mid << "'): SHOP run completed, collecting results.";
                try {
                    auto prefix="dstm://M"+mid;
                    auto ts_ids=ts_url_generator(prefix,*(ctx->mdl));
                    slog << dlib::LINFO << "slave optimize('" << mid << "'): get_ts of "<<ts_ids.size();
                    auto ts_res=slave->get_ts(mid,ts_ids);
                    auto summary=slave->get_optimization_summary(mid);
                    ctx->mdl->set_summary(summary);
                    ctx->mdl->run_params.run_time_axis=ta;// ensure to at least update run_time_axis.
                    remote.register_contact(utctime_now());
                    slog << dlib::LINFO << "slave optimize('" << mid << "'): removing slave model";
                    slave->remove_model(mid);
                    slog << dlib::LINFO << "slave optimize('" << mid << "'): applying ts to local model.. "<<ts_res.size();
                    ats_vector ats_res;ats_res.reserve(ts_res.size());
                    for(auto i=0u;i<ts_res.size();++i) {
                        if(!ts_res[i].ts) {
                        // if there are no results, we currently leave the source model untouched
                        //  we could try to clear out the existing values, or we could let do_set_ts have suitable semantics for applying /empty/unchanged ts
                        //slog << dlib::LINFO << "slave optimize('" << mid << "'): empty url"<<ts_ids[i];
                        // ats_res.emplace_back(ts_ids[i],empty_ts);// need url-annotated tsv, with ref ts; bind to empty ts
                        } else {
                            ats_res.emplace_back(ts_ids[i],std::move(ts_res[i]));// need url-annotated tsv
                        }
                    }
                    do_set_ts(mid,ats_res);// we have the lock, so we can safely set ts directly
                    rebind_expression(*(ctx->mdl),mid);// find all expressions with refs to dstm://.. refresh those.
                    for(const auto&g:ctx->mdl->unit_groups) {// need to update sum expressions after getting resuls filled in
                        g->update_sum_expressions();
                        sm->notify_change(g->all_urls(prefix));//notify change ug production etc.
                    }
                    sm->notify_change(ctx->mdl->summary->all_urls(prefix));// notify summary changes
                } catch(std::exception const&e) {
                    slog << dlib::LWARN << "slave optimize('" << mid << "): threw exception while reading/applying result: " << e.what();
                    ctx->state=model_state::failed;// important .. put back to idle
                    // Before returning, we notify changes based on what's been reported to shop's visitor:
                    return ctx->shop_result=shop_flag::other;
                }
                ctx->state = model_state::finished;
                slog << dlib::LINFO << "slave optimize('" << mid << "'): completed.";
                return ctx->shop_result=shop_flag::success;
            }
        );
        return true; // To signify that optimization has started correctly.
    }

    vector<shop_log_entry> server::do_slave_get_log(const string& mid) {
        return do_get_log(mid);// nothing special with slave get logs, as our worker threads polls it from the slave
    }

    void server::do_add_compute_node(string host_port) {
        if(cn_mgr==nullptr) {
            cn_mgr=make_unique<compute_node_manager>();
        }
        cn_mgr->add_compute_nodes(vector<string>{host_port});
    }

    void server::do_remove_compute_node(string host_port) {
        if(cn_mgr==nullptr) {
            return;// a noop if we are not in master-mode
        }
        cn_mgr->remove_compute_nodes(vector<string>{host_port});
    }

    vector<compute_node> server::do_compute_node_info() {
        if(cn_mgr!=nullptr)
            return cn_mgr->compute_nodes();
        return {};
    }


    /** @brief start SHOP optimization on a model
        *  returns whether the shop optimization was started or not.
        */
    bool server::do_optimize(const string& mid, const generic_dt& ta, const vector<shop_command>& cmd, bool opt_only){
        if(is_master()) {// then forward all optimization to the slaves (so that using callbacks from py etc. works as expected
            return do_slave_optimize(mid,ta,cmd);
        } else {
#ifdef SHYFT_WITH_SHOP
            auto ctx = do_get_context(mid);

            srv_upgradable_lock sl(ctx->mtx);
            auto shop = std::make_unique<shop_system>(ta, ctx.get(), "dstm://M" + mid);
            // Set up signal handler:
            shop_segfault_handler sh;
            // Check if ctx has any unbound series. If so, we shouldn't start an optimization.
            if (stm_system_needs_bind(*(ctx->mdl))){
                slog << dlib::LERROR << "optimize('" << mid << "'): Cannot run optimization on model with unbound attributes.\n"
                    << "\tConsider evaluating model before running SHOP optimization.";
                return false;
            }
            if (ctx->state == model_state::running) {
                slog << dlib::LWARN << "optimize: Optimization is already running on '" << mid << "'";
                return false;
            }
            /* scope a unique lock here to ensure proper modify access to local variables*/ {
                srv_upgrade_lock ul(sl);// pass in the upgradable lock here.

                // Preprocessing:
                if (!setjmp(buf)) {
                    // Emit to shop
                    sh.shop_call = true; // If we get SIGSEGV from now on, use custom handling
                    shop->set_logging_to_stdstreams(false);
                    //shop->set_logging_to_files(true);
                    auto cmdl = std::const_pointer_cast<const stm_system>(ctx->mdl);
                    shop->emit(*cmdl);
                    sh.shop_call = false;
                } else {
                    slog << dlib::LERROR << "optimize('" << mid << "'): SIGSEGV signal received while emitting model to shop.";
                    return false;
                }
                // Finally, we set the optimizing flag, to signify that the model should only be read_only from this point on.
                ctx->state = model_state::running;
            }
            // We can now release the unique lock, keep the shared lock so that we keep model consistent(unchanged) while optimizing
            ctx->current_run = std::async(std::launch::async,
                [this, ctx, sli{std::move(sl)}, cmd, shopi{std::move(shop)}, shi{std::move(sh)}, mid, opt_only]() mutable -> shop_flag {
                    // Move arguments in from lambda scope, to release them on return instead of waiting for anyone to perform wait/get on the future
                    auto sl = std::move(sli); // ensure lock is released on return
                    auto shop = std::move(shopi); // ensure shop api, and its locks on log files, is released on return
                    auto sh = std::move(shi); // ensure segfault handler is reset to default on return
                    auto prefix="dstm://M"+mid;
                    auto ts_ids=ts_url_generator(prefix,*(ctx->mdl));

                    // Start optimization
                    slog << dlib::LINFO << "Starting optimization on '" << mid << "'";
                    if (!setjmp(buf)) {
                        sh.shop_call = true;
                        try {
                            shop->command(cmd);// could segfault, then we drop to else part, or throw, then current_run keep the exception
                            if(ctx->killed) {
                                throw std::runtime_error("deliberately killed in action");
                            }
                        } catch(std::exception const&e) {
                            slog << dlib::LWARN << "optimize('" << mid << "): threw exception " << e.what();
                            ctx->state=model_state::failed;// important
                            sh.shop_call=false;// and this as well.
                            return ctx->shop_result=shop_flag::other;
                        }
                        // Optimization is now done.
                        slog << dlib::LINFO << "optimize('" << mid << "'): SHOP run completed, upgrade to write-lock on model.";

                        // We now upgrade the lock-type and collect results
                        srv_upgrade_lock ul(sl);
                        slog << dlib::LINFO << "optimize('" << mid << "'): SHOP run completed, collecting results.";
                        try {
                            shop->collect(*(ctx->mdl));// note.. this can also segfault (jump to else part), or throw
                        } catch(std::exception const&e) {
                            slog << dlib::LWARN << "optimize('" << mid << "): threw exception while reading result " << e.what();
                            ctx->state=model_state::failed;// important .. put back to idle
                            sh.shop_call=false;// and this as well.
                            // Before returning, we notify changes based on what's been reported to shop's visitor:
                            return ctx->shop_result=shop_flag::other;
                        }
                        shop->set_logging_to_files(false);// do we dare to do this in the exception sections?
                        sh.shop_call = false;
                        ctx->state = model_state::finished;
                        slog << dlib::LINFO << "optimize('" << mid << "'): completed.";
                        shop->complete(*(ctx->mdl));
                        if(!opt_only) rebind_expression(*(ctx->mdl),mid);
                        shop->vis->notify_changes(*sm);
                        dtss->sm->notify_change(ts_ids);// also ensure to notify all result ts that they are now changed
                        for(const auto&g:ctx->mdl->unit_groups) {// need to update sum expressions after getting results filled in
                            //g->update_sum_expressions(); // already done as part of shop->update call!
                            dtss->sm->notify_change(g->all_urls(prefix));//notify change ug production etc.
                        }
                        sm->notify_change(ctx->mdl->summary->all_urls(prefix));// notify summary changes

                        return ctx->shop_result=shop_flag::success;
                    } else {
                        // We received a SIGSEGV from shop.
                        slog << dlib::LERROR << "optimize('" << mid << "'): Received SIGSEGV signal while executing shop.";
                        sh.shop_call=false;
                        ctx->state=model_state::failed;// important .. put back to failed
                        return ctx->shop_result=shop_flag::segfault;
                    }
                }
            );
            return true; // To signify that optimization has started correctly.
#else
            slog << dlib::LERROR << "optimize('" << mid << "'): this dstm server is not linked with shop features, and there are no compute nodes registered to distribute the job to";
            return false; // refuse to optimize (not linked in functionality in this server
#endif
        }
    }


    /**
     * @brief Get SHOP log for a model
     */
    vector<shop_log_entry> server::do_get_log(const string& mid) {
        auto ctx = do_get_context(mid);
        srv_shared_lock ul(ctx->mtx); // ok. shared to get the log.
        auto r= ctx->shop_log();
        slog << dlib::LTRACE << "get_log: retrieving shop log entries for '" << mid << "', entries ="<<r.size();
        return r;
    }

    /**
     * @brief Evaluate any unbound time series of a model
     */
    ts_vector_t server::evaluate_stm_system(stm_system const & mdl, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
        ts_vector_t tsv;
        stm_ts_operation find_unbound{
            [&tsv](apoint_ts&ats)->bool {
                if(!ats.needs_bind()) return false;
                tsv.push_back(ats);
                return true;
            }
        };
        find_unbound.apply(const_cast<stm_system&>(mdl));
        return dtss->do_evaluate_ts_vector(bind_period, tsv, use_ts_cached_read, update_ts_cache, clip_period);
    }
    
    /**
     * @brief Check whether any attributes in an stm_system needs bind_period
     */
    bool server::stm_system_needs_bind(const stm_system& mdl) {
        stm_ts_operation needs_bind{
            [](apoint_ts&ats)->bool {
                return ats.needs_bind();
            }
        };
        return needs_bind.apply(const_cast<stm_system&>(mdl));
    }
    
    bool server::do_evaluate_model(const string& mid, utcperiod bind_period, bool use_ts_cached_read, bool update_ts_cache, utcperiod clip_period) {
        auto ctx = do_get_context(mid);
        srv_unique_lock sl(ctx->mtx); // We are potentially rewriting attributes.
        auto tsv = evaluate_stm_system(*(ctx->mdl), bind_period, use_ts_cached_read, update_ts_cache, clip_period);
        auto prefix="dstm://M"+mid;/// update unit expressions, with notify.
        for(const auto&g:ctx->mdl->unit_groups) {// need to update sum expressions after getting results filled in
            g->update_sum_expressions();
            dtss->sm->notify_change(g->all_urls(prefix));//notify change ug production etc.
        }
        slog << dlib::LINFO << "Evaluated model '" << mid << "'";
        return tsv.size() > 0;
    }
    

    
    bool server::do_fx(string mid, string action) {
            return fx_cb? fx_cb(mid,action):false;
    }
    
    /**
     * @brief rebind_expression
     * @details
     *
     * This routine traverses all time-series of the locked and mutable stm_system mdl,
     * its purpose is to be used after optimization/computation have updated the result terminals
     * of the stm_system. In that case, any expressions refering to those terminals needs to be
     * properly recomputed/reconfigured for computation.
     *
     * The algorithm outline:
     * for each ts of the model, if it's an expression,
     *   find all the terminal nodes of the expression
     *     and save their bound time-series
     *   then IF there was 1 or more dstm://refs,
     *        unbind the expression (will wipe out bound entities)
     *        then put into place the saved bound time-series
     *        finally, re-evaluate/prepare the expression.
     *
     * @param mdl a locked exclusive mode stm_system
     * @param new_mkey the dstm m_key for the stm_system
     * @return true if any rebind occured (e.g. there was at least one dtstm://new_mkey/ ref. in the expressions)
     */
    bool rebind_expression(stm_system&mdl,string const& new_mkey) {
        auto mdl_prefix="dstm://M"+new_mkey+"/";// important with the trailing / to avoid mixing up any future possible multimodelkeys
        apoint_ts nan_fill{mdl.run_params.run_time_axis,shyft::nan,time_series::ts_point_fx::POINT_AVERAGE_VALUE};
        stm_ts_operation r{
            [&mdl,&mdl_prefix,&new_mkey,&nan_fill](apoint_ts&ats)->bool {
                auto atsv = ats.find_ts_bind_info();
                std::vector<ts_bind_info> restore_info;
                std::vector<apoint_ts>   restore_ts;
                bool rebind_done=false;
                shyft::web_api::grammar::dstm_path_grammar<const char*> pth_{scoped_ts_url_resolver{&mdl,new_mkey}};
                for (auto & tsi : atsv) {
                    if (tsi.reference.rfind(mdl_prefix, 0) != 0) {
                        auto rts=std::dynamic_pointer_cast<aref_ts const>(tsi.ts.ts);
                        if(rts) {
                            if(rts->rep == nullptr) {
                                server::slog<<dlib::LERROR<<"Non dstm rebind attribute ts:"<<ats.id()<<",ts ref==nullptr for "<<tsi.reference;
                            }
                            restore_ts.push_back(apoint_ts{rts->rep});
                            restore_info.push_back(tsi);//
                        } else {
                            server::slog << dlib::LERROR <<"Rebind attribute ts:"<<ats.id()<<",null ts ref for "<<tsi.reference;
                        }
                        continue;
                    }
                    apoint_ts xts;
                    auto ok_parse = shyft::web_api::grammar::phrase_parser(tsi.reference.c_str(), pth_, xts);
                    if(ok_parse) { // gts is now ref to the ts attribute, we want to bind this to the tsi.ts
                        restore_info.push_back(tsi);
                        if(!xts.needs_bind()) { //only formal requirement, is that it is bound
                            restore_ts.push_back(xts);
                        } else {
                            restore_ts.push_back(nan_fill);
                            server::slog << dlib::LWARN <<"repair rebind nan done due to missing ts "<<tsi.reference;
                        }
                        rebind_done =true;
                    } else {
                        server::slog << dlib::LERROR <<"rebind gts failed parse:"<<tsi.reference;
                    }
                }
                if (rebind_done) {
                    ats.do_unbind(); //do ats.unbind(ts) operation, that resets all expression trees from top down to this tsi.ts.
                    // restore all series that where wiped out in the line above
                    for(auto i=0u;i<restore_info.size();++i) {
                        restore_info[i].ts.bind(restore_ts[i]);// now put back those time-series other than dstm://M<thismodel>, we want those unmodified.
                    }
                    ats.do_bind();// finally, restore computational structure
                }
                return rebind_done;
            }
        };
        return r.apply(mdl);
    }

    bool replace_model_key_in_id(apoint_ts& ats, string const& new_mkey){
        using namespace time_series::dd;
        if (auto ts = ts_as_mutable<aref_ts>(ats.ts)) {
            if (ts->id.rfind("dstm://M", 0) == 0) {
                // Get position where model-key ends:
                auto rpos = ts->id.find("/", 8); //"dstm://M".size() == 8
                if(rpos==string::npos)
                    return false;// assume nothing, false is ok to return
                if(new_mkey!=ts->id.substr(8,rpos-8)) {
                    ts->id = ts->id.replace(8, rpos-8, new_mkey);
                    return true;
                }
            }
        }
        return false;
    }

    bool rebind_ts(apoint_ts& ats, string const& new_mkey) {
        auto atsv = ats.find_ts_bind_info();
        bool rebind_done=false;
        for (auto & tsi : atsv) {
            rebind_done |= replace_model_key_in_id(tsi.ts, new_mkey);
        }
        return rebind_done;
    }

    bool rebind_ts(stm_system& mdl, string const& new_mkey) {
        stm_ts_operation r{
            [&new_mkey](apoint_ts&ats)->bool {
                auto atsv = ats.find_ts_bind_info();
                bool done=false;
                for (auto & tsi : atsv) {
                    done |= replace_model_key_in_id(tsi.ts, new_mkey);
                }
                return done;
            }
        };
        return r.apply(mdl);
    }

    /**
     * @brief handle one client connection
     * @details
     * Reads messages/requests from the clients,
     * - act and perform request,
     * - return response
     * for as long as the client keep the connection
     * open.
     *
     */
    void server::on_connect(
        std::istream & in,
        std::ostream & out,
        const std::string & foreign_ip,
        const std::string & local_ip,
        unsigned short foreign_port,
        unsigned short local_port,
        dlib::uint64 /*connection_id*/
    ) {

        using shyft::core::core_iarchive;
        using shyft::core::core_oarchive;
        try {
            while (in.peek() != EOF) {
                auto msg_type= msg::read_type(in);
                try {
                    switch (msg_type) {
                        case message_type::VERSION_INFO: {
                            auto result=do_get_version_info();// get result
                            msg::write_type(message_type::VERSION_INFO,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::CREATE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;
                            ia>>mid;
                            auto result=do_create_model(mid);// get result
                            msg::write_type(message_type::CREATE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::ADD_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;
                            stm_system_ mdl;
                            ia>>mid>>mdl;
                            auto result=do_add_model(mid, mdl);// get result
                            msg::write_type(message_type::ADD_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::REMOVE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto result=do_remove_model(mid);// get result
                            msg::write_type(message_type::REMOVE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::RENAME_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string old_mid, new_mid; 
                            ia>>old_mid>>new_mid;
                            auto result=do_rename_model(old_mid, new_mid);// get result
                            msg::write_type(message_type::RENAME_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::CLONE_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string old_mid, new_mid;
                            ia>>old_mid>>new_mid;
                            auto result=do_clone_model(old_mid, new_mid);// get result
                            msg::write_type(message_type::CLONE_MODEL,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::GET_MODEL_IDS: {
                            auto result=do_get_model_ids();// get result
                            msg::write_type(message_type::GET_MODEL_IDS,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::GET_MODEL_INFOS: {
                            auto result=do_get_model_infos();// get result
                            msg::write_type(message_type::GET_MODEL_INFOS,out);// then send
                            core_oarchive oa(out,core_arch_flags);
                            oa<<result;
                        } break;

                        case message_type::GET_MODEL: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid; 
                            ia>>mid;
                            auto ctx=do_get_context(mid);
                            /* scoped lock for the ctx->mtx_ protecting the model, so it's read-only while streaming  */{
                                boost::shared_lock<srv_shared_mutex> sl(ctx->mtx);
                                msg::write_type(message_type::GET_MODEL, out);// then send
                                core_oarchive oa(out, core_arch_flags);
                                oa<<ctx->mdl;// this is safe, now it's read-only and consistent while we stream it back
                            }
                        } break;

                        case message_type::OPTIMIZE: {
                            core_iarchive ia(in,core_arch_flags);// create the stream
                            string mid;
                            generic_dt ta;
                            vector<shop_command> cmd;
                            bool opt_only{false};
                            ia >> mid >> ta >> cmd >> opt_only;
                            auto result = is_master() ? do_slave_optimize(mid, ta, cmd) : do_optimize(mid, ta, cmd, opt_only);
                            msg::write_type(message_type::OPTIMIZE, out);// then send
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::GET_LOG: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            ia >> mid;
                            auto result = is_master() ? do_slave_get_log(mid) : do_get_log(mid);
                            msg::write_type(message_type::GET_LOG, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::GET_STATE: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            ia >> mid;
                            auto result = do_get_state(mid);
                            msg::write_type(message_type::GET_STATE, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::SET_STATE: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;model_state x;
                            ia >> mid>>x;
                            do_set_state(mid,x);
                            msg::write_type(message_type::SET_STATE, out);
                        } break;
                        case message_type::FX: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid,fx_arg;
                            ia >> mid>>fx_arg;
                            auto result=do_fx(mid,fx_arg);
                            msg::write_type(message_type::FX,out);
                            core_oarchive oa(out, core_arch_flags);
                            oa<<result;
                        } break;
                        case message_type::EVALUATE_MODEL: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            bool use_cache, update_cache;
                            utcperiod bind_period, clip_period;
                            ia >> mid >> bind_period >> use_cache >> update_cache >> clip_period;
                            auto result = do_evaluate_model(mid, bind_period, use_cache, update_cache, clip_period);
                            msg::write_type(message_type::EVALUATE_MODEL, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::GET_TS: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            vector<string> ts_urls;
                            ia >> mid >> ts_urls;
                            auto result = do_get_ts(mid, ts_urls);
                            msg::write_type(message_type::GET_TS, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::SET_TS: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            ats_vector tsv;
                            ia >> mid >> tsv;
                            do_lock_set_ts(mid, tsv);
                            msg::write_type(message_type::SET_TS, out);
                            core_oarchive oa(out, core_arch_flags);
                        } break;
                        case message_type::ADD_COMPUTE_NODE: {
                            core_iarchive ia(in, core_arch_flags);
                            string host_port;
                            ia >> host_port;
                            do_add_compute_node(host_port);
                            msg::write_type(message_type::ADD_COMPUTE_NODE, out);
                            core_oarchive oa(out, core_arch_flags);
                        } break;
                        case message_type::REMOVE_COMPUTE_NODE: {
                            core_iarchive ia(in, core_arch_flags);
                            string host_port;
                            ia >> host_port;
                            do_remove_compute_node(host_port);
                            msg::write_type(message_type::REMOVE_COMPUTE_NODE, out);
                            core_oarchive oa(out, core_arch_flags);
                        } break;
                        case message_type::COMPUTE_NODE_INFO: {
                            core_iarchive ia(in, core_arch_flags);
                            auto result = do_compute_node_info();
                            msg::write_type(message_type::COMPUTE_NODE_INFO, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::GET_OPTIMIZATION_SUMMARY: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid;
                            ia>>mid;
                            auto result = do_get_optimization_summary(mid);
                            msg::write_type(message_type::GET_OPTIMIZATION_SUMMARY, out);
                            core_oarchive oa(out, core_arch_flags);
                            oa << result;
                        } break;
                        case message_type::KILL_OPTIMIZATION: {
                            core_iarchive ia(in, core_arch_flags);
                            string mid; 
                            ia>>mid;
                            const auto can_kill = is_optimization_running(mid);
                            { // send the response before handling kill signal
                                msg::write_type(message_type::KILL_OPTIMIZATION, out);
                                core_oarchive oa(out, core_arch_flags);
                                oa << can_kill;
                            }
                            if (can_kill) {
                                set_kill_flag(mid);//set killflag regardless
                                if (!is_master()) { // then try to quit
                                    //std::this_thread::sleep_for(std::chrono::milliseconds(200));// allow something to happen before
                                    raise_sigterm();// we terminate
                                }
                            } else {
                                slog << dlib::LWARN << "kill_optimization: No optimization running for '" << mid << "'";
                            }
                            
                        } break;
                        // other
                        default:
                            throw std::runtime_error(std::string("Server got unknown message type:") + std::to_string((int)msg_type));
                    }
                } catch (std::exception const& e) {
                    msg::send_exception(e,out);
                }
            }
        } catch(...) {
                // exit the loop and close connection
                slog<<dlib::LERROR<< "dstm-service: failed and cleanup connection from '"<<foreign_ip<<"'@"<<foreign_port<<", served at local '"<< local_ip<<"'@"<<local_port<<"\n";
        }

    }

    utctime server::calc_suggested_timelimit(const vector<shop_command>& cmds) {
        // Helpers to compare shop command types
        auto eq_cmd_type = [](const shop_command& a, const shop_command& b) { return a.keyword == b.keyword && a.specifier == b.specifier; };
        auto is_timelimit_cmd = [&eq_cmd_type](const shop_command& cmd) { 
            return eq_cmd_type(cmd, shop_command::set_timelimit(0)); 
        };
        auto is_start_cmd = [&eq_cmd_type](const shop_command& cmd) { 
            return eq_cmd_type(cmd, shop_command::start_sim(0)) || eq_cmd_type(cmd, shop_command::start_shopsim());
        };

        // Find last timelimit between start commands, and add to total timelimit
        int64_t total_timelimit {0};
        int64_t last_timelimit {0};
        for (const auto& cmd : cmds) {
            if (is_timelimit_cmd(cmd)) {
                if (last_timelimit != 0)
                    slog << dlib::LTRACE << "multiple timelimits provided before start cmd. Overwriting previous timelimit";

                last_timelimit = std::stoi(cmd.objects.at(0)); // Assume shop command for timelimit is constructed correctly

            } else if (is_start_cmd(cmd)) {
                int n_iterations= cmd.objects.size()? std::atoi(cmd.objects[0].c_str()):0;
                if(n_iterations==0) n_iterations=1;
                if (last_timelimit != 0) {
                    total_timelimit += last_timelimit*n_iterations;
                    last_timelimit = 0;
                } else {
                    slog << dlib::LTRACE << "no timelimit provided before cmd '" << cmd.keyword << " " << cmd.specifier << "'. Ignoring timelimits";
                    total_timelimit = 0;
                    break;
                }
            }
        }

        if (total_timelimit == 0) {
            slog << dlib::LTRACE << "using default timelimit of 1 hours";
            total_timelimit = 1*3600;
        } else {
            const auto sum_timelimit = total_timelimit;
            // shop needs to finish ongoing iteration before it can exit on timeout.
            // add a margin to give shop a chance to finish on timelimit, before 'hard' timeout
            const auto multiplier = 1.5;
            total_timelimit *= multiplier;
            slog << dlib::LTRACE << "using a total timelimit of " << sum_timelimit << " * " << multiplier << " = " << total_timelimit << " seconds";
        }

        return shyft::core::from_seconds(total_timelimit);
    }

}
