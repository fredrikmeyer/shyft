#pragma once
#include <cstdint>

namespace shyft::energy_market::stm::srv::dstm {
    
    enum class model_state:int8_t {
        idle,     ///< Model is available in full to the user
        setup,    ///< Indicates that model is being set up, e.g. input data is being processed from a UI.
        running,  ///< Model is currently running an optimizing procedure, limited availability
        finished, ///< Indicates that the algorithm is finished with success and results are ready.
        failed    ///< FAILED - Indicates that the algorithm failed to produce results for the model.
    };
    
    enum class shop_flag:int8_t {
        success,    // The SHOP optimization returned without error
        segfault,   // At some stage, SHOP raised a segmentation error signal
        other       // Some other error
        //,undefined, as in not yet started ?
    };
    
}
