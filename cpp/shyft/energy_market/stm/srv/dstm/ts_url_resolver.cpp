#include <shyft/energy_market/stm/srv/dstm/ts_url_resolver.h>
#include <shyft/mp.h>
#include <boost/hana.hpp>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/web_api/json_struct.h>

namespace hana = boost::hana;

using shyft::energy_market::id_base;
using shyft::energy_market::stm::reservoir;
using shyft::energy_market::stm::unit;
using shyft::energy_market::stm::waterway;
using shyft::energy_market::stm::power_plant;
using shyft::energy_market::stm::catchment;
using shyft::energy_market::stm::gate;
using shyft::energy_market::stm::unit_group;
using shyft::energy_market::stm::unit_group_member;

using shyft::energy_market::stm::reservoir_aggregate;
using shyft::time_series::dd::apoint_ts;

//using shyft::energy_market::stm::srv::srv_shared_lock;
using shyft::energy_market::stm::stm_system;
using shyft::energy_market::stm::srv::dstm::server;
using shyft::web_api::energy_market::attribute_value_type;

  /// dstm resolver; given url, figure out where in the model to get the timeseries.
namespace {
    namespace mp = shyft::mp;

    template<class Fx>
    bool fx_ts_custom(unit_group_member&, const std::string&, Fx&&) {
        return false;
    }
    template<class Fx>
    bool fx_ts_custom(id_base& t, const std::string& attr_id, Fx&& fx) {
        if (attr_id.length() > 3 && attr_id.rfind("ts.", 0) == 0) {
            const auto key = attr_id.substr(3);
            const auto it = t.tsm.find(key);
            if (it != t.tsm.end()) {
                fx(it->second);
                return true;
            }
            throw std::runtime_error("Error when resolving dstm ts_url. No such tsm key: " + key);
        }
        return false;
    }

    /** @brief apply fx to a ts attribute from t and provided attr_id.
     * @details
     * If the attribute is not found, or has wrong type it throws runtime_error
     * @tparam T : Hana struct to find attribute in
     * @tparam Fx : Any lambda that takes apoint_ts of some form
     * @param t : Attribute value to set
     * @param attr_id : attribute id to get
     * @param fx
     * @return : If T::attr_id is valid, then it will return t.attr_id.
     */
    template<class T, class Fx>
    void fx_ts_attr(T & t, const std::string& attr_id, Fx && fx) {
        if (fx_ts_custom(t, attr_id, fx))
            return;
        constexpr auto attr_paths = mp::leaf_accessors(hana::type_c<T>);
        const auto aid = attr_id.c_str();
        bool found{false};
        hana::for_each(attr_paths,
            [&found,&aid,&t,&fx,&attr_id](auto m) {
                // Compare attr_id with boost::hana::attr_id.
                if (!strcmp(aid, mp::leaf_accessor_id_str(m))) {
                    if constexpr (mp::accessor_ptr_type(mp::leaf_accessor(m)) == hana::type_c<apoint_ts>) {
                        fx(mp::leaf_access(t,m));
                        found=true;
                    } else {
                        throw std::runtime_error("Error when resolving dstm ts_url. Type mismatch, attribute " + attr_id + " is not a time series.");
                    }
                }
            }
        );
        if(!found)
            throw std::runtime_error("Error when resolving dstm ts_url. No such attribute path: " + std::string(typeid(T).name()) + "." + attr_id);
    }

    template<class T> // just to make next section shorter
    constexpr auto as_=[](auto &&p) {return std::dynamic_pointer_cast<T>(p);};

    // just to fix minor type variations regarding how to the id out of them;
    template<class T> static int64_t cid_of(T const &t) {return t.id;}
    template <> int64_t cid_of(unit_group_member const&ug) {return ug.id();}

    constexpr auto require_component=[](auto const& c, int id,std::string const&mid) {
        auto f=std::find_if(std::begin(c),std::end(c),[&id](auto const& e) { return cid_of(*e) == id; });
        if (f == std::end(c)) {
            throw std::runtime_error( std::string("Unable to find component id=") + std::to_string(id) + std::string(" in model '") + mid + "'");
        }
        return *f;
    };

}
    
namespace shyft::energy_market::stm::srv::dstm {
using std::string;
namespace mp = shyft::mp;


template <class Fx,class StmSys>
static void _set_ts_attr(StmSys mdl, const string& mid, std::vector<tp_id> const & cpth, const string& attr_id,Fx &&fx) {

    if(cpth[0].tp=='H') { // hydro power system path
        if(cpth.size()!=2)
            throw std::runtime_error(string("Hydro power system urls must have 2 level of component adressing ")+mid);

        auto hps_id=cpth[0].id;
        auto comp_id=cpth[1].id;
        auto comp_type=cpth[1].tp;
        auto hps = require_component(mdl->hps,hps_id,mid);
        // Get component:
        auto require_attr=[&mid,comp_type,comp_id,hps_id,&attr_id,&fx](auto p) {
            if(!p)
                throw std::runtime_error(string("Can not find object type='")+char(comp_type) + "' oid="+std::to_string(comp_id)+ ", located in stm_system='"+mid+"'/H"+std::to_string(hps_id));
            fx_ts_attr(*p,attr_id,fx);
        };

        if      (comp_type == 'R')  require_attr(as_<reservoir>          (hps->find_reservoir_by_id          (comp_id)));
        else if (comp_type == 'U')  require_attr(as_<unit>               (hps->find_unit_by_id               (comp_id)));
        else if (comp_type == 'W')  require_attr(as_<waterway>           (hps->find_waterway_by_id           (comp_id)));
        else if (comp_type == 'P')  require_attr(as_<power_plant>        (hps->find_power_plant_by_id        (comp_id)));
        else if (comp_type == 'C')  require_attr(as_<catchment>          (hps->find_catchment_by_id          (comp_id)));
        else if (comp_type == 'G')  require_attr(as_<gate>               (hps->find_gate_by_id               (comp_id)));
        else if (comp_type == 'A')  require_attr(as_<reservoir_aggregate>(hps->find_reservoir_aggregate_by_id(comp_id)));
        else {
            throw std::runtime_error(string("Invalid HPS component type ") + char(comp_type) + ". Valid options are R|U|W|P|C|G|A.");
        }
    } else if (cpth[0].tp=='u') {
        auto ug= require_component(mdl->unit_groups,cpth[0].id,mid);
        if(cpth.size()==2) {
            if(cpth[1].tp != 'M')
                throw std::runtime_error(string("Invalid unit group subcomponent type ") + char(cpth[1].tp) + ". Valid options are M (unit group member).");
            auto ugm=require_component(ug->members,cpth[1].id,mid);
            fx_ts_attr(*ugm,attr_id,fx);
        } else {
            fx_ts_attr(*ug,attr_id,fx);
        }
    } else if (cpth[0].tp=='m') {
        auto ma= require_component(mdl->market,cpth[0].id,mid);
        if(cpth.size()==2) {
            throw std::runtime_error(string("Energy market area have only one level component paths, requested from model '")  + mid + "'");
        }
        fx_ts_attr(*ma,attr_id,fx);
    } else if (cpth[0].tp=='c') {
        auto ma= require_component(mdl->contracts,cpth[0].id,mid);
        if(cpth.size()==2) {
            throw std::runtime_error(string("Energy market contract have only one level component paths, requested from model '")  + mid + "'");
        }
        fx_ts_attr(*ma,attr_id,fx);
    } else if (cpth[0].tp=='p') {
        auto ma= require_component(mdl->contract_portfolios,cpth[0].id,mid);
        if(cpth.size()==2) {
            throw std::runtime_error(string("Energy market contract portfolio have only one level component paths, requested from model '")  + mid + "'");
        }
        fx_ts_attr(*ma,attr_id,fx);
    } else if (cpth[0].tp=='P') {
        auto ma= require_component(mdl->power_modules,cpth[0].id,mid);
        if(cpth.size()==2) {
            throw std::runtime_error(string("Energy market power modules have only one level component paths, requested from model '")  + mid + "'");
        }
        fx_ts_attr(*ma,attr_id,fx);
    } else if (cpth[0].tp=='n') {
        auto net = require_component(mdl->networks,cpth[0].id,mid);
        if(cpth.size()==2) {
            if(cpth[1].tp == 'b') {
                auto b = require_component(net->busbars,cpth[1].id,mid);
                fx_ts_attr(*b,attr_id,fx);
            } else if (cpth[1].tp == 't') {
                auto t = require_component(net->transmission_lines,cpth[1].id,mid);
                fx_ts_attr(*t,attr_id,fx);
            } else
                throw std::runtime_error(string("Invalid network subcomponent type ") + char(cpth[1].tp) + ". Valid options are b, t (busbars, transmission_lines).");
            
        } else {
            fx_ts_attr(*net,attr_id,fx);
        }
    } else {
        throw std::runtime_error(string("First level component ctype must be one of H,u,m,c,p,P,n (HPS,UnitGroups,Market,Contract,ContractPortfolio,PowerModule,Network) for model '")  + mid + "'");
    }
}

/** @brief callback from ts_url_parser
 * @details
 * The ts_url_parser extracts info from dstm://Mmymodel/H1/U2.production.result,
 * into mid=mymodel, cpth={ {H,1} {U,2} }, attr_id='production.result'
 * and calls the operator () here.
 * We then uses srv to lookup the stm sys context with mid, and then navigate the way down to
 * the attribute using cpth and attr_id.
 *
 */
apoint_ts ts_url_resolver::operator() ( const string& mid, std::vector<tp_id> const & cpth, const string& attr_id) {
    if (!srv) {
        throw std::runtime_error("server must be set to successfully parse a ts_url");
    }
    if(cpth.size()==0 || cpth.size()>2) {
        throw std::runtime_error("attribute path must be 1..2 elements, was "+ std::to_string(cpth.size())+ " for request related to "+mid);
    }
    auto ctx= srv->do_get_context(mid);// this will require a lock on srv, hard to avoid.
    auto mdl=std::const_pointer_cast<const stm_system>(ctx->mdl);// get a const stm_system to ensure we are only reading
    apoint_ts x;
    _set_ts_attr(mdl,mid,cpth,attr_id,[&x](apoint_ts const&a) {x=a;});
    return x;
}

apoint_ts scoped_ts_url_resolver::operator() ( const string& mid, std::vector<tp_id> const & cpth, const string& attr_id) {
    if(mid!=this->mid)
        throw std::runtime_error(
            "scoped_ts_url_resolver: attempt to address another stm_system,current scope is "
            + this->mid + ", attempted scope was :" + mid
        );
    apoint_ts x;
    _set_ts_attr(mdl,mid,cpth,attr_id,[&x](apoint_ts const&a) {x=a;});
//    _set_ts_attr(srv,mid,cpth,attr_id,[&x](apoint_ts&a){a=x;});
    return x;
}

apoint_ts scoped_ts_url_resolver_setter::operator() ( const string& mid, std::vector<tp_id> const & cpth, const string& attr_id) {
    if(mid!=this->mid)
        throw std::runtime_error(
            "scoped_ts_url_resolver: attempt to address another stm_system,current scope is "
            + this->mid + ", attempted scope was :" + mid
        );
    _set_ts_attr(mdl,mid,cpth,attr_id,[&](apoint_ts&a){a=v;});
    return v;
}
}
