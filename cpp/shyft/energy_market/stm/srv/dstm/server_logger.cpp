#include <shyft/energy_market/stm/srv/dstm/server_logger.h>
#include <iostream>
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::srv::dstm {

using namespace std;
using namespace shyft::core;
server_log_hook::server_log_hook() {}
server_log_hook::server_log_hook(string const&fname):log_file{fname} {
    fout.open(fname.c_str());
}


void server_log_hook::log(const string& logger_name,
                 const dlib::log_level& ll,
                 const dlib::uint64 thread_id,
                 const char* message_to_log){
    static calendar utc{};//make it once, keep it
    if(ll>=current_level) {
        ostream *log_output =fout.is_open()?&fout:&cout;
        (*log_output) << utc.to_string(utctime_now()) <<" "<< ll << " [" << thread_id << "] " << logger_name << ": " << message_to_log << std::endl;
    }
}

void server_log_hook::configure(const std::string& fname, const dlib::log_level ll) {
    //TODO: I(sih) am afraid we are not using dlib log at it best here, read dlib examples, and redo this stuff.
    static server_log_hook l;// currently, a bit clumsy, but we need to make sure that any hooks outlives the logger, we can not trust python here
    if(l.fout.is_open()) {l.fout.flush();l.fout.close();} //close previous stream
    // prepare the global static logger,
    l.log_file=fname;
    l.fout.open(l.log_file);
    l.current_level=ll;
    // connect to dlib
    dlib::set_all_logging_output_hooks(l);
    dlib::set_all_logging_levels(ll);
}

void configure_logger(
                server_log_hook& hook,
                const dlib::log_level& ll){
    server_log_hook::configure(hook.log_file,ll);

}

}
