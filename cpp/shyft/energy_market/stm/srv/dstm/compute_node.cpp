#include <shyft/energy_market/stm/srv/dstm/compute_node.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>

namespace shyft::energy_market::stm::srv::dstm {
using boost::serialization::make_nvp;

template <class Archive>
void compute_node::serialize(Archive & ar, const unsigned int /* file_version*/) {
    ar
    & make_nvp("host_port",host_port)
    & make_nvp("fail_count",fail_count)
    & make_nvp("kill_count",kill_count)
    & make_nvp("last_contact",last_contact)
    & make_nvp("mid",mid)
    & make_nvp("allocated_time",allocated_time)
    & make_nvp("released_time",released_time)
    & make_nvp("removed",removed)

    ;
}



compute_node_manager::compute_node_manager (std::vector<string> const&c_nodes) {
    add_compute_nodes(c_nodes);
}

void compute_node_manager::add_compute_nodes(std::vector<string> const &c_nodes) {
    std::lock_guard _{mx};
    for(auto const&host_port:c_nodes) {
        if(host_port.size()==0) throw std::runtime_error("dstm::compute_node_manager: attempt to add empty node");
        auto found_node=all_nodes.find(host_port);
        if(  found_node == all_nodes.end() ) {
            auto cn=std::make_shared<compute_node>(host_port);
            all_nodes.insert({host_port,cn});
            idle_nodes.push(cn);
        } else {
            found_node->second->removed=false;//reset remove flag if pending remove
            found_node->second->kill_count=0;// also needed,(maybe reallocate to fail count)
        }
    }
}

void compute_node_manager::remove_compute_nodes(std::vector<string> const &c_nodes) {
    // 1. mark c_nodes for removal and get them away from all_nodes.
    std::lock_guard _{mx};
    for(auto const& host_port:c_nodes) {
        if(host_port.size()==0) throw std::runtime_error("dstm::compute_node_manager: attempt to remove empty node");
        if(auto f=all_nodes.find(host_port); f !=all_nodes.end()) { // only add nodes that are not yet there.
            f->second->removed=true;// mark it for removal
            all_nodes.erase(f);
        }
    }
    // 2. do best effort non-blocking remove from the idle queue
    vector<compute_node_> tmp;// to stash the compute nodes we want to keep.
    compute_node_ cn;
    while( !idle_nodes.closed() && idle_nodes.try_pull(cn) == boost::concurrent::queue_op_status::success) {
        if(!cn->removed)//
            tmp.push_back(cn);
    }

    if(!idle_nodes.closed()) {
        for(auto const&x:tmp)// insert back nodes that did not match.
            idle_nodes.push(x);
    }
} 

compute_node_ compute_node_manager::get(string const& assign_mid) {
    compute_node_ cn;
    while(cn==nullptr || cn->removed)
        cn=idle_nodes.pull();//wait until ready and we get a non-removed item
    std::lock_guard _{mx};
    cn->allocated_time=utctime_now();
    cn->mid=assign_mid;
    return cn;
}

void compute_node_manager::put(compute_node_ const&cn) {
    std::lock_guard _{mx};
    cn->released_time=utctime_now();
    if(!cn->removed )//only if not removed, put it back to idle_nodes
        idle_nodes.push(cn);
}

void compute_node_manager::register_contact(compute_node_ const&cn, utctime t) const {
    std::lock_guard _{mx};
    cn->last_contact=t;
}

void compute_node_manager::register_failures(compute_node_ const& cn, size_t n_new_failures) const {
    std::lock_guard _{mx};
    cn->fail_count+=n_new_failures;
}

void compute_node_manager::register_kills(compute_node_ const& cn, size_t n_new_kills) const {
    std::lock_guard _{mx};
    cn->kill_count+=n_new_kills;
    cn->removed=true;// implicated action, if we kill something, do not try to contact it, should be about dead.
    // when it becomes alive again, it has to register it self to duty. we do that, but keep track of same-ip/port and kills
}

std::vector<compute_node> compute_node_manager::compute_nodes() const {
    std::lock_guard _{mx};
    std::vector<compute_node> r;r.reserve(all_nodes.size());
    for(auto const& p:all_nodes)
        r.push_back(*p.second);
    return r;
}

}

x_serialize_instantiate_and_register(shyft::energy_market::stm::srv::dstm::compute_node);
