#pragma once
#include <shyft/srv/server.h>
#include <shyft/srv/db.h>
#include <shyft/energy_market/stm/srv/task/msg_defs.h>
#include <shyft/core/core_archive.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>

namespace shyft::energy_market::stm::srv::task {
    using std::string;

    using shyft::srv::model_info;
    using shyft::energy_market::stm::srv::stm_task;
    using shyft::energy_market::stm::srv::stm_case;
    using shyft::energy_market::stm::srv::stm_case_;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::energy_market::stm::srv::model_ref_;

    using db_t = shyft::srv::db<stm_task>;
	using fx_call_back_t = std::function<bool(int64_t,string)>; ///< the type of callback provided, 1st arg model-id, 2nd arg fx-verb 

    struct server : shyft::srv::server<db_t> {
        using super = shyft::srv::server<db_t>;

        server(string root_dir) : super(root_dir) {}
        server(server&&)=delete;
        server(const server&)=delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;
        ~server()=default;
        
        fx_call_back_t fx_cb;///< user specified callback that can be invoked using web-api fx(..) or python c.fx(..) typically run model

        /**@brief do fx(arg1,arg2)
         * 
         * if the fx_cb exist, call it
         */
        bool do_fx(int64_t mid, string action);
    protected:

        virtual bool message_dispatch(
            std::istream & in,
            std::ostream & out,
            message_type::type msg_type
            );
    };
}
