#pragma once
#include <memory>
#include <shyft/energy_market/stm/srv/task/msg_defs.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/srv/client.h>
#include <shyft/core/core_archive.h>

namespace shyft::energy_market::stm::srv::task {
    using std::shared_ptr;
    using std::string;
    using std::to_string;

    using shyft::energy_market::stm::srv::stm_task;
    using shyft::energy_market::stm::srv::stm_case_;
    using shyft::energy_market::stm::srv::model_ref_;


    struct client : shyft::srv::client<stm_task> {
        using super = shyft::srv::client<stm_task>;
        client() = delete;
        client(string host_port, int timeout_ms=1000) : super(host_port, timeout_ms) {}

        /** @brief Add a case to an existing task
         *
         * @param mid: Session ID.
         * @param cse: stm_case to add to task
         */
        void add_case(int64_t mid, const stm_case_& cse);

        /** @brief remove case from an existing task
         *
         * @param mid: task ID.
         * @param rid: run id to remove
         */
        bool remove_case(int64_t mid, int64_t rid);

        /** @brief remove run from an existing task
         *
         * @param mid: task ID.
         * @param rid: run name to remove
         */
        bool remove_case(int64_t mid, const string& rname );
        /** @brief get run based on ID.
         *
         * @param mid: task ID.
         * @param rid: run ID.
         * @returns run: requested run, or nullptr if not found
         */
        stm_case_ get_case(int64_t mid, int64_t rid);

        /** @brief get run based on name.
         *
         * @param mid: task ID.
         * @param rname: run name.
         * @returns run: requested run, or nullptr if not found
         */
        stm_case_ get_case(int64_t mid, const string& rname);

        /** @brief update case inplace in a task
         *
         * @param mid: task ID.
         * @param c: case instance
         */
        void update_case(int64_t mid, const stm_case& ce);

        /** @brief add a model_ref to a run
         *
         * @param mid: Session ID
         * @param rid: Run ID
         * @param mr: The model reference to add
         */
        void add_model_ref(int64_t mid, int64_t rid, const model_ref_& mr);

        /** @brief remove a model_ref from a run
         *
         * @param mid: Session ID
         * @param rid: run ID
         * @param mkey: Model reference key
         *
         * @returns success: true if model ref was found and removed, false otherwise.
         */
        bool remove_model_ref(int64_t mid, int64_t rid, const string& mkey);

        /** @brief get model_reference
         *
         * @param mid: Session ID
         * @param rid: Run ID
         * @param mkey: Key to model reference in
         */
        model_ref_ get_model_ref(int64_t mid, int64_t rid, const string& mkey);
        
        /** @brief fx
         *
         * @param mid: Session ID
         * @param args: args
         */
        bool fx(int64_t mid, string const& args);
    };
}
