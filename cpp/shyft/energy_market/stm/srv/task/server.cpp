#include <shyft/energy_market/stm/srv/task/server.h>

namespace shyft::energy_market::stm::srv::task {

bool server::do_fx(int64_t mid, string action) {
        return fx_cb? fx_cb(mid,action):false;
}
    
bool server::message_dispatch(
    std::istream & in,
    std::ostream & out,
    message_type::type msg_type
    )
{
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_arch_flags;

    if (!super::message_dispatch(in, out, msg_type)) {
        core_iarchive ia(in, core_arch_flags);
        core_oarchive oa(out, core_arch_flags);
        switch(msg_type) {
            case message_type::ADD_CASE: {
                stm_case_ run;
                int64_t mid;
                ia >> mid >> run;
                auto session = db.read_model(mid);
                session->add_case(run);
                model_info mi;
                if (!db.try_get_info_item(mid, mi)) {
                    mi = model_info(session->id, session->name, session->created, session->json);
                }
                db.store_model(session, mi);
                msg::write_type(message_type::ADD_CASE, out);
            } break;
            case message_type::REMOVE_CASE_ID: {
                int64_t mid;
                int64_t rid;
                ia >> mid >> rid;
                auto session = db.read_model(mid);
                auto result = session->remove_case(rid);
                model_info mi;
                if (!db.try_get_info_item(mid, mi)) {
                    mi = model_info(session->id, session->name, session->created, session->json);
                }
                db.store_model(session, mi);
                msg::write_type(message_type::REMOVE_CASE_ID, out);
                oa << result;
            } break;
            case message_type::REMOVE_CASE_NAME: {
                int64_t mid;
                string rname;
                ia >> mid >> rname;
                auto session = db.read_model(mid);
                auto result = session->remove_case(rname);
                model_info mi;
                if (!db.try_get_info_item(mid, mi)) {
                    mi = model_info(session->id, session->name, session->created, session->json);
                }
                db.store_model(session, mi);
                msg::write_type(message_type::REMOVE_CASE_NAME, out);
                oa << result;
            } break;
            case message_type::GET_CASE_ID: {
                int64_t mid;
                int64_t rid;
                ia >> mid >> rid;
                auto session = db.read_model(mid);
                auto result = session->get_case(rid);
                msg::write_type(message_type::GET_CASE_ID, out);
                oa << result;
            } break;
            case message_type::GET_CASE_NAME: {
                int64_t mid;
                string rname;
                ia >> mid >> rname;
                auto session = db.read_model(mid);
                auto result = session->get_case(rname);
                msg::write_type(message_type::GET_CASE_NAME, out);
                oa << result;
            } break;
            case message_type::UPDATE_CASE: {
                int64_t mid;
                stm_case c;
                ia >> mid >> c;
                auto session = db.read_model(mid);
                if(session->update_case(c)) {
                    model_info mi;
                    if (!db.try_get_info_item(mid, mi)) {
                        mi = model_info(session->id, session->name, session->created, session->json);
                    }
                    db.store_model(session, mi);
                }
                msg::write_type(message_type::UPDATE_CASE, out);
            } break;
            case message_type::ADD_MODEL_REF: {
                int64_t mid;
                int64_t rid;
                model_ref_ mr;
                ia >> mid >> rid >> mr;
                auto session = db.read_model(mid);
                auto run = session->get_case(rid);
                if (run) { // If we found the run, we can add to it
                    run->add_model_ref(mr);
                    model_info mi;
                    if (!db.try_get_info_item(mid, mi)) {
                        mi = model_info(session->id, session->name, session->created, session->json);
                    }
                    db.store_model(session, mi);
                }
                msg::write_type(message_type::ADD_MODEL_REF, out);
            } break;
            case message_type::REMOVE_MODEL_REF: {
                int64_t mid;
                int64_t rid;
                string mkey;
                bool result = false;
                ia >> mid >> rid >> mkey;
                auto session = db.read_model(mid);
                auto run = session->get_case(rid);
                if (run) result = run->remove_model_ref(mkey);
                if (result) { // If model ref was successully removed, we must update session
                    model_info mi;
                    if (!db.try_get_info_item(mid, mi)) {
                        mi = model_info(session->id, session->name, session->created, session->json);
                    }
                    db.store_model(session, mi);
                }
                msg::write_type(message_type::REMOVE_MODEL_REF, out);
                oa << result;
            } break;
            case message_type::GET_MODEL_REF: {
                int64_t mid;
                int64_t rid;
                string mkey;
                model_ref_ mr = nullptr;
                ia >> mid >> rid >> mkey;
                auto session = db.read_model(mid);
                auto run = session->get_case(rid);
                if (run) mr = run->get_model_ref(mkey);
                msg::write_type(message_type::GET_MODEL_REF, out);
                oa << mr;
            } break;
            case message_type::FX: {
                core_iarchive ia(in, core_arch_flags);
                int64_t mid;string fx_arg;
                ia >> mid>>fx_arg;
                auto result=do_fx(mid,fx_arg);
                msg::write_type(message_type::FX,out);
                core_oarchive oa(out, core_arch_flags);
                oa<<result;
            } break;
            // other
            default:
                return false;
        }
    }
    return true;
}


}
