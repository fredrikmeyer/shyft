#include <shyft/energy_market/stm/srv/task/client.h>
#include <boost/serialization/shared_ptr.hpp>

namespace shyft::energy_market::stm::srv::task {
    
    using shyft::core::core_iarchive;
    using shyft::core::core_oarchive;
    using shyft::core::core_arch_flags;
    using shyft::core::srv_connection;
    using shyft::core::scoped_connect;
    using shyft::core::do_io_with_repair_and_retry;

    
void client::add_case(int64_t mid, const stm_case_& run) {
    scoped_connect sc(c);
    do_io_with_repair_and_retry(c, [&mid, &run](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::ADD_CASE, io);
            oa << mid << run;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::ADD_CASE) {
            return;
        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
}

bool client::remove_case(int64_t mid, int64_t rid) {
    scoped_connect sc(c);
    bool result = false;
    do_io_with_repair_and_retry(c, [&result, mid, rid](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::REMOVE_CASE_ID, io);
            oa << mid << rid;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::REMOVE_CASE_ID) {
            core_iarchive ia(io, core_arch_flags);
            ia >> result;

        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
    return result;
}

bool client::remove_case(int64_t mid, const string& rname ) {
    scoped_connect sc(c);
    bool result = false;
    do_io_with_repair_and_retry(c, [&result, mid, &rname](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::REMOVE_CASE_NAME, io);
            oa << mid << rname;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::REMOVE_CASE_NAME) {
            core_iarchive ia(io, core_arch_flags);
            ia >> result;

        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
    return result;
}

stm_case_ client::get_case(int64_t mid, int64_t rid) {
    scoped_connect sc(c);
    stm_case_ r;
    do_io_with_repair_and_retry(c, [&r, mid, rid](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::GET_CASE_ID, io);
            oa << mid << rid;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_CASE_ID) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
    return r;
}

stm_case_ client::get_case(int64_t mid, const string& rname) {
    scoped_connect sc(c);
    stm_case_ r;
    do_io_with_repair_and_retry(c, [&r, mid, rname](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::GET_CASE_NAME, io);
            oa << mid << rname;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::GET_CASE_NAME) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
    return r;
}

void client::update_case(int64_t mid, const stm_case& ce) {
    scoped_connect sc(c);
    do_io_with_repair_and_retry(c, [mid, ce](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::UPDATE_CASE, io);
            oa << mid << ce;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::UPDATE_CASE) {
            return;
        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
}

void client::add_model_ref(int64_t mid, int64_t rid, const model_ref_& mr) {
    scoped_connect sc(c);
    do_io_with_repair_and_retry(c, [mid, rid, &mr](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::ADD_MODEL_REF, io);
            oa << mid << rid << mr;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::ADD_MODEL_REF) {
            return;
        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
}

bool client::remove_model_ref(int64_t mid, int64_t rid, const string& mkey) {
    scoped_connect sc(c);
    bool r = false;
    do_io_with_repair_and_retry(c, [mid, rid, &r, &mkey](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::REMOVE_MODEL_REF, io);
            oa << mid << rid << mkey;
        }
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::REMOVE_MODEL_REF) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
        }
    });
    return r;
}

model_ref_ client::get_model_ref(int64_t mid, int64_t rid, const string& mkey) {
    scoped_connect sc(c);
    model_ref_ mr = nullptr;
    do_io_with_repair_and_retry(c, [mid, rid, &mkey, &mr](srv_connection&c) {
        auto& io = *c.io;
        {
            core_oarchive oa(io, core_arch_flags);
            msg::write_type(message_type::GET_MODEL_REF, io);
            oa << mid << rid << mkey;
        }
        auto response_type = msg::read_type(io);
            if (response_type == message_type::SERVER_EXCEPTION) {
                auto re = msg::read_exception(io);
                throw re;
            } else if (response_type == message_type::GET_MODEL_REF) {
                core_iarchive ia(io, core_arch_flags);
                ia >> mr;
            } else {
                throw std::runtime_error(string("Got unexpected response: ") + to_string((int) response_type));
            }
    });
    return mr;
}    


bool client::fx(int64_t mid, const string& fx_arg) {
    scoped_connect sc(c);
    bool r;
    do_io_with_repair_and_retry(c,[&r,&mid,&fx_arg](srv_connection&c) {
        auto& io = *c.io;
        msg::write_type(message_type::FX,io);
        core_oarchive oa(io, core_arch_flags);
        oa << mid<<fx_arg;
        auto response_type = msg::read_type(io);
        if (response_type == message_type::SERVER_EXCEPTION) {
            auto re = msg::read_exception(io);
            throw re;
        } else if (response_type == message_type::FX) {
            core_iarchive ia(io, core_arch_flags);
            ia >> r;
        } else {
            throw std::runtime_error(string("Got unexpected response:") + to_string((int)response_type));
        }
    });
    return r;
}

}
