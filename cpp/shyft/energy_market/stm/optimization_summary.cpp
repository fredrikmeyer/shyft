#include <shyft/energy_market/stm/optimization_summary.h>
#include <shyft/mp.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {

    optimization_summary::optimization_summary()
    :total{nan},sum_penalties{nan},minor_penalties{nan},major_penalties{nan},grand_total{nan}
    {
        mk_url_fx(this);
    }

    void optimization_summary::generate_url(std::back_insert_iterator<string> &rbi, int levels,
                                            int template_levels) const {
        //if (levels) {
        //    auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
        //    if (tmp) tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
       // }
#if 0
        if (!template_levels) {
            constexpr std::string_view a = ".summary";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            std::string idstr = ".summary";
            std::copy(std::begin(idstr), std::end(idstr), rbi);
        }
#endif
        if (mdl) {
            mdl->generate_url(rbi, levels, template_levels);
            constexpr std::string_view part_name=".summary";
            std::copy(std::begin(part_name),std::end(part_name),rbi);
        } else {
            constexpr std::string_view a = "O";
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }

    optimization_summary&
    optimization_summary::operator=(optimization_summary const &o) {
        using namespace ::shyft::mp;

        id=o.id;// (1) copy the id_base members
        name=o.name;
        json=o.json;
        hana::for_each(leaf_accessors(hana::type_c<optimization_summary>), // (2) boost hana stuff
            [&](auto a) {
                leaf_access(*this,a)=leaf_access(o,a);
            }
        );
        // leave out the fx stuff.
        return *this;
    }
    bool optimization_summary::operator==(const optimization_summary& other) const {
        if(this==&other) return true;
        return  hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<optimization_summary>),
            id_base::operator==(other),//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    };
    std::vector<string> 
    optimization_summary::all_urls(std::string const& prefix) const {
        std::vector<std::string> r;
        string pre=prefix+".summary.";
        hana::for_each(mp::leaf_accessor_map(hana::type_c<optimization_summary>), [&r,&pre] (auto p) {
            r.push_back(pre+hana::first(p).c_str());
        });
        return r;
    }

}
