#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::time_series::dd::apoint_ts;

    struct unit_group;
    using unit_group_ = shared_ptr<unit_group>;

    /* @brief energy market area
     *
     * suited for the stm optimization, keeps attributes for an area where the price/load and
     * other parameters controlling the optimization problem can be kept
     */
    struct energy_market_area : id_base {
        using super = id_base;

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        energy_market_area();
        energy_market_area(int id, const string& name, const string& json, const stm_system_& sys);

        bool operator==(const energy_market_area& other) const;
        bool operator!=(const energy_market_area& other) const { return !(*this == other); }
        vector<unit_group_> unit_groups;
        /** set a unit group on the energy market area creating an association */
        void set_unit_group(const unit_group_& ug);
        /** get the unit group, if available */
        unit_group_ get_unit_group() const;
        /** disassociating a unit group with the energy market area */
        unit_group_ remove_unit_group();

        vector<contract_> contracts; ///< association with contracts

        stm_system_ sys_() const { return sys.lock(); }
        stm_system__ sys; ///< reference up to the 'owning' optimization system.

        /** for practial planning usage, we end up in .realised,.schedule and .result */
        struct ts_triplet_ {
            url_fx_t url_fx;
            BOOST_HANA_DEFINE_STRUCT(ts_triplet_,
                (apoint_ts,realised), ///< SI unit, as in historical fact
                (apoint_ts,schedule), ///< the current schedule
                (apoint_ts,result)    ///< the optimal/simulated/estimated result
            );
        };
        /** */
        struct offering_ {
            url_fx_t url_fx;
            BOOST_HANA_DEFINE_STRUCT(offering_,
                (t_xy_, bids),    ///< x= price[money/J], y= amount [J/s] aka [W], ordered by t and then x,y x-ascending
                (ts_triplet_,usage), ///< how much has been used out of the bids
                (ts_triplet_,price)  ///< based on usage, what is the effective price paid for the usage [money/J]
            );
        };

        BOOST_HANA_DEFINE_STRUCT(energy_market_area,
            (apoint_ts, price),   ///< money/J (input, see also deman/supply)
            (apoint_ts, load),    ///< W (requirement for optimiser)
            (apoint_ts, max_buy), ///< W (constraint for optimiser)
            (apoint_ts, max_sale),///< W (constraint for optimiser)
            (apoint_ts, buy),     ///< W (result, as pr. optimiser)
            (apoint_ts, sale),    ///< W (result, as pr. optimiser)
            (apoint_ts, production), ///< W (result, as pr. optimiser)
            (apoint_ts, reserve_obligation_penalty), ///< money (result from optimiser)
            (offering_, demand), ///< the demand side of the market, willing to buy power(as seller you can sell to pricy offering first)
            (offering_, supply)  ///< the supply side of the market, wants to sell power(as buyer, you can pick the cheapest offering first)
        );

        x_serialize_decl();
    };

    using energy_market_area_ = shared_ptr<energy_market_area>;
}

x_serialize_export_key(shyft::energy_market::stm::energy_market_area);
