#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    
    run_parameters::run_parameters():n_inc_runs(0),n_full_runs(0),head_opt(false) {mk_url_fx(this);}
    run_parameters::run_parameters(stm_system* mdl): mdl{mdl},n_inc_runs(0),n_full_runs(0),head_opt(false)  {mk_url_fx(this);}

    void run_parameters::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (mdl) {
            mdl->generate_url(rbi, levels, template_levels);
            constexpr std::string_view part_name=".run_params";
            std::copy(std::begin(part_name),std::end(part_name),rbi);
        } else {
            constexpr std::string_view a = "RP";
            std::copy(std::begin(a), std::end(a), rbi);
        }
    }
    bool run_parameters::operator==(const run_parameters& other) const {
        if(this==&other) return true;
        return  hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<run_parameters>),
            id_base::operator==(other),//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    };

}
