#include <shyft/energy_market/stm/price_delivery_convert.h>

namespace shyft::energy_market::stm {

    generic_dt mk_t_xy_time_axis(t_xy_ const&pd, utctime t_end) {
        vector<utctime> t;t.reserve(pd->size()+1);
        for(auto const& e:*pd) {
            if(e.first>=t_end)
                break;
            t.push_back(e.first);
        }
        return {t,t_end};
    };

    vector<ats_vector> convert_to_price_delivery_tsv(t_xy_ const& pd, utctime t_end ) { //pd=price-delivery (or offering)
        if(!pd || pd->size()==0)
            return {};
        if(pd->cbegin()->first >= t_end) {
            throw runtime_error("make_price_delivery require t_end to be after first price/delivery curve");
        }

        constexpr auto max_dim_of=[](t_xy_ const&pd,utctime t_end) { //given t_xy_ find max dim of the xy points for any t
            size_t s=0;
            if(pd) {
                for(auto const& e:*pd) {
                    if(e.first>= t_end)
                        break;
                    if(e.second && e.second->points.size()>s)
                        s=e.second->points.size();
                }
            }
            return s;
        };


        auto const n_steps=max_dim_of(pd,t_end);
        auto ta=mk_t_xy_time_axis(pd,t_end);
        ats_vector p;
        ats_vector d;
        for(auto i=0u;i<n_steps;++i) {// fill all with 0.0 price and 0.0 volume
            p.emplace_back(ta,0.0,POINT_AVERAGE_VALUE);
            d.emplace_back(ta,0.0,POINT_AVERAGE_VALUE);
        }
        // then fill in price and volume, for each  timestep, in to a grid that fits time-points
        auto i=0u;// the i'th value position, on time-series, guaranteed to follow e below
        for(auto const&e:*pd) { //guaranteed ordering of  key,aka time
            if(e.first>=t_end)
                break;
            for(auto j=0u;j<e.second->points.size();++j){
                p[j].set(i,e.second->points[j].x);//
                d[j].set(i,e.second->points[j].y);//
            }
            ++i;
        }
        return {p,d};
    }

    double effective_price(double y, xy_points const&t,bool take_cheapest) {
        if ( (t.points.size()==0 )| !std::isfinite(y))
            return shyft::nan;
        if( (t.points.size()==1) | (y==0.0))
            return take_cheapest?t.points.front().x:t.points.back().x;
        auto y_sum{0.0};// acc effect as we consume from lowest to highest price
        auto p_sum{0.0};// price x effect
        auto accumulate_offerings=[&](auto const&p) ->bool {
            if(y_sum+p.y< y) { //entire step consumed.
                y_sum += p.y;
                p_sum+= p.y*p.x; // this much at this price
                return true; //continue iteration, collect more offerings
            } else { //partial usage of last step
                p_sum += (y-y_sum)*p.x;
                y_sum=y;
                return false;// signal we are done
            }
        };
        if(take_cheapest) {
            for(auto i=std::begin(t.points);i!=std::end(t.points);++i){
                if(!accumulate_offerings(*i)) break;
            }
        } else {
            for(auto i=std::rbegin(t.points);i!=std::rend(t.points);++i){
                if(!accumulate_offerings(*i)) break;
            }
        }
        return p_sum/y_sum; // average price for amount y
    }

    apoint_ts effective_price(apoint_ts const&d, t_xy_ const& pd, bool take_cheapest) {
        if( !d || pd==nullptr || pd->size()==0 ) {
            return {};// empty ts , we could consider return nan*d
        }
        auto const& d_ta=d.time_axis();
        auto t_end=d_ta.total_period().end;//
        // check that pd have a point before t_end, otherwise, ret nan?
        if( std::begin(*pd)->first >= t_end) {
            return {d_ta,shyft::nan,POINT_AVERAGE_VALUE};
        }
        auto pd_ta=mk_t_xy_time_axis(pd,t_end);
        auto ta=combine(d_ta,pd_ta);
        vector<double> v;v.reserve(ta.size());
        // ix -space crawling: we iterate through index space, to avoid bin-search
        // make valid&correct start position for  the iterators.
        auto pdx=pd_ta.index_of(ta.time(0));// first common index for pd
        auto pdi=pd->find(pd_ta.time(pdx));// get iterator to first xy entry
        auto dx=d_ta.index_of(ta.time(0));
        for(size_t i=0u;i<ta.size();++i) {
            v.push_back(effective_price(d.value(dx),*(pdi->second),take_cheapest));//
            // now check if we need to advance dx, pdi, or both.
            auto p=ta.period(i);// current period.
            if(dx+1<d_ta.size() && d_ta.time(dx+1)<=p.end ) {
                ++dx;// advance the index, because we are done at this interval
            }
            auto pdn= pdi;// try increment t-xy
            ++pdn;
            if(pdn != std::end(*pd) && pdn->first <=p.end) {
                ++pdi;
            }
        }
        return {ta,std::move(v),POINT_AVERAGE_VALUE};
    }

}
