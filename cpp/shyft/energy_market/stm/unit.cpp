#include <shyft/energy_market/stm/unit.h>

namespace shyft::energy_market::stm {

    namespace hana = boost::hana;
    namespace mp = shyft::mp;

	unit::unit(int id, const string& name, const string& json, const stm_hps_& hps)
		: super(id, name, json, hps) {
        mk_url_fx(this);
	}
	unit::unit() { 
        mk_url_fx(this);
    }
    
	void unit::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = dynamic_pointer_cast<stm_hps>(hps_());
            if (tmp) tmp->generate_url(rbi, levels - 1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/U{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/U"+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }

    bool unit::operator==(const unit& other) const {
        if(this==&other) return true;//equal by addr.
        return  hana::fold( // hana::any_of ... does not compile at all(even the example) on ms windows s c++ , so we use this that seems to be robust cross platform construct
            mp::leaf_accessors(hana::type_c<unit>),
            id_base::operator==(other),//initial value of the fold
            [this, &other](bool s, auto&& a) {
                return s?stm::equal_attribute(mp::leaf_access(*this, a), mp::leaf_access(other, a)):false; // only evaluate equal if the fold state is still true
            }
        );
    }
}
