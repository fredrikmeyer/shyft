#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <memory>
#include <string>
#include <optional>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::pair;
    using shyft::energy_market::id_base;
    using shyft::core::utctime;

    struct stm_system;

    struct run_parameters : public id_base {
        using super = id_base;

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        stm_system * mdl{nullptr}; ///< _not_ owned ref to the owning model that is _required_ to outlive the scope of the run_parameters,  The pointer should be const, but the model can be modified.

        run_parameters();
        run_parameters(stm_system* mdl);
        bool operator==(const run_parameters& other) const;
        bool operator!=(const run_parameters& other) const { return !( *this == other); }

        // Attributes:
        BOOST_HANA_DEFINE_STRUCT(run_parameters,
            (uint16_t, n_inc_runs), ///< Number of runs with incremental
            (uint16_t, n_full_runs),///< Number of full runs
            (bool, head_opt),  ///< head optimization on/off
            (generic_dt, run_time_axis), ///< the run_time_axis for optimization/simulation/computation
            (vector<pair<utctime, string>>, fx_log) ///< the logs as collected from the algorithm execution
        );

        x_serialize_decl();
    };

    using run_parameters_ = std::shared_ptr<run_parameters>;
    using run_parameters__ = std::weak_ptr<run_parameters>;
}

x_serialize_export_key(shyft::energy_market::stm::run_parameters);

BOOST_CLASS_VERSION(shyft::energy_market::stm::run_parameters, 1);
