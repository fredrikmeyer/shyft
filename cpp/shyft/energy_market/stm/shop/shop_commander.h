#pragma once

#include <string>
#include <string_view>
#include <vector>
#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_types.h>

namespace shyft::energy_market::stm::shop {

struct shop_commander {
    shop_api& api;
    shop_commander(shop_api& api) : api{api} {}

    // Commands:
    // Implementing individual methods for all commands, and for each invdvidual combination of options for each of them.
    // Methods are declared like this (* with some exceptions): void <keyword>_<specifier>_<options...>(<objects...>);
    // For example the "penalty flag" command can take the three options "on" "plant" and "min_p_con",
    // so then there is a method penalty_flag_plant_min_p_con. An alternative implementation could be to have
    // enumerations for the different options, but then additional logic is needed to verify that the combination is
    // legal, e.g. option "min_p_con" can only be used together with option "plant" and for command "penalty flag".
    // * Note that options might have underscore in their name which will lead to minor ambiguity, e.g.
    //   command "penalty flag /plant /min_p_con" corresponds to method penalty_flag_plant_min_p_con.
    // * Note that for simple boolean options, typically /on and /off, there is an exception: To reduce number
    //   of method names the option are usually represented as boolean argument (as if were object) instead of
    //   two different methods with "_on" and "_off" as suffix.
    void set_method_primal()                                           { api.execute_cmd("set method", "primal"); }
    void set_method_dual()                                             { api.execute_cmd("set method", "dual"); }
    void set_method_baropt()                                           { api.execute_cmd("set method", "baropt"); }
    void set_method_hydbaropt()                                        { api.execute_cmd("set method", "hydbaropt"); }
    void set_method_netprimal()                                        { api.execute_cmd("set method", "netprimal"); }
    void set_method_netdual()                                          { api.execute_cmd("set method", "netdual"); }
    void set_code_full()                                               { api.execute_cmd("set code", "full"); }
    void set_code_incremental()                                        { api.execute_cmd("set code", "incremental"); }
    void set_code_head()                                               { api.execute_cmd("set code", "head"); }
    void set_password(const std::string& key, const std::string& value){ api.execute_cmd("set password", "", key + std::string("=") + value); }
    void start_sim(int iterations)                                     { api.execute_cmd("start sim", "", std::to_string(iterations)); } //api.start_sim(std::to_string(iterations));
    void start_shopsim()                                               { api.execute_cmd("start shopsim", ""); }
    void log_file()                                                    { api.execute_cmd("log file"); }
    void log_file(const std::string& filename)                         { api.execute_cmd("log file", "", filename); }
    void log_file_lp(const std::string& filename)                      { api.execute_cmd("log file", "lp", filename); }
    void set_xmllog(bool on)                                           { api.execute_cmd("set xmllog", on ? "on" : "off"); }
    void return_simres(const std::string& filename)                    { api.execute_cmd("return simres", "", filename); }
    void return_simres_gen(const std::string& filename)                { api.execute_cmd("return simres", "gen", filename); }
    void save_series(const std::string& filename)                      { api.execute_cmd("save series", "", filename); }
    void save_xmlseries(const std::string& filename)                   { api.execute_cmd("save xmlseries", "", filename); }
    void print_model(const std::string& filename)                      { api.execute_cmd("print model", "", filename); }
    void return_scenario_result_table(const std::string& filename)     { api.execute_cmd("return scenario_result_table", "", filename); }
    void save_tunnelloss()                                             { api.execute_cmd("save tunnelloss", ""); }
    void save_shopsimseries(const std::string& filename)               { api.execute_cmd("save shopsimseries", "", filename); }
    void return_shopsimres(const std::string& filename)                { api.execute_cmd("return shopsimres", "", filename); }
    void return_shopsimres_gen(const std::string& filename)            { api.execute_cmd("return shopsimres", "gen", filename); }
    void save_xmlshopsimseries(const std::string& filename)            { api.execute_cmd("save xmlshopsimseries", "", filename); }
    void save_pq_curves(bool on)                                       { api.execute_cmd("save pq_curves", on ? "on" : "off"); }
    void print_pqcurves_all()                                          { api.execute_cmd("print pqcurves", "all"); }
    void print_pqcurves_all(const std::string& filename)               { api.execute_cmd("print pqcurves", "all", filename); }
    void print_pqcurves_original()                                     { api.execute_cmd("print pqcurves", "original"); }
    void print_pqcurves_original(const std::string& filename)          { api.execute_cmd("print pqcurves", "original", filename); }
    void print_pqcurves_convex()                                       { api.execute_cmd("print pqcurves", "convex"); }
    void print_pqcurves_convex(const std::string& filename)            { api.execute_cmd("print pqcurves", "convex", filename); }
    void print_pqcurves_final()                                        { api.execute_cmd("print pqcurves", "final"); }
    void print_pqcurves_final(const std::string& filename)             { api.execute_cmd("print pqcurves", "final", filename); }
    void print_mc_curves(const std::string& filename)                  { api.execute_cmd("print mc_curves", filename); }
    void print_mc_curves_up(const std::string& filename)               { api.execute_cmd("print mc_curves", "up", filename); }
    void print_mc_curves_down(const std::string& filename)             { api.execute_cmd("print mc_curves", "down", filename); }
    void print_mc_curves_pq(const std::string& filename)               { api.execute_cmd("print mc_curves", "pq", filename); }
    void print_mc_curves_mod(const std::string& filename)              { api.execute_cmd("print mc_curves", "mod", filename); }
    void print_mc_curves_up_pq(const std::string& filename)            { api.execute_cmd("print mc_curves", { { "up" }, { "pq" } }, { filename }); }
    void print_mc_curves_up_mod(const std::string& filename)           { api.execute_cmd("print mc_curves", { { "up" }, { "mod" } }, { filename }); }
    void print_mc_curves_up_pq_mod(const std::string& filename)        { api.execute_cmd("print mc_curves", { { "up" }, { "pq" }, { "mod" } }, { filename }); }
    void print_mc_curves_down_pq(const std::string& filename)          { api.execute_cmd("print mc_curves", { { "down" }, { "pq" } }, { filename }); }
    void print_mc_curves_down_mod(const std::string& filename)         { api.execute_cmd("print mc_curves", { { "down" }, { "mod" } }, { filename }); }
    void print_mc_curves_down_pq_mod(const std::string& filename)      { api.execute_cmd("print mc_curves", { { "down" }, { "pq" }, { "mod" } }, { filename }); }
    void print_mc_curves_up_down(const std::string& filename)          { api.execute_cmd("print mc_curves", { { "up" }, { "down" } }, { filename }); }
    void print_mc_curves_up_down_pq(const std::string& filename)       { api.execute_cmd("print mc_curves", { { "up" }, { "down" }, { "pq" } }, { filename }); }
    void print_mc_curves_up_down_mod(const std::string& filename)      { api.execute_cmd("print mc_curves", { { "up" }, { "down" }, { "mod" } }, { filename }); }
    void print_mc_curves_up_down_pq_mod(const std::string& filename)   { api.execute_cmd("print mc_curves", { { "up" }, { "down" }, { "pq" }, { "mod" } }, { filename }); }
    void print_mc_curves_pq_mod(const std::string& filename)           { api.execute_cmd("print mc_curves", { { "pq" }, { "mod" } }, { filename }); }
    void print_bp_curves()                                             { api.execute_cmd("print bp_curves"); }
    void print_bp_curves_all_combinations()                            { api.execute_cmd("print bp_curves", "all_combinations"); }
    void print_bp_curves_current_combination()                         { api.execute_cmd("print bp_curves", "current_combination"); }
    void print_bp_curves_from_zero()                                   { api.execute_cmd("print bp_curves", "from_zero"); }
    void print_bp_curves_mc_format()                                   { api.execute_cmd("print bp_curves", "mc_format"); }
    void print_bp_curves_operation()                                   { api.execute_cmd("print bp_curves", "operation"); }
    void print_bp_curves_discharge()                                   { api.execute_cmd("print bp_curves", "discharge"); }
    void print_bp_curves_production()                                  { api.execute_cmd("print bp_curves", "production"); }
    void print_bp_curves_dyn_points()                                  { api.execute_cmd("print bp_curves", "dyn_points"); }
    void print_bp_curves_old_points()                                  { api.execute_cmd("print bp_curves", "old_points"); }
    void print_bp_curves_market_ref_mc()                               { api.execute_cmd("print bp_curves", "market_ref_mc"); }
    void print_bp_curves_no_vertical_step()                            { api.execute_cmd("print bp_curves", "no_vertical_step"); }
    void penalty_flag_all(bool on)                                     { api.execute_cmd(std::string_view{"penalty flag"}, { {"all"}, {on ? "on" : "off"}  }); }
    void penalty_flag_reservoir_ramping(bool on)                       { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"reservoir"}, {"ramping"} }); }
    void penalty_flag_reservoir_endpoint(bool on)                      { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"reservoir"}, {"endpoint"} }); }
    void penalty_flag_load(bool on)                                    { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"load"} }); }
    void penalty_flag_powerlimit(bool on)                              { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"powerlimit"} }); }
    void penalty_flag_discharge(bool on)                               { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"discharge"} }); }
    void penalty_flag_plant_min_p_con(bool on)                         { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"plant"}, {"min_p_con"} }); }
    void penalty_flag_plant_max_p_con(bool on)                         { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"plant"}, {"max_p_con"} }); }
    void penalty_flag_plant_min_q_con(bool on)                         { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"plant"}, {"min_q_con"} }); }
    void penalty_flag_plant_max_q_con(bool on)                         { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"plant"}, {"max_q_con"} }); }
    void penalty_flag_plant_schedule(bool on)                          { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"plant"}, {"schedule"} }); }
    void penalty_flag_gate_min_q_con(bool on)                          { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"gate"}, {"min_q_con"} }); }
    void penalty_flag_gate_max_q_con(bool on)                          { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"gate"}, {"max_q_con"} }); }
    void penalty_flag_gate_ramping(bool on)                            { api.execute_cmd("penalty flag", { {on ? "on" : "off"}, {"gate"}, {"ramping"} }); }
    void penalty_cost_all(double value)                                { api.execute_cmd("penalty cost", "all", std::to_string(value)); }
    void penalty_cost_reservoir_ramping(double value)                  { api.execute_cmd("penalty cost", { {"reservoir"}, {"ramping"} }, { std::to_string(value) }); }
    void penalty_cost_reservoir_endpoint(double value)                 { api.execute_cmd("penalty cost", { {"reservoir"}, {"endpoint"} }, { std::to_string(value) }); }
    void penalty_cost_load(double value)                               { api.execute_cmd("penalty cost", "load", std::to_string(value)); }
    void penalty_cost_powerlimit(double value)                         { api.execute_cmd("penalty cost", "powerlimit", std::to_string(value)); }
    void penalty_cost_discharge(double value)                          { api.execute_cmd("penalty cost", "discharge", std::to_string(value)); }
    //void penalty_cost_plant_min_p_con(double value)                  {}
    //void penalty_cost_plant_max_p_con(double value)                  {}
    //void penalty_cost_plant_min_q_con(double value)                  {}
    //void penalty_cost_plant_max_q_con(double value)                  {}
    //void penalty_cost_plant_schedule(double value)                   {}
    //void penalty_cost_gate_min_q_con(double value)                   {}
    //void penalty_cost_gate_max_q_con(double value)                   {}
    void penalty_cost_gate_ramping(double value)                       { api.execute_cmd("penalty cost", { {"gate"}, {"ramping"} }, { std::to_string(value) }); }
    void penalty_cost_overflow(double value)                           { api.execute_cmd("penalty cost", "overflow", std::to_string(value)); }
    void penalty_cost_overflow_time_adjust(double value)               { api.execute_cmd("penalty cost", "overflow_time_adjust", std::to_string(value)); }
    void penalty_cost_reserve(double value)                            { api.execute_cmd("penalty cost", "reserve", std::to_string(value)); }
    void penalty_cost_soft_p_penalty(double value)                     { api.execute_cmd("penalty cost", "soft_p_penalty", std::to_string(value)); }
    void penalty_cost_soft_q_penalty(double value)                     { api.execute_cmd("penalty cost", "soft_q_penalty", std::to_string(value)); }
    void set_newgate(bool on)                                          { api.execute_cmd("set newgate", on ? "on" : "off"); }
    void set_ramping(int mode)                                         { api.execute_cmd("set ramping", mode == 0 ? "off" : mode == 1 ? "on" : "request"); }
    void set_mipgap(bool absolute, double value)                       { api.execute_cmd("set mipgap", absolute ? "absolute" : "relative", std::to_string(value)); }
    void set_nseg_all(double value)                                    { api.execute_cmd("set nseg", "all", std::to_string(value)); }
    void set_nseg_up(double value)                                     { api.execute_cmd("set nseg", "up", std::to_string(value)); }
    void set_nseg_down(double value)                                   { api.execute_cmd("set nseg", "down", std::to_string(value)); }
    void set_dyn_seg_on()                                              { api.execute_cmd("set dyn_seg", "on"); }
    void set_dyn_seg_incr()                                            { api.execute_cmd("set dyn_seg", "incr"); }
    void set_dyn_seg_mip()                                             { api.execute_cmd("set dyn_seg", "mip"); }
    void set_max_num_threads(int value)                                { api.execute_cmd("set max_num_threads", "", std::to_string(value)); }
    void set_parallel_mode_auto()                                      { api.execute_cmd("set parallel_mode", "auto"); }
    void set_parallel_mode_deterministic()                             { api.execute_cmd("set parallel_mode", "deterministic"); }
    void set_parallel_mode_opportunistic()                             { api.execute_cmd("set parallel_mode", "opportunistic"); }
    void set_capacity_all(double value)                                { api.execute_cmd("set capacity", "all", std::to_string(value)); }
    void set_capacity_gate(double value)                               { api.execute_cmd("set capacity", "gate", std::to_string(value)); }
    void set_capacity_bypass(double value)                             { api.execute_cmd("set capacity", "bypass", std::to_string(value)); }
    void set_capacity_spill(double value)                              { api.execute_cmd("set capacity", "spill", std::to_string(value)); }
    void set_headopt_feedback(double value)                            { api.execute_cmd("set headopt_feedback", "", std::to_string(value)); }
    void set_timelimit(int value)                                      { api.execute_cmd("set timelimit", "", std::to_string(value)); }
    void set_dyn_flex_mip(int value)                                   { api.execute_cmd("set dyn_flex_mip", "", std::to_string(value)); }
    void set_universal_mip_on()                                        { api.execute_cmd("set universal_mip", "on"); }
    void set_universal_mip_off()                                       { api.execute_cmd("set universal_mip", "off"); }
    void set_universal_mip_not_set()                                   { api.execute_cmd("set universal_mip", "not_set"); }
    void set_merge_on()                                                { api.execute_cmd("set merge", "on"); }
    void set_merge_off()                                               { api.execute_cmd("set merge", "off"); }
    void set_merge_stop()                                              { api.execute_cmd("set merge", "stop"); }
    void set_com_dec_period(int value)                                 { api.execute_cmd("set com_dec_period", "", std::to_string(value)); }
    void set_time_delay_unit_hour()                                    { api.execute_cmd("set time_delay_unit", "", "hour"); }
    void set_time_delay_unit_minute()                                  { api.execute_cmd("set time_delay_unit", "", "minute"); }
    void set_time_delay_unit_time_step_length()                        { api.execute_cmd("set time_delay_unit", "", "time_step_length"); }
    void set_power_head_optimization(bool on)                          { api.execute_cmd("set power_head_optimization", on ? "on" : "off"); }
    //void set_pump_head_optimization()                                {}
    void set_bypass_loss(bool on)                                      { api.execute_cmd("set bypass_loss", on ? "on" : "off"); }
    void set_reserve_slack_cost(double value)                          { api.execute_cmd("set reserve_slack_cost", "", std::to_string(value)); }
    void set_fcr_n_band(double value)                                  { api.execute_cmd("set fcr_n_band", "", std::to_string(value)); }
    void set_fcr_d_band(double value)                                  { api.execute_cmd("set fcr_d_band", "", std::to_string(value)); }
    void set_fcr_n_equality(int value)                                 { api.execute_cmd("set fcr_n_equality", "", std::to_string(value)); }
    //void set_linear_startup()                                        {}
    //void set_bid_aggregation_level()                                 {}
    //void set_delay_valuation()                                       {}
    void set_reserve_ramping_cost(double value)                        { api.execute_cmd("set reserve_ramping_cost", "", std::to_string(value)); }
    //void set_distribute_production()                                 {}
    void set_gen_turn_off_limit(double value)                          { api.execute_cmd("set gen_turn_off_limit", "", std::to_string(value)); }
    //void set_prod_from_ref_prod()                                    {}
    //void set_startup_cost_printout()                                 {}
    //void set_reserve_min_capacity()                                  {}
    //void set_ownership_scaling()                                     {}
    //void set_stop_cost_from_start_cost()                             {}
    //void set_simple_pq_recovery()                                    {}
    //void set_plant_uploading()                                       {}
    //void set_sim_schedule_correction()                               {}
    //void set_power_loss()                                            {}
    //void set_safe_mode()                                             {}
    //void set_lp_info()                                               {}
    //void set_nodelog()                                               {}
    //void set_solver()                                                {}
    //void read_model()                                                {}
    //void add_model()                                                 {}
    //void quit()                                                      {}

    void execute(const shop_command& command) {
        // Submit command object as individual string components to api.
        std::vector<std::string_view> options; options.reserve(command.options.size());
        for (auto it = command.options.cbegin(); it != command.options.cend(); it++) { options.emplace_back(*it); }
        std::vector<std::string_view> objects; objects.reserve(command.objects.size());
        for (auto it = command.objects.cbegin(); it != command.objects.cend(); it++) { objects.emplace_back(*it); }
        api.execute_cmd(command.keyword + (command.specifier.size() ? " " + command.specifier : ""), options, objects);
    }
    void execute_string(const std::string& command_string) {
        // Submit unparsed string directly to api.
        api.execute_cmd_string(command_string);
    }
    std::vector<shop_command> executed() {
        // Retrieve executed commands from the api, parsed into command objects.
        auto cmd_strings = api.get_executed_cmd_strings();
        return std::vector<shop_command>(cmd_strings.begin(), cmd_strings.end());
    }
    std::vector<std::string> executed_strings() {
        // Retrieve executed commands from the api as unparsed strings.
        return api.get_executed_cmd_strings();
    }
};

}
