#include <shyft/energy_market/stm/shop/shop_adapter.h>
#include <shyft/energy_market/stm/shop/shop_data.h>
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/energy_market/stm/price_delivery_convert.h>
#include <iostream>
namespace shyft::energy_market::stm::shop {

    shop_reserve_group shop_adapter::to_shop(const unit_group& a, const apoint_ts& mask_ts, const string& name) const {
        auto b = api.create<shop_reserve_group>(name);
        // a.group_type == would be fcr_n|d, affr,mfrr, frr, rr
        // and that selects what attribute to map the 
        // a.constraint(.limit,.flag,.cost , result .penalty)
        // into the shop_reserve_group property.'
        // TODO: consider find common  group/properties, and collapse common reserve group into one group.
        //       it is unclear if this will change the optimisation rules.
        auto create_group_ts = [&mask_ts](apoint_ts const &ts) {
            if (!ts) return ts;
            shyft::time_series::dd::qac_parameter nan_to_0; nan_to_0.constant_filler=0.0; nan_to_0.max_timespan=utctime(0);
            return ts.use_time_axis_from(mask_ts).quality_and_self_correction(nan_to_0) * mask_ts; // resample to mask_ts time axis, set 0 out when nan/uspecified, then apply mask (set 0 when mask 0)
        };
        switch (a.group_type) {
        case unit_group_type::fcr_n_up:
            set_optional(b.fcr_n_up_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.fcr_n_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::fcr_n_down:
            set_optional(b.fcr_n_down_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.fcr_n_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::fcr_d_up:
            set_optional(b.fcr_d_up_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.fcr_d_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::fcr_d_down:
            set_optional(b.fcr_d_down_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.fcr_d_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::afrr_up:
            set_optional(b.frr_up_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.frr_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::afrr_down:
            set_optional(b.frr_down_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.frr_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::rr_up:
            set_optional(b.rr_up_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.rr_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        case unit_group_type::rr_down:
            set_optional(b.rr_down_obligation, create_group_ts(a.obligation.schedule));
            set_optional(b.rr_penalty_cost, create_group_ts(a.obligation.cost));
            break;
        // TODO: Consider how to map mFRR up/down.
        }
        return b;
    }

    shop_market shop_adapter::to_shop(const energy_market_area& a) const {
        auto b = api.create<shop_market>(a.name);
        b.prod_area=ema_prod_area_id++;//a.id;// this seems to work.prod_area++;
        // a lot of trouble in this area: std::cout<<"EMA "<<a.name<<", shop.id="<<b.id<<", prod_area="<<b.prod_area <<std::endl;
        if (valid(a.price)) {
            set(b.sale_price, a.price);
            set(b.buy_price, ((apoint_ts)a.price) + 0.0000001); // TODO: Currently same as sale-price with fixed small addition (0.1 NOK/MWH == 0,0000001 NOK/WH), should add as dedicated stm attribute?
        }
        set_optional(b.max_sale, a.max_sale);
        set_optional(b.max_buy, a.max_buy);
        set_optional(b.load, a.load);
        if(exists(a.demand.bids)|| exists(a.supply.bids)) { // is there a price/delivery descriptive data?
            auto sale_pd= convert_to_price_delivery_tsv(a.demand.bids,time_axis.total_period().end);
            auto buy_pd= convert_to_price_delivery_tsv(a.supply.bids,time_axis.total_period().end);
            size_t n_sale= sale_pd.size()?sale_pd[0].size():0;
            size_t n_buy = buy_pd.size()?buy_pd[0].size():0;
            size_t n_mkts= n_buy>n_sale?n_buy:n_sale;
            vector<shop_market> sm;//submarkets
            // std::cout<<"Got price/delivery demand/supply (aka shop sale/buy) steps "<<n_mkts<<std::endl;
            apoint_ts zeros{time_axis,0.0,POINT_AVERAGE_VALUE};
            for(size_t i=0;i<n_mkts;++i) {
                auto m=api.create<shop_market>(a.name+"m"+std::to_string(i));
                sm.push_back(m);
                m.prod_area=b.prod_area;//same prod-area
                if(i<n_sale) {
                    set(m.sale_price,sale_pd[0][i]);//.average(time_axis).evaluate());
                    set(m.max_sale,sale_pd[1][i]);//.average(time_axis).evaluate());
                } else {
                    set(m.max_sale,zeros);
                    set(m.sale_price,zeros);//hmm.
                }
                if(i<n_buy) {
                    set(m.buy_price,buy_pd[0][i]);//.average(time_axis).evaluate());
                    set(m.max_buy,buy_pd[1][i]);//.average(time_axis).evaluate());
                } else {
                    set(m.max_buy,zeros);
                    //set(m.sale_price,zeros);//hmm.
                }
            }
            ema_map[b.prod_area]= sm;// keep it so we can sum results in from_shop.
        }
        return b;
    }

    xy_point_curve_ shop_adapter::get_spill_description(const reservoir& a) const {
        waterway_ wtr = nullptr;
        for (auto& conn : a.downstreams) {
            if (conn.role == connection_role::flood) {
                wtr = std::dynamic_pointer_cast<waterway>(conn.target);
                break;
            }
        }
        if (wtr) {
            if (wtr->gates.size() == 1) {
                auto gt = std::dynamic_pointer_cast<gate>(wtr->gates[0]);
                if (valid(gt->flow_description)) {
                    auto xyz_list = get(gt->flow_description); // Return std::shared_ptr<vector<xyz_point_curve_list>>
                    if (xyz_list && xyz_list->size()) {
                        return std::make_shared<xy_point_curve>((*xyz_list)[0].xy_curve);
                    }
                }
            } else {
                // Error handling if not the correct number of gates
                return nullptr;
            }
        } else {
            // Error handling if no spill waterway was found
            return nullptr;
        }
        return nullptr;
    }

    shop_reservoir shop_adapter::to_shop(const reservoir& a) const {
        auto b = api.create<shop_reservoir>(a.name);
        double default_lrl = 0., default_hrl = 0., default_max_vol = 0.;
        shared_ptr<xy_point_curve> vmap;
        auto valid_vmap = [&vmap]() {return vmap && vmap->points.size()>1;};
        if (valid(a.volume_level_mapping)) { // Exists and valid at period.start
            vmap = get(a.volume_level_mapping);
            if (valid_vmap()) { // require 2 or more points to consider valid
                default_lrl = vmap->points.front().y;
                default_hrl = vmap->points.back().y; // if spill description this default value will be replaced
                default_max_vol = vmap->points.back().x;
                set(b.vol_head, vmap);
            }
        }
        auto v = get_spill_description(a);
        if (v != nullptr) {
            default_hrl = v->points.front().x;
            if(valid_vmap())
                default_max_vol = vmap->calculate_x(default_hrl);
            set(b.flow_descr, v);
        }
        set_required(b.lrl, a.level.regulation_min, default_lrl);
        set_required(b.hrl, a.level.regulation_max, default_hrl);
        set_required(b.max_vol, a.volume.static_max, default_max_vol);// for some reason flooding does not work unless max_vol/ hrl is set
        if (!!a.water_value.endpoint_desc)
            set_optional(b.energy_value_input, get(a.water_value.endpoint_desc, period().end - utctimespan(1)));
        set_optional(b.min_head_constr, a.level.constraint.min);
        set_optional(b.max_head_constr, a.level.constraint.max);
        set_optional(b.min_vol_constr, a.volume.constraint.min);
        set_optional(b.max_vol_constr, a.volume.constraint.max);

        set_optional(b.tactical_limit_min, a.volume.constraint.tactical.min.limit);
        set_optional(b.tactical_limit_min_flag, a.volume.constraint.tactical.min.flag);
        set_optional(b.tactical_cost_min, a.volume.constraint.tactical.min.cost);
        //set_optional(b.tactical_cost_min_flag, a.volume.constraint.tactical.min.cost.flag);

        set_optional(b.tactical_limit_max, a.volume.constraint.tactical.max.limit);
        set_optional(b.tactical_limit_max_flag, a.volume.constraint.tactical.max.flag);
        set_optional(b.tactical_cost_max, a.volume.constraint.tactical.max.cost);
        //set_optional(b.tactical_cost_max_flag, a.volume.constraint.tactical.max.cost.flag);

        set_optional(b.level_ramping_up, a.ramping.level_up);
        set_optional(b.level_ramping_down, a.ramping.level_down);

        set_optional(b.lower_slack, a.volume.slack.lower);
        set_optional(b.upper_slack, a.volume.slack.upper);
        set_optional(b.flood_volume_cost_curve, a.volume.cost.flood.curve);
        set_optional(b.peak_volume_cost_curve, a.volume.cost.peak.curve);

        // Require user to use either realised or schedule
        if (exists(a.inflow.schedule)) {
            set_optional(b.inflow, a.inflow.schedule);
        } else {
            set_optional(b.inflow, a.inflow.realised);
        }

        set_optional(b.volume_schedule, a.volume.schedule);
        set_optional(b.level_schedule, a.level.schedule);
        set_optional(b.start_head, a.level.realised); // Set start reservoir scalar value in Shop from value at start of period in historical time series in stm
        return b;
    }

    shop_unit shop_adapter::to_shop(const unit& a) const {
        auto b = api.create<shop_unit>(a.name);
        double default_pmin = 0., default_pmax = 0.;
        apoint_ts run_ts(time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE);//a ts for entire time_axis with zeros
        auto z_fill=[&run_ts] (apoint_ts const &x) { // ensures zeros where nan/ not specified 
            if(!x || x.total_period().contains(run_ts.total_period())) 
                return x;// if empty or covering the run time-axis, return as-is
            using shyft::time_series::dd::qac_parameter;//otherwise, resample(could be average..) and replace nan with z0.
            qac_parameter nan_to_0;nan_to_0.constant_filler=0.0;nan_to_0.max_timespan=utctime(0);
            return x.use_time_axis_from(run_ts).quality_and_self_correction(nan_to_0);
        };
        auto nan_fill=[&run_ts] (apoint_ts const &x) { // ensures zeros where nan/ not specified
            if(!x || x.total_period().contains(run_ts.total_period())) 
                return x;// if empty or covering the run time-axis, return as-is
            //using shyft::time_series::dd::qac_parameter;//otherwise, resample(could be average..) and replace nan with z0.
            //qac_parameter nan_to_0;nan_to_0.constant_filler=0.0;nan_to_0.max_timespan=utctime(0);
            return x.use_time_axis_from(run_ts);//.quality_and_self_correction(nan_to_0);
        };

        // Generator efficiency
        if (valid(a.generator_description)) { // Exists and valid at period.start
            auto v = get(a.generator_description);
            if (v != nullptr) {
                default_pmin = v->points.front().x;
                default_pmax = v->points.back().x;
                set(b.gen_eff_curve, v);
            }
        }
        // Min/max/nom prod
        // TODO: Not used on aggregate level for pelton turbines when specified with needle combinations,
        // then it is part of the needle combination instead, but perhaps it does not hurt to always set them?
        set_required(b.p_min, a.production.static_min, default_pmin);
        set_required(b.p_max, a.production.static_max, default_pmax);
        b.p_nom = b.p_max;                          // Nominal production is often same as maximum, and we don't need it to be anything, but Shop uses it for calculating the
        set_optional(b.p_nom, a.production.nominal);// maximal FCR bounds and historically used it for several other uses and therefore in general recommend it to always be set!
        // Turbine efficiency
        if (valid(a.turbine_description)) { // Exists and valid at period.start
            auto v = get(a.turbine_description);
            if (v != nullptr && v->efficiencies.size() > 0) {
                if (v->efficiencies.size() < 2) {
                    // Only one efficiency: Set it as total turbine efficiency
                    b.turb_eff_curves = v->efficiencies.front().efficiency_curves;
                } else {
                    // More than one efficiency: Assume Pelton turbine, or forbidden area functionality, emit as needle combinations.
                    const auto mk_txy=[&](double v)->apoint_ts {
                        return apoint_ts(time_axis,v,shyft::time_series::POINT_AVERAGE_VALUE);
                    };
                    size_t i = 0;
                    for (auto& nc : v->efficiencies) {
                        auto b2 = api.create<shop_needle_combination>(a.name + std::to_string(++i));
                        b2.p_min = nc.production_min;
                        b2.p_max = nc.production_max;
                        b2.p_nom = nc.production_nominal; // See unit production.nominal above
                        b2.turb_eff_curves = nc.efficiency_curves;
                        //emit fcr_max/min, assuming start posistion the same (later we might collect from t-mapped turbine description)
                        b2.p_fcr_max = mk_txy(nc.fcr_max);
                        b2.p_fcr_min = mk_txy(nc.fcr_min);
                        api.connect_generator_needle_combination(b.id, b2.id);
                    }
                }
            }
        }
        // Other attributes
        set_optional(b.min_p_constr, a.production.constraint.min);
        set_optional(b.max_p_constr, a.production.constraint.max);
        set_optional(b.min_q_constr, a.discharge.constraint.min);
        set_optional(b.max_q_constr, a.discharge.constraint.max);
        set_optional(b.max_q_limit_rsv_down, a.discharge.constraint.max_from_downstream_level);
        set_optional(b.startcost, a.cost.start);
        set_optional(b.stopcost, a.cost.stop);
        set_optional(b.maintenance_flag, a.unavailability);
        set_optional(b.priority, a.priority);
        set_optional(b.discharge_schedule, a.discharge.schedule);
        if(valid(a.reserve.droop_steps)) {
            auto v = get(a.reserve.droop_steps);
            if (v != nullptr && v->points.size() > 0) {
                std::vector<double> y;
                for(auto const &p:v->points) {
                    y.push_back(p.y);//consider check p.y>0 or throw
                }
                b.discrete_droop_values=y;
            }
        }
        //--------------------------------------------------------
        // operational reserve attributes
        // notice that we currently do not emit the group membership time-series here
        // instead, we emit those along with the unit-group emitter.
        //    so first emit group-definitions with the requirement/constraint to be reached
        //       then  emit each unit member-ship time-series into the group for the respective
        //                  7 reserve types (fcr_n.up|down, fcr_d, frr.up|down, rr.up|down)
        const auto set_opt_w_flag=[this,nan_fill](auto&& shop_schedule, auto&& /*shop_flag*/, auto&& shop_min, auto&& shop_max, auto&& shop_cost, auto&& ts, auto&& ts_min, auto&& ts_max, auto&& ts_cost) {
            if(exists(ts)) {
                set_optional(shop_schedule,nan_fill(ts));//currently we do not need to worry about the flag, just pass nans
                // After tip from Ole Andreas/Tellef 2021.03.09: try to leave the flag, just pass on nans
                // and it could work, even better.
                // auto flag=a.reserve.droop.schedule.inside(0.5,1000.0,0.0, 1.0,0.0);// just set limits low..high enough for the practical usage.
                // set_optional(shop_flag,z_fill(flag));
            }
            set_optional(shop_min,ts_min);// there was a comment in shop manual about schedule at the same time as 
            set_optional(shop_max,ts_max);// maybe with flag?
            set_optional(shop_cost,ts_cost);
        };

        set_opt_w_flag(b.fixed_droop,b.fixed_droop_flag,b.droop_min,b.droop_max,b.droop_cost,a.reserve.droop.schedule,a.reserve.droop.min,a.reserve.droop.max,a.reserve.droop.cost);

        set_opt_w_flag(b.fcr_n_up_schedule,b.fcr_n_up_schedule_flag,b.fcr_n_up_min,b.fcr_n_up_max,b.fcr_n_up_cost,a.reserve.fcr_n.up.schedule,a.reserve.fcr_n.up.min,a.reserve.fcr_n.up.max,a.reserve.fcr_n.up.cost);
        set_opt_w_flag(b.fcr_n_down_schedule,b.fcr_n_down_schedule_flag,b.fcr_n_down_min,b.fcr_n_down_max,b.fcr_n_down_cost,a.reserve.fcr_n.down.schedule,a.reserve.fcr_n.down.min,a.reserve.fcr_n.down.max,a.reserve.fcr_n.down.cost);

        set_opt_w_flag(b.fcr_d_up_schedule,b.fcr_d_up_schedule_flag,b.fcr_d_up_min,b.fcr_d_up_max,b.fcr_d_up_cost,a.reserve.fcr_d.up.schedule,a.reserve.fcr_d.up.min,a.reserve.fcr_d.up.max,a.reserve.fcr_d.up.cost);
        set_opt_w_flag(b.fcr_d_down_schedule,b.fcr_d_down_schedule_flag,b.fcr_d_down_min,b.fcr_d_down_max,b.fcr_d_down_cost,a.reserve.fcr_d.down.schedule,a.reserve.fcr_d.down.min,a.reserve.fcr_d.down.max,a.reserve.fcr_d.down.cost);

        // TODO: shop frr, is that aFRR and mFRR ? currently we map affr to frr
        set_opt_w_flag(b.frr_up_schedule,b.frr_up_schedule_flag,b.frr_up_min,b.frr_up_max,b.frr_up_cost,a.reserve.afrr.up.schedule,a.reserve.afrr.up.min,a.reserve.afrr.up.max,a.reserve.afrr.up.cost);
        set_opt_w_flag(b.frr_down_schedule,b.frr_down_schedule_flag,b.frr_down_min,b.frr_down_max,b.frr_down_cost,a.reserve.afrr.down.schedule,a.reserve.afrr.down.min,a.reserve.afrr.down.max,a.reserve.afrr.down.cost);

        set_opt_w_flag(b.rr_up_schedule,b.rr_up_schedule_flag,b.rr_up_min,b.rr_up_max,b.rr_up_cost,a.reserve.rr.up.schedule,a.reserve.rr.up.min,a.reserve.rr.up.max,a.reserve.rr.up.cost);
        set_opt_w_flag(b.rr_down_schedule,b.rr_down_schedule_flag,b.rr_down_min,b.rr_down_max,b.rr_down_cost,a.reserve.rr.down.schedule,a.reserve.rr.down.min,a.reserve.rr.down.max,a.reserve.rr.down.cost);

        set_optional(b.p_fcr_min, z_fill(a.reserve.fcr_static_min));
        set_optional(b.p_fcr_max, z_fill(a.reserve.fcr_static_max));

        set_optional(b.fcr_mip_flag, a.reserve.fcr_mip);
        set_optional(b.p_rr_min, a.reserve.mfrr_static_min);


        // This can be controlled with the commited_flag
        // Case:
        // If commited_in is set to true (1), in the same interval as a schedule,
        // schedule should prevail over commited_in

        // After tip from Ole Andreas/Tellef 2021.03.09: try to leave the flag, just pass on nans
        // and it could work, even better.

        // production_schedule serves two roles in SHOP:
        //  - input to plan optimisation - planned production
        //  - input to inflow calculation - actual production

        // Require user to use either realised or schedule
        if (exists(a.production.schedule) || exists(a.production.commitment)) {
            set_optional(b.production_schedule, a.production.schedule);
            set_optional(b.committed_in, a.production.commitment);
        } else {
            set_optional(b.production_schedule, a.production.realised);
        }

        return b;
    }

    shop_power_plant shop_adapter::to_shop(const power_plant& a) const {
        auto b = api.create<shop_power_plant>(a.name);
        set_optional(b.outlet_line, a.outlet_level);
        set_optional(b.mip_flag, a.mip);
        set_optional(b.block_merge_tolerance, a.production.merge_tolerance);
        set_optional(b.power_ramping_up, a.production.ramping_up);
        set_optional(b.power_ramping_down, a.production.ramping_down);
        set_optional(b.discharge_ramping_up, a.discharge.ramping_up);
        set_optional(b.discharge_ramping_down, a.discharge.ramping_down);
        set_optional(b.min_p_constr, a.production.constraint_min);
        set_optional(b.max_p_constr, a.production.constraint_max);
        set_optional(b.min_q_constr, a.discharge.constraint_min);
        set_optional(b.max_q_constr, a.discharge.constraint_max);
        set_optional(b.production_schedule, a.production.schedule);
        set_optional(b.discharge_schedule, a.discharge.schedule);
        set_optional(b.intake_loss_from_bypass_flag, a.discharge.intake_loss_from_bypass_flag);
        set_optional(b.maintenance_flag, a.unavailability);
        return b;
    }

    shop_tunnel shop_adapter::to_shop(const waterway& wtr) const {
        auto b = api.create<shop_tunnel>(wtr.name);
        set_optional(b.max_flow, wtr.discharge.static_max); // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
        auto loss_coeff = valid_temporal(wtr.head_loss_coeff) ? get_temporal(wtr.head_loss_coeff, 0.0) : 0.0;
        set_optional(b.loss_factor, loss_coeff);
        set_optional(b.start_height, wtr.geometry.z0);
        set_optional(b.end_height, wtr.geometry.z1);
        set_optional(b.diameter, wtr.geometry.diameter);
        set_optional(b.length, wtr.geometry.length);
        if (wtr.gates.size() == 1) {
            if (auto gt = std::dynamic_pointer_cast<gate>(wtr.gates[0])) {
                set_optional(b.gate_adjustment_cost, gt->cost);
                set_optional(b.gate_opening_curve, gt->opening.constraint.positions);
                auto continuous_gate_flag = valid_temporal(gt->opening.constraint.continuous) ? get_temporal(gt->opening.constraint.continuous, 0.0) : 0.0;
                set(b.continuous_gate, continuous_gate_flag >= 0.5 ? 1 : 0);
                set_optional(b.gate_opening_schedule, gt->opening.schedule);
                set_optional(b.initial_opening, gt->opening.realised); // Set initial opening value in Shop from value at start of period in historical time series in stm
                set_optional(b.min_flow, gt->discharge.constraint.min);
                set_optional(b.max_flow, gt->discharge.constraint.max);
            }
        }
        return b;
    }

    shop_gate shop_adapter::to_shop_gate(const waterway& wtr, gate const* gt) const {
        auto b = api.create<shop_gate>(gt?gt->name:wtr.name);
        set_optional(b.max_discharge, wtr.discharge.static_max); // constraint on the flow in the river bed and so comply to all the parallel gates as a whole
        //set_optional(b.shape_discharge, wtr.delay); // time delay on the flow in the river bed and so comply to all the parallel gates as a whole
        set_shop_time_delay(b, wtr.delay); // time delay on the flow in the river bed and so comply to all the parallel gates as a whole
        if (gt) {
            if (&wtr != std::dynamic_pointer_cast<waterway>(gt->wtr_()).get()) // Gate is assumed to be in given water route!
                throw std::runtime_error("Specified waterroute and gate are not connected"s);

            set_optional(b.max_discharge, gt->discharge.static_max);
            set_optional(b.min_flow, gt->discharge.constraint.min);
            set_optional(b.max_flow, gt->discharge.constraint.max);
            set_optional(b.functions_meter_m3s, gt->flow_description);// if flood gate, this is already set on the rsv, is it ok to emit?
            set_optional(b.functions_deltameter_m3s, gt->flow_description_delta_h);

            // Note that only one of 'schedule_percent' and 'schedule_m3s' can exist,
            // setting 'schedule_percent' will erase 'schedule_m3s' and vice versa.
            if (exists(gt->discharge.schedule) || exists(gt->opening.schedule)) {
                // assume plan is intended and set to planned values
                set_optional(b.schedule_percent, gt->opening.schedule);
                set_optional(b.schedule_m3s, gt->discharge.schedule);  // Last to be set and taking precendence if available
            } else {
                // provide realised production data for inflow calculation, if available
                set_optional(b.schedule_percent, gt->opening.realised);
                set_optional(b.schedule_m3s, gt->discharge.realised);  // Last to be set and taking precendence if available
            }
        }
        return b;
    }

    shop_discharge_group shop_adapter::to_shop_discharge_group(const waterway& wtr) {
        auto b = api.create<shop_discharge_group>("dg_" + wtr.name);
        if(!!wtr.discharge.reference && (!!wtr.discharge.constraint.accumulated_max || !!wtr.discharge.constraint.accumulated_min) ) {
            auto dev0 = initial_acc_deviation(time_axis.total_period().start,wtr.discharge.realised,wtr.discharge.reference);
            set_required(b.weighted_discharge_m3s, wtr.discharge.reference);
            set_required(b.initial_deviation_mm3, dev0);
            set_optional(b.max_accumulated_deviation_mm3_down, wtr.discharge.constraint.accumulated_min);
            set_optional(b.max_accumulated_deviation_mm3_up, wtr.discharge.constraint.accumulated_max);
        }
        set_optional(b.min_discharge_m3s, wtr.discharge.constraint.min);
        set_optional(b.max_discharge_m3s, wtr.discharge.constraint.max);
        set_optional(b.ramping_up_m3s, wtr.discharge.constraint.ramping_up);
        set_optional(b.ramping_down_m3s, wtr.discharge.constraint.ramping_down);
        set_optional(b.min_discharge_penalty_cost, wtr.discharge.penalty.cost.constraint_min);
        set_optional(b.max_discharge_penalty_cost, wtr.discharge.penalty.cost.constraint_max);
        set_optional(b.ramping_up_penalty_cost, wtr.discharge.penalty.cost.ramping_up);
        set_optional(b.ramping_down_penalty_cost, wtr.discharge.penalty.cost.ramping_down);
        set_optional(b.penalty_cost_down_per_mm3, wtr.discharge.penalty.cost.accumulated_min);
        set_optional(b.penalty_cost_up_per_mm3, wtr.discharge.penalty.cost.accumulated_max);
        return b;
    }

    void shop_adapter::from_shop(energy_market_area& mkt, const shop_market& shop_mkt) {
        set_optional(mkt.buy, shop_mkt.buy);
        set_optional(mkt.sale, shop_mkt.sale);
        set_optional(mkt.reserve_obligation_penalty, shop_mkt.reserve_obligation_penalty);
        // Inflow calc
        set_optional(mkt.sale, shop_mkt.sim_sale);
        if( ema_map.find(shop_mkt.prod_area)!= ema_map.end()) {

            const auto add_contrib=[&] (auto &ema_a, auto &shop_a) {
                if(exists(shop_a)) {
                    if(ema_a.size()==0){
                        ema_a=shop_a.get();
                    } else {
                        ema_a= ema_a +shop_a.get();
                    }
                }
            };

            auto sms=ema_map[shop_mkt.prod_area];
            for(auto const&sm:sms) {
                add_contrib(mkt.sale,sm.sale);
                add_contrib(mkt.buy, sm.buy);
            }
            mkt.supply.usage.result = 1.0*mkt.buy;
            mkt.supply.price.result = effective_price(mkt.supply.usage.result,mkt.supply.bids,true);//take cheapest supply offerings

            mkt.demand.usage.result = -1.0*mkt.sale;
            mkt.demand.price.result = effective_price(mkt.demand.usage.result,mkt.demand.bids,false);// take best paid offers first
        }
    }

    void shop_adapter::from_shop(reservoir& rsv, const shop_reservoir& shop_rsv) {
        set_optional(rsv.volume.result, shop_rsv.storage);
        set_optional(rsv.level.result, shop_rsv.head);
        set_optional(rsv.volume.penalty, shop_rsv.penalty);
        set_optional(rsv.volume.constraint.tactical.min.penalty, shop_rsv.tactical_penalty_down);
        set_optional(rsv.volume.constraint.tactical.max.penalty, shop_rsv.tactical_penalty_up);
        set_optional(rsv.volume.cost.flood.penalty, shop_rsv.flood_volume_penalty);
        set_optional(rsv.volume.cost.peak.penalty, shop_rsv.peak_volume_penalty);

        // from inflow calculation
        set_optional(rsv.inflow.result, shop_rsv.sim_inflow);
        set_optional(rsv.volume.result, shop_rsv.sim_storage);
        set_optional(rsv.level.result, shop_rsv.sim_head);

        // water-values
        set_optional(rsv.water_value.result.local_energy, shop_rsv.energy_value_local_result);
        set_optional(rsv.water_value.result.local_volume, shop_rsv.water_value_local_result);
        set_optional(rsv.water_value.result.global_volume, shop_rsv.water_value_global_result);
        set_optional(rsv.water_value.result.end_value, shop_rsv.end_value);
    }

    void shop_adapter::from_shop(power_plant& pp, const shop_power_plant & shop_ps) {
        set_optional(pp.production.result, shop_ps.production);
        set_optional(pp.discharge.result, shop_ps.discharge);

        // from inflow calculation
        set_optional(pp.production.result, shop_ps.sim_production);
        set_optional(pp.discharge.result, shop_ps.sim_discharge);// * should be * sum of units..
        set_optional(pp.discharge.realised, shop_ps.sim_discharge);// * should be * sum of units..
#if 0
        // eventually, we should be able to fetch mc etc.
        {
            using namespace std;
            auto mc=shop_ps.best_profit_mc.get();
            calendar utc;
            for(auto const &e:mc){
                auto t=utctime_from_seconds64(e.first);
                cout<<"time "<<utc.to_string(t)<<"z="<<e.second.z<<", xy:"<<endl;
                for(auto const &p:e.second.xy_curve.points){
                    cout<<"    ("<<p.x<<","<<p.y<<")"<<endl;
                }
            }
        }
#endif
    }

    void shop_adapter::from_shop(unit& agg, const shop_unit& shop_agg) {
        set_optional(agg.discharge.result, shop_agg.discharge);
        set_optional(agg.production.result, shop_agg.production);

        // from operational reserve (frequency control)
        set_optional(agg.reserve.fcr_n.up.result,shop_agg.fcr_n_up_delivery);
        set_optional(agg.reserve.fcr_n.down.result,shop_agg.fcr_n_down_delivery);
        set_optional(agg.reserve.fcr_d.up.result,shop_agg.fcr_d_up_delivery);
        set_optional(agg.reserve.fcr_d.down.result,shop_agg.fcr_d_down_delivery);
        set_optional(agg.reserve.afrr.up.result,shop_agg.frr_up_delivery);
        set_optional(agg.reserve.afrr.down.result,shop_agg.frr_down_delivery);
        set_optional(agg.reserve.rr.up.result,shop_agg.rr_up_delivery);
        set_optional(agg.reserve.rr.down.result,shop_agg.rr_down_delivery);
        set_optional(agg.reserve.frr.up.penalty, shop_agg.frr_up_schedule_penalty);
        set_optional(agg.reserve.fcr_n.up.penalty, shop_agg.fcr_n_up_schedule_penalty);
        set_optional(agg.reserve.fcr_n.down.penalty, shop_agg.fcr_n_down_schedule_penalty);
        set_optional(agg.reserve.droop.result, shop_agg.droop_result);

        // from inflow calculation
        set_optional(agg.production.result, shop_agg.sim_production);// ? usually a forcing variable..

        set_optional(agg.discharge.result, shop_agg.sim_discharge);//
        set_optional(agg.discharge.realised, shop_agg.sim_discharge);// most natural mapping
        set_optional(agg.effective_head, shop_agg.sim_eff_head);
    }

    void shop_adapter::from_shop(waterway& wtr, const shop_gate& shop_gt) {
        set_optional(wtr.discharge.result, shop_gt.discharge);

        // from inflow calculation
        set_optional(wtr.discharge.result, shop_gt.sim_discharge);
    }

    void shop_adapter::from_shop(waterway& wtr, const shop_discharge_group& shop_dg) {
        set_optional(wtr.discharge.result, shop_dg.actual_discharge_m3s);
        set_optional(wtr.discharge.penalty.result.constraint_min, shop_dg.min_discharge_penalty);
        set_optional(wtr.discharge.penalty.result.constraint_max, shop_dg.max_discharge_penalty);
        set_optional(wtr.discharge.penalty.result.ramping_up, shop_dg.ramping_up_penalty);
        set_optional(wtr.discharge.penalty.result.ramping_down, shop_dg.ramping_down_penalty);
        set_optional(wtr.discharge.penalty.result.accumulated_min, shop_dg.lower_penalty_mm3);
        set_optional(wtr.discharge.penalty.result.accumulated_max, shop_dg.upper_penalty_mm3);

        // debug : get back results
        //set_optional(wtr.discharge.constraint.accumulated_min, shop_dg.max_accumulated_deviation_mm3_down);
        //set_optional(wtr.discharge.constraint.accumulated_max, shop_dg.max_accumulated_deviation_mm3_up);
        //set_optional(wtr.discharge.schedule, shop_dg.accumulated_deviation_mm3);
    }

    void shop_adapter::from_shop(gate& gt, const shop_gate& shop_gt) {
        set_optional(gt.discharge.result, shop_gt.discharge);

        // from inflow calculation
        set_optional(gt.discharge.result, shop_gt.sim_discharge);
    }

    void shop_adapter::from_shop(waterway& wtr, const shop_tunnel& shop_tn) {
        set_optional(wtr.discharge.result, shop_tn.flow);
        if (wtr.gates.size() == 1) {
            if (auto gt = std::dynamic_pointer_cast<gate>(wtr.gates[0])) {
                set_optional(gt->opening.result, shop_tn.gate_opening);
            }
        }

        // from inflow calculation
        set_optional(wtr.discharge.result, shop_tn.sim_flow);
    }

    void shop_adapter::from_shop(unit_group& a, const shop_reserve_group& b) {
        auto add_optional = [this](apoint_ts& ts, const auto& shop_attr) {
            if (!ts) {
                set_optional(ts, shop_attr);
            } else { // read values from shop attribute and add to the time series
                apoint_ts tmp_ts(time_axis,0.0,shyft::time_series::POINT_AVERAGE_VALUE); // a ts filled with zeros for each step of entire time axis
                set_optional(tmp_ts, shop_attr);
                ts = ts + tmp_ts;
            }
        };
        switch (a.group_type) {
        case unit_group_type::fcr_n_up:
            add_optional(a.obligation.result, b.fcr_n_up_slack);
            add_optional(a.obligation.penalty, b.fcr_n_up_violation);
            break;
        case unit_group_type::fcr_n_down:
            add_optional(a.obligation.result, b.fcr_n_down_slack);
            add_optional(a.obligation.penalty, b.fcr_n_down_violation);
            break;
        case unit_group_type::fcr_d_up:
            add_optional(a.obligation.result, b.fcr_d_up_slack);
            add_optional(a.obligation.penalty, b.fcr_d_up_violation);
            break;
        case unit_group_type::fcr_d_down:
            add_optional(a.obligation.penalty, b.fcr_d_down_violation);
            add_optional(a.obligation.result, b.fcr_d_down_slack);
            break;
        case unit_group_type::afrr_up:
            add_optional(a.obligation.result, b.frr_up_slack);
            add_optional(a.obligation.penalty, b.frr_up_violation);
            break;
        case unit_group_type::afrr_down:
            add_optional(a.obligation.result, b.frr_down_slack);
            add_optional(a.obligation.penalty, b.frr_down_violation);
            break;
        case unit_group_type::rr_up:
            add_optional(a.obligation.result, b.rr_up_slack);
            add_optional(a.obligation.penalty, b.rr_up_violation);
            break;
        case unit_group_type::rr_down:
            add_optional(a.obligation.result, b.rr_down_slack);
            add_optional(a.obligation.penalty, b.rr_down_violation);
            break;
        // TODO: Consider how to map mFRR up/down.
        }
    }

    void shop_adapter::from_shop(optimization_summary& osm, const shop_objective& shop_obj) const {
        /* Note double set_optional on shop attrs containing sim_**
         * If shop is used for optimization, then the attr NOT containing sim_** will be set.
         * Else, if shop is used for simulation, the sim_** attr will be set. Hence we expose
         * the shop attr to stm model by context, how the model i runned by the user.
         **/
        // Top attrs(
        set_optional(osm.total, shop_obj.total);
        set_optional(osm.sum_penalties, shop_obj.sum_penalties);
        set_optional(osm.minor_penalties, shop_obj.minor_penalties);
        set_optional(osm.major_penalties, shop_obj.major_penalties);
        set_optional(osm.grand_total, shop_obj.grand_total);
        // Inflow calc
        set_optional(osm.grand_total, shop_obj.sim_grand_total);

        // Reservoir
        set_optional(osm.reservoir.sum_ramping_penalty, shop_obj.rsv_ramping_penalty);
        set_optional(osm.reservoir.sum_limit_penalty, shop_obj.rsv_penalty);
        set_optional(osm.reservoir.sum_limit_penalty, shop_obj.sim_rsv_penalty);
        set_optional(osm.reservoir.end_value, shop_obj.rsv_end_value);
        set_optional(osm.reservoir.end_value, shop_obj.sim_rsv_end_value);
        set_optional(osm.reservoir.end_limit_penalty, shop_obj.rsv_end_penalty);
        set_optional(osm.reservoir.hard_limit_penalty, shop_obj.rsv_hard_limit_penalty);

        // Waterway
        set_optional(osm.waterway.vow_in_transit, shop_obj.vow_in_transit);
        set_optional(osm.waterway.sum_discharge_fee, shop_obj.sum_discharge_fee);
        set_optional(osm.waterway.discharge_group_penalty, shop_obj.discharge_group_penalty);
        set_optional(osm.waterway.discharge_group_ramping_penalty, shop_obj.discharge_group_ramping_penalty);

        // Gate
        set_optional(osm.gate.ramping_penalty, shop_obj.gate_ramping_penalty);
        set_optional(osm.gate.discharge_cost, shop_obj.gate_discharge_cost);
        set_optional(osm.gate.discharge_constraint_penalty, shop_obj.gate_q_constr_penalty);

        // Spill
        set_optional(osm.spill.cost, shop_obj.gate_spill_cost);
        set_optional(osm.spill.physical_cost, shop_obj.physical_spill_cost);
        set_optional(osm.spill.nonphysical_cost, shop_obj.nonphysical_spill_cost);
        set_optional(osm.spill.physical_volume, shop_obj.physical_spill_volume);
        set_optional(osm.spill.nonphysical_volume, shop_obj.nonphysical_spill_volume);

        // Bypass
        set_optional(osm.bypass.cost, shop_obj.bypass_cost);

        // Ramping
        set_optional(osm.ramping.ramping_penalty, shop_obj.sum_ramping_penalty);

        // Reserve
        set_optional(osm.reserve.violation_penalty, shop_obj.reserve_violation_penalty);
        set_optional(osm.reserve.sale_buy, shop_obj.reserve_sale_buy);
        set_optional(osm.reserve.obligation_value, shop_obj.reserve_oblig_value);

        // Unit
        set_optional(osm.unit.startup_cost, shop_obj.startup_costs);
        set_optional(osm.unit.startup_cost, shop_obj.sim_startup_costs);
        set_optional(osm.unit.schedule_penalty, shop_obj.gen_schedule_penalty);

        // Plant
        set_optional(osm.plant.production_constraint_penalty, shop_obj.plant_p_constr_penalty);
        set_optional(osm.plant.discharge_constraint_penalty, shop_obj.plant_q_constr_penalty);
        set_optional(osm.plant.schedule_penalty, shop_obj.plant_schedule_penalty);
        set_optional(osm.plant.ramping_penalty, shop_obj.plant_ramping_penalty);

        // Market
        set_optional(osm.market.sum_sale_buy, shop_obj.market_sale_buy);
        set_optional(osm.market.sum_sale_buy, shop_obj.market_sale_buy);

        set_optional(osm.market.load_penalty, shop_obj.load_penalty);
        set_optional(osm.market.load_value, shop_obj.load_value);

        // Inflow calc
        set_optional(osm.market.sum_sale_buy, shop_obj.sim_market_sale_buy);
    }
}
