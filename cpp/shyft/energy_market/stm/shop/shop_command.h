#pragma once

#include <string>
#include <vector>
#include <stdexcept>
#include <regex>

#include <shyft/core/core_serialization.h>

namespace shyft::energy_market::stm::shop {

struct shop_command {
    std::string keyword;
    std::string specifier;
    std::vector<std::string> options;
    std::vector<std::string> objects;

    shop_command() = default; // needed for serialization
    explicit shop_command(const std::string& cmd) { // command cmd("penalty cost /reservoir /ramping 123.50");
        parse_lenient(cmd);
    }
    void parse_lenient(const std::string& cmd) {
        // Base syntax: keyword [specifier] [/<option>...] [<object>...]
        // Lenient version, accepts anything that shop does in command file:
        // - Order of options and objects does not matter, and they can be mixed.
        // - The '/' character identifies an option, even without space.
        // - Commands without specifier may have options and/or objects,
        //   though not sure about this one, i.e. if any such command exists.
        static const std::regex re_prefix(R"(^\s*([^/\s]+)(?:\s+([^/\s]+))?)");
        std::smatch m;
        if (std::regex_search(cmd, m, re_prefix)) {
            if (m[1].matched)
                keyword = m[1].str();
            if (m[2].matched)
                specifier = m[2].str();
            static const std::regex re_suffix(R"((/)?([^/\s]+))");
            for (auto it = std::sregex_iterator(m[0].second, cmd.end(), re_suffix), end = std::sregex_iterator(); it != end; ++it) {
                if ((*it)[1].matched)
                    options.push_back((*it)[2].str());
                else
                    objects.push_back((*it)[2].str());
            }   
        }
    }
    void parse_strict(const std::string& cmd) {
        // Base syntax: keyword [specifier [/<option>...] [<object>...]]
        // Strict version, only accepts the normalized form used when converting back
        // to string with string():
        // - Commands that are not keyword-only (single word, like "quit"), 
        //   must have a specifier, i.e. options/objects only allowed after 
        //   keyword and specifier.
        // - Any options must come before objects, i.e. the following is not accepted
        //   here, even though it is accepted by shop in command file:
        //     "print mc_curves mc.xml 12 24 /up /down"
        //   It must be rewritten to:
        //     "print mc_curves /up /down mc.xml 12 24"
        // - Space must separate every part, i.e. the following is not accepted here, even
        //   though it is accepted by shop in command file:
        //     "return simres/generator shop_gen.result"
        //   It must be rewritten to:
        //     "return simres /generator shop_gen.result"
        static const std::regex re(
            "^(?:\\s*)" // optional whitespace at beginning (ignored)
            "([^/\\s]+)" // <keyword>
            "(?:\\s+" // optional whitespace (followed by [<specifier> [/<option>...] [<object>...]])
                "([^/\\s]+)" // <specifier>
                "((?:\\s+/[^/\\s]+)+)?" // optional whitespace followed by list of <option> prefixed with / and separated by space
                "((?:\\s+[^/\\s]+)+)?" // optional whitespace followed by list of <object> separated by space
            ")?"
            "(?:\\s*)$" // optional whitespace at end (ignored)
        );
        static const std::regex re_opts(R"((?:\s+/)([^/\s]+))");
        static const std::regex re_objs(R"((?:\s+)([^/\s]+))");
        std::smatch m;
        if (std::regex_match(cmd, m, re)) {
            keyword = m[1].str();
            if (m[2].matched)
                specifier = m[2].str();
            if (m[3].matched) {
                for (std::sregex_iterator it = std::sregex_iterator(m[3].first, m[3].second, re_opts), end = std::sregex_iterator(); it != end; ++it)
                    options.push_back(it->str(1));
            }
            if (m[4].matched) {
                for (std::sregex_iterator it = std::sregex_iterator(m[4].first, m[4].second, re_objs), end = std::sregex_iterator(); it != end; ++it)
                    objects.push_back(it->str(1));
            }
        } else {
            throw std::invalid_argument("invalid shop command syntax");
        }
    }
    explicit operator std::string() const {
        std::string str{ keyword };
        if (!specifier.empty()) { str += " " + specifier; }
        for (auto it = options.cbegin(); it != options.cend(); ++it) { str += " /" + *it; }
        for (auto it = objects.cbegin(); it != objects.cend(); ++it) { str += " " + *it; }
        return str;
    }
    shop_command(std::string keyword, std::string specifier) : keyword{std::move(keyword)}, specifier{std::move(specifier)} {} // command cmd("save", "tunnelloss");
    shop_command(std::string keyword, std::string specifier, int value) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, objects{std::to_string(value)} {} // command cmd("set", "max_num_threads", 8);
    shop_command(std::string keyword, std::string specifier, double value) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, objects{std::to_string(value)} {} // command cmd("set", "fcr_n_band", 0.4);
    //shop_command(std::string keyword, std::string specifier, std::string option_or_object, bool is_object = false) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{is_object?"":std::move(option_or_object)}, objects{is_object?std::move(option_or_object):""} {} // command cmd("set", "method", "dual"); command cmd2("log", "file", "filename.log", true);
    //shop_command(std::string keyword, std::string specifier, std::string option) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(option)} {} // command cmd("set", "method", "dual");
    shop_command(std::string keyword, std::string specifier, std::string option) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(option)} {} // command cmd("set", "method", "dual");
    shop_command(std::string keyword, std::string specifier, std::string object, bool /*is_object*/) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, objects{std::move(object)} {} // command cmd2("log", "file", "filename.log");
    shop_command(std::string keyword, std::string specifier, std::string option, std::string object) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(option)}, objects{std::move(object)} {} // command cmd("return", "simres", "gen", "filename.log");
    shop_command(std::string keyword, std::string specifier, std::string option, int value) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(option)}, objects{std::to_string(value)} {} // command cmd{"set", "nseg", "all", 3};
    shop_command(std::string keyword, std::string specifier, std::string option, double value) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(option)}, objects{std::to_string(value)} {} // command cmd("penalty", "cost", "all", 123.50);
    shop_command(std::string keyword, std::string specifier, std::vector<std::string> options) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(options)} {} // command cmd("penalty", "flag", {"on", "reservoir", "ramping"});
    shop_command(std::string keyword, std::string specifier, std::vector<std::string> options, int value) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(options)}, objects{std::to_string(value)} {}
    shop_command(std::string keyword, std::string specifier, std::vector<std::string> options, double value) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(options)}, objects{std::to_string(value)} {} // command cmd("penalty", "cost", { "reservoir", "ramping" }, 123.50);
    shop_command(std::string keyword, std::string specifier, std::vector<std::string> options, std::string object) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(options)}, objects{std::move(object)} {}
    shop_command(std::string keyword, std::string specifier, std::vector<std::string> options, std::vector<std::string> objects) : keyword{std::move(keyword)}, specifier{std::move(specifier)}, options{std::move(options)}, objects{std::move(objects)} {} // Unused, no commands take multiple objects?
    bool operator==(const shop_command& other) const {
        return keyword == other.keyword
            && specifier == other.specifier
            && options == other.options
            && objects == other.objects;
    }
    bool operator!=(const shop_command& other) const {
        return !operator==(other);
    }
    static shop_command set_method_primal()                                 { return shop_command{ "set", "method", "primal" }; }
    static shop_command set_method_dual()                                   { return shop_command{ "set", "method", "dual" }; }
    static shop_command set_method_baropt()                                 { return shop_command{ "set", "method", "baropt" }; }
    static shop_command set_method_hydbaropt()                              { return shop_command{ "set", "method", "hydbaropt" }; }
    static shop_command set_method_netprimal()                              { return shop_command{ "set", "method", "netprimal" }; }
    static shop_command set_method_netdual()                                { return shop_command{ "set", "method", "netdual" }; }
    static shop_command set_code_full()                                     { return shop_command{ "set", "code", "full" }; }
    static shop_command set_code_incremental()                              { return shop_command{ "set", "code", "incremental" }; }
    static shop_command set_code_head()                                     { return shop_command{ "set", "code", "head" }; }
    static shop_command set_password(std::string key, std::string value)    { return shop_command{ "set password", key + std::string("=") + value, true }; }
    static shop_command start_sim(int iterations)                           { return shop_command{ "start", "sim", iterations }; } //api.start_sim(std::to_string(iterations));
    static shop_command start_shopsim()                                     { return shop_command{ "start", "shopsim" }; }
    static shop_command log_file()                                          { return shop_command{ "log", "file" }; }
    static shop_command log_file(std::string filename)                      { return shop_command{ "log", "file", std::move(filename), true }; }
    static shop_command log_file_lp(std::string filename)                   { return shop_command{ "log", "file", "lp", std::move(filename) }; }
    static shop_command set_xmllog(bool on)                                 { return shop_command{ "set", "xmllog", on ? "on" : "off" }; }
    static shop_command return_simres(std::string filename)                 { return shop_command{ "return", "simres", std::move(filename), true }; }
    static shop_command return_simres_gen(std::string filename)             { return shop_command{ "return", "simres", "gen", std::move(filename) }; }
    static shop_command save_series(std::string filename)                   { return shop_command{ "save", "series", std::move(filename), true }; }
    static shop_command save_xmlseries(std::string filename)                { return shop_command{ "save", "xmlseries", std::move(filename), true }; }
    static shop_command print_model(std::string filename)                   { return shop_command{ "print", "model", std::move(filename), true }; }
    static shop_command return_scenario_result_table(std::string filename)  { return shop_command{ "return", "scenario_result_table", std::move(filename), true }; }
    static shop_command save_tunnelloss()                                   { return shop_command{ "save", "tunnelloss" }; }
    static shop_command save_shopsimseries(std::string filename)            { return shop_command{ "save", "shopsimseries", std::move(filename), true }; }
    static shop_command return_shopsimres(std::string filename)             { return shop_command{ "return", "shopsimres", std::move(filename), true }; }
    static shop_command return_shopsimres_gen(std::string filename)         { return shop_command{ "return", "shopsimres", "gen", std::move(filename) }; }
    static shop_command save_xmlshopsimseries(std::string filename)         { return shop_command{ "save", "xmlshopsimseries", std::move(filename), true }; }
    static shop_command save_pq_curves(bool on)                             { return shop_command{ "save", "pq_curves", on ? "on" : "off" }; }
    static shop_command print_pqcurves_all()                                { return shop_command{ "print", "pqcurves", "all" }; }
    static shop_command print_pqcurves_all(std::string filename)            { return shop_command{ "print", "pqcurves", "all", std::move(filename) }; }
    static shop_command print_pqcurves_original()                           { return shop_command{ "print", "pqcurves", "original" }; }
    static shop_command print_pqcurves_original(std::string filename)       { return shop_command{ "print", "pqcurves", "original", std::move(filename) }; }
    static shop_command print_pqcurves_convex()                             { return shop_command{ "print", "pqcurves", "convex" }; }
    static shop_command print_pqcurves_convex(std::string filename)         { return shop_command{ "print", "pqcurves", "convex", std::move(filename) }; }
    static shop_command print_pqcurves_final()                              { return shop_command{ "print", "pqcurves", "final" }; }
    static shop_command print_pqcurves_final(std::string filename)          { return shop_command{ "print", "pqcurves", "final", std::move(filename) }; }
    static shop_command print_mc_curves(std::string filename)               { return shop_command{ "print", "mc_curves", std::move(filename) }; }
    static shop_command print_mc_curves_up(std::string filename)            { return shop_command{ "print", "mc_curves", "up", std::move(filename) }; }
    static shop_command print_mc_curves_down(std::string filename)          { return shop_command{ "print", "mc_curves", "down", std::move(filename) }; }
    static shop_command print_mc_curves_pq(std::string filename)            { return shop_command{ "print", "mc_curves", "pq", std::move(filename) }; }
    static shop_command print_mc_curves_mod(std::string filename)           { return shop_command{ "print", "mc_curves", "mod", std::move(filename) }; }
    static shop_command print_mc_curves_up_pq(std::string filename)         { return shop_command{ "print", "mc_curves", { { "up" }, { "pq" } }, std::move(filename) }; }
    static shop_command print_mc_curves_up_mod(std::string filename)        { return shop_command{ "print", "mc_curves", { { "up" }, { "mod" } }, std::move(filename) }; }
    static shop_command print_mc_curves_up_pq_mod(std::string filename)     { return shop_command{ "print", "mc_curves", { { "up" }, { "pq" }, { "mod" } }, std::move(filename) }; }
    static shop_command print_mc_curves_down_pq(std::string filename)       { return shop_command{ "print", "mc_curves", { { "down" }, { "pq" } }, std::move(filename) }; }
    static shop_command print_mc_curves_down_mod(std::string filename)      { return shop_command{ "print", "mc_curves", { { "down" }, { "mod" } }, std::move(filename) }; }
    static shop_command print_mc_curves_down_pq_mod(std::string filename)   { return shop_command{ "print", "mc_curves", { { "down" }, { "pq" }, { "mod" } }, std::move(filename) }; }
    static shop_command print_mc_curves_up_down(std::string filename)       { return shop_command{ "print", "mc_curves", { { "up" }, { "down" } }, std::move(filename) }; }
    static shop_command print_mc_curves_up_down_pq(std::string filename)    { return shop_command{ "print", "mc_curves", { { "up" }, { "down" }, { "pq" } }, std::move(filename) }; }
    static shop_command print_mc_curves_up_down_mod(std::string filename)   { return shop_command{ "print", "mc_curves", { { "up" }, { "down" }, { "mod" } }, std::move(filename) }; }
    static shop_command print_mc_curves_up_down_pq_mod(std::string filename){ return shop_command{ "print", "mc_curves", { { "up" }, { "down" }, { "pq", "mod" } }, std::move(filename) }; }
    static shop_command print_mc_curves_pq_mod(std::string filename)        { return shop_command{ "print", "mc_curves", { { "pq" }, { "mod" } }, std::move(filename) }; }
    static shop_command print_bp_curves()                                   { return shop_command{ "print", "bp_curves" }; }
    static shop_command print_bp_curves_all_combinations()                  { return shop_command{ "print", "bp_curves", "all_combinations" }; }
    static shop_command print_bp_curves_current_combination()               { return shop_command{ "print", "bp_curves", "current_combination" }; }
    static shop_command print_bp_curves_from_zero()                         { return shop_command{ "print", "bp_curves", "from_zero" }; }
    static shop_command print_bp_curves_mc_format()                         { return shop_command{ "print", "bp_curves", "mc_format" }; }
    static shop_command print_bp_curves_operation()                         { return shop_command{ "print", "bp_curves", "operation" }; }
    static shop_command print_bp_curves_discharge()                         { return shop_command{ "print", "bp_curves", "discharge" }; }
    static shop_command print_bp_curves_production()                        { return shop_command{ "print", "bp_curves", "production" }; }
    static shop_command print_bp_curves_dyn_points()                        { return shop_command{ "print", "bp_curves", "dyn_points" }; }
    static shop_command print_bp_curves_old_points()                        { return shop_command{ "print", "bp_curves", "old_points" }; }
    static shop_command print_bp_curves_market_ref_mc()                     { return shop_command{ "print", "bp_curves", "market_ref_mc" }; }
    static shop_command print_bp_curves_no_vertical_step()                  { return shop_command{ "print", "bp_curves", "no_vertical_step" }; }
    static shop_command penalty_flag_all(bool on)                           { return shop_command{ "penalty", "flag", { { "all" }, { on ? "on" : "off" } } }; }
    static shop_command penalty_flag_reservoir_ramping(bool on)             { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "reservoir" }, { "ramping" } } }; }
    static shop_command penalty_flag_reservoir_endpoint(bool on)            { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "reservoir" }, { "endpoint" } } }; }
    static shop_command penalty_flag_load(bool on)                          { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "load" } } }; }
    static shop_command penalty_flag_powerlimit(bool on)                    { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "powerlimit" } } }; }
    static shop_command penalty_flag_discharge(bool on)                     { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "discharge" } } }; }
    static shop_command penalty_flag_plant_min_p_con(bool on)               { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "plant" }, { "min_p_con" } } }; }
    static shop_command penalty_flag_plant_max_p_con(bool on)               { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "plant" }, { "max_p_con" } } }; }
    static shop_command penalty_flag_plant_min_q_con(bool on)               { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "plant" }, { "min_q_con" } } }; }
    static shop_command penalty_flag_plant_max_q_con(bool on)               { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "plant" }, { "max_q_con" } } }; }
    static shop_command penalty_flag_plant_schedule(bool on)                { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "plant" }, { "schedule" } } }; }
    static shop_command penalty_flag_gate_min_q_con(bool on)                { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "gate" }, { "min_q_con" } } }; }
    static shop_command penalty_flag_gate_max_q_con(bool on)                { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "gate" }, { "max_q_con" } } }; }
    static shop_command penalty_flag_gate_ramping(bool on)                  { return shop_command{ "penalty", "flag", { { on ? "on" : "off" }, { "gate" }, { "ramping" } } }; }
    static shop_command penalty_cost_all(double value)                      { return shop_command{ "penalty", "cost", "all", value }; }
    static shop_command penalty_cost_reservoir_ramping(double value)        { return shop_command{ "penalty", "cost", { { "reservoir" }, { "ramping" } }, value }; }
    static shop_command penalty_cost_reservoir_endpoint(double value)       { return shop_command{ "penalty", "cost", { { "reservoir" }, { "endpoint" } }, value }; }
    static shop_command penalty_cost_load(double value)                     { return shop_command{ "penalty", "cost", "load", std::to_string(value) }; }
    static shop_command penalty_cost_powerlimit(double value)               { return shop_command{ "penalty", "cost", "powerlimit", value }; }
    static shop_command penalty_cost_discharge(double value)                { return shop_command{ "penalty", "cost", "discharge", value }; }
    //static shop_command penalty_cost_plant_min_p_con(double value)        {}
    //static shop_command penalty_cost_plant_max_p_con(double value)        {}
    //static shop_command penalty_cost_plant_min_q_con(double value)        {}
    //static shop_command penalty_cost_plant_max_q_con(double value)        {}
    //static shop_command penalty_cost_plant_schedule(double value)         {}
    //static shop_command penalty_cost_gate_min_q_con(double value)         {}
    //static shop_command penalty_cost_gate_max_q_con(double value)         {}
    static shop_command penalty_cost_gate_ramping(double value)             { return shop_command{ "penalty", "cost", std::vector<std::string>{ "gate", "ramping" }, value }; }
    static shop_command penalty_cost_overflow(double value)                 { return shop_command{ "penalty", "cost", "overflow", value }; }
    static shop_command penalty_cost_overflow_time_adjust(double value)     { return shop_command{ "penalty", "cost", "overflow_time_adjust", value }; }
    static shop_command penalty_cost_reserve(double value)                  { return shop_command{ "penalty", "cost", "reserve", value }; }
    static shop_command penalty_cost_soft_p_penalty(double value)           { return shop_command{ "penalty", "cost", "soft_p_penalty", value }; }
    static shop_command penalty_cost_soft_q_penalty(double value)           { return shop_command{ "penalty", "cost", "soft_q_penalty", value }; }
    static shop_command set_newgate(bool on)                                { return shop_command{ "set", "newgate", on ? "on" : "off" }; }
    static shop_command set_ramping(int mode)                               { return shop_command{ "set", "ramping", mode == 0 ? "off" : mode == 1 ? "on" : "request" }; }
    static shop_command set_mipgap(bool absolute, double value)             { return shop_command{ "set", "mipgap", absolute ? "absolute" : "relative", value }; }
    static shop_command set_nseg_all(double value)                          { return shop_command{ "set", "nseg", "all", value }; }
    static shop_command set_nseg_up(double value)                           { return shop_command{ "set", "nseg", "up", value }; }
    static shop_command set_nseg_down(double value)                         { return shop_command{ "set", "nseg", "down", value }; }
    static shop_command set_dyn_seg_on()                                    { return shop_command{ "set", "dyn_seg", "on" }; }
    static shop_command set_dyn_seg_incr()                                  { return shop_command{ "set", "dyn_seg", "incr" }; }
    static shop_command set_dyn_seg_mip()                                   { return shop_command{ "set", "dyn_seg", "mip" }; }
    static shop_command set_max_num_threads(int value)                      { return shop_command{ "set", "max_num_threads", value }; }
    static shop_command set_parallel_mode_auto()                            { return shop_command{ "set", "parallel_mode", "auto" }; }
    static shop_command set_parallel_mode_deterministic()                   { return shop_command{ "set", "parallel_mode", "deterministic" }; }
    static shop_command set_parallel_mode_opportunistic()                   { return shop_command{ "set", "parallel_mode", "opportunistic" }; }
    static shop_command set_capacity_all(double value)                      { return shop_command{ "set", "capacity", "all", value }; }
    static shop_command set_capacity_gate(double value)                     { return shop_command{ "set", "capacity", "gate", value }; }
    static shop_command set_capacity_bypass(double value)                   { return shop_command{ "set", "capacity", "bypass", value }; }
    static shop_command set_capacity_spill(double value)                    { return shop_command{ "set", "capacity", "spill", value }; }
    static shop_command set_headopt_feedback(double value)                  { return shop_command{ "set", "headopt_feedback", value }; }
    static shop_command set_timelimit(int value)                            { return shop_command{ "set", "timelimit", value }; }
    static shop_command set_dyn_flex_mip(int value)                         { return shop_command{ "set", "dyn_flex_mip", value }; }
    static shop_command set_universal_mip_on()                              { return shop_command{ "set", "universal_mip", "on" }; }
    static shop_command set_universal_mip_off()                             { return shop_command{ "set", "universal_mip", "off" }; }
    static shop_command set_universal_mip_not_set()                         { return shop_command{ "set", "universal_mip", "not_set" }; }
    static shop_command set_merge_on()                                      { return shop_command{ "set", "merge", "on" }; }
    static shop_command set_merge_off()                                     { return shop_command{ "set", "merge", "off" }; }
    static shop_command set_merge_stop()                                    { return shop_command{ "set", "merge", "stop" }; }
    static shop_command set_com_dec_period(int value)                       { return shop_command{ "set", "com_dec_period", value }; }
    static shop_command set_time_delay_unit_hour()                          { return shop_command{ "set", "time_delay_unit", "hour" }; }
    static shop_command set_time_delay_unit_minute()                        { return shop_command{ "set", "time_delay_unit", "minute" }; }
    static shop_command set_time_delay_unit_time_step_length()              { return shop_command{ "set", "time_delay_unit", "time_step_length" }; }
    static shop_command set_power_head_optimization(bool on)                { return shop_command{ "set", "power_head_optimization", on ? "on" : "off" }; }
    //static shop_command set_pump_head_optimization()                      {}
    static shop_command set_bypass_loss(bool on)                            { return shop_command{ "set", "bypass_loss", on ? "on" : "off" }; }
    static shop_command set_reserve_slack_cost(double value)                { return shop_command{ "set", "reserve_slack_cost", value }; }
    static shop_command set_fcr_n_band(double value)                        { return shop_command{ "set", "fcr_n_band", value }; }
    static shop_command set_fcr_d_band(double value)                        { return shop_command{ "set", "fcr_d_band", value }; }
    static shop_command set_fcr_n_equality(int value)                       { return shop_command{ "set", "fcr_n_equality", value }; }
    static shop_command set_droop_discretization_limit(double value)        { return shop_command{ "set", "droop_discretization_limit",value};}
    //static shop_command set_linear_startup()                              {}
    //static shop_command set_bid_aggregation_level()                       {}
    //static shop_command set_delay_valuation()                             {}
    static shop_command set_reserve_ramping_cost(double value)              { return shop_command{ "set", "reserve_ramping_cost", value }; }
    //static shop_command set_distribute_production()                       {}
    static shop_command set_gen_turn_off_limit(double value)                { return shop_command{ "set", "gen_turn_off_limit", value }; }
    //static shop_command set_prod_from_ref_prod()                          {}
    //static shop_command set_startup_cost_printout()                       {}
    //static shop_command set_reserve_min_capacity()                        {}
    //static shop_command set_ownership_scaling()                           {}
    //static shop_command set_stop_cost_from_start_cost()                   {}
    //static shop_command set_simple_pq_recovery()                          {}
    //static shop_command set_plant_uploading()                             {}
    //static shop_command set_sim_schedule_correction()                     {}
    //static shop_command set_power_loss()                                  {}
    //static shop_command set_safe_mode()                                   {}
    //static shop_command set_lp_info()                                     {}
    //static shop_command set_nodelog()                                     {}
    //static shop_command set_solver()                                      {}
    //static shop_command read_model()                                      {}
    //static shop_command add_model()                                       {}
    //static shop_command quit()                                            {}

    x_serialize_decl();
};
}

x_serialize_export_key(shyft::energy_market::stm::shop::shop_command);
