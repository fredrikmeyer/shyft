#include <shyft/energy_market/stm/shop/export/shop_export.h>
#include <shyft/energy_market/stm/shop/api/shop_enums.h>

#include <string>
#include <algorithm>
#include <stdexcept>
#include <unordered_map>
#include <cstring>
#include <vector>
#include <iostream>

namespace shyft::energy_market::stm::shop {

using namespace ::shop::enums;

template<typename T> using shop_info_t = std::tuple<T, const char* const>;
template<typename T, int N> using shop_info_array_t = shop_info_t<T>[N];
template<typename T, int N>
static auto name_of(T id, const shop_info_array_t<T, N>& id_and_name) {
    auto it = std::find_if(std::begin(id_and_name), std::end(id_and_name), [&id](const auto& v) { return std::get<0>(v) == id; });
    if (it != std::end(id_and_name))
        return std::get<1>(*it);
    throw std::runtime_error(std::string{ "id " } +std::to_string(static_cast<int>(id)) + " not found");
}
template<typename T, int N>
static T id_of(const char* const name, const shop_info_array_t<T, N>& id_and_name) {
    auto it = std::find_if(std::begin(id_and_name), std::end(id_and_name), [&name](const auto& v) { return strcmp(std::get<1>(v), name) == 0; });
    if (it != std::end(id_and_name))
        return std::get<0>(*it);
    throw std::runtime_error(std::string{ "name " } +name + " not found");
}

static auto number_of_objects(ShopSystem* api) { return ShopGetObjectCount(api); }
//static auto type_name(shop_object_type id) { return name_of(id, shop_object_type_info); }
static auto relation_name(shop_relation id) { return name_of(id, shop_relation_info); }
static auto relation_friendly_name(shop_relation id) {
    auto name = relation_name(id);
    if (strncmp(name, "connection_", strlen("connection_")) == 0)
        name = &name[strlen("connection_")];
    return name;
}
static auto object_name(ShopSystem* api, int oi) { return ShopGetObjectName(api, oi); }
static auto object_type_name(ShopSystem* api, int oi) { return ShopGetObjectType(api, oi); }
static auto object_type(ShopSystem* api, int oi) { return id_of(object_type_name(api, oi), shop_object_type_info); }

class object_relations
{
public:
    using object_index_t = int;
    using index_t = int;
    static constexpr object_index_t invalid_object_index{ -1 };
    static constexpr int capacity{ 100 };
    explicit object_relations(ShopSystem* api, shop_relation relation, object_index_t oi) : api{ api }, size{ 0 } {
        std::string shopstring{ relation_name(relation) };
        ShopGetRelations(api, oi, shopstring.data(), size, &data[0]);
    }
    constexpr auto begin() { return std::cbegin(data); }
    constexpr auto end() { return std::cbegin(data) + size; }
    constexpr auto empty() { return size == 0; }
    constexpr auto get(index_t i = 0) { return i >= 0 && i < size ? *(begin() + i) : invalid_object_index; }
    static auto get(ShopSystem* api, shop_relation relation, object_index_t oi, index_t i = 0) {
        object_relations relations{ api, relation, oi };
        return relations.get(i);
    }
private:
    ShopSystem* api;
    int size;
    object_index_t data[capacity];
};

static void export_objects(std::ostream& out, ShopSystem* api, bool all = false, std::vector<shop_object_type> exclude = {}) {
    for (int oi = 0, n = number_of_objects(api); oi < n; ++oi) {
        const auto type = object_type(api, oi);
        const auto name = object_name(api, oi);
        if (std::find(std::begin(exclude), std::end(exclude), type) == std::end(exclude)) {
            switch (type) {
            case shop_object_type::plant:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#ED9D53\" shape=house fillcolor=\"#EBAE75\" width=2]" << std::endl; break;
            case shop_object_type::generator:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#D6D6D6\" shape=oval fillcolor=\"#F5F5F5\" fontsize=7]" << std::endl; break;
            case shop_object_type::pump:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#D6D6D6\" shape=oval fillcolor=\"#F5F5F5\" fontsize=7]" << std::endl; break;
            case shop_object_type::reservoir:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#4A828C\" shape=invtrapezium fillcolor=\"#6ABAC9\"]" << std::endl; break;
            case shop_object_type::creek_intake:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#4A828C\" shape=invhouse fillcolor=\"#6ABAC9\"]" << std::endl; break;
            case shop_object_type::gate:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#EDEDED\" shape=box fillcolor=\"#F5F5F5\"]" << std::endl; break;
            case shop_object_type::junction:
                out << "o" << oi << " [label=\"" << name << " [junction]\" color=\"#EDEDED\" shape=octagon fillcolor=\"#F5F5F5\"]" << std::endl; break;
            case shop_object_type::junction_gate:
                out << "o" << oi << " [label=\"" << name << " [junction_gate]\" color=\"#EDEDED\" shape=octagon fillcolor=\"#F5F5F5\"]" << std::endl; break;
            case shop_object_type::tunnel:
                out << "o" << oi << " [label=\"" << name << "\" shape=diamond]" << std::endl; break;
            case shop_object_type::reserve_group:
            case shop_object_type::discharge_group:
            case shop_object_type::production_group:
                out << "o" << oi << " [label=\"" << name << "\" color=\"#D6D6D6\" shape=folder fillcolor=\"#F5F5F5\"]" << std::endl; break;
            default:
                if (all)
                    out << "o" << oi << " [label=\"" << name << " [" << object_type_name(api, oi) << "]\" color=\"#FFFFCC\" shape=note fillcolor=\"#FFFFE0\"]" << std::endl;
            }
        }
    }
}

static void add_relation_color(std::ostream& out, shop_object_type /*ot*/, shop_relation relation) {
    switch (relation) {
    case shop_relation::connection_spill:
        out << "color=\"#CD5C5C\""; break;
    case shop_relation::connection_bypass:
        out << "color=\"#4169E1\""; break;
    default:break;
    }
}
static void add_relation_formatting(std::ostream& out, shop_object_type ot, shop_relation relation) {
    if (ot == shop_object_type::reserve_group || ot == shop_object_type::discharge_group || ot == shop_object_type::production_group) {
        out << "arrowhead=none color=\"#D6D6D6\"";
    } else {
        switch (relation) {
        case shop_relation::connection_standard:
            out << "weight=3"; break;
        case shop_relation::generator_of_plant:
            out << "dir=back arrowtail=diamond color=\"#D6D6D6\""; break;
        default:
            add_relation_color(out, ot, relation);
        }
    }
}
static void export_generic_relations(std::ostream& out, ShopSystem* api, shop_object_type ot, int oi, shop_relation relation, std::vector<shop_object_type> filter = {}, bool exclude = false) {
    object_relations relations(api, relation, oi);
    if (!relations.empty()) {
        out << "o" << oi << " -> {";
        if (filter.empty()) {
            std::for_each(std::begin(relations), std::end(relations), [&out](const auto& v) { out << " o" << v; });
        } else {
            for (auto it = std::begin(relations); it != std::end(relations); ++it) {
                auto related_type = object_type(api, *it);
                auto filter_match = std::find(std::begin(filter), std::end(filter), related_type) != std::end(filter);
                if (filter_match != exclude) { // match==true && exclude==false || match==false && exclude==true
                    out << " o" << *it;
                }
            }
        }
        out << " }" << " [";
        add_relation_formatting(out, ot, relation);
        out << "]" << std::endl;
    }
}
static void export_plant_relations(std::ostream& out, ShopSystem* api, int oi, bool raw) {
    out << "subgraph cluster_o" << oi << " {" << std::endl;
    out << "style=filled" << std::endl;
    out << "color=\"#D6D6D6\"" << std::endl;
    out << "fillcolor=\"#F5F5F5\"" << std::endl;
    //const auto name = object_name(api, oi);
    //out << "label=\"" << name << "\"" << endl;
    //out << "o" << oi << endl;
    export_generic_relations(out, api, shop_object_type::plant, oi, shop_relation::generator_of_plant);
    out << "}" << std::endl;
    if (raw) {
        export_generic_relations(out, api, shop_object_type::plant, oi, shop_relation::connection_standard);
    } else {
        // Do not export relation *from* plant to pump (only the opposite)
        export_generic_relations(out, api, shop_object_type::plant, oi, shop_relation::connection_standard, { shop_object_type::pump }, true);
    }
}

static void export_reservoir_relations(std::ostream& out, ShopSystem* api, int oi, bool raw) {
    // Export relations out from reservoirs and creeks.
    if (raw) {
        for (auto relation : { shop_relation::connection_standard, shop_relation::connection_bypass, shop_relation::connection_spill })
            export_generic_relations(out, api, shop_object_type::reservoir, oi, relation);
    } else {
        // Extended handling of gates: Group gates with the reservoir they are connected to,
        // and show parallel gates (gates connected to same output) as parts of a common gate object.
        // This is optional, since it to some extent hides away the raw view from the Shop API.
        // Start by finding all connected output.
        // - Connected gates are collected to be processed later, grouped according to the relation and their downstream connection.
        // - Connections to any objects but gates are just printed as relations immediately, assuming destination object is already printed.
        // TODO: Consider refactoring, add_relation_formatting and add_relation_color has gotten new argument shop_object_type since this
        // was written, which might be used to handle gates there as well?
        std::unordered_map<shop_relation, std::unordered_map<int, std::vector<int>>> output_from_gates; // relation -> { output -> [gate,...] }
        for (auto relation : { shop_relation::connection_standard, shop_relation::connection_bypass, shop_relation::connection_spill }) {
            object_relations relations(api, relation, oi);
            for (auto it = std::begin(relations); it != std::end(relations); ++it) {
                if (object_type(api, *it) == shop_object_type::gate) {
                    object_relations gate_output_relations(api, shop_relation::connection_standard, *it);
                    if (gate_output_relations.empty()) {
                        output_from_gates[relation][object_relations::invalid_object_index].push_back(*it); // Add dummy output
                    } else {
                        for (auto it2 = std::begin(gate_output_relations); it2 != std::end(gate_output_relations); ++it2) // Looping just in case, but should be only one relation out from a gate...
                            output_from_gates[relation][*it2].push_back(*it);
                    }
                } else {
                    out << "o" << oi << " -> o" << *it << " [";
                    add_relation_formatting(out, shop_object_type::gate, relation);
                    out << "]" << std::endl;
                }
            }
        }
        // Process collected gates
        if (!output_from_gates.empty()) {
            // Export a subgraph with the reservoir/creek and all its gates
            out << "subgraph cluster_o" << oi << "_gt {" << std::endl;
            out << "style=filled" << std::endl;
            out << "color= \"#D6D6D6\"" << std::endl;
            out << "fillcolor=\"#F5F5F5\"" << std::endl;
            out << "rankdir=TB" << std::endl;
            for (auto it = std::cbegin(output_from_gates); it != std::cend(output_from_gates); ++it) {
                const auto& relation = it->first;
                const auto& output_from_gates_rel = it->second;
                auto relation_name = relation_friendly_name(relation);
                for (auto it = std::cbegin(output_from_gates_rel); it != std::cend(output_from_gates_rel); ++it) {
                    auto gate_output = it->first;
                    auto gates = it->second;
                    // Export a gate object representing all of the gates with same output
                    // Generate a unique name, containing object index of upstream reservoir/creek,
                    // downstream object if any, and relation type (e.g. if flood and bypass is connected
                    // to same downstream object, they must have unique name).
                    out << "o" << oi;
                    if (gate_output != object_relations::invalid_object_index)
                        out << "_o" << gate_output;
                    out << "_gt_" << relation_name << " [shape=record style=rounded label=\"";
                    for (auto it = std::begin(gates); it != std::end(gates); ++it)
                        out << (it!=std::begin(gates)?"|":"") << "<f" << *it << "> " << object_name(api, *it); // field name is the actual gate object index in api
                    out << "\" fontsize=7 ";
                    add_relation_color(out, shop_object_type::gate, relation);
                    out << "]" << std::endl;
                    // Export relation from reservoir/creek (upstream, the owner) to the gate object
                    out << "o" << oi << " -> o" << oi;
                    if (gate_output != object_relations::invalid_object_index)
                        out << "_o" << gate_output;
                    out << "_gt_" << relation_name << " [";
                    add_relation_formatting(out, shop_object_type::gate, relation);
                    out << "]" << std::endl;
                }
            }
            out << "}" << std::endl;
            // Export relations from gate objects to downstream objects (if any, could end in nothing - a.k.a. The Sea)
            for (auto it = std::cbegin(output_from_gates); it != std::cend(output_from_gates); ++it) {
                const auto& relation = it->first;
                const auto& output_from_gates_rel = it->second;
                auto relation_name = relation_friendly_name(relation);
                for (auto it = std::cbegin(output_from_gates_rel); it != std::cend(output_from_gates_rel); ++it) {
                    auto gate_output = it->first;
                    if (gate_output >= 0) {
                        out << "o" << oi << "_o" << gate_output << "_gt_" << relation_name << " -> o" << gate_output << " [";
                        add_relation_formatting(out, shop_object_type::gate, relation);
                        out << "]" << std::endl;
                    }
                }
            }

        }
    }
}
void shop_export::export_topology(ShopSystem* api, bool all, bool raw, std::ostream& out) {
    // Set all to true to also export objects such as system, scenario etc, which are not topological and exluded by default.
    // Set raw to true to get a raw export of what the Shop API contains. With raw set to false some additional logic is
    // applied to get a nicer result (gates are grouped with the reservoir they are connected to, parallel gates shown as one object, etc),
    // but the risk is that some needed information is hidden away.
    out << "digraph g {" << std::endl;
    out << "node [style=filled fontsize=10 fontname=\"calibri\"]" << std::endl;
    std::vector<shop_object_type> exclude_filter;
    if (!raw) exclude_filter.push_back(shop_object_type::gate); // If not raw then skip gates for now, handle them later while exporting reserovoir relations.
    export_objects(out, api, all, exclude_filter);
    for (int oi = 0, n = number_of_objects(api); oi < n; ++oi) {
        auto ot=object_type(api, oi);
        switch (ot) {
        case shop_object_type::plant: {
            export_plant_relations(out, api, oi, raw);
            break;
        }
        case shop_object_type::reservoir:
        case shop_object_type::creek_intake:
            export_reservoir_relations(out, api, oi, raw);
            break;
        case shop_object_type::gate:
            if (raw) // If not raw then skip gates as they are handled while exporting reservoir relations.
                export_generic_relations(out, api, ot, oi, shop_relation::connection_standard);
            break;
        case shop_object_type::generator:
            // Export relations only if raw mode. Generators are normally just exported as *destination* of
            // the dedicated relation type generator_of_plant from plants, and as *destination* of relations of
            // type connection_standard from group objects. We do not want to show duplicate/opposite relations.
            // If raw then export most relations, but still exclude connections to group objects because we
            // still only want to show it as a single relation and only want to export these connections from
            // the group objects.
            // Note: In Shop v14 the generator_of_plant is deprecated and connection_standard recommended instead,
            // and as of 14.3.2.0 relations of both types are created regardless which ones are used to connect!
            if (raw)
                export_generic_relations(out, api, ot, oi, shop_relation::connection_standard,
                  { shop_object_type::reserve_group, shop_object_type::discharge_group, shop_object_type::production_group }, true);
            break;
        case shop_object_type::pump:
            // In contrast to generators, we export the connection_standard relation to plant from pumps,
            // to show it as an upward connection from pump to plant, and instead exclude the opposite
            // connection downwards from plant when exporting the plant.
            // Same as for genreators, always exclude connections to group objects.
            export_generic_relations(out, api, ot, oi, shop_relation::connection_standard,
                { shop_object_type::reserve_group, shop_object_type::discharge_group, shop_object_type::production_group }, true);
            break;
        default:
            export_generic_relations(out, api, ot, oi, shop_relation::connection_standard);
        }
    }
    out << "}" << std::endl;
}

}
