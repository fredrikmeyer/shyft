#include <shyft/energy_market/stm/shop/export/shop_export.h>
#include <memory>
#include <string>
#include <vector>
#include <cmath>
#include <cstring>
#include <iostream>

namespace shyft::energy_market::stm::shop {

void shop_export::export_data(ShopSystem* api, bool all, std::ostream& out) {
    using std::endl;
    const int no = ShopGetObjectCount(api);
    const double shop_nan = 1e40; // Shop represents NaN values as 1e40 internally, and this will be returned when getting values, while it accepts both std::nan and 1e40 as input both representing NaN.
    auto out_double_or_nan = [&out, &shop_nan](const double& v) { if (!std::isfinite(v) || v==shop_nan) { out << "NaN"; } else { out << v; } }; // Write out non-standard JSON value NaN like python's json.dumps.
    int indsize = 2;
    std::string ind;
    out << "{" << endl;
    ind += std::string(indsize, ' ');
    char t_start[18], t_end[18];// YYYYMMDDhhmmssxxx0
    char t_unit[7]; // second0
    if (ShopGetTimeResolution(api, t_start, t_end, t_unit)) {
        out << ind << "\"time_start\": \"" << t_start << "\"," << endl;
        out << ind << "\"time_end\": \"" << t_end << "\"," << endl;
        out << ind << "\"time_unit\": \"" << t_unit << "\"," << endl;
    }
    if (int n;GetTimeResolutionDimensions(api, t_start, n)) {
        auto t = std::make_unique<int[]>(n);
        auto y = std::make_unique<double[]>(n);
        if (ShopGetTimeResolution(api, t_start, t_end, t_unit, n, t.get(), y.get())) {
            if (n > 0) {
                out << ind << "\"time_resolution\": {" << endl;
                ind += std::string(indsize, ' ');
                out << ind << "\"start\": \"" << t_start << "\"," << endl;
                out << ind << "\"end\": \"" << t_end << "\"," << endl;
                out << ind << "\"unit\": \"" << t_unit << "\"," << endl;
                out << ind << "\"entries\": [" << endl;
                ind += std::string(indsize, ' ');
                for (int i = 0; i < n; ++i) {
                    if (i > 0) out << "," << endl;
                    out << ind << "{ \"t\": " << t[i] << ", \"y\": "; out_double_or_nan(y[i]); out  << " }";
                }
                ind.resize(ind.size() - indsize);
                out << endl << ind << "]" << endl;
                ind.resize(ind.size() - indsize);
                out << ind << "}," << endl;
            }
        }
    }
    out << ind << "\"objects\": [" << endl;
    ind += std::string(indsize, ' ');
    for (int oix = 0; oix < no; ++oix) {
        if (oix > 0)
            out << "," << endl;
        out << ind << "{" << endl;
        ind += std::string(indsize, ' ');
        const auto otype = ShopGetObjectType(api, oix);
        const auto oname = ShopGetObjectName(api, oix);
        out << ind << "\"type\": \"" << otype << "\"," << endl;
        out << ind << "\"name\": \"" << oname << "\"," << endl;
        out << ind << "\"attributes\": [" << endl;
        ind += std::string(indsize, ' ');
        int na = 128; // Must be large enough to include all attributes for every type
        int alist[128]; // Array of size according to nAttributes
        const size_t max_type_len=64;
        char otype_str[max_type_len]; // Must be large enough for longest type name
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstringop-truncation"
#endif
        strncpy(otype_str, otype,max_type_len);
#ifdef __GNUC__
#pragma GCC diagnostic push
#endif
        ShopGetObjectTypeAttributeIndices(api, otype_str, na, alist);
        int acount = 0;
        for (int ai = 0; ai < na; ++ai) {
            int aix = alist[ai];
            char aname[64];
            char afunc[64]; char atype[64];
            char xunit[64]; char yunit[64];
            bool aparam, ain, aout;
            ShopGetAttributeInfo(api, aix, otype_str, aparam, aname, afunc, atype, xunit, yunit, ain, aout);
            if (ShopCheckAttributeToGet(api, aix)) {
                if (all || ShopAttributeExists(api, oix, aix)) { // Filter on existing? Shop seem to report incorrect value in some cases (seen on load_penalty result series on market, ShopAttributeExists returns false even if it contains values)
                    if (acount > 0)
                        out << "," << endl;
                    out << ind << "{" << endl;
                    ind += std::string(indsize, ' ');
                    out << ind << "\"type\": \"" << atype << "\"," << endl;
                    out << ind << "\"name\": \"" << aname << "\"," << endl;
                    out << ind << "\"data_function\": \"" << afunc << "\"," << endl;
                    out << ind << "\"unit_x\": \"" << xunit << "\"," << endl;
                    out << ind << "\"unit_y\": \"" << yunit << "\"," << endl;
                    out << ind << "\"is_object_parameter\": " << (aparam ? "true" : "false") << "," << endl;
                    out << ind << "\"is_input\": " << (ain ? "true" : "false") << "," << endl;
                    out << ind << "\"is_output\": " << (aout ? "true" : "false") << "," << endl;
                    if (all) out << ind << "\"exists\": " << (ShopAttributeExists(api, oix, aix) ? "true" : "false") << "," << endl; // Only relevant when not filter on existing
                    out << ind << "\"value\": ";
                    if (strcmp(atype, "double") == 0) {
                        if (double v; ShopGetDoubleAttribute(api, oix, aix, v)) {
                            out_double_or_nan(v);
                        } else {
                            out << "null";
                        }
                    } else if (strcmp(atype, "double_array") == 0) {
                        if (int n; ShopGetDoubleArrayLength(api, oix, aix, n) && n > 0) {
                            if (auto v = std::make_unique<double[]>(n); ShopGetDoubleArrayAttribute(api, oix, aix, n, v.get())) {
                                out << "[ " << v[0];
                                for (int i=1;i<n;++i) {
                                    out << ", ";
                                    out_double_or_nan(v[i]);
                                }
                                out << " ]";
                            } else {
                                out << "[]";
                            }
                        } else {
                            out << "[]";
                        }
                    } else if (strcmp(atype, "int") == 0) {
                        if (int v; ShopGetIntAttribute(api, oix, aix, v)) {
                            out << v;
                        } else {
                            out << "null";
                        }
                    } else if (strcmp(atype, "int_array") == 0) {
                        if (int n; ShopGetIntArrayLength(api, oix, aix, n) && n > 0) {
                            if (auto v = std::make_unique<int[]>(n); ShopGetIntArrayAttribute(api, oix, aix, n, v.get())) {
                                out << "[ " << v[0];
                                for (int i=1;i<n;++i)
                                    out << ", " << v[i];
                                out << " ]";
                            } else {
                                out << "[]";
                            }
                        } else {
                            out << "[]";
                        }
                    } else if (strcmp(atype, "string") == 0) {
                        if (char v[128]; ShopGetStringAttribute(api, oix, aix, v)) {
                            out << "\"" << v << "\"";
                        } else {
                            out << "null";
                        }
                    } else if (strcmp(atype, "txy") == 0) {
                        char start_time[18];
                        if (int n_scenarios, n; ShopGetTxyAttributeDimensions(api, oix, aix, start_time, n, n_scenarios) && n_scenarios && n) {
                            auto t = std::make_unique<int[]>(n);
                            auto vs = std::make_unique<double*[]>(n_scenarios);
                            std::vector<std::unique_ptr<double[]>> vsv(n_scenarios);
                            for (int i = 0; i < n_scenarios; ++i) {
                                vsv[i] = std::make_unique<double[]>(n);
                                vs[i] = vsv[i].get();
                            }
                            if (ShopGetTxyAttribute(api, oix, aix, n, n_scenarios, t.get(), vs.get())) {
                                out << "{" << endl;
                                ind += std::string(indsize, ' ');
                                out << ind << "\"start\": \"" << start_time << "\"," << endl;
                                out << ind << "\"scenarios\": [" << endl;
                                ind += std::string(indsize, ' ');
                                for (int i = 0; i < n_scenarios; ++i) {
                                    if (i > 0) out << "," << endl;
                                    out << ind << "{" << endl;
                                    ind += std::string(indsize, ' ');
                                    out << ind << "\"scenario\": " << i << ", " << endl;
                                    out << ind << "\"entries\": [" << endl;
                                    ind += std::string(indsize, ' ');
                                    for (int j = 0; j < n; ++j) {
                                        if (j > 0) out << "," << endl;
                                        out << ind << "{ \"t\": " << t[j] << ", \"v\": "; out_double_or_nan(vs[i][j]); out << " }";
                                    }
                                    ind.resize(ind.size() - indsize);
                                    out << endl << ind << "]" << endl;
                                    ind.resize(ind.size() - indsize);
                                    out << ind << "}" << endl;
                                }
                                ind.resize(ind.size() - indsize);
                                out << ind << "]" << endl;
                                ind.resize(ind.size() - indsize);
                                out << ind << "}";
                            } else {
                                out << "[]";
                            }
                        } else {
                            out << "[]";
                        }
                    } else if (strcmp(atype, "xy") == 0) {
                        if (int n; ShopGetXyAttributeNPoints(api, oix, aix, n) && n > 0) {
                            auto x = std::make_unique<double[]>(n);
                            auto y = std::make_unique<double[]>(n);
                            double r;
                            if (ShopGetXyAttribute(api, oix, aix, r, n, x.get(), y.get())) {
                                out << "{" << endl;
                                ind += std::string(indsize, ' ');
                                out << ind << "\"reference\": " << r << "," << endl;
                                out << ind << "\"entries\": [" << endl;
                                ind += std::string(indsize, ' ');
                                for (int i = 0; i < n; ++i) {
                                    if (i > 0) out << "," << endl;
                                    out << ind << "{ \"x\": " << x[i] << ", \"y\": "; out_double_or_nan(y[i]); out << " }";
                                }
                                ind.resize(ind.size() - indsize);
                                out << endl << ind << "]" << endl;
                                ind.resize(ind.size() - indsize);
                                out << ind << "}";
                            } else {
                                out << "null";
                            }
                        } else {
                            out << "null";
                        }
                    } else if (strcmp(atype, "xy_array") == 0) {
                        if (int n, np; ShopGetXyArrayDimensions(api, oix, aix, n, np) && n > 0 && np > 0) {
                            auto r = std::make_unique<double[]>(n);
                            auto p = std::make_unique<int[]>(n);
                            auto x = std::make_unique<double*[]>(n);
                            auto y = std::make_unique<double*[]>(n);
                            std::vector<std::unique_ptr<double[]>> xv(n);
                            std::vector<std::unique_ptr<double[]>> yv(n);
                            for (int i = 0; i < n; ++i) {
                                xv[i] = std::make_unique<double[]>(np);
                                yv[i] = std::make_unique<double[]>(np);
                                x[i] = xv[i].get();
                                y[i] = yv[i].get();
                            }
                            if (ShopGetXyArrayAttribute(api, oix, aix, n, r.get(), p.get(), x.get(), y.get())) {
                                out << "[" << endl;
                                ind += std::string(indsize, ' ');
                                for (int i = 0; i < n; ++i) {
                                    if (i > 0) out << "," << endl;
                                    out << ind << "{" << endl;
                                    ind += std::string(indsize, ' ');
                                    out << ind << "\"reference\": " << r[i] << "," << endl;
                                    out << ind << "\"entries\": [" << endl;
                                    ind += std::string(indsize, ' ');
                                    for (int j = 0, n = p[i]; j < n; ++j) {
                                        if (j > 0) out << "," << endl;
                                        out << ind << "{ \"x\": " << x[i][j] << ", \"y\": ";  out_double_or_nan(y[i][j]); out << " }";
                                    }
                                    ind.resize(ind.size() - indsize);
                                    out << endl << ind << "]" << endl;
                                    ind.resize(ind.size() - indsize);
                                    out << ind << "}";
                                }
                                ind.resize(ind.size() - indsize);
                                out << ind << "]";
                            } else {
                                out << "[]";
                            }
                        } else {
                            out << "[]";
                        }
                    } else if (strcmp(atype, "xyt") == 0) {
                        if (int nt; ShopGetXytNTimes(api, oix, aix, nt) && nt > 0) {
                            if (auto ta = std::make_unique<int[]>(nt); ShopGetXytTimesIntArray(api, oix, aix, nt, ta.get())) {
                                out << "[" << endl;
                                ind += std::string(indsize, ' ');
                                for (int i=0;i<nt;++i) {
                                    out << ind << "{" << endl;
                                    ind += std::string(indsize, ' ');
                                    auto t = ta[i];
                                    out << ind << "\"t\": " << t << "," << endl;
                                    out << ind << "\"entries\": ";
                                    if (int np; ShopGetXytAttributeNPoints(api, oix, aix, np, t) && np > 0) {
                                        out << "[" << endl;
                                        ind += std::string(indsize, ' ');
                                        double r;
                                        auto x = std::make_unique<double[]>(np);
                                        auto y = std::make_unique<double[]>(np);
                                        if (ShopGetXytAttribute(api, oix, aix, r, np, x.get(), y.get(), t)) {
                                            out << ind << "{" << endl;
                                            ind += std::string(indsize, ' ');
                                            out << ind << "\"reference\": " << r << "," << endl;
                                            out << ind << "\"entries\": [" << endl;
                                            ind += std::string(indsize, ' ');
                                            for (int j = 0; j < np; ++j) {
                                                if (j > 0) out << "," << endl;
                                                out << ind << "{ \"x\": " << x[j] << ", \"y\": "; out_double_or_nan(y[j]); out << " }";
                                            }
                                            ind.resize(ind.size() - indsize);
                                            out << endl << ind << "]";
                                            ind.resize(ind.size() - indsize);
                                            out << ind << "}";
                                        }
                                        ind.resize(ind.size() - indsize);
                                        out << ind << "]" << endl;
                                    } else {
                                        out << "[]" << endl;
                                    }
                                    ind.resize(ind.size() - indsize);
                                    out << ind << "}" << endl;
                                }
                                ind.resize(ind.size() - indsize);
                                out << ind << "]";
                            } else {
                                out << "[]";
                            }
                        } else {
                            out << "[]";
                        }
                    } else {
                        out << "null";
                    }
                    out << endl;
                    ind.resize(ind.size() - indsize);
                    out << ind << "}";
                    ++acount;
                }
            }
        }
        ind.resize(ind.size() - indsize);
        out << endl << ind << "]" << endl;
        ind.resize(ind.size() - indsize);
        out << ind << "}";
    }
    ind.resize(ind.size() - indsize);
    out << endl << ind << "]" << endl;
    ind.resize(ind.size() - indsize);
    out << ind << "}" << endl;
}

}
