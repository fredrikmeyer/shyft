/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
/*
 * Adapting stm types to shop api.
 */
#pragma once
#include <vector>
#include <memory>
#include <string>
#include <utility>
#include <algorithm>
#include <stdexcept>
#include <cmath>

#include <shyft/energy_market/stm/shop/shop_types.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/optimization_summary.h>

namespace shyft::energy_market::stm::shop {

using std::string;
using std::to_string;
using std::isfinite;
using namespace std::string_literals;
using shyft::core::utctime;
using shyft::core::utcperiod;
using shyft::time_series::dd::apoint_ts;
using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using hydro_power::xy_point_curve;
using hydro_power::xy_point_curve_with_z;
using hydro_power::connection_role;

/** @brief adapter for exchanging object and their attribute between stm and shop
 *  @details
 *  This class acts as a helper for the shop-emitter, 
 *  especially in the emitter-phase: .to_shop(object..) and then in the collector phase .from_shop methods.
 *  Things that this class resolves are like:
 *   # the stm have temporal variables for effiency curves, so we map the version available at the start of the time_axis
 *   # some attributes in the stm are optional, and is mapped optionally
 *   # some results after stepping the opt-engine is optional, thus we collect them optionally.
 * 
 */
struct shop_adapter {
    shop_api& api;///< upward not owning reference to the shop_api (as provided by the emitter.
    shyft::time_axis::generic_dt time_axis;///< the shop run time-axis
    utctime time_delay_unit;///< the shop time resolution for time delay values
    mutable int ema_prod_area_id{1};///< seems that the only solution that works stable is to give markets strictly increasing prod_areas, starting from 1(!)
    mutable map<int,vector<shop_market>> ema_map;

    shop_adapter(shop_api& api, shyft::time_axis::generic_dt const& ta) : api{api},time_axis{ta} {
        if (!time_axis.total_period().valid())
            throw std::runtime_error("Creating shop adapter needs a valid time-axis");
        // Configure time delay handling to use finest possible fixed resolution,
        // currently minutes, instead of the default which is according to timestep length!
        time_delay_unit = shyft::core::utctime_from_seconds64(60);
    }

    /** utility to create a one-step constant ts for the total period of the adapter.time_axis */
    apoint_ts mk_constant_ts(double value) const {
        return  apoint_ts{
            shyft::time_axis::point_dt{ {time_axis.total_period().start,time_axis.total_period().end} },
            value,
            shyft::time_series::POINT_AVERAGE_VALUE
        };
    }

    /** returns the time_axis.total_period() */
    utcperiod period() const { return time_axis.total_period();}

    using ts_t = apoint_ts; // Time series, which may be "series-like" time-varying values, or it may be temporal scalar value.
    using ts_value_t = double;

    template<typename V> 
    using tv_t = t_T<V>; // Temporal stm attributes represented by map:utctime->v (t_double_ (legacy), t_xy_, t_xyz_, t_xyz_list_ and t_turbine_description_)

    template<typename O, int a, typename V, typename Ux, typename Uy>
    using shop_r_t = ::shop::ro<O, a, V, Ux, Uy>;
    template<typename O, int a, typename V, typename Ux, typename Uy>
    using shop_w_t = ::shop::rw<O, a, V, Ux, Uy>;

    // Checking exists, only relevant for stm attributes (but anything can be checked)
    template<typename V> static bool exists(const V& ) { return true; }
    static bool exists(const ts_t& v) { return v.ts ? true : false; } // Check that time series is not empty
    template<typename V> static bool exists(const tv_t<V>& tv) { return tv && !tv->empty(); } // Check that temporal attribute is not empty (shared_ptr non-null and contained map is not empty)

    // Checking valid specifically for non-temporal values (just checking exists so not realy a validity check)
    template<typename V> static bool valid(const V& v) { return exists(v); }
    // Checking valid specifically for temporal values, with explicit valid time
    static bool valid_temporal(const ts_t& ts, utctime t) { return exists(ts) && ts.total_period().contains(t); } // Should only be called for time series representing temporal attribute value
    template<typename V> static bool valid_temporal(const tv_t<V>& tv, utctime t) { return exists(tv) && tv->begin()->first <= t; }
    // Checking valid specifically for temporal values, with implicit valid time according to current period
    template<typename V> bool valid_temporal(const V& v) const { return valid_temporal(v, time_axis.time(0)); }
    // Checking valid for implicitely deduced temporal values, with implicit valid time according to current period
    // Note that this only works for temporal types t_T (map:utctime->v), time series must be explicitely checked
    // as temporal since it could be a non-temporal regular time series value
    template<typename V> bool valid(const tv_t<V>& tv) const { return valid_temporal(tv); }

    // Check for any values, not considering nan or infinite.
    // Note: Currently only implemented for stm attributes, and only used as a common utility from emitter etc.
    static bool has_values(const ts_t& v) {
        // False if time series object is empty or describes no points.
        if (v.size() == 0)
            return false;
        // True if time series has any non-nan values.
        const auto values = v.values(); // Note: Throws exception if unbound, check needs_bind() first to avoid it.
        return std::any_of(std::cbegin(values), std::cend(values), [](double const& v) { return std::isfinite(v); });
    };

    /* Section with 'getter methods' both temporal and non-temporal that helps getting the 'value' type out from stm.
     * some of them require mapping from time-variant structures to simple structure as indicated as this class
     * functional requirements.
     */
    template<typename V> static const V& get(const V& v) { return v; } // Basic value
    template<typename O, int a, typename V, typename Ux, typename Uy> V get(shop_r_t<O, a, V, Ux, Uy>& v) { return (V)v; } // Shop attribute value
    template<typename V, typename... Args> V get(const tv_t<V>& v, Args... args) const { return get_temporal(v, time_axis.time(0), args...); } // Get value of implicit temporal types t_T (map:utctime->v), using start of current period as implicit valid time.
    template<typename V, typename... Args> auto get_temporal(const V& v, Args... args) const { return get_temporal(v, time_axis.time(0), args...); } // Get value of explicit temporal type, using start of current period as implicit valid time. This is the only way to get value of temporal time series, since it cannot implicitely known if it is a regular time series attribute or a temporal attribute stored as a time series.

    // Static helpers for looking up value of temporal attributes valid at specified time
    template<typename V, typename... Args> static auto get(const V& v, Args... args) { // Temporal value (implicit when additional arguments
        return get_temporal(v, args...);
    }
    template<typename V, typename... Args> static V get(const tv_t<V>& tv, utctime t, Args... args) { // Specific overload to avoid utctime argument being handled as part of args... after period.start in member version!
        return get_temporal(tv, t, args...);
    }
    static ts_value_t get_temporal(const ts_t& ts, utctime t) { // Get value valid at t from time series (throws if not valid)
        if (!ts)
            throw std::runtime_error("Temporal attribute is empty, hence not valid at time "s + to_string(t.count()));
        if (!ts.total_period().contains(t))
            throw std::runtime_error("Temporal attribute is not valid at time "s + to_string(t.count()));
        return ts(t);
    }
    static ts_value_t get_temporal(const ts_t& ts, utctime t, const ts_value_t& invalid_v) { // Get value valid at t, or specified value if none
        if (!ts)
            return invalid_v;
        if (!ts.total_period().contains(t)) // Implicitely handles the case of !ts (total_period then returns an empty period)
            return invalid_v;
        return ts(t);
    }
    static ts_value_t get_temporal(const ts_t& ts, utctime t, const ts_value_t& invalid_v, const ts_value_t& nan_v) { // Get value valid at t, or specified value if none, or specifed value if nan
        if (!ts)
            return invalid_v;
        if (!ts.total_period().contains(t))
            return invalid_v;
        ts_value_t v = ts(t);
        return isfinite(v) ? v : nan_v;
    }
    template<typename V> static V get_temporal(const tv_t<V>& tv, utctime t) { // Get value valid at t (throws if not valid)
        if (!tv)
            throw std::runtime_error("Temporal attribute is not set, hence not valid at time "s + to_string(t.count())); // Never valid
        auto it = tv->lower_bound(t);
        if (it == tv->cend()) {
            if (tv->empty())
                throw std::runtime_error("Temporal attribute is empty, hence not valid at time "s + to_string(t.count())); // Never valid
            --it;
        } else if (it->first > t) {
            if (it == tv->cbegin())
                throw std::runtime_error("Temporal attribute is not valid at time "s + to_string(t.count())); // Only valid after t
            --it;
        }
        return it->second;
    }
    template<typename V> static V get_temporal(const tv_t<V>& tv, utctime t, const V& invalid_v) { // Get value valid at t, or specified value if none
        if (!tv)
            return invalid_v; // Return default value since attribute is not set
        auto it = tv->lower_bound(t);
        if (it == tv->cend()) {
            if (tv->empty())
                return invalid_v; // Return default value since attribute is empty (no validity segments)
            --it;
        } else if (it->first > t) {
            if (it == tv->cbegin())
                return invalid_v; // Return default value since attribute only has values valid after t
            --it;
        }
        return it->second;
    }

    /* Section for 'setters', that validates the stm atribute to set, before extracting the value and injecting it to shop

     Check if specified source is valid considering the destination, which will implicit deduce temporal values and consider according to current period
     Note that it does not tell if the source can actually be assigned to the destination, it just checks the validity of the source!
     */
    template<typename D, typename S> bool valid_to_set(const D& /*d*/, const S& s) const { return valid(s); }
    // Temporal types time series, assuming the time series represents a temporal attribute value and not "time series data" since destination is scalar double, using start of current period as implicite valid time
    bool valid_to_set(const ts_value_t& , const ts_t& s) const { return valid_temporal(s, time_axis.time(0)); }
    template<typename DO, int da, typename Ux, typename Uy> bool valid_to_set(const shop_w_t<DO, da, ts_value_t, Ux, Uy>& /*d*/, const ts_t& s) const { return valid_temporal(s, time_axis.time(0)); }

    // Basic assignment/copy
    template<typename D, typename S> D& set(D& d, const S& s) const { d = s; return d; }
    // Set shop attributes from stm attributes where some additional conversion is needed
    template<typename DO, int da, typename V, typename Ux, typename Uy> shop_w_t<DO, da, V, Ux, Uy>& set(shop_w_t<DO, da, V, Ux, Uy>& d, const std::shared_ptr<V>& s) const { d = *s; return d; } // Dereference shared pointer before assigning to Shop attribute
    template<typename DO, int da, typename Ux, typename Uy> shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>& set(shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>& d, const xy_point_curve& s) const { d = xy_point_curve_with_z{ s, 0.0 }; return d; } // Convert plain xy into xyz (with z=0) before assigning to Shop attribute
    template<typename DO, int da, typename Ux, typename Uy> shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>& set(shop_w_t<DO, da, xy_point_curve_with_z, Ux, Uy>& d, const xy_point_curve_& s) const { set(d, *s); return d; } // Dereference shared pointer xy to xyz mapping above
    // Set temporal types t_T (map:utctime->v) using start of current period as implicite valid time
    template<typename D, typename V, typename... Args> D& set(D& d, const tv_t<V>& s, Args... args) const { return set(d, get(s, time_axis.time(0), args...)); }
    // Set temporal types time series, assuming the time series represents a temporal attribute value and not "time series data" since destination is scalar double, using start of current period as implicite valid time
    template<typename... Args> ts_value_t& set(ts_value_t& d, const ts_t& s, Args... args) const { return set(d, get(s, time_axis.time(0), args...)); }
    template<typename DO, int da, typename Ux, typename Uy, typename... Args> shop_w_t<DO, da, ts_value_t, Ux, Uy>& set(shop_w_t<DO, da, ts_value_t, Ux, Uy>& d, const ts_t& s, Args... args) const { return set(d, get(s, time_axis.time(0), args...)); }
    // Set stm attributes from shop attributes where som additional conversion is needed
    template<typename DO, int da, typename V, typename Ux, typename Uy, typename D> D& set(D& d, const shop_w_t<DO, da, V, Ux, Uy>& s) const {
        // Helper needed for template specialization/overload resolution to work correct,
        // simply upcasting shop_w_t to shop_r_t, which enables more specialized variants
        // to consistently use shop_r_t as source type while also handling shop_w_t.
        return set(d, shop_r_t<DO, da, V, Ux, Uy>(s));
    }
    template<typename DO, int da, typename V, typename Ux, typename Uy> std::shared_ptr<V>& set(std::shared_ptr<V>& d, const shop_r_t<DO, da, V, Ux, Uy>& s) const {
        // Assigning value into shared_ptr
        if (!d)
            d = std::make_shared<V>();
        return *d = s;
    }
    template<typename DO, int da, typename V, typename Ux, typename Uy> tv_t<V>& set(tv_t<V>& d, const shop_r_t<DO, da, V, Ux, Uy>& s) const {
        // Convert non-temporal value into temporal before assigning to stm attribute
        if (d)
            d->clear();
        else
            d = std::make_shared<tv_t<V>::element_type>();
        d->emplace(time_axis.time(0), s);
        return d;
    }
    template<typename DO, int da, typename V, typename Ux, typename Uy> t_xy_& set(t_xy_& d, const shop_r_t<DO, da, V, Ux, Uy>& s) const {
        // Convert xyz into plain xy before assigning to stm attribute
        if (d)
            d->clear();
        else
            d = std::make_shared<t_xy_::element_type>();
        xy_point_curve_with_z xyz = s; // maybe set(xyz, s)?
        auto xy = std::make_shared<xy_point_curve>();
        *xy = xyz.xy_curve;
        d->emplace(time_axis.time(0), std::move(xy));
        return d;
    }
    template<typename DO, int da, typename Ux, typename Uy> t_xy_& set(t_xy_& d, const shop_r_t<DO, da, map<time_t, xy_point_curve_with_z>, Ux, Uy>& s) const {
        // Convert xyt with map key type time_t into t_xy_ with map key type utctime before assigning to stm attribute
        if (d)
            d->clear();
        else
            d = std::make_shared<t_xy_::element_type>();
        map<time_t, xy_point_curve_with_z> xyt = s; // maybe set(xyt, s)?
        for (auto it = std::cbegin(xyt); it != std::cend(xyt); ++it) {
            auto xy = std::make_shared<xy_point_curve>();
            *xy = it->second.xy_curve;
            d->emplace(shyft::core::utctime_from_seconds64(it->first), std::move(xy));
        }
        return d;
    }
    // Conditional setters, checking if source exists
    // Note if source is temporal it will still throw if source is not valid at specified time, unless an additional default value is also specified (which will be passed into parameter invalid_v of at())
    template<typename D, typename S, typename... Args> D& set_if(D& d, const S& s, Args... args) const // Set value from attribute if the attribute exists, else do nothing.
        { return exists(s) ? set(d, s, args...) : d; }
    template<typename D, typename S, typename V, typename... Args> D& set_or(D& d, const S& s, const V& v_not_exists, Args... args) const // Set value from attribute if the attribute exists, or set specified default value.
        { if (exists(s)) { return set(d, s, args...); } else { return set(d, v_not_exists); } }
    // Higher level setters for pure optional values
    // May or may not set a value. Similar to conditional setters (set_if/set_or) but does not throw (does nothing) if source does not exist or if temporal value not valid at specified time.
    template<typename D, typename S, typename... Args> D& set_optional(D& d, const S& s, Args... args) const // Set pure optional: May or may not set a value. Like set_if does nothing (skip) if source not exists, but in addition also does nothing (does not throw) if source exists but is not valid at specified time and no default value is specified (can still specify additional default value which will be passed into parameter invalid_v of at())
        { try { return set_if(d, s, args...); } catch (...) { return d; } }
    template<typename D, typename S, typename V, typename... Args> D& set_optional(D& d, const S& s, const V& v_not_exists, Args... args) const // Set pure optional: May or may not set a value. Like set_or sets default value if source not exists, but does nothing (does not throw) if source exists but is not valid at specified time and no default value is specified (can still specify additional default value which will be passed into parameter invalid_v of at())
        { try { return set_or(d, s, v_not_exists, args...); } catch (...) { return d; } }
    // Higher level setters for pure required values
    // Will never silently skip setting of results, will either ensure a value is set or throw exception.
    template<typename D, typename S, typename... Args> D& set_required(D& d, const S& s, Args... args) const // Set pure required: Will set value or throw. Is just an alias for set.
        { return set(d, s, args...); }
    template<typename D, typename S, typename V, typename... Args> D& set_required(D& d, const S& s, const V& v_default, Args... args) const // Set pure required: Will always set value and never throw. Like set_optional sets default value if source not exists, but also sets the same value if source exists but is not valid at specified time and no additional default value is specified (can still specify additional default value which will be passed into parameter invalid_v of at())
        { try { return set_or(d, s, v_default, args...); } catch (...) { return set(d, v_default); } }

    /** Section for additional utils */

    template<class T_shop> void set_shop_time_delay(T_shop& o_shop, const t_xy_& delay) const {
        // Convert stm delay xy into shop attributes: time_delay (number) and/or shape_discharge (xy).
        // Shop requires first X value in shape_discharge to be 0.0.
        // In stm we allow it to be >0 in which case we move it to the scalar
        // time_delay attribute instead, and skew all other values correspondingly.
        if (!o_shop.time_delay.is_default() || o_shop.shape_discharge.exists())
            return; // Time delay already set!
        if (valid(delay)) {
            if (const auto xy = get(delay)) { // Return std::shared_ptr<xy_point_curve>
                auto delay_scale = shyft::core::to_seconds64(time_delay_unit);
                if (delay_scale == 0) delay_scale = 1;
                if(xy->points.size() == 2 && xy->points.front().y == 0.0) {
                    // The xy has zero value as first x and y, and exactly one more point,
                    // which means it is valid as shape_discharge but is in reality simply
                    // a constant delay. We may just as well set the second x value into
                    // time_delay (number), instead of shape_discharge (xy).
                    auto delay = xy->points[1].x / delay_scale;
                    set_optional(o_shop.time_delay, delay);
                } else if (xy->points.size() > 0) {
                    // Either xy has non-zero first x:
                    //   - That means it is not valid as shape_discharge, but we move that
                    //     initial value, representing an offset, into time_delay, and
                    //     set the rest of the xy points (if any), representing the wave,
                    //     as shape_discharge, but with x-values reduced according to the offset.
                    // Or, xy has zero first x:
                    //   - If there is only this one point, then there is no time delay to set.
                    //   - Else, the xy is assumed to be a valid shape_discharge, e.g.
                    //     difference between any two succeeding x values are the same as the
                    //     difference between the first two.
                    auto delay_offset = xy->points.front().x / delay_scale;
                    if (delay_offset > 0) {
                        set_optional(o_shop.time_delay, delay_offset);
                    }
                    if (xy->points.size() > 1) {
                        auto xy_copy = *xy;
                        auto it = xy_copy.points.begin();
                        it->x = 0.0;
                        for (++it; it != xy_copy.points.end(); ++it) {
                            it->x = it->x / delay_scale - delay_offset;
                        }
                        set_optional(o_shop.shape_discharge, xy_copy);
                    }
                }
            }
        }
    }

    /** Section for .to_shop(some-stm-type-object)  */

    shop_market to_shop(const energy_market_area& a) const;
    shop_reserve_group to_shop(const unit_group& a, const apoint_ts& mask_ts, const string& name) const;
    xy_point_curve_ get_spill_description(const reservoir& a) const;
    shop_reservoir to_shop(const reservoir& a) const;
    shop_unit to_shop(const unit& a) const;
    shop_power_plant to_shop(const power_plant& a) const;
    shop_tunnel to_shop(const waterway& wtr) const;
    shop_discharge_group to_shop_discharge_group(const waterway &wtr);
    shop_gate to_shop_gate(const waterway& wtr, gate const* gt) const;

    void from_shop(energy_market_area& mkt, const shop_market& shop_mkt);
    void from_shop(reservoir& rsv, const shop_reservoir& shop_rsv);
    void from_shop(unit& agg, const shop_unit& shop_agg);
    void from_shop(waterway& wtr, const shop_gate& shop_gt);
    void from_shop(waterway &wtr, const shop_discharge_group &shop_dg);
    void from_shop(gate& gt, const shop_gate& shop_gt);
    void from_shop(waterway& wtr, const shop_tunnel& shop_tn);
    void from_shop(unit_group& ug, const shop_reserve_group& shop_ug);
    void from_shop(power_plant& pp, const shop_power_plant& shop_ps);
    void from_shop(optimization_summary& osm, const shop_objective& shop_obj) const;

};

}
