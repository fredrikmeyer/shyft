/*
 * Handling of log callbacks from shop api.
 *
 * The current sintef SHOP legacy logging interface lacks
 * support for proper multithreaded operations, both when it
 * comes to the technical interface (global external hooks),
 * and the interface of those hooks(lacking the context from which
 * the log messages belongs to (which optimization, which system?)
 * Work by Sintef is ongoing to overcome this.
 */
#include <shyft/energy_market/stm/shop/shop_log_hook.h>

namespace shyft::energy_market::stm::shop {

    // Define static function objects that can be hooked onto programatically
    // to receive callbacks from the core shop api.

    std::function<void(const char*)> shop_log_hook::info;
    std::function<void(const char*)> shop_log_hook::warning;
    std::function<void(const char*)> shop_log_hook::error;
    std::function<void()> shop_log_hook::exit;

}

// Define external log functions called from Shop.lib (global callbacks).

void SHOP_log_info(char* msg) {
    if (shyft::energy_market::stm::shop::shop_log_hook::info)
        shyft::energy_market::stm::shop::shop_log_hook::info(msg);
}
void SHOP_log_warning(char* msg) {
    if (shyft::energy_market::stm::shop::shop_log_hook::warning)
        shyft::energy_market::stm::shop::shop_log_hook::warning(msg);
}
void SHOP_log_error(char* msg) {
    if (shyft::energy_market::stm::shop::shop_log_hook::error)
        shyft::energy_market::stm::shop::shop_log_hook::error(msg);
}
void SHOP_exit(void*) { // NOTE: It seems the Shop API never calls this
    if (shyft::energy_market::stm::shop::shop_log_hook::exit)
        shyft::energy_market::stm::shop::shop_log_hook::exit();
}