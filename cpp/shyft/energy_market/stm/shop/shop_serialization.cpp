/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
//
// 1. first include std stuff and the headers for
// files with serializeation support
//

#include <shyft/core/core_serialization.h>
#include <shyft/core/core_archive.h>

#include <shyft/energy_market/stm/shop/shop_command.h>
#include <shyft/energy_market/stm/shop/shop_log_entry.h>

// then include stuff you need like vector,shared, base_obj,nvp etc.
#include <boost/format.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/nvp.hpp>

//
// 2. Then implement each class serialization support
//

using namespace boost::serialization;

template <class Archive>
void shyft::energy_market::stm::shop::shop_command::serialize(Archive & ar, const unsigned int /*file_version*/) {
    ar
    & make_nvp("keyword", keyword)
    & make_nvp("specifier", specifier)
    & make_nvp("options", options)
    & make_nvp("objects", objects)
    ;
}

template <class Archive>
void shyft::energy_market::stm::shop::shop_log_entry::serialize(Archive & ar, const unsigned int /*version*/) {
    ar
    & make_nvp("time", time)
    & make_nvp("message", message)
    & make_nvp("code", code)
    & make_nvp("severity", severity)
    ;
}

//
// 3. Then export class serialization support
//
//x_serialize_implement(shyft::energy_market::core::utcperiod);
x_serialize_implement(shyft::energy_market::stm::shop::shop_command);
x_serialize_implement(shyft::energy_market::stm::shop::shop_log_entry);

//
// 4. Then include the archive supported
//
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

// repeat template instance for each archive class
#define xx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)
xx_arch(shyft::energy_market::stm::shop::shop_command);
xx_arch(shyft::energy_market::stm::shop::shop_log_entry);
