#pragma once
#include <cctype>
#include <string>
#include <algorithm>
#include <shyft/core/core_serialization.h>
#if SHYFT_WITH_SHOP
#include <shyft/energy_market/stm/shop/api/shop_proxy.h>
#else
#include <shyft/energy_market/stm/shop/api/shop_log_severity.h>
#endif
#include <shyft/time/utctime_utilities.h>

namespace shyft::energy_market::stm::shop {

struct shop_log_entry {
    using log_severity = ::shop::data::log_severity;
    log_severity severity;
    std::string message;
    int code{0};
    shyft::core::utctime time{shyft::core::no_utctime};

    shop_log_entry() = default; // Used by serialization
    shop_log_entry(log_severity severity, std::string message) // Used by shop_logger implementations, where code and time is not part of the interface
        : severity{severity}, message{std::move(message)} {}
    shop_log_entry(log_severity severity, std::string message, int code, shyft::core::utctime time)
        : severity{severity}, message{std::move(message)}, code{code}, time{time} {}
#ifdef SHYFT_WITH_SHOP
    explicit shop_log_entry(const ::shop::data::shop_log_entry& entry) // Copy construct from core object
        : severity{entry.severity}, code{entry.code}, time{shyft::core::from_seconds(entry.time)} {
        std::string_view msg{ entry.message };
        message = std::string{
            std::find_if(msg.cbegin(), msg.cend(), [](int ch) { return !std::isspace(ch); }), // Some shop messages include line endings at beginning which is not wanted here (e.g. 3006: "End overview over input data:")
            std::find_if(msg.crbegin(), msg.crend(), [](int ch) { return !std::isspace(ch); }).base() }; // Many (all?) shop messages include line endings at the end which is not wanted here
    }
#endif
    bool operator==(const shop_log_entry& other) const {
        return time == other.time
            && severity == other.severity
            && message == other.message
            && code == other.code;
    }
    bool operator!=(const shop_log_entry& other) const {
        return !operator==(other);
    }

    x_serialize_decl();
};

}
