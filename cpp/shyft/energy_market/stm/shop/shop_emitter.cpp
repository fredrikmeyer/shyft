#include <cstdint>
#include <cmath>
#include <string>
#include <numeric>
#include <unordered_set>
#include <shyft/energy_market/stm/shop/shop_emitter.h>
#include <shyft/time_series/dd/qac_ts.h>
namespace shyft::energy_market::stm::shop {

using std::size_t;
using std::string;
using std::string_view;
using std::vector;
using std::runtime_error;
using std::dynamic_pointer_cast;
using qac_ts = shyft::time_series::dd::qac_ts;
using qac_parameter = shyft::time_series::dd::qac_parameter;
using namespace shyft::energy_market;
using namespace shyft::energy_market::stm;
using hydro_power::xy_point_curve;
using hydro_power::xy_point_curve_with_z;
using hydro_power::hydro_component;
using hydro_power::hydro_component_;
using hydro_power::hydro_connection;

namespace topology {

    enum class direction : uint8_t {
        upstream,
        downstream
    };

    enum class role_filter : uint8_t { // See also shyft::energy_market::hydro_power::connection_role
        main = 1u << 1,  // Output main waterway, usually a production waterway
        bypass = 1u << 2,// Output bypass, in the meaning of bypass relative a hydro-aggregate/or production unit
        flood = 1u << 3, // Output flood/spill, water escaping a reservoir uncontrolled, at high water-levels
        input = 1u << 4, // Input
        all = 0xFF       // All, convenience for bitwise combination of all values
    };

    template<class T> static shared_ptr<T> as(const hydro_component_& comp) {
        return std::dynamic_pointer_cast<T>(comp);
    }
    template<class T> static shared_ptr<T> as(const hydro_power::power_plant_& comp) {
        return std::dynamic_pointer_cast<T>(comp);
    }
    template<class T> static shared_ptr<T> as(const hydro_power::gate_& comp) {
        return std::dynamic_pointer_cast<T>(comp);
    }
    template<class T> static bool is(const auto& comp) {
        return as<T>(comp) ? true : false;
    }
    static bool match_role_filter(connection_role role, role_filter filter) {
        switch (role) {
        case connection_role::main: return (uint8_t)filter & (uint8_t)role_filter::main;
        case connection_role::bypass: return (uint8_t)filter & (uint8_t)role_filter::bypass;
        case connection_role::flood: return (uint8_t)filter & (uint8_t)role_filter::flood;
        case connection_role::input: return (uint8_t)filter & (uint8_t)role_filter::input;
        default: throw runtime_error("Unexpected connection role"); }
    }
    static const vector<hydro_connection>& connections(const hydro_component& from, direction direction) {
        switch (direction) {
        case direction::upstream: return from.upstreams;
        case direction::downstream: return from.downstreams;
        default: throw runtime_error("Unexpected topology direction"); } // Should never occur, mostly to silence compiler warning (control reaches end of non-void function [-Wreturn-type])
    }
#if 0 // Currently not in use, other utilities duplicates the code to avoid allocating the vector.
    static vector<hydro_connection> connections(const hydro_component& from, direction direction, role_filter roles) {
    	vector<hydro_connection> result;
    	for (const auto& connection : connections(from, direction))
    		if (match_role_filter(connection.role, roles))
    			result.push_back(connection);
    	return result;
    }
#endif
    static hydro_component_ first_connected(const hydro_component& from, direction direction, role_filter roles = role_filter::all) {
        // Return first connected component matching the role filter,
        // looking at immediate connections only (no traversal).
        for (const auto& connection : connections(from, direction))
            if (match_role_filter(connection.role, roles))
                return connection.target_();
        return nullptr;
    }
#if 0 // Currently not in use, other utilities duplicates the code to avoid allocating the vector.
    static vector<hydro_component_> all_connected(const hydro_component& from, direction direction, role_filter roles = role_filter::all) {
        // Return all connected components matching the role filter,
        // looking at immediate connections only (no traversal).
        vector<hydro_component_> result;
        for (const auto& connection : connections(from, direction))
            if (match_role_filter(connection.role, roles))
                result.push_back(connection.target_());
        return result;
    }
#endif
    static bool is_connected(const hydro_component& from, const hydro_component& to, direction direction, role_filter roles = role_filter::all, int levels = -1) {
        // Returns true if there is a topological connection between specified
        // components, in specified direction. Traversal through waterways can be
        // be controlled by argument levels: Will always first check target of
        // connections, and then if any of them are waterways then may traverse
        // them if specified level is at least 1 and then check the waterway's
        // connection targets. Level 0 means no traversing of waterways,
        // <0 means no limit.
        for (const auto& connection : connections(from, direction)) {
            if (match_role_filter(connection.role, roles)) {
                if (const auto& other = connection.target_()) {
                    if (*other == to) return true;
                    if (levels != 0 && is<waterway>(other)) {
                        if (is_connected(*other, to, direction, roles, levels - 1))
                            return true;
                    }
                }
            }
        }
        return false;
    }
    static hydro_component_ traverse(const hydro_component& from, direction direction, role_filter roles, int levels = -1, vector<hydro_connection>* result_backtrack = nullptr) {
        // Return first component connected with a role matching the role filter,
        // optionally traversing waterways, optionally in a limited number of levels.
        // Searches breadth first: Returning result with shortest possible path.
        // Optionally returning backtrack of the traversed path.
        const auto passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
        for (int pass = 0; pass < passes; ++pass) {
            for (const auto& connection : connections(from, direction)) {
                if (match_role_filter(connection.role, roles)) {
                    if (const auto& to = connection.target_()) {
                        if (pass == 0) {
                            if (!is<waterway>(to)) {
                                if (result_backtrack)
                                    result_backtrack->push_back(connection);
                                return to;
                            }
                        } else if (const auto& found = traverse(*to, direction, roles, levels - 1, result_backtrack)) {
                            if (result_backtrack) result_backtrack->push_back(connection);
                            return found;
                        }
                    }
                }
            }
        }
        return nullptr;
    }
    template<class T> static shared_ptr<T> traverse_find_type(const hydro_component& from, direction direction, role_filter roles = role_filter::all, int levels = -1) {
        // Variant of traverse with additional criteria of only stopping on a specific
        // type of component - assuming the type to search for is not a waterway.
        const auto passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
        for (int pass = 0; pass < passes; ++pass) {
            for (const auto& connection : connections(from, direction)) {
                if (match_role_filter(connection.role, roles)) {
                    if (const auto& to = connection.target_()) {
                        if (pass == 0) {
                            if (const auto& found = as<T>(to))
                                return found;
                        } else if (is<waterway>(to)) {
                            if (const auto& found = traverse_find_type<T>(*to, direction, roles, levels - 1))
                                return found;
                        }
                    }
                }
            }
        }
        return nullptr;
    }
    template<class F> static hydro_component_ traverse_find(F fx, const hydro_component& from, direction direction, role_filter roles = role_filter::all, int levels = -1) {
        // Variant of traverse with custom predicate for matching.
        const auto passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
        for (int pass = 0; pass < passes; ++pass) {
            for (const auto& connection : connections(from, direction)) {
                if (match_role_filter(connection.role, roles)) {
                    if (const auto& to = connection.target_()) {
                        if (pass == 0) {
                            if (fx(*to))
                                return to;
                        } else if (is<waterway>(to)) {
                            if (const auto& found = traverse_find(fx, *to, direction, roles, levels - 1))
                                return found;
                        }
                    }
                }
            }
        }
        return nullptr;
    }
    template<class F, class G> static void traverse_for_each(F f_process_component, G f_consider_traverse, const hydro_component& from, direction direction, role_filter roles = role_filter::all, int levels = -1) {
        // Variant of traverse which calls callables:
        //   - Callable f_process_component is called on connected components that are not waterways.
        //   - Callable f_consider_traverse is called on connected waterways, when considering them
        //     for traversal, must return value true if traversal should continue
        //     through it, false if not. Note that on last level of limited traversal, or when
        //     no traversal, i.e. level has value 0, then this callable will not be called on
        //     any of the current component's connected waterways.
        const auto passes = levels == 0 ? 1 : 2; // Breadth first algorithm: Immediates in pass 1, next level in pass 2.
        for (int pass = 0; pass < passes; ++pass) {
            for (const auto& connection : connections(from, direction)) {
                if (match_role_filter(connection.role, roles)) {
                    if (const auto& to = connection.target_()) {
                        if (pass == 0) {
                            if (!is<waterway>(to))
                                f_process_component(to);
                        } else if (const auto& wtr = as<waterway>(to)) {
                            if (f_consider_traverse(wtr))
                                traverse_for_each(f_process_component, f_consider_traverse, *to, direction, roles, levels - 1);
                        }
                    }
                }
            }
        }
    }
}

waterway_ shop_emitter::get_tailrace(const power_plant& pl) {
    // NEW: This version handles different variations of topology:
    //  - Assuming a single tailrace exists accross all units in the plant (this is how Shop handles it).
    //  - Some or all units may have draft tube waterway, connected to tailrace downstream,
    //    while some units may be connected directly to the tailrace without a draft tube.
    //  - Draft tube waterways are optional, all units can be connected directly to the tailrace downstream.
    //  - Draft tube waterwas can be shared by multiple units, or units can have separate draft tubes.
    //  - When there is only one unit it cannot have a draft tube, the first downstream waterway is always the tailrace.
    //  - If all units (including if there is only one) are connected to the same downstream waterway
    //    then this is always the tailrace, there are no draft tubes.
    auto it = std::cbegin(pl.units);
    const auto it_end = std::cend(pl.units);
    if (it == it_end) return nullptr;
    const auto& ag_first = dynamic_pointer_cast<unit>(*it);
    if (!ag_first) return nullptr;
    const auto& ag_first_outlet = dynamic_pointer_cast<waterway>(topology::first_connected(*ag_first, topology::direction::downstream));
    if (!ag_first_outlet) return nullptr;
    if (std::size(pl.units) == 1)
        return ag_first_outlet;
    // Check if all other units in plant are upstream of the outlet tunnel of the first unit
    ++it;
    if (std::all_of(it, it_end, [&ag_first_outlet](const auto& i){return i ? topology::is_connected(*i, *ag_first_outlet, topology::direction::downstream, topology::role_filter::all, 1) : false;}))
        return ag_first_outlet;
    const auto& ag_second_outlet = dynamic_pointer_cast<waterway>(topology::first_connected(*ag_first_outlet, topology::direction::downstream));
    if (!ag_second_outlet) return nullptr;
    if (std::all_of(it, it_end, [&ag_second_outlet](const auto& i){return i ? topology::is_connected(*i, *ag_second_outlet, topology::direction::downstream, topology::role_filter::all, 1) : false;}))
        return ag_second_outlet;
    return nullptr;
}

waterway_ shop_emitter::get_tailrace(const unit& agg) {
    // OLD: Assuming fixed topology where each unit have always one draft tube that ends up in one common tailrace.
    if (const auto& draft_tube = dynamic_pointer_cast<waterway>(agg.downstream())) { // Assuming only one output from each aggreagate, which is always a draft tube.
        if (const auto& tailrace = draft_tube->downstreams.size() ? dynamic_pointer_cast<waterway>(draft_tube->downstreams.front().target_()) : nullptr) { // Assuming only one output from the draft tube, which is always the tailrace.
            return tailrace;
        }
    }
    return {};
}

bool shop_emitter::is_plant_tailrace(const waterway& wtr) {
    // Returns true if specified waterway is the tailrace of an upstream plant.
    if (const auto& ag = topology::traverse_find_type<unit>(wtr, topology::direction::upstream, topology::role_filter::all, 1)) {
        if (const auto& plant = dynamic_pointer_cast<power_plant>(ag->pwr_station_())) {
            if (const auto& tailrace = get_tailrace(*plant)) {
                return *tailrace == wtr;
            }
        }
    }
    return false;
}

bool shop_emitter::is_plant_outlet(const waterway& wtr) {
    // Returns true if specified waterway is part of plant outlet topology,
    // i.e. draft tube or tailrace waterway segments.
    if (const auto& ag = topology::traverse_find_type<unit>(wtr, topology::direction::upstream, topology::role_filter::all, 1)) {
        if (const auto& plant = dynamic_pointer_cast<power_plant>(ag->pwr_station_())) {
            if (const auto& tailrace = get_tailrace(*plant)) {
                if (*tailrace == wtr) return true; // Is it the tailrace?
                return is_connected(wtr, *tailrace, topology::direction::downstream, topology::role_filter::all, 0); // Is it a draft tube?
            }
        }
    }
    return false;
}

bool shop_emitter::is_tunnel(const waterway& wtr) const {
    return adapter.valid_temporal(wtr.head_loss_coeff);
}

double shop_emitter::get_tunnel_loss_coeff(const waterway& wtr) const {
    return adapter.valid_temporal(wtr.head_loss_coeff) ? adapter.get_temporal(wtr.head_loss_coeff, 0.0) : 0.0;
}

/**
 * Utility function that traverses upstream from a waterway, finding components
 * that in shop model might contribute to discharge in the given waterway. Possible
 * components are powerplants and gates, as well as waterways with gates (which
 * also ends the traversal). Note that the given waterway will be checked first,
 * i.e. if it contains a gate emitted to shop then only this will be considered
 * and no traversal will actually be done. The function (e.g. lambda) given as
 * argument will be called on each of the found components.
 *
 * Note: It is a prerequisite that the candidate components are already emitted to shop.
 */
template<class F> void shop_emitter::foreach_upstream_shop_source(const waterway& wtr, F func) {
    vector<const power_plant*> plants;
    auto f_process_component_ = [this, &func, &plants](const hydro_component_& c) {
        // When reaching a non-waterway component
        if (auto ut = topology::as<unit>(c)) {
            if (auto ps = topology::as<power_plant>(ut->pwr_station_())) {
                // Since it is the units we find during stm traverse but plants that are
                // represented in shop, we lookup stm plant from the unit and then corresponding
                // shop plant. But for a plant with more than one unit we would then process the
                // same plant multiple times, so to avoid this we keep track of which plants are
                // already considered and skip them next time.
                if (std::find(std::cbegin(plants), std::cend(plants), ps.get()) == std::cend(plants)) {
                    if (const auto it = objects.find<shop_power_plant>(ps.get()); it != objects.end<shop_power_plant>())
                        func(it->second);
                    else throw runtime_error("Plant not emitted to shop: " + ps->name); // Forgot to emit plants before calling this?
                    plants.push_back(ps.get());
                }
            }
        }
    };
    auto f_consider_waterway = [this, &func](const waterway& w) {
        // When reaching a waterway
        auto git = w.gates.cbegin();
        bool is_shop_gate = false;
        for (auto id = git != w.gates.cend() ? static_cast<const id_base*>(git->get()) : static_cast<const id_base*>(&w);
            id; id = git < w.gates.cend() ? static_cast<const id_base*>((++git)->get()) : nullptr) { // Loop on gates, or iff no gates: 1 iteration with just w!
            if (const auto it = objects.find<shop_gate>(id); it != objects.end<shop_gate>()) {
                func(it->second);
                is_shop_gate = true;
            }
        }
        // Stop traverse by returning false if this waterway or any of its
        // gates were emitted as gates to shop.
        return !is_shop_gate;
    };
    auto f_consider_waterway_ = [&f_consider_waterway](const waterway_& w) { return w && f_consider_waterway(*w); };
    if (f_consider_waterway(wtr)) {
        traverse_for_each(f_process_component_, f_consider_waterway_, wtr, topology::direction::upstream);
    }
}

/**
 * Find and connect stm objects and shop objects, related to a discharge group.
 *
 * @param wtr Waterway to start iterative search for components, related to a discharge group.
 */
void shop_emitter::handle_discharge_group(const waterway& wtr) {
    // Precondition: Reservoir, gates, powerplants, units must be emitted to shop

    if ( ( shop_adapter::exists(wtr.discharge.reference) &&
            (shop_adapter::exists(wtr.discharge.constraint.accumulated_max) ||
            shop_adapter::exists(wtr.discharge.constraint.accumulated_min)))
        ||
        (shop_adapter::exists(wtr.discharge.constraint.max) || shop_adapter::exists(wtr.discharge.constraint.min))
        || (shop_adapter::exists(wtr.discharge.constraint.ramping_up) ||
            shop_adapter::exists(wtr.discharge.constraint.ramping_down))) {

        auto sdg = objects.add<shop_discharge_group>(wtr, adapter.to_shop_discharge_group(wtr));

        foreach_upstream_shop_source(wtr, [this, &sdg](auto& shop_obj){
            api._connect_objects(sdg.id, ::shop::connection::main, shop_obj.id);
        });
    }
}

/**
 * Handle time delay for topology differences.
 *
 * When emitting waterways representing flood and bypass river output from
 * reservoirs, any time delay will be set on the gate. This function adds
 * additional handling of time delay for topology differences between stm and shop:
 * Time delay on a downstream waterway is also set on any/all gate or plant objects
 * emitted to shop that represent upstream objects, except if these already had a
 * time delay set directly.
 */
void shop_emitter::handle_time_delay(const waterway& wtr) {
    foreach_upstream_shop_source(wtr, [this, &wtr](auto& shop_obj){
        adapter.set_shop_time_delay(shop_obj, wtr.delay);
    });
}

waterway_ shop_emitter::get_penstock(const unit& agg, bool always_inlet) {
    // Assuming each aggregate have a dedicated waterroute upstream, which is either its penstock
    // or an inlet tunnel leading to a penstock (possibly) shared with other aggregates.
    // There can only be one waterroute segment of any inlet and penstock.
    if (always_inlet) {
        // Mode 1: Assuming there is always an inlet tunnel connecting an aggregate to its penstock.
        // This is easy, the penstock is always the second waterroute segment above the aggregate!
        if (const auto& inlet = dynamic_pointer_cast<waterway>(agg.upstream())) { // Assuming only one input to each aggreagate, which is always an inlet tunnel out from a penstock.
            if (const auto& penstock = inlet->upstreams.size() ? dynamic_pointer_cast<waterway>(inlet->upstreams.front().target_()) : nullptr) { // Assuming only one input to the inlet, which is always the penstock.
                return penstock;
            }
        }
    } else {
        // Mode 2: Assuming the inlet tunnel is optional, and only present if the penstock is shared.
        // This is a bit harder, since we might have combinations of branches with shared penstocks and inlets,
        // and branches with individual penstocks and no inlets. But with the two assumptions, inlet only
        // present if necessary (shared penstocks), and only one waterroute segment of any penstock and inlet,
        // it is rather easy to unambiguously find out: We know that we just have to consider at most two waterroute
        // segments up, so by moving up three segments from the aggregate, we have the entire penstock-inlet-aggregate
        // tree structure below, and we also know that a penstock can only have other penstocks as sibling segments,
        // and inlets can only have other inlets as siblings.
        // It is considered an error to include inlet tunnel for an aggregate with a penstock that is not
        // shared, because it is not possible to know where the penstock is in all cases. E.g. with a stream
        // aggregate-inlet-penstock-main-rsv and another stream aggregate-penstock-main1-main2-rsv we cannot detect
        // the penstock waterroute consistently in both cases (we would pick the inlet as the penstock in the first case).
        if (const auto& first = dynamic_pointer_cast<waterway>(agg.upstream())) { // Assuming only one input to each aggreagate, which is either the penstock or an inlet out from a shared penstock.
            if (const auto& second = first->upstreams.size() ? dynamic_pointer_cast<waterway>(first->upstreams.front().target_()) : nullptr) { // Assuming only one input to the tunnel (no junction), which is either a shared penstock or the main tunnel.
                size_t siblings = 0; // Count sibling segments of first (sibling penstocks if first is a penstock, sibling inlets if first is an inlet)
                for (const auto& con : second->downstreams) {
                    if (const auto& sibling_of_first = dynamic_pointer_cast<waterway>(con.target_())) {
                        if (sibling_of_first != first) {
                            ++siblings;
                            for (const auto& con : sibling_of_first->downstreams) {
                                if (const auto& target = con.target_()) {
                                    if (dynamic_pointer_cast<waterway>(target)) {
                                        // The segment sibling to the first upstream from the aggregate leads into a deeper structure,
                                        // and then it is safe to assume it is a penstock connected by inlets into multiple aggregates.
                                        // This means the "first" segment we found initially cannot cannot be an inlet, as penstocks and
                                        // inlets cannot be siblings, so this means it must be the penstock we are looking for!
                                        return first;
                                    } else if (dynamic_pointer_cast<unit>(target)) {
                                        // The segment sibling to the first upstream from the aggregate leads into another aggregate,
                                        // and then we do not know if the two segments are inlets from a shared penstock, or if these
                                        // aggregates have separate penstocks without inlets. To find out we must go to level three above
                                        // the aggregate, to see if there are other branches down from there. If there is, then we know that
                                        // our current branch is with a shared penstock - with an inlet segment first and penstock second.
                                        if (const auto& third = second->upstreams.size() ? dynamic_pointer_cast<waterway>(second->upstreams.front().target_()) : nullptr) {
                                            for (const auto& con : third->downstreams) {
                                                if (const auto& sibling_of_second = dynamic_pointer_cast<waterway>(con.target_())) {
                                                    if (sibling_of_second != second) {
                                                        return second;
                                                    }
                                                }
                                            }
                                        }
                                        return first;
                                    } else {
                                        throw runtime_error(string("Unexpected element in plant topology: ") + target->name + " [" + std::to_string(target->id) + "]");
                                    }
                                }
                            }
                        }
                    }
                }
                if (siblings < 2) {
                    // There were no siblings of the first segment, which means it cannot be an inlet and it must be the penstock
                    // (from the assumption that inlets are only modelled when connecting multiple aggregates to a shared penstock).
                    return first;
                }
            }
        }
    }
    return {};
}

void shop_emitter::handle_plant_input(const waterway& stm_obj, shop_object_id shop_downstream_obj) {
    // Create shop tunnel representing current stm waterway
    auto& shop_obj = objects.add(&stm_obj, adapter.to_shop(stm_obj));
    api._connect_objects(shop_obj.id, ::shop::connection::main, shop_downstream_obj.id);
    // Look upstream
    const size_t n_upstreams = stm_obj.upstreams.size();
    if (n_upstreams < 1) {
        // No upstream: Invalid!
        throw runtime_error(string("No upstreams from waterways ") + stm_obj.name + " [" + std::to_string(stm_obj.id) + "]");
    } else if (n_upstreams < 2) {
        // Single upstream: Either a reservoir, or an additional waterway segment.
        if (const auto& upstream = dynamic_pointer_cast<reservoir>(stm_obj.upstreams.front().target_())) {
            // Reached the end of this stream (and recursion): Upstream tunnel segment ended in reservoir.
            // Assuming reservoirs have already been handled, we just return it's shop id.
            if (auto shop_upstream = objects.id_of(upstream)) {
                api._connect_objects(shop_upstream.id, ::shop::connection::main, shop_obj.id);
            } else {
                throw runtime_error("Reached an input reservoir which has not been emitted");
            }
        } else if (const auto& upstream = dynamic_pointer_cast<waterway>(stm_obj.upstreams.front().target_())) {
            // Additional water route segments
            handle_plant_input(*upstream, shop_obj); // Recurse
        } else {
            // Only reservoirs are allowed upstream from plant!?
            throw runtime_error("Not implemented: Input tunnel from anything other than reservoir!");
        }
    } else {
        // Multiple upstreams: All upstreams should be additional water route segments (junction)
        for (const auto& up : stm_obj.upstreams) {
            if (const auto& upstream = dynamic_pointer_cast<waterway>(up.target_())) {
                handle_plant_input(*upstream, shop_obj); // Recurse
            } else {
                throw runtime_error(string("Multiple upstreams are not all waterways from waterways ") + stm_obj.name + " [" + std::to_string(stm_obj.id) + "]");
            }
        }
    }
}

bool shop_emitter::handle_plant_output(const shop_object_id shop_plant_obj, const waterway& tailrace) {
    // Handle topology downstream from plant/trailrace: If there is a downstream reservoir, then
    // connect the plant to it - either directly (river) or via tunnel segments.
    vector<hydro_connection> backtrack;
    const auto& down = topology::traverse(tailrace, topology::direction::downstream, topology::role_filter::all, -1, &backtrack);
    if (const auto& rsv = dynamic_pointer_cast<reservoir>(down)) {
        if (auto rsv_id = objects.id_of<shop_reservoir>(rsv)) {
            auto it = backtrack.crbegin();
            if (const auto& wtr = dynamic_pointer_cast<waterway>(it->target_())) {
                if (is_tunnel(*wtr)) {
                    // Emitting connection to downstream reservoir as tunnel segments
                    auto& st = objects.add(wtr.get(), adapter.to_shop(*wtr));
                    api._connect_objects(shop_plant_obj.id, ::shop::connection::main, st.id);
                    auto prev_id = st.id;
                    for (++it; it != backtrack.crend(); ++it) {
                        if (const auto& wtr = dynamic_pointer_cast<waterway>(it->target_())) {
                            auto next_id = objects.id_of(wtr).id;
                            if (next_id >= 0) {
                                api._connect_objects(prev_id, ::shop::connection::main, next_id);
                                break;
                            } else if (const auto& tunnel = dynamic_pointer_cast<waterway>(it->target_())) {
                                auto& next = objects.add(it->target_().get(), adapter.to_shop(*tunnel));
                                api._connect_objects(prev_id, ::shop::connection::main, next.id);
                                prev_id = next.id;
                            }
                        } else {
                            break; // Last item is the reservoir that we reached, should be the only possible case for this!
                        }
                    }
                    api._connect_objects(prev_id, ::shop::connection::main, rsv_id.id);
                    return true; // Done, connected to downstream reservoir via plant
                }
            }
            // Emitting connection to downstream reservoir as river, connecting plant directly to the reservoir.
            api._connect_objects(shop_plant_obj.id, ::shop::connection::main, rsv_id.id);
            return true; // Done, connected to downstream reservoir directly (river)
        }
    }
    return false; // Not connected to downstream reservoir
}

void shop_emitter::handle_reservoir_output(const reservoir& rsv) {
    // Handle output connections from a reservoir, but only those ending
    // in other reservoirs. Connections from reservoirs to aggregates are handled
    // by the upstream tunnel handling for power plants.
    // NOTE: Since we currently allow bypass and main waterroutes out from reservoir
    // which are not connected to anything downstream, implicitely to the sea,
    // we must process the connections _out_ and not _in_ from the reservoirs!
    for (const auto& out : rsv.downstreams) {
        switch (out.role) {
        case hydro_power::main:
        case hydro_power::bypass:
        case hydro_power::flood:
            if (const auto& wtr = dynamic_pointer_cast<waterway>(out.target_())) {
                // Find downstream object (if any), and then:
                // - If a downstream object was found, and it is a reservoir, create a gate
                //   object and connect the two reservoirs via it. The stm model may or may
                //   not have actual gates, it could be waterway with delta-meter function,
                //   but in any case it must be modelled as gate in shop.
                // - If the downstream object was anything else, skip - aggregates will be
                //   handled elsewhere by the upstream tunnel handling for power plants.
                // - If there was no downstream object then add a gate connected upstream only,
                //   effectively just letting it drain out into the sea. This is often used for
                //   bypass and flood, but could also be main waterway out from a reservoir
                //   at the end of the topology (ref: Trollheim). This case is only supported
                //   for rivers (not tunnels).
                vector<hydro_connection> backtrack;
                const auto& down = topology::traverse(*wtr, topology::direction::downstream, topology::role_filter::all, -1, &backtrack);
                int rsv2_id = -1;
                if (const auto& rsv2 = dynamic_pointer_cast<reservoir>(down)) {
                    rsv2_id = objects.id_of<shop_reservoir>(rsv2).id;
                }
                if (rsv2_id >= 0 || !down) {
                    // This is a reservoir output that must be emitted: Flow from one reservoir
                    // into another, or from reservoir into nothing/sea.
                    auto rsv1_id = objects.id_of(rsv).id;
                    if (!rsv1_id)
                        throw runtime_error(string("Failed to create reservoir output: Unable to find shop object for stm reservoir ") + rsv.name + " [" + std::to_string(rsv.id) + "]");
                    auto shop_role = out.role == hydro_power::bypass ? ::shop::connection::bypass : out.role == hydro_power::flood ? ::shop::connection::flood : ::shop::connection::main;
                    if (rsv2_id >= 0 && is_tunnel(*wtr)) {
                        // Tunnel, represented by tunnel object also in Shop.
                        // Note: Requires it to end in a reservoir, a waterway string not connected
                        // to anything downstream will always be emitted as a river.
                        auto& st = objects.add(wtr.get(), adapter.to_shop(*wtr));
                        api._connect_objects(rsv1_id, shop_role, st.id);
                        auto prev_id = st.id;
                        for (auto it = backtrack.crbegin(); it != backtrack.crend(); ++it) {
                            if (const auto& wtr = dynamic_pointer_cast<waterway>(it->target_())) {
                                auto next_id = objects.id_of(wtr).id;
                                if (next_id >= 0) {
                                    api._connect_objects(prev_id, ::shop::connection::main, next_id);
                                    break;
                                } else if (const auto& tunnel = dynamic_pointer_cast<waterway>(it->target_())) {
                                    auto& next = objects.add(it->target_().get(), adapter.to_shop(*tunnel));
                                    api._connect_objects(prev_id, ::shop::connection::main, next.id);
                                    prev_id = next.id;
                                }
                            } else {
                                break; // Last item is the reservoir that we reached, should be the only possible case for this!
                            }
                        }
                        api._connect_objects(prev_id, ::shop::connection::main, rsv2_id);
                    } else {
                        // River, represented by gate object in Shop.
                        // If there are no actual gates in stm, send the waterroute as a gate,
                        // since Shop has no notion of water routes but represent all as gates.
                        // Note: We only use the first (upstream) stm waterroute (with its gates) to create the Shop gate,
                        //       if there are multiple water route segments then we ignore any gate on following segments.
                        // TODO: We send all gates on the water route (parallel gates) to Shop, but it is not
                        //       currently supporting more than one gate on the flood connection!
                        auto git = wtr->gates.cbegin();
                        auto gt = git != wtr->gates.cend() ? std::dynamic_pointer_cast<gate>(*git) : nullptr;
                        do {
                            auto& sg = objects.add(
                                gt ? static_cast<const id_base*>(gt.get()) : static_cast<const id_base*>(wtr.get()),
                                adapter.to_shop_gate(*wtr, gt.get()));
                            // Connect required upstream reservoir/creek to the gate
                            api.connect_reservoir_gate(rsv1_id, shop_role, sg.id);
                            // Connect optional dowstream reservoir to the gate
                            if (rsv2_id >= 0)
                                api.connect_reservoir_gate(sg.id, ::shop::connection::main, rsv2_id);
                            if (gt)
                                gt = ++git != wtr->gates.cend() ? std::dynamic_pointer_cast<gate>(*git) : nullptr;
                        } while (gt);
                    }
                }
            }
            break;
        case hydro_power::input:
            break;
        }
    }
}

void shop_emitter::emit(const vector<hydro_power::reservoir_>& reservoirs) {
    // First emit all
    for (const auto& r : reservoirs) {
        if (const auto& rsv = dynamic_pointer_cast<reservoir>(r)) {
            objects.add(rsv.get(), adapter.to_shop(*rsv));
        }
    }
    // Go through them all again to handle any gate connections between them
    for (const auto& r : reservoirs) {
        if (const auto & rsv = dynamic_pointer_cast<reservoir>(r)) {
            handle_reservoir_output(*rsv);
        }
    }
}

void shop_emitter::emit(const hydro_power::power_plant& plant) {
    // Migrate power plant, with surrounding topology (aggregates and tunnels),
    // and connect it to upstream reservoirs, possibly via junctions and/or creek intakes, as well
    // as downstream reservoir (if any).
    // Current assumptions:
    // - Only a single main tunnel from intake reservoirs (not supporting parallel intakes).
    // - Each aggregate have a dedicated waterroute upstream, which is either its penstock or an inlet
    //   tunnel leading from a penstock shared with other aggregates.
    // - There can only be one waterroute segment of any inlet and penstock.
    // - Penstock loss is the headloss coefficient on individual penstocks, where aggregates can have
    //   separate or shared penstocks which must be indicated by their penstock attribute.
    // - Tailrace loss is the headloss coefficient on the shared (accross aggregates) tailrace.
    //   Assuming always one output tunnel from each aggreagate, which is always a draft tube,
    //   and then always one output tunnel from the draft tube, which is always the tailrace.
    // - Plant main loss is the headloss in the shared (accross all penstocks and aggregates)
    //   main intake tunnel, between the reservoir and the penstocks, and can be described in two ways:
    //     - Head loss coefficients: An array of double values representing loss factors. Normally there
    //       is only one coefficient, but there may be multiple values if there is a need to differentate
    //       segments of the main tunnel. Currently we only consider loss from a single segment, and then
    //       pick the most downstream main tunnel segment (immediately upstream from penstocks).
    //     - Loss function: An array of XYZ descriptions, one for each head. When this is used, one
    //       normally does not specify head loss coefficients, nor penstock loss coefficients, as the loss
    //       function describes the complete input loss in detail. We only look for loss function on the
    //       most downstream main tunnel segment (immediately upstream from penstocks).
    auto const& pl = dynamic_cast<const power_plant&>(plant);
    // Create basic Shop representation of the power station
    auto sp = objects.add(&pl, adapter.to_shop(pl));
    // Handle aggreagates, with immediately surrounding topology (penstocks and tailrace)
    vector<waterway_> penstocks;
    waterway_ tailrace = get_tailrace(pl);
    if (tailrace) {
        // If tailrace have an input from a reservoir, assuming bypass, then we will
        // have emitted this as a gate connection from this reservoir to the reservoir
        // downstream from plant, but must now set the option on plant that triggers Shop
        // to include this flow in the tailrace loss calculation.
        if (topology::traverse_find_type<reservoir>(*tailrace, topology::direction::upstream)) {
            apoint_ts flag_ts{adapter.time_axis,1.0,time_series::POINT_AVERAGE_VALUE};
            adapter.set(sp.tailrace_loss_from_bypass_flag, flag_ts);
        }
    }
    for (const auto& g : pl.units) {
        if (const auto& ag = dynamic_pointer_cast<unit>(g)) {
            auto& sg = objects.add(ag.get(), adapter.to_shop(*ag));
            api.connect_plant_generator(sp.id, sg.id);
            if (const auto& penstock = get_penstock(*ag)) { // Find penstock of the aggregate
                int penstock_number;
                if (auto it = std::find_if(std::cbegin(penstocks), std::cend(penstocks), [&penstock](const auto& v) { return penstock->id == v->id; }); it != std::cend(penstocks)) {
                    penstock_number = (int) std::distance(std::cbegin(penstocks), it) + 1;
                } else {
                    penstocks.push_back(penstock);
                    penstock_number = penstocks.size();
                }
                adapter.set(sg.penstock, penstock_number);
            } else {
                throw runtime_error(string("Unable to find penstock for plant ") + pl.name + " [" + std::to_string(ag->id) + "] aggregate " + ag->name + " [" + std::to_string(ag->id) + "]");
            }
#if 0
            if (const auto& tr = get_tailrace(*ag)) { // Find tailrace of the aggregate
                if (!tailrace) {
                    tailrace = tr;
                } else if (tr->id != tailrace->id) {
                    throw runtime_error(string("More than one tailrace for plant ") + pl.name);
                }
            } else {
                // Accept missing tailrace for one aggregate? If another aggregate do have a tailrace, then this will be used to represent entire plant!
                //throw runtime_error(string("Unable to find tailrace for plant ") + ps->name + " [" + std::to_string(ag->id) + "] aggregate " + ag->name + " [" + std::to_string(ag->id) + "]");
            }
#endif
        }
    }
    // Set penstock and tailrace head loss attributes (penstock and tailrace segments are parts of the plant in shop)
    vector<double> penstock_loss;
    std::transform(std::cbegin(penstocks), std::cend(penstocks), std::back_inserter(penstock_loss), [this](const auto& penstock) {return get_tunnel_loss_coeff(*penstock);});
    adapter.set(sp.penstock_loss, penstock_loss);
    if (tailrace && adapter.exists(tailrace->head_loss_func)) { // Accept missing tailrace for plant?
        adapter.set(sp.tailrace_loss, tailrace->head_loss_func);
    }
    // Continue with topology upstream from penstocks: Connect shop plant with tunnel objects (new tunnel module) up to input reservoirs.
    if (auto main = penstocks.front()->upstreams.size() ? dynamic_pointer_cast<waterway>(penstocks.front()->upstreams.front().target_()) : nullptr) {
        handle_plant_input(*main, sp);
        if (adapter.exists(main->head_loss_func)) // Head loss function (XYZ array)
            adapter.set(sp.intake_loss, main->head_loss_func);
    }
    vector<double> main_loss{0.0}; // Legacy (pre tunnel module): Array with loss in each segment of the main tunnel.
    adapter.set(sp.main_loss, main_loss);
    // Continue with topology downstream from trailrace: Connect plant output to downstream reservoir - if any.
    if (tailrace) {
        if (!handle_plant_output(sp, *tailrace)) {
            // If no reservoir downstream, emit tides attribute as deltas to the outlet level.
            // The initial value of outlet level attribute is already emitted as scalar value
            // outlet_level, and we will now emit the deltas compared to this for any additional
            // outlet level values within the timeaxis.
            // NOTE: This is an experimental/hacky solution, because it has not yet been
            // concluded how users want this to be handled, and also epecting improvements in
            // shop will affect this feature in coming versions.
            if (adapter.valid_temporal(pl.outlet_level)) {
                auto base_level = adapter.get_temporal(pl.outlet_level, 0.0);
                apoint_ts diff_ts{adapter.time_axis,0.0,POINT_AVERAGE_VALUE};
                diff_ts = pl.outlet_level.use_time_axis_from(diff_ts) - base_level;
                adapter.set(sp.tides, diff_ts);
            } 
        }
    }
}

void shop_emitter::emit(const stm_hps& hps) {
    // Emit reservoirs first, they must be emitted before emitting plants.
    emit(hps.reservoirs);
    // Emit plants, and connect upstream tunnels and reservoirs.
    emit(hps.power_plants);
    // Emit any discharge groups, and set time delay from stm waterroutes,
    // that are not emitted directly, on shop representation of upstream
    // gate and plant objects.
    for (const auto &wv : hps.waterways) {
        if (const auto &w = dynamic_pointer_cast<waterway>(wv)) {
            handle_discharge_group(*w);
            handle_time_delay(*w);
        }
    }
}

void shop_emitter::to_shop(const stm_system& stm) {
    adapter.ema_prod_area_id=1;// shop need to start at 1,
    emit(stm.market);
    emit(stm.hps);

    // Unit groups: Reserves
    if (stm.unit_groups.size()) {
        for (auto const& ug : stm.unit_groups) {
            if (ug->group_type >= unit_group_type::fcr_n_up && ug->group_type <= unit_group_type::rr_down) {
                // Record which units are active in this group for each time step as bit coded values in a time series
                const auto group_ts = compute_group_unit_combinations_ts(ug, adapter.time_axis);
                // For each unique set of members, emit a separate group with series masked to the time steps where these members are active and connect these members to it
                vector<shop_reserve_group> shop_groups;
                // NEW: Find the unique set of unit combinations and then check each of these.
                const auto group_ts_values = group_ts.values();
                const std::unordered_set unit_combinations(std::cbegin(group_ts_values), std::cend(group_ts_values));
                // OLD: Looping on all possible unit combinations, checking each of them if activate at any time step.
                //      This "explodes" in loop size when number of members are large (e.g. loops 256 times when 8 members, 65536 times when 16, and 4294967296 times when 32 members).
                //      (To be deleted as soon as we have verified the new approach in deployed environment.)
                //for (size_t unit_combination = 1, n = static_cast<size_t>(1)<<ug->members.size(); unit_combination < n; ++unit_combination) {
                for (const auto& unit_combination : unit_combinations) {
                    const apoint_ts mask_ts = group_ts.inside(unit_combination-0.1,unit_combination+0.1,0.0,1.0,0.0); // mask ts initially all zeros, later set to 1 in each time step this combination of units are active
                    // OLD: When looping on all possible unit combinations (see above)
                    //const auto mask_ts_values = mask_ts.values();
                    //if (std::find_if(std::cbegin(mask_ts_values), std::cend(mask_ts_values), [](const auto& v){return v > 0.5;}) != std::cend(mask_ts_values)) { // if this combination of units are active at any time step
                        const auto name = ug->name + string{"#"} + std::to_string(static_cast<size_t>(unit_combination)); // generate unique name since same stm unit group may be emitted several times (with different unit combination masks)
                        auto shop_ug = adapter.to_shop(*ug, mask_ts, name);
                        for (size_t i = 0, n = ug->members.size(); i < n; ++i) {
                            if (static_cast<size_t>(1)<<i & static_cast<size_t>(unit_combination)) {
                                const auto& ugm = ug->members[i];
                                const auto& shop_u = objects.cget<shop_unit>(ugm->unit.get());
                                api._connect_objects(shop_u.id, ::shop::connection::main, shop_ug.id);
                            }
                        }
                        shop_groups.push_back(std::move(shop_ug));
                    //}
                }
                // Save the list of emitted shop groups emitted for this stm unit group in object map
                objects.add(ug.get(), std::move(shop_groups));
            }
        }
    }

    // Unit groups: Production
    if(stm.market.size()) { //Given that there is a market!
        // need to map the energy market area id which is associated with a unit group to the shop_unit.prod_area
        vector<unit_group_> ema_groups;// unit group with production for each area
        map<int64_t,int64_t> mid_map; // energy market area to shop market id map. shop only deals with its own ids
        for(auto const&ema:stm.market) {
            auto ug=ema->get_unit_group();
            if(ug) {
                ema_groups.push_back(ug);
                auto const& mkt=objects.get<shop_market>(ema.get());
                mid_map[ug->id]=mkt.prod_area;//notice! its the unit group id mapping that we need for the compute_unit_group_ts
                // std::cout<<" EMA."<<ema->id<<" -> SMKT."<<mkt.prod_area<<std::endl;
            }
        }
        apoint_ts ts{adapter.time_axis,1.0,POINT_AVERAGE_VALUE};// ensure we are complete in timesteps for the run

        if(ema_groups.size()==0) {// BWCOMPAT: if no unit groups, create a temporary one, and add all units.
            auto ema=stm.market.front();//just pick the first(and only we hope).
            //std::cout<<"Gen default group for area "<<ema->name<<", .id="<<ema->id<<std::endl;
            auto ug=std::make_shared<unit_group>();//const_cast<stm_system*>(&stm));
            for (auto const& hps:stm.hps)
                for(auto const&u_:hps->units)
                    ug->add_unit(std::dynamic_pointer_cast<stm::unit>(u_),ts);
            ug->group_type=unit_group_type::production;
            ug->id=stm.market.front()->id;
            auto const& mkt=objects.get<shop_market>(ema.get());
            mid_map[ug->id]=mkt.prod_area;// make sure to provide a mapping for the bwcompat case
            ema_groups.push_back(ug);
        } else {
            ;//std::cout<<"ema groups is configured already"<<std::endl;
        }

        for (auto const& hps:stm.hps) {
            for(auto const&u_:hps->units) { //for all units, attach the time-dependent market ts
                auto u=std::dynamic_pointer_cast<stm::unit>(u_);//because hps->units are core units, not stm units that we need
                auto mts=compute_unit_group_membership_ts(ema_groups,u,ts,unit_group_type::production, &mid_map);// time dep. market assoc
                if(!mts) {
                    //TODO: throw or ignore? std::cout<<"u "<<u->name<<"has no membership to market areas/unit group\n";
                    continue;
                }
                auto &s_u=objects.get<shop_unit>(u.get());
                adapter.set(s_u.prod_area, mts);
            }
        }
    }
}

void shop_emitter::from_shop(stm_system& stm) {
    {
        auto so = api.get<shop_objective>("average_objective");
        adapter.from_shop(*stm.summary, so);
    }
    for (const auto& m : stm.market) {
        if (const auto& mkt = dynamic_pointer_cast<energy_market_area>(m)) {
            if (const auto it = objects.find<shop_market>(mkt.get()); it != objects.end<shop_market>()) {
                adapter.from_shop(*mkt, it->second);
            }
        }
    }
    if(stm.unit_groups.size()) {
        for (const auto& ug : stm.unit_groups) {
            if (const auto it = objects.find<vector<shop_reserve_group>>(ug.get()); it != objects.end<vector<shop_reserve_group>>()) {
                for (const auto& shop_group : it->second) {
                    adapter.from_shop(*ug, shop_group);
                }
            }
        }
    }
    for (const auto& hps : stm.hps) {
        for (const auto& r : hps->reservoirs) {
            if (const auto& rsv = dynamic_pointer_cast<reservoir>(r)) {
                if (const auto it = objects.find<shop_reservoir>(rsv.get()); it != objects.end<shop_reservoir>()) {
                    adapter.from_shop(*rsv, it->second);
                }
            }
        }
        for (const auto& p : hps->power_plants) {
            if (const auto& ps = dynamic_pointer_cast<power_plant>(p)) {
                if (const auto it = objects.find<shop_power_plant>(ps.get()); it != objects.end<shop_power_plant>()) {
                    adapter.from_shop(*ps, it->second);
                }
            }
        }
        for (const auto& a : hps->units) {
            if (const auto& agg = dynamic_pointer_cast<unit>(a)) {
                if (const auto it = objects.find<shop_unit>(agg.get()); it != objects.end<shop_unit>()) {
                    adapter.from_shop(*agg, it->second);
                }
            }
        }
        for (const auto& w : hps->waterways) {
            if (const auto& wtr = dynamic_pointer_cast<waterway>(w)) {
                if (const auto it = objects.find<shop_tunnel>(wtr.get()); it != objects.end<shop_tunnel>()) {
                    // Entire waterway was emitted as shop tunnel, put results back to waterway (no gates will be considered for it)
                    adapter.from_shop(*wtr, it->second);
                } else if (const auto it = objects.find<shop_gate>(wtr.get()); it != objects.end<shop_gate>()) {
                    // Entire waterway was emitted as shop gate, put results back to waterway (no gates will be considered for it)
                    adapter.from_shop(*wtr, it->second);
                } else {
                    // Waterway may have gates emitted to shop, put results back to the gates (not the waterway, will be calculated)
                    for (auto const& g : wtr->gates) {
                        if (const auto& gt = dynamic_pointer_cast<gate>(g)) {
                            if (const auto it = objects.find<shop_gate>(gt.get()); it != objects.end<shop_gate>()) {
                                adapter.from_shop(*gt, it->second);
                            }
                        }
                    }
                }
                if (const auto it = objects.find<shop_discharge_group>(wtr.get()); it != objects.end<shop_discharge_group>()) {
                    adapter.from_shop(*wtr, it->second);
                }
            }
        }
    }
}

namespace {

/**
 * @brief make waterway discharge complete
 * @details
 * Shop only fills in what is given to it, and barely that, so
 * we need to iterate over the hps, and compute the remaining flows
 * to make it easier for users to interpret the results.
 * There are some issues outstanding here, like time-delay
 * (when flow into a waterway is not instantly the same as goes out).
 */
void compute_missing_waterway_discharges(stm_system& stm) {
    auto _copy_if_values = [](const apoint_ts& ts) {
        // Copy if it is a bound non-empty time series containing any non-nan values.
        // Does explicitely not copy if it is an expression with unbound time series.
        if (!ts.needs_bind() && shop_adapter::has_values(ts))
            return apoint_ts(ts.time_axis(), ts.values(), ts.point_interpretation());
        return apoint_ts{};
    };
    auto _get_discharge = [&_copy_if_values](const auto& o) {
        if (const auto& x = std::dynamic_pointer_cast<stm::unit>(o))
            return _copy_if_values(x->discharge.result);
        if (const auto& x = std::dynamic_pointer_cast<stm::waterway>(o))
            return _copy_if_values(x->discharge.result);
        if (const auto& x = std::dynamic_pointer_cast<stm::gate>(o))
            return _copy_if_values(x->discharge.result);
        return apoint_ts{};
    };
    for (const auto& hps : stm.hps) {
        for (size_t r=0;r<5;++r) { // 5 iterations: 1st, will fill in all 1st order simple cases, 2nd, and 3rd,etc. will do more simple plus complex
            size_t n_wtr_discharge_missing{0};// n wtr filled in
            for (const auto& wx : hps->waterways){
                if (const auto& w = std::dynamic_pointer_cast<stm::waterway>(wx)) {

                    // Compute waterway discharge result if it is a bound time series that is empty or
                    // contains nan values only. Silently skip if it is an expression with unbound time series.
                    if (!w->discharge.result.needs_bind() && !shop_adapter::has_values(w->discharge.result)) {

                        ++n_wtr_discharge_missing;
                        apoint_ts computed_discharge;

                        // Check gates.
                        // If waterway has gates, and any of them have flow, then sum them togheter.
                        if (w->gates.size() > 0) {
                            for (auto const& g : w->gates) {
                                auto ts = _get_discharge(g);
                                if (ts.size() != 0) // sum all with flow, silently skip any without? since gate flow is not computed, it is a raw shop result, we will not gain anything from waiting to a later iteration
                                    computed_discharge = computed_discharge.ts ? computed_discharge + ts : ts;
                            }
                            if (computed_discharge.size() != 0) {
                                w->discharge.result = computed_discharge.evaluate();
                                --n_wtr_discharge_missing; // fixed it
                                continue; // take next
                            }
                        }

                        // Check upstreams.
                        // If single connect upstream.downstream, and has flow, take the flow from that (a waterway, or unit).
                        if (w->upstreams.size() == 1) { // one single upstream connect
                            auto us=w->upstreams.front().target_();
                            if(us && us->downstreams.size()==1) { // single connect: upstream only feed to this
                                computed_discharge = _get_discharge(us);
                                if (computed_discharge.size() != 0) {
                                    w->discharge.result = computed_discharge;
                                    --n_wtr_discharge_missing; // fixed it
                                    continue; // take next
                                }
                            }
                        }

                        // Check downstream.
                        // If single connect downstream, take the flow from that (a waterway, or unit).
                        if (w->downstreams.size() == 1) {
                            auto ds = w->downstreams.front().target_();
                            if (ds && ds->upstreams.size()==1) { // single connect: only feed to this
                                computed_discharge = _get_discharge(ds);
                                if (computed_discharge.size() != 0) {
                                    w->discharge.result = computed_discharge;
                                    --n_wtr_discharge_missing; // fixed it
                                    continue; // take next
                                }
                            }
                        }

                        // Check if all (multiple>1) upstreams flows to this, and sum together.
                        // Needed to cover topology cases:
                        //  (a) R1..Rn .. flow to a junction, then to multiple units
                        //  (b) U1..Un .. tailrace to common downstream river?
                        for (auto const& hc : w->upstreams) {
                            if (auto us = hc.target_()) {
                                if (us->downstreams.size() == 1) {
                                    auto ts = _get_discharge(us);
                                    if (ts.size() == 0) { // this upstream is missing discharge
                                        computed_discharge = apoint_ts{}; // don't use this sum, wait for all upstreams to be computed, then revisit later for a complete sum
                                        break;
                                    }
                                    computed_discharge = computed_discharge.ts ? computed_discharge + ts : ts;
                                } else { // this upstream is a split
                                    computed_discharge = apoint_ts{}; // don't compute this one
                                    break;
                                }
                            } // ignore null ptrs
                        }
                        if (computed_discharge.size() != 0) {
                            w->discharge.result = computed_discharge.evaluate(); // We know from above computed_discharge.size()>0
                            --n_wtr_discharge_missing; // fixed it
                        }
                    }
                }
            }
            if (n_wtr_discharge_missing == 0)
                break; // we are done
        }
    }
}
}

/**
 * Update stm system, performing post-process calculations on STM system
 * based on collected results from Shop.
 */
void shop_emitter::complete(stm_system& stm) {
    // Unit group sum expressions
    for(const auto&g : stm.unit_groups) {
        g->update_sum_expressions();
    }
    // Waterway discharges
    compute_missing_waterway_discharges(stm);
}

}
