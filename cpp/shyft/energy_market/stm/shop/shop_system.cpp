#include <memory>
#include <cstdint>
#include <cmath>
#ifndef _WIN32
#include <unistd.h> // environ
#endif
#include <cstdlib>
#include <string>
#include <stdexcept>
#include <filesystem> // current_path
#include <thread>
#include <shyft/energy_market/stm/shop/shop_system.h>

namespace shyft::energy_market::stm::shop {


shop_system::shop_system(const shyft::time_axis::generic_dt& ta, stm_system_context* ctx, string prefix)
    : api{}, adapter{api, ta}, emitter{api, adapter}, commander{api}, vis{}, time_axis{ta} {
    if (time_axis.size() == 0)
        throw runtime_error("time_axis.size() == 0");
    set_time_axis(api, time_axis);
    if (!std::getenv("ICC_COMMAND_PATH")) {
        // Shop API v0.3.0 needs to be told path to solver interface library from either API function ShopAddDllPath
        // or env.var. ICC_COMMAND_PATH (but core solver library can be in PATH, application directory, working directory etc).
        // Any license file must be either in working directory or path given by env.var. ICC_COMMAND_PATH (but ShopAddDllPath
        // is not being considered).
        // Here we default to load solver interface library from current working directory, when ICC_COMMAND_PATH is not set,
        // so that solver interface library file and the license file both must be in path given by either of these.
        api.set_library_path(std::filesystem::current_path().string().c_str());
        //std::cerr << "Using current path: " << std::filesystem::current_path().string() << std::endl;
    } else {
        //std::cerr << "Using ICC_COMMAND_PATH: " << std::getenv("ICC_COMMAND_PATH") << std::endl;
    }
    if (ctx) {
        vis = std::make_shared<shop_visitor>(*ctx, prefix);
        vis->update_run_time_axis(time_axis);
    }
    switch (shyft::core::to_seconds64(adapter.time_delay_unit)) {
    case 0: commander.set_time_delay_unit_time_step_length(); break; // This is the default in Shop if we don't set anything
    case 60: commander.set_time_delay_unit_minute(); break;
    case 3600: commander.set_time_delay_unit_hour(); break;
    default: throw runtime_error("invalid time_delay_unit");
    }
}

shop_system::~shop_system() {
    uninstall_logger();
}

// private static utility
void shop_system::set_time_axis(shop_api& api, const utcperiod& period, const utctimespan& t_step) {
    api.set_time_axis(to_seconds64(period.start), to_seconds64(period.end), to_seconds64(t_step));
}

// private static utility
void shop_system::set_time_axis(shop_api& api, const shyft::time_axis::generic_dt& time_axis) {
    vector<time_t> t_axis; t_axis.reserve(time_axis.size());
    if (time_axis.gt == shyft::time_axis::generic_dt::POINT) {
        std::transform(std::cbegin(time_axis.p.t), std::cend(time_axis.p.t), std::back_inserter(t_axis), to_seconds64);
        t_axis.push_back(to_seconds64(time_axis.p.t_end));
    } else {
        auto point_axis = shyft::time_axis::convert_to_point_dt(time_axis);
        std::transform(std::cbegin(point_axis.t), std::cend(point_axis.t), std::back_inserter(t_axis), to_seconds64);
        t_axis.push_back(to_seconds64(point_axis.t_end));
    }
    api.set_time_axis(t_axis);
}

void shop_system::environment(std::ostream& out) {
    for (char** the_environ = environ; *the_environ; the_environ++)
        out << *the_environ << std::endl;
}

void shop_system::install_logger(std::shared_ptr<shop_logger> alogger) {
    logger = std::move(alogger);
    shop_log_hook::info = std::bind(&shop_logger::info, logger, std::placeholders::_1);
    shop_log_hook::warning = std::bind(&shop_logger::warning, logger, std::placeholders::_1);
    shop_log_hook::error = std::bind(&shop_logger::error, logger, std::placeholders::_1);
    shop_log_hook::exit = std::bind(&shop_logger::exit, logger);
}
void shop_system::uninstall_logger() {
    shop_log_hook::info = nullptr;
    shop_log_hook::warning = nullptr;
    shop_log_hook::error = nullptr;
    shop_log_hook::exit = nullptr;
    logger.reset();
}

void shop_system::emit(const stm_system& stm) {
    emitter.to_shop(stm);
}
void shop_system::collect(stm_system& stm) {
    emitter.from_shop(stm);
}
void shop_system::complete(stm_system& stm) {
    emitter.complete(stm);
}
void shop_system::command(std::vector<shop_command> const& commands) {
    std::atomic_bool collect_logs{true};
    auto log_collector=[&]() {
        shyft::core::utctime poll_interval{shyft::core::from_seconds(0.005)};
        while(collect_logs) {
            auto log=get_log_buffer();
            if(log.size()) {
                if(vis) vis->add_shop_log(log);
            } else
                std::this_thread::sleep_for(poll_interval);
        }
    };
    auto log_worker= std::async(log_collector);
    try {
        for (const auto& cmd : commands) {
            commander.execute(cmd);
            if (vis) {
                // We update run parameters based on the command executed
                vis->update_by_command(cmd);
                // done in separate thread:Add new shop log entries
                //vis->add_shop_log(get_log_buffer());
            }
        }
        collect_logs=false;
        log_worker.wait();
    } catch(std::exception &se) {
        //log_severity severity, std::string message, int code, shyft::core::utctime time
        shop_log_entry x{shop_log_entry::log_severity::error,std::string("command raised exception:")+se.what(),0,::shyft::core::utctime_now()};
        collect_logs=false;
        log_worker.wait();
        if(vis) {
            vector<shop_log_entry> logs;logs.push_back(x);
            vis->add_shop_log(logs); //add another log..
        } else {
            throw std::runtime_error(x.message);
        }
    }
}

// Get known default objects.
// There are a few objects that are always present in Shop:
//   - Object "S1" (0) of type "scenario" (19) - default object, additional objects may
//     be added, only relevant when running the stochastic version of SHOP (SHARM).
//   - Object "average_objective" (1) of type "objective" (20) - default object, one
//     additional object will automatically be created for each created scenario object
//     when running the stochastic version of SHOP (SHARM).
//   - Object "global_settings" (2) of type global_settings (13) - singleton object.
//   - Object "lp_model" (3) of type "lp_model" (29) - singleton object?
//   - Object "system" (4) of type "system" (24) - singleton object?
// Below are dedicated getters for the relevant ones. Lookup up the object by name,
// instead of relying on the current object index as noted above, as that is more
// subject to change.

shop_objective shop_system::get_average_objective() {
    return api.get<shop_objective>("average_objective");
}

shop_global_settings shop_system::get_global_settings() {
    return api.get<shop_global_settings>("global_settings");
}

void shop_system::export_topology(bool all , bool raw, std::ostream& destination) const {
    shop_export::export_topology(api.c, all, raw, destination);
}
void shop_system::export_data(bool all, std::ostream& destination) const {
    shop_export::export_data(api.c, all, destination);
}

// static utility and ease of use functions
void shop_system::optimize(stm_system& stm, const generic_dt& time_axis, const std::vector<shop_command>& commands, bool logging_to_stdstreams, bool logging_to_files, string prefix) {
    shop_system shop{ time_axis, nullptr, prefix };
    // Had to remove use of shop-visitor from this scope, as it needs stm_system_context.
    //shop.vis->update_run_time_axis(shop.time_axis);
    shop.set_logging_to_stdstreams(logging_to_stdstreams);
    shop.set_logging_to_files(logging_to_files);
    shop.emit(stm);
    shop.command(commands);
    shop.collect(stm);
    shop.complete(stm); // forced post-processing (could be optional/argument?)
}

}
