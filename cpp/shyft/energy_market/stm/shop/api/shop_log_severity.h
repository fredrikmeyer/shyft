#pragma once
namespace shop::data {

enum log_severity {
    information,
    diagnosis_information,
    warning,
    diagnosis_warning,
    error,
    diagnosis_error
};

}
