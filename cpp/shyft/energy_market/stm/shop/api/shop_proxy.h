#pragma once
#include <memory>
#include <cstring>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <chrono>
#include <limits>
#include <stdexcept>
#include <mutex>
#include <type_traits>
#include <shop_lib_interface.h>
#include "shop_log_severity.h"

namespace shop::proxy {

/**@brief shop::proxy:unit namespace
 *
 * Contains basic definition of value units used by shop attributes,
 * and functions for converting scaled units to and from its base,
 * which is generally the SI base unit.
 */
namespace unit
{
    struct exp10_scale {
        template<typename V> static constexpr V scale(V v, int e) {
            return e == 0 ? v : e < 0 ? static_cast<V>(1) / 10 * scale(v, e + 1) : static_cast<V>(10) * scale(v, e - 1);
        }
    };
    struct exp10_unit_exponent {
        static constexpr int nano = -9;
        static constexpr int micro = -6;
        static constexpr int milli = -3;
        static constexpr int base = 0;
        static constexpr int kilo = 3;
        static constexpr int mega = 6;
        static constexpr int giga = 9;
    };
    template<int exp = exp10_unit_exponent::base>
    struct exp10_scaled_unit {
        static constexpr bool is_base = exp == exp10_unit_exponent::base;
        template<typename V> static constexpr V to_base(V v) { return exp10_scale::scale(v, exp); };
        template<typename V> static constexpr V from_base(V v) { return exp10_scale::scale(v, -exp); };
    };

    using base_unit = exp10_scaled_unit<exp10_unit_exponent::base>;
    using mega_unit = exp10_scaled_unit<exp10_unit_exponent::mega>;

    using no_unit = base_unit;
    using invalid = base_unit; // There should not be any attributes with this unit, but sometimes there is because the real unit has not yet been added to shop
    using percent = base_unit;
    using meter = base_unit;
    using delta_meter = base_unit;
    using second = base_unit;
    using hour = base_unit;
    using nok = base_unit;
    using nok_per_h_per_m3_per_s = base_unit;
    using nok_per_m3_per_s = base_unit;
    using nok_per_meter = base_unit;
    using s2_per_m5 = base_unit;
    using m3_per_s = base_unit;
    using m3sec_hour = base_unit;
    using mwh_per_mm3 = base_unit;
	using m3sec_hour = base_unit;
	using meter_per_hour = base_unit;
	using nok_per_meter_hour = base_unit;

    using mw = mega_unit;
    using mm3 = mega_unit;
    using mwh = mega_unit;
    using mw_hour = mega_unit;

    using km2 = mega_unit;

    using kwh_per_mm3 = exp10_scaled_unit<exp10_unit_exponent::kilo - exp10_unit_exponent::mega>;
    using nok_per_mm3 = exp10_scaled_unit<-exp10_unit_exponent::mega>;
    using nok_per_mm3h = exp10_scaled_unit<-exp10_unit_exponent::mega>;
    using nok_per_mw = exp10_scaled_unit<-exp10_unit_exponent::mega>;
    using nok_per_mwh = exp10_scaled_unit<-exp10_unit_exponent::mega>;
}

/** @brief shop proxy object
*
* This class represents a shop object by proxy,
* using a reference to the shop-lib handle
* @tparam S shop handle, holding any operations that we would like to invoke
* @tparam T native shop type index for this object
*
*/
template <class S, int T>
struct obj {
    static constexpr int t_id = T;///< the shop object type id
    obj() = default;
    obj(S* s, int oid) :s{ s }, id{ oid }{}
    S* s;///< upward reference to the shop-sys (proxy for the lib handle)
    int id;///< the shop object index, as reported when object is created
};

/**
 * @brief shop object proxy attribute
 * 
 * This base class implements read-only access. Some attributes are strictly
 * read-only, e.g. constants and results, and these are intances of this
 * class only. Other attributes are also writable, e.g. input, and are then
 * instances of subclass rw extending with write capabilities.
 *
 * Keeeping enough references to act as an ordinary proxy for the attribute.
 * allowing syntax like:
 *     reservoir.hrl=123.0
 * or:
 *     if (reservoir.hrl.exists()) ...
 *
 * @tparam O the parent class where the requirement is that p->s has:
 *             .get(oid,aid,V*) -> V
 *             .exists(oid,aid,V*) -> bool
 *             .is_default(oid,aid,V*) -> bool
 * @tparam a the attribute id as used by native shop-interface
 * @tparam V the value-type of the attribute
 * @see rw
 */
template <class O, int a, class V, class Ux, class Uy>
struct ro {
    using value_t = V;///< the value type, nice to have
    static constexpr int aid = a;///< the attribute id, as required by the c-api
    using x_unit = Ux;
    using y_unit = Uy;
    O* p;///< reference to the owning object
    ro(O* p) : p(p) {}
    bool exists() const { return p->s->exists(p->id, aid, (V*)nullptr); } /// probably not relevant for read only attribute?
    bool is_default() const { return p->s->is_default(p->id, aid, (V*)nullptr); } /// probably not relevant for read only attribute?
    V get() const { return p->s->template get<Ux, Uy>(p->id, aid, (V*)nullptr); } // pass a V* to help compiler select right return type
    operator V() const { return get(); } // candy to allow cast syntax to extract value
};

/**
 * @brief shop object proxy attribute that are writable (input)
 *
 * @tparam O the parent class where the requirement is that p->s has:
 *             .get(oid,aid,V*) -> V
 *             .exists(oid,aid,V*) -> bool
 *             .is_default(oid,aid,V*) -> bool
 *             .set(oid,aid,v)
 * @tparam a the attribute id as used by native shop-interface
 * @tparam V the value-type of the attribute
 * @see ro
 */
template <class O, int a, class V, class Ux, class Uy>
struct rw : ro<O, a, V, Ux, Uy> {
    rw(O* p) : ro<O, a, V, Ux, Uy>(p) {}
    void set(V v) { this->p->s->template set<Ux, Uy>(this->p->id, this->aid, v); }
    rw& operator=(const V& v) { set(v); return *this; } // candy to allow syntax r.flow_desc= ... and xy fd=r.flow_descr
};

} // shop::proxy

/**
 * @brief shop::date namespace
 *
 * contains the c++ bare-bone *SAFE* representation of the c-types used by
 * the shop-interace library.
 *
 * The user of this library provide his type T, and with a factory in the
 * shop::data namespace which can:
 *
 *  T create(basic_args...)
 *  shop_object convert_to_shop(const T& user_object)
 */
namespace shop::data {

using std::unique_ptr;
using std::make_unique;
using std::move;
using std::string_view;
using std::string;
using std::vector;
using std::runtime_error;
using std::to_string;

using shop_time_str         = char[  18]; // date/time, format "YYYYMMDDHHMMSSxxx", which is 17 characters + 1 null terminator
using shop_time_unit_str    = char[   7]; // time unit, currently longest values are "minute" and "second"
using shop_name_str         = char[ 100]; // object name, qualified guessed size based on testing
//using shop_type_str       = char[  19]; // type names, currently longest value is "needle_combination"
//using shop_attr_str       = char[  31]; // attribute name, currently longest value is "tailrace_loss_from_bypass_flag" (attribute of plant)
using shop_relation_str     = char[  32]; // relation, currently longest value is "needle_combination_of_generator"
using shop_cmd_str          = char[1024]; // full command string, guessed size
using shop_cmd_opt_str      = char[  21]; // option argument of commands, currently longest value is "overflow_time_adjust"
using shop_cmd_obj_str      = char[ 260]; // object argument of commands, qualified guessed size based on testing, and since value can be file path a value matching MAX_PATH should be allowed
using shop_log_message_str  = char[1024]; // log entry message, maximum size returned by ShopGetProgress according to sintef
using shop_log_severity_str = char[  32]; // log entry severity, maximum size returned by ShopGetProgress according to sintef
using shop_attr_value_str   = char[1024]; // value of attributes of type string, maximum theoretical size returned by ShopGetStringAttribute according to sintef (in reality all values are shorter, longest is file path up to 203 chars)

/**
 * @brief list of ptr util
 *
 * @details Many of shop api calls take char*[n] type of structures,
 * so we make this wrapper to provide a view into
 * a vector of type C, C like string_view or string,
 * that is supposed to have a lifetime that exceeds the call to shop,
 * e.g.: shop can take copy of those.
 */
struct list_of_str_ptr {
    template<class C>
    list_of_str_ptr(std::vector<C> const& x, size_t max_len) {
        rep.reserve(x.size());
        for(auto const &i:x) {
            if ( i.size()>=max_len) {
                throw std::runtime_error("shop-api: size of argument/object"+std::to_string(i.size())+" is longer than allowed max limit"+std::to_string(max_len));
            }
            rep.push_back(const_cast<char*>(i.data())); //cast away const, since the interfaces occasionally takes const char **
        }
    }
    char ** get() { return rep.data();} // yes, would be nice to switch to char const * const*
    size_t size() const {return rep.size();}
    private:
    std::vector<char*> rep;/// a simple struct to keep the pointer refs, no ownership for the refs.
};


struct shop_time_unit {
    static constexpr shop_time_unit_str second{ "second" };
    static constexpr shop_time_unit_str minute{ "minute" };
    static constexpr shop_time_unit_str hour{ "hour" };
    static constexpr shop_time_unit_str day{ "day" };
    static constexpr shop_time_unit_str week{ "week" };
    static constexpr shop_time_unit_str month{ "month" };
    static constexpr shop_time_unit_str year{ "year" };
    static auto from_time_t(time_t s) {
        switch (s) {
        case 3600LL:            return shop_time_unit::hour;
        case 60LL:              return shop_time_unit::minute;
        case 1LL:               return shop_time_unit::second;
        case 3600 * 24LL:       return shop_time_unit::day;
        case 7 * 3600 * 24LL:   return shop_time_unit::week;
        case 30 * 3600 * 24LL:  return shop_time_unit::month;
        case 365 * 3600 * 24LL: return shop_time_unit::year;
        }
        throw runtime_error("shop::data unsupported time unit" + to_string(int(s)));
    }
};

static constexpr time_t shop_time_resolution_unit{ 60 }; // Unit in number of seconds that relative time integer values in Shop should represents. Must match one of the shop_time_unit constants. Minute is minimum, as of shop.lib version 0.1.1 (2018.12.12) using second leads to crash.

struct shop_relation {
    //static constexpr shop_relation_str generator_of_plant{ "generator_of_plant" }; // Note: Deprecated in Shop v14, should use connection_standard instead.
    //static constexpr shop_relation_str pump_of_plant{ "pump_of_plant" }; // Note: Deprecated in Shop v14, should use connection_standard instead.
    static constexpr shop_relation_str needle_combination_of_generator{ "needle_combination_of_generator" };
    static constexpr shop_relation_str main{ "connection_standard" };
    static constexpr shop_relation_str spill{ "connection_spill" };
    static constexpr shop_relation_str bypass{ "connection_bypass" };
};

/** shop_time, incredible 17c text representation */

//--NOTE!!
//-- from shyft.core.utctime_utiles.h to keep it minimal (use howard hinnant)
namespace time_util {
    static constexpr const int64_t UnixDay = 2440588;///< Calc::julian_day_number(ymd(1970,01,01));
    static constexpr const int64_t UnixSecond = 86400LL * UnixDay;///<Calc::julian_day_number(ymd(1970,01,01));
    struct YMDhms { int year, month, day, hour, second, minute; };
    // Snapped from boost gregorian_calendar.ipp
    inline unsigned long day_number(const YMDhms& ymd) noexcept {
        unsigned short a = static_cast<unsigned short>((14 - ymd.month) / 12);
        unsigned short y = static_cast<unsigned short>(ymd.year + 4800 - a);
        unsigned short m = static_cast<unsigned short>(ymd.month + 12 * a - 3);
        unsigned long  d = ymd.day + ((153 * m + 2) / 5) + 365 * y + (y / 4) - (y / 100) + (y / 400) - 32045;
        return d;
    }
    inline YMDhms from_day_number(unsigned long dayNumber) noexcept {
        int a = dayNumber + 32044;
        int b = (4 * a + 3) / 146097;
        int c = a - ((146097 * b) / 4);
        int d = (4 * c + 3) / 1461;
        int e = c - (1461 * d) / 4;
        int m = (5 * e + 2) / 153;
        unsigned short day = static_cast<unsigned short>(e - ((153 * m + 2) / 5) + 1);
        unsigned short month = static_cast<unsigned short>(m + 3 - 12 * (m / 10));
        int year = static_cast<unsigned short>(100 * b + d - 4800 + (m / 10));
        return YMDhms{ year, month, day, 0,0,0 };
    }
    inline int64_t day_number(time_t t) noexcept {
        return (int64_t)((UnixSecond + std::chrono::duration_cast<std::chrono::seconds>(std::chrono::seconds(t)).count()) / std::chrono::duration_cast<std::chrono::seconds>(std::chrono::seconds(3600 * 24)).count());
    }
    inline YMDhms& fill_in_hms_from_t(time_t& t_, YMDhms& r) {
        auto t = std::chrono::seconds(t_);
        long long ts = std::chrono::duration_cast<std::chrono::seconds>(t).count();
        long long tj = UnixSecond + ts;
        long long td = 86400LL * (tj / 86400LL);
        long long dx = tj - td;// n seconds this day.
        r.hour = int(dx / 3600);
        dx -= r.hour * 3600LL;
        r.minute = int(dx / 60);
        r.second = int(dx % 60);
        return r;
    }
    static inline YMDhms calendar_units(time_t t) {
        YMDhms r;
        fill_in_hms_from_t(t, r);// might modify t
        auto jdn = day_number(t);
        auto x = from_day_number((unsigned long)jdn);
        r.year = x.year; r.month = x.month; r.day = x.day;
        return r;
    }
    inline time_t hms_seconds(int h, int m, int s) { return h * 3600LL + m * 60LL + s; }
    inline time_t time_from_calendar_units(const YMDhms&c) {
        return ((day_number(c) - UnixDay) * 86400LL) + hms_seconds(c.hour, c.minute, c.second);
    }

} //-- end details from shyft_core.


struct shop_log_entry {
    time_t time;
    log_severity severity;
    int code;
    shop_log_message_str message;
    shop_log_entry(time_t t, string_view s, int c, string_view m) : time{t}, code{c}, message{} {
        if (s == "OK") {
            severity = information;
        } else if (s == "Diagnosis OK") {
            severity = diagnosis_information;
        } else if (s == "Warning") {
            severity = warning;
        } else if (s == "Diagnosis warning") {
            severity = diagnosis_warning;
        } else if (s == "Error") {
            severity = error;
        } else if (s == "Diagnosis error") {
            severity = diagnosis_error;
        } else {
             throw runtime_error(string("unexpected severity string: ") + string(s));
        }
        if (m.size() > sizeof(message)) throw runtime_error("message string too long");
        for (size_t i = 0; i < m.size(); ++i) message[i] = m[i];
    }
};

struct shop_time {//012345678901234570
    char rep[18];// YYYYMMDDhhmmssxxx0
    shop_time(const char*x,bool check_it) {
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstringop-truncation"
#endif
        strncpy(rep, x, sizeof(rep)); rep[17] = 0;
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
        if(check_it)
            (void) operator time_t();//force check
    }
    explicit shop_time(time_t x) {
        auto c = time_util::calendar_units(x);
#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-overflow"
#endif
        sprintf(rep, "%04d%02d%02d%02d%02d%02d000", c.year, c.month, c.day, c.hour, c.minute, c.second);
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif
    }
    explicit operator time_t() const {
        time_util::YMDhms c;
        if (sscanf(rep, "%04d%02d%02d%02d%02d%02d", &c.year, &c.month, &c.day, &c.hour, &c.minute, &c.second) != 6)
            throw runtime_error("shop_time.rep invalid format:" + string(rep));
        return time_util::time_from_calendar_units(c);
    }
    operator char*() const { return (char*)(&rep[0]); }

    bool operator==(const shop_time&o) const { return strcmp(rep, o.rep) == 0; }
    bool operator!=(const shop_time&o) const { return !operator==(o); }
};

/** bare-bone but safe rep of shop XY type */
struct XY {
    double ref;
    /*size_t*/ int n; // shop_lib xy uses int's for everything
    unique_ptr<double[]> x;
    unique_ptr<double[]> y;

    XY(double r, int n, unique_ptr<double[]> x, unique_ptr<double[]> y) : ref(r), n(n), x(move(x)), y(move(y)) {}

    //-disallow most operations
    XY() = delete;
    XY(const XY&) = delete;
    XY& operator=(const XY&) = delete;
    // move is ok
    XY(XY&&o) : ref(o.ref), n(o.n), x(move(o.x)), y(move(o.y)) {}
    XY& operator=(XY&&o) {
        ref = o.ref;
        n = o.n;
        x = move(o.x);
        y = move(o.y);
        return *this;
    }
};

/** factory to create XY types
 *
 * User must implement method for creating its internal type from basic types,
 * and for converting it to shop representation. Templated on user's type T and
 * the units Ux and Uy that shop interprets the values as.
 */
template <class T, class Ux, class Uy>
struct xy_factory {
    static T create(double, size_t, unique_ptr<double[]>, unique_ptr<double[]>) {
        //static_assert(false, "You must provide 'yourtype xy_factory<your_type>.create(...)', see messages below to identify your type");
        throw runtime_error("You must provide 'yourtype xy_factory<your_type>.create(...)', see messages below to identify your type");
        //return T{};
    }
    static XY convert_to_shop(const T&) {
        //static_assert(false, "You must provide 'XY xy_factory<your_type>::convert_to_shop(const your_type&)', see the messages below for detailed type information");
        throw runtime_error("You must provide 'XY xy_factory<your_type>::convert_to_shop(const your_type&)', see the messages below for detailed type information");
        //return XY{};
    }
};

/** bare-bone but safe rep of shop TXY type */
struct TXY {
    shop_time start;// very special.. is there requirement for start related to t's below ?
    int n; // shop_lib xy uses int's for everything
    unique_ptr<int[]> t;
    unique_ptr<double[]> y;

    TXY(shop_time start, int n, unique_ptr<int[]> t, unique_ptr<double[]> y) : start(move(start)), n(n), t(move(t)), y(move(y)) {}

    //-disallow most operations
    TXY() = delete;
    TXY(const TXY&) = delete;
    TXY& operator=(const TXY&) = delete;
    // move is ok
    TXY(TXY&&o) : start(move(o.start)), n(o.n), t(move(o.t)), y(move(o.y)) {}
    TXY& operator=(TXY&&o) {
        start = move(o.start);
        n = o.n;
        t = move(o.t);
        y = move(o.y);
        return *this;
    }
};

template<class T>
struct shop_convertible {
    static constexpr bool value=false;
};

/** factory to create TXY types
 *
 * User must implement method for creating its internal type from basic types,
 * and for converting it to shop representation. Templated on user's type T and
 * the unit U that shop interprets the values as.
 */
template <class T, class U>
struct txy_factory {
    //-- these static members must be provided by foreign txy representation
    static T create(const vector<time_t>&, size_t, unique_ptr<int[]>, unique_ptr<double[]>) {
        //static_assert(false, "You must provide 'yourtype txy_factory::create<your_type>(...)', see messages below to identify your type");
        throw runtime_error("You must provide 'yourtype txy_factory::create<your_type>(...)', see messages below to identify your type");
        //return T{};
    }
    static TXY convert_to_shop(const T&, const vector<time_t>&) {
        static_assert(shop_convertible<T>::value, "You must provide 'TXY txy_factory<your_type>::convert_to_shop(const your_type&)', see the messages below for detailed type information");
        throw runtime_error("You must provide 'TXY txy_factory<your_type>::convert_to_shop(const your_type&)', see the messages below for detailed type information");
        //return TXY{};
    }
};

}

namespace shop {

using std::string_view;
using std::string;
using std::runtime_error;
using std::move;
using std::vector;
using std::map;
using std::to_string;
using std::unique_ptr;
using std::make_unique;
using contxt = ShopSystem*; // handle for the shop system
using std::recursive_mutex;
using std::unique_lock;
using std::numeric_limits;
enum class connection {
    main,
    bypass,
    flood,
};

/** Seems like shop-api ShopInit calls are not thread-safe, so we need something pr. process protection to avoid trouble */
inline recursive_mutex& api_mx() {
    static recursive_mutex global_api_mx;
    return global_api_mx;
}

/**@brief shop::api that deals with all c-style calls
*
* The user supplies the types of his 'basic' data-types
* to this class.
*
* It require that the user-class have appropriate wrappers/converters
* defined in the shop::data namespace
*
* @tparam _XY is the user-space data-type for shop xy type structures {reference, + list of points(x,y)}, shop::data should have xy xy_factory::create and XY xy_factory::convert_to_shop
*
* TODO: add _TXY (and the vector variants, could be derived from those?), ommit basic types like double,int, string etc.
*
*/
template <class _XY, class _TXY>
struct api {
    using _xy = _XY;
    using _txy = _TXY;
    mutable recursive_mutex c_mx;///< to enforce that only one call are in flight for a specific contxt c.
    contxt c;///< only external member, just the context shop_lib handle
    vector<time_t> time_axis;
    bool time_axis_defined{ false }; // a lot of stuff is not possible (like in crash), unless time-axis is set first

    api() {
        unique_lock _(api_mx()); // global needed because of problem inside shop-api/license-file init ++
        c = ShopInit();
    }
    ~api() {
        unique_lock _(api_mx());// global needed because of problem inside shop-api/license-file init ++
        if (c)
            (void)ShopFree(c); // ignore results
    }

    api(const api&) = delete; // disallow copy (keep one!)
    api& operator=(const api&) = delete;

    // allow move (still one copy)
    api(api&&o):c(move(o.c)), time_axis(move(o.time_axis)), time_axis_defined(move(o.time_axis_defined)) {  o.c = nullptr; }
    api& operator=(api&&o) {
        c = move(o.c);
        o.c = nullptr;
        time_axis = move(o.time_axis);
        time_axis_defined = move(o.time_axis_defined);
        return *this;
    }
    //--
    string get_version_info() {
        unique_lock _(c_mx);
        int len = 0;
        if (ShopGetVersionInfo(c, nullptr, len)) {
            vector<char> res;
            res.reserve(len);
            if (ShopGetVersionInfo(c, res.data(), len)) {
                return string{res.data()};
            }
        }
        return string{};
    }
    void set_library_path(string_view path) {
        unique_lock _(c_mx);
        ShopAddDllPath(c, string(path).c_str());
    }
    void set_logging_to_stdstreams(bool on = true) { // enable (or disable) output of messages on stdout/stderr from shop api (disabled by default)
        unique_lock _(c_mx);
        ShopSetSilentConsole(c, !on);
    }
    void set_logging_to_files(bool on = true) { // enable (or disable) writing to log files from shop api (disabled by default)
        unique_lock _(c_mx);
        ShopSetSilentLog(c, !on);
    }
    vector<data::shop_log_entry> get_log_buffer(int limit = 0) {
        //unique_lock _(c_mx); // ref issue https://gitlab.com/shyft-os/shyft/-/issues/869 , this blocks the control flow
        if (limit <= 0 || limit > 1024) limit = 1024; // maximum relevant limit due to internal buffer size in shop api
        int n = 0;
        auto percent_list = make_unique<int[]>(limit);
        auto timestamp_list = make_unique<int[]>(limit);
        struct slm{data::shop_log_message_str m; };
        struct sls{data::shop_log_severity_str s;};
        vector<slm> message_list(limit);
        vector<sls> severity_list(limit);

        auto v_message_list = make_unique<char*[]>(limit);
        auto v_severity_list = make_unique<char*[]>(limit);
        for (size_t i = 0; i < size_t(limit); ++i) {
            v_message_list[i] = message_list[i].m;
            v_severity_list[i] = severity_list[i].s;
        }
        auto code_list = make_unique<int[]>(limit);
        vector<data::shop_log_entry> r;
        if (ShopGetProgress(c, n, v_message_list.get(), percent_list.get(), timestamp_list.get(), v_severity_list.get(), code_list.get(), limit)) {
            r.reserve(n);
            for (size_t i = 0; i < size_t(n); ++i) {
                r.emplace_back(timestamp_list[i], severity_list[i].s, code_list[i], message_list[i].m);
            }
        }
        return r;
    }
    void set_time_axis(time_t t_begin, time_t t_end, time_t t_step) {
        unique_lock _(c_mx);
        if (t_begin >= t_end)
            throw runtime_error("set_time_axis called with t_begin >= t_end");
        if (t_step <= 0)
            throw runtime_error("set_time_axis called with t_step <= 0");
        auto n_steps{ (t_end - t_begin) / t_step };
        if (n_steps <= 0)
            throw runtime_error("set_time_axis called with n_steps <= 0");
        vector<time_t> t_axis;
        t_axis.reserve(n_steps);
        for (auto t = t_begin; t <= t_end; t += t_step) {
            t_axis.push_back(t);
        }
        set_time_axis(t_axis);
    }
    void set_time_axis(const vector<time_t>& t_axis) {
        unique_lock _(c_mx);
        using data::TXY;
        if (t_axis.size() < 2)
            throw runtime_error(string("set_time_axis called with ") + to_string(t_axis.size()) + string(" point(s), must be at least 2"));
        auto ti{ t_axis.cbegin() };
        time_t t{ *ti++ };
        time_t dt{ *ti - t };
        if (dt <= 0)
            throw runtime_error("set_time_axis called with t_axis that is not increasing");
        const auto t_first{ t };
        const auto ti_last{ t_axis.cend() - 1 };
        vector<int> steps_t{ 0 };
        vector<double> steps_l{ (double)(dt/data::shop_time_resolution_unit) };
        time_t prev_dt{ dt };
        while (ti < ti_last) {
            t = *ti++;
            dt = *ti - t;
            if (dt != prev_dt) {
                if (dt <= 0)
                    throw runtime_error("set_time_axis called with t_axis that is not increasing");
                steps_t.push_back((int)((t - t_first) / data::shop_time_resolution_unit));
                steps_l.push_back((double)(dt/data::shop_time_resolution_unit));
                prev_dt = dt;
            }
        }
        data::shop_time start_time{ t_first };
        data::shop_time end_time{ *ti_last };
        //const auto n_steps{ t_axis.size() - 1 };
        if (!ShopSetTimeResolution(c, start_time, end_time, data::shop_time_unit::from_time_t(data::shop_time_resolution_unit), (int)steps_t.size(), const_cast<int*>(steps_t.data()), const_cast<double*>(steps_l.data())))
            throw runtime_error("failed to set time resolution");
        time_axis = t_axis;
        time_axis_defined = true;
    }
    void execute_cmd(string_view cmd, string_view opt = "", string_view obj = "") {
        unique_lock _(c_mx);
        data::shop_cmd_opt_str opt_;
        data::shop_cmd_obj_str obj_;
        if (opt.size() >= sizeof(opt_))
            throw runtime_error("execute_cmd called with too long option argument: " + string(opt));
        if (obj.size() >= sizeof(obj_))
            throw runtime_error("execute_cmd called with too long object argument: " + string(obj));
        size_t i;
        for (i = 0; i < opt.size(); ++i) {opt_[i] = opt[i];} opt_[i] = 0;
        int n_opt = i > 0;
        for (i = 0; i < obj.size(); ++i) {obj_[i] = obj[i];} obj_[i] = 0;
        int n_obj = i > 0;
        char *opt_list[] = {opt_};
        char *obj_list[] = {obj_};
        if (!ShopExecuteCommand(c, string(cmd).c_str(), n_opt, opt_list, n_obj, obj_list))
            throw runtime_error("failed to execute shop command: " + string(cmd) + (opt.empty()?string(""):string(" /")+string(opt)) + (obj.empty()?string(""):string(" ")+string(obj)));
    }
    void execute_cmd(string_view cmd, vector<string_view> opt, vector<string_view> obj = {}) {
        unique_lock _(c_mx);
        constexpr const auto opt_sz = sizeof(data::shop_cmd_opt_str);
        constexpr const auto obj_sz = sizeof(data::shop_cmd_obj_str);
        data::list_of_str_ptr v_opt_list(opt, opt_sz);
        data::list_of_str_ptr v_obj_list(obj, obj_sz);
        auto n_opt = opt.size();
        auto n_obj = obj.size();
        if (!ShopExecuteCommand(c, string(cmd).c_str(), n_opt, v_opt_list.get(), n_obj, v_obj_list.get())) {
            string s("failed to execute shop command: " + string(cmd));
            for (const auto& o : opt) s += string(" /") + string(o);
            for (const auto& o : obj) s += string(" ") + string(o);
            throw runtime_error(s);
        }
    }
    void execute_cmd_string(string_view cmd) {
        unique_lock _(c_mx);
        if (!ShopExecuteCommand(c, string(cmd).c_str()))
            throw runtime_error("failed to execute shop command: " + string(cmd));
    }
    vector<string> get_executed_cmd_strings() {
        unique_lock _(c_mx);
        int limit = 100; // guessing this is enough
        int n = 0;
        struct scs {data::shop_cmd_str c;char * data() {return c;}};
        vector<scs> cmd_list(limit);
        auto v_cmd_list = make_unique<char*[]>(limit);
        for (size_t i = 0; i < size_t(limit); ++i)
            v_cmd_list[i] = cmd_list[i].data();
        if (!ShopGetExecutedCommands(c, n, v_cmd_list.get(), limit))
            throw runtime_error("failed to get shop commands");
        vector<string> r;
        r.reserve(n);
        for (size_t i = 0; i < size_t(n); ++i)
            r.emplace_back(cmd_list[i].c);
        return r;
    }
    void start_sim(string_view o) {
        unique_lock _(c_mx);
        data::shop_cmd_obj_str o_;
        if (o.size() >= sizeof(o_))
            throw runtime_error("start_sim called with too long argument: " + string(o));
        size_t i;
        for (i = 0; i < o.size(); ++i) {o_[i] = o[i];} o_[i] = 0;
        char *o_list[] = { o_ };
        const char* cmd{ "start sim" };
        if (!ShopExecuteCommand(c, cmd, 0, nullptr, 1, o_list))
            throw runtime_error("failed to execute shop command: " + string(cmd) + string(" ") + string(o));
    }

    string dump_yaml(bool input_only = false, bool compress_txy = false, bool compress_connection = false) {
        unique_lock _(c_mx);
        int n = 0;
        if (!ShopDumpYamlString(c, nullptr, n, input_only, compress_txy, compress_connection))
            throw runtime_error("failed to get yaml string allocation size");
        if (n < 1)
            return {};
        string r(n, '\0');
        if (!ShopDumpYamlString(c, r.data(), n, input_only, compress_txy, compress_connection))
            throw runtime_error("failed to create yaml string");
        r.resize(n - 1);
        return r;
    }

    /** for diagnostics */
    string obj_ref_str(int oid) const {
        unique_lock _(c_mx);
        // SHOP API calls cause segfault if we ask for indices out of range.
        string obj_name = oid < ShopGetObjectCount(c) ? ShopGetObjectName(c, oid) : "INVALID ATTRIBUTE ID";
        string obj_type = oid < ShopGetObjectCount(c) ? ShopGetObjectType(c, oid) : "NO OBJECT TYPE";
        return "'" + obj_type + "[" +  to_string(oid) + ", " + obj_name + "]";
    }
    string attr_ref_str(int oid, int aid) const {
        unique_lock _(c_mx);
        // SHOP API calls cause segfault if we ask for indices out of range.
        string attr_name = aid < ShopGetAttributeCount(c) ? ShopGetAttributeName(c, aid) : "INVALID OBJECT ID";
        string obj_name = oid < ShopGetObjectCount(c) ? ShopGetObjectName(c, oid) : "INVALID ATTRIBUTE ID";
        string obj_type = oid < ShopGetObjectCount(c) ? ShopGetObjectType(c, oid) : "NO OBJECT TYPE";
        return "'" + obj_type + "[" +  to_string(oid) + ", " + obj_name + "].(" + to_string(aid) + ", " + attr_name + ")'";
    }

    //TODO: provide 'known' object as created with create object
    // either as tuple
    // tuple<vector<reservoir>,vector<..>>  obj;
    // get<vector<reservoir>>(obj)  ..
    // or explicit (22 types, that rarely grows):
    // vector<reservoir> reservoirs;
    // vector<plant> plants;

    template<class T>
    T create(string_view name) {
        auto oid = create_object(T::t_id, name);
        return T{ this, oid };
    }

    template<class T>
    T get(string_view name) {
        auto oid = get_object(T::t_id, name);
        return T{ this, oid };
    }

    int create_object(int otype, string_view name) {
        unique_lock _(c_mx);
        auto ix = ShopAddObject(c, otype, string(name).c_str());
        if (ix < 0)
            throw runtime_error("failed to create object of type " + to_string(otype) + " (name: " + string(name) + ")");
        return ix;
    }

    int get_object(int otype, string_view name) {
        unique_lock _(c_mx);
        auto typeName = ShopGetObjectTypeName(c, otype);
        auto ix = ShopGetObjectIndex(c, typeName, string(name).c_str());
        if(ix < 0)
            throw runtime_error("failed to get object of type " + to_string(otype) + " (name: " + string(name) + ")");
        return ix;
    }

    void connect_plant_generator(int plant_oid, int generator_oid) {
        _connect_objects(plant_oid, connection::main, generator_oid);
    }
    void connect_reservoir_plant(int reservoir_oid, connection role, int plant_oid) {
        _connect_objects(reservoir_oid, role, plant_oid);
    }
    void connect_reservoir_gate(int reservoir_oid, connection role, int gate_oid) {
        _connect_objects(reservoir_oid, role, gate_oid);
    }
    void connect_generator_needle_combination(int generator_oid, int needle_combination_id) {
        unique_lock _(c_mx);
        if (!ShopAddRelation(c, generator_oid, data::shop_relation::needle_combination_of_generator, needle_combination_id))
            throw runtime_error("failed to connect object " + obj_ref_str(generator_oid) + " and " + obj_ref_str(needle_combination_id) + " with role needle_combination_of_generator");
    }
    void _connect_objects(int oid1, connection role, int oid2) {
        unique_lock _(c_mx);
        switch (role) {
        case connection::main:
            if (!ShopAddRelation(c, oid1, data::shop_relation::main,  oid2))
                throw runtime_error("failed to connect object " + obj_ref_str(oid1) + " and " + obj_ref_str(oid2) + " with role main");
            break;
        case connection::bypass:
            if (!ShopAddRelation(c, oid1, data::shop_relation::bypass, oid2))
                throw runtime_error("failed to connect object " + obj_ref_str(oid1) + " and " + obj_ref_str(oid2) + " with role bypass");
            break;
        case connection::flood:
            if (!ShopAddRelation(c, oid1, data::shop_relation::spill,  oid2))
                throw runtime_error("failed to connect object " + obj_ref_str(oid1) + " and " + obj_ref_str(oid2) + " with role flood");
            break;
        default:
            throw runtime_error("failed to connect object " + obj_ref_str(oid1) + " and " + obj_ref_str(oid2) + " because of unsupported role");
        }
    }

    // section with .exists, .get , .set for all basic shop types

    // any-type
    template <class V>
    bool exists(int oid, int aid, V* /*_x0*/ = nullptr) {
        unique_lock _(c_mx);
        return ShopAttributeExists(c, oid, aid);
    }

    template <class V>
    bool is_default(int oid, int aid, V* /*_x0*/ = nullptr) {
        unique_lock _(c_mx);
        return ShopAttributeIsDefault(c, oid, aid);
    }

    // int
    template <class Ux, class Uy>
    int get(int oid, int aid, int* = nullptr) {
        unique_lock _(c_mx);
        int r = numeric_limits<int>::min();
        if (!ShopGetIntAttribute(c, oid, aid, r))
            throw runtime_error("int attr.get failed for: " + attr_ref_str(oid, aid));
        return Uy::to_base(r);
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, int v) {
        unique_lock _(c_mx);
        if(!ShopSetIntAttribute(c, oid, aid, Uy::from_base(v)))
            throw runtime_error("int attr.set failed for: " + attr_ref_str(oid, aid));

    }

    // vector<int>
    template <class Ux, class Uy>
    vector<int> get(int oid, int aid, vector<int>* = nullptr) {
        unique_lock _(c_mx);
        vector<int> r;
        int n{ 0 };
        if (!ShopGetDoubleArrayLength(c, oid, aid, n)) {
            throw runtime_error("vector<int> attr.get n failed for: " + attr_ref_str(oid, aid));
        }
        r.resize(n, numeric_limits<int>::min());
        if (!ShopGetIntArrayAttribute(c, oid, aid, n, r.data()))
            throw runtime_error("vector<int> attr.get failed for: " + attr_ref_str(oid, aid));
        for (auto& v : r)
            v = Uy::to_base(v);
        return r;
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, const vector<int>& v) {
        unique_lock _(c_mx);
        if (v.size() > numeric_limits<int>::max())
            throw runtime_error("vector<int> attr.set failed for: " + attr_ref_str(oid, aid));
        int n = static_cast<int>(v.size());
        if (!Uy::is_base) {
            vector<int> vs = v; // Need a copy to convert unit in
            for (auto& v : vs)
                v = Uy::from_base(v);
            if (!ShopSetIntArrayAttribute(c, oid, aid, n, const_cast<int*>(vs.data())))
                throw runtime_error("vector<int> attr.set failed for: " + attr_ref_str(oid, aid));
        }
        else {
            if (!ShopSetIntArrayAttribute(c, oid, aid, n, const_cast<int*>(v.data())))
                throw runtime_error("vector<int> attr.set failed for: " + attr_ref_str(oid, aid));
        }
    }

    // double
    template <class Ux, class Uy>
    double get(int oid, int aid, double* = nullptr) {
        unique_lock _(c_mx);
        double r = numeric_limits<double>::max();
        if (!ShopGetDoubleAttribute(c, oid, aid, r))
            throw runtime_error("double attr.get failed for: " + attr_ref_str(oid, aid));
        return Uy::to_base(r);
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, double v) {
        unique_lock _(c_mx);
        if (!ShopSetDoubleAttribute(c, oid, aid, Uy::from_base(v)))
            throw runtime_error("double attr.set failed for: " + attr_ref_str(oid, aid));
    }

    // vector<double>
    template <class Ux, class Uy>
    vector<double> get(int oid, int aid, vector<double>* = nullptr) {
        unique_lock _(c_mx);
        vector<double> r;
        int n{ 0 };
        if (!ShopGetDoubleArrayLength(c, oid, aid, n)) {
            throw runtime_error("vector<double> attr.get n failed for: " + attr_ref_str(oid, aid));
        }
        r.resize(n, numeric_limits<double>::max());// better nan ??
        if (!ShopGetDoubleArrayAttribute(c, oid, aid, n, r.data()))
            throw runtime_error("vector<double> attr.get failed for: " + attr_ref_str(oid, aid));
        for (auto& v : r)
            v = Uy::to_base(v);
        return r;
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, const vector<double>& v) {
        unique_lock _(c_mx);
        if (v.size() > numeric_limits<int>::max())
            throw runtime_error("vector<double> attr.set failed for: " + attr_ref_str(oid, aid));
        int n = static_cast<int>(v.size());
        if (!Uy::is_base) {
            vector<double> vs = v; // Need a copy to convert unit in
            for (auto& v : vs)
                v = Uy::from_base(v);
            if (!ShopSetDoubleArrayAttribute(c, oid, aid, n, const_cast<double*>(vs.data())))
                throw runtime_error("vector<double> attr.set failed for: " + attr_ref_str(oid, aid));
        } else {
            if (!ShopSetDoubleArrayAttribute(c, oid, aid, n, const_cast<double*>(v.data())))
                throw runtime_error("vector<double> attr.set failed for: " + attr_ref_str(oid, aid));
        }
    }

    // string
    template <class Ux, class Uy>
    string get(int oid, int aid, string_view* = nullptr) {
        unique_lock _(c_mx);
        data::shop_attr_value_str r;
        if (!ShopGetStringAttribute(c, oid, aid, r))
            throw runtime_error("string attr.get failed for: " + attr_ref_str(oid, aid));
        return r;
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, string_view v) {
        unique_lock _(c_mx);
        if (!ShopSetStringAttribute(c, oid, aid, v.data()))
            throw runtime_error("string attr.set failed for: " + attr_ref_str(oid, aid));
    }

    // XY type (ref, points(x,y))
    template <class Ux, class Uy>
    _XY get(int oid, int aid, _XY* = nullptr) {
        unique_lock _(c_mx);
        int n{ 0 };
        if (ShopGetXyAttributeNPoints(c, oid, aid, n)) {
            auto x = make_unique<double[]>(n);
            auto y = make_unique<double[]>(n);
            double ref{ 0 };
            if (ShopGetXyAttribute(c, oid, aid, ref, n, x.get(), y.get())) {
                return data::xy_factory<_XY,Ux,Uy>::create(ref, n, move(x), move(y));
            }
        }
        throw runtime_error("xy attr.get failed for: "+attr_ref_str(oid,aid));
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, const _XY& v) {
        unique_lock _(c_mx);
        auto cv = data::xy_factory<_XY,Ux,Uy>::convert_to_shop(v);
        if (!ShopSetXyAttribute(c, oid, aid, cv.ref, cv.n, cv.x.get(), cv.y.get())) {
            throw runtime_error("xy attr.set failed for: " + attr_ref_str(oid,aid));
        }
    }

    // vector<XY> like turb-eff curves
    template <class Ux, class Uy>
    vector<_XY> get(int oid, int aid, vector<_XY>* = nullptr) {
        unique_lock _(c_mx);
        int n_curves{ 0 };
        int n_points{ 0 };
        if (!ShopGetXyArrayDimensions(c, oid, aid, n_curves, n_points))
            throw runtime_error("vector<xy> attr.get n failed for: " + attr_ref_str(oid, aid));
        auto ref = make_unique<double[]>(n_curves);
        auto n_pts = make_unique<int[]>(n_curves);
        // ok, here it will be messy to ensure safe handling
        // first we alloc unique memory structure (and know it is destroyed)
        // then raw mem-structure that shop-needs
        vector<unique_ptr<double[]>> a_x(n_curves);
        vector<unique_ptr<double[]>> a_y(n_curves);
        auto v_x = make_unique<double*[]>(n_curves);
        auto v_y = make_unique<double*[]>(n_curves);
        for (auto i = 0; i < n_curves; ++i) {
            a_x[i] = make_unique<double[]>(n_points);
            a_y[i] = make_unique<double[]>(n_points);
            v_x[i] = a_x[i].get();// store managed raw memory pointers here, unmanaged.
            v_y[i] = a_y[i].get();
        }
        if (!ShopGetXyArrayAttribute(c, oid, aid, static_cast<int>( n_curves), ref.get(), n_pts.get(), v_x.get(), v_y.get()))
            throw runtime_error("vector<xy> attr.get failed for: " + attr_ref_str(oid, aid));
        vector<_XY> r; r.reserve(n_curves);
        for (auto i = 0; i < n_curves; ++i) {
            r.emplace_back(data::xy_factory<_XY,Ux,Uy>::create(ref[i], n_pts[i], move(a_x[i]), move(a_y[i])));// move out memory!
        }
        // unique ptrs' goes out of scope here, and memory released
        return r;
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, const vector<_XY>& v) {
        unique_lock _(c_mx);
        //ok. first construct managed structures that shop needs
        if (v.size() > numeric_limits<int>::max())
            throw runtime_error("vector<xy> attr.set failed for: " + attr_ref_str(oid, aid));
        int n_curves = static_cast<int>(v.size());
        auto ref = make_unique<double[]>(n_curves);
        auto n_pts = make_unique<int[]>(n_curves);
        vector<unique_ptr<double[]>> a_x(n_curves);// = make_unique<unique_ptr<double[]>>(n_curves);// this is managed memory stuff
        vector<unique_ptr<double[]>> a_y(n_curves);
        auto v_x = make_unique<double*[]>(n_curves);// this is the stuff we are passing to shop
        auto v_y = make_unique<double*[]>(n_curves);// this is the stuff we are passing to shop
        for (int i = 0; i < n_curves; ++i) {
            auto cv = data::xy_factory<_XY,Ux,Uy>::convert_to_shop(v[i]); // now, convert to shop-rep and pull out data
            ref[i] = cv.ref;
            n_pts[i] = cv.n;
            a_x[i] = move(cv.x);
            a_y[i] = move(cv.y);
            v_x[i] = a_x[i].get();
            v_y[i] = a_y[i].get();
        }
        if (!ShopSetXyArrayAttribute(c, oid, aid, n_curves, ref.get(), n_pts.get(), v_x.get(), v_y.get()))
            throw runtime_error("vector<xy> attr.set failed for: " + attr_ref_str(oid, aid));
    }

    // map<time,XY> like XYT type
    template <class Ux, class Uy>
    map<time_t, _XY> get(int oid, int aid, map<time_t, _XY>* = nullptr) {
        unique_lock _(c_mx);
        auto t_axis_start{ time_axis.front() };
        int n_curves{ 0 };
        if (!ShopGetXytNTimes(c, oid, aid, n_curves))
            throw runtime_error("xyt attr.get n failed for: " + attr_ref_str(oid, aid));
        auto t_curves = make_unique<int[]>(n_curves);
        if (!ShopGetXytTimesIntArray(c, oid, aid, n_curves, t_curves.get()))
            throw runtime_error("xyt attr.get t failed for: " + attr_ref_str(oid, aid));
        map<time_t, _XY> r;
        for (int i = 0; i < n_curves; ++i) {
            int n_points{ 0 };
            if (!ShopGetXytAttributeNPoints(c, oid, aid, n_points, t_curves[i]))
                throw runtime_error("xyt attr.get np failed for: " + attr_ref_str(oid, aid));
            double ref = numeric_limits<double>::max();
            auto x = make_unique<double[]>(n_points);
            auto y = make_unique<double[]>(n_points);
            if (!ShopGetXytAttribute(c, oid, aid, ref, n_points, x.get(), y.get(), t_curves[i]))
                throw runtime_error("xyt attr.get failed for: " + attr_ref_str(oid, aid));
            time_t t{ t_axis_start + t_curves[i] * data::shop_time_resolution_unit };
            r[t]= data::xy_factory<_XY,Ux,Uy>::create(ref, n_points, move(x), move(y));
        }
        return r;
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, const map<time_t, _XY>& v) {
        throw runtime_error("xyt attr.set not supported for: " + attr_ref_str(oid, aid));
    }

    // TXY as in kind of point time-series
    template <class Ux, class Uy>
    _TXY get(int oid, int aid, _TXY* = nullptr) {
        unique_lock _(c_mx);
        int n{ 0 };
        int n_scen{ 0 };
        data::shop_time start_time(0);
        if (!ShopGetTxyAttributeDimensions(c, oid, aid, start_time, n, n_scen))
            throw runtime_error("txy attr.get n failed for: " + attr_ref_str(oid, aid));
        auto t = make_unique<int[]>(n);
        auto y = make_unique<double[]>(n);
        double *yv = y.get();
        if (!ShopGetTxyAttribute(c, oid, aid, n, n_scen, t.get(), &yv))
            throw runtime_error("txy attr.get failed for: " + attr_ref_str(oid, aid));
        return data::txy_factory<_TXY,Uy>::create(time_axis, n, move(t), move(y));
    }
    template <class Ux, class Uy>
    void set(int oid, int aid, const _TXY& v) {
        unique_lock _(c_mx);
        auto cv = data::txy_factory<_TXY,Uy>::convert_to_shop(v, time_axis);
        if (cv.n) {
            double* y = cv.y.get();
            data::shop_time start_time(cv.start);
            if (!ShopSetTxyAttribute(c, oid, aid, start_time, cv.n, 1, cv.t.get(), &y)) {
                throw runtime_error("txy attr.set failed for: " + attr_ref_str(oid, aid));
            }
        }
    }

};
}
