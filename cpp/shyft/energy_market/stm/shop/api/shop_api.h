#pragma once
// auto genereated files based on active version of Sintef SHOP api dll
#include "shop_proxy.h"
namespace shop {

using proxy::obj;
using proxy::rw;
using proxy::ro;
using namespace proxy::unit;

template<class A>
struct reservoir:obj<A,0> {
    using super=obj<A,0>;
    reservoir()=default;
    reservoir(A* s,int oid):super(s, oid) {}
    reservoir(const reservoir& o):super(o) {}
    reservoir(reservoir&& o):super(std::move(o)) {}
    reservoir& operator=(const reservoir& o) {
        super::operator=(o);
        return *this;
    }
    reservoir& operator=(reservoir&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<reservoir,3,int,no_unit,no_unit> energy_value_converted{this}; // x[no_unit],y[no_unit]
    rw<reservoir,4,double,no_unit,no_unit> latitude{this}; // x[no_unit],y[no_unit]
    rw<reservoir,5,double,no_unit,no_unit> longitude{this}; // x[no_unit],y[no_unit]
    rw<reservoir,6,double,mm3,mm3> max_vol{this}; // x[mm3],y[mm3]
    rw<reservoir,7,double,meter,meter> lrl{this}; // x[meter],y[meter]
    rw<reservoir,8,double,meter,meter> hrl{this}; // x[meter],y[meter]
    rw<reservoir,9,typename A::_xy,mm3,meter> vol_head{this}; // x[mm3],y[meter]
    rw<reservoir,10,typename A::_xy,meter,km2> head_area{this}; // x[meter],y[km2]
    rw<reservoir,11,typename A::_txy,no_unit,meter> elevation_adjustment{this}; // x[no_unit],y[meter]
    rw<reservoir,12,double,mm3,mm3> start_vol{this}; // x[mm3],y[mm3]
    rw<reservoir,13,double,meter,meter> start_head{this}; // x[meter],y[meter]
    rw<reservoir,14,typename A::_txy,no_unit,m3_per_s> inflow{this}; // x[no_unit],y[m3_per_s]
    rw<reservoir,15,typename A::_txy,no_unit,no_unit> inflow_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,16,typename A::_txy,no_unit,no_unit> sim_inflow_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,17,typename A::_xy,meter,m3_per_s> flow_descr{this}; // x[meter],y[m3_per_s]
    rw<reservoir,18,typename A::_txy,no_unit,no_unit> overflow_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,19,typename A::_txy,no_unit,nok_per_mm3> overflow_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<reservoir,20,typename A::_txy,no_unit,no_unit> overflow_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,21,typename A::_txy,no_unit,mm3> min_vol_constr{this}; // x[no_unit],y[mm3]
    rw<reservoir,22,typename A::_txy,no_unit,no_unit> min_vol_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,23,typename A::_txy,no_unit,mm3> max_vol_constr{this}; // x[no_unit],y[mm3]
    rw<reservoir,24,typename A::_txy,no_unit,no_unit> max_vol_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,25,typename A::_txy,no_unit,meter> min_head_constr{this}; // x[no_unit],y[meter]
    rw<reservoir,26,typename A::_txy,no_unit,no_unit> min_head_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,27,typename A::_txy,no_unit,meter> max_head_constr{this}; // x[no_unit],y[meter]
    rw<reservoir,28,typename A::_txy,no_unit,no_unit> max_head_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,29,typename A::_txy,no_unit,mm3> tactical_limit_min{this}; // x[no_unit],y[mm3]
    rw<reservoir,30,typename A::_txy,no_unit,no_unit> tactical_limit_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,31,typename A::_txy,no_unit,nok_per_mm3h> tactical_cost_min{this}; // x[no_unit],y[nok_per_mm3h]
    rw<reservoir,32,typename A::_txy,no_unit,no_unit> tactical_cost_min_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,33,typename A::_txy,no_unit,mm3> tactical_limit_max{this}; // x[no_unit],y[mm3]
    rw<reservoir,34,typename A::_txy,no_unit,no_unit> tactical_limit_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,35,typename A::_txy,no_unit,nok_per_mm3h> tactical_cost_max{this}; // x[no_unit],y[nok_per_mm3h]
    rw<reservoir,36,typename A::_txy,no_unit,no_unit> tactical_cost_max_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,37,typename A::_txy,no_unit,mm3> upper_slack{this}; // x[no_unit],y[mm3]
    rw<reservoir,38,typename A::_txy,no_unit,mm3> lower_slack{this}; // x[no_unit],y[mm3]
    rw<reservoir,39,typename A::_txy,no_unit,mm3> schedule{this}; // x[no_unit],y[mm3]
    rw<reservoir,40,typename A::_txy,no_unit,no_unit> schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<reservoir,41,typename A::_txy,no_unit,mm3> volume_schedule{this}; // x[no_unit],y[mm3]
    rw<reservoir,42,typename A::_txy,no_unit,meter> level_schedule{this}; // x[no_unit],y[meter]
    rw<reservoir,43,typename A::_txy,no_unit,mm3> volume_ramping_up{this}; // x[no_unit],y[mm3]
    rw<reservoir,44,typename A::_txy,no_unit,mm3> volume_ramping_down{this}; // x[no_unit],y[mm3]
    rw<reservoir,45,typename A::_txy,no_unit,meter> level_ramping_up{this}; // x[no_unit],y[meter]
    rw<reservoir,46,typename A::_txy,no_unit,meter> level_ramping_down{this}; // x[no_unit],y[meter]
    rw<reservoir,47,vector<typename A::_xy>,mm3,nok_per_mm3> water_value_input{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,48,double,nok_per_mwh,nok_per_mwh> energy_value_input{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<reservoir,49,typename A::_xy,mm3,nok_per_mm3> peak_volume_cost_curve{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,50,typename A::_xy,mm3,nok_per_mm3> flood_volume_cost_curve{this}; // x[mm3],y[nok_per_mm3]
    rw<reservoir,51,typename A::_txy,no_unit,meter_per_hour> evaporation_rate{this}; // x[no_unit],y[meter_per_hour]
    ro<reservoir,52,typename A::_txy,no_unit,mm3> storage{this}; // x[no_unit],y[mm3]
    ro<reservoir,53,typename A::_txy,no_unit,mm3> sim_storage{this}; // x[no_unit],y[mm3]
    ro<reservoir,54,typename A::_txy,no_unit,meter> head{this}; // x[no_unit],y[meter]
    ro<reservoir,55,typename A::_txy,no_unit,km2> area{this}; // x[no_unit],y[km2]
    ro<reservoir,56,typename A::_txy,no_unit,meter> sim_head{this}; // x[no_unit],y[meter]
    ro<reservoir,57,typename A::_txy,no_unit,m3_per_s> sim_inflow{this}; // x[no_unit],y[m3_per_s]
    ro<reservoir,58,typename A::_txy,mm3,mm3> endpoint_penalty{this}; // x[mm3],y[mm3]
    ro<reservoir,59,typename A::_txy,no_unit,mm3> penalty{this}; // x[no_unit],y[mm3]
    ro<reservoir,60,typename A::_txy,no_unit,mm3> tactical_penalty_up{this}; // x[no_unit],y[mm3]
    ro<reservoir,61,typename A::_txy,no_unit,mm3> tactical_penalty_down{this}; // x[no_unit],y[mm3]
    ro<reservoir,62,typename A::_txy,no_unit,nok> end_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,63,typename A::_txy,no_unit,nok> penalty_nok{this}; // x[no_unit],y[nok]
    ro<reservoir,64,typename A::_txy,no_unit,nok> tactical_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,65,typename A::_txy,no_unit,nok_per_mm3> water_value_global_result{this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir,66,typename A::_txy,no_unit,nok_per_mm3> water_value_local_result{this}; // x[no_unit],y[nok_per_mm3]
    ro<reservoir,67,typename A::_txy,no_unit,nok_per_mwh> energy_value_local_result{this}; // x[no_unit],y[nok_per_mwh]
    ro<reservoir,68,typename A::_txy,no_unit,nok> end_value{this}; // x[no_unit],y[nok]
    ro<reservoir,69,typename A::_txy,no_unit,nok> change_in_end_value{this}; // x[no_unit],y[nok]
    ro<reservoir,70,typename A::_txy,no_unit,nok> vow_in_transit{this}; // x[no_unit],y[nok]
    ro<reservoir,71,double,nok_per_mm3,nok_per_mm3> calc_global_water_value{this}; // x[nok_per_mm3],y[nok_per_mm3]
    ro<reservoir,72,double,mwh_per_mm3,mwh_per_mm3> energy_conversion_factor{this}; // x[mwh_per_mm3],y[mwh_per_mm3]
    ro<reservoir,73,vector<typename A::_xy>,mm3,nok_per_mm3> water_value_cut_result{this}; // x[mm3],y[nok_per_mm3]
    ro<reservoir,74,int,no_unit,no_unit> added_to_network{this}; // x[no_unit],y[no_unit]
    ro<reservoir,75,int,no_unit,no_unit> network_no{this}; // x[no_unit],y[no_unit]
    ro<reservoir,76,typename A::_txy,no_unit,nok> peak_volume_penalty{this}; // x[no_unit],y[nok]
    ro<reservoir,77,typename A::_txy,no_unit,nok> flood_volume_penalty{this}; // x[no_unit],y[nok]
};
template<class A>
struct power_plant:obj<A,1> {
    using super=obj<A,1>;
    power_plant()=default;
    power_plant(A* s,int oid):super(s, oid) {}
    power_plant(const power_plant& o):super(o) {}
    power_plant(power_plant&& o):super(std::move(o)) {}
    power_plant& operator=(const power_plant& o) {
        super::operator=(o);
        return *this;
    }
    power_plant& operator=(power_plant&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<power_plant,82,int,no_unit,no_unit> num_gen{this}; // x[no_unit],y[no_unit]
    ro<power_plant,83,int,no_unit,no_unit> num_pump{this}; // x[no_unit],y[no_unit]
    rw<power_plant,86,int,no_unit,no_unit> equal_distribution{this}; // x[no_unit],y[no_unit]
    rw<power_plant,87,double,no_unit,no_unit> less_distribution_eps{this}; // x[no_unit],y[no_unit]
    rw<power_plant,94,double,no_unit,no_unit> latitude{this}; // x[no_unit],y[no_unit]
    rw<power_plant,95,double,no_unit,no_unit> longitude{this}; // x[no_unit],y[no_unit]
    rw<power_plant,96,double,percent,percent> ownership{this}; // x[percent],y[percent]
    rw<power_plant,97,typename A::_txy,no_unit,no_unit> prod_area{this}; // x[no_unit],y[no_unit]
    rw<power_plant,98,typename A::_txy,no_unit,no_unit> prod_area_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,99,double,kwh_per_mm3,kwh_per_mm3> prod_factor{this}; // x[kwh_per_mm3],y[kwh_per_mm3]
    rw<power_plant,100,double,meter,meter> outlet_line{this}; // x[meter],y[meter]
    rw<power_plant,101,typename A::_txy,no_unit,meter> intake_line{this}; // x[no_unit],y[meter]
    rw<power_plant,102,vector<double>,s2_per_m5,s2_per_m5> main_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<power_plant,103,vector<double>,s2_per_m5,s2_per_m5> penstock_loss{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<power_plant,104,vector<typename A::_xy>,m3_per_s,meter> tailrace_loss{this}; // x[m3_per_s],y[meter]
    rw<power_plant,105,typename A::_txy,no_unit,no_unit> tailrace_loss_from_bypass_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,106,vector<typename A::_xy>,m3_per_s,meter> intake_loss{this}; // x[m3_per_s],y[meter]
    rw<power_plant,107,typename A::_txy,no_unit,no_unit> intake_loss_from_bypass_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,108,typename A::_txy,no_unit,delta_meter> tides{this}; // x[no_unit],y[delta_meter]
    rw<power_plant,109,int,no_unit,no_unit> time_delay{this}; // x[no_unit],y[no_unit]
    rw<power_plant,110,typename A::_xy,hour,no_unit> shape_discharge{this}; // x[hour],y[no_unit]
    rw<power_plant,111,typename A::_txy,no_unit,nok_per_mm3> discharge_fee{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant,112,typename A::_txy,no_unit,no_unit> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,113,typename A::_xy,m3_per_s,nok_per_h_per_m3_per_s> discharge_cost_curve{this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<power_plant,114,typename A::_txy,no_unit,nok_per_mwh> feeding_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,115,typename A::_txy,no_unit,no_unit> feeding_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,116,typename A::_txy,no_unit,nok_per_mwh> production_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,117,typename A::_txy,no_unit,no_unit> production_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,118,typename A::_txy,no_unit,nok_per_mwh> consumption_fee{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,119,typename A::_txy,no_unit,no_unit> consumption_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,120,typename A::_txy,no_unit,no_unit> linear_startup_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,121,typename A::_txy,no_unit,no_unit> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,122,typename A::_txy,no_unit,no_unit> mip_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,123,typename A::_txy,no_unit,no_unit> mip_length{this}; // x[no_unit],y[no_unit]
    rw<power_plant,124,typename A::_txy,no_unit,no_unit> mip_length_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,125,vector<int>,no_unit,no_unit> gen_priority{this}; // x[no_unit],y[no_unit]
    rw<power_plant,126,typename A::_txy,no_unit,no_unit> n_seg_down{this}; // x[no_unit],y[no_unit]
    rw<power_plant,127,typename A::_txy,no_unit,no_unit> n_seg_up{this}; // x[no_unit],y[no_unit]
    rw<power_plant,128,typename A::_txy,no_unit,no_unit> n_mip_seg_down{this}; // x[no_unit],y[no_unit]
    rw<power_plant,129,typename A::_txy,no_unit,no_unit> n_mip_seg_up{this}; // x[no_unit],y[no_unit]
    rw<power_plant,130,typename A::_txy,no_unit,no_unit> dyn_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,131,typename A::_txy,no_unit,no_unit> dyn_mip_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,132,typename A::_txy,no_unit,no_unit> build_original_pq_curves_by_turb_eff{this}; // x[no_unit],y[no_unit]
    rw<power_plant,133,typename A::_txy,no_unit,mw> min_p_constr{this}; // x[no_unit],y[mw]
    rw<power_plant,134,typename A::_txy,no_unit,no_unit> min_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,135,typename A::_txy,no_unit,no_unit> min_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,136,typename A::_txy,no_unit,nok_per_mwh> min_p_penalty_cost{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,137,typename A::_txy,no_unit,no_unit> min_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,138,typename A::_txy,no_unit,mw> max_p_constr{this}; // x[no_unit],y[mw]
    rw<power_plant,139,typename A::_txy,no_unit,no_unit> max_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,140,typename A::_txy,no_unit,no_unit> max_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,141,typename A::_txy,no_unit,nok_per_mwh> max_p_penalty_cost{this}; // x[no_unit],y[nok_per_mwh]
    rw<power_plant,142,typename A::_txy,no_unit,no_unit> max_p_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,143,typename A::_txy,no_unit,m3_per_s> min_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,144,typename A::_txy,no_unit,no_unit> min_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,145,typename A::_txy,no_unit,no_unit> min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,146,typename A::_txy,no_unit,nok_per_mm3> min_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant,147,typename A::_txy,no_unit,no_unit> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,148,typename A::_txy,no_unit,m3_per_s> max_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,149,typename A::_txy,no_unit,no_unit> max_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,150,typename A::_txy,no_unit,no_unit> max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,151,typename A::_txy,no_unit,nok_per_mm3> max_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<power_plant,152,typename A::_txy,no_unit,no_unit> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,153,typename A::_xy,meter,m3_per_s> max_q_limit_rsv_up{this}; // x[meter],y[m3_per_s]
    rw<power_plant,154,typename A::_xy,meter,m3_per_s> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<power_plant,155,typename A::_txy,no_unit,mw> production_schedule{this}; // x[no_unit],y[mw]
    rw<power_plant,156,typename A::_txy,no_unit,no_unit> production_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,157,typename A::_txy,no_unit,m3_per_s> discharge_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,158,typename A::_txy,no_unit,no_unit> discharge_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,159,typename A::_txy,no_unit,mw> consumption_schedule{this}; // x[no_unit],y[mw]
    rw<power_plant,160,typename A::_txy,no_unit,no_unit> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,161,typename A::_txy,no_unit,m3_per_s> upflow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<power_plant,162,typename A::_txy,no_unit,no_unit> upflow_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,163,double,nok,nok> sched_penalty_cost_down{this}; // x[nok],y[nok]
    rw<power_plant,164,double,nok,nok> sched_penalty_cost_up{this}; // x[nok],y[nok]
    rw<power_plant,165,typename A::_txy,no_unit,mw_hour> power_ramping_up{this}; // x[no_unit],y[mw_hour]
    rw<power_plant,166,typename A::_txy,no_unit,mw_hour> power_ramping_down{this}; // x[no_unit],y[mw_hour]
    rw<power_plant,167,typename A::_txy,no_unit,m3sec_hour> discharge_ramping_up{this}; // x[no_unit],y[m3sec_hour]
    rw<power_plant,168,typename A::_txy,no_unit,m3sec_hour> discharge_ramping_down{this}; // x[no_unit],y[m3sec_hour]
    rw<power_plant,169,typename A::_txy,no_unit,mw> block_merge_tolerance{this}; // x[no_unit],y[mw]
    rw<power_plant,170,typename A::_txy,no_unit,hour> block_generation_mwh{this}; // x[no_unit],y[hour]
    rw<power_plant,171,typename A::_txy,no_unit,hour> block_generation_m3s{this}; // x[no_unit],y[hour]
    rw<power_plant,172,typename A::_txy,no_unit,mw> frr_up_min{this}; // x[no_unit],y[mw]
    rw<power_plant,173,typename A::_txy,no_unit,mw> frr_up_max{this}; // x[no_unit],y[mw]
    rw<power_plant,174,typename A::_txy,no_unit,mw> frr_down_min{this}; // x[no_unit],y[mw]
    rw<power_plant,175,typename A::_txy,no_unit,mw> frr_down_max{this}; // x[no_unit],y[mw]
    rw<power_plant,176,typename A::_txy,no_unit,mw> rr_up_min{this}; // x[no_unit],y[mw]
    rw<power_plant,177,typename A::_txy,no_unit,no_unit> frr_symmetric_flag{this}; // x[no_unit],y[no_unit]
    rw<power_plant,178,typename A::_txy,no_unit,no_unit> bp_dyn_wv_flag{this}; // x[no_unit],y[no_unit]
    ro<power_plant,179,typename A::_txy,no_unit,mw> ref_prod{this}; // x[no_unit],y[mw]
    rw<power_plant,180,typename A::_txy,no_unit,no_unit> plant_unbalance_recommit{this}; // x[no_unit],y[no_unit]
    rw<power_plant,181,typename A::_txy,no_unit,mw> spinning_reserve_up_max{this}; // x[no_unit],y[mw]
    rw<power_plant,182,typename A::_txy,no_unit,mw> spinning_reserve_down_max{this}; // x[no_unit],y[mw]
    ro<power_plant,183,typename A::_txy,no_unit,mw> production{this}; // x[no_unit],y[mw]
    ro<power_plant,184,typename A::_txy,no_unit,mw> solver_production{this}; // x[no_unit],y[mw]
    ro<power_plant,185,typename A::_txy,no_unit,mw> sim_production{this}; // x[no_unit],y[mw]
    ro<power_plant,186,typename A::_txy,no_unit,mw> prod_unbalance{this}; // x[no_unit],y[mw]
    ro<power_plant,187,typename A::_txy,no_unit,mw> consumption{this}; // x[no_unit],y[mw]
    ro<power_plant,188,typename A::_txy,no_unit,mw> sim_consumption{this}; // x[no_unit],y[mw]
    ro<power_plant,189,typename A::_txy,no_unit,mw> solver_consumption{this}; // x[no_unit],y[mw]
    ro<power_plant,190,typename A::_txy,no_unit,mw> cons_unbalance{this}; // x[no_unit],y[mw]
    ro<power_plant,191,typename A::_txy,no_unit,m3_per_s> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,192,typename A::_txy,no_unit,m3_per_s> solver_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,193,typename A::_txy,no_unit,m3_per_s> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,194,typename A::_txy,no_unit,m3_per_s> upflow{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,195,typename A::_txy,no_unit,m3_per_s> sim_upflow{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,196,typename A::_txy,no_unit,m3_per_s> solver_upflow{this}; // x[no_unit],y[m3_per_s]
    ro<power_plant,197,typename A::_txy,no_unit,meter> gross_head{this}; // x[no_unit],y[meter]
    ro<power_plant,198,typename A::_txy,no_unit,meter> eff_head{this}; // x[no_unit],y[meter]
    ro<power_plant,199,typename A::_txy,no_unit,meter> head_loss{this}; // x[no_unit],y[meter]
    ro<power_plant,200,typename A::_txy,no_unit,mw> min_p_penalty{this}; // x[no_unit],y[mw]
    ro<power_plant,201,typename A::_txy,no_unit,mw> max_p_penalty{this}; // x[no_unit],y[mw]
    ro<power_plant,202,typename A::_txy,no_unit,mm3> min_q_penalty{this}; // x[no_unit],y[mm3]
    ro<power_plant,203,typename A::_txy,no_unit,mm3> max_q_penalty{this}; // x[no_unit],y[mm3]
    ro<power_plant,204,typename A::_txy,no_unit,nok> p_constr_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,205,typename A::_txy,no_unit,nok> q_constr_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,206,typename A::_txy,no_unit,nok> schedule_up_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,207,typename A::_txy,no_unit,nok> schedule_down_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,208,typename A::_txy,no_unit,nok> schedule_penalty{this}; // x[no_unit],y[nok]
    ro<power_plant,209,map<int64_t, typename A::_xy>,mw,m3_per_s> best_profit_q{this}; // x[mw],y[m3_per_s]
    ro<power_plant,210,map<int64_t, typename A::_xy>,mw,nok_per_mw> best_profit_mc{this}; // x[mw],y[nok_per_mw]
    ro<power_plant,211,map<int64_t, typename A::_xy>,mw,nok_per_mw> best_profit_ac{this}; // x[mw],y[nok_per_mw]
    ro<power_plant,212,map<int64_t, typename A::_xy>,mw,nok> best_profit_commitment_cost{this}; // x[mw],y[nok]
    ro<power_plant,213,map<int64_t, typename A::_xy>,mw,nok_per_mw> best_profit_bid_matrix{this}; // x[mw],y[nok_per_mw]
    ro<power_plant,214,int,no_unit,no_unit> times_of_wrong_pq_uploading{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct unit:obj<A,2> {
    using super=obj<A,2>;
    unit()=default;
    unit(A* s,int oid):super(s, oid) {}
    unit(const unit& o):super(o) {}
    unit(unit&& o):super(std::move(o)) {}
    unit& operator=(const unit& o) {
        super::operator=(o);
        return *this;
    }
    unit& operator=(unit&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<unit,224,int,no_unit,no_unit> type{this}; // x[no_unit],y[no_unit]
    ro<unit,225,int,no_unit,no_unit> num_needle_comb{this}; // x[no_unit],y[no_unit]
    rw<unit,226,typename A::_txy,no_unit,no_unit> prod_area{this}; // x[no_unit],y[no_unit]
    rw<unit,227,int,no_unit,no_unit> initial_state{this}; // x[no_unit],y[no_unit]
    rw<unit,228,int,no_unit,no_unit> penstock{this}; // x[no_unit],y[no_unit]
    rw<unit,229,double,mw,mw> p_min{this}; // x[mw],y[mw]
    rw<unit,230,double,mw,mw> p_max{this}; // x[mw],y[mw]
    rw<unit,231,double,mw,mw> p_nom{this}; // x[mw],y[mw]
    rw<unit,232,typename A::_xy,mw,percent> gen_eff_curve{this}; // x[mw],y[percent]
    rw<unit,233,vector<typename A::_xy>,m3_per_s,percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<unit,234,int,no_unit,no_unit> affinity_eq_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,235,typename A::_txy,no_unit,no_unit> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,236,typename A::_txy,no_unit,nok> startcost{this}; // x[no_unit],y[nok]
    rw<unit,237,typename A::_txy,no_unit,nok> stopcost{this}; // x[no_unit],y[nok]
    rw<unit,238,vector<typename A::_xy>,m3_per_s,nok_per_h_per_m3_per_s> discharge_cost_curve{this}; // x[m3_per_s],y[nok_per_h_per_m3_per_s]
    rw<unit,239,typename A::_txy,no_unit,no_unit> priority{this}; // x[no_unit],y[no_unit]
    rw<unit,240,typename A::_txy,no_unit,no_unit> committed_in{this}; // x[no_unit],y[no_unit]
    rw<unit,241,typename A::_txy,no_unit,no_unit> committed_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,242,typename A::_txy,no_unit,mw> min_p_constr{this}; // x[no_unit],y[mw]
    rw<unit,243,typename A::_txy,no_unit,no_unit> min_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,244,typename A::_txy,no_unit,mw> max_p_constr{this}; // x[no_unit],y[mw]
    rw<unit,245,typename A::_txy,no_unit,no_unit> max_p_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,246,typename A::_txy,no_unit,m3_per_s> min_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<unit,247,typename A::_txy,no_unit,no_unit> min_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,248,typename A::_txy,no_unit,m3_per_s> max_q_constr{this}; // x[no_unit],y[m3_per_s]
    rw<unit,249,typename A::_txy,no_unit,no_unit> max_q_constr_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,250,typename A::_xy,meter,m3_per_s> max_q_limit_rsv_down{this}; // x[meter],y[m3_per_s]
    rw<unit,251,typename A::_txy,no_unit,meter> upstream_min{this}; // x[no_unit],y[meter]
    rw<unit,252,typename A::_txy,no_unit,no_unit> upstream_min_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,253,typename A::_txy,no_unit,meter> downstream_max{this}; // x[no_unit],y[meter]
    rw<unit,254,typename A::_txy,no_unit,no_unit> downstream_max_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,255,typename A::_txy,no_unit,mw> production_schedule{this}; // x[no_unit],y[mw]
    rw<unit,256,typename A::_txy,no_unit,no_unit> production_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,257,typename A::_txy,no_unit,m3_per_s> discharge_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<unit,258,typename A::_txy,no_unit,no_unit> discharge_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,259,typename A::_txy,no_unit,no_unit> fcr_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,260,typename A::_txy,no_unit,mw> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<unit,261,typename A::_txy,no_unit,mw> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<unit,262,typename A::_txy,no_unit,mw> p_rr_min{this}; // x[no_unit],y[mw]
    rw<unit,263,typename A::_txy,no_unit,mw> frr_up_min{this}; // x[no_unit],y[mw]
    rw<unit,264,typename A::_txy,no_unit,mw> frr_up_max{this}; // x[no_unit],y[mw]
    rw<unit,265,typename A::_txy,no_unit,mw> frr_down_min{this}; // x[no_unit],y[mw]
    rw<unit,266,typename A::_txy,no_unit,mw> frr_down_max{this}; // x[no_unit],y[mw]
    rw<unit,267,typename A::_txy,no_unit,mw> fcr_n_up_min{this}; // x[no_unit],y[mw]
    rw<unit,268,typename A::_txy,no_unit,mw> fcr_n_up_max{this}; // x[no_unit],y[mw]
    rw<unit,269,typename A::_txy,no_unit,mw> fcr_n_down_min{this}; // x[no_unit],y[mw]
    rw<unit,270,typename A::_txy,no_unit,mw> fcr_n_down_max{this}; // x[no_unit],y[mw]
    rw<unit,271,typename A::_txy,no_unit,mw> fcr_d_up_min{this}; // x[no_unit],y[mw]
    rw<unit,272,typename A::_txy,no_unit,mw> fcr_d_up_max{this}; // x[no_unit],y[mw]
    rw<unit,273,typename A::_txy,no_unit,mw> fcr_d_down_min{this}; // x[no_unit],y[mw]
    rw<unit,274,typename A::_txy,no_unit,mw> fcr_d_down_max{this}; // x[no_unit],y[mw]
    rw<unit,275,typename A::_txy,no_unit,mw> rr_up_min{this}; // x[no_unit],y[mw]
    rw<unit,276,typename A::_txy,no_unit,mw> rr_up_max{this}; // x[no_unit],y[mw]
    rw<unit,277,typename A::_txy,no_unit,mw> rr_down_min{this}; // x[no_unit],y[mw]
    rw<unit,278,typename A::_txy,no_unit,mw> rr_down_max{this}; // x[no_unit],y[mw]
    rw<unit,279,typename A::_txy,no_unit,mw> fcr_n_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,280,typename A::_txy,no_unit,no_unit> fcr_n_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,281,typename A::_txy,no_unit,mw> fcr_n_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,282,typename A::_txy,no_unit,no_unit> fcr_n_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,283,typename A::_txy,no_unit,mw> fcr_d_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,284,typename A::_txy,no_unit,no_unit> fcr_d_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,285,typename A::_txy,no_unit,mw> fcr_d_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,286,typename A::_txy,no_unit,no_unit> fcr_d_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,287,typename A::_txy,no_unit,mw> frr_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,288,typename A::_txy,no_unit,no_unit> frr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,289,typename A::_txy,no_unit,mw> frr_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,290,typename A::_txy,no_unit,no_unit> frr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,291,typename A::_txy,no_unit,mw> rr_up_schedule{this}; // x[no_unit],y[mw]
    rw<unit,292,typename A::_txy,no_unit,no_unit> rr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,293,typename A::_txy,no_unit,mw> rr_down_schedule{this}; // x[no_unit],y[mw]
    rw<unit,294,typename A::_txy,no_unit,no_unit> rr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,295,typename A::_txy,no_unit,nok> droop_cost{this}; // x[no_unit],y[nok]
    rw<unit,296,typename A::_txy,no_unit,no_unit> fixed_droop{this}; // x[no_unit],y[no_unit]
    rw<unit,297,typename A::_txy,no_unit,no_unit> fixed_droop_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,298,typename A::_txy,no_unit,no_unit> droop_min{this}; // x[no_unit],y[no_unit]
    rw<unit,299,typename A::_txy,no_unit,no_unit> droop_max{this}; // x[no_unit],y[no_unit]
    rw<unit,300,vector<double>,no_unit,no_unit> discrete_droop_values{this}; // x[no_unit],y[no_unit]
    rw<unit,301,typename A::_txy,no_unit,nok> reserve_ramping_cost_up{this}; // x[no_unit],y[nok]
    rw<unit,302,typename A::_txy,no_unit,nok> reserve_ramping_cost_down{this}; // x[no_unit],y[nok]
    rw<unit,303,typename A::_txy,no_unit,mw> ref_production{this}; // x[no_unit],y[mw]
    rw<unit,304,typename A::_txy,no_unit,no_unit> schedule_deviation_flag{this}; // x[no_unit],y[no_unit]
    rw<unit,305,typename A::_txy,no_unit,no_unit> gen_turn_off_limit{this}; // x[no_unit],y[no_unit]
    rw<unit,306,typename A::_txy,no_unit,nok_per_mw> fcr_n_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,307,typename A::_txy,no_unit,nok_per_mw> fcr_n_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,308,typename A::_txy,no_unit,nok_per_mw> fcr_d_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,309,typename A::_txy,no_unit,nok_per_mw> fcr_d_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,310,typename A::_txy,no_unit,nok_per_mw> frr_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,311,typename A::_txy,no_unit,nok_per_mw> frr_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,312,typename A::_txy,no_unit,nok_per_mw> rr_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,313,typename A::_txy,no_unit,nok_per_mw> rr_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<unit,314,typename A::_txy,no_unit,mw> spinning_reserve_up_max{this}; // x[no_unit],y[mw]
    rw<unit,315,typename A::_txy,no_unit,mw> spinning_reserve_down_max{this}; // x[no_unit],y[mw]
    ro<unit,316,typename A::_txy,no_unit,meter> eff_head{this}; // x[no_unit],y[meter]
    ro<unit,317,typename A::_txy,no_unit,meter> sim_eff_head{this}; // x[no_unit],y[meter]
    ro<unit,318,typename A::_txy,no_unit,meter> head_loss{this}; // x[no_unit],y[meter]
    ro<unit,319,typename A::_txy,no_unit,mw> production{this}; // x[no_unit],y[mw]
    ro<unit,320,typename A::_txy,no_unit,mw> solver_production{this}; // x[no_unit],y[mw]
    ro<unit,321,typename A::_txy,no_unit,mw> sim_production{this}; // x[no_unit],y[mw]
    ro<unit,322,typename A::_txy,no_unit,m3_per_s> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<unit,323,typename A::_txy,no_unit,m3_per_s> solver_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<unit,324,typename A::_txy,no_unit,m3_per_s> sim_discharge{this}; // x[no_unit],y[m3_per_s]
    ro<unit,325,typename A::_txy,no_unit,no_unit> committed_out{this}; // x[no_unit],y[no_unit]
    ro<unit,326,typename A::_txy,no_unit,mw> production_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,327,typename A::_txy,no_unit,mm3> discharge_schedule_penalty{this}; // x[no_unit],y[mm3]
    ro<unit,328,typename A::_txy,no_unit,mw> fcr_n_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,329,typename A::_txy,no_unit,mw> fcr_n_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,330,typename A::_txy,no_unit,mw> fcr_d_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,331,typename A::_txy,no_unit,mw> fcr_d_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,332,typename A::_txy,no_unit,mw> frr_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,333,typename A::_txy,no_unit,mw> frr_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,334,typename A::_txy,no_unit,mw> rr_up_delivery{this}; // x[no_unit],y[mw]
    ro<unit,335,typename A::_txy,no_unit,mw> rr_down_delivery{this}; // x[no_unit],y[mw]
    ro<unit,336,typename A::_txy,no_unit,mw> fcr_n_up_delivery_physical{this}; // x[no_unit],y[mw]
    ro<unit,337,typename A::_txy,no_unit,mw> fcr_n_down_delivery_physical{this}; // x[no_unit],y[mw]
    ro<unit,338,typename A::_txy,no_unit,mw> fcr_d_up_delivery_physical{this}; // x[no_unit],y[mw]
    ro<unit,339,typename A::_txy,no_unit,mw> fcr_d_down_delivery_physical{this}; // x[no_unit],y[mw]
    ro<unit,340,typename A::_txy,no_unit,mw> fcr_n_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,341,typename A::_txy,no_unit,mw> fcr_n_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,342,typename A::_txy,no_unit,mw> fcr_d_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,343,typename A::_txy,no_unit,mw> fcr_d_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,344,typename A::_txy,no_unit,mw> frr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,345,typename A::_txy,no_unit,mw> frr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,346,typename A::_txy,no_unit,mw> rr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,347,typename A::_txy,no_unit,mw> rr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<unit,348,typename A::_txy,no_unit,no_unit> droop_result{this}; // x[no_unit],y[no_unit]
    ro<unit,349,map<int64_t, typename A::_xy>,mw,m3_per_s> best_profit_q{this}; // x[mw],y[m3_per_s]
    ro<unit,350,map<int64_t, typename A::_xy>,mw,mw> best_profit_p{this}; // x[mw],y[mw]
    ro<unit,351,map<int64_t, typename A::_xy>,mw,m3_per_s> best_profit_dq_dp{this}; // x[mw],y[m3_per_s]
    ro<unit,352,map<int64_t, typename A::_xy>,mw,no_unit> best_profit_needle_comb{this}; // x[mw],y[no_unit]
    ro<unit,353,typename A::_txy,no_unit,nok> startup_cost_mip_objective{this}; // x[no_unit],y[nok]
    ro<unit,354,typename A::_txy,no_unit,nok> startup_cost_total_objective{this}; // x[no_unit],y[nok]
    ro<unit,355,typename A::_txy,no_unit,nok> discharge_fee_objective{this}; // x[no_unit],y[nok]
    ro<unit,356,typename A::_txy,no_unit,nok> feeding_fee_objective{this}; // x[no_unit],y[nok]
    ro<unit,357,typename A::_txy,no_unit,nok> schedule_penalty{this}; // x[no_unit],y[nok]
    ro<unit,358,typename A::_txy,no_unit,nok> market_income{this}; // x[no_unit],y[nok]
    ro<unit,359,map<int64_t, typename A::_xy>,m3_per_s,mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<unit,360,map<int64_t, typename A::_xy>,m3_per_s,mw> convex_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<unit,361,map<int64_t, typename A::_xy>,m3_per_s,mw> final_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<unit,362,typename A::_txy,no_unit,mw> max_prod{this}; // x[no_unit],y[mw]
    ro<unit,363,typename A::_txy,no_unit,mw> min_prod{this}; // x[no_unit],y[mw]
};
template<class A>
struct needle_combination:obj<A,3> {
    using super=obj<A,3>;
    needle_combination()=default;
    needle_combination(A* s,int oid):super(s, oid) {}
    needle_combination(const needle_combination& o):super(o) {}
    needle_combination(needle_combination&& o):super(std::move(o)) {}
    needle_combination& operator=(const needle_combination& o) {
        super::operator=(o);
        return *this;
    }
    needle_combination& operator=(needle_combination&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<needle_combination,367,double,mw,mw> p_max{this}; // x[mw],y[mw]
    rw<needle_combination,368,double,mw,mw> p_min{this}; // x[mw],y[mw]
    rw<needle_combination,369,double,mw,mw> p_nom{this}; // x[mw],y[mw]
    rw<needle_combination,370,vector<typename A::_xy>,m3_per_s,percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<needle_combination,371,typename A::_txy,no_unit,mw> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<needle_combination,372,typename A::_txy,no_unit,mw> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<needle_combination,373,typename A::_xy,mw,nok_per_mw> production_cost{this}; // x[mw],y[nok_per_mw]
    ro<needle_combination,374,map<int64_t, typename A::_xy>,m3_per_s,mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<needle_combination,375,map<int64_t, typename A::_xy>,m3_per_s,mw> convex_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<needle_combination,376,map<int64_t, typename A::_xy>,m3_per_s,mw> final_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<needle_combination,377,typename A::_txy,no_unit,mw> max_prod{this}; // x[no_unit],y[mw]
    ro<needle_combination,378,typename A::_txy,no_unit,mw> min_prod{this}; // x[no_unit],y[mw]
};
template<class A>
struct pump:obj<A,4> {
    using super=obj<A,4>;
    pump()=default;
    pump(A* s,int oid):super(s, oid) {}
    pump(const pump& o):super(o) {}
    pump(pump&& o):super(std::move(o)) {}
    pump& operator=(const pump& o) {
        super::operator=(o);
        return *this;
    }
    pump& operator=(pump&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<pump,385,int,no_unit,no_unit> initial_state{this}; // x[no_unit],y[no_unit]
    rw<pump,386,int,no_unit,no_unit> penstock{this}; // x[no_unit],y[no_unit]
    rw<pump,387,double,mw,mw> p_min{this}; // x[mw],y[mw]
    rw<pump,388,double,mw,mw> p_max{this}; // x[mw],y[mw]
    rw<pump,389,double,mw,mw> p_nom{this}; // x[mw],y[mw]
    rw<pump,390,typename A::_xy,mw,percent> gen_eff_curve{this}; // x[mw],y[percent]
    rw<pump,391,vector<typename A::_xy>,m3_per_s,percent> turb_eff_curves{this}; // x[m3_per_s],y[percent]
    rw<pump,392,typename A::_txy,no_unit,no_unit> maintenance_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,393,typename A::_txy,no_unit,nok> startcost{this}; // x[no_unit],y[nok]
    rw<pump,394,typename A::_txy,no_unit,nok> stopcost{this}; // x[no_unit],y[nok]
    rw<pump,395,typename A::_txy,no_unit,no_unit> committed_in{this}; // x[no_unit],y[no_unit]
    rw<pump,396,typename A::_txy,no_unit,no_unit> committed_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,397,typename A::_txy,no_unit,meter> upstream_max{this}; // x[no_unit],y[meter]
    rw<pump,398,typename A::_txy,no_unit,no_unit> upstream_max_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,399,typename A::_txy,no_unit,meter> downstream_min{this}; // x[no_unit],y[meter]
    rw<pump,400,typename A::_txy,no_unit,meter> downstream_min_flag{this}; // x[no_unit],y[meter]
    rw<pump,401,typename A::_txy,no_unit,mw> consumption_schedule{this}; // x[no_unit],y[mw]
    rw<pump,402,typename A::_txy,no_unit,no_unit> consumption_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,403,typename A::_txy,no_unit,m3_per_s> upflow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<pump,404,typename A::_txy,no_unit,no_unit> upflow_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,405,typename A::_txy,no_unit,no_unit> fcr_mip_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,406,typename A::_txy,no_unit,mw> p_fcr_min{this}; // x[no_unit],y[mw]
    rw<pump,407,typename A::_txy,no_unit,mw> p_fcr_max{this}; // x[no_unit],y[mw]
    rw<pump,408,typename A::_txy,no_unit,mw> p_rr_min{this}; // x[no_unit],y[mw]
    rw<pump,409,typename A::_txy,no_unit,mw> frr_up_min{this}; // x[no_unit],y[mw]
    rw<pump,410,typename A::_txy,no_unit,mw> frr_up_max{this}; // x[no_unit],y[mw]
    rw<pump,411,typename A::_txy,no_unit,mw> frr_down_min{this}; // x[no_unit],y[mw]
    rw<pump,412,typename A::_txy,no_unit,mw> frr_down_max{this}; // x[no_unit],y[mw]
    rw<pump,413,typename A::_txy,no_unit,mw> fcr_n_up_min{this}; // x[no_unit],y[mw]
    rw<pump,414,typename A::_txy,no_unit,mw> fcr_n_up_max{this}; // x[no_unit],y[mw]
    rw<pump,415,typename A::_txy,no_unit,mw> fcr_n_down_min{this}; // x[no_unit],y[mw]
    rw<pump,416,typename A::_txy,no_unit,mw> fcr_n_down_max{this}; // x[no_unit],y[mw]
    rw<pump,417,typename A::_txy,no_unit,mw> fcr_d_up_min{this}; // x[no_unit],y[mw]
    rw<pump,418,typename A::_txy,no_unit,mw> fcr_d_up_max{this}; // x[no_unit],y[mw]
    rw<pump,419,typename A::_txy,no_unit,mw> fcr_d_down_min{this}; // x[no_unit],y[mw]
    rw<pump,420,typename A::_txy,no_unit,mw> fcr_d_down_max{this}; // x[no_unit],y[mw]
    rw<pump,421,typename A::_txy,no_unit,mw> rr_up_min{this}; // x[no_unit],y[mw]
    rw<pump,422,typename A::_txy,no_unit,mw> rr_up_max{this}; // x[no_unit],y[mw]
    rw<pump,423,typename A::_txy,no_unit,mw> rr_down_min{this}; // x[no_unit],y[mw]
    rw<pump,424,typename A::_txy,no_unit,mw> rr_down_max{this}; // x[no_unit],y[mw]
    rw<pump,425,typename A::_txy,no_unit,mw> fcr_n_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,426,typename A::_txy,no_unit,no_unit> fcr_n_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,427,typename A::_txy,no_unit,mw> fcr_n_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,428,typename A::_txy,no_unit,no_unit> fcr_n_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,429,typename A::_txy,no_unit,mw> fcr_d_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,430,typename A::_txy,no_unit,no_unit> fcr_d_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,431,typename A::_txy,no_unit,mw> fcr_d_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,432,typename A::_txy,no_unit,no_unit> fcr_d_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,433,typename A::_txy,no_unit,mw> frr_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,434,typename A::_txy,no_unit,no_unit> frr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,435,typename A::_txy,no_unit,mw> frr_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,436,typename A::_txy,no_unit,no_unit> frr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,437,typename A::_txy,no_unit,mw> rr_up_schedule{this}; // x[no_unit],y[mw]
    rw<pump,438,typename A::_txy,no_unit,no_unit> rr_up_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,439,typename A::_txy,no_unit,mw> rr_down_schedule{this}; // x[no_unit],y[mw]
    rw<pump,440,typename A::_txy,no_unit,no_unit> rr_down_schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,441,typename A::_txy,no_unit,nok> droop_cost{this}; // x[no_unit],y[nok]
    rw<pump,442,typename A::_txy,no_unit,no_unit> fixed_droop{this}; // x[no_unit],y[no_unit]
    rw<pump,443,typename A::_txy,no_unit,no_unit> fixed_droop_flag{this}; // x[no_unit],y[no_unit]
    rw<pump,444,typename A::_txy,no_unit,no_unit> droop_min{this}; // x[no_unit],y[no_unit]
    rw<pump,445,typename A::_txy,no_unit,no_unit> droop_max{this}; // x[no_unit],y[no_unit]
    rw<pump,446,typename A::_txy,no_unit,nok_per_mw> reserve_ramping_cost_up{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,447,typename A::_txy,no_unit,nok_per_mw> reserve_ramping_cost_down{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,448,vector<double>,no_unit,no_unit> discrete_droop_values{this}; // x[no_unit],y[no_unit]
    rw<pump,449,typename A::_txy,no_unit,nok_per_mw> fcr_n_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,450,typename A::_txy,no_unit,nok_per_mw> fcr_n_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,451,typename A::_txy,no_unit,nok_per_mw> fcr_d_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,452,typename A::_txy,no_unit,nok_per_mw> fcr_d_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,453,typename A::_txy,no_unit,nok_per_mw> frr_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,454,typename A::_txy,no_unit,nok_per_mw> frr_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,455,typename A::_txy,no_unit,nok_per_mw> rr_up_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,456,typename A::_txy,no_unit,nok_per_mw> rr_down_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<pump,457,typename A::_txy,no_unit,mw> spinning_reserve_up_max{this}; // x[no_unit],y[mw]
    rw<pump,458,typename A::_txy,no_unit,mw> spinning_reserve_down_max{this}; // x[no_unit],y[mw]
    ro<pump,459,typename A::_txy,no_unit,meter> eff_head{this}; // x[no_unit],y[meter]
    ro<pump,460,typename A::_txy,no_unit,meter> head_loss{this}; // x[no_unit],y[meter]
    ro<pump,461,typename A::_txy,no_unit,mw> consumption{this}; // x[no_unit],y[mw]
    ro<pump,462,typename A::_txy,no_unit,mw> sim_consumption{this}; // x[no_unit],y[mw]
    ro<pump,463,typename A::_txy,no_unit,mw> solver_consumption{this}; // x[no_unit],y[mw]
    ro<pump,464,typename A::_txy,no_unit,m3_per_s> upflow{this}; // x[no_unit],y[m3_per_s]
    ro<pump,465,typename A::_txy,no_unit,m3_per_s> sim_upflow{this}; // x[no_unit],y[m3_per_s]
    ro<pump,466,typename A::_txy,no_unit,m3_per_s> solver_upflow{this}; // x[no_unit],y[m3_per_s]
    ro<pump,467,typename A::_txy,no_unit,no_unit> committed_out{this}; // x[no_unit],y[no_unit]
    ro<pump,468,typename A::_txy,no_unit,mw> consumption_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,469,typename A::_txy,no_unit,mm3> upflow_schedule_penalty{this}; // x[no_unit],y[mm3]
    ro<pump,470,typename A::_txy,no_unit,mw> fcr_n_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,471,typename A::_txy,no_unit,mw> fcr_n_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,472,typename A::_txy,no_unit,mw> fcr_d_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,473,typename A::_txy,no_unit,mw> fcr_d_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,474,typename A::_txy,no_unit,mw> frr_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,475,typename A::_txy,no_unit,mw> frr_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,476,typename A::_txy,no_unit,mw> rr_up_delivery{this}; // x[no_unit],y[mw]
    ro<pump,477,typename A::_txy,no_unit,mw> rr_down_delivery{this}; // x[no_unit],y[mw]
    ro<pump,478,typename A::_txy,no_unit,mw> fcr_n_up_delivery_physical{this}; // x[no_unit],y[mw]
    ro<pump,479,typename A::_txy,no_unit,mw> fcr_n_down_delivery_physical{this}; // x[no_unit],y[mw]
    ro<pump,480,typename A::_txy,no_unit,mw> fcr_d_up_delivery_physical{this}; // x[no_unit],y[mw]
    ro<pump,481,typename A::_txy,no_unit,mw> fcr_d_down_delivery_physical{this}; // x[no_unit],y[mw]
    ro<pump,482,typename A::_txy,no_unit,mw> fcr_n_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,483,typename A::_txy,no_unit,mw> fcr_n_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,484,typename A::_txy,no_unit,mw> fcr_d_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,485,typename A::_txy,no_unit,mw> fcr_d_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,486,typename A::_txy,no_unit,mw> frr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,487,typename A::_txy,no_unit,mw> frr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,488,typename A::_txy,no_unit,mw> rr_up_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,489,typename A::_txy,no_unit,mw> rr_down_schedule_penalty{this}; // x[no_unit],y[mw]
    ro<pump,490,typename A::_txy,no_unit,no_unit> droop_result{this}; // x[no_unit],y[no_unit]
    ro<pump,491,map<int64_t, typename A::_xy>,m3_per_s,mw> original_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<pump,492,map<int64_t, typename A::_xy>,m3_per_s,mw> convex_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<pump,493,map<int64_t, typename A::_xy>,m3_per_s,mw> final_pq_curves{this}; // x[m3_per_s],y[mw]
    ro<pump,494,typename A::_txy,no_unit,mw> max_cons{this}; // x[no_unit],y[mw]
    ro<pump,495,typename A::_txy,no_unit,mw> min_cons{this}; // x[no_unit],y[mw]
};
template<class A>
struct gate:obj<A,5> {
    using super=obj<A,5>;
    gate()=default;
    gate(A* s,int oid):super(s, oid) {}
    gate(const gate& o):super(o) {}
    gate(gate&& o):super(std::move(o)) {}
    gate& operator=(const gate& o) {
        super::operator=(o);
        return *this;
    }
    gate& operator=(gate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<gate,498,int,no_unit,no_unit> type{this}; // x[no_unit],y[no_unit]
    rw<gate,500,int,no_unit,no_unit> time_delay{this}; // x[no_unit],y[no_unit]
    rw<gate,501,int,no_unit,no_unit> add_slack{this}; // x[no_unit],y[no_unit]
    rw<gate,502,double,m3_per_s,m3_per_s> max_discharge{this}; // x[m3_per_s],y[m3_per_s]
    rw<gate,505,double,no_unit,no_unit> lin_rel_a{this}; // x[no_unit],y[no_unit]
    rw<gate,506,double,mm3,mm3> lin_rel_b{this}; // x[mm3],y[mm3]
    rw<gate,507,typename A::_xy,hour,no_unit> shape_discharge{this}; // x[hour],y[no_unit]
    rw<gate,508,typename A::_xy,m3_per_s,nok_per_m3_per_s> spill_cost_curve{this}; // x[m3_per_s],y[nok_per_m3_per_s]
    rw<gate,509,typename A::_xy,m3_per_s,nok_per_m3_per_s> peak_flow_cost_curve{this}; // x[m3_per_s],y[nok_per_m3_per_s]
    ro<gate,510,typename A::_txy,no_unit,nok> peak_flow_penalty{this}; // x[no_unit],y[nok]
    rw<gate,511,vector<typename A::_xy>,meter,m3_per_s> functions_meter_m3s{this}; // x[meter],y[m3_per_s]
    rw<gate,512,vector<typename A::_xy>,delta_meter,m3_per_s> functions_deltameter_m3s{this}; // x[delta_meter],y[m3_per_s]
    rw<gate,513,typename A::_txy,no_unit,m3_per_s> min_flow{this}; // x[no_unit],y[m3_per_s]
    rw<gate,514,typename A::_txy,no_unit,no_unit> min_flow_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,515,typename A::_txy,no_unit,m3_per_s> max_flow{this}; // x[no_unit],y[m3_per_s]
    rw<gate,516,typename A::_txy,no_unit,no_unit> max_flow_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,517,typename A::_txy,no_unit,m3_per_s> schedule_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<gate,518,typename A::_txy,no_unit,percent> schedule_percent{this}; // x[no_unit],y[percent]
    rw<gate,519,typename A::_txy,no_unit,no_unit> schedule_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,520,typename A::_txy,no_unit,no_unit> setting{this}; // x[no_unit],y[no_unit]
    rw<gate,521,typename A::_txy,no_unit,no_unit> setting_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,522,typename A::_txy,no_unit,nok_per_mm3> discharge_fee{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,523,typename A::_txy,no_unit,no_unit> discharge_fee_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,524,typename A::_txy,no_unit,m3_per_s> block_merge_tolerance{this}; // x[no_unit],y[m3_per_s]
    rw<gate,525,typename A::_txy,no_unit,no_unit> min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,526,typename A::_txy,no_unit,no_unit> max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,527,typename A::_txy,no_unit,m3sec_hour> ramping_up{this}; // x[no_unit],y[m3sec_hour]
    rw<gate,528,typename A::_txy,no_unit,no_unit> ramping_up_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,529,typename A::_txy,no_unit,m3sec_hour> ramping_down{this}; // x[no_unit],y[m3sec_hour]
    rw<gate,530,typename A::_txy,no_unit,no_unit> ramping_down_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,531,typename A::_txy,no_unit,nok_per_m3_per_s> ramp_penalty_cost{this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<gate,532,typename A::_txy,no_unit,no_unit> ramp_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,533,typename A::_txy,no_unit,nok_per_mm3> max_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,534,typename A::_txy,no_unit,nok_per_mm3> min_q_penalty_cost{this}; // x[no_unit],y[nok_per_mm3]
    rw<gate,535,typename A::_txy,no_unit,no_unit> max_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    rw<gate,536,typename A::_txy,no_unit,no_unit> min_q_penalty_cost_flag{this}; // x[no_unit],y[no_unit]
    ro<gate,537,typename A::_txy,no_unit,nok> min_q_penalty{this}; // x[no_unit],y[nok]
    ro<gate,538,typename A::_txy,no_unit,nok> max_q_penalty{this}; // x[no_unit],y[nok]
    ro<gate,539,typename A::_txy,no_unit,m3_per_s> discharge{this}; // x[no_unit],y[m3_per_s]
    ro<gate,540,typename A::_txy,no_unit,m3_per_s> sim_discharge{this}; // x[no_unit],y[m3_per_s]
};
template<class A>
struct thermal:obj<A,6> {
    using super=obj<A,6>;
    thermal()=default;
    thermal(A* s,int oid):super(s, oid) {}
    thermal(const thermal& o):super(o) {}
    thermal(thermal&& o):super(std::move(o)) {}
    thermal& operator=(const thermal& o) {
        super::operator=(o);
        return *this;
    }
    thermal& operator=(thermal&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<thermal,1060,typename A::_txy,nok_per_mwh,nok_per_mwh> fuel_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<thermal,1061,typename A::_txy,nok_per_mwh,nok_per_mwh> quadratic_fuel_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<thermal,1062,int,no_unit,no_unit> n_segments{this}; // x[no_unit],y[no_unit]
    rw<thermal,1063,typename A::_txy,mw,mw> min_prod{this}; // x[mw],y[mw]
    rw<thermal,1064,typename A::_txy,mw,mw> max_prod{this}; // x[mw],y[mw]
    rw<thermal,1065,typename A::_txy,nok,nok> startcost{this}; // x[nok],y[nok]
    rw<thermal,1066,typename A::_txy,nok,nok> stopcost{this}; // x[nok],y[nok]
    ro<thermal,1067,typename A::_txy,mw,mw> production{this}; // x[mw],y[mw]
};
template<class A>
struct junction:obj<A,7> {
    using super=obj<A,7>;
    junction()=default;
    junction(A* s,int oid):super(s, oid) {}
    junction(const junction& o):super(o) {}
    junction(junction&& o):super(std::move(o)) {}
    junction& operator=(const junction& o) {
        super::operator=(o);
        return *this;
    }
    junction& operator=(junction&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<junction,556,int,no_unit,no_unit> junc_slack{this}; // x[no_unit],y[no_unit]
    rw<junction,557,double,meter,meter> altitude{this}; // x[meter],y[meter]
    ro<junction,558,typename A::_txy,no_unit,m3_per_s> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction,559,typename A::_txy,no_unit,m3_per_s> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    ro<junction,560,typename A::_txy,no_unit,m3_per_s> sim_tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction,561,typename A::_txy,no_unit,m3_per_s> sim_tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    rw<junction,562,double,s2_per_m5,s2_per_m5> loss_factor_1{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<junction,563,double,s2_per_m5,s2_per_m5> loss_factor_2{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<junction,564,typename A::_txy,no_unit,meter> min_pressure{this}; // x[no_unit],y[meter]
    ro<junction,565,typename A::_txy,no_unit,meter> pressure_height{this}; // x[no_unit],y[meter]
    ro<junction,566,typename A::_txy,no_unit,meter> sim_pressure_height{this}; // x[no_unit],y[meter]
    ro<junction,567,typename A::_txy,no_unit,nok> incr_cost{this}; // x[no_unit],y[nok]
    ro<junction,568,typename A::_txy,no_unit,nok> local_incr_cost{this}; // x[no_unit],y[nok]
};
template<class A>
struct junction_gate:obj<A,8> {
    using super=obj<A,8>;
    junction_gate()=default;
    junction_gate(A* s,int oid):super(s, oid) {}
    junction_gate(const junction_gate& o):super(o) {}
    junction_gate(junction_gate&& o):super(std::move(o)) {}
    junction_gate& operator=(const junction_gate& o) {
        super::operator=(o);
        return *this;
    }
    junction_gate& operator=(junction_gate&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<junction_gate,543,int,no_unit,no_unit> add_slack{this}; // x[no_unit],y[no_unit]
    rw<junction_gate,544,double,meter,meter> height_1{this}; // x[meter],y[meter]
    rw<junction_gate,545,double,s2_per_m5,s2_per_m5> loss_factor_1{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<junction_gate,546,double,s2_per_m5,s2_per_m5> loss_factor_2{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<junction_gate,547,typename A::_txy,no_unit,no_unit> schedule{this}; // x[no_unit],y[no_unit]
    ro<junction_gate,548,typename A::_txy,no_unit,meter> pressure_height{this}; // x[no_unit],y[meter]
    ro<junction_gate,549,typename A::_txy,no_unit,m3_per_s> tunnel_flow_1{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate,550,typename A::_txy,no_unit,m3_per_s> tunnel_flow_2{this}; // x[no_unit],y[m3_per_s]
    ro<junction_gate,551,typename A::_txy,no_unit,meter> tunnel_loss_1{this}; // x[no_unit],y[meter]
    ro<junction_gate,552,typename A::_txy,no_unit,meter> tunnel_loss_2{this}; // x[no_unit],y[meter]
};
template<class A>
struct creek_intake:obj<A,9> {
    using super=obj<A,9>;
    creek_intake()=default;
    creek_intake(A* s,int oid):super(s, oid) {}
    creek_intake(const creek_intake& o):super(o) {}
    creek_intake(creek_intake&& o):super(std::move(o)) {}
    creek_intake& operator=(const creek_intake& o) {
        super::operator=(o);
        return *this;
    }
    creek_intake& operator=(creek_intake&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<creek_intake,571,double,meter,meter> net_head{this}; // x[meter],y[meter]
    rw<creek_intake,572,double,m3_per_s,m3_per_s> max_inflow{this}; // x[m3_per_s],y[m3_per_s]
    rw<creek_intake,573,typename A::_txy,no_unit,m3_per_s> max_inflow_dynamic{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,574,typename A::_txy,no_unit,m3_per_s> inflow{this}; // x[no_unit],y[m3_per_s]
    ro<creek_intake,575,typename A::_txy,no_unit,m3_per_s> sim_inflow{this}; // x[no_unit],y[m3_per_s]
    ro<creek_intake,576,typename A::_txy,no_unit,meter> sim_pressure_height{this}; // x[no_unit],y[meter]
    rw<creek_intake,577,typename A::_txy,no_unit,m3_per_s> inflow_percentage{this}; // x[no_unit],y[m3_per_s]
    rw<creek_intake,578,typename A::_txy,no_unit,nok_per_mm3> overflow_cost{this}; // x[no_unit],y[nok_per_mm3]
    ro<creek_intake,579,typename A::_txy,no_unit,no_unit> non_physical_overflow_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct contract:obj<A,10> {
    using super=obj<A,10>;
    contract()=default;
    contract(A* s,int oid):super(s, oid) {}
    contract(const contract& o):super(o) {}
    contract(contract&& o):super(std::move(o)) {}
    contract& operator=(const contract& o) {
        super::operator=(o);
        return *this;
    }
    contract& operator=(contract&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<contract,1102,double,mw,mw> initial_trade{this}; // x[mw],y[mw]
    rw<contract,1103,map<int64_t, typename A::_xy>,mw,nok_per_mwh> trade_curve{this}; // x[mw],y[nok_per_mwh]
    rw<contract,1104,typename A::_txy,no_unit,mw> min_trade{this}; // x[no_unit],y[mw]
    rw<contract,1105,typename A::_txy,no_unit,mw> max_trade{this}; // x[no_unit],y[mw]
    rw<contract,1106,typename A::_txy,no_unit,mw_hour> ramping_up{this}; // x[no_unit],y[mw_hour]
    rw<contract,1107,typename A::_txy,no_unit,mw_hour> ramping_down{this}; // x[no_unit],y[mw_hour]
    rw<contract,1108,typename A::_txy,no_unit,nok_per_mw> ramping_up_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<contract,1109,typename A::_txy,no_unit,nok_per_mw> ramping_down_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    ro<contract,1110,typename A::_txy,no_unit,mw> trade{this}; // x[no_unit],y[mw]
    ro<contract,1111,typename A::_txy,no_unit,nok> ramping_up_penalty{this}; // x[no_unit],y[nok]
    ro<contract,1112,typename A::_txy,no_unit,nok> ramping_down_penalty{this}; // x[no_unit],y[nok]
};
template<class A>
struct network:obj<A,11> {
    using super=obj<A,11>;
    network()=default;
    network(A* s,int oid):super(s, oid) {}
    network(const network& o):super(o) {}
    network(network&& o):super(std::move(o)) {}
    network& operator=(const network& o) {
        super::operator=(o);
        return *this;
    }
    network& operator=(network&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct market:obj<A,12> {
    using super=obj<A,12>;
    market()=default;
    market(A* s,int oid):super(s, oid) {}
    market(const market& o):super(o) {}
    market(market&& o):super(std::move(o)) {}
    market& operator=(const market& o) {
        super::operator=(o);
        return *this;
    }
    market& operator=(market&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<market,580,int,no_unit,no_unit> prod_area{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<market,581,string,no_unit,no_unit> market_type{this}; // x[no_unit],y[no_unit]
    rw<market,582,typename A::_txy,no_unit,mw> load{this}; // x[no_unit],y[mw]
    rw<market,583,typename A::_txy,no_unit,mw> max_buy{this}; // x[no_unit],y[mw]
    rw<market,584,typename A::_txy,no_unit,mw> max_sale{this}; // x[no_unit],y[mw]
    rw<market,585,typename A::_txy,no_unit,nok_per_mwh> load_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,586,typename A::_txy,no_unit,nok_per_mwh> buy_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,587,typename A::_txy,no_unit,nok_per_mwh> sale_price{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,588,typename A::_txy,no_unit,nok_per_mwh> buy_delta{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,589,typename A::_txy,no_unit,nok_per_mwh> sale_delta{this}; // x[no_unit],y[nok_per_mwh]
    rw<market,590,typename A::_txy,no_unit,no_unit> bid_flag{this}; // x[no_unit],y[no_unit]
    rw<market,591,typename A::_txy,no_unit,no_unit> common_scenario{this}; // x[no_unit],y[no_unit]
    ro<market,592,typename A::_txy,no_unit,mw> buy{this}; // x[no_unit],y[mw]
    ro<market,593,typename A::_txy,no_unit,mw> sale{this}; // x[no_unit],y[mw]
    ro<market,594,typename A::_txy,no_unit,mw> sim_sale{this}; // x[no_unit],y[mw]
    ro<market,595,typename A::_txy,no_unit,mw> reserve_obligation_penalty{this}; // x[no_unit],y[mw]
    ro<market,596,typename A::_txy,no_unit,mw> load_penalty{this}; // x[no_unit],y[mw]
};
template<class A>
struct global_settings:obj<A,13> {
    using super=obj<A,13>;
    global_settings()=default;
    global_settings(A* s,int oid):super(s, oid) {}
    global_settings(const global_settings& o):super(o) {}
    global_settings(global_settings&& o):super(std::move(o)) {}
    global_settings& operator=(const global_settings& o) {
        super::operator=(o);
        return *this;
    }
    global_settings& operator=(global_settings&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<global_settings,767,int,no_unit,no_unit> load_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,768,int,no_unit,no_unit> rsv_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,769,int,no_unit,no_unit> volume_ramp_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,770,int,no_unit,no_unit> level_ramp_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,771,int,no_unit,no_unit> production_ramp_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,772,int,no_unit,no_unit> plant_min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,773,int,no_unit,no_unit> plant_min_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,774,int,no_unit,no_unit> plant_max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,775,int,no_unit,no_unit> plant_max_p_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,776,int,no_unit,no_unit> gate_min_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,777,int,no_unit,no_unit> gate_max_q_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,778,int,no_unit,no_unit> gate_ramp_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,779,int,no_unit,no_unit> plant_schedule_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,780,int,no_unit,no_unit> gen_discharge_schedule_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,781,int,no_unit,no_unit> pump_schedule_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,782,int,no_unit,no_unit> power_limit_penalty_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,783,double,nok_per_mwh,nok_per_mwh> power_limit_penalty_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings,784,double,nok_per_mwh,nok_per_mwh> load_penalty_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings,785,double,nok_per_mm3,nok_per_mm3> rsv_penalty_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,786,double,nok_per_mm3,nok_per_mm3> rsv_hard_limit_penalty_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,787,double,nok_per_mm3,nok_per_mm3> volume_ramp_penalty_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,788,double,nok_per_meter,nok_per_meter> level_ramp_penalty_cost{this}; // x[nok_per_meter],y[nok_per_meter]
    rw<global_settings,789,double,nok_per_mwh,nok_per_mwh> production_ramp_penalty_cost{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings,790,double,nok_per_mwh,nok_per_mwh> plant_soft_p_penalty{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<global_settings,791,double,nok_per_mm3,nok_per_mm3> plant_soft_q_penalty{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,792,double,nok,nok> plant_sched_penalty_cost_up{this}; // x[nok],y[nok]
    rw<global_settings,793,double,nok,nok> plant_sched_penalty_cost_down{this}; // x[nok],y[nok]
    rw<global_settings,794,double,nok_per_mm3,nok_per_mm3> gen_discharge_sched_penalty_cost_up{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,795,double,nok_per_mm3,nok_per_mm3> gen_discharge_sched_penalty_cost_down{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,796,double,nok,nok> pump_sched_penalty_cost_up{this}; // x[nok],y[nok]
    rw<global_settings,797,double,nok,nok> pump_sched_penalty_cost_down{this}; // x[nok],y[nok]
    rw<global_settings,798,double,nok_per_m3_per_s,nok_per_m3_per_s> gate_ramp_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings,799,double,nok_per_mm3,nok_per_mm3> discharge_group_penalty_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,800,double,nok_per_mw,nok_per_mw> reserve_schedule_penalty_cost{this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings,801,double,nok_per_mw,nok_per_mw> reserve_group_penalty_cost{this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings,802,double,nok_per_mm3,nok_per_mm3> bypass_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,803,double,nok_per_mm3,nok_per_mm3> gate_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,804,double,nok_per_mm3,nok_per_mm3> overflow_cost{this}; // x[nok_per_mm3],y[nok_per_mm3]
    rw<global_settings,805,double,nok_per_mm3h,nok_per_mm3h> overflow_cost_time_factor{this}; // x[nok_per_mm3h],y[nok_per_mm3h]
    rw<global_settings,806,double,nok_per_mw,nok_per_mw> gen_reserve_ramping_cost{this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings,807,double,nok_per_mw,nok_per_mw> pump_reserve_ramping_cost{this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings,808,double,nok,nok> reserve_contribution_cost{this}; // x[nok],y[nok]
    rw<global_settings,809,double,nok_per_m3_per_s,nok_per_m3_per_s> gate_ramp_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings,810,double,nok_per_mw,nok_per_mw> reserve_group_slack_cost{this}; // x[nok_per_mw],y[nok_per_mw]
    rw<global_settings,811,int,no_unit,no_unit> use_heuristic_basis{this}; // x[no_unit],y[no_unit]
    rw<global_settings,812,int,no_unit,no_unit> nodelog{this}; // x[no_unit],y[no_unit]
    rw<global_settings,813,int,no_unit,no_unit> max_num_threads{this}; // x[no_unit],y[no_unit]
    rw<global_settings,814,int,no_unit,no_unit> parallelmode{this}; // x[no_unit],y[no_unit]
    rw<global_settings,815,double,second,second> timelimit{this}; // x[second],y[second]
    rw<global_settings,816,double,no_unit,no_unit> mipgap_rel{this}; // x[no_unit],y[no_unit]
    rw<global_settings,817,double,nok,nok> mipgap_abs{this}; // x[nok],y[nok]
    rw<global_settings,818,double,no_unit,no_unit> inteps{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,819,string,no_unit,no_unit> input_basis_name{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,820,string,no_unit,no_unit> output_basis_name{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,821,string,no_unit,no_unit> solver_algorithm{this}; // x[no_unit],y[no_unit]
    rw<global_settings,822,int,no_unit,no_unit> n_seg_up{this}; // x[no_unit],y[no_unit]
    rw<global_settings,823,int,no_unit,no_unit> n_seg_down{this}; // x[no_unit],y[no_unit]
    rw<global_settings,824,int,no_unit,no_unit> n_mip_seg_up{this}; // x[no_unit],y[no_unit]
    rw<global_settings,825,int,no_unit,no_unit> n_mip_seg_down{this}; // x[no_unit],y[no_unit]
    rw<global_settings,826,int,no_unit,no_unit> dyn_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,827,int,no_unit,no_unit> dyn_mip_pq_seg_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,828,int,no_unit,no_unit> bypass_segments{this}; // x[no_unit],y[no_unit]
    rw<global_settings,829,int,no_unit,no_unit> gate_segments{this}; // x[no_unit],y[no_unit]
    rw<global_settings,830,int,no_unit,no_unit> overflow_segments{this}; // x[no_unit],y[no_unit]
    rw<global_settings,831,double,no_unit,no_unit> gravity{this}; // x[no_unit],y[no_unit]
    rw<global_settings,832,double,no_unit,no_unit> gen_reserve_min_free_cap_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings,833,double,no_unit,no_unit> fcr_n_band{this}; // x[no_unit],y[no_unit]
    rw<global_settings,834,double,no_unit,no_unit> fcr_d_band{this}; // x[no_unit],y[no_unit]
    rw<global_settings,835,int,no_unit,no_unit> universal_mip{this}; // x[no_unit],y[no_unit]
    rw<global_settings,836,int,no_unit,no_unit> universal_overflow_mip{this}; // x[no_unit],y[no_unit]
    rw<global_settings,837,int,no_unit,no_unit> linear_startup{this}; // x[no_unit],y[no_unit]
    rw<global_settings,838,int,no_unit,no_unit> dyn_flex_mip_steps{this}; // x[no_unit],y[no_unit]
    rw<global_settings,839,int,no_unit,no_unit> merge_blocks{this}; // x[no_unit],y[no_unit]
    rw<global_settings,840,int,no_unit,no_unit> power_head_optimization{this}; // x[no_unit],y[no_unit]
    rw<global_settings,841,double,no_unit,no_unit> droop_discretization_limit{this}; // x[no_unit],y[no_unit]
    rw<global_settings,842,double,no_unit,no_unit> droop_cost_exponent{this}; // x[no_unit],y[no_unit]
    rw<global_settings,843,double,no_unit,no_unit> droop_ref_value{this}; // x[no_unit],y[no_unit]
    rw<global_settings,844,int,no_unit,no_unit> create_cuts{this}; // x[no_unit],y[no_unit]
    rw<global_settings,845,int,no_unit,no_unit> print_sim_inflow{this}; // x[no_unit],y[no_unit]
    rw<global_settings,846,int,no_unit,no_unit> pump_head_optimization{this}; // x[no_unit],y[no_unit]
    rw<global_settings,847,int,no_unit,no_unit> save_pq_curves{this}; // x[no_unit],y[no_unit]
    rw<global_settings,848,int,no_unit,no_unit> build_original_pq_curves_by_turb_eff{this}; // x[no_unit],y[no_unit]
    rw<global_settings,849,int,no_unit,no_unit> plant_unbalance_recommit{this}; // x[no_unit],y[no_unit]
    rw<global_settings,850,int,no_unit,no_unit> prefer_start_vol{this}; // x[no_unit],y[no_unit]
    rw<global_settings,851,int,no_unit,no_unit> bp_print_discharge{this}; // x[no_unit],y[no_unit]
    rw<global_settings,852,int,no_unit,no_unit> prod_from_ref_prod{this}; // x[no_unit],y[no_unit]
    rw<global_settings,853,int,no_unit,no_unit> bp_min_points{this}; // x[no_unit],y[no_unit]
    rw<global_settings,854,int,no_unit,no_unit> bp_mode{this}; // x[no_unit],y[no_unit]
    rw<global_settings,855,int,no_unit,no_unit> stop_cost_from_start_cost{this}; // x[no_unit],y[no_unit]
    rw<global_settings,856,int,no_unit,no_unit> ownership_scaling{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,857,string,no_unit,no_unit> time_delay_unit{this}; // x[no_unit],y[no_unit]
    rw<global_settings,858,int,no_unit,no_unit> bypass_loss{this}; // x[no_unit],y[no_unit]
    rw<global_settings,859,double,no_unit,no_unit> gen_turn_off_limit{this}; // x[no_unit],y[no_unit]
    rw<global_settings,860,double,no_unit,no_unit> pump_turn_off_limit{this}; // x[no_unit],y[no_unit]
    rw<global_settings,861,int,no_unit,no_unit> fcr_n_equality_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,862,int,no_unit,no_unit> delay_valuation_mode{this}; // x[no_unit],y[no_unit]
    rw<global_settings,863,int,no_unit,no_unit> universal_affinity_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,864,int,no_unit,no_unit> ramp_code{this}; // x[no_unit],y[no_unit]
    rw<global_settings,865,int,no_unit,no_unit> print_original_pq_curves{this}; // x[no_unit],y[no_unit]
    rw<global_settings,866,int,no_unit,no_unit> print_convex_pq_curves{this}; // x[no_unit],y[no_unit]
    rw<global_settings,867,int,no_unit,no_unit> print_final_pq_curves{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,868,string,no_unit,no_unit> shop_log_name{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,869,string,no_unit,no_unit> shop_yaml_log_name{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,870,string,no_unit,no_unit> solver_log_name{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,871,string,no_unit,no_unit> model_file_name{this}; // x[no_unit],y[no_unit]
    //--TODO: rw<global_settings,872,string,no_unit,no_unit> pq_curves_name{this}; // x[no_unit],y[no_unit]
    rw<global_settings,873,int,no_unit,no_unit> print_loss{this}; // x[no_unit],y[no_unit]
    rw<global_settings,874,int,no_unit,no_unit> shop_xmllog{this}; // x[no_unit],y[no_unit]
    rw<global_settings,875,int,no_unit,no_unit> print_optimized_startup_costs{this}; // x[no_unit],y[no_unit]
    rw<global_settings,876,int,no_unit,no_unit> get_duals_from_mip{this}; // x[no_unit],y[no_unit]
    rw<global_settings,877,int,no_unit,no_unit> bid_aggregation_level{this}; // x[no_unit],y[no_unit]
    rw<global_settings,878,int,no_unit,no_unit> simple_pq_recovery{this}; // x[no_unit],y[no_unit]
    rw<global_settings,879,int,no_unit,no_unit> rr_up_schedule_slack_flag{this}; // x[no_unit],y[no_unit]
    ro<global_settings,880,int,no_unit,no_unit> bp_ref_mc_from_market{this}; // x[no_unit],y[no_unit]
    rw<global_settings,881,int,no_unit,no_unit> bp_bid_matrix_points{this}; // x[no_unit],y[no_unit]
    rw<global_settings,882,double,no_unit,no_unit> ramp_scale_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings,883,double,nok_per_m3_per_s,nok_per_m3_per_s> river_flow_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings,884,double,nok_per_m3_per_s,nok_per_m3_per_s> river_flow_schedule_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<global_settings,885,int,no_unit,no_unit> recommit{this}; // x[no_unit],y[no_unit]
    rw<global_settings,886,int,no_unit,no_unit> build_pq_curve_eff_order{this}; // x[no_unit],y[no_unit]
    ro<global_settings,887,int,no_unit,no_unit> overflow_cut_description{this}; // x[no_unit],y[no_unit]
    ro<global_settings,888,int,no_unit,no_unit> dynamic_junction_loss{this}; // x[no_unit],y[no_unit]
    ro<global_settings,889,int,no_unit,no_unit> sim_gen_schedule_correction{this}; // x[no_unit],y[no_unit]
    ro<global_settings,890,int,no_unit,no_unit> bp_upload_from_zero{this}; // x[no_unit],y[no_unit]
    ro<global_settings,891,int,no_unit,no_unit> use_xml_system_name{this}; // x[no_unit],y[no_unit]
    ro<global_settings,892,int,no_unit,no_unit> max_iter{this}; // x[no_unit],y[no_unit]
    ro<global_settings,893,int,no_unit,no_unit> full_iter{this}; // x[no_unit],y[no_unit]
    ro<global_settings,894,int,no_unit,no_unit> finished{this}; // x[no_unit],y[no_unit]
    ro<global_settings,895,int,no_unit,no_unit> plant_code{this}; // x[no_unit],y[no_unit]
    rw<global_settings,896,int,no_unit,no_unit> tuning_code{this}; // x[no_unit],y[no_unit]
    rw<global_settings,897,int,no_unit,no_unit> tuning_mode{this}; // x[no_unit],y[no_unit]
    rw<global_settings,898,int,no_unit,no_unit> run_test{this}; // x[no_unit],y[no_unit]
    rw<global_settings,899,int,no_unit,no_unit> presim{this}; // x[no_unit],y[no_unit]
    rw<global_settings,900,int,no_unit,no_unit> head_optimize{this}; // x[no_unit],y[no_unit]
    rw<global_settings,901,int,no_unit,no_unit> head_optimize_junction{this}; // x[no_unit],y[no_unit]
    rw<global_settings,902,int,no_unit,no_unit> cont_if_feas{this}; // x[no_unit],y[no_unit]
    rw<global_settings,903,int,no_unit,no_unit> print_result{this}; // x[no_unit],y[no_unit]
    rw<global_settings,904,int,no_unit,no_unit> dp_history{this}; // x[no_unit],y[no_unit]
    rw<global_settings,905,int,no_unit,no_unit> save_case_next_flag{this}; // x[no_unit],y[no_unit]
    ro<global_settings,906,int,no_unit,no_unit> print_input_series{this}; // x[no_unit],y[no_unit]
    rw<global_settings,907,int,no_unit,no_unit> pq_upper_convex_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,908,int,no_unit,no_unit> pq_lower_convex_flag{this}; // x[no_unit],y[no_unit]
    rw<global_settings,909,int,no_unit,no_unit> scalemode{this}; // x[no_unit],y[no_unit]
    rw<global_settings,910,int,no_unit,no_unit> scalecount{this}; // x[no_unit],y[no_unit]
    rw<global_settings,911,int,no_unit,no_unit> solve_again{this}; // x[no_unit],y[no_unit]
    rw<global_settings,912,int,no_unit,no_unit> add_universal_penalty_vars{this}; // x[no_unit],y[no_unit]
    rw<global_settings,913,int,no_unit,no_unit> fractional_start_perturbation{this}; // x[no_unit],y[no_unit]
    rw<global_settings,914,int,no_unit,no_unit> min_lp_info{this}; // x[no_unit],y[no_unit]
    rw<global_settings,915,double,no_unit,no_unit> accuracy{this}; // x[no_unit],y[no_unit]
    rw<global_settings,916,double,no_unit,no_unit> iteration_gap_rel{this}; // x[no_unit],y[no_unit]
    rw<global_settings,917,double,no_unit,no_unit> iteration_gap_abs{this}; // x[no_unit],y[no_unit]
    rw<global_settings,918,double,no_unit,no_unit> mip_progress{this}; // x[no_unit],y[no_unit]
    rw<global_settings,919,double,no_unit,no_unit> mip_progress_time{this}; // x[no_unit],y[no_unit]
    rw<global_settings,920,double,no_unit,no_unit> tune_epsilon{this}; // x[no_unit],y[no_unit]
    rw<global_settings,921,double,no_unit,no_unit> bypass_maxcap{this}; // x[no_unit],y[no_unit]
    rw<global_settings,922,double,no_unit,no_unit> gate_maxcap{this}; // x[no_unit],y[no_unit]
    rw<global_settings,923,double,no_unit,no_unit> overflow_maxcap{this}; // x[no_unit],y[no_unit]
    rw<global_settings,924,double,no_unit,no_unit> reserve_penalty{this}; // x[no_unit],y[no_unit]
    rw<global_settings,925,double,no_unit,no_unit> default_water_value{this}; // x[no_unit],y[no_unit]
    rw<global_settings,926,double,no_unit,no_unit> headopt_feedback_factor{this}; // x[no_unit],y[no_unit]
    rw<global_settings,927,double,no_unit,no_unit> slack_coeff{this}; // x[no_unit],y[no_unit]
    rw<global_settings,928,double,no_unit,no_unit> gen_reserve_commitment_cost{this}; // x[no_unit],y[no_unit]
    rw<global_settings,929,double,no_unit,no_unit> gen_reserve_switch_cost{this}; // x[no_unit],y[no_unit]
    rw<global_settings,930,double,no_unit,no_unit> common_decision_penalty{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct reserve_group:obj<A,14> {
    using super=obj<A,14>;
    reserve_group()=default;
    reserve_group(A* s,int oid):super(s, oid) {}
    reserve_group(const reserve_group& o):super(o) {}
    reserve_group(reserve_group&& o):super(std::move(o)) {}
    reserve_group& operator=(const reserve_group& o) {
        super::operator=(o);
        return *this;
    }
    reserve_group& operator=(reserve_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<reserve_group,597,int,no_unit,no_unit> group_id{this}; // x[no_unit],y[no_unit]
    rw<reserve_group,598,typename A::_txy,no_unit,mw> fcr_n_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,599,typename A::_txy,no_unit,mw> fcr_n_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,600,typename A::_txy,no_unit,mw> fcr_d_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,601,typename A::_txy,no_unit,mw> fcr_d_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,602,typename A::_txy,no_unit,mw> frr_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,603,typename A::_txy,no_unit,mw> frr_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,604,typename A::_txy,no_unit,mw> rr_up_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,605,typename A::_txy,no_unit,mw> rr_down_obligation{this}; // x[no_unit],y[mw]
    rw<reserve_group,606,typename A::_txy,no_unit,nok_per_mw> fcr_n_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,607,typename A::_txy,no_unit,nok_per_mw> fcr_d_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,608,typename A::_txy,no_unit,nok_per_mw> frr_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<reserve_group,609,typename A::_txy,no_unit,nok_per_mw> rr_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    ro<reserve_group,610,typename A::_txy,no_unit,mw> fcr_n_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,611,typename A::_txy,no_unit,mw> fcr_n_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,612,typename A::_txy,no_unit,mw> fcr_d_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,613,typename A::_txy,no_unit,mw> fcr_d_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,614,typename A::_txy,no_unit,mw> frr_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,615,typename A::_txy,no_unit,mw> frr_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,616,typename A::_txy,no_unit,mw> rr_up_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,617,typename A::_txy,no_unit,mw> rr_down_slack{this}; // x[no_unit],y[mw]
    ro<reserve_group,618,typename A::_txy,no_unit,mw> fcr_n_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,619,typename A::_txy,no_unit,mw> fcr_n_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,620,typename A::_txy,no_unit,mw> fcr_d_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,621,typename A::_txy,no_unit,mw> fcr_d_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,622,typename A::_txy,no_unit,mw> frr_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,623,typename A::_txy,no_unit,mw> frr_down_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,624,typename A::_txy,no_unit,mw> rr_up_violation{this}; // x[no_unit],y[mw]
    ro<reserve_group,625,typename A::_txy,no_unit,mw> rr_down_violation{this}; // x[no_unit],y[mw]
};
template<class A>
struct commit_group:obj<A,15> {
    using super=obj<A,15>;
    commit_group()=default;
    commit_group(A* s,int oid):super(s, oid) {}
    commit_group(const commit_group& o):super(o) {}
    commit_group(commit_group&& o):super(std::move(o)) {}
    commit_group& operator=(const commit_group& o) {
        super::operator=(o);
        return *this;
    }
    commit_group& operator=(commit_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<commit_group,667,typename A::_txy,no_unit,no_unit> deactivate_exclusion_flag{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct discharge_group:obj<A,16> {
    using super=obj<A,16>;
    discharge_group()=default;
    discharge_group(A* s,int oid):super(s, oid) {}
    discharge_group(const discharge_group& o):super(o) {}
    discharge_group(discharge_group&& o):super(std::move(o)) {}
    discharge_group& operator=(const discharge_group& o) {
        super::operator=(o);
        return *this;
    }
    discharge_group& operator=(discharge_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<discharge_group,626,double,mm3,mm3> initial_deviation_mm3{this}; // x[mm3],y[mm3]
    rw<discharge_group,627,typename A::_txy,no_unit,mm3> max_accumulated_deviation_mm3_up{this}; // x[no_unit],y[mm3]
    rw<discharge_group,628,typename A::_txy,no_unit,mm3> max_accumulated_deviation_mm3_down{this}; // x[no_unit],y[mm3]
    rw<discharge_group,629,typename A::_txy,no_unit,m3_per_s> weighted_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group,630,typename A::_txy,no_unit,nok_per_mm3> penalty_cost_up_per_mm3{this}; // x[no_unit],y[nok_per_mm3]
    rw<discharge_group,631,typename A::_txy,no_unit,nok_per_mm3> penalty_cost_down_per_mm3{this}; // x[no_unit],y[nok_per_mm3]
    rw<discharge_group,632,typename A::_txy,no_unit,m3_per_s> min_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group,633,typename A::_txy,no_unit,m3_per_s> max_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    rw<discharge_group,634,typename A::_txy,no_unit,nok_per_h_per_m3_per_s> min_discharge_penalty_cost{this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group,635,typename A::_txy,no_unit,nok_per_h_per_m3_per_s> max_discharge_penalty_cost{this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group,636,typename A::_txy,no_unit,m3sec_hour> ramping_up_m3s{this}; // x[no_unit],y[m3sec_hour]
    rw<discharge_group,637,typename A::_txy,no_unit,m3sec_hour> ramping_down_m3s{this}; // x[no_unit],y[m3sec_hour]
    rw<discharge_group,638,typename A::_txy,no_unit,nok_per_h_per_m3_per_s> ramping_up_penalty_cost{this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    rw<discharge_group,639,typename A::_txy,no_unit,nok_per_h_per_m3_per_s> ramping_down_penalty_cost{this}; // x[no_unit],y[nok_per_h_per_m3_per_s]
    ro<discharge_group,640,typename A::_txy,no_unit,m3_per_s> actual_discharge_m3s{this}; // x[no_unit],y[m3_per_s]
    ro<discharge_group,641,typename A::_txy,no_unit,mm3> accumulated_deviation_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,642,typename A::_txy,no_unit,mm3> upper_penalty_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,643,typename A::_txy,no_unit,mm3> lower_penalty_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,644,typename A::_txy,no_unit,mm3> upper_slack_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,645,typename A::_txy,no_unit,mm3> lower_slack_mm3{this}; // x[no_unit],y[mm3]
    ro<discharge_group,646,typename A::_txy,no_unit,nok> min_discharge_penalty{this}; // x[no_unit],y[nok]
    ro<discharge_group,647,typename A::_txy,no_unit,nok> max_discharge_penalty{this}; // x[no_unit],y[nok]
    ro<discharge_group,648,typename A::_txy,no_unit,nok> ramping_up_penalty{this}; // x[no_unit],y[nok]
    ro<discharge_group,649,typename A::_txy,no_unit,nok> ramping_down_penalty{this}; // x[no_unit],y[nok]
};
template<class A>
struct production_group:obj<A,17> {
    using super=obj<A,17>;
    production_group()=default;
    production_group(A* s,int oid):super(s, oid) {}
    production_group(const production_group& o):super(o) {}
    production_group(production_group&& o):super(std::move(o)) {}
    production_group& operator=(const production_group& o) {
        super::operator=(o);
        return *this;
    }
    production_group& operator=(production_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<production_group,650,double,mwh,mwh> energy_target{this}; // x[mwh],y[mwh]
    rw<production_group,651,double,nok_per_mwh,nok_per_mwh> energy_penalty_cost_up{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<production_group,652,double,nok_per_mwh,nok_per_mwh> energy_penalty_cost_down{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<production_group,653,typename A::_txy,no_unit,mwh> energy_target_period_flag{this}; // x[no_unit],y[mwh]
    rw<production_group,654,typename A::_txy,no_unit,mw> max_p_limit{this}; // x[no_unit],y[mw]
    rw<production_group,655,typename A::_txy,no_unit,mw> min_p_limit{this}; // x[no_unit],y[mw]
    rw<production_group,656,typename A::_txy,no_unit,nok_per_mw> max_p_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    rw<production_group,657,typename A::_txy,no_unit,nok_per_mw> min_p_penalty_cost{this}; // x[no_unit],y[nok_per_mw]
    ro<production_group,658,typename A::_txy,no_unit,mw> sum_production{this}; // x[no_unit],y[mw]
    ro<production_group,659,typename A::_txy,no_unit,nok> min_p_penalty{this}; // x[no_unit],y[nok]
    ro<production_group,660,typename A::_txy,no_unit,nok> max_p_penalty{this}; // x[no_unit],y[nok]
    ro<production_group,661,typename A::_txy,nok,nok> energy_penalty_up{this}; // x[nok],y[nok]
    ro<production_group,662,typename A::_txy,nok,nok> energy_penalty_down{this}; // x[nok],y[nok]
};
template<class A>
struct volume_constraint:obj<A,18> {
    using super=obj<A,18>;
    volume_constraint()=default;
    volume_constraint(A* s,int oid):super(s, oid) {}
    volume_constraint(const volume_constraint& o):super(o) {}
    volume_constraint(volume_constraint&& o):super(std::move(o)) {}
    volume_constraint& operator=(const volume_constraint& o) {
        super::operator=(o);
        return *this;
    }
    volume_constraint& operator=(volume_constraint&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<volume_constraint,663,typename A::_txy,no_unit,mm3> min_vol{this}; // x[no_unit],y[mm3]
    rw<volume_constraint,664,typename A::_txy,no_unit,mm3> max_vol{this}; // x[no_unit],y[mm3]
    rw<volume_constraint,665,typename A::_txy,no_unit,nok_per_mm3> min_vol_penalty{this}; // x[no_unit],y[nok_per_mm3]
    rw<volume_constraint,666,typename A::_txy,no_unit,nok_per_mm3> max_vol_penalty{this}; // x[no_unit],y[nok_per_mm3]
};
template<class A>
struct scenario:obj<A,19> {
    using super=obj<A,19>;
    scenario()=default;
    scenario(A* s,int oid):super(s, oid) {}
    scenario(const scenario& o):super(o) {}
    scenario(scenario&& o):super(std::move(o)) {}
    scenario& operator=(const scenario& o) {
        super::operator=(o);
        return *this;
    }
    scenario& operator=(scenario&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<scenario,668,int,no_unit,no_unit> scenario_id{this}; // x[no_unit],y[no_unit]
    rw<scenario,669,typename A::_txy,no_unit,no_unit> probability{this}; // x[no_unit],y[no_unit]
    rw<scenario,670,typename A::_txy,no_unit,no_unit> common_scenario{this}; // x[no_unit],y[no_unit]
    rw<scenario,671,typename A::_txy,no_unit,no_unit> common_history{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct objective:obj<A,20> {
    using super=obj<A,20>;
    objective()=default;
    objective(A* s,int oid):super(s, oid) {}
    objective(const objective& o):super(o) {}
    objective(objective&& o):super(std::move(o)) {}
    objective& operator=(const objective& o) {
        super::operator=(o);
        return *this;
    }
    objective& operator=(objective&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: ro<objective,672,string,no_unit,no_unit> solver_status{this}; // x[no_unit],y[no_unit]
    ro<objective,673,int,no_unit,no_unit> times_of_wrong_pq_uploading{this}; // x[no_unit],y[no_unit]
    ro<objective,674,double,nok,nok> grand_total{this}; // x[nok],y[nok]
    ro<objective,675,double,nok,nok> sim_grand_total{this}; // x[nok],y[nok]
    ro<objective,676,double,nok,nok> total{this}; // x[nok],y[nok]
    ro<objective,677,double,nok,nok> sum_penalties{this}; // x[nok],y[nok]
    ro<objective,678,double,nok,nok> minor_penalties{this}; // x[nok],y[nok]
    ro<objective,679,double,nok,nok> major_penalties{this}; // x[nok],y[nok]
    ro<objective,680,double,nok,nok> rsv_end_value{this}; // x[nok],y[nok]
    ro<objective,681,double,nok,nok> sim_rsv_end_value{this}; // x[nok],y[nok]
    ro<objective,682,double,nok,nok> rsv_end_value_relative{this}; // x[nok],y[nok]
    ro<objective,683,double,nok,nok> vow_in_transit{this}; // x[nok],y[nok]
    ro<objective,684,double,nok,nok> rsv_spill_vol_end_value{this}; // x[nok],y[nok]
    ro<objective,685,double,nok,nok> market_sale_buy{this}; // x[nok],y[nok]
    ro<objective,686,double,nok,nok> sim_market_sale_buy{this}; // x[nok],y[nok]
    ro<objective,687,double,nok,nok> load_value{this}; // x[nok],y[nok]
    ro<objective,688,double,nok,nok> reserve_sale_buy{this}; // x[nok],y[nok]
    ro<objective,689,double,nok,nok> reserve_oblig_value{this}; // x[nok],y[nok]
    ro<objective,690,double,nok,nok> contract_value{this}; // x[nok],y[nok]
    ro<objective,691,double,nok,nok> startup_costs{this}; // x[nok],y[nok]
    ro<objective,692,double,nok,nok> sim_startup_costs{this}; // x[nok],y[nok]
    ro<objective,693,double,nok,nok> sum_feeding_fee{this}; // x[nok],y[nok]
    ro<objective,694,double,nok,nok> sum_discharge_fee{this}; // x[nok],y[nok]
    ro<objective,695,double,nok,nok> thermal_cost{this}; // x[nok],y[nok]
    ro<objective,696,double,nok,nok> reserve_allocation_cost{this}; // x[nok],y[nok]
    ro<objective,697,double,nok,nok> rsv_tactical_penalty{this}; // x[nok],y[nok]
    ro<objective,698,double,nok,nok> plant_p_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,699,double,nok,nok> plant_q_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,700,double,nok,nok> plant_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,701,double,nok,nok> plant_rsv_q_limit_penalty{this}; // x[nok],y[nok]
    ro<objective,702,double,nok,nok> gen_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,703,double,nok,nok> pump_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,704,double,nok,nok> gate_q_constr_penalty{this}; // x[nok],y[nok]
    ro<objective,705,double,nok,nok> gate_discharge_cost{this}; // x[nok],y[nok]
    ro<objective,706,double,nok,nok> bypass_cost{this}; // x[nok],y[nok]
    ro<objective,707,double,nok,nok> gate_spill_cost{this}; // x[nok],y[nok]
    ro<objective,708,double,nok,nok> physical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,709,double,mm3,mm3> physical_spill_volume{this}; // x[mm3],y[mm3]
    ro<objective,710,double,nok,nok> nonphysical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,711,double,mm3,mm3> nonphysical_spill_volume{this}; // x[mm3],y[mm3]
    ro<objective,712,double,nok,nok> gate_slack_cost{this}; // x[nok],y[nok]
    ro<objective,713,double,nok,nok> creek_spill_cost{this}; // x[nok],y[nok]
    ro<objective,714,double,nok,nok> creek_physical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,715,double,nok,nok> creek_nonphysical_spill_cost{this}; // x[nok],y[nok]
    ro<objective,716,double,nok,nok> junction_slack_cost{this}; // x[nok],y[nok]
    ro<objective,717,double,nok,nok> reserve_violation_penalty{this}; // x[nok],y[nok]
    ro<objective,718,double,nok,nok> reserve_slack_cost{this}; // x[nok],y[nok]
    ro<objective,719,double,nok,nok> reserve_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,720,double,nok,nok> rsv_peak_volume_penalty{this}; // x[nok],y[nok]
    ro<objective,721,double,nok,nok> gate_peak_flow_penalty{this}; // x[nok],y[nok]
    ro<objective,722,double,nok,nok> rsv_flood_volume_penalty{this}; // x[nok],y[nok]
    ro<objective,723,double,nok,nok> river_peak_flow_penalty{this}; // x[nok],y[nok]
    ro<objective,724,double,nok,nok> river_flow_penalty{this}; // x[nok],y[nok]
    ro<objective,725,double,nok,nok> rsv_penalty{this}; // x[nok],y[nok]
    ro<objective,726,double,nok,nok> rsv_hard_limit_penalty{this}; // x[nok],y[nok]
    ro<objective,727,double,nok,nok> rsv_over_limit_penalty{this}; // x[nok],y[nok]
    ro<objective,728,double,nok,nok> sim_rsv_penalty{this}; // x[nok],y[nok]
    ro<objective,729,double,nok,nok> rsv_end_penalty{this}; // x[nok],y[nok]
    ro<objective,730,double,nok,nok> load_penalty{this}; // x[nok],y[nok]
    ro<objective,731,double,nok,nok> group_time_period_penalty{this}; // x[nok],y[nok]
    ro<objective,732,double,nok,nok> group_time_step_penalty{this}; // x[nok],y[nok]
    ro<objective,733,double,nok,nok> sum_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,734,double,nok,nok> plant_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,735,double,nok,nok> rsv_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,736,double,nok,nok> gate_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,737,double,nok,nok> contract_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,738,double,nok,nok> group_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,739,double,nok,nok> discharge_group_penalty{this}; // x[nok],y[nok]
    ro<objective,740,double,nok,nok> discharge_group_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,741,double,nok,nok> production_group_energy_penalty{this}; // x[nok],y[nok]
    ro<objective,742,double,nok,nok> production_group_power_penalty{this}; // x[nok],y[nok]
    ro<objective,743,double,nok,nok> river_min_flow_penalty{this}; // x[nok],y[nok]
    ro<objective,744,double,nok,nok> river_max_flow_penalty{this}; // x[nok],y[nok]
    ro<objective,745,double,nok,nok> river_ramping_penalty{this}; // x[nok],y[nok]
    ro<objective,746,double,nok,nok> river_flow_schedule_penalty{this}; // x[nok],y[nok]
    ro<objective,747,double,nok,nok> river_gate_adjustment_penalty{this}; // x[nok],y[nok]
    ro<objective,748,double,nok,nok> common_decision_penalty{this}; // x[nok],y[nok]
    ro<objective,749,double,nok,nok> bidding_penalty{this}; // x[nok],y[nok]
    ro<objective,750,double,nok,nok> safe_mode_universal_penalty{this}; // x[nok],y[nok]
};
template<class A>
struct bid_group:obj<A,21> {
    using super=obj<A,21>;
    bid_group()=default;
    bid_group(A* s,int oid):super(s, oid) {}
    bid_group(const bid_group& o):super(o) {}
    bid_group(bid_group&& o):super(std::move(o)) {}
    bid_group& operator=(const bid_group& o) {
        super::operator=(o);
        return *this;
    }
    bid_group& operator=(bid_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<bid_group,752,int,no_unit,no_unit> price_dimension{this}; // x[no_unit],y[no_unit]
    ro<bid_group,753,int,no_unit,no_unit> time_dimension{this}; // x[no_unit],y[no_unit]
    ro<bid_group,754,int,no_unit,no_unit> bid_start_interval{this}; // x[no_unit],y[no_unit]
    ro<bid_group,755,int,no_unit,no_unit> bid_end_interval{this}; // x[no_unit],y[no_unit]
    ro<bid_group,757,double,no_unit,no_unit> reduction_cost{this}; // x[no_unit],y[no_unit]
    ro<bid_group,759,map<int64_t, typename A::_xy>,nok_per_mwh,mwh> bid_curves{this}; // x[nok_per_mwh],y[mwh]
    ro<bid_group,760,typename A::_txy,nok_per_mwh,mwh> bid_penalty{this}; // x[nok_per_mwh],y[mwh]
};
template<class A>
struct cut_group:obj<A,22> {
    using super=obj<A,22>;
    cut_group()=default;
    cut_group(A* s,int oid):super(s, oid) {}
    cut_group(const cut_group& o):super(o) {}
    cut_group(cut_group&& o):super(std::move(o)) {}
    cut_group& operator=(const cut_group& o) {
        super::operator=(o);
        return *this;
    }
    cut_group& operator=(cut_group&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<cut_group,761,vector<typename A::_xy>,no_unit,nok> rhs{this}; // x[no_unit],y[nok]
    ro<cut_group,762,typename A::_txy,no_unit,nok> end_value{this}; // x[no_unit],y[nok]
    ro<cut_group,763,typename A::_txy,no_unit,no_unit> binding_cut_up{this}; // x[no_unit],y[no_unit]
    ro<cut_group,764,typename A::_txy,no_unit,no_unit> binding_cut_down{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct inflow_series:obj<A,23> {
    using super=obj<A,23>;
    inflow_series()=default;
    inflow_series(A* s,int oid):super(s, oid) {}
    inflow_series(const inflow_series& o):super(o) {}
    inflow_series(inflow_series&& o):super(std::move(o)) {}
    inflow_series& operator=(const inflow_series& o) {
        super::operator=(o);
        return *this;
    }
    inflow_series& operator=(inflow_series&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<inflow_series,765,vector<typename A::_xy>,mm3,nok_per_mm3> cut_coeffs{this}; // x[mm3],y[nok_per_mm3]
};
template<class A>
struct system:obj<A,24> {
    using super=obj<A,24>;
    system()=default;
    system(A* s,int oid):super(s, oid) {}
    system(const system& o):super(o) {}
    system(system&& o):super(std::move(o)) {}
    system& operator=(const system& o) {
        super::operator=(o);
        return *this;
    }
    system& operator=(system&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<system,766,vector<typename A::_xy>,nok,nok> cut_output_rhs{this}; // x[nok],y[nok]
};
template<class A>
struct unit_combination:obj<A,25> {
    using super=obj<A,25>;
    unit_combination()=default;
    unit_combination(A* s,int oid):super(s, oid) {}
    unit_combination(const unit_combination& o):super(o) {}
    unit_combination(unit_combination&& o):super(std::move(o)) {}
    unit_combination& operator=(const unit_combination& o) {
        super::operator=(o);
        return *this;
    }
    unit_combination& operator=(unit_combination&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    ro<unit_combination,931,map<int64_t, typename A::_xy>,mw,m3_per_s> discharge{this}; // x[mw],y[m3_per_s]
    ro<unit_combination,932,map<int64_t, typename A::_xy>,mw,nok_per_mw> marginal_cost{this}; // x[mw],y[nok_per_mw]
    ro<unit_combination,933,map<int64_t, typename A::_xy>,mw,nok_per_mw> average_cost{this}; // x[mw],y[nok_per_mw]
};
template<class A>
struct tunnel:obj<A,26> {
    using super=obj<A,26>;
    tunnel()=default;
    tunnel(A* s,int oid):super(s, oid) {}
    tunnel(const tunnel& o):super(o) {}
    tunnel(tunnel&& o):super(std::move(o)) {}
    tunnel& operator=(const tunnel& o) {
        super::operator=(o);
        return *this;
    }
    tunnel& operator=(tunnel&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<tunnel,934,double,meter,meter> start_height{this}; // x[meter],y[meter]
    rw<tunnel,935,double,meter,meter> end_height{this}; // x[meter],y[meter]
    rw<tunnel,936,double,meter,meter> diameter{this}; // x[meter],y[meter]
    rw<tunnel,937,double,meter,meter> length{this}; // x[meter],y[meter]
    rw<tunnel,938,double,s2_per_m5,s2_per_m5> loss_factor{this}; // x[s2_per_m5],y[s2_per_m5]
    rw<tunnel,939,double,meter,meter> weir_width{this}; // x[meter],y[meter]
    rw<tunnel,940,int,no_unit,no_unit> time_delay{this}; // x[no_unit],y[no_unit]
    rw<tunnel,941,typename A::_xy,no_unit,no_unit> gate_opening_curve{this}; // x[no_unit],y[no_unit]
    rw<tunnel,942,typename A::_txy,nok,nok> gate_adjustment_cost{this}; // x[nok],y[nok]
    rw<tunnel,943,typename A::_txy,no_unit,no_unit> gate_opening_schedule{this}; // x[no_unit],y[no_unit]
    rw<tunnel,944,double,no_unit,no_unit> initial_opening{this}; // x[no_unit],y[no_unit]
    rw<tunnel,945,int,no_unit,no_unit> continuous_gate{this}; // x[no_unit],y[no_unit]
    ro<tunnel,946,typename A::_txy,meter,meter> end_pressure{this}; // x[meter],y[meter]
    ro<tunnel,947,typename A::_txy,meter,meter> sim_end_pressure{this}; // x[meter],y[meter]
    ro<tunnel,948,typename A::_txy,m3_per_s,m3_per_s> flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<tunnel,949,typename A::_txy,m3_per_s,m3_per_s> physical_flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<tunnel,950,typename A::_txy,m3_per_s,m3_per_s> sim_flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<tunnel,951,typename A::_txy,no_unit,no_unit> gate_opening{this}; // x[no_unit],y[no_unit]
    ro<tunnel,952,int,no_unit,no_unit> network_no{this}; // x[no_unit],y[no_unit]
    rw<tunnel,953,typename A::_txy,m3_per_s,m3_per_s> min_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<tunnel,954,typename A::_txy,m3_per_s,m3_per_s> max_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<tunnel,955,typename A::_txy,nok,nok> min_flow_penalty_cost{this}; // x[nok],y[nok]
    rw<tunnel,956,typename A::_txy,nok,nok> max_flow_penalty_cost{this}; // x[nok],y[nok]
    rw<tunnel,957,typename A::_txy,meter,meter> min_start_pressure{this}; // x[meter],y[meter]
};
template<class A>
struct interlock_constraint:obj<A,27> {
    using super=obj<A,27>;
    interlock_constraint()=default;
    interlock_constraint(A* s,int oid):super(s, oid) {}
    interlock_constraint(const interlock_constraint& o):super(o) {}
    interlock_constraint(interlock_constraint&& o):super(std::move(o)) {}
    interlock_constraint& operator=(const interlock_constraint& o) {
        super::operator=(o);
        return *this;
    }
    interlock_constraint& operator=(interlock_constraint&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<interlock_constraint,958,typename A::_txy,no_unit,no_unit> min_open{this}; // x[no_unit],y[no_unit]
    rw<interlock_constraint,959,typename A::_txy,no_unit,no_unit> max_open{this}; // x[no_unit],y[no_unit]
    rw<interlock_constraint,960,double,no_unit,no_unit> forward_switch_time{this}; // x[no_unit],y[no_unit]
    rw<interlock_constraint,961,double,no_unit,no_unit> backward_switch_time{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct flow_constraint:obj<A,28> {
    using super=obj<A,28>;
    flow_constraint()=default;
    flow_constraint(A* s,int oid):super(s, oid) {}
    flow_constraint(const flow_constraint& o):super(o) {}
    flow_constraint(flow_constraint&& o):super(std::move(o)) {}
    flow_constraint& operator=(const flow_constraint& o) {
        super::operator=(o);
        return *this;
    }
    flow_constraint& operator=(flow_constraint&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
};
template<class A>
struct lp_model:obj<A,29> {
    using super=obj<A,29>;
    lp_model()=default;
    lp_model(A* s,int oid):super(s, oid) {}
    lp_model(const lp_model& o):super(o) {}
    lp_model(lp_model&& o):super(std::move(o)) {}
    lp_model& operator=(const lp_model& o) {
        super::operator=(o);
        return *this;
    }
    lp_model& operator=(lp_model&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<lp_model,962,int,no_unit,no_unit> sim_mode{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,963,string_array,no_unit,no_unit> var_type_names{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,964,string_array,no_unit,no_unit> var_type_abbrev{this}; // x[no_unit],y[no_unit]
    ro<lp_model,965,vector<int>,no_unit,no_unit> var_type_index_type_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model,966,vector<int>,no_unit,no_unit> var_type_index_type_cnt{this}; // x[no_unit],y[no_unit]
    ro<lp_model,967,vector<int>,no_unit,no_unit> var_type_index_type_val{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,968,string_array,no_unit,no_unit> row_type_names{this}; // x[no_unit],y[no_unit]
    ro<lp_model,969,vector<int>,no_unit,no_unit> row_type_index_type_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model,970,vector<int>,no_unit,no_unit> row_type_index_type_cnt{this}; // x[no_unit],y[no_unit]
    ro<lp_model,971,vector<int>,no_unit,no_unit> row_type_index_type_val{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,972,string_array,no_unit,no_unit> index_type_names{this}; // x[no_unit],y[no_unit]
    ro<lp_model,973,vector<int>,no_unit,no_unit> index_type_desc_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model,974,vector<int>,no_unit,no_unit> index_type_desc_cnt{this}; // x[no_unit],y[no_unit]
    //--TODO: ro<lp_model,975,string_array,no_unit,no_unit> index_type_desc_val{this}; // x[no_unit],y[no_unit]
    ro<lp_model,976,vector<double>,no_unit,no_unit> AA{this}; // x[no_unit],y[no_unit]
    ro<lp_model,977,vector<int>,no_unit,no_unit> Irow{this}; // x[no_unit],y[no_unit]
    ro<lp_model,978,vector<int>,no_unit,no_unit> Jcol{this}; // x[no_unit],y[no_unit]
    ro<lp_model,979,vector<double>,no_unit,no_unit> rhs{this}; // x[no_unit],y[no_unit]
    ro<lp_model,980,vector<int>,no_unit,no_unit> sense{this}; // x[no_unit],y[no_unit]
    ro<lp_model,981,vector<double>,no_unit,no_unit> ub{this}; // x[no_unit],y[no_unit]
    ro<lp_model,982,vector<double>,no_unit,no_unit> lb{this}; // x[no_unit],y[no_unit]
    ro<lp_model,983,vector<double>,no_unit,no_unit> cc{this}; // x[no_unit],y[no_unit]
    ro<lp_model,984,vector<int>,no_unit,no_unit> bin{this}; // x[no_unit],y[no_unit]
    ro<lp_model,985,vector<double>,no_unit,no_unit> x{this}; // x[no_unit],y[no_unit]
    ro<lp_model,986,vector<double>,no_unit,no_unit> dual{this}; // x[no_unit],y[no_unit]
    ro<lp_model,987,vector<int>,no_unit,no_unit> var_type{this}; // x[no_unit],y[no_unit]
    ro<lp_model,988,vector<int>,no_unit,no_unit> var_index_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model,989,vector<int>,no_unit,no_unit> var_index_cnt{this}; // x[no_unit],y[no_unit]
    ro<lp_model,990,vector<int>,no_unit,no_unit> var_index_val{this}; // x[no_unit],y[no_unit]
    ro<lp_model,991,vector<int>,no_unit,no_unit> row_type{this}; // x[no_unit],y[no_unit]
    ro<lp_model,992,vector<int>,no_unit,no_unit> row_index_beg{this}; // x[no_unit],y[no_unit]
    ro<lp_model,993,vector<int>,no_unit,no_unit> row_index_cnt{this}; // x[no_unit],y[no_unit]
    ro<lp_model,994,vector<int>,no_unit,no_unit> row_index_val{this}; // x[no_unit],y[no_unit]
    rw<lp_model,995,int,no_unit,no_unit> add_row_type{this}; // x[no_unit],y[no_unit]
    rw<lp_model,996,vector<int>,no_unit,no_unit> add_row_index{this}; // x[no_unit],y[no_unit]
    rw<lp_model,997,vector<int>,no_unit,no_unit> add_row_variables{this}; // x[no_unit],y[no_unit]
    rw<lp_model,998,vector<double>,no_unit,no_unit> add_row_coeff{this}; // x[no_unit],y[no_unit]
    rw<lp_model,999,double,no_unit,no_unit> add_row_rhs{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1000,int,no_unit,no_unit> add_row_sense{this}; // x[no_unit],y[no_unit]
    ro<lp_model,1001,int,no_unit,no_unit> add_row_last{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1002,int,no_unit,no_unit> add_var_type{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1003,vector<int>,no_unit,no_unit> add_var_index{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1004,double,no_unit,no_unit> add_var_ub{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1005,double,no_unit,no_unit> add_var_lb{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1006,double,no_unit,no_unit> add_var_cc{this}; // x[no_unit],y[no_unit]
    rw<lp_model,1007,int,no_unit,no_unit> add_var_bin{this}; // x[no_unit],y[no_unit]
    ro<lp_model,1008,int,no_unit,no_unit> add_var_last{this}; // x[no_unit],y[no_unit]
};
template<class A>
struct river:obj<A,30> {
    using super=obj<A,30>;
    river()=default;
    river(A* s,int oid):super(s, oid) {}
    river(const river& o):super(o) {}
    river(river&& o):super(std::move(o)) {}
    river& operator=(const river& o) {
        super::operator=(o);
        return *this;
    }
    river& operator=(river&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<river,1009,double,meter,meter> length{this}; // x[meter],y[meter]
    rw<river,1010,double,meter,meter> upstream_elevation{this}; // x[meter],y[meter]
    rw<river,1011,double,meter,meter> downstream_elevation{this}; // x[meter],y[meter]
    rw<river,1012,double,hour,hour> time_delay_const{this}; // x[hour],y[hour]
    rw<river,1013,double,nok_per_mwh,nok_per_mwh> delayed_water_energy_value{this}; // x[nok_per_mwh],y[nok_per_mwh]
    rw<river,1014,double,no_unit,no_unit> initial_gate_opening{this}; // x[no_unit],y[no_unit]
    rw<river,1015,int,no_unit,no_unit> main_river{this}; // x[no_unit],y[no_unit]
    rw<river,1016,typename A::_xy,meter,meter> width_depth_curve{this}; // x[meter],y[meter]
    rw<river,1017,typename A::_xy,m3_per_s,nok_per_m3_per_s> flow_cost_curve{this}; // x[m3_per_s],y[nok_per_m3_per_s]
    rw<river,1018,typename A::_xy,m3_per_s,nok_per_m3_per_s> peak_flow_cost_curve{this}; // x[m3_per_s],y[nok_per_m3_per_s]
    rw<river,1019,vector<typename A::_xy>,hour,m3_per_s> time_delay_curve{this}; // x[hour],y[m3_per_s]
    rw<river,1020,typename A::_xy,hour,m3_per_s> past_upstream_flow{this}; // x[hour],y[m3_per_s]
    rw<river,1021,vector<typename A::_xy>,meter,m3_per_s> up_head_flow_curve{this}; // x[meter],y[m3_per_s]
    rw<river,1022,typename A::_xy,no_unit,meter> gate_opening_curve{this}; // x[no_unit],y[meter]
    rw<river,1023,vector<typename A::_xy>,meter,m3_per_s> delta_head_ref_up_flow_curve{this}; // x[meter],y[m3_per_s]
    rw<river,1024,vector<typename A::_xy>,meter,m3_per_s> delta_head_ref_down_flow_curve{this}; // x[meter],y[m3_per_s]
    rw<river,1025,typename A::_txy,m3_per_s,m3_per_s> inflow{this}; // x[m3_per_s],y[m3_per_s]
    rw<river,1026,typename A::_txy,m3_per_s,m3_per_s> min_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<river,1027,typename A::_txy,m3_per_s,m3_per_s> max_flow{this}; // x[m3_per_s],y[m3_per_s]
    rw<river,1028,typename A::_txy,nok_per_m3_per_s,nok_per_m3_per_s> min_flow_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river,1029,typename A::_txy,nok_per_m3_per_s,nok_per_m3_per_s> max_flow_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river,1030,typename A::_txy,m3sec_hour,m3sec_hour> ramping_up{this}; // x[m3sec_hour],y[m3sec_hour]
    rw<river,1031,typename A::_txy,m3sec_hour,m3sec_hour> ramping_down{this}; // x[m3sec_hour],y[m3sec_hour]
    rw<river,1032,typename A::_txy,nok_per_m3_per_s,nok_per_m3_per_s> ramping_up_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river,1033,typename A::_txy,nok_per_m3_per_s,nok_per_m3_per_s> ramping_down_penalty_cost{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river,1034,typename A::_txy,nok_per_m3_per_s,nok_per_m3_per_s> cost_curve_scaling{this}; // x[nok_per_m3_per_s],y[nok_per_m3_per_s]
    rw<river,1035,typename A::_txy,no_unit,m3_per_s> flow_schedule{this}; // x[no_unit],y[m3_per_s]
    rw<river,1036,typename A::_txy,no_unit,nok_per_m3_per_s> flow_schedule_penalty_cost{this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<river,1037,typename A::_txy,no_unit,no_unit> gate_opening_schedule{this}; // x[no_unit],y[no_unit]
    rw<river,1038,typename A::_txy,no_unit,m3_per_s> flow_block_merge_tolerance{this}; // x[no_unit],y[m3_per_s]
    rw<river,1039,typename A::_txy,no_unit,meter_per_hour> gate_ramping{this}; // x[no_unit],y[meter_per_hour]
    rw<river,1040,typename A::_txy,no_unit,nok_per_meter_hour> gate_ramping_penalty_cost{this}; // x[no_unit],y[nok_per_meter_hour]
    rw<river,1041,typename A::_txy,no_unit,nok_per_meter> gate_adjustment_cost{this}; // x[no_unit],y[nok_per_meter]
    rw<river,1042,typename A::_txy,no_unit,nok_per_m3_per_s> flow_cost{this}; // x[no_unit],y[nok_per_m3_per_s]
    rw<river,1043,typename A::_txy,no_unit,no_unit> mip_flag{this}; // x[no_unit],y[no_unit]
    ro<river,1044,typename A::_txy,m3_per_s,m3_per_s> flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<river,1045,typename A::_txy,m3_per_s,m3_per_s> upstream_flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<river,1046,typename A::_txy,m3_per_s,m3_per_s> downstream_flow{this}; // x[m3_per_s],y[m3_per_s]
    ro<river,1047,typename A::_txy,meter,meter> gate_height{this}; // x[meter],y[meter]
    ro<river,1048,typename A::_txy,nok,nok> min_flow_penalty{this}; // x[nok],y[nok]
    ro<river,1049,typename A::_txy,nok,nok> max_flow_penalty{this}; // x[nok],y[nok]
    ro<river,1050,typename A::_txy,nok,nok> ramping_up_penalty{this}; // x[nok],y[nok]
    ro<river,1051,typename A::_txy,nok,nok> ramping_down_penalty{this}; // x[nok],y[nok]
    ro<river,1052,typename A::_txy,no_unit,nok> flow_penalty{this}; // x[no_unit],y[nok]
    ro<river,1053,typename A::_txy,no_unit,nok> peak_flow_penalty{this}; // x[no_unit],y[nok]
    ro<river,1054,typename A::_txy,no_unit,nok> gate_ramping_penalty{this}; // x[no_unit],y[nok]
    ro<river,1055,typename A::_txy,no_unit,nok> gate_adjustment_penalty{this}; // x[no_unit],y[nok]
    ro<river,1056,typename A::_txy,no_unit,nok> flow_schedule_penalty{this}; // x[no_unit],y[nok]
    ro<river,1057,typename A::_txy,no_unit,m3_per_s> physical_flow{this}; // x[no_unit],y[m3_per_s]
    ro<river,1058,typename A::_txy,no_unit,m3_per_s> initial_downstream_flow{this}; // x[no_unit],y[m3_per_s]
    ro<river,1059,typename A::_xy,hour,m3_per_s> distributed_past_upstream_flow{this}; // x[hour],y[m3_per_s]
};
template<class A>
struct busbar:obj<A,31> {
    using super=obj<A,31>;
    busbar()=default;
    busbar(A* s,int oid):super(s, oid) {}
    busbar(const busbar& o):super(o) {}
    busbar(busbar&& o):super(std::move(o)) {}
    busbar& operator=(const busbar& o) {
        super::operator=(o);
        return *this;
    }
    busbar& operator=(busbar&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<busbar,1068,typename A::_txy,no_unit,mw> load{this}; // x[no_unit],y[mw]
    //--TODO: rw<busbar,1069,sy,no_unit,no_unit> ptdf{this}; // x[no_unit],y[no_unit]
    ro<busbar,1070,typename A::_txy,no_unit,nok_per_mwh> energy_price{this}; // x[no_unit],y[nok_per_mwh]
    ro<busbar,1071,typename A::_txy,no_unit,mwh> power_deficit{this}; // x[no_unit],y[mwh]
    ro<busbar,1072,typename A::_txy,no_unit,mwh> power_excess{this}; // x[no_unit],y[mwh]
};
template<class A>
struct ac_line:obj<A,32> {
    using super=obj<A,32>;
    ac_line()=default;
    ac_line(A* s,int oid):super(s, oid) {}
    ac_line(const ac_line& o):super(o) {}
    ac_line(ac_line&& o):super(std::move(o)) {}
    ac_line& operator=(const ac_line& o) {
        super::operator=(o);
        return *this;
    }
    ac_line& operator=(ac_line&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<ac_line,1073,typename A::_txy,no_unit,mw> max_forward_flow{this}; // x[no_unit],y[mw]
    rw<ac_line,1074,typename A::_txy,no_unit,mw> max_backward_flow{this}; // x[no_unit],y[mw]
    rw<ac_line,1075,int,no_unit,no_unit> n_loss_segments{this}; // x[no_unit],y[no_unit]
    rw<ac_line,1076,double,no_unit,no_unit> loss_factor_linear{this}; // x[no_unit],y[no_unit]
    rw<ac_line,1077,double,no_unit,no_unit> loss_factor_quadratic{this}; // x[no_unit],y[no_unit]
    ro<ac_line,1078,typename A::_txy,no_unit,mw> flow{this}; // x[no_unit],y[mw]
    ro<ac_line,1079,typename A::_txy,no_unit,mw> forward_loss{this}; // x[no_unit],y[mw]
    ro<ac_line,1080,typename A::_txy,no_unit,mw> backward_loss{this}; // x[no_unit],y[mw]
};
template<class A>
struct dc_line:obj<A,33> {
    using super=obj<A,33>;
    dc_line()=default;
    dc_line(A* s,int oid):super(s, oid) {}
    dc_line(const dc_line& o):super(o) {}
    dc_line(dc_line&& o):super(std::move(o)) {}
    dc_line& operator=(const dc_line& o) {
        super::operator=(o);
        return *this;
    }
    dc_line& operator=(dc_line&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    rw<dc_line,1081,typename A::_txy,no_unit,mw> max_forward_flow{this}; // x[no_unit],y[mw]
    rw<dc_line,1082,typename A::_txy,no_unit,mw> max_backward_flow{this}; // x[no_unit],y[mw]
    rw<dc_line,1083,int,no_unit,no_unit> n_loss_segments{this}; // x[no_unit],y[no_unit]
    rw<dc_line,1084,double,no_unit,no_unit> loss_constant{this}; // x[no_unit],y[no_unit]
    rw<dc_line,1085,double,no_unit,no_unit> loss_factor_linear{this}; // x[no_unit],y[no_unit]
    rw<dc_line,1086,double,no_unit,no_unit> loss_factor_quadratic{this}; // x[no_unit],y[no_unit]
    ro<dc_line,1087,typename A::_txy,no_unit,mw> flow{this}; // x[no_unit],y[mw]
    ro<dc_line,1088,typename A::_txy,no_unit,mw> forward_loss{this}; // x[no_unit],y[mw]
    ro<dc_line,1089,typename A::_txy,no_unit,mw> backward_loss{this}; // x[no_unit],y[mw]
};
template<class A>
struct gen_reserve_capability:obj<A,34> {
    using super=obj<A,34>;
    gen_reserve_capability()=default;
    gen_reserve_capability(A* s,int oid):super(s, oid) {}
    gen_reserve_capability(const gen_reserve_capability& o):super(o) {}
    gen_reserve_capability(gen_reserve_capability&& o):super(std::move(o)) {}
    gen_reserve_capability& operator=(const gen_reserve_capability& o) {
        super::operator=(o);
        return *this;
    }
    gen_reserve_capability& operator=(gen_reserve_capability&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: rw<gen_reserve_capability,1090,string,no_unit,no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<gen_reserve_capability,1091,typename A::_txy,no_unit,mw> p_extended{this}; // x[no_unit],y[mw]
    rw<gen_reserve_capability,1092,typename A::_txy,no_unit,mw> schedule{this}; // x[no_unit],y[mw]
};
template<class A>
struct pump_reserve_capability:obj<A,35> {
    using super=obj<A,35>;
    pump_reserve_capability()=default;
    pump_reserve_capability(A* s,int oid):super(s, oid) {}
    pump_reserve_capability(const pump_reserve_capability& o):super(o) {}
    pump_reserve_capability(pump_reserve_capability&& o):super(std::move(o)) {}
    pump_reserve_capability& operator=(const pump_reserve_capability& o) {
        super::operator=(o);
        return *this;
    }
    pump_reserve_capability& operator=(pump_reserve_capability&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: rw<pump_reserve_capability,1093,string,no_unit,no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<pump_reserve_capability,1094,typename A::_txy,no_unit,mw> p_extended{this}; // x[no_unit],y[mw]
    rw<pump_reserve_capability,1095,typename A::_txy,no_unit,mw> schedule{this}; // x[no_unit],y[mw]
};
template<class A>
struct plant_reserve_capability:obj<A,36> {
    using super=obj<A,36>;
    plant_reserve_capability()=default;
    plant_reserve_capability(A* s,int oid):super(s, oid) {}
    plant_reserve_capability(const plant_reserve_capability& o):super(o) {}
    plant_reserve_capability(plant_reserve_capability&& o):super(std::move(o)) {}
    plant_reserve_capability& operator=(const plant_reserve_capability& o) {
        super::operator=(o);
        return *this;
    }
    plant_reserve_capability& operator=(plant_reserve_capability&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: rw<plant_reserve_capability,1096,string,no_unit,no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<plant_reserve_capability,1097,typename A::_txy,no_unit,mw> p_gen_extended{this}; // x[no_unit],y[mw]
    rw<plant_reserve_capability,1098,typename A::_txy,no_unit,mw> p_pump_extended{this}; // x[no_unit],y[mw]
    rw<plant_reserve_capability,1099,typename A::_txy,no_unit,mw> schedule{this}; // x[no_unit],y[mw]
};
template<class A>
struct needle_comb_reserve_capability:obj<A,37> {
    using super=obj<A,37>;
    needle_comb_reserve_capability()=default;
    needle_comb_reserve_capability(A* s,int oid):super(s, oid) {}
    needle_comb_reserve_capability(const needle_comb_reserve_capability& o):super(o) {}
    needle_comb_reserve_capability(needle_comb_reserve_capability&& o):super(std::move(o)) {}
    needle_comb_reserve_capability& operator=(const needle_comb_reserve_capability& o) {
        super::operator=(o);
        return *this;
    }
    needle_comb_reserve_capability& operator=(needle_comb_reserve_capability&& o) {
        super::operator=(std::move(o));
        return *this;
    }
    // attributes
    //--TODO: rw<needle_comb_reserve_capability,1100,string,no_unit,no_unit> reserve_type_name{this}; // x[no_unit],y[no_unit]
    rw<needle_comb_reserve_capability,1101,typename A::_txy,no_unit,mw> p_extended{this}; // x[no_unit],y[mw]
};

}
