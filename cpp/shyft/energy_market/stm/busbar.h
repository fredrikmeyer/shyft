#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/network.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;

    /** @brief Busbar
     *
     * Busbar, a hub connected by transmission lines
     */
    struct busbar : id_base {
        using super = id_base;

        busbar() { mk_url_fx(this); }
        busbar(int id, const string& name, const string& json, const network_& net)
            : super{id,name,json,{},{}},net{net} { mk_url_fx(this); }

        bool operator==(const busbar& other) const;
        bool operator!=(const busbar& other) const { return !(*this == other); }

        /** @brief generate an almost unique, url-like string for a reservoir.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        network_ net_() const { return net.lock(); }
        network__ net; ///< Reference up to the 'owning' network.
        busbar_ shared_from_this() const; // Get shared pointer from this, via network.

        BOOST_HANA_DEFINE_STRUCT(busbar,
            (apoint_ts, dummy) // TODO: Required to build busbar from request_handler
        );

        vector<transmission_line_> get_transmission_lines_from_busbar() const;
        vector<transmission_line_> get_transmission_lines_to_busbar() const;
        vector<power_module_>  get_power_modules() const;

        void add_to_start_of_transmission_line(transmission_line_) const;
        void add_to_end_of_transmission_line(transmission_line_) const;
        void add_to_power_module(power_module_) const;

        x_serialize_decl();
    };
    using busbar_ = shared_ptr<busbar>;
    using busbar__ = weak_ptr<busbar>;
}

x_serialize_export_key(shyft::energy_market::stm::busbar);
