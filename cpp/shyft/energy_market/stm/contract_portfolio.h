#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using shyft::time_series::dd::apoint_ts;

    /** @brief Contract portfolio
     *
     * Represents a collection of related contracts.
     */
    struct contract_portfolio : id_base {
        using super = id_base;

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        contract_portfolio() { mk_url_fx(this); }
        contract_portfolio(int id, const string& name, const string& json, const stm_system_& sys)
            : super{id,name,json,{},{}},sys{sys} { mk_url_fx(this); }

        bool operator==(const contract_portfolio& other) const;
        bool operator!=(const contract_portfolio& other) const { return !(*this == other); }

        vector<contract_> contracts; ///< association with contract

        stm_system_ sys_() const { return sys.lock(); }
        stm_system__ sys; ///< reference up to the 'owning' optimization system.

        BOOST_HANA_DEFINE_STRUCT(contract_portfolio,
            (apoint_ts, quantity),  ///< Quantity (volume) of the portfolio (e.g. sum from contracts)
            //(apoint_ts, price),   ///< Price of the portfolio (e.g. average from contracts)
            (apoint_ts, fee),       ///< Fees of the portfolio (e.g. sum from contracts)
            (apoint_ts, revenue)    ///< Revenue, actual or forecast. Usually a calculated value.
        );

        x_serialize_decl();
    };

    using contract_portfolio_ = shared_ptr<contract_portfolio>;
}

x_serialize_export_key(shyft::energy_market::stm::contract_portfolio);
