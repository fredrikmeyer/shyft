#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <memory>
#include <string>
#include <optional>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/constraints.h>

namespace shyft::energy_market::stm {
    using std::size_t;
    using std::string;
    using std::vector;
    using std::pair;
    using std::shared_ptr;
    using shyft::energy_market::id_base;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_axis::generic_dt;

    struct unit;
    using unit_ = shared_ptr<unit>;
    struct stm_system;
    struct energy_market_area;
    using energy_market_area_ = shared_ptr<energy_market_area>;
    using shyft::energy_market::core::penalty_constraint;
    struct unit_group_member;
    using unit_group_member_ = shared_ptr<unit_group_member>;

    namespace unit_group_type { // ensure namespace closure
        enum unit_group_type_:uint16_t {
            unspecified,
            //-- operational reserve groups
            fcr_n_up,
            fcr_n_down,
            fcr_d_up,
            fcr_d_down,
            afrr_up,
            afrr_down,
            mfrr_up,
            mfrr_down,
            ffr,
            rr_up,
            rr_down,
            //-- spinning
            commit,
            //-- with production requirement
            production
        };
    }

    /** @brief unit group consists of _members
     *
     * 
     *
     */
    struct unit_group : public id_base {
        using super = id_base;

        stm_system* mdl{nullptr}; ///< _not_ owned ref to the owning model that is _required_ to outlive the scope of the unit_group,  The pointer should be const, but the model can be modified.

        unit_group();
        unit_group(stm_system* mdl);

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;
        
        /** unit_group.obligation_ provides schedule, cost and results
        */
        struct obligation_ {
            BOOST_HANA_DEFINE_STRUCT(obligation_,
                (apoint_ts, schedule), ///< W scheduled or target obligation, system should provide at least this amount
                (apoint_ts, cost),     ///< money/W if schedule violation
                (apoint_ts, result),   ///< W resulting amount of obligation(usually schedule, or economical best result)
                (apoint_ts, penalty)   ///< money used in penalty cost due to violation 
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        /** unit_group.delivery_ provides sum delivery of product from units
        */
        struct delivery_ {
            BOOST_HANA_DEFINE_STRUCT(delivery_,
                (apoint_ts, schedule), ///< sum of unit scheduled delivery for unit group product
                (apoint_ts, realised), ///< sum of unit realised delivery, as in historical fact
                (apoint_ts, result)    ///< sum of unit result delivery result as from optimisation
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        // Attributes:
        BOOST_HANA_DEFINE_STRUCT(unit_group,
            (uint16_t, group_type),    ///< unit_group_type
            (obligation_, obligation), ///< obligation description for the group
            (delivery_, delivery),     ///< sum delivery for the units
            (apoint_ts, production),   ///< [W] sum production in units TODO: consider use pure getter, constructing the expression on demand.
            (apoint_ts, flow)          ///< [m3/s] sum flow of units TODO: consider use pure getter, constructing the expression on demand.
        );
        bool operator==(const unit_group& other) const;
        bool operator!=(const unit_group& other) const { return !( *this == other); };
        vector<unit_group_member_> members;    ///< the members units in this group

        /** adding units must update expressions, and ensure unique set */
        void add_unit(unit_ const& u, apoint_ts const& active);
        /** removing units must update expressions, and ensure unique set */
        void remove_unit(unit_ const& u);
        /** update the production and sum to express the units */
        void update_sum_expressions();
        /** get the energy market area this unit group is associated with */
        energy_market_area_ get_energy_market_area() const;

        /** @return all urls of form dstm://M123/U1.production i.e prefix/path, where prefix is like dstm://M123
         */
        std::vector<string> all_urls(std::string const& prefix) const;
        x_serialize_decl();
    };
    using unit_group_ = std::shared_ptr<unit_group>;
    using unit_group__ = std::weak_ptr<unit_group>;

    /** @brief unit group member
     * 
     * @details The member-ship in the group might be temporal, and we 
     * choose to represent this as a time-series,  'active'.
     * This allows us to use time-series math to express the membership.
     * E.g. 
     * group_property = sum_of ( member.unit.property * member.active).
     * 
     * Thus a value of 1.0 propagates the unit property (e.g. .production.realised) to the
     * group-sum. A active(t) == 0.0, means it's zeroed out.
     * 
     * Finally, if the active ts is 'null', not set, then we assume the member-ship is
     * constant on.
     * 
     * Regarding url-generation: We would like to subscribe to the active ts changes.
     * Thus, we need to have a unique path for it.
     *  like '/M1/U1/M1.active'
     * to address that specific setting, 
     *    model id=1, 
     *    unit group id=1, and 
     *    unit-group-member id=1 
     *     attribute .active.
     * the unit-group-member-id could be the id of the unit, since this unit.id is unique for the system.
     * Later there might be other attribute at the member-level as well.
     * 
     */
    struct unit_group_member {
        using super = id_base;
        unit_group_member();
        unit_group_member(unit_group *owner, unit_ const& u, apoint_ts const& active);
        unit_group *group{nullptr}; ///< the  unit_group for this member (assume lifetime(unit_group)>= member)
        //-- stuff to ensure url-generation
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;
        url_fx_t url_fx;
        //-- equality
        bool operator==(const unit_group_member& other) const;
        bool operator!=(const unit_group_member& other) const { return !( *this == other); }
        unit_ unit; ///< unit for this group member
        int64_t id() const;
        //-- hana data member list
        BOOST_HANA_DEFINE_STRUCT(unit_group_member,
            (apoint_ts, active) ///< [unit_less] if not set, always member, if set, active(t) determines the accumulated sum.
        );
        x_serialize_decl();
    };

    /** @brief algorithm that provides a unit group-membership ts for specified group-type
     * 
     * @detail
     *  A unit can only contribute to one kind of group in each time-step.
     *  The optimizer api needs to know which identified group the unit
     *  contributes to (if any) in each time-step.
     *  A group is identified by group-id (a number, integer part), thus this can be
     *  represented by a time-series.
     *  The outcome of this function is either an empty ts (means no membership found)
     *  or a time-series with group-id.
     *  This function intended usage and design is for emitting information to 
     *  the optimization engine.
     * 
     * @note Since this function actually verifies duplicate membership, the time-series
     *       needs to be bound, otherwise exception is thrown.
     * 
     * @throw runtime_error if conflicting membership is detected, or any ts is unbound.
     * @param groups the complete set of relevant unit groups to check
     * @param u the unit that we seek member-ship mapping ts for
     * @param run_ts the time-series filled with ones for each time-period for which we investigate.
     * @param group_type the specific group-type (e.g. fcr-n, fcr-d etc..) we search for
     * @param id_map optional ptr to group-id mapping, translate group-id to group using this map.
     * @return a time-series where each time-period as dictated by run_ts, is filled with the group-id, or 0.
     */
    apoint_ts compute_unit_group_membership_ts(
        std::vector<unit_group_> const &groups,
        unit_ const &u,
        apoint_ts run_ts, //  a ts filled with one's for all time-steps of interest
        int64_t group_type,
        std::map<int64_t,int64_t> const *id_map=nullptr
    );

    /** @brief algorithm that provides a unit combinations ts for specified group
     * 
     * @detail
     *  For a given single group, records which units are active in this group for
     *  each time step. Saving the result as bit coded values in a time series,
     *  where each bit represents the group member in order of appearance in the group.
     * 
     * @see compute_unit_group_membership_ts
     * @param group the unit group to check
     * @param time_axis the time-axis to consider
     * @return a time-series where each time-period is filled with bit encoded unit combination value
     */
    apoint_ts compute_group_unit_combinations_ts(unit_group_ const &group, generic_dt const &time_axis);
}

x_serialize_export_key(shyft::energy_market::stm::unit_group);
x_serialize_export_key(shyft::energy_market::stm::unit_group_member);
