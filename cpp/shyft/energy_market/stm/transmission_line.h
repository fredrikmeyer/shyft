#pragma once
#include <string>
#include <vector>
#include <memory>
#include <shyft/mp.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/id_base.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::shared_ptr;
    using shyft::time_series::dd::apoint_ts;

    /** @brief Transmission line
     *
     * Transmission line, connecting bus bars
     */
    struct transmission_line : id_base {
        using super = id_base;

        transmission_line() { mk_url_fx(this); }
        transmission_line(int id, const string& name, const string& json, const network_& net)
            : super{id,name,json,{},{}},net{net} { mk_url_fx(this); }

        bool operator==(const transmission_line& other) const;
        bool operator!=(const transmission_line& other) const { return !(*this == other); }

        /** @brief generate an almost unique, url-like string for a reservoir.
         *
         * @param rbi: back inserter to store result
         * @param levels: How many levels of the url to include.
         * 		levels == 0 includes only this level. Use level < 0 to include all levels.
         * @param placeholders: The last element of the vector states wethers to use the reservoir ID
         * 		in the url or a placeholder. The remaining vector will be used in subsequent levels of the url.
         * 		If the vector is empty, the function defaults to not using placeholders.
         * @return
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        network_ net_() const { return net.lock(); }
        network__ net; ///< Reference up to the 'owning' network.

        BOOST_HANA_DEFINE_STRUCT(transmission_line,
            (apoint_ts, capacity) ///< Transmission line capacity (TODO: unit?)
        );

        busbar_ from_bb; ///< Association busbar at start of transmission line
        busbar_ to_bb; ///< Association busbar at end of transmission line

        x_serialize_decl();
    };
    using transmission_line_=shared_ptr<transmission_line>;
}

x_serialize_export_key(shyft::energy_market::stm::transmission_line);
