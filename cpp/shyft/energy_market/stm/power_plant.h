#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::shared_ptr;
    using std::dynamic_pointer_cast;
    using std::map;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;

    struct power_plant : hydro_power::power_plant {
        using super = hydro_power::power_plant;

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;
        
        power_plant(int id, const string& name, const string& json, const stm_hps_& hps);
        power_plant();

        bool operator==(const power_plant& other) const;
        bool operator!=(const power_plant& other) const { return !( *this == other); }

        static void add_unit(const power_plant_& ps, const unit_& a);
        void remove_unit(const unit_& a);

        struct production_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(production_,
                (apoint_ts, constraint_min),  ///< W
                (apoint_ts, constraint_max),  ///< W
                (apoint_ts, schedule),        ///< W
                (apoint_ts, realised),        ///< W, the sum of unit.production.realised
                (apoint_ts, merge_tolerance), ///< W, Max deviation in production plan
                (apoint_ts, ramping_up),      ///< W, Constraint of ramping up production
                (apoint_ts, ramping_down),    ///< W, Constraint of ramping down production
                (apoint_ts, result)           ///< W
            );
        };// production{ *this };

        struct discharge_ {
            url_fx_t url_fx;// needed to link up py wrapped url paths
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, constraint_min),
                (apoint_ts, constraint_max),
                (apoint_ts, schedule),
                (apoint_ts, result),
                (apoint_ts, realised),                     ///< the sum of all unit.discharge.realised
                (apoint_ts, intake_loss_from_bypass_flag), ////< x: time / y: 0 or 1
                (t_xy_, upstream_level_constraint),        ///< x: m : y: m3/s
                (t_xy_, downstream_level_constraint),      ///< x: m / y: m3/s
                (apoint_ts, ramping_up),                   ///< W, Constraint of ramping up discharge
                (apoint_ts, ramping_down)                  ///< W, Constraint of ramping down discharge

            );
        };

        BOOST_HANA_DEFINE_STRUCT(power_plant,
            (apoint_ts, outlet_level),  ///< masl, (input, or result ?)
            (apoint_ts, mip),           ///< bool, opt. with mip
            (apoint_ts, unavailability),///< bool, .. 
            (production_, production),
            (discharge_, discharge)
        );

        x_serialize_decl();
    };

    using power_plant_ = shared_ptr<power_plant>;
}

x_serialize_export_key(shyft::energy_market::stm::power_plant);

