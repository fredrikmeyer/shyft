#pragma once
/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <map>
#include <string>
#include <memory>
#include <vector>
#include <shyft/mp.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/core/core_serialization.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/url_fx.h>

namespace shyft::energy_market::stm {
    using std::string;
    using std::shared_ptr;
    using std::dynamic_pointer_cast;
    using std::map;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;

    struct unit : hydro_power::unit {
        using super = hydro_power::unit;

        /** @brief Generate an almost unique, url-like string for this object.
         *
         * @param rbi Back inserter to store result.
         * @param levels How many levels of the url to include. Use value 0 to
         *     include only this level, negative value to include all levels (default).
         * @param template_levels From which level to start using placeholder instead of
         *     actual object ID. Use value 0 for all, negative value for none (default).
         */
        void generate_url(std::back_insert_iterator<string>& rbi, int levels = -1, int template_levels = -1) const;

        unit();
        unit(int id, const string& name, const string& json, const stm_hps_& hps);

        bool operator==(const unit& other) const;
        bool operator!=(const unit& other) const { return !( *this == other); }

        struct production_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min), ///< W
                    (apoint_ts, max)  ///< W
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };
            BOOST_HANA_DEFINE_STRUCT(production_, // units are W, watt
                (apoint_ts, schedule),     ///< W
                (apoint_ts, commitment),   ///< (1, 0), indicating if unit should produce(1) or not(0), alternative to schedule
                (apoint_ts, realised),     ///< W  as in historical fact
                (apoint_ts, static_min),   ///< W
                (apoint_ts, static_max),   ///< W
                (apoint_ts, nominal),      ///< W
                (constraint_, constraint),
                (apoint_ts, result)        ///< W
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        //-- definition of nested composition groups
        struct discharge_ {
            struct constraint_ {
                BOOST_HANA_DEFINE_STRUCT(constraint_,
                    (apoint_ts, min), ///< m3/s
                    (apoint_ts, max), ///< m3/s
                    (t_xy_, max_from_downstream_level) ///< masl -> m3/s
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };
            BOOST_HANA_DEFINE_STRUCT(discharge_,
                (apoint_ts, result),   ///< m3/s
                (apoint_ts, schedule), ///< m3/s
                (apoint_ts, realised), ///< m3/s non trivial computation, based on production.realised etc.
                (constraint_, constraint)
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        struct cost_ {
            BOOST_HANA_DEFINE_STRUCT(cost_,
                (apoint_ts, start),  ///< money/#start
                (apoint_ts, stop)    ///< money/#stop
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        /** operational reserve, frequency balancing support */
        struct reserve_ {// 
            /** reserve_.spec_ provides schedule, or min-max + result mode
             *  that is: if the schedule is provided, the optimizer will respect that
             *  otherwise, plan-mode: find .result within min..max so that external
             *  group requirement is satisfied
             */
            struct spec_ {
                BOOST_HANA_DEFINE_STRUCT(spec_,
                    (apoint_ts, schedule), ///< W or % if droop, if specified 'schedule-mode' and next members ignored
                    (apoint_ts, min),      ///< W or % if droop
                    (apoint_ts, max),      ///< W or % if droop
                    (apoint_ts, cost),     ///< money
                    (apoint_ts, result),   ///< W or % if droop
                    (apoint_ts, penalty),  ///< money
                    (apoint_ts, realised)  ///< as in historical fact,possibly computed from production/min/max etc.
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };

            /** most reserve modes goes in up and down regulations */
            struct pair_ {
                BOOST_HANA_DEFINE_STRUCT(pair_,
                    (spec_, up), ///< up regulation reserve
                    (spec_, down)///< down regulation reserve
                );
                url_fx_t url_fx;// needed by .url(...) to python exposure
            };

            BOOST_HANA_DEFINE_STRUCT(reserve_,
                (apoint_ts, fcr_static_min),///< W if specified overrides long running static_min for FCR calculations
                (apoint_ts, fcr_static_max),///< W if specified overrides long running static_max for FCR calculations
                (pair_, fcr_n),///< FCR up,down
                (pair_, afrr), ///< aFRR up,down
                (pair_, mfrr), ///< mFRR up,down
                (apoint_ts, mfrr_static_min), ///< minimum production for RR
                (pair_, rr),   ///< RR up/down
                (pair_, fcr_d),///< FCR-D up/down
                (apoint_ts, fcr_mip),///< FCR flag for SHOP
                (pair_, frr),  ///< FRR  up,down
                (spec_, droop), ///< droop control, relates to fcr_n and fcr_d
                (t_xy_, droop_steps) ///< x=step number, asc, 1..n, y= discrete droop setting
            );
            url_fx_t url_fx;// needed by .url(...) to python exposure
        };

        //-- class descriptive members
        BOOST_HANA_DEFINE_STRUCT(unit,
            (apoint_ts, effective_head), ///< meter
            (t_xy_, generator_description),
            (t_turbine_description_, turbine_description),
            (t_xyz_list_, pump_description),
            (apoint_ts, unavailability), ///< no-unit, 1== unavailable, (0,nan)-> available
            (apoint_ts, priority),       ///< unit priority value for uploading order
            (production_, production),
            (discharge_, discharge),
            (cost_, cost),               ///< start/stop
            (reserve_, reserve)          ///< operational reserve, frequency balancing support
        );

        x_serialize_decl();
    };

    using unit_ = shared_ptr<unit>;
}

x_serialize_export_key(shyft::energy_market::stm::unit);
