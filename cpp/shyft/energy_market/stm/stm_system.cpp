#include <boost/format.hpp>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/em_utils.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/time_series/dd/aref_ts.h>

namespace shyft::energy_market::stm {
    using std::make_shared;
    using std::any_of;
    using std::begin;
    using std::end;
    using boost::format;

    stm_system::stm_system(){
        // as empty as possible.. serialization etc.
        mk_url_fx(this);
        run_params.mdl=this;
        summary = std::make_shared<optimization_summary>();
        summary->mdl=this;
    }

    stm_system::stm_system(int id, string name, string json)
        : id_base{id,name,json,{},{}} {
        mk_url_fx(this);
        run_params.mdl=this;
        summary = std::make_shared<optimization_summary>();
        summary->mdl=this;
    }
    
    unit_group_ stm_system::add_unit_group(int id, string name, string json, int group_type) {
        for(auto const&ug:unit_groups) {
            if(ug->id==id || ug->name==name)
                throw std::runtime_error("unit group with same id or name already exists: id="
                + std::to_string(ug->id)+",name="+ug->name);
        }
        auto ug=std::make_shared<unit_group>(this);
        ug->id=id;ug->name=name;ug->json=json;
        ug->group_type = group_type;
        unit_groups.push_back(ug);
        return ug;        
    }

    shared_ptr<stm_system> stm_system::clone_stm_system(const shared_ptr<stm_system>& s) {
        auto blob = to_blob(s);
        return from_blob(blob);
    }

    static void _generate_url(std::string const& prefix, std::string const& id_expr,int id,std::back_insert_iterator<string>& rbi, int /*levels*/, int template_levels) {
        if (!template_levels) {
            const auto a = prefix + id_expr;
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            const auto idstr=prefix+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }    
    }
    void stm_system::generate_url(std::back_insert_iterator<string>& , int , int ) const  {
        // ref issue https://gitlab.com/shyft-os/shyft/-/issues/829
        //_generate_url("/M","{o_id}",id,rbi,levels,template_levels);//do not make url at this level
    }
    stm_hps::stm_hps() {
        // as empty as possible.. serialization etc.
    }

    stm_hps::stm_hps(int id, const string&name) :super(id, name) {
    }

    void stm_hps::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        _generate_url("/H","{parent_id}",id,rbi,levels,template_levels);
    }


    bool stm_hps::operator==(const stm_hps& other) const {
        if(this==&other) return true;//equal by addr.

        return super::operator==(other) // basic structure compared equal
            && equal_vector_ptr_content<reservoir>(reservoirs, other.reservoirs)
            && equal_vector_ptr_content<unit>(units, other.units)
            && equal_vector_ptr_content<waterway>(waterways, other.waterways)
            && equal_vector_ptr_content<power_plant>(power_plants, other.power_plants)
            && equal_vector_ptr_content<reservoir_aggregate>(reservoir_aggregates,other.reservoir_aggregates)
            ;
    }

    reservoir_aggregate_ stm_hps::find_reservoir_aggregate_by_name(const string& name) const {return hydro_power_system::find_by_name(reservoir_aggregates, name);}
    reservoir_aggregate_ stm_hps::find_reservoir_aggregate_by_id(int64_t id) const { return hydro_power_system::find_by_id(reservoir_aggregates, id); }


    template <class T,class CT>
    static void ensure_unique_id_and_name(stm_hps_& /*sys*/,const string &tp_name,CT& c,int id, const string& name,const string& /*json*/) {
        if (any_of(begin(c), end(c), [&name](const auto&w)->bool {return w->name == name;}))
            throw stm_rule_exception((format("%2% name must be unique within a HydroPowerSystem, name' %1%' already exists")% name%tp_name).str());
        if (any_of(begin(c), end(c), [&id](const auto&w)->bool {return w->id == id;}))
            throw stm_rule_exception((format("%2% id must be unique within a HydroPowerSystem, id %1% already exists")% id%tp_name).str());
    }

    template <class T,class CT>
    static shared_ptr<T> add_ensure_unique_id_and_name(stm_hps_&sys,const string &tp_name,CT& c,int id, const string& name,const string& json) {
        ensure_unique_id_and_name<T>(sys,tp_name,c,id,name,json);
        auto o=make_shared<T>(id,name,json,sys);
        c.push_back(o);
        return o;
    }
    
    reservoir_ stm_hps_builder::create_reservoir(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<reservoir>(s,"Reservoir",s->reservoirs,id,name,json);
    }
    reservoir_aggregate_ stm_hps_builder::create_reservoir_aggregate(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<reservoir_aggregate>(s,"ReservoirAggregate",s->reservoir_aggregates,id,name,json);
    }
    unit_ stm_hps_builder::create_unit(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<unit>(s,"Unit",s->units,id,name,json);
    }
    power_plant_ stm_hps_builder::create_power_plant(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<power_plant>(s,"PowerPlant",s->power_plants,id,name,json);
    }
    waterway_ stm_hps_builder::create_waterway(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<waterway>(s,"Waterway",s->waterways,id,name,json);
    }
    catchment_ stm_hps_builder::create_catchment(int id,const string&name,const string &json) {
        return add_ensure_unique_id_and_name<catchment>(s,"Catchment",s->catchments,id,name,json);
    }
    gate_ stm_hps_builder::create_gate(int id,const string&name,const string &json) {
        auto gts=s->gates();
        ensure_unique_id_and_name<gate>(s,"Gate",gts,id,name,json);
        return make_shared<gate>(id,name,json);
    }

    void stm_system::set_summary(optimization_summary_ const&x) {
        if(!x) {
            if(summary) *summary= optimization_summary{};
        } else {
            if(summary) *summary=*x;
        }
    }
    bool stm_system::operator==(const stm_system& o) const {
          if(this==&o) return true;//equal by addr.
          return super::operator==(o)
              && equal_vector_ptr_content<stm_hps>(hps,o.hps)
              && equal_vector_ptr_content<energy_market_area>(market,o.market)
              && equal_vector_ptr_content<contract>(contracts,o.contracts)
              && equal_vector_ptr_content<contract_portfolio>(contract_portfolios,o.contract_portfolios)
              && equal_vector_ptr_content<network>(networks,o.networks)
              && equal_vector_ptr_content<power_module>(power_modules,o.power_modules)
              && run_params ==o.run_params
              && equal_vector_ptr_content<unit_group>(unit_groups,o.unit_groups)
              && (summary==o.summary || (summary && o.summary && *summary== *o.summary))
          ;
    }
    bool stm_ts_operation::apply_tsm(unit_group_member&) const {return false;}

    bool stm_ts_operation::apply_tsm(id_base& t) const {
        bool done=false;
        for (auto it = t.tsm.begin(); it != t.tsm.end(); ++it) {
            done |= fx(it->second);
        }
        return done;
    }

    bool stm_ts_operation::apply(stm_hps&  hps) const {
        bool done=false;
        done |= apply_objects<stm::reservoir>(hps.reservoirs);
        done |= apply_objects<stm::unit>(hps.units);
        done |= apply_objects<stm::waterway>(hps.waterways);
        done |= apply_objects<stm::catchment>(hps.catchments);
        done |= apply_objects<stm::power_plant>(hps.power_plants);
        done |= apply_objects<stm::gate>(hps.gates());
        done |= apply_objects<stm::reservoir_aggregate>(hps.reservoir_aggregates);
        return done;
    }

    bool stm_ts_operation::apply(stm_system& mdl) const {
        bool done=false;
        for (auto & h : mdl.hps) {
            done |=  apply(*h);
        }
        done |= apply_objects<stm::energy_market_area>(mdl.market);
        done |= apply_objects<stm::unit_group>(mdl.unit_groups);
        done |= apply_objects<stm::contract>(mdl.contracts);
        done |= apply_objects<stm::contract_portfolio>(mdl.contract_portfolios);
        done |= apply_objects<stm::network>(mdl.networks);
        done |= apply_objects<stm::power_module>(mdl.power_modules);
        for(auto const& ug:mdl.unit_groups) { // ensure to rebind ug.members
            done |= apply_objects<stm::unit_group_member>(ug->members);
        }
        return done;
    }


}

