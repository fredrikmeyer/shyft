#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/power_module.h>

namespace shyft::energy_market::stm {
    namespace hana = boost::hana;
    namespace mp = shyft::mp;

    bool busbar::operator==(busbar const&o) const {
        if(this==&o) return true;//equal by addr.
        if (super::operator!=(o)) return false;

        // Compare relations
        constexpr auto equal_id_base = [](const super& s1, const super& s2) { return s1 == s2; };
        constexpr auto equal_transmission_lines = [equal_id_base] (const auto& t1, const auto& t2) -> bool {
            if (t1 == t2) return true; // equal by addr
            if (t1 == nullptr || t2 == nullptr) return false;
            return equal_id_base(*t1, *t2); // safe to dereference shared_ptr here
        };

        // Compare associated transmission lines to busbar
        const auto t_to = get_transmission_lines_to_busbar();
        const auto other_t_to = o.get_transmission_lines_to_busbar();
        if (!std::is_permutation(t_to.begin(), t_to.end(), other_t_to.begin(), other_t_to.end(),
                                 equal_transmission_lines)) {
            return false;
        }

        // Compare associated transmission lines from busbar
        const auto t_from = get_transmission_lines_from_busbar();
        const auto other_t_from = o.get_transmission_lines_from_busbar();
        if (!std::is_permutation(t_from.begin(), t_from.end(), other_t_from.begin(), other_t_from.end(),
                                 equal_transmission_lines)) {
            return false;
        }

        // Compare associated power modules
        constexpr auto equal_power_modules = [equal_id_base] (const auto& p1, const auto& p2) -> bool {
            if (p1 == p2) return true; // equal by addr
            if (p1 == nullptr || p2 == nullptr) return false;
            return equal_id_base(*p1, *p2); // safe to dereference shared_ptr here
        };
        const auto pm = get_power_modules();
        const auto other_pm = o.get_power_modules();
        if (!std::is_permutation(pm.begin(), pm.end(), other_pm.begin(), other_pm.end(),
                                 equal_power_modules)) {
            return false;
        }

        return true;
    }

    void busbar::generate_url(std::back_insert_iterator<string>& rbi, int levels, int template_levels) const {
        if (levels) {
            auto tmp = dynamic_pointer_cast<network>(net_());
            if (tmp) tmp->generate_url(rbi, levels-1, template_levels ? template_levels - 1 : template_levels);
        }
        if (!template_levels) {
            constexpr std::string_view a = "/b{o_id}";
            std::copy(std::begin(a), std::end(a), rbi);
        } else {
            auto idstr="/b"+std::to_string(id);
            std::copy(std::begin(idstr),std::end(idstr),rbi);
        }
    }

    busbar_ busbar::shared_from_this() const {
        if (auto n = net_(); n) {
            for (auto const& b : n->busbars)
                if(b.get()==this) return b;
        }
        return nullptr;
    }

    vector<transmission_line_> busbar::get_transmission_lines_from_busbar() const {
        vector<transmission_line_> result;
        if(auto n = net_(); n) {
            for (auto const& tl : n->transmission_lines) {
                if (tl->from_bb.get() == this) {
                    result.push_back(tl);
                }
            }
        }
        return result;
    }

    vector<transmission_line_> busbar::get_transmission_lines_to_busbar() const {
        vector<transmission_line_> result;
        if(auto n = net_(); n) {
            for (auto const& tl : n->transmission_lines) {
                if (tl->to_bb.get() == this) {
                    result.push_back(tl);
                }
            }
        }
        return result;
    }

    vector<power_module_> busbar::get_power_modules() const {
        vector<power_module_> result;
        if (auto n = net_(); n) {
            if (auto s = n->sys_(); s) {
                for (auto const& pm : s->power_modules) {
                    if (pm->connected.get() == this) {
                        result.push_back(pm);
                    }
                }
            }
        }
        return result;
    }

    void busbar::add_to_start_of_transmission_line(transmission_line_ tl) const {
        auto me = shared_from_this();
        if (!me) throw std::runtime_error("this busbar is not associated with a network");
        tl->from_bb = me;
    }

    void busbar::add_to_end_of_transmission_line(transmission_line_ tl) const {
        auto me = shared_from_this();
        if (!me) throw std::runtime_error("this busbar is not associated with a network");
        tl->to_bb = me;
}

    void busbar::add_to_power_module(power_module_ pm) const {
        auto me = shared_from_this();
        if (!me) throw std::runtime_error("this busbar is not associated with a network");
        pm->connected = me;
    }
}
