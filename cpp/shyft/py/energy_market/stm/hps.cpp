/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <boost/format.hpp>
#include <shyft/py/api/expose_container.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>


namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;
    using stm::catchment_;
    using stm::reservoir_;
    using stm::reservoir_aggregate_;
    using stm::power_plant_;
    using stm::waterway_;
    using stm::gate_;
    using stm::unit_;
    using stm::stm_hps_;
    using stm::stm_hps_builder;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;


    template<> string str_(stm::stm_hps const& o) {
        return (format("HydroPowerSystem(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }

    /** extensions to ease py expose */
    struct hps_ext {
        static std::vector<char> to_blob(const stm_hps_& m) {
            auto s=shyft::energy_market::stm::stm_hps::to_blob(m);
            return std::vector<char>(s.begin(),s.end());
        }
        static stm_hps_ from_blob(std::vector<char>& blob) {
            std::string s(blob.begin(),blob.end());
            return shyft::energy_market::stm::stm_hps::from_blob(s);
        }
        // wrap all create calls via the stm_hps_builder to enforce build-rules
        static catchment_ create_catchment(stm_hps_&s,int id,const string&name,const string &json) {return stm_hps_builder(s).create_catchment(id,name,json);}
        static reservoir_ create_reservoir(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_reservoir(id,name,json);}
        static reservoir_aggregate_ create_reservoir_aggregate(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_reservoir_aggregate(id,name,json);}
        static unit_ create_unit(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_unit(id,name,json);}
        static waterway_ create_waterway(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_waterway(id,name,json);}
        static gate_ create_gate(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_gate(id,name,json);}
        static power_plant_ create_power_plant(stm_hps_&s,int id,const string&name,const string &json){return stm_hps_builder(s).create_power_plant(id,name,json);}
        static waterway_ create_tunnel(stm_hps_&s,int id,const string&name,const string &json) {return create_waterway(s,id,name,json);}
        static waterway_ create_river(stm_hps_&s,int id,const string&name,const string &json) {return create_waterway(s,id,name,json);}
    };
    
    void stm_hps() {
        using std::dynamic_pointer_cast;
        
        auto h=py::class_<
            stm::stm_hps,
            py::bases<hydro_power::hydro_power_system>,
            shared_ptr<stm::stm_hps>,
            boost::noncopyable
        >("HydroPowerSystem",
            doc_intro("A hydro power system, with indataset.")
            doc_details(
                "The hydro power system consists of reservoirs, waterway (river/tunnel)\n"
                "and units. In addition, the power plant has the role of keeping\n"
                "related units together into a group that resembles what most people\n"
                "would think is a power plant in this context. The power plant has just\n"
                "references to the units (generator/turbine parts), but can keep\n"
                "sum-requirements/schedules and computations valid at power plant level.")
            ,py::no_init);
        h
            .def(py::init<int, string>((py::arg("uid"), py::arg("name")),
                "Create hydro power system with unique uid.")
            )
            .def_readonly("reservoir_aggregates",&stm::stm_hps::reservoir_aggregates,"all the reservoir aggregates")
            .def("create_reservoir", &hps_ext::create_reservoir,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm reservoir with unique uid.")
                doc_returns("reservoir","Reservoir","The new reservoir.")
            )
            .def("create_reservoir_aggregate", &hps_ext::create_reservoir_aggregate,
                 (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                 doc_intro("Create stm reservoir aggregate with unique uid.")
            doc_returns("reservoir_aggregate","ReservoirAggregate","The new ReservoirAggregate.")
            )
            .def("create_unit", &hps_ext::create_unit,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm unit.")
                doc_returns("unit","Unit","The new unit.")
            )
            .def("create_power_plant", &hps_ext::create_power_plant,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm power plant that keeps units.")
                doc_returns("power_plant","PowerPlant","The new PowerPlant.")
            )
            .def("create_waterway", &hps_ext::create_waterway,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm waterway (a tunnel or river).")
                doc_returns("waterway","Waterway","The new waterway.")
            )
            .def("create_gate", &hps_ext::create_gate,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm gate.")
                doc_returns("gate","Gate","The new gate.")
            )
            .def("create_tunnel", &hps_ext::create_waterway,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm waterway (a tunnel or river).")
                doc_returns("waterway","Waterway","The new waterway.")
            )
            .def("create_river", &hps_ext::create_waterway,
                (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json")=string("")),
                doc_intro("Create stm waterway (a tunnel or river).")
                doc_returns("waterway","Waterway","The new waterway.")
            )
            .def("to_blob",&hps_ext::to_blob,(py::arg("self")),
                doc_intro("Serialize the model to a blob.")
                doc_returns("blob","ByteVector","Blob form of the model.")
            )
            .def("from_blob",&hps_ext::from_blob,(py::arg("blob")),
                doc_intro("Re-create a stm hps from a previously create blob.")
                doc_returns("hps","HydroPowerSystem","A stm hydro-power-system including it's attributes in the ids.")
            ).staticmethod("from_blob")
            // core baseclass docstrings will be used automatically
            .def("find_waterway_by_id", +[](stm::stm_hps*s,int64_t id) {return dynamic_pointer_cast<stm::waterway>(s->find_waterway_by_id(id));})
            .def("find_gate_by_id", +[](stm::stm_hps*s,int64_t id) {return dynamic_pointer_cast<stm::gate>(s->find_gate_by_id(id));})
            .def("find_reservoir_by_id", +[](stm::stm_hps*s,int64_t id) {return dynamic_pointer_cast<stm::reservoir>(s->find_reservoir_by_id(id));})
            .def("find_reservoir_aggregate_by_id", +[](stm::stm_hps*s,int64_t id) {return dynamic_pointer_cast<stm::reservoir_aggregate>(s->find_reservoir_aggregate_by_id(id));})
            .def("find_unit_by_id", +[](stm::stm_hps*s,int64_t id) {return dynamic_pointer_cast<stm::unit>(s->find_unit_by_id(id));})
            .def("find_power_plant_by_id",+[](stm::stm_hps*s,int64_t id) {return dynamic_pointer_cast<stm::power_plant>(s->find_power_plant_by_id(id));})
            .def("find_catchment_by_id", +[](stm::stm_hps*s,int64_t id){return dynamic_pointer_cast<stm::catchment>(s->find_catchment_by_id(id));})
            
            .def("find_waterway_by_name", +[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::waterway>(s->find_waterway_by_name(n));})
            .def("find_gate_by_name", +[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::gate>(s->find_gate_by_name(n));})
            .def("find_reservoir_by_name", +[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::reservoir>(s->find_reservoir_by_name(n));})
            .def("find_reservoir_aggregate_by_name", +[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::reservoir_aggregate>(s->find_reservoir_aggregate_by_name(n));})
            .def("find_unit_by_name", +[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::unit>(s->find_unit_by_name(n));})
            .def("find_power_plant_by_name",+[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::power_plant>(s->find_power_plant_by_name(n));})
            .def("find_catchment_by_name", +[](stm::stm_hps*s,string const&n){return dynamic_pointer_cast<stm::catchment>(s->find_catchment_by_name(n));})
        ;
        expose_str_repr(h);

        expose_vector_eq<stm::stm_hps_>("HydroPowerSystemList", "A strongly typed list of HydroPowerSystem.",&stm::equal_attribute<std::vector<stm_hps_>>,false);
    }
}
