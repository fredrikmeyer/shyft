#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/scoped_gil.h>

#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <shyft/energy_market/stm/srv/task/client.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/web_api/energy_market/stm/task/request_handler.h>

namespace shyft::energy_market::stm::srv {
    using shyft::py::scoped_gil_release;
    using shyft::py::scoped_gil_aquire;
    using boost::python::class_;
    using boost::python::bases;
    namespace py=boost::python;

    using std::mutex;
    using std::unique_lock;
    using shyft::py::energy_market::py_server_with_web_api;
    using shyft::py::energy_market::py_client;
    using shyft::web_api::energy_market::stm::task::request_handler;

    struct py_task_client : py_client<task::client> {
        using super = py_client<task::client>;
        py_task_client(const std::string& host_port, int timeout_ms): super(host_port, timeout_ms) {}

        void add_case(int64_t mid, const stm_case_& run) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.add_case(mid, run);
        }

        bool remove_case(int64_t mid, int64_t rid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_case(mid, rid);
        }

        bool remove_case(int64_t mid, const std::string& rname) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_case(mid, rname);
        }

        stm_case_ get_case(int64_t mid, int64_t rid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_case(mid, rid);
        }

        stm_case_ get_case(int64_t mid, const string& rname) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_case(mid, rname);
        }

        void update_case(int64_t mid, const stm_case& ce) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.update_case(mid, ce);
        }

        void add_model_ref(int64_t mid, int64_t rid, const model_ref_& mr) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.add_model_ref(mid, rid, mr);
        }

        bool remove_model_ref(int64_t mid, int64_t rid, const string& mkey) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_model_ref(mid, rid, mkey);
        }

        model_ref_ get_model_ref(int64_t mid, int64_t rid, const string& mkey) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_ref(mid, rid, mkey);
        }
        bool fx(int64_t mid, string fx_args) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.fx(mid, fx_args);
        }

    };

    struct py_task_server : py_server_with_web_api<task::server, request_handler> {
        using super = py_server_with_web_api<task::server, request_handler>;
        py::object py_fx_cb;///< python callback that can be set by the py user

        py_task_server(const string& root_dir): super(root_dir) {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads(); // ensure threads is enabled
            }
            // rig the c++ fx_cb to forward calls to this python-layer
            impl.fx_cb = [&](int64_t mid,string fx_args)->bool { return this->py_do_fx(mid,fx_args); };

        }
        
                /** we need to handle errors when executing the user python code, 
         */
        void handle_pyerror() {
            // from SO: https://stackoverflow.com/questions/1418015/how-to-get-python-exception-text
            namespace py=boost::python;
            using namespace boost;
            std::string msg{"unspecified error"};
            if(PyErr_Occurred()) {
                PyObject *exc,*val,*tb;
                py::object formatted_list, formatted;
                PyErr_Fetch(&exc,&val,&tb);
                py::handle<> hexc(exc),hval(py::allow_null(val)),htb(py::allow_null(tb));
                py::object traceback(py::import("traceback"));
                if (!tb) {
                    py::object format_exception_only{ traceback.attr("format_exception_only") };
                    formatted_list = format_exception_only(hexc,hval);
                } else {
                    py::object format_exception{traceback.attr("format_exception")};
                    if (format_exception) {
                        try {
                            formatted_list = format_exception(hexc, hval, htb);
                        } catch (...) { // any error here, and we bail out, no crash please
                            msg = "not able to extract exception info";
                        }
                    } else
                        msg="not able to extract exception info";
                }
                if (formatted_list) {
                    formatted = py::str("\n").join(formatted_list);
                    msg = py::extract<std::string>(formatted);
                }
            }
            py::handle_exception();
            PyErr_Clear();
            throw std::runtime_error(msg);
        }
        
        /** this is where we attempt to fire user specified callback, */
        bool py_do_fx(int64_t mid, string const& fx_arg) {
            bool r{false};
            if (py_fx_cb.ptr() != Py_None) {
                scoped_gil_aquire gil;// we need to use the GIL here before trying to call python.
                try {
                    r = boost::python::call<bool>(py_fx_cb.ptr(), mid,fx_arg);
                } catch  (const boost::python::error_already_set&) {
                    handle_pyerror();
                }
            }
            return r;
        }

    };
}

namespace expose {
    namespace py = boost::python;

    using std::string;
    using std::shared_ptr;

    using namespace shyft::energy_market::stm::srv;

    void stm_task_server() {
        // Run info
        class_<model_ref, bases<>, shared_ptr<model_ref>>("StmModelRef",
            doc_intro("Reference to a model, and where to find it."),
            py::init<string, int, int, string>((py::arg("self"), py::arg("host"), py::arg("port_num"), py::arg("api_port_num"), py::arg("model_key")),
                doc_intro("Create a run info.")
                doc_parameters()
                doc_parameter("host", "str", "Where the referenced model is stored.")
                doc_parameter("port_num", "int", "At what port number to interface with the server.")
                doc_parameter("api_port_num", "int", "At what port number to interface with the server using the web API.")
                doc_parameter("model_key", "str", "The model key the referenced model is stored under."))
            )
            .def(py::init<>((py::arg("self")), doc_intro("Default constructor for StmRunInfo.")))
            .def_readwrite("host", &model_ref::host, "Where model is stored.")
            .def_readwrite("port_num", &model_ref::port_num, "Port number to interface with server.")
            .def_readwrite("api_port_num", &model_ref::api_port_num, "Port number to interface with server through web API.")
            .def_readwrite("model_key", &model_ref::model_key, "The model key the referenced model is stored under.")
            .def(py::self == py::self)
            .def(py::self != py::self)
            ;

        using ModelRefList = vector<shared_ptr<model_ref>>;
        class_<ModelRefList>("ModelRefList", "A strongly typed list of StmModelRef.")
            .def(py::vector_indexing_suite<ModelRefList, true>())
            ;

        // Expose StmCase:
        class_<stm_case, py::bases<>, shared_ptr<stm_case>>(
            "StmCase",
            doc_intro("Provided a case concept for Stm. A case, can hold a number of references to stm model runs. A StmTask contains many cases, each with a set of stm model runs.")
            )
            .def(py::init<int64_t, string const&, utctime, py::optional<string, vector<string>, vector<model_ref_>>>(
                (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("created"),
                    py::arg("json")=string{""}, py::arg("labels")=vector<string>(), py::arg("model_refs")=vector<shared_ptr<model_ref>>()),
                doc_intro("Create a stm case")
                )
            )
            .def_readwrite("id", &stm_case::id, "The unique case ID.")
            .def_readwrite("name", &stm_case::name, "Any useful name of description.")
            .def_readwrite("created", &stm_case::created, "The time of creation, or last modification.")
            .def_readwrite("json", &stm_case::json, "A json formatted string with miscellaneous data.")
            .def_readwrite("labels", &stm_case::labels, "A set of labels for the case.")
            .def_readonly("model_refs", &stm_case::model_refs, "Set of stm run model references that are related to the case.")
            .def(py::self == py::self)
            .def(py::self != py::self)
            .def("add_model_ref", &stm_case::add_model_ref, (py::arg("self"), py::arg("mr")),
                doc_intro("Add a model reference to the case.")
                doc_parameters()
                doc_parameter("mr", "StmModelRef", "The model reference to add.")
            )
            .def("remove_model_ref", &stm_case::remove_model_ref, (py::arg("self"), py::arg("mkey")),
                doc_intro("Remove a stm run model reference from the case.")
                doc_parameters()
                doc_parameter("mkey", "str", "The stm run model key the reference has stored. Cf. StmModelRef.model_key.")
                doc_returns("success", "Boolean", "True if model reference was successfully removed. False if case did not contain model reference with given model key.")
            )
            .def("get_model_ref", &stm_case::get_model_ref, (py::arg("self"), py::arg("mkey")),
                doc_intro("Get model reference in the case based on model key.")
                doc_parameters()
                doc_parameter("mkey", "str", "The model key the reference has stored. Cf. StmModelRef.model_key.")
                doc_returns("mr", "StmModelRef", "Requested model reference. None if not found in the case.")
            )
            ;

        using StmCaseVector = vector<shared_ptr<stm_case>>;
        class_<StmCaseVector>("StmCaseVector", "A strongly typed list, vector, of StmCases.")
            .def(py::vector_indexing_suite<StmCaseVector, true>())
        ;

        // Expose StmTask:
        class_<stm_task, py::bases<>, shared_ptr<stm_task>>(
            "StmTask",
            doc_intro("Task concept for Stm. Can contain a number of cases, each of them can contain one or more stm model-run references.")
            doc_intro("Defined through its set of labels, a base model, which should relate to child runs and model_refs,")
            doc_intro("and a task name.")
            )
            .def(py::init<int64_t, const string&, utctime, py::optional<string, vector<string>, vector<stm_case_>, model_ref, string>>(
                (py::arg("self"), py::arg("id"), py::arg("name"), py::arg("created"), py::arg("json")=string{""},
                    py::arg("labels")=vector<string>(), py::arg("cases")=vector<stm_case_>(), py::arg("base_model")=model_ref(),
                    py::arg("task_name")=string{""}),
                doc_intro("Create an stm task.")
                )
            )
            .def_readwrite("id", &stm_task::id, "The unique task ID.")
            .def_readwrite("name", &stm_task::name, "Any useful name or description.")
            .def_readwrite("created", &stm_task::created, "The time of creation, or last modification.")
            .def_readwrite("json", &stm_task::json, "A json formatted string with miscellaneous data.")
            .def_readwrite("labels", &stm_task::labels, "A set of labels for the task.")
            .def_readonly("cases", &stm_task::cases, "Set of runs connected to task.")
            .def_readwrite("base_model", &stm_task::base_mdl, "Base model, which should correspond to models used in runs.")
            .def_readwrite("task_name", &stm_task::task_name, "Name of task connected to task.")
            .def(py::self == py::self)
            .def(py::self != py::self)
            .def("add_case", &stm_task::add_case, (py::arg("self"), py::arg("case")),
                doc_intro("Add a case (set of stm model runs) to the task.")
                doc_parameters()
                doc_parameter("case", "StmCase", "The case instance to add to the task.")
                doc_returns("None", "None", "Returns nothing."))
            .def("remove_case", static_cast<bool (stm_task::*)(int64_t)>(&stm_task::remove_case),
                 (py::arg("self"), py::arg("id")), doc_intro("Remove a case from task based on id."))
            .def("remove_case", static_cast<bool (stm_task::*)(const string&)>(&stm_task::remove_case),
                 (py::arg("self"), py::arg("name")), doc_intro("Remove a case from task based on name."))
            .def("get_case", static_cast<stm_case_ (stm_task::*)(int64_t)>(&stm_task::get_case),
                 (py::arg("self"), py::arg("cid")),
                 doc_intro("Get run from task based on ID.")
                 doc_parameters()
                 doc_parameter("cid", "int", "Id of case you want to get.")
                 doc_returns("case", "StmCase", "Request case, None if not found."))
            .def("get_case", static_cast<stm_case_ (stm_task::*)(const string&)>(&stm_task::get_case),
                 (py::arg("self"), py::arg("rname")),
                 doc_intro("Get case based on its name.")
                 doc_parameters()
                 doc_parameter("rname", "str", "Name of case you want to get.")
                 doc_returns("case", "StmCase", "Request case, None if not found."))

            .def("update_case", static_cast<bool (stm_task::*)(const stm_case&)>(&stm_task::update_case),
                 (py::arg("self"), py::arg("case")),
                         doc_intro("Update case inplace")
                         doc_parameters()
                         doc_parameter("case", "StmCase", "The case to update inplace.")
                         )
            ;
        using StmTaskVector = vector<shared_ptr<stm_task>>;
        class_<StmTaskVector>("StmTaskVector", "A strongly typed list, vector, of StmTasks")
            .def(py::vector_indexing_suite<StmTaskVector, true>())
            ;

        // Server- and client:
        auto session_client = shyft::py::energy_market::expose_client<py_task_client>("StmTaskClient",
            "The client api for the Stm Task repository.");
        session_client.def("add_case", &py_task_client::add_case,
                (py::arg("self"),py::arg("mid"),py::arg("case")),
                doc_intro("Add a case to a task on server.")
                doc_parameters()
                doc_parameter("mid", "int", "Task id to add a run to.")
                doc_parameter("case", "StmCase", "The case to add to the task.")
                doc_raises()
                doc_raise("RuntimeError", "If provided mid is an invalid task id.")
                doc_raise("RuntimeError", "If provided case has the same id or name as a case already in task.")
            )
            .def("remove_case", static_cast< bool(py_task_client::*)(int64_t, int64_t) >(&py_task_client::remove_case),
                (py::arg("self"),py::arg("mid"), py::arg("cid")),
                doc_intro("Remove a case from a task on server.")
                doc_parameters()
                doc_parameter("mid", "int", "Task id to remove run from.")
                doc_parameter("cid", "int", "Id of case to remove.")
                doc_returns("success", "Boolean", "True if the case was successfully removed from task.")
            )
            .def("remove_case", static_cast< bool(py_task_client::*)(int64_t, const string&) >(&py_task_client::remove_case),
                 (py::arg("self"), py::arg("mid"), py::arg("rname")),
                 doc_intro("Remove case from a task on server based on it's case name.")
                 doc_parameters()
                 doc_parameter("mid", "int", "Task id to remove run from.")
                 doc_parameter("cname", "str", "Name of the case to remove.")
                 doc_returns("success", "Boolean", "True if case was successfully removed from task.")
            )
            .def("get_case", static_cast< stm_case_ (py_task_client::*)(int64_t, int64_t)>(&py_task_client::get_case),
                 (py::arg("self"), py::arg("mid"), py::arg("cid")),
                 doc_intro("Get case from task based on case's id.")
                 doc_parameters()
                 doc_parameter("mid", "int", "Task ID.")
                 doc_parameter("cid", "int", "Case ID.")
                 doc_returns("case", "StmCase", "Requested case, or None if not found.")
            )
            .def("get_case", static_cast< stm_case_ (py_task_client::*)(int64_t, const string&)>(&py_task_client::get_case),
                 (py::arg("self"), py::arg("mid"), py::arg("cname")),
                 doc_intro("Get from task based on case's name.")
                 doc_parameters()
                 doc_parameter("mid", "int", "Task ID.")
                 doc_parameter("cname", "str", "Case name.")
                 doc_parameter("case", "StmCase", "Requested run, or None if not found.")
            )
            .def("update_case", static_cast< void (py_task_client::*)(int64_t, const stm_case&)>(&py_task_client::update_case),
                 (py::arg("self"), py::arg("mid"), py::arg("case")),
                 doc_parameters()
                 doc_parameter("mid", "int", "Task ID.")
                 doc_parameter("case", "StmCase", "Case to be updated inplace within task.")
            )
            .def("add_model_ref", &py_task_client::add_model_ref,
                 (py::arg("self"), py::arg("mid"), py::arg("cid"), py::arg("mr")),
                 doc_intro("Add a model reference to a case on server.")
                 doc_parameters()
                 doc_parameter("mid", "int", "Task ID.")
                 doc_parameter("cid", "int", "Case ID.")
                 doc_parameter("mr", "StmModelRef", "The model reference to add to case with ID=cid, contained in task with ID=mid.")
            )
            .def("remove_model_ref", &py_task_client::remove_model_ref,
                (py::arg("self"), py::arg("mid"), py::arg("cid"), py::arg("mkey")),
                doc_intro("Remove a model reference from a case contained in a task.")
                doc_parameters()
                doc_parameter("mid", "int", "Task ID.")
                doc_parameter("cid", "int", "Case ID.")
                doc_parameter("mkey", "str", "Model key of model reference. What is stored in StmModelRef.model_key.")
                doc_returns("success", "Boolean", "True if model reference was successfully removed, False otherwise.")
            )
            .def("get_model_ref", &py_task_client::get_model_ref,
                (py::arg("self"), py::arg("mid"), py::arg("cid"), py::arg("mkey")),
                doc_intro("Get a model reference stored in a case contained in a task.")
                doc_parameters()
                doc_parameter("mid", "int", "Task ID.")
                doc_parameter("cid", "int", "Case ID.")
                doc_parameter("mkey", "str", "Model key of model reference. What is stored in StmModelRef.model_key.")
                doc_returns("mr", "StmModelRef", "Requested model reference. None if run wasn't found or didn't contain requested model reference.")
            )
            .def("fx", &py_task_client::fx, (py::arg("self"), py::arg("mid"),py::arg("fx_arg")),
                 doc_intro("Execute the serverside fx, passing supplied arguments.")
                 doc_parameters()
                 doc_parameter("mid", "str", "ID of task for the server-side fx.")
                 doc_parameter("fx_arg","str", "Any argument passed to the server-side fx.")
                 doc_returns("success", "bool", "true if call successfully done.")
            )

            ;

        //using run_srv_t = shyft::py::energy_market::py_server_with_web_api<shyft::energy_market::stm::srv::task::server,
        //    shyft::web_api::energy_market::stm::task::request_handler>;
        auto py_srv=shyft::py::energy_market::expose_server_with_web_api<py_task_server>("StmTaskServer",
            "The server-side component for the STM Task repository.");
        py_srv
            .def_readwrite("fx",&py_task_server::py_fx_cb,
                doc_intro("server-side callable function(lambda) that takes two parameters:")
                doc_intro("mid :  the model id")
                doc_intro("fx_arg: arbitrary string to pass to the server-side function")
                doc_intro("The server-side fx is called when the client (or web-api) invoke the c.fx(mid,fx_arg).")
                doc_intro("The signature of the callback function should be fx_cb(mid:str, fx_arg:str)->bool")
                doc_intro("This feature is simply enabling the users to tailor server-side functionality in python!")
                doc_intro("\nExamples:\n")
                doc_intro(
                    ">>> from shyft.energy_market.stm import StmTaskServer\n"
                    ">>> s=StmTaskServer()\n"
                    ">>> def my_fx(mid:str, fx_arg:str)->bool:\n"
                    ">>>     print(f'invoked with mid={mid} fx_arg={fx_arg}')\n"
                    ">>>   # note we can use captured Server s here!"
                    ">>>     return True\n"
                    ">>> # and then bind the function to the callback\n"
                    ">>> s.fx=my_fx\n"
                    ">>> s.start_server()\n"
                    ">>> : # later using client from anywhere to invoke the call\n"
                    ">>> fx_result=c.fx('my_model_id', 'my_args')\n\n"
                )
            )
        ;
    }
}
