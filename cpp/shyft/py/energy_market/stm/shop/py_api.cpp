#include <shyft/py/api/boostpython_pch.h>
#include <shyft/version.h>

#ifdef SHYFT_WITH_SHOP

#include <shyft/py/api/boostpython_pch.h>
#include <boost/format.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <shyft/py/api/expose_str.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/py/energy_market/stm/shop/py_shop_command.h>
#include <shyft/py/energy_market/stm/shop/py_shop_commander.h>

#include <shyft/time/utctime_utilities.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/waterway.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/srv/db.h>

#include <shyft/energy_market/stm/shop/shop_system.h>

namespace py=boost::python;

namespace expose {

using std::string;
using std::vector;
using std::shared_ptr;
using shyft::core::utctime;
using shyft::core::utctimespan;
using shyft::core::utcperiod;
using shyft::core::calendar;
using shyft::time_axis::generic_dt;
using shyft::time_series::dd::apoint_ts;
using shyft::energy_market::stm::stm_system_;
using shyft::energy_market::stm::shop::shop_system;
using shyft::energy_market::stm::shop::shop_command;
using shyft::energy_market::stm::shop::shop_log_entry;

// Utility functions for Python wrapping of generic shop_system functions that streams
// data into an std::ostream reference (by default std::cout) given as last argument,
// to collect the data and return as a string, which is easier to work with from Python.
template<class T, class Func, class... Args>
string string_wrap_streaming_function(const T& obj, Func func, Args... args) {
    std::ostringstream stream; (obj.*func)(args..., stream); return stream.str();
}
template<class Func, class... Args>
string string_wrap_streaming_function(Func func, Args... args) {
    std::ostringstream stream; func(args..., stream); return stream.str();
}

template<> string str_(const shop_log_entry::log_severity& o) {
    switch(o) {
        case shop_log_entry::log_severity::information: return "INFORMATION";
        case shop_log_entry::log_severity::diagnosis_information: return "DIAGNOSIS_INFORMATION";
        case shop_log_entry::log_severity::warning: return "WARNING";
        case shop_log_entry::log_severity::diagnosis_warning: return "DIAGNOSIS_WARNING";
        case shop_log_entry::log_severity::error: return "ERROR";
        case shop_log_entry::log_severity::diagnosis_error: return "DIAGNOSIS_ERROR";
        default: return "UNDEFINED";
    }
}

template<> string str_(const shop_log_entry& o) {
    return (boost::format("ShopLogEntry(time=%1%, severity=%2%, code=%3%, message=%4%)")
        %str_(o.time)
        %str_(o.severity)
        %str_(o.code)
        %str_(o.message)
    ).str();
}

template<> std::string str_(std::vector<std::string> const&v) {
    return str_<std::string, 0, 0>(v); // Force printing of all entries (max_items=0) and on separate lines (compact=0)
}
template<> std::string str_(std::vector<shop_log_entry> const&v) {
    return str_<shop_log_entry, 0, 0>(v); // Force printing of all entries (max_items=0) and on separate lines (compact=0)
}
template<> std::string str_(std::vector<shop_command> const&v) {
    return str_<shop_command, 0, 0>(v); // Force printing of all entries (max_items=0) and on separate lines (compact=0)
}

void expose_shop_optimize(shyft::energy_market::stm::stm_system& stm, utctime t0, utctime t_end, utctimespan dt, const vector<shyft::energy_market::stm::shop::shop_command>& cmds, bool log_to_stdstream, bool log_to_files) {
    calendar utc;
    size_t n_steps = utcperiod(t0,t_end).diff_units(utc,dt);
    shyft::time_axis::generic_dt ta{t0, dt,n_steps};
    return shop_system::optimize(stm, ta, cmds, log_to_stdstream, log_to_files);
}

void expose_shop_optimize_ta(shyft::energy_market::stm::stm_system& stm, shyft::time_axis::generic_dt ta, const vector<shop_command>& cmds, bool log_stream, bool log_file) {
    return shop_system::optimize(stm, ta, cmds, log_stream, log_file);
}

struct shop_system_ext {
    static shop_system *create_from_period_and_dt(utcperiod p, utctimespan dt) {
        calendar utc;
        size_t n_steps = p.diff_units(utc,dt);
        shyft::time_axis::generic_dt ta{p.start, dt,n_steps};
        return new shop_system(ta);
    }
};

// Exposing stm::shop::shop_system
static void expose_shop_system() {

    expose_vector<string>("StringList", "A strongly typed list of strings."); // shop_command.objects, shop_command.options, shop_commander.executed_raw return list of strings

    py::enum_<shop_log_entry::log_severity>("ShopLogSeverity")
        .value("INFORMATION", shop_log_entry::log_severity::information)
        .value("DIAGNOSIS_INFORMATION", shop_log_entry::log_severity::diagnosis_information)
        .value("WARNING", shop_log_entry::log_severity::warning)
        .value("DIAGNOSIS_WARNING", shop_log_entry::log_severity::diagnosis_warning)
        .value("ERROR", shop_log_entry::log_severity::error)
        .value("DIAGNOSIS_ERROR", shop_log_entry::log_severity::diagnosis_error)
        .export_values()
        ;

    auto x=py::class_<
        shop_log_entry,
        py::bases<>
    >("ShopLogEntry", "A shop log entry, produced by the shop core.", py::no_init)
        .def_readonly("time",&shop_log_entry::time,"The timestamp for when the entry was produced.")
        .def_readonly("severity",&shop_log_entry::severity,"The severity of the message.")
        .def_readonly("code",&shop_log_entry::code,"The numerical identifier of the log message type.")
        .def_readonly("message",&shop_log_entry::message,"The textual message, or description.")
        .def(py::self == py::self)
        .def(py::self != py::self)
    ;
    expose_str_repr(x);
    expose_vector<shop_log_entry>("ShopLogEntryList", "A strongly typed list of ShopLogEntry.");

    py::class_<
        shop_system,
        py::bases<>,
        shared_ptr<shop_system>,
        boost::noncopyable
    >("ShopSystem", "A shop system, managing a session to the shop core.", py::no_init)
        .def("__init__",py::make_constructor(&shop_system_ext::create_from_period_and_dt,py::default_call_policies()),
                doc_intro("Create shop system, initialize with fixed time resolution. note that you should provide valid period and reasonable dt")
        )
        .def(py::init<const generic_dt&>(
            (py::arg("time_axis")),
            "Create shop system, initialize with time axis."))
        .def_readonly("commander",&shop_system::commander,
            doc_intro("Get shop commander object.")
            doc_details("Use it to send individual commands to the shop core."))
        .def("get_version_info", &shop_system::get_version_info, (py::arg("self")), "Get version information from the shop core.")
        .def("set_logging_to_stdstreams",&shop_system::set_logging_to_stdstreams,(py::arg("self"),
            py::arg("on") = true),
            doc_intro("Turn on or off logging from the shop core to standard output and error streams (console).")
            doc_details("Default is off.")
            doc_parameters()
            doc_parameter("on", "bool", "Turn on, set false to turn off (default true)."))
        .def("set_logging_to_files",&shop_system::set_logging_to_files,(py::arg("self"),
            py::arg("on") = true),
            doc_intro("Turn on or off logging from the shop core to files.")
            doc_details("When enabled shop will write its execution log file (shop_messages.log)\n"
                        "and optimization log file (cplex.log) to current directory. Custom log\n"
                        "files specified with the 'log_file' command will also not be written\n"
                        "unless enabled.\n"
                        "Default is off.")
            doc_parameters()
            doc_parameter("on", "bool", "Turn on, set false to turn off (default true)."))
        .def("get_log_buffer", &shop_system::get_log_buffer, (py::arg("self"),
            py::arg("limit")),
            doc_intro("Get specified number of entries from log buffer.")
            doc_details("Retrieved log entries will be removed from shop's internal log buffer,\n"
                "so a following call will not retrieve the same entries.\n"
                "The log buffer has a maximum capacity of 1024 log entries,\n"
                "this function retrieves up to a specified number of entries.")
            doc_parameters()
            doc_parameter("limit", "int", "Limit number of returned log entries.")
            doc_returns("entries", "ShopLogEntryList", "List of log entries."))
        .def<vector<shop_log_entry>(*)(shop_system&)>("get_log_buffer", //&shop_system::get_log_buffer,(py::arg("self"),
            [](shop_system& o)->vector<shop_log_entry>{return o.get_log_buffer();},
            (py::arg("self")),
            doc_intro("Get all entries from log buffer.")
            doc_details("Retrieved log entries will be removed from shop's internal log buffer,\n"
                "so a following call will not retrieve the same entries.\n"
                "The log buffer has a maximum capacity of 1024 log entries,\n"
                "this function will retrieve all.")
            doc_returns("entries", "ShopLogEntryList", "List of log entries."))
        .def("emit",&shop_system::emit,(py::arg("self"),
            py::arg("stm_system")),
            "Emit a stm system into the shop core.")
        .def("command",&shop_system::command,(py::arg("self"),
            py::arg("commands")),
            "Send a set of commands to the shop core.")
        .def("collect",&shop_system::collect,(py::arg("self"),
            py::arg("stm_system")),
            doc_intro("Collect results from shop core into stm system.")
            doc_details("The stm system should be the one previously emitted.")
            doc_parameters()
            doc_parameter("stm_system","StmSystem","The stm system to collect results into."))
        .def("complete",&shop_system::complete,(py::arg("self"),
            py::arg("stm_system")),
            doc_intro("Complete the stm system by performing relevant post-processing tasks.")
            doc_details("The stm system should be previously emitted, optimized,\n"
                        "and collected results into. This will then perform additional\n"
                        "calculations based on the results, such as filling in discharge\n"
                        "in waterway segments not handled by shop core.")
            doc_parameters()
            doc_parameter("stm_system","StmSystem","The stm system to update."))
        .def("optimize2",&expose_shop_optimize,
            (py::arg("stm_system"),py::arg("time_begin"),py::arg("time_end"),py::arg("time_step"),py::arg("commands"),py::arg("logging_to_stdstreams"),py::arg("logging_to_files")),
            doc_intro("Run one-step optimization with fixed time resolution.")
            doc_details("This will initialize a shop system with fixed time resolution,\n"
                        "emit stm system, send commands, collect and complete results.")
            doc_parameters()
            doc_parameter("stm_system","StmSystem","The stm system to optimize.")
            doc_parameter("time_begin","time","The start time for the optimization.")
            doc_parameter("time_end", "time", "The end time for the optimization.")
            doc_parameter("time_step", "utctime", "The time step of the optimization.")
            doc_parameter("commands", "ShopCommandList", "The commands to send to the shop api core.")
            doc_parameter("logging_to_stdstreams", "bool", "Allow logging from the shop core to standard output and error streams (console).")
            doc_parameter("logging_to_files", "bool", "Allow logging from the shop core to log files.")).staticmethod("optimize2")
        .def("optimize", &expose_shop_optimize_ta,
            (py::arg("stm_system"),py::arg("time_axis"),py::arg("commands"),py::arg("logging_to_stdstreams"),py::arg("logging_to_files")),
            doc_intro("Run one-step optimization with time axis.")
            doc_details("This will initialize a shop system with time axis,\n"
                        "emit stm system, send commands, collect and complete results.")
            doc_parameters()
            doc_parameter("stm_system","StmSystem","The stm system to optimize.")
            doc_parameter("time_axis","TimeAxis","The time axis for the optimization.")
            doc_parameter("commands", "ShopCommandList", "The commands to send to the shop api core.")
            doc_parameter("logging_to_stdstreams", "bool", "Allow logging from the shop core to standard output and error streams (console).")
            doc_parameter("logging_to_files", "bool", "Allow logging from the shop core to log files.")).staticmethod("optimize")
        .def("export_yaml_string", &shop_system::export_yaml,
            (py::arg("self"), py::arg("input_only") = false, py::arg("compress_txy") = false, py::arg("compress_connection") = false),
            doc_intro("Export emitted system as YAML formatted string value.")
            doc_details("The export is performed directly by the external Shop API.")
            doc_parameters()
            doc_parameter("input_only", "bool", "Include input data only.")
            doc_parameter("compress_txy", "bool", "Compress time series.")
            doc_parameter("compress_connection", "bool", "Compress connections.")
            doc_returns("YAML", "str", "System as YAML formatted string."))
        .def<string(*)(const shop_system&,bool,bool)>("export_topology_string",
            [](const shop_system& o,bool all,bool raw)->string{return string_wrap_streaming_function(o,&shop_system::export_topology,all,raw);},
            (py::arg("self"), py::arg("all")=false, py::arg("raw")=false),
            doc_intro("Export emitted topology as a DOT formatted string value.")
            doc_details("Use the Graphviz package to generate picture file from it.")
            doc_parameters()
            doc_parameter("all","bool","Include all objects, also non-topological.")
            doc_parameter("raw", "bool", "Raw export showing all details.")
            doc_paramcont("This disables the default mangling, intended to give nicer output")
            doc_paramcont("at the risk of hiding away some important details.")
            doc_returns("DOT","str","Topology as DOT formatted string."))
        .def<void(*)(const shop_system&,bool,bool)>("export_topology",
            [](const shop_system& o,bool all,bool raw){o.export_topology(all,raw, std::cout);},
            (py::arg("self"), py::arg("all")=false, py::arg("raw")=false),
            doc_intro("Export emitted topology in DOT format to standard output.")
            doc_details("Copy and paste into your favorite Graphviz editor.")
            doc_parameters()
            doc_parameter("all","bool","Include all objects, also non-topological.")
            doc_parameter("raw", "bool", "Raw export showing all details.")
            doc_paramcont("This disables the default mangling, intended to give nicer output")
            doc_paramcont("at the risk of hiding away some important details."))
        .def<string(*)(const shop_system&,bool)>("export_data_string",
            [](const shop_system& o,bool all)->string{return string_wrap_streaming_function(o,&shop_system::export_data,all);},
            (py::arg("self"), py::arg("all")=false),
            doc_intro("Export emitted data objects and attributes as a JSON formatted string value.")
            doc_details("Use a JSON package to decode it.")
            doc_parameters()
            doc_parameter("all","bool","Include all attributes, even non-existing ones.")
            doc_returns("JSON","str","Data objects and attributes as JSON formatted string."))
        .def<void(*)(const shop_system&,bool)>("export_data",
            [](const shop_system& o,bool all){o.export_data(all,std::cout);},
            (py::arg("self"), py::arg("all")=false),
            doc_intro("Export emitted data objects and attributes in JSON format to standard output.")
            doc_details("Copy and paste into your favorite JSON editor.")
            doc_parameters()
            doc_parameter("all","bool","Include all attributes, even non-existing ones."))

        .def<string(*)()>("environment_string",[]()->string{return string_wrap_streaming_function(&shop_system::environment);},
            "Get all environment variables as a newline delimited string value.").staticmethod("environment_string")
        .def<void(*)()>("environment",[](){shop_system::environment(std::cout);},
            "Print all environment variables to standard output.").staticmethod("environment")
    ;
}

}

BOOST_PYTHON_MODULE(_shop) {
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    py::scope().attr("__doc__") = "Statkraft Energy Market short term model Shop adapter";
    py::scope().attr("__version__") = shyft::_version_string();
    py::scope().attr("shyft_with_shop") = true; // True if shyft was built with Shop integrations.
    py::scope().attr("shop_api_version") = BOOST_PP_STRINGIZE(SHOP_API_VERSION); // Version number of the Shop API package shyft was built with - when built with Shop integrations (i.e. shyft_with_shop is True)
    expose::expose_shop_system();
    expose::expose_shop_command();
    expose::expose_shop_commander();
}
#else
namespace py=boost::python;
BOOST_PYTHON_MODULE(_shop) {
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    py::scope().attr("__doc__") = "Dummy Stub for: Statkraft Energy Market short term model Shop adapter";
    py::scope().attr("__version__") = shyft::_version_string();
    py::scope().attr("shyft_with_shop") = false;
    py::scope().attr("shop_api_version") = "";
}
#endif
