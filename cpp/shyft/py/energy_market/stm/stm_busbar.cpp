/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <boost/format.hpp>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::busbar const& o) {
        return (format("Busbar(id=%1%, name='%2%')")
            %o.id
            %o.name
        ).str();
    }

    void stm_busbar() {
        auto c=py::class_<
            stm::busbar,
            py::bases<>,
            shared_ptr<stm::busbar>,
            boost::noncopyable
        >("Busbar",
          doc_intro("A hub connected by transmission lines"),
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::network_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("net")),
                "Create busbar with unique id and name for a network."))
            .def_readwrite("id",&stm::busbar::id,"Unique id for this object.")
            .def_readwrite("name",&stm::busbar::name,"Name for this object.")
            .def_readwrite("json",&stm::busbar::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::busbar& self){return url_tag(self);}, "Url tag.")
            .def("__eq__", &stm::busbar::operator==)
            .def("__ne__", &stm::busbar::operator!=)
            .def("flattened_attributes", +[](stm::busbar& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
            .def("get_transmission_lines_from_busbar", &stm::busbar::get_transmission_lines_from_busbar, "Get any transmission lines connected from this busbar")
            .def("get_transmission_lines_to_busbar", &stm::busbar::get_transmission_lines_to_busbar, "Get any transmission lines connected to this busbar")
            .def("get_power_modules", &stm::busbar::get_power_modules, "Get any power modules associated with this busbar")
            .def("add_to_start_of_transmission_line", &stm::busbar::add_to_start_of_transmission_line, "Add this busbar to the start of a transmission line")
            .def("add_to_end_of_transmission_line", &stm::busbar::add_to_end_of_transmission_line, "Add this busbar to the end of a transmission line")
            .def("add_to_power_module", &stm::busbar::add_to_power_module, "Associate a power module to this busbar")
            
        ;
        expose_str_repr(c);
        expose_tsm(c);

        add_proxy_property(c,"dummy", stm::busbar, dummy, "dummy variable")

        expose_vector_eq<stm::busbar_>("BusbarList", "A strongly typed list of Busbar.", &stm::equal_attribute<std::vector<stm::busbar_>>, false);
    }
}
