/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <boost/format.hpp>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::transmission_line const& o) {
        return (format("TransmissionLine(id=%1%, name='%2%')")
            %o.id
            %o.name
        ).str();
    }

    void stm_transmission_line() {
        auto c=py::class_<
            stm::transmission_line,
            py::bases<>,
            shared_ptr<stm::transmission_line>,
            boost::noncopyable
        >("TransmissionLine",
          doc_intro("A transmission line connecting two busbars."),
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::network_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("net")),
                "Create transmission line with unique id and name for a network."))
            .def_readwrite("id",&stm::transmission_line::id,"Unique id for this object.")
            .def_readwrite("name",&stm::transmission_line::name,"Name for this object.")
            .def_readwrite("json",&stm::transmission_line::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::transmission_line& self){return url_tag(self);}, "Url tag.")
            .def_readonly("from_bb",&stm::transmission_line::from_bb,"Busbar connected from this transmission line")
            .def_readonly("to_bb",&stm::transmission_line::to_bb,"Busbar connected to this transmission line")
            .def("__eq__", &stm::transmission_line::operator==)
            .def("__ne__", &stm::transmission_line::operator!=)
            .def("flattened_attributes", +[](stm::transmission_line& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(c);
        expose_tsm(c);

        add_proxy_property(c,"capacity", stm::transmission_line, capacity, "Transmission line capacity")

        expose_vector_eq<stm::transmission_line_>("TransmissionLineList", "A strongly typed list of TransmissionLine.", &stm::equal_attribute<std::vector<stm::transmission_line_>>, false);
    }
}
