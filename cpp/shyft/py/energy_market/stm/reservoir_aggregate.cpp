/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/api/expose_container.h>
#include <boost/format.hpp>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;
    using boost::format;

    template<> string str_(stm::reservoir_aggregate const& o) {
        return (format("ReservoirAggregate(id=%1%, name=%2%)")
                %str_(o.id)
                %str_(o.name)
        ).str();
    }

    template<> string str_(stm::reservoir_aggregate::inflow_ const& o) {
        return (format("_ReservoirAggregateInflow(schedule=%1%, realised=%2%, result=%3%)")
                %str_(o.schedule)
                %str_(o.realised)
                % str_(o.result)
        ).str();
    }

    template<> string str_(stm::reservoir_aggregate::volume_ const& o) {
        return (format("_ReservoirAggregateVolume(static_max=%1%, schedule=%2%, realised=%3%, result=%4%)")
                %str_(o.static_max)
                %str_(o.schedule)
                %str_(o.realised)
                %str_(o.result)
        ).str();
    }

    void stm_reservoir_aggregate() {
        auto ra=py::class_<
                stm::reservoir_aggregate,
                py::bases<>,
                shared_ptr<stm::reservoir_aggregate>,
                boost::noncopyable
                >("ReservoirAggregate",
                  doc_intro("A reservoir_aggregate keeping stm reservoirs.")
                  doc_details("After creating the reservoirs, create the reservoir aggregate,\n"
                              "and add the reservoirs to it.")
                , py::no_init);
        ra
            .def_readwrite("id",&stm::reservoir_aggregate::id,"Unique id for this reservoir aggregate.")
            .def_readwrite("name",&stm::reservoir_aggregate::name,"Name of the reservoir aggregate.")
            .def_readwrite("json",&stm::reservoir_aggregate::json,"Json payload to adapt py-extension.")
            .add_property("tag", +[](const stm::reservoir_aggregate& self){return url_tag(self);}, "Url tag.")
            .def_readonly("reservoirs",&stm::reservoir_aggregate::reservoirs)
            .def(py::init<int, const string&, const string&, stm::stm_hps_ &>(
                    (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
                    "Create reservoir aggregate with unique id and name for a hydro power system."))
            .def("add_reservoir",&stm::reservoir_aggregate::add_reservoir,(py::arg("self"),py::arg("reservoir")),
                doc_intro("Adds a reservoir to the reservoir aggregate.")
                doc_parameters()
                doc_parameter("reservoir","Reservoir","The reservoir to be added to the reservoir aggregate")
            )
            .def("remove_reservoir",&stm::reservoir_aggregate::remove_reservoir,(py::arg("self"),py::arg("reservoir")),
                 doc_intro("Remove a reservoir from the reservoir aggregate")
                 doc_parameters()
                 doc_parameter("reservoir","Reservoir","The reservoir to be removed from the reservoir aggregate")
            )
            .def_readonly("inflow", &stm::reservoir_aggregate::inflow, "Inflow attributes.")
            .def_readonly("volume", &stm::reservoir_aggregate::volume, "Volume attributes.")
            .def("__eq__", &stm::reservoir_aggregate::operator==)
            .def("__ne__", &stm::reservoir_aggregate::operator!=)
            .def("flattened_attributes", +[](stm::reservoir_aggregate& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(ra);

        {
            py::scope ra_scope=ra;
            auto  rai=py::class_<stm::reservoir_aggregate::inflow_, boost::noncopyable>("_Inflow", py::no_init);
            expose_str_repr(rai);

            _add_proxy_property(rai,"schedule", stm::reservoir_aggregate::inflow_,schedule, "Inflow schedule, time series.")
            _add_proxy_property(rai,"realised", stm::reservoir_aggregate::inflow_,realised, "Inflow realised, time series.")
            _add_proxy_property(rai,"result", stm::reservoir_aggregate::inflow_,result, "Inflow result, time series.")


            auto rav=py::class_<stm::reservoir_aggregate::volume_, boost::noncopyable>("_Volume", py::no_init);
            expose_str_repr(rav);
            _add_proxy_property(rav,"static_max", stm::reservoir_aggregate::volume_,static_max, "Production static max, time series.")
            _add_proxy_property(rav,"schedule", stm::reservoir_aggregate::volume_,schedule, "Production schedule, time series.")
            _add_proxy_property(rav,"realised", stm::reservoir_aggregate::volume_,realised, "Production realised, time series.")
            _add_proxy_property(rav,"result", stm::reservoir_aggregate::volume_,result, "Production result, time series.")


        }

        expose_vector_eq<stm::reservoir_aggregate_>("ReservoirAggregateList", "A strongly typed list of ReservoirAggregates.",&stm::equal_attribute<std::vector<stm::reservoir_aggregate_>>,false);

    }

}
