/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/api/expose_from_list.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::energy_market::stm::energy_market_area;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::utctime;
    using shyft::core::utcperiod;
    using shyft::core::calendar;
    using shyft::time_axis::generic_dt;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;
    
    static const char* group_type_string(int64_t gt) {
        using namespace stm::unit_group_type;
        switch(gt) {
            case unspecified: return "UNSPECIFIED";
            //-- operational reserve groups
            case fcr_n_up: return "FCR_N_UP";
            case fcr_n_down: return "FCR_N_DOWN";
            case fcr_d_up: return "FCR_D_UP";
            case fcr_d_down: return "FCR_D_DOWN";
            case afrr_up:return "AFRR_UP";
            case afrr_down: return "AFRR_DOWN";
            case mfrr_up: return "MFRR_UP";
            case mfrr_down: return "MFRR_DOWN";
            case ffr:return "FFR";
            case rr_up:return "RR_UP";
            case rr_down:return "RR_DOWN";
            //-- spinning
            case commit: return "COMMIT";
            //-- with production requirement
            case production:return "PRODUCTION";
        }
        return "UNKNOWN_GROUP_TYPE";
    }

    template<> string str_(stm::unit_group const& o) {
        return (format("UnitGroup(id=%1%, name=%2%, group_type=%3%)")
            %o.id
            %o.name
            %group_type_string(o.group_type) //.value_or(0)
        ).str();
    }
    
    template<> string str_(stm::unit_group_member const& o) {
        return (format("_Member(unit.id=%1%, unit.name='%2%')")
            %o.unit->id
            %o.unit->name
        ).str();
    }
    template<> string str_(stm::unit_group::obligation_ const& o) {
        return (format("_Obligation(schedule=%1%, cost=%2%,result=%3%,penalty=%4%)")
            %str_(o.schedule)
            %str_(o.cost)
            %str_(o.result)
            %str_(o.penalty)
        ).str();
    }
    template<> string str_(stm::unit_group::delivery_ const& o) {
        return (format("_Delivery(schedule=%1%, result=%2%,realised=%3%)")
            %str_(o.schedule)
            %str_(o.result)
            %str_(o.realised)
        ).str();
    }

    void stm_unit_group() {
        
        auto ug=py::class_<
            stm::unit_group,
            py::bases<>,
            shared_ptr<stm::unit_group>,
            boost::noncopyable
        >("UnitGroup", "A a group of Units, with constraints applicable to the sum of the unit-features (production, flow...), that the optimization can take into account.", py::no_init);
        ug
        .def_readwrite("id",&stm::unit_group::id,"Unique id for this group.")
        .def_readwrite("name",&stm::unit_group::name,"Name of the group.")
        .def_readwrite("json",&stm::unit_group::json,"Json payload to adapt py-extension.")
        .add_property("tag", +[](const stm::unit_group& self){return url_tag(self);}, "Url tag.")
        .def_readwrite("group_type",&stm::unit_group::group_type, "Unit group-type, one of GroupType enum, fcr_n_up, fcr_n_down etc.")
        .def_readonly("members",&stm::unit_group::members)
        .add_property("market_area",&stm::unit_group::get_energy_market_area,"Getter market area.")
        .def("add_unit",&stm::unit_group::add_unit,(py::arg("self"),py::arg("unit"),py::arg("active")),
            doc_intro("Adds a unit to the group, maintaining any needed expressions.")
            doc_parameters()
            doc_parameter("unit","Unit","The unit to be added to the group (sum expressions automagically updated).")
            doc_parameter("active","TimeSeries","Determine the temporal group-member-ship, if empty Ts, then always member.")
        )
        .def("remove_unit",&stm::unit_group::remove_unit,(py::arg("self"),py::arg("unit")),
            doc_intro("Remove a nunit from the group maintaining any needed expressions.")
            doc_parameters()
            doc_parameter("unit","Unit","The unit to be removed from the group (sum expressions automagically updated).")
        )
        .def("_update_sum_expressions",&stm::unit_group::update_sum_expressions,(py::arg("self")),
             doc_intro("update the sum-expressions, in the case this has not been done, or is out of sync.")
         )
        .def_readonly("obligation",&stm::unit_group::obligation,
                      doc_intro("Obligation schedule, cost, results and penalty.")
        )
        .def_readonly("delivery",&stm::unit_group::delivery,
                      doc_intro("Sum of product/obligation delivery schedule,result and realised for unit-members.")
        )
        .def(py::self==py::self)
        .def(py::self!=py::self)
        .add_property("obj", &py_object_ext<stm::unit_group>::get_obj, &py_object_ext<stm::unit_group>::set_obj)
        .def("flattened_attributes", +[](stm::unit_group& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")

        ;
        expose_str_repr(ug);
        expose_tsm(ug);
        //add_proxy_property(ug,"constraint",stm::unit_group,constraint,
        //    doc_intro("Constraint with limit,penalty etc.,valid for this unit-group")
        //);
        add_proxy_property(ug,"production",stm::unit_group,production,
            doc_intro("[W] the sum resulting production for this unit-group.")
        );
        add_proxy_property(ug,"flow",stm::unit_group,flow,
            doc_intro("[m3/s] the sum resulting water flow for this unit-group.")
        );
        //add_proxy_property(ug,"group_type", stm::unit_group,group_type,
        //    doc_intro("Unit group-type, one of GroupType enum, fcr_n_up, fcr_n_down etc. ")
        //);
        
        using UnitGroupList=std::vector<stm::unit_group_>;
        auto ul=py::class_<UnitGroupList>("UnitGroupList", "A strongly typed list of UnitsGroups.");
        ul
            .def(py::vector_indexing_suite<UnitGroupList, true>())
            .def("__init__",
                 construct_from<UnitGroupList>(py::default_call_policies(),(py::arg("unit_groups"))),
                "Construct from list."
            )
            .def("__eq__",+[](UnitGroupList const&a, UnitGroupList const &b)->bool {
                 return stm::equal_attribute(a,b);
                }
             )
            .def("__ne__",+[](UnitGroupList const&a, UnitGroupList const &b)->bool {
                 return !stm::equal_attribute(a,b);
                }
             )

        ;

        expose_str_repr(ul);
        /** scope unit_group_member like UnitGroup._Member*/{
            py::scope up_scope=ug;
            using ugt=stm::unit_group_type::unit_group_type_;
            py::enum_<ugt>("unit_group_type",
                doc_intro(
                "The unit-group type specifies the purpose of the group, and thus also how\n"
                "it is mapped to the optimization as constraint. E.g. operational reserve fcr_n.up.\n"
                "Current mapping to optimizer/shop:\n\n"
                "    *        FCR* :  primary reserve, instant response, note, sensitivity set by droop settings on the unit \n"
                "    *       AFRR* : automatic frequency restoration reserve, ~ minute response\n"
                "    *       MFRR* : NOT MAPPED, it is the manual frequency restoration reserve, ~ 15 minute response\n"
                "    *         FFR : fast frequency restoration reserve, 49.5..49.7 Hz, ~ 1..2 sec response\n"
                "    *        RR*  : replacement reserve, 40..60 min response\n"
                "    *      COMMIT : currently not mapped\n"
                "    *  PRODUCTION : used for energy market area unit groups\n\n"
                )
            )
            .value("UNSPECIFIED",ugt::unspecified)
            .value("FCR_N_UP",ugt::fcr_n_up)
            .value("FCR_N_DOWN",ugt::fcr_n_down)
            .value("FCR_D_UP",ugt::fcr_d_up)
            .value("FCR_D_DOWN",ugt::fcr_d_down)
            .value("AFRR_UP",ugt::afrr_up)
            .value("AFRR_DOWN",ugt::afrr_down)
            .value("MFRR_UP",ugt::mfrr_up)
            .value("MFRR_DOWN",ugt::mfrr_down)
            .value("FFR",ugt::ffr)
            .value("RR_UP",ugt::rr_up)
            .value("RR_DOWN",ugt::rr_down)
            .value("COMMIT",ugt::commit)
            .value("PRODUCTION",ugt::production)
            .export_values()
            ;
            auto ugo=py::class_<stm::unit_group::obligation_,py::bases<>,boost::noncopyable>(
                "_Obligation",
                doc_intro("This describes the obligation aspects of the unit group that the members should satisfy."),
                py::no_init
            );
            expose_str_repr(ugo);
            _add_proxy_property(ugo,"schedule",stm::unit_group::obligation_,schedule, "[W] schedule/target that should be met.");
            _add_proxy_property(ugo,"cost",stm::unit_group::obligation_,cost, "[money/W] the cost of not satisfying the schedule.");
            _add_proxy_property(ugo,"result",stm::unit_group::obligation_,result, "[W] the resulting target/slack met after optimization.");
            _add_proxy_property(ugo,"penalty",stm::unit_group::obligation_,penalty, "[money] If target violated, the cost of the violation.");
            
            auto ugd=py::class_<stm::unit_group::delivery_,py::bases<>,boost::noncopyable>(
                "_Delivery",
                doc_intro("This describes the sum of product delivery aspects for the units."),
                py::no_init
            );
            expose_str_repr(ugd);
            _add_proxy_property(ugd,"schedule",stm::unit_group::delivery_,schedule, "[product-unit] sum schedule");
            _add_proxy_property(ugd,"result",stm::unit_group::delivery_,result, "[product-unit] sum result");
            _add_proxy_property(ugd,"realised",stm::unit_group::delivery_,realised, "[product-unit] sum realised, as historical fact");

            auto ugm=py::class_<
                stm::unit_group_member,
                py::bases<>,
                shared_ptr<stm::unit_group_member>,
                boost::noncopyable
            >("_Member", "A unit group member, refers to a specific unit along with the time-series that takes care of the temporal membership/contribution to the unit_group sum.", py::no_init);

            ugm.def_readonly("unit",&stm::unit_group_member::unit
               ,doc_intro("The unit this member represents.")
            );
            add_proxy_property(ugm,"active",stm::unit_group_member,active,
                doc_intro("[unit-less] if available, this time-series is multiplied with the contribution from the unit.")
            );
            expose_str_repr(ugm);

            using UnitGroupMemberList=std::vector<stm::unit_group_member_>;
            auto ugml=py::class_<UnitGroupMemberList>("_MemberList", "A strongly typed list of UnitsGroup._Member.");
            ugml
                .def(py::vector_indexing_suite<UnitGroupMemberList, true>())
                .def("__init__",
                    construct_from<UnitGroupMemberList>(py::default_call_policies(),(py::arg("unit_group_members."))),
                    "Construct from list."
                )
            ;
            expose_str_repr(ugml);
            
        }
    }
}
