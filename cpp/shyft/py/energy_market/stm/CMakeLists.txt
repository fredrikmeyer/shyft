# ensure to have python last, since it could contain ref to another boost version

add_library(stm SHARED
    expose_str.cpp
    stm_api.cpp
    dstm_server.cpp
    hps_client_server.cpp
    stm_task_server.cpp
    stm_client_server.cpp
    basic_types.cpp
    run_parameters.cpp
    optimization_summary.cpp
    reservoir.cpp
    reservoir_aggregate.cpp
    unit.cpp
    unit_group.cpp
    stm_power_module.cpp
    stm_busbar.cpp
    stm_network.cpp
    stm_transmission_line.cpp
    waterway.cpp
    gate.cpp
    power_plant.cpp
    stm_system.cpp
    stm_contract.cpp
    hps.cpp
)
target_include_directories(stm PRIVATE ${python_include} ${python_numpy_include})
target_link_libraries(
    stm
    PRIVATE shyft_private_flags stm_core ${boost_py_link_libraries} ${python_lib}
    PUBLIC shyft_public_flags
)

set_target_properties(stm PROPERTIES
    OUTPUT_NAME stm
    VISIBILITY_INLINES_HIDDEN TRUE
    PREFIX "_" # Python extensions do not use the 'lib' prefix
    SUFFIX ${py_ext_suffix}
    INSTALL_RPATH "$ORIGIN/../../lib"
)

install(TARGETS stm ${shyft_runtime} DESTINATION ${SHYFT_PYTHON_DIR}/shyft/energy_market/stm)

