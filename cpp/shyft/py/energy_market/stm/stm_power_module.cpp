/** This file is part of Shyft. Copyright 2015-2022 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_tsm_expose.h>

#include <boost/format.hpp>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/busbar.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using std::string;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::power_module const& o) {
        return (format("PowerModule(id=%1%, name='%2%')")
            %o.id
            %o.name
        ).str();
    }

    template<> string str_(stm::power_module::power_ const& o) {
        return (format("PowerModule._Power(realised=%1%, schedule=%2%, result=%3%)")
            %str_(o.realised)
            %str_(o.schedule)
            %str_(o.result)
        ).str();
    }

    void stm_power_module() {
        auto c=py::class_<
            stm::power_module,
            py::bases<>,
            shared_ptr<stm::power_module>,
            boost::noncopyable
        >("PowerModule",
          doc_intro("A power module representing consumption."),
          py::no_init);
        c
            .def(py::init<int, const string&, const string&, stm::stm_system_&>(
                (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("sys")),
                "Create power module with unique id and name for a stm system."))
            .def_readwrite("id",&stm::power_module::id,"Unique id for this object.")
            .def_readwrite("name",&stm::power_module::name,"Name for this object.")
            .def_readwrite("json",&stm::power_module::json,"Json keeping any extra data for this object.")
            .add_property("tag", +[](const stm::power_module& self){return url_tag(self);}, "Url tag.")
            .def_readonly("connected",&stm::power_module::connected,"Associated busbar.")
            .def_readonly("power", &stm::power_module::power, "Power attributes.")
            .def("__eq__", &stm::power_module::operator==)
            .def("__ne__", &stm::power_module::operator!=)
            .def("flattened_attributes", +[](stm::power_module& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(c);
        expose_tsm(c);

        expose_vector_eq<stm::power_module_>("PowerModuleList", "A strongly typed list of PowerModule.", &stm::equal_attribute<std::vector<stm::power_module_>>, false);
    
        {
            py::scope scope_unit=c;
            auto p=py::class_<stm::power_module::power_, py::bases<>, boost::noncopyable>(
                "_Power", 
                doc_intro("Unit.Power attributes, consumption[W, J/s]."),
                py::no_init
            );
            expose_str_repr(p);
            _add_proxy_property(p, "realised", stm::power_module::power_,realised, "Historical fact, time series. W, J/s, if positive then production, if negative then consumption.")
            _add_proxy_property(p, "schedule", stm::power_module::power_,schedule, "The current schedule, time series. W, J/s, if positive then production, if negative then consumption")
            _add_proxy_property(p, "result", stm::power_module::power_,result, "The optimal/simulated/estimated result, time series. W, J/s, if positive then production, if negative then consumption")
        }
    }
}
