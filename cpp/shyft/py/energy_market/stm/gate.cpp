/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <boost/format.hpp>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/waterway.h>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::core::utctime;
    using shyft::core::utctimespan;
    using shyft::core::utcperiod;
    using shyft::core::calendar;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;
    using shyft::time_axis::generic_dt;
    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<> string str_(stm::gate const& g) {
        return (format("Gate(flow_description=%1%)")%str_(g.flow_description)).str();
    }

    template<> string str_(stm::gate::opening_ const& o) {
        return (format("_GateOpening(schedule=%1%, realised=%2%, result=%3%)")%str_(o.schedule)%str_(o.realised)%str_(o.result)).str();
    }

    template<> string str_(stm::gate::opening_::constraint_ const& o) {
        return (format("_GateOpeningConstraint(positions=%1%, continuous=%2%")
                %str_(o.positions)
                %str_(o.continuous)
        ).str();
    }

    template<> string str_(stm::gate::discharge_::constraint_ const& o) {
        return (format("_GateDischargeConstraint(min=%1%, max=%2%")
                %str_(o.min)
                %str_(o.max)
        ).str();
    }

    template<> string str_(stm::gate::discharge_ const& o) {
        return (format("_GateDischarge(schedule=%1%, constraint=%2%, realised=%3%, result=%4%, static_max=%5%)")
            %str_(o.schedule)
            %str_(o.constraint)
            %str_(o.realised)
            %str_(o.result)
            %str_(o.static_max)
        ).str();
    }

    void stm_gate() {
        auto g=py::class_<
            stm::gate,
            py::bases<hydro_power::gate>,
            shared_ptr<stm::gate>,
            boost::noncopyable
        >("Gate", "Stm gate.", py::no_init);
        g
            .def_readonly("opening", &stm::gate::opening, "Opening attributes.")
            .def_readonly("discharge", &stm::gate::discharge, "Discharge attributes.")
            .add_property("tag", +[](const stm::gate& self){return url_tag(self);}, "Url tag.")

            .def(py::self==py::self)
            .def(py::self!=py::self)

            .def("flattened_attributes", +[](stm::gate& self) { return make_flat_attribute_dict(self); }, "Flat dict containing all component attributes.")
        ;
        expose_str_repr(g);
        expose_tsm(g);
        add_proxy_property(g,"flow_description", stm::gate,flow_description, 
            doc_intro("Gate flow description. Flow [m^3/s] as a function of water level [m] for relative gate opening [%].")
        )

        add_proxy_property(g,"flow_description_delta_h", stm::gate,flow_description_delta_h,
            doc_intro("Gate flow description. Flow [m^3/s] as a function of water level difference [m] for relative gate opening [%].")
        )

        add_proxy_property(g,"cost", stm::gate,cost, 
            doc_intro("Gate adjustment cost, time series.")
        )

        {
            py::scope scope_gate=g;
            
            auto go=py::class_<stm::gate::opening_, py::bases<>, boost::noncopyable>("_Opening", py::no_init);
            go
                .def_readonly("constraint", &stm::gate::opening_::constraint, "Opening constraint attributes.");
            
            _add_proxy_property(go,"schedule", stm::gate::opening_,schedule, "Planned opening schedule, value between 0.0 and 1.0, time series.")
            _add_proxy_property(go,"realised", stm::gate::opening_,realised, "Historical opening schedule, value between 0.0 and 1.0, time series.")
            _add_proxy_property(go,"result",  stm::gate::opening_,result, "Result opening schedule, value between 0.0 and 1.0, time series.")
            
            expose_str_repr(go);
            
            auto gd=py::class_<stm::gate::discharge_, py::bases<>, boost::noncopyable>("_Discharge", py::no_init);
            gd
                .def_readonly("constraint", &stm::gate::discharge_::constraint, "Discharge constraint attributes.");

            _add_proxy_property(gd,"schedule",  stm::gate::discharge_,schedule, "[m^3/s]Discharge schedule restriction, time series.")
            _add_proxy_property(gd,"realised",  stm::gate::discharge_,realised, "[m^3/s]Historical discharge restriction, time series.")
            _add_proxy_property(gd,"result",    stm::gate::discharge_,result, "[m^3/s]Discharge result, time series.")
            _add_proxy_property(gd,"static_max",stm::gate::discharge_,static_max, "[m^3/s]Maximum discharge, time series.")
            _add_proxy_property(gd,"merge_tolerance",stm::gate::discharge_,merge_tolerance, "[m^3/s]Maximum deviation in discharge between two timesteps, time series.")

            expose_str_repr(gd);

            {
                py::scope gdc=gd;
                auto gdcc=py::class_<stm::gate::opening_::constraint_, py::bases<>, boost::noncopyable>("_Constraints", py::no_init);
                expose_str_repr(gdcc);
                _add_proxy_property(gdcc,"positions", stm::gate::opening_::constraint_,positions, "Predefined gate positions mapped to gate opening.")
                _add_proxy_property(gdcc,"continuous", stm::gate::opening_::constraint_,continuous, "Flag determining whether the gate can be set to anything between predefined positions, time-dependent attribute.")
            }

            {
                py::scope gdc=gd;
                auto gdcc=py::class_<stm::gate::discharge_::constraint_, py::bases<>, boost::noncopyable>("_Constraints", py::no_init);
                expose_str_repr(gdcc);
                _add_proxy_property(gdcc,"min", stm::gate::discharge_::constraint_,min, "[masl] Discharge constraint minimum, time series.")
                _add_proxy_property(gdcc,"max", stm::gate::discharge_::constraint_,max, "[masl] Discharge constraint maximum, time series.")
            }
        }
    }
}
