#include <shyft/py/api/boostpython_pch.h>
#include <boost/python/module.hpp>
#include <boost/python/scope.hpp>
#include <boost/python/docstring_options.hpp>
#include <shyft/version.h>

namespace expose {
    extern void stm_reservoir() ;
    extern void stm_reservoir_aggregate() ;
    extern void stm_unit() ;
    extern void stm_unit_group() ;
    extern void stm_power_module();
    extern void stm_busbar();
    extern void stm_network();
    extern void stm_transmission_line();
    extern void stm_power_plant();
    extern void stm_waterway() ;
    extern void stm_gate();
    extern void stm_basic_attributes();
    extern void stm_system();
    extern void stm_contract();
    extern void stm_optimization_summary();
    extern void stm_hps();
    extern void stm_run_parameters();
    extern void hps_client_server();
    extern void stm_client_server();
    extern void dstm_client_server();
    extern void dstm_server_logging();
    extern void stm_task_server();
}

BOOST_PYTHON_MODULE(_stm) {
    namespace py=boost::python;
    py::docstring_options doc_options(true, true, false);// all except c++ signatures
    py::scope().attr("__doc__") = "Shyft Energy Market detailed model";
    py::scope().attr("__version__") = shyft::_version_string();
    expose::stm_basic_attributes();
    expose::stm_reservoir() ;
    expose::stm_reservoir_aggregate();
    expose::stm_unit() ;
    expose::stm_unit_group() ;
    expose::stm_power_plant();
    expose::stm_waterway() ;
    expose::stm_gate();
    expose::stm_hps();
    expose::stm_run_parameters();
    expose::stm_system();
    expose::stm_contract();
    expose::stm_power_module();
    expose::stm_busbar();
    expose::stm_network();
    expose::stm_transmission_line();
    expose::stm_optimization_summary();
    expose::hps_client_server();
    expose::stm_client_server();
    expose::dstm_client_server();
    expose::dstm_server_logging();
    expose::stm_task_server();
}
