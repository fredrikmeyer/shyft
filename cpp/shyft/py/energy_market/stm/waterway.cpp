/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>
#include <shyft/py/energy_market/py_attr_wrap.h>
#include <shyft/py/energy_market/py_tsm_expose.h>
#include <shyft/py/api/expose_container.h>
#include <boost/format.hpp>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/waterway.h>

namespace py = boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

    template<>
    string str_(stm::waterway::discharge_ const &o) {
        return (format("Waterway._Discharge(result=%1%, static_max=%2%, schedule=%3%, result=%4%)")
                % str_(o.result)
                % str_(o.static_max)
                % str_(o.schedule)
                % str_(o.result)
                % str_(o.reference)
        ).str();
    }

    template<>
    string str_(stm::waterway::discharge_::constraint_ const &o) {
        return (format(
                "_Constraint(min=%1%, max=%2%, ramping_up=%3%, ramping_down=%4%, accumulated_min=%5%, accumulated_max%6%)")
                % str_(o.min)
                % str_(o.max)
                % str_(o.ramping_up)
                % str_(o.ramping_down)
                % str_(o.accumulated_min)
                % str_(o.accumulated_max)
        ).str();
    }

    template<>
    string str_(stm::waterway::discharge_::penalty_::cost_ const &o) {
        return (format(
                "_Cost(constraint_min=%1%, constraint_max=%2%, ramping_up=%3%, ramping_down=%4%, accumulated_min=%5%, accumulated_max%6%)")
                % str_(o.constraint_min)
                % str_(o.constraint_max)
                % str_(o.ramping_up)
                % str_(o.ramping_down)
                % str_(o.accumulated_min)
                % str_(o.accumulated_max)
        ).str();
    }

    template<>
    string str_(stm::waterway::discharge_::penalty_::result_ const &o) {
        return (format(
                "_Cost(constraint_min=%1%, constraint_max=%2%, ramping_up=%3%, ramping_down=%4%, accumulated_min=%5%, accumulated_max%6%)")
                % str_(o.constraint_min)
                % str_(o.constraint_max)
                % str_(o.ramping_up)
                % str_(o.ramping_down)
                % str_(o.accumulated_min)
                % str_(o.accumulated_max)
        ).str();
    }

    template<>
    string str_(stm::waterway::geometry_ const &o) {
        return (format("Waterway._Geometry(length=%1%, diameter=%2%, z0=%3%, z1=%4%)")
                % str_(o.length)
                % str_(o.diameter)
                % str_(o.z0)
                % str_(o.z1)
        ).str();
    }

    template<>
    string str_(stm::waterway const &o) {
        return (format("Waterway(id=%1%, name=%2%, geometry=%3%)")
                % str_(o.id)
                % str_(o.name)
                % str_(o.geometry)
        ).str();
    }

    void stm_waterway() {
        auto w = py::class_<
                stm::waterway,
                py::bases<hydro_power::waterway>,
                shared_ptr<stm::waterway>,
                boost::noncopyable
        >("Waterway", "Stm waterway.", py::no_init);
        w
                .def(py::init<int, const string &, const string &, stm::stm_hps_ &>(
                        (py::arg("uid"), py::arg("name"), py::arg("json"), py::arg("hps")),
                        "Create waterway with unique id and name for a hydro power system."))
                .def_readonly("geometry", &stm::waterway::geometry, "Geometry attributes.")
                .def_readonly("discharge", &stm::waterway::discharge, "Discharge attributes.")
                .add_property("tag", +[](const stm::waterway &self) { return url_tag(self); }, "Url tag.")
                .def("add_gate",
                     +[](std::shared_ptr<stm::waterway> &self, int uid, const string &name,
                         const string &json) -> stm::gate_ {
                         auto hps = std::static_pointer_cast<stm::stm_hps>(self->hps.lock());
                         stm::gate_ gate = stm::stm_hps_builder(hps).create_gate(uid, name, json);
                         stm::waterway::add_gate(self, gate);
                         return gate;
                     },
                     (py::arg("self"), py::arg("uid"), py::arg("name"), py::arg("json") = string("")),
                     "Create and add a new gate to the waterway.")
                .def("add_gate",
                     +[](std::shared_ptr<stm::waterway> &self, std::shared_ptr<stm::gate> &gate) -> stm::gate_ {
                         stm::waterway::add_gate(self, gate);
                         return gate;
                     },
                     (py::arg("self"), py::arg("gate")),
                     "Add an existing gate to the waterway.")

                .def(py::self==py::self)
                .def(py::self!=py::self)

                .def("flattened_attributes", +[](stm::waterway &self) { return make_flat_attribute_dict(self); },
                     "Flat dict containing all component attributes.");
        expose_str_repr(w);
        expose_tsm(w);
        add_proxy_property(w, "head_loss_coeff", stm::waterway, head_loss_coeff,
                           "Loss factor, time-dependent attribute.")
        add_proxy_property(w, "head_loss_func", stm::waterway, head_loss_func,
                           "Loss function, time-dependent attribute.")
        add_proxy_property(w, "delay", stm::waterway, delay,
                           "Time delay of flow.")
        {
            py::scope w_scope = w;
            auto wd = py::class_<stm::waterway::discharge_, boost::noncopyable>("_Discharge", py::no_init);
            wd
                    .def_readonly("constraint", &stm::waterway::discharge_::constraint,
                                  "Discharge constraint attributes")
                    .def_readonly("penalty", &stm::waterway::discharge_::penalty, "Discharge penalty attributes.");
            expose_str_repr(wd);
            _add_proxy_property(wd, "static_max", stm::waterway::discharge_, static_max,
                                "[m3/s] Discharge maximum, time-dependent attribute, bi-directional value.")
            _add_proxy_property(wd, "result", stm::waterway::discharge_, result,
                                "[m3/s] Discharge result, time series. As computed by optimization/simulation process.")
            _add_proxy_property(wd, "schedule", stm::waterway::discharge_, schedule,
                                "[m3/s] Discharge schedule, time series. As in wanted scheduled flow.")
            _add_proxy_property(wd, "reference", stm::waterway::discharge_, reference,
                                "[m3/s] Discharge reference, time series. Used as reference for the constraint criteria (might be different from .schedule).")
            _add_proxy_property(wd, "realised", stm::waterway::discharge_, realised,
                                "[m3/s] Discharge realised, time series. - as in historical fact. For the case of constraint, used to establish intial accumulated deviation at the start of the optimisation period.")

            {
                py::scope wd_scope = wd;

                auto wdc = py::class_<stm::waterway::discharge_::constraint_, py::bases<>, boost::noncopyable>(
                        "_Constraints",
                        doc_intro("The constraints for a waterway, provide means of controlling the flow, change of flow, or even accumulated volume of flow."),
                        py::no_init);
                _add_proxy_property(wdc, "min", stm::waterway::discharge_::constraint_, min,
                                    "[m3/s] Discharge constraint min flow.");
                _add_proxy_property(wdc, "max", stm::waterway::discharge_::constraint_, max,
                                    "[m3/s] Discharge constraint max flow.");
                _add_proxy_property(wdc, "ramping_up", stm::waterway::discharge_::constraint_, ramping_up,
                                    "[m3/s] Discharge constraint ramping_up.");
                _add_proxy_property(wdc, "ramping_down", stm::waterway::discharge_::constraint_, ramping_down,
                                    "[m3/s] Discharge constraint ramping_down.");
                _add_proxy_property(wdc, "accumulated_min", stm::waterway::discharge_::constraint_, accumulated_min,
                                    "[m3] allowed accumulated negative deviation volume, actual vs .reference. If set to nan at a timestep, the accumulator is reset to 0 starting from that timestep.");
                _add_proxy_property(wdc, "accumulated_max", stm::waterway::discharge_::constraint_, accumulated_max,
                                    "[m3] allowed accumulated positive deviation volume, actual vs .reference. If set to nan at a timestep, the accumulator is reset to 0 starting from that timestep.");
                expose_str_repr(wdc);

                auto wdp = py::class_<stm::waterway::discharge_::penalty_, py::bases<>, boost::noncopyable>(
                        "_Penalties", py::no_init);
                wdp
                        .def_readonly("cost", &stm::waterway::discharge_::penalty_::cost, "Penalty cost attributes.")
                        .def_readonly("result", &stm::waterway::discharge_::penalty_::result,
                                      "Penalty result attributes.");
                //expose_str_repr(wdp);
                {
                    py::scope wdp_scope = wdp;
                    auto wdpc = py::class_<stm::waterway::discharge_::penalty_::cost_, py::bases<>, boost::noncopyable>(
                            "_Costs", py::no_init);
                    _add_proxy_property(wdpc, "constraint_min", stm::waterway::discharge_::penalty_::cost_,
                                        constraint_min, "[m3/s] Penalty cost constraint_min.");
                    _add_proxy_property(wdpc, "constraint_max", stm::waterway::discharge_::penalty_::cost_,
                                        constraint_max, "[m3/s] Penalty cost constraint_max.");
                    _add_proxy_property(wdpc, "ramping_up", stm::waterway::discharge_::penalty_::cost_, ramping_up,
                                        "[m3/s] Penalty cost ramping_up.");
                    _add_proxy_property(wdpc, "ramping_down", stm::waterway::discharge_::penalty_::cost_, ramping_down,
                                        "[m3/s] Penalty cost ramping_down.");
                    _add_proxy_property(wdpc, "accumulated_min", stm::waterway::discharge_::penalty_::cost_,
                                        accumulated_min, "[money/m3] Penalty cost accumulated_min.");
                    _add_proxy_property(wdpc, "accumulated_max", stm::waterway::discharge_::penalty_::cost_,
                                        accumulated_max, "[money/m3] Penalty cost constraint accumulated_max.");
                    expose_str_repr(wdpc);

                    auto wdpr = py::class_<stm::waterway::discharge_::penalty_::result_, py::bases<>, boost::noncopyable>(
                            "_Results", py::no_init);
                    _add_proxy_property(wdpr, "constraint_min", stm::waterway::discharge_::penalty_::result_,
                                        constraint_min, "[money] Penalty result constraint_min.");
                    _add_proxy_property(wdpr, "constraint_max", stm::waterway::discharge_::penalty_::result_,
                                        constraint_max, "[money] Penalty result constraint_max.");
                    _add_proxy_property(wdpr, "ramping_up", stm::waterway::discharge_::penalty_::result_, ramping_up,
                                        "[money] Penalty result ramping_up.");
                    _add_proxy_property(wdpr, "ramping_down", stm::waterway::discharge_::penalty_::result_,
                                        ramping_down, "[money] Penalty result ramping_down.");
                    _add_proxy_property(wdpr, "accumulated_min", stm::waterway::discharge_::penalty_::result_,
                                        accumulated_min, "[money] Penalty result accumulated_min.");
                    _add_proxy_property(wdpr, "accumulated_max", stm::waterway::discharge_::penalty_::result_,
                                        accumulated_max, "[money] Penalty result constraint accumulated_max.");
                    expose_str_repr(wdpr);
                }
            }


            auto wg = py::class_<stm::waterway::geometry_, boost::noncopyable>("_Geometry", py::no_init);
            expose_str_repr(wg);
            _add_proxy_property(wg, "length", stm::waterway::geometry_, length,
                                "[m] Tunnel length, time-dependent attribute.")
            _add_proxy_property(wg, "diameter", stm::waterway::geometry_, diameter,
                                "[m] Tunnel diameter, time-dependent attribute.")
            _add_proxy_property(wg, "z0", stm::waterway::geometry_, z0,
                                "[masl] Tunnel inlet level, time-dependent attribute.")
            _add_proxy_property(wg, "z1", stm::waterway::geometry_, z1,
                                "[masl] Tunnel outlet level, time-dependent attribute.");
        }
        expose_vector_eq<stm::waterway_>("WaterwayList", "A strongly typed list of Waterways.",&stm::equal_attribute<std::vector<stm::waterway_>>,false);

    }
}
