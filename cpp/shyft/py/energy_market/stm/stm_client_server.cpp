/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/time/utctime_utilities.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <shyft/srv/db.h>

#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/py/energy_market/py_url_tag.h>
#include <shyft/py/energy_market/stm/expose_str.h>

#include <boost/format.hpp>

namespace py=boost::python;

namespace expose {
    using namespace shyft::energy_market;

    using shyft::time_series::dd::apoint_ts;

    using std::string;
    using std::to_string;
    using std::make_shared;
    using std::shared_ptr;

    using boost::format;

#if 0
    template<> string str_(stm::energy_market_area const& o) {
        return (format("MarketArea(id=%1%, name=%2%)")
            %str_(o.id)
            %str_(o.name)
        ).str();
    }
#endif

    void stm_client_server() {
        using model=shyft::energy_market::stm::stm_system;
        using client_t = shyft::py::energy_market::py_client<shyft::srv::client<model>>;
        using srv_t = shyft::py::energy_market::py_server<shyft::srv::server<shyft::srv::db<model>>>;

        shyft::py::energy_market::expose_client<client_t>("StmClient",
            "The client api for the stm repository server."
        );
        shyft::py::energy_market::expose_server<srv_t>("StmServer",
            "The server-side component for the stm energy_market model repository."
        );

    }
}
