/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

//#include <shyft/py/energy_market/stm/expose_str.h> // No; gcc reports "specialization after instantiation"
#include <shyft/py/api/expose_str.h>
#include <shyft/time/utctime_utilities.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <boost/format.hpp>

namespace expose {
    using namespace shyft::energy_market;

    using shyft::core::calendar;
    using shyft::energy_market::core::absolute_constraint;
    using shyft::energy_market::core::penalty_constraint;

    using std::string;
    using std::to_string;

    using boost::format;

    template<>
    string str_(const hydro_power::point &p) {
        return "("+to_string(p.x)+","+to_string(p.y)+")";
    }
    template<>
    string str_(const hydro_power::xy_point_curve& m) {
        return str_(m.points);
    }
    
    template<>
    string str_(const stm::t_xy_::element_type& m) {
        string r("{");
        calendar utc;
        if (!std::empty(m)) {
            for (const auto& i : m) {
                r += "\n\t" + utc.to_string(i.first) + ": [";
                if (!std::empty(i.second->points)) {
                    for (const auto& p : i.second->points)
                        r += "(" + to_string(p.x) + "," + to_string(p.y) + "),";
                    r.back() = ']';
                } else {
                    r += "]";    
                }
                r += ",";
            }
            r.back() = '\n';
        }
        return r + "}";
    }

    template<>
    string str_(const stm::t_xyz_::element_type& m) {
        string r("{");
        calendar utc;
        if (!std::empty(m)) {
            for(const auto& i : m) {
                r += "\n\t" + utc.to_string(i.first) + ": z" + to_string(i.second->z) + " [";
                if (!std::empty(i.second->xy_curve.points)) {
                    for(const auto& p : i.second->xy_curve.points)
                        r += "(" + to_string(p.x) + "," + to_string(p.y) + "),";
                    r.back() = ']';
                } else {
                    r += "]";
                }
                r += ",";
            }
            r.back() = '\n';
        }
        return r + "}";
    }

    template<>
    string str_(stm::xyz_point_curve_list::value_type const& o) {
        string r;
        r += "{z@"+to_string(o.z) + ": [";
        if (!std::empty(o.xy_curve.points)) {
            for (const auto&p : o.xy_curve.points)
                r += "(" + to_string(p.x) + "," + to_string(p.y) + "),";
            r.back() = ']';
        } else {
            r += "]";
        }
        return r + "}";
    }

    template<>
    string str_(const stm::t_xyz_list_::element_type& m) {
        if (std::empty(m)) return "{}";
        string r("{");
        calendar utc;
        for(const auto& i : m) {
            r += "\n\t" + utc.to_string(i.first) + ": {";
            if (!std::empty(*i.second)) {
                for(const auto& j : *i.second) {
                    r += "\n\t\tz@" + to_string(j.z) + ": [";
                    if (!std::empty(j.xy_curve.points)) {
                        for(const auto& p : j.xy_curve.points)
                            r += "(" + to_string(p.x) + "," + to_string(p.y) + "),";
                        r.back() = ']';
                    } else {
                        r += "]";
                    }
                    r += ",";
                }
                r.back() = '\n';
                r += "\t}";
            }
            r += "},";
        }
        return r + "\n}";
    }

    template<> 
    string str_(const hydro_power::turbine_description &m) {
        string r("{");
        if (m.efficiencies.size() > 1) {
            // Pelton turbine: One turbine efficiency object for each needle combination, with additional min/max attributes.
            for (auto i=0u;i<m.efficiencies.size();++i) {
                const auto& eff = m.efficiencies[i];
                r += "\n\t\t" + to_string(i+1) + ": {";
                r += "\n\t\t\tmin@" + to_string(eff.production_min) + ",";
                r += "\n\t\t\tmax@" + to_string(eff.production_max) + ",";
                if (!std::empty(eff.efficiency_curves)) {
                    for (const auto& ec : eff.efficiency_curves) {
                        r += "\n\t\t\tz@"+to_string(ec.z) + ": [";
                        if (!std::empty(ec.xy_curve.points)) {
                            for (const auto& p : ec.xy_curve.points)
                                r += "(" + to_string(p.x) + "," + to_string(p.y) + "),";
                            r.back() = ']';
                        } else {
                            r += "]";
                        }
                        r += ",";
                    }
                }
                r.back() = '\n';
                r += "\t\t},";
            }
            r.back() = '\n';
            r += "\t}";
        } else if (m.efficiencies.size() > 0) {
            // Not a Pelton turbine: Only one turbine effieciency object, describing the entire turbine, ignoring min/max attributes.
            const auto& eff = m.efficiencies.front();
            if (!std::empty(eff.efficiency_curves)) {
                for (const auto& ec : eff.efficiency_curves) {
                    r += "\n\t\tz@"+to_string(ec.z) + ": [";
                    if (!std::empty(ec.xy_curve.points)) {
                        for(const auto&p:ec.xy_curve.points)
                            r += "(" + to_string(p.x) + "," + to_string(p.y) + "),";
                        r.back() = ']';
                    } else {
                        r += "]";
                    }
                    r += ",";
                }
                r.back() = '\n';
                r += "\t}";
            } else {
                r += "[]}";
            }
        } else {
            r += "}";
        }
        return r;
    }

    template<>
    string str_(const stm::t_turbine_description_::element_type& m) {
        if (std::empty(m)) return "{}";
        string r("{");
        calendar utc;
        for (const auto& i : m) {
            r += "\n\t" + utc.to_string(i.first) + ": ";
            r += str_(*i.second);
            r += ",";
        }
        r.back() = '\n';
        return r + "}";
    }

    template<>
    string str_(const absolute_constraint& m) {
        return (format("AbsoluteConstraint(limit=%1%, flag=%2%)")%m.limit.stringify()%m.flag.stringify()).str();
    }

    template<>
    string str_(const penalty_constraint& m) {
        return (format("PenaltyConstraint(limit=%1%, flag=%2%, cost=%3%, penalty=%4%)")%m.limit.stringify()%m.flag.stringify()%m.cost.stringify()%m.penalty.stringify()).str();
    }

}
