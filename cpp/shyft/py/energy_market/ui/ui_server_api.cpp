#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/energy_market/py_model_client_server.h>
#include <boost/format.hpp>

#include <shyft/energy_market/ui/ui_core.h>
#include <shyft/energy_market/ui/srv/server.h>
#include <shyft/energy_market/ui/srv/client.h>
#include <shyft/web_api/ui/request_handler.h>
#include <shyft/py/api/expose_container.h>
#include <shyft/py/api/expose_str.h>

#include <mutex>
#include <string>
#include <memory>
#include <csignal>
#include <shyft/py/scoped_gil.h>

namespace shyft::py::energy_market::ui {
    using std::string;
    using std::mutex;
    using std::unique_lock;
    using std::shared_ptr;
    using std::make_shared;
    using shyft::energy_market::ui::layout_info;
    using shyft::energy_market::ui::srv::config_server;
    using shyft::energy_market::ui::srv::config_client;

    namespace py = boost::python;
    /** @brief A  client for model type M suitable for python exposure
     *
     * This class takes care of  python gil and mutex, ensuring that any attempt using
     * multiple python threads will be serialized.
     * gil is released while the call is in progress.
     *
     * Using this template saves us the repeating work for similar model-repositories
     *
     * @tparam M a model, same requirements as for shyft::srv::client<M>
     *
     */
    struct py_config_client : shyft::py::energy_market::py_client<config_client> {
        using client_t=config_client;
        using super = shyft::py::energy_market::py_client<config_client>;
        py_config_client(const std::string& host_port,int timeout_ms): super{host_port,timeout_ms} {}
        ~py_config_client() { }

        py_config_client()=delete;
        py_config_client(py_config_client const&) = delete;
        py_config_client(py_config_client &&) = delete;
        py_config_client& operator=(py_config_client const&o) = delete;

        shared_ptr<M_t> read_model_with_args(int64_t mid, const string& layout_name, const string& args, bool store_layout=false) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.read_model_with_args(mid, layout_name, args, store_layout);
        }
    };

    /** @brief The server side component for a config repository
     *
     *
     * This class wraps/provides the server-side for the config-repository of type DB<M>
     * suitable for exposure to python.
     *
     */
    struct py_config_server : shyft::py::energy_market::py_server_with_web_api<config_server,
        shyft::web_api::ui::request_handler> {
        using super = shyft::py::energy_market::py_server_with_web_api<config_server, shyft::web_api::ui::request_handler>;
        py::object py_fx_cb;///< python callback that can be set by the py user

        /**@brief We need to handle errors when executing the user Python code
         */
        void handle_pyerror() {
            using namespace boost::python;
            using namespace boost;
            std::string msg{"unspecified error"};
            if (PyErr_Occurred()) {
                PyObject *exc, *val, *tb;
                object formatted_list, formatted;
                PyErr_Fetch(&exc, &val, &tb);
                PyErr_NormalizeException(&exc, &val, &tb);
                if (tb != nullptr) {
                    PyException_SetTraceback(val, tb);
                }
                handle<> hexc(exc), hval(allow_null(val)), htb(allow_null(tb));
                object traceback(import("traceback"));
                if (!tb) {
                    object format_exception_only{ traceback.attr("format_exception_only") };
                    formatted_list = format_exception_only(hexc, hval);
                } else {
                    object format_exception{traceback.attr("format_exception")};
                    if (format_exception) {
                        try {
                            formatted_list = format_exception(hexc, hval, htb);
                        } catch (...) {
                            msg = "unable to exctract exception info";
                        }
                    } else
                        msg = "Unable to extract exception info";
                }
                if (formatted_list) {
                    formatted = py::str("\n").join(formatted_list);
                    msg = extract<std::string>(formatted);
                }
            }
            handle_exception();
            PyErr_Clear();
            throw std::runtime_error(msg);
        }

        /** This is where we try to fire user specified callback */
        string py_do_fx(string const& layout_name, string const& fx_args) {
            string r;
            if (py_fx_cb.ptr() != Py_None) {
                scoped_gil_aquire gil;// We need to use the GIL here before trying to call Python.
                try {
                    r = boost::python::call<string>(py_fx_cb.ptr(), layout_name, fx_args);
                } catch (const boost::python::error_already_set&) {
                    handle_pyerror();
                }
            }
            return r;
        }

        py_config_server(const string& root_dir): super(root_dir) {
            // rig the c++ fx_cb to forward calls to this python layer
            impl.set_read_cb([&](string const& layout_name, string const& args)-> string {
                return this->py_do_fx(layout_name, args);
            });
        }

    };
}
namespace expose {
    namespace py = boost::python;
    using std::string;
    using namespace shyft::energy_market::ui;
    using namespace shyft::py::energy_market::ui;
    using shyft::energy_market::id_base;
    using boost::format;

    template<> string str_(layout_info const &o) {
        return (format("LayoutInfo(id=%1%,name=%2%)")%str_(o.id)%str_(o.name)).str();
    }

    void ex_layout_info() {
        using py::self;
        auto o=py::class_ < layout_info, py::bases<>, boost::noncopyable,
        std::shared_ptr<layout_info>> ("LayoutInfo",
            doc_intro("Provides layout information that can be leveraged by a renderer.")
        );
        o
        .def(py::init<int, const string &, const string &>((py::arg("self"), py::arg("id"), py::arg(
        "name"), py::arg("json")=""),
            doc_intro("Construct a LayoutInfo from id, name, and json")))
        .def_readwrite("id", &layout_info::id, "identifying number")
        .def_readwrite("name", &layout_info::name, "A descriptive name")
        .def_readwrite("json", &layout_info::json, "Json-string containing layout information")
        .def(self == self)
        .def(self != self)
        ;
        expose_str_repr(o);
        expose_vector<std::shared_ptr<layout_info>>("layoutInfoVector","A strongly typed list of LayoutInfo objects",true,false);
    }

    void expose_client() {
        using py::self;
        using cm = py_config_client;

        auto wrapper_client = shyft::py::energy_market::expose_client<cm>("LayoutClient",
            doc_intro("The client-side components for layouts"));
        wrapper_client.def("read_model_with_args", &cm::read_model_with_args,
                 (py::arg("self"), py::arg("mid"), py::arg("name"), py::arg("args"), py::arg("store_layout")=false),
                 doc_intro("Read a layout from storage.")
                 doc_intro("If it is not found, it will try to generate a new layout based")
                 doc_intro(" on provided arguments using the callback function on the server.")
                 doc_parameters()
                 doc_parameter("mid", "int", "Model ID of layout to read")
                 doc_parameter("name", "str", "Name of layout, if it needs to be generated")
                 doc_parameter("args", "str", "Json format of arguments to generate layout, if needed.")
                 doc_parameter("store_layout", "bool", "If a new layout is generated, whether to store it on server. Defaults to False.")
                 doc_returns("li", "LayoutInfo", "Read or generated LayoutInfo")
            );
    }

    void expose_server() {
        using py::self;
        auto srv_wrapper = shyft::py::energy_market::expose_server_with_web_api<py_config_server>(
            "LayoutServer",
            doc_intro("The server-side components for layouts")
            );

        srv_wrapper.def_readwrite("fx", &py_config_server::py_fx_cb,
            doc_intro("server-side callable function(lambda) that takes two parameters:")
                doc_intro("name :  the name of the layout")
                doc_intro("fx_args: arbitrary string to pass to the server-side function")
                doc_intro("The server-side fx is called when the client (or web-api) invoke the c.read_model_with_args(name,fx_args).")
                doc_intro("The signature of the callback function should be fx_cb(name:str, fx_args:str)->str")
                doc_intro("This feature is simply enabling the users to tailor server-side functionality in python!")
                doc_intro("\nExample\n--------\n")
                doc_intro(
                    "from shyft.energy_market.ui import LayoutServer\n\n"
                    "s=LayoutServer()\n"
                    "def my_fx(name:str, fx_args:str)->bool:\n"
                    "    print(f'invoked with name={name} fx_args={fx_args}')\n"
                    "  # note we can use captured Server s here!"
                    "    return True\n"
                    "# and then bind the function to the callback\n"
                    "s.fx=my_fx\n"
                    "s.start_server()\n"
                    ": # later using client from anywhere to invoke the call\n"
                    "fx_result=c.fx('my_layout_id', 'my_args')\n"
                )
        )
        ;
    }

    void ex_client_server() {
        ex_layout_info();

        expose_server();
        expose_client();
    }
}
