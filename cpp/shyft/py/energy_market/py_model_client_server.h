#pragma once

#include <mutex>
#include <string>
#include <memory>
#include <vector>
#include <csignal>
#include <shyft/py/scoped_gil.h>
#include <shyft/py/energy_market/py_object_ext.h>
#include <shyft/srv/model_info.h>
#include <shyft/srv/client.h>
#include <shyft/srv/server.h>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <shyft/web_api/bg_work_result.h>
#include <shyft/web_api/energy_market/srv/request_handler.h>

namespace shyft::py::energy_market {

    using std::mutex;
    using std::unique_lock;
    using std::string;
    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using shyft::core::utcperiod;
    using shyft::srv::model_info;
    using shyft::srv::client;
    using shyft::srv::server;
    using shyft::web_api::energy_market::srv::request_handler;
    namespace py=boost::python;
    using namespace shyft::py;

    /** @brief A  client for model type M suitable for python exposure
     *
     * This class takes care of  python gil and mutex, ensuring that any attempt using
     * multiple python threads will be serialized.
     * gil is released while the call is in progress.
     * 
     * Using this template saves us the repeating work for similar model-repositories
     *
     * @tparam Client a client type, should be shyft::srv::client<M> or derived from it.
     *
     */
    template<class Client>
    struct py_client {
        using M_t = typename Client::M_t;
        mutex mx; ///< to enforce just one thread active on this client object at a time
        Client impl;
        py_client(const std::string& host_port,int timeout_ms):impl{host_port,timeout_ms} {}
        ~py_client() { }

        py_client()=delete;
        py_client(py_client const&) = delete;
        py_client(py_client &&) = delete;
        py_client& operator=(py_client const&o) = delete;

        string get_host_port() {
            return impl.c.host_port;
        }
        int get_timeout_ms() {
            return impl.c.timeout_ms;
        }
        bool is_open() const {
            return impl.c.is_open;
        }
        size_t get_reconnect_count() const {
            return impl.c.reconnect_count;
        }

        void close(int timeout_ms=1000) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close(timeout_ms);
        }
        
        void reopen(int timeout_ms=1000) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.reopen(timeout_ms);
        }
        
        vector<model_info> get_model_infos(vector<int64_t>const & mids, utcperiod per) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.get_model_infos(mids, per);
        }
        
        int64_t store_model( shared_ptr<M_t> const& m, model_info const &mi) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.store_model(m,mi);
        }
        
        shared_ptr<M_t> read_model(int64_t mid)  {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.read_model(mid);
        }
        
        vector<shared_ptr<M_t>> read_models(vector<int64_t> mids) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.read_models(mids);
        }
        
        int64_t remove_model(int64_t mid) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.remove_model(mid);
        }

        bool update_model_info(int64_t mid, model_info const &mi) {
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            return impl.update_model_info(mid,mi);
        }
        void close_conn() {//weird, close is not a name we can use here..
            scoped_gil_release gil;
            unique_lock<mutex> lck(mx);
            impl.close();
        }
    };

    /** @brief The server side component for a model repository
     *
     * 
     * This class wraps/provides the server-side for the model-repository of type DB<M>
     * suitable for exposure to python.
     *
     * @tparam Server, the server type. Should be a specialization of shyft::srv::server<DB>,
     *   or derived from it.
     * 
     */
    template<class Server>
    struct py_server  {
        Server impl;
        py_server(const string& root_dir):impl(root_dir) {
            if (!PyEval_ThreadsInitialized()) {
                PyEval_InitThreads();// ensure threads-is enabled
            }
        }
        
        void set_listening_port(int port) {impl.set_listening_port(port);}
        int  get_listening_port() { return impl.get_listening_port();}
        
        void set_max_connections(int  n) {impl.set_max_connections(size_t(n));}
        int  get_max_connections() { return int(impl.get_max_connections());}
        
        void set_listening_ip(string const& ip) {impl.set_listening_ip(ip);}
        string  get_listening_ip() {return impl.get_listening_ip();}
        
        int start_server() {return impl.start_server(); }
        void stop_server(int timeout_ms) { 
            impl.set_graceful_close_timeout(timeout_ms);
            impl.clear(); 
        }
        
        bool is_running() { return impl.is_running(); }
        
        ~py_server() { }
        //-- get rid of stuff that would not work
         py_server()=delete;
         py_server(py_server const&)=delete;
         py_server(py_server&&)=delete;
         py_server& operator=(py_server const &)=delete;
         py_server& operator=(py_server&&)=delete;
    };

    template<class Server, class RequestHandler = request_handler<Server>>
    struct py_server_with_web_api : public py_server<Server> {
        using super = py_server<Server>;
        using DB_t = typename Server::DB_t;
        RequestHandler bg_server;
        std::future<int> web_srv;

        py_server_with_web_api(const string& root_dir): super(root_dir) {
            bg_server.srv = &(this->impl);
        }

        void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads, bool tls_only) {
            scoped_gil_release gil;
            if (!web_srv.valid()) {
                web_srv = std::async(std::launch::async,
                    [this, host_ip, port, doc_root, fg_threads, bg_threads,tls_only]()->int {
                        return shyft::web_api::run_web_server(
                            bg_server,
                            host_ip,
                            static_cast<unsigned short>(port),
                            make_shared<string>(doc_root),
                            fg_threads,
                            bg_threads,
                            tls_only
                        );
                    });
            }
        }

        void stop_web_api() {
            scoped_gil_release gil;
            if (web_srv.valid()) {
                std::raise(SIGINT);
                (void) web_srv.get();
            }
        }

        bool auth_needed() const {
            return bg_server.auth.needed();
        }
        vector<string> auth_tokens() const {
            return bg_server.auth.tokens();
        }
        void add_auth_tokens(vector<string> const&tokens) {
            bg_server.auth.add(tokens);
        }
        void remove_auth_tokens(vector<string> const&tokens) {
            bg_server.auth.remove(tokens);
        }

    };

    /** @brief Expose to python the client side api for a model-repository of type M
     *
     * @tparam PyClient the client type. Should be an instantiation of shyft::py::energy_market::py_client<Client>,
     *  or derived from it.
     *
     *  @return an instance of boost::python::class_ that exposes PyClient to Python with interface for base class
     *      functionality.
     */
    template <class PyClient>
    py::class_<PyClient, boost::noncopyable> expose_client(const char* name,const char *doc_str) {
        using cm=PyClient;

        py::class_<cm,boost::noncopyable> py_wrapper(name,doc_str,py::no_init);
        py_wrapper
            .def(py::init<string const&,int>(
                (py::arg("self"),py::arg("host_port"),py::arg("timeout_ms")),
                "TODO"
            )
            )
            .def_readonly("host_port", &cm::get_host_port, "Endpoint network address of the remote server.")
            .def_readonly("timeout_ms", &cm::get_timeout_ms, "Timout for remote server operations, in number milliseconds.")
            .def_readonly("is_open", &cm::is_open, "If the connection to the remote server is (still) open.")
            .def_readonly("reconnect_count", &cm::get_reconnect_count, "Number of reconnects to the remote server that have been performed.")
            .def("close", &cm::close_conn, (py::arg("self")),
                doc_intro("Close the connection. It will automatically reopen if needed.")
            )
            .def("get_model_infos",&cm::get_model_infos,
                 (py::arg("self"),py::arg("mids"), py::arg("created_in")=utcperiod()),
                doc_intro("returns all or selected model-info objects based on model-identifiers(mids)")
                doc_parameters()
                doc_parameter("mids","IntVector","empty = all, or a list of known exisiting model-identifiers")
                doc_parameter("created_in", "UtcPeriod", "For which period you are interested in model-infos.")
                doc_returns("model_infos","ModelInfoVector","Strongly typed list of ModelInfo")
            )
            .def("store_model",&cm::store_model,
                 (py::arg("self"),py::arg("m"),py::arg("mi")),
                doc_intro("Store the model to backend, if m.id==0 then a new unique model-info is created and used")
                doc_parameters()
                doc_parameter("m","Model","The model to store")
                doc_parameter("mi","ModelInfo","The model-info to store for the model")
                doc_returns("mid","int","model-identifier for the stored model and model-info")
            )
            .def("read_model",&cm::read_model,
                 (py::arg("self"),py::arg("mid")),
                doc_intro("Read and return the model for specified model-identifier (mid)")
                doc_parameters()
                doc_parameter("mid","int","the model-identifer for the wanted model")
                doc_returns("m","Model","The resulting model from the server")
            )
            .def("read_models",&cm::read_models,
                 (py::arg("self"),py::arg("mids")),
                doc_intro("Read and return the model for specified model-identifier (mid)")
                doc_parameters()
                doc_parameter("mids","Int64Vector","A strongly typed list of ints, the model-identifers for the wanted models")
                doc_returns("m","Model","The resulting model from the server")
            )
            .def("remove_model",&cm::remove_model,
                 (py::arg("self"),py::arg("mid")),
                doc_intro("Remove the specified model bymodel-identifier (mid)")
                doc_parameters()
                doc_parameter("mid","int","the model-identifer for the wanted model")
                doc_returns("ec","int","0 or error-code?")
            )
            .def("update_model_info",&cm::update_model_info,
                 (py::arg("self"),py::arg("mid"),py::arg("mi")),
                doc_intro("Update the model-info for specified model-identifier(mid)")
                doc_parameters()
                doc_parameter("mid","int","model-identifer")
                doc_parameter("mi","ModelInfo","The new updated model-info")
                doc_returns("ok","bool","true if success")
            )
        ;
        return py_wrapper;
    }
    
    /** @brief a function to expose a server using a DB for model DB::model_t
     * 
     * 
     * This class simply expose the methods of the above py_server that takes
     * a backend storage service 'DB', for  model type M=DB::model_t
     * @tparam DB template for backend-store, same requirements as for py_server<M>
     * @tparam PyServer python server wrapper type. Should be specialization of py_server<Server>,
     *  or derived from it.
     */
    template<class PyServer>
    py::class_<PyServer, boost::noncopyable> expose_server(const char *name, const char *doc_str) {
        using srv = PyServer;
            py::class_<srv, boost::noncopyable > srv_wrapper(name,doc_str,
                py::init<const string&>((py::arg("self"),py::arg("root_dir")),
                        doc_intro("Creates a server object that serves models from root_dir.")
                        doc_intro("The root_dir will be create if it does not exsists.")
                        doc_parameters()
                        doc_parameter("root_dir","str","Path to the root-directory that keeps/will keep the model-files")
                )
            );
            srv_wrapper.def("set_listening_port", &srv::set_listening_port, (py::arg("self"),py::arg("port_no")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("port_no","int","a valid and available tcp-ip port number to listen on.")
                doc_paramcont("typically it could be 20000 (avoid using official reserved numbers)")
                doc_returns("nothing","None","")
            )
            .def("set_listening_ip", &srv::set_listening_ip, (py::arg("self"),py::arg("ip")),
                doc_intro("set the listening port for the service")
                doc_parameters()
                doc_parameter("ip","str","ip or host-name to start listening on")
                doc_returns("nothing","None","")
            )
            .def("start_server",&srv::start_server,(py::arg("self")),
                doc_intro("start server listening in background, and processing messages")
                doc_see_also("set_listening_port(port_no),is_running")
                doc_returns("port_no","in","the port used for listening operations, either the value as by set_listening_port, or if it was unspecified, a new available port")
            )
            .def("set_max_connections",&srv::set_max_connections,(py::arg("self"),py::arg("max_connect")),
                doc_intro("limits simultaneous connections to the server (it's multithreaded, and uses on thread pr. connect)")
                doc_parameters()
                doc_parameter("max_connect","int","maximum number of connections before denying more connections")
                doc_see_also("get_max_connections()")
            )
            .def("get_max_connections",&srv::get_max_connections, (py::arg("self")),
                doc_intro("returns the maximum number of connections to be served concurrently"))
            .def("stop_server",&srv::stop_server, (py::arg("self"),py::arg("timeout")=1000),
                doc_intro("stop serving connections, gracefully.")
                doc_see_also("start_server()")
            )
            .def("is_running",&srv::is_running, (py::arg("self")),
                doc_intro("true if server is listening and running")
                doc_see_also("start_server()")
            )
            .def("get_listening_port",&srv::get_listening_port, (py::arg("self")),
                "returns the port number it's listening at for serving incoming request"
            )
            ;
        return srv_wrapper;
    }

    template <class PyServer>
    py::class_<PyServer, boost::noncopyable> expose_server_with_web_api(const char *name, const char *doc_str) {
        using srv = PyServer;
        // The base needs to be exposed as well:
        auto srv_wrapper = expose_server<PyServer>(name, doc_str);

        srv_wrapper.def("start_web_api", &srv::start_web_api,
            (py::arg("self"), py::arg("host_ip"), py::arg("port"), py::arg("doc_root"), py::arg("fg_threads")=2, py::arg("bg_threads")=4,py::arg("tls_only")=false),
            doc_intro("Start a web API for communicating with server")
            doc_parameters()
            doc_parameter("host_ip", "str", "0.0.0.0 for any interface, 127.0.0.1 for local only, &c.")
            doc_parameter("port", "int", "port number to serve the web API on. Ensure it's available")
            doc_parameter("doc_root", "str", "directory form which we will serve http/https documents.")
            doc_parameter("fg_threads", "int", "number of web API foreground threads, typically 1-4 depending on load.")
            doc_parameter("bg_threads", "int", "number of long running background thread workers to serve requests &c.")
            doc_parameter("tls_only","bool","default false, set to true to enforce tls sessions only.")

        )
        .def("stop_web_api", &srv::stop_web_api, (py::arg("self")),
            doc_intro("Stops any ongoing web API service.")
        )
        .add_property("auth_needed",&srv::auth_needed,
            doc_intro("returns true if the server is setup with auth-tokens, requires web-api clients to pass a valid token")
        )
        .def("auth_tokens",&srv::auth_tokens,(py::arg("self")),
            doc_intro("returns the registered authentication tokens.")
        )
        .def("add_auth_tokens",&srv::add_auth_tokens,(py::arg("self"),py::arg("tokens")),
            doc_intro("Adds auth tokens, and activate authentication.")
            doc_intro("The tokens is compared exactly to the autorization token passed in the request.")
            doc_intro("Authorization should onlye be used for the https/wss, unless other measures(vpn/ssh tunnels etc.) are used to protect auth tokens on the wire")
            doc_parameters()
            doc_parameter("tokens","","list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")
        )
        .def("remove_auth_tokens",&srv::remove_auth_tokens,(py::arg("self"),py::arg("tokens")),
            doc_intro("removes auth tokens, if it matches all available tokens, then deactivate auth requirement for clients")
            doc_parameters()
            doc_parameter("tokens","","list of tokens, where each token is like `Basic dXNlcjpwd2Q=`, e.g: base64 user:pwd")
        )
        ;
        return srv_wrapper;
    }

}
