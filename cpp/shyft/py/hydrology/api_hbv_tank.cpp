/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/methods/hbv_tank.h>

namespace expose {
    void hbv_tank() {
        using namespace shyft::core::hbv_tank;
        namespace py=boost::python;

        py::class_<parameter>("HbvTankParameter")
            .def(py::init<py::optional<double, double, double, double, double>>((py::arg("uz1"),py::arg("kuz2"),py::arg("kuz1"),py::arg("perc"),py::arg("klz")), "create parameter object with specifed values"))
            .def_readwrite("uz1", &parameter::uz1, "mm, .. , default=25")
            .def_readwrite("kuz2", &parameter::kuz2, ",default=0.5")
            .def_readwrite("kuz1", &parameter::kuz1, ",default=0.3")
            .def_readwrite("perc", &parameter::perc, ",default=0.8")
            .def_readwrite("klz", &parameter::klz, ",default=0.02")
            ;

        py::class_<state>("HbvTankState")
            .def(py::init<py::optional<double,double>>((py::arg("uz"),py::arg("lz")), "create a state with specified values"))
            .def_readwrite("uz", &state::uz, "Water Level Upper Zone [mm]")
            .def_readwrite("lz", &state::lz, "Water Level Lower Zone [mm]")
            ;

        py::class_<response>("HbvTankResponse")
            .def_readwrite("outflow", &response::outflow, "from Tank-routine in [mm]")
            ;

        typedef  calculator<parameter> HbvTankCalculator;
        py::class_<HbvTankCalculator>("HbvTankCalculator",
            "tobe done.. \n"
            "\n"
            "\n", py::no_init
            )
            .def(py::init<const parameter&>((py::arg("parameter")), "creates a calculator with given parameter"))
            .def("step", &HbvTankCalculator::step<response, state>, (py::arg("self"),py::arg("state"),py::arg("response"),py::arg("t0"),py::arg("t1"),py::arg("soil_outflow")),
                "steps the model forward from t0 to t1, updating state and response")
            ;
    }
}
