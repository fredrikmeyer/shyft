/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#include <shyft/py/api/boostpython_pch.h>

#include <shyft/hydrology/geo_point.h>
#include <shyft/hydrology/geo_cell_data.h>
namespace expose {
    using namespace shyft::core;
    namespace py=boost::python;

    void api_geo_cell_data() {
        py::class_<land_type_fractions>("LandTypeFractions",
            doc_intro(
            "LandTypeFractions are used to describe type of land,\n"
            "like glacier, lake, reservoir and forest.\n"
            "It is designed as a part of GeoCellData"
            )
        )
        .def(py::init<double,double,double,double,double>(
            (py::arg("glacier"),py::arg("lake"),py::arg("reservoir"),py::arg("forest"),py::arg("unspecified")),
            "construct LandTypeFraction specifying the area of each type"
        ))
        .def("glacier",&land_type_fractions::glacier,"returns the glacier part")
        .def("lake",&land_type_fractions::lake,"returns the lake part")
        .def("reservoir",&land_type_fractions::reservoir,"returns the reservoir part")
        .def("forest",&land_type_fractions::forest,"returns the forest part")
        .def("unspecified",&land_type_fractions::unspecified,"returns the unspecified part")
        .def("set_fractions",&land_type_fractions::set_fractions,(py::arg("self"),py::arg("glacier"),py::arg("lake"),py::arg("reservoir"),py::arg("forest")),"set the fractions explicit, each a value in range 0..1, sum should be 1.0")
        .def("snow_storage",&land_type_fractions::snow_storage,(py::arg("self")),"returns the area where snow can build up, 1.0-lake-reservoir")
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;

        py::class_<geo_cell_data>("GeoCellData",
                doc_intro(
                "Represents common constant geo_cell properties across several possible models and cell assemblies.\n"
                "The idea is that most of our algorithms uses one or more of these properties,\n"
                "so we provide a common aspect that keeps this together.\n"
                "Currently it keep these items:\n"
                "- mid-point geo_point, (x,y,z) (default 0)\n"
                "- TIN data \n"
                "- the area in m^2, (default 1000 x 1000 m^2)\n"
                "- land_type_fractions (unspecified=1)\n"
                "- catchment_id   def (-1)\n"
                "- radiation_slope_factor def 0.9\n"
                "- routing_info def(0,0.0), i.e. not routed and hydrological distance=0.0m"
                )
        )
        .def(py::init<geo_point,double,int64_t,py::optional<double,const land_type_fractions&,routing_info>>(
            (py::arg("self"),py::arg("mid_point"),py::arg("area"),py::arg("catchment_id"),py::arg("radiation_slope_factor"),py::arg("land_type_fractions"),py::arg("routing_info")),
                doc_intro("Constructs a GeoCellData with all parameters specified")
                doc_parameters()
                doc_parameter("mid_point","GeoPoint","specifies the x,y,z mid-point of the cell-area")
                doc_parameter("area","float","area in unit [m^2]")
                doc_parameter("catchment_id","int","catchment-id that this cell is a part of")
                doc_parameter("radiation_slope_factor","float","aspect dependent factor used to calculate the effective radiation for this cell,range 0.0..1.0")
                doc_parameter("land_type_fractions","LandTypeFractions","specifies the fractions of glacier, forrest, lake and reservoirs for this cell")
                doc_parameter("routing_info","RoutingInfo","Specifies the destination routing network-node and velocity for this cell")
            )
        )
        .def(py::init<geo_point,geo_point,geo_point,int64_t,int64_t,py::optional<const land_type_fractions&,routing_info>>(
            (py::arg("self"),py::arg("p1"),py::arg("p2"),py::arg("p3"),py::arg("epsg_id"),py::arg("catchment_id"),py::arg("land_type_fractions"),py::arg("routing_info")),
                doc_intro("Constructs a TIN-based GeoCellData with all parameters specified, slope/aspect etc. is based on TIN")
                doc_parameters()
                doc_parameter("p1","GeoPoint","specifies the first vertex in TIN")
                doc_parameter("p2","GeoPoint","specifies the second vertex in TIN")
                doc_parameter("p3","GeoPoint","specifies the third vertex in TIN")
                doc_parameter("epsg_id","int","specifies the geo projection as epsg-id, used for projection from cartesian coord to long-lat  algorithms that needs this(e.g. radiation, TIN-models)")
                doc_parameter("catchment_id","int","catchment-id that this cell is a part of")
                doc_parameter("land_type_fractions","LandTypeFractions","specifies the fractions of glacier, forrest, lake and reservoirs for this cell")
                doc_parameter("routing_info","RoutingInfo","Specifies the destination routing network-node and velocity for this cell")
            )
        )
        
        .def("mid_point",&geo_cell_data::mid_point,(py::arg("self")),"returns the mid_point",py::return_value_policy<py::copy_const_reference>())
        .def("catchment_id",&geo_cell_data::catchment_id,(py::arg("self")),"returns the current catchment_id")
        .def("set_catchment_id",&geo_cell_data::set_catchment_id,(py::arg("self"),py::arg("catchment_id")),
             doc_intro("Set the catchment_id to specified value")
             doc_intro("Note: Only use this method *before* a region-model is created")
             doc_parameters()
             doc_parameter("catchment_id","int","Catchment-id for this cell")
            )
        .def("radiation_slope_factor",&geo_cell_data::radiation_slope_factor,(py::arg("self")),"radiation slope factor")
        .def("land_type_fractions_info",&geo_cell_data::land_type_fractions_info,(py::arg("self")),"land_type_fractions",py::return_value_policy<py::copy_const_reference>())
        .def("set_land_type_fractions",&geo_cell_data::set_land_type_fractions,(py::arg("self"),py::arg("ltf")),"set new LandTypeFractions")
        .def_readwrite("routing_info",&geo_cell_data::routing,"the routing information for the cell keep destination id and hydrological distance to destination")
        .def_readwrite("epsg_id",&geo_cell_data::epsg_id,"the epsg-id that should consistently identify the geo projection for all cells in a region model")
        .def("surface_area",&geo_cell_data::rarea,(py::arg("self")),"returns the real surface area in m^2")
	    .def("area",&geo_cell_data::area,(py::arg("self")),"returns the effective area, as projected to the horisontal plane, in m^2")
        .def("set_tin_data",&geo_cell_data::set_tin_data,(py::arg("self"),py::arg("vertexes")),"set TIN data from vector of vertexes")
        .def("slope",&geo_cell_data::slope,(py::arg("self")),"returns slope, deg")
        .def("aspect",&geo_cell_data::aspect,(py::arg("self")),"returns aspect, deg")
        .def("vertexes",&geo_cell_data::vertexes,(py::arg("self")),"returns vector of tin vertexes")
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
    }
}
