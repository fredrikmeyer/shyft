/** This file is part of Shyft. Copyright 2015-2020 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once
#include <string>
#include <vector>
#include <map>
#include <type_traits>
#include <shyft/py/api/boostpython_pch.h>
#include <shyft/py/api/py_convertible.h>
#include <shyft/py/api/expose_str.h>

namespace expose {

    namespace py=boost::python;

    namespace detail {

        /* Check if a converter has been registered for a given C++ type in boost python's
         * global converter registry.
         * 
         * Checks to_python converter. There can only be one of these, any attempts to
         * register additional converters for same type will be rejected in runtime with a
         * warning ("to-Python converter for <type> already registered; second conversion
         * method ignored."). There can be multiple from_python converters, all tied to the
         * same C++ type registration as the to_python converter, but these will not be
         * considered here.
         */
        template<class T>
        static bool has_converter() {
            auto reg = py::converter::registry::query(py::type_id<T>());
            return reg && reg->m_to_python;
        }

        /* Basic expose of container, e.g. vector<T>, and converters for the C++ type.
         *
         * Registers global Python/C++ converters: The default to_python converter, created
         * automatically by the boost::python::class_ type creation, but also an optional
         * custom from_python iterable converter, making it possible to create the C++ type
         * from python iterable types.
         * 
         * The converter registry is global, shared by all extension modules, and the
         * converters are tied to the C++ type, which means they will be applicable for any
         * exposed functions that accept this C++ type as an argument. This also means that
         * only a single set of converters should be created for each C++ type, so use
         * has_converter to check and then create_class instead if already
         * registered.
         * 
         * Returns the boost python class_ type for further extension.
         * 
         * NOTE: The exposed type must be cloneable (see expose_clone) for the from_python
         * iterable converter to work (runtime error in Python if not).
         */
        template<class ContainerType>
        static py::class_<ContainerType>
        create_class_and_converters(const char* name, const char* doc = nullptr, bool from_python_iterable = true) {
            if (from_python_iterable)
                py_api::iterable_converter().from_python<ContainerType>();
            return py::class_<ContainerType>(name, doc);
        }

        /* Basic expose of container, e.g. vector<T>.
         *
         * Suppresses registration of the default to_python converter by adding
         * boost::noncopyable argument on boost::python::class_ type creation, and skips
         * the explicit registration of the custom from_python iterable converter.
         * 
         * If a container of same type has been exposed with converters, these will apply to
         * all exposed types based on same C++ container type - but all types must be cloneable
         * (see expose_clone) for the from_python converter to work (runtime error in
         * Python if not).
         */
        template<class ContainerType>
        static py::class_<ContainerType, boost::noncopyable>
        create_class(const char* name, const char* doc = nullptr) {
            return py::class_<ContainerType, boost::noncopyable>(name, doc);
        }


        /* Helper function to define a clone method, a __init__ with an instance of same
         * type as argument, on exposed container type.
         * 
         * NOTE: This is required to be able to create it from an iterable with the
         * from_python converter (see parameter from_python_iterable in
         * create_class_and_converters).
         */
        template <class PyCls>
        static PyCls& expose_clone(PyCls& c) {
            return c.def(py::init<const typename PyCls::wrapped_type&>((py::arg("clone")), "Create a clone."));
        }

        /* Helper function to define indexing capabilities to an exposed vector, which
         * makes it appear as python list.
         * 
         * This exposes a set of methods: __len__, __getitem__, __setitem__, __delitem__,
         * __iter__, __contains__, append and extend.
         *
         * By default indexed elements have Python reference semantics and are returned by
         * proxy, this can be disabled by setting parameter proxy to false (e.g. if type
         * already have reference semantics, such as shared_ptr).
         *
         * NOTE: The wrapped vector's value type must have operator== implemented (compile
         * error if not).
         */
        template <class PyCls>
        static PyCls& expose_vector_indexing(PyCls& c, bool proxy = true) {
            if (proxy)
                return c.def(py::vector_indexing_suite<typename PyCls::wrapped_type, false>());
            return c.def(py::vector_indexing_suite<typename PyCls::wrapped_type, true>());
        }

        /* Helper function to define indexing capabilities to an exposed map, which
         * makes it appear as python dict.
         * 
         * This exposes a set of methods: __len__, __getitem__, __setitem__, __delitem__,
         * __iter__, __contains__, append and extend.
         *
         * By default indexed elements have Python reference semantics and are returned by
         * proxy, this can be disabled by setting parameter proxy to false (e.g. if type
         * already have reference semantics, such as shared_ptr).
         *
         * NOTE: The wrapped map's value type must have operator== implemented (compile
         * error if not).
         *
         * NOTE: Boost will automatically register to_python converter for the value type,
         * i.e. pair<KeyType,MappedType>, which means the notorious warning
         * "already registered; second conversion method ignored." will still appear if
         * exposing multiple maps of same type with indexing, regardless of the
         * has_converter check implemented in this file!
         */
        template <class PyCls>
        static PyCls& expose_map_indexing(PyCls& c, bool proxy = true) {
            if (proxy)
                return c.def(py::map_indexing_suite<typename PyCls::wrapped_type, false>());
            return c.def(py::map_indexing_suite<typename PyCls::wrapped_type, true>());
        }

        /* Helper function to define __eq__ and __ne__ on exposed container type.
         *
         * NOTE: The wrapped container's value type must have operator== implemented
         * (compile error if not).
         */
        template <class PyCls>
        static PyCls& expose_eq_ne(PyCls& c) {
            return c.def(py::self==py::self)
                    .def(py::self!=py::self);
        }
        template <class PyCls, class Eq>
        static PyCls& expose_eq_ne2(PyCls&c, Eq &&eq) {
            //using wt_t=typename PyCls::wrapped_type;
            return c.def("__eq__",eq);
        }

        /* Helper function for adding a "complete" set of utility methods on an already
         * exposed container type, common for the two different Python classes wrapping
         * the same container with or without to_python converter.
         */
       template <class PyCls>
       static PyCls& expose_container_methods(PyCls& c, bool cloneable, bool comparable) {
            if (cloneable)
                expose_clone(c); // Expose __init__ for cloning. Required for it to be created from iterable by a from_python converter registered for the C++ type (see parameter from_python_iterable in create_class_and_converters).
            expose_str_repr(c); // Expose __str__ and __repr__. Requires str_ function specialized for the container's value_type (see expose_str.h).
            if(comparable)
                expose_eq_ne(c); // Expose __eq__ and __ne__.
            return c;
        }

        template <class PyCls,class Eq>
        static PyCls& expose_container_methods_eq(PyCls& c, bool cloneable, Eq &&eq) {
            if (cloneable)
                expose_clone(c); // Expose __init__ for cloning. Required for it to be created from iterable by a from_python converter registered for the C++ type (see parameter from_python_iterable in create_class_and_converters).
            expose_str_repr(c); // Expose __str__ and __repr__. Requires str_ function specialized for the container's value_type (see expose_str.h).
            expose_eq_ne2(c,std::forward<Eq>(eq)); // Expose __eq__ and __ne__.
            return c;
        }
    }

    /** oneliner to expose vector<T>
     * 
     * Automatically handles registration of converters for the C++ type, in such a way
     * that the same type can be exposed multiple times (e.g. vector<string> can be
     * exposed from multiple extension modules).
     * 
     * Includes optional, but default, enabling of indexing capabilities, which makes
     * the exposed vector appear as a list in Python. This requires the C++ type to
     * implement operator==, which for vector also includes the vector's value_type.
     * The parameter comparable indicates if the C++ type does this, and if true
     * (the default) it will expose indexing methods as well as standard comparison
     * methods (__eq__ and __ne__).
     * 
     * @tparam ValueType the value type of the C++ vector to be exposed
     * @param name the name of the Python wrapper class
     * @param doc the optional docstring for the Python wrapper class
     * @param comparable if the vector, thus also the value type, implements operator==
     * @param proxy returned indexed elements by proxy to enable Python reference semantics (disable if type already have reference semantics, such as shared_ptr)
     */
    template<class ValueType>
    static void expose_vector(const char* name, const char* doc = nullptr, bool comparable = true, bool proxy = true) {
        using ContainerType = std::vector<ValueType>;
        constexpr bool cloneable = true; // Required if from_python_iterable=true.
        constexpr bool from_python_iterable = true; // The iterable_converter works as a from_python converter on vectors. Requires cloneable=true.
        if (detail::has_converter<ContainerType>()) {
            // Expose without converters since it already exists for the C++ type.
            //std::cout << "expose_vector(\"" << name << "\"): Without converters for C++ type " << typeid(ValueType).name() << " (to_python found in registry)\n";
            auto c = detail::create_class<ContainerType>(name, doc);
            detail::expose_container_methods(c, cloneable, comparable);
            detail::expose_vector_indexing(c, proxy); // Expose full set of methods that will make it appear as a regular python list.
        } else {
            // Expose and create converters for the C++ type.
            //std::cout << "expose_vector(\"" << name << "\"): With converters for C++ type " << typeid(ValueType).name() << "\n";
            auto c = detail::create_class_and_converters<ContainerType>(name, doc, from_python_iterable);
            detail::expose_container_methods(c, cloneable, comparable);
            detail::expose_vector_indexing(c, proxy); // Expose full set of methods that will make it appear as a regular python list.
        }
    }

    template<class ValueType,class Eq>
    static void expose_vector_eq(const char* name, const char* doc , Eq&& eq, bool proxy = true) {
        using ContainerType = std::vector<ValueType>;
        constexpr bool cloneable = true; // Required if from_python_iterable=true.
        constexpr bool from_python_iterable = true; // The iterable_converter works as a from_python converter on vectors. Requires cloneable=true.
        if (detail::has_converter<ContainerType>()) {
            // Expose without converters since it already exists for the C++ type.
            //std::cout << "expose_vector(\"" << name << "\"): Without converters for C++ type " << typeid(ValueType).name() << " (to_python found in registry)\n";
            auto c = detail::create_class<ContainerType>(name, doc);
            detail::expose_container_methods_eq(c, cloneable, std::forward<Eq>(eq));
            detail::expose_vector_indexing(c, proxy); // Expose full set of methods that will make it appear as a regular python list.
        } else {
            // Expose and create converters for the C++ type.
            //std::cout << "expose_vector(\"" << name << "\"): With converters for C++ type " << typeid(ValueType).name() << "\n";
            auto c = detail::create_class_and_converters<ContainerType>(name, doc, from_python_iterable);
            detail::expose_container_methods_eq(c, cloneable, std::forward<Eq>(eq));
            detail::expose_vector_indexing(c, proxy); // Expose full set of methods that will make it appear as a regular python list.
        }
    }

    /** oneliner to expose map<KeyType,MappedType>
     *
     * Se documentation of expose_vector for details. Exposing maps and vectors are
     * similar, except (currently) the 
     * 
     * @tparam ValueType the value type of the C++ vector to be exposed
     * @param name the name of the Python wrapper class
     * @param doc the optional docstring for the Python wrapper class
     * @param comparable if the vector, thus also the value type, implements operator==
     * @param proxy returned indexed elements by proxy to enable Python reference semantics (disable if type already have reference semantics, such as shared_ptr)
     */
    template<class KeyType, class MappedType>
    static void expose_map(const char* name, const char* doc = nullptr, bool comparable = true, bool proxy = true) {
        using ContainerType = std::map<KeyType, MappedType>;
        constexpr bool cloneable = false; // Not required, since from_python_iterable=false.
        constexpr bool from_python_iterable = false; // The iterable_converter currently does not handle map types: "No registered converter was able to produce a C++ rvalue of type std::pair<T1,T2> from this Python object of type T" (T1, T2, T replaced by actual types).
        if (detail::has_converter<ContainerType>()) {
            // Expose without converters since it already exists for the C++ type.
            //std::cout << "expose_map(\"" << name << "\"): Without converters for C++ type " << typeid(KeyType).name() << ", " << typeid(MappedType).name() << " (to_python found in registry)\n";
            auto c = detail::create_class<ContainerType>(name, doc);
            detail::expose_container_methods(c, cloneable, comparable);
            detail::expose_map_indexing(c, proxy); // Expose full set of methods that will make it appear as a regular python dict.
        } else {
            // Expose and create converters for the C++ type.
            //std::cout << "expose_map(\"" << name << "\"): With converters for C++ type " << typeid(KeyType).name() << ", " << typeid(MappedType).name() << "\n";
            auto c = detail::create_class_and_converters<ContainerType>(name, doc, from_python_iterable);
            detail::expose_container_methods(c, cloneable, comparable);
            detail::expose_map_indexing(c, proxy); // Expose full set of methods that will make it appear as a regular python dict.
        }
    }

}
