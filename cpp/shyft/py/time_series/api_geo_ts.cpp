#include <shyft/py/api/boostpython_pch.h>
#include <vector>
#include <string>
#include <stdexcept>
#include <shyft/time/utctime_utilities.h>
#include <shyft/dtss/geo.h>

#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>
#include <shyft/py/api/numpy_boost_python.hpp>


namespace expose {
    namespace py = boost::python;
    using shyft::core::utcperiod;
    using shyft::core::utctimespan;
    using shyft::core::utctime;
    using shyft::time_series::ts_point_fx;
    using shyft::core::geo_point;
    using shyft::time_series::dd::gta_t;
    using shyft::time_series::dd::apoint_ts;
       /** extension class do deal with vector constructs etc. */
    using geo_tsv=shyft::time_series::dd::geo_ts_vector;
    using std::runtime_error;
    using std::to_string;
    using std::vector;
    using std::string;

    using shyft::core::geo_point;
    using shyft::time_series::dd::apoint_ts;
    using std::vector;

    struct geo_tsv_ext {
        static  geo_tsv* create_default() {return new geo_tsv{}; }
        static geo_tsv* create_from_list(py::list tsl) {
            size_t n = py::len(tsl);
            if(n==0) return new geo_tsv{};
            auto r= new geo_tsv{};
            r->reserve(n);
            for(size_t i=0;i<n;++i) {
                py::object ts= tsl[i];
                py::extract<geo_tsv::value_type> xts(ts);
                if(xts.check()) {
                    r->push_back(xts());
                } else {
                    throw runtime_error("Failed to convert "+ to_string(i)+" element to GeoTimeSeries");
                }
            }
            return r;
        }
        
        static geo_tsv* create_from_geo_tsv_from_np(const gta_t& ta,const vector<geo_point>&gpv ,const numpy_boost<double,2>& a ,ts_point_fx point_fx) {
            size_t n_ts = a.shape()[0];
            size_t n_pts = a.shape()[1];
            if(ta.size() != n_pts)
                throw runtime_error("time-axis should have same length as second dim in numpy array");
            if(n_ts != gpv.size())
                throw runtime_error("geo-point vector should have same size as first dim (n_ts) in numpy array");
            geo_tsv *r=new geo_tsv{};
            r->reserve(n_ts);
            for(size_t i=0;i<n_ts;++i) {
                std::vector<double> v;v.reserve(n_pts);
                for(size_t j=0;j<n_pts;++j) v.emplace_back(a[i][j]);
                r->emplace_back(gpv[i], apoint_ts(ta,v ,point_fx));
            }
            return r;
        }
        
        static vector<double> values_at_time(geo_tsv const &gtsv, utctime t) {
        std::vector<double> r;
        if (gtsv.size()) {
            r.reserve(gtsv.size());
            for (auto const &gts : gtsv)
                r.push_back(gts.ts(t));
        }
        return r;
    }
    };
    
    static void geo_ts() {
        using shyft::time_series::dd::geo_ts;
        py::class_<geo_ts>("GeoTimeSeries",
            doc_intro("A minimal geo-located time-series, a time-series plus a representative 3d mid_point")
        )
        .def(py::init<geo_point,apoint_ts>(
            (py::arg("mid_point"),py::arg("ts")),
            doc_intro("Construct a GeoTimeSeries")
            doc_parameters()
            doc_parameter("mid_point","GeoPoint","The 3d location representative for ts")
            doc_parameter("ts","TimeSeries","Any kind of TimeSeries")
            )
        )
        .def_readwrite("ts",&geo_ts::ts)
        .def_readwrite("mid_point",&geo_ts::mid_point)
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
        // consider holder object shared_ptr to geo_tsv 
        py::class_<geo_tsv>("GeoTimeSeriesVector",py::no_init)
        .def(py::vector_indexing_suite<geo_tsv>())
        .def("__init__",py::make_constructor(&geo_tsv_ext::create_default,py::default_call_policies()),
                doc_intro("Create an empty TsVector")
        )
        
        .def("__init__",make_constructor(&geo_tsv_ext::create_from_list,py::default_call_policies(),(py::arg("geo_ts_list"))),
                doc_intro("Create a GeoTimeSeriesVector from a python list of GeoTimeSeries")
                doc_parameters()
                doc_parameter("geo_ts_list","List[GeoTimeSeries]","A list of GeoTimeSeries")
        )
        .def("__init__",make_constructor(&geo_tsv_ext::create_from_geo_tsv_from_np,py::default_call_policies(),
                (py::arg("time_axis"),py::arg("geo_points"),py::arg("np_array"),py::arg("point_fx"))),
                doc_intro("Create a GeoTimeSeriesVector from time-axis,geo-points,2d-numpy-array and point-interpretation")
                doc_parameters()
                doc_parameter("time_axis","TimeAxis","time-axis that matches in length to 2nd dim of np_array")
                doc_parameter("geo_points","GeoPointVector","the geo-positions for the time-series, should be of length n_ts")
                doc_parameter("np_array","np.ndarray","numpy array of dtype=np.float64, and shape(n_ts,n_points)")
                doc_parameter("point_fx","point interpretation", "one of POINT_AVERAGE_VALUE|POINT_INSTANT_VALUE")
                doc_returns("GeoTimeSeriesVector","GeoTimeSeriesVector","a GeoTimeSeriesVector of length first np_array dim, n_ts, each with geo-point and time-series with time-axis, values and point_fx")
        )
        .def("values_at_time",&geo_tsv_ext::values_at_time,(py::arg("self"),py::arg("t")),
            doc_intro("The values at specified time as a DoubleVector, that you can use .to_numpy() to get np array from")
            doc_intro("This function can be suitable if you are doing area-animated (birds-view) presentations")
            doc_parameters()
            doc_parameter("t","time","the time that should be used for getting each value")
            doc_returns("values","DoubleVector","The evaluated geo.ts(t) for all items in the vector")
        )
        ;
        numpy_boost_python_register_type<double, 2>(); // should only be done once.
        //TODO: convertible functions from GeoTimeSeriesVector -> TemperatureSourceVector etc..
    }
    
    static void geo_query() {
        using geo_query=shyft::dtss::geo::query;
        py::class_<geo_query>("GeoQuery",
            doc_intro("A query as a polygon with specified geo epsg coordinate system")
        )
        .def(py::init<int64_t,vector<geo_point>>(
            (py::arg("epsg"),py::arg("points")),
            doc_intro("Construct a GeoQuery from specified parameterrs")
            doc_parameters()
            doc_parameter("epsg","int","A valid epsg for the polygon, and also wanted coordinate system")
            doc_parameter("points","GeoPointVector","3 or more points forming a polygon that is the spatial scope")
            )
        )
        .def_readonly("epsg",&geo_query::epsg, "the epsg coordinate system")
        .def_readonly("polygon",&geo_query::polygon,"the polygon giving the spatial scope")
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
    }
    
    static void geo_grid_spec() {
        using geo_grid_spec=shyft::dtss::geo::grid_spec;
    
        py::class_<geo_grid_spec>("GeoGridSpec",
            doc_intro("A point set for a geo-grid, but does not have to be a regular grid.")
            doc_intro("It serves the role of defining the spatial representative points for")
            doc_intro("a typical spatial  grid, e.g as for arome, or ec forecasts.")
        )
        .def(py::init<int64_t,vector<geo_point>>(
            (py::arg("epsg"),py::arg("points")),
            doc_intro("Construct a GeoQuery from specified parameterrs")
            doc_parameters()
            doc_parameter("epsg","int","A valid epsg for the spatial points")
            doc_parameter("points","GeoPointVector","0 or more representative points for the spatial properties of the grid")
            )
        )
        .def_readonly("epsg",&geo_grid_spec::epsg, "the epsg coordinate system")
        .def_readonly("points",&geo_grid_spec::points,"the representative points of the spatial grid")
        .def("find_geo_match",&geo_grid_spec::find_geo_match_ix,(py::arg("self"),py::arg("geo_query")),
             doc_intro("finds the points int the grid that is covered by the polygon of the geo_query")
             doc_intro("note: that currently we only consider the horizontal dimension when matching points")
             doc_parameters()
             doc_parameter("geo_query","GeoQuery","A polygon giving an area to capture")
             doc_returns("matches","IntVector","a list of all points that is inside, or on the border of the specified polygon, in guaranteed ascending point index order")
        )
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
    }

    static void geo_slice() {
        using geo_slice=shyft::dtss::geo::slice;
        using shyft::dtss::geo::ix_vector;
        
        py::class_<geo_slice>("GeoSlice",
            doc_intro("Keeps data that describes as slice into the t0-variable-ensemble-geo, (t,v,e,g), space.")
            doc_intro("It is the result-type of GeoTimeSeriesConfiguration.compute(GeoEvalArgs)")
            doc_intro("and is passed to the geo-db-read callback to specify wanted time-series to read.")
            doc_intro("Note that the content of a GeoSlice can only be interpreteded in terms of the")
            doc_intro("GeoTimeSeriesConfiguration it is derived from.")
            doc_intro("The indicies and values of the slice, strongly relates to the definition of it's geo-tsdb.")
        )
        .def(py::init<ix_vector const&,ix_vector const&,ix_vector const&,vector<utctime>const&,utctime>(
            (py::arg("v"),py::arg("g"),py::arg("e"),py::arg("t"),py::arg("ts_dt")),
            doc_intro("Construct a GeoSlice from supplied vectors.")
            doc_parameters()
            doc_parameter("v","IntVector", "list of variables idx, each defined by GeoTimeSeriesConfiguration.variables[i]")
            doc_parameter("e","IntVector", "list of ensembles, each in range 0..GeoTimeSeriesConfiguration.n_ensembles-1")
            doc_parameter("g","IntVector", "list of geo-point idx, each defined by GeoTimeSeriesConfiguration.grid.points[i]")
            doc_parameter("t","UtcTimeVector","list of t0-time points, each of them should exist in GeoTimeSeriesConfiguration.t0_times")
            doc_parameter("ts_dt","time", "time-length to read from each time-series, we read from [t0 .. t0+ts_dt>")
            )
        )
        .def_readwrite("v",&geo_slice::v, "list of variables idx, each defined by GeoTimeSeriesConfiguration.variables[i]")
        .def_readwrite("e",&geo_slice::e, "list of ensembles, each in range 0..GeoTimeSeriesConfiguration.n_ensembles-1")
        .def_readwrite("g",&geo_slice::g, "list of geo-point idx, each defined by GeoTimeSeriesConfiguration.grid.points[i]")
        .def_readwrite("t",&geo_slice::t, "list of t0-time points, each of them should exist in GeoTimeSeriesConfiguration.t0_times")
        .def_readwrite("ts_dt",&geo_slice::ts_dt,"time length to read from each time-series, [t0 .. t0+ts_dt>")
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
    }

    static void geo_ts_config() {
        using geo_ts_db_config=shyft::dtss::geo::ts_db_config;
        using geo_grid_spec=shyft::dtss::geo::grid_spec;
        
        py::class_<geo_ts_db_config,py::bases<>, std::shared_ptr<geo_ts_db_config> >("GeoTimeSeriesConfiguration",
            doc_intro("Contain minimal  description to efficiently work with arome/ec  forecast data")
            doc_intro("It defines the spatial, temporal and ensemble dimensions available, and")
            doc_intro("provides means of mapping a GeoQuery to a set of ts_urls that ")
            doc_intro("serves as keys for manipulating and assembling forcing input data")
            doc_intro("for example to the shyft hydrology region-models.")
        )
        .def(py::init<string const&,string const&,string const&,geo_grid_spec const&,vector<utctime> const&,utctime,int64_t,vector<string> const&>(
            (py::arg("prefix"),py::arg("name"),py::arg("description"),py::arg("grid"),py::arg("t0_times"),py::arg("dt"),py::arg("n_ensembles"),py::arg("variables")),
            doc_intro("Construct a GeoQuery from specified parameterrs")
            doc_parameters()
            doc_parameter("prefix","str","ts-url prefix, like shyft:// for internally stored ts, or geo:// for externally stored parts")
            doc_parameter("name","str","A shortest possible unique name of the configuration")
            doc_parameter("description","str","a human readable description of the configuration")
            doc_parameter("grid","GeoGridSpec","specification of the spatial grid")
            doc_parameter("t0_times","UtcTimeVector","List of time where we have register time-series,e.g forecast times, first timepoint")
            doc_parameter("dt","time","the (max) length of each geo-ts, so geo_ts total_period is [t0..t0+dt>")
            doc_parameter("n_ensembles","int","number of ensembles available, must be >0, 1 if no ensemples")
            doc_parameter("variables","string","list of minimal keys, representing temperature, preciptiation etc")
            )
        )
        .def_readonly("prefix",&geo_ts_db_config::prefix,"ts-url prefix, like shyft:// for internally stored ts, or geo:// for externally stored parts")
        .def_readwrite("name",&geo_ts_db_config::name, "the name for the config (keep it minimal)")
        .def_readwrite("description",&geo_ts_db_config::descr,"the human readable description of this geo ts db")
        .def_readonly("grid",&geo_ts_db_config::grid,"the spatial grid definition")
        .def_readwrite("t0_times",&geo_ts_db_config::t0_times,"list of time-points, where there are registred/available time-series")
        .def_readonly("dt",&geo_ts_db_config::dt,"the (max) length of each geo-ts, so geo_ts total_period is [t0..t0+dt>")
        .def_readonly("n_ensembles",&geo_ts_db_config::n_ensembles,"number of ensembles available, range 1..n")
        .def_readonly("variables",&geo_ts_db_config::variables,"the list of available properties, like short keys for preciptation,temperature etc")
        .def_readonly("t0_time_axis",&geo_ts_db_config::t0_time_axis,"t0 time-points as time-axis")
        //-- methods
        
        .def("compute",&geo_ts_db_config::compute,
             (py::arg("self"),py::arg("eval_args")),
             doc_intro("Compute the GeoSlice from evaluation arguments")
            doc_parameters()
            doc_parameter("eval_args","GeoEvalArgs","Specification to evaluate")
            doc_returns("geo_slice","GeoSlice","A geo-slice describing (t0,v,e,g) computed")
        )
        .def("find_geo_match_ix",&geo_ts_db_config::find_geo_match_ix,(py::arg("self"),py::arg("geo_query")),
            doc_intro("Returns the indicies to the points that matches the geo_query (polygon)")
            doc_parameters()
            doc_parameter("geo_query","GeoQuery","The query, polygon that matches the spatial scope")
            doc_returns("point_indexes","IntVector","The list of indicies that  matches the geo_query")
        )
        .def("create_geo_ts_matrix",&geo_ts_db_config::create_geo_ts_matrix,(py::arg("self"),py::arg("slice")),
            doc_intro("Creates a GeoTsMatrix(element type is GeoTimeSeries) to hold the values according to dimensionality of GeoSlice")
            doc_parameters()
            doc_parameter("slice","GeoSlice","a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
            doc_returns("geo_ts_matrix","GeoTsMatrix","ready to be filled in with points and time-series")
        )
        .def("create_ts_matrix",&geo_ts_db_config::create_ts_matrix,(py::arg("self"),py::arg("slice")),
            doc_intro("Creates a GeoMatrix (element type is TimeSeries only) to hold the values according to dimensionality of GeoSlice")
            doc_parameters()
            doc_parameter("slice","GeoSlice","a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
            doc_returns("ts_matrix","GeoMatrix","ready to be filled in time-series(they are all empty/null)")
        )
        .def("bounding_box",&geo_ts_db_config::bounding_box,(py::arg("self"),py::arg("slice")),
            doc_intro("Compute the 3D bounding_box, as two GeoPoints containing the min-max of x,y,z of points in the GeoSlice")
            doc_intro("Could be handy when generating queries to externally  stored geo-ts databases like netcdf etc.")
	    doc_intro("See also convex_hull().")
            doc_parameters()
            doc_parameter("slice","GeoSlice","a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
            doc_returns("bbox","GeoPointVector","with two GeoPoints, [0] keeping the minimum x,y,z, and [1] the maximum x,y,z")
         )
	.def("convex_hull",&geo_ts_db_config::convex_hull,(py::arg("self"),py::arg("slice")),
	    doc_intro("Compute the 2D convex hull, as a list of GeoPoints describing the smallest convex planar polygon ")
	    doc_intro("containing all points in the slice wrt. x,y. ")
	    doc_intro("The returned point sequence is 'closed', i.e the first and last point in the sequence are equal. ")
	    doc_intro("See also bounding_box().")
            doc_parameters()
            doc_parameter("slice","GeoSlice","a geo-slice with specified dimensions in terms of t0, variables, ensembles,geo-points")
            doc_returns("hull","GeoPointVector","containing the sequence of points of the convex hull polygon.")
         )
        //-- handy stuff
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
        using GDBVector=vector<std::shared_ptr<geo_ts_db_config>>;
        py::class_<GDBVector>("GeoTimeSeriesConfigurationVector",
            doc_intro("A strongly typed list of GeoTimeSeriesConfigurations")
        )
        .def(py::vector_indexing_suite<GDBVector,true>()) // important! do not wrap shared ptr in vectors
        ;
    }
    static void geo_eval_args() {
        using geo_args=shyft::dtss::geo::eval_args;
        using geo_query=shyft::dtss::geo::query;
        
        py::class_<geo_args>("GeoEvalArgs",
            doc_intro(
            "GeoEvalArgs is used for the geo-evaluate functions.\n\n"
            "It describes scope for the geo-evaluate function, in terms of:\n"
            "    \n"
            "     - the geo-ts database identifier\n"
            "     - variables to extract, by names\n"
            "     - ensemble members (list of ints)\n"
            "     - temporal, using `t0` from specified time-axis, `+ ts_dt` for time-range\n"
            "     - spatial, using `points` for a polygon\n"
            "\n"
            "and optionally:\n"
            "    \n"
            "     - the concat postprocessing with parameters\n"
            "\n"
            )
        )
        .def(py::init<string const &, vector<string> const & ,vector<int64_t> const & ,gta_t  const &,utctime, geo_query const & , bool,utctime >(
            (py::arg("geo_ts_db_id"),py::arg("variables"),py::arg("ensembles"),py::arg("time_axis"),py::arg("ts_dt"),py::arg("geo_range"),py::arg("concat"),py::arg("cc_dt0")),
            doc_intro("Construct GeoEvalArgs from specified parameters")
            doc_parameters()
            doc_parameter("geo_ts_db_id","str","identifies the geo-ts-db, short, as 'arome', 'ec', as specified with server.add_geo_ts_db(cfg)")
            doc_parameter("variables","StringVector","names of the wanted variables, if empty, return all variables configured")
            doc_parameter("ensembles","IntVector","List of ensembles, if empty, return all ensembles configured")
            doc_parameter("time_axis","TimeAxis","specifies the t0, and  .total_period().end is used as concact open-end fill-in length")
            doc_parameter("ts_dt","time","specifies the time-length to read from  each time-series,t0.. t0+ts_dt, and  .total_period().end is used as concact open-end fill-in length")
            doc_parameter("geo_range","GeoQuery","the spatial scope of the query, if empty, return all configured")
            doc_parameter("concat","bool","postprocess using concatenated forecast, returns 'one' concatenated forecast from many.")
            doc_parameter("cc_dt0","time","concat lead-time, skip cc_dt0 of each forecast (offsets the slice you selects)")
            )
        )
        .def_readwrite("geo_ts_db_id",&geo_args::geo_ts_db_id, "the name for the config (keep it minimal)")
        .def_readwrite("variables",&geo_args::variables,"the human readable description of this geo ts db")
        .def_readwrite("ens",&geo_args::ens,"list of ensembles to return, empty=all, if specified >0")
        .def_readwrite("t0_time_axis",&geo_args::ta,"specifies the t0, and  .total_period().end is used as concact open-end fill-in length")
        .def_readwrite("ts_dt",&geo_args::ts_dt,"specifies the time-length to read from  each time-series,t0.. t0+ts_dt,")
        .def_readwrite("geo_range",&geo_args::geo_range,"the spatial scope, as simple polygon")
        .def_readwrite("concat",&geo_args::concat,"postprocess using concatenated forecast, returns 'one' concatenated forecast from many")
        .def_readwrite("cc_dt0",&geo_args::cc_dt0,"concat lead-time")

        //-- handy stuff
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
    }
    using shyft::time_series::dd::ats_vector;
    using shyft::dtss::geo::detail::ix_calc;
    

    void def_geo_matrix_shape() {
        using shyft::dtss::geo::detail::ix_calc;
        py::class_<ix_calc>("GeoMatrixShape",py::no_init)
        .def(
            py::init<int,int,int,int>( (py::arg("n_t0"),py::arg("n_v"),py::arg("n_e"),py::arg("n_g")),
             doc_intro("Create with specified dimensionality")
            )
        )
        .def_readonly("n_t0",&ix_calc::n_t0)
        .def_readonly("n_v",&ix_calc::n_v)
        .def_readonly("n_e",&ix_calc::n_e)
        .def_readonly("n_g",&ix_calc::n_g)
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
    }
    void def_ts_matrix() {
        using shyft::dtss::geo::ts_matrix;
        py::class_<ts_matrix>("GeoMatrix",
            doc_intro("GeoMatrix is 4d matrix,index dimensions (t0,variable,ensemble,geo_point)")
            doc_intro("to be understood as a slice of a geo-ts-db (slice could be the entire db)")
            doc_intro("The element type of the matrix is TimeSeries")
            ,
            py::no_init
        )
        .def(py::init<int,int,int,int>(
              (py::arg("n_t0"),py::arg("n_v"),py::arg("n_e"),py::arg("n_g")),
               doc_intro("create GeoMatrix with specified t0,variables,ensemble and geo-point dimensions")
            )
        )
        .def("set_ts",&ts_matrix::set_ts,(py::arg("self"),py::arg("t"),py::arg("v"),py::arg("e"),py::arg("g"),py::arg("ts")),
            doc_intro("performs self[t,v,e,g]= ts")
        )
        .def("get_ts",&ts_matrix::ts,(py::arg("self"),py::arg("t"),py::arg("v"),py::arg("e"),py::arg("g")),
            doc_intro("return self[t,v,e,g] of type TimeSeries"),
             py::return_value_policy<py::copy_const_reference>()
        )
        .def_readonly("shape",&ts_matrix::shape)
        .def("concatenate",&ts_matrix::concatenate,(py::arg("self"),py::arg("cc_dt0"),py::arg("concat_interval")),
             doc_intro("Concatenate all the forecasts in the GeoMatrix using supplied parameters")
             doc_parameters()
             doc_parameter("cc_dt0","time","skip first period of length cc_dt0 each forecast")
             doc_parameter("concat_interval","time","the nominal length between each ts.time(0) of all time-series")
             doc_returns("tsm","GeoMatrix","A new concatenated geo-ts-matix")
         )
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
        ;
    }
    void def_geo_ts_matrix() {
        using shyft::dtss::geo::geo_ts_matrix;
        py::class_<geo_ts_matrix>("GeoTsMatrix",
            doc_intro("GeoTsMatrix is 4d matrix,index dimensions `(t0,variable,ensemble,geo_point)`")
            doc_intro("to be understood as a slice of a geo-ts-db (slice could be the entire db)")
            doc_intro("The element types of the matrix is " doc_ref_class("GeoTimeSeries"))
            ,
            py::no_init
        )
        .def(py::init<int,int,int,int>(
              (py::arg("n_t0"),py::arg("n_v"),py::arg("n_e"),py::arg("n_g")),
               doc_intro("create GeoTsMatrix with specified t0,variables,ensemble and geo-point dimensions")
            )
        )
        .def_readonly("shape",&geo_ts_matrix::shape)
        .def("set_ts",&geo_ts_matrix::set_ts,(py::arg("self"),py::arg("t"),py::arg("v"),py::arg("e"),py::arg("g"),py::arg("ts")),
            doc_intro("performs self[t,v,e,g].ts= ts")
        )
        .def("get_ts",&geo_ts_matrix::ts,(py::arg("self"),py::arg("t"),py::arg("v"),py::arg("e"),py::arg("g")),
            doc_intro("return self[t,v,e,g] of type TimeSeries"),
             py::return_value_policy<py::copy_const_reference>()
        )
        .def("set_geo_point",&geo_ts_matrix::set_geo_point,(py::arg("self"),py::arg("t"),py::arg("v"),py::arg("e"),py::arg("g"),py::arg("point")),
            doc_intro("performs self[t,v,e,g].mid_point= point")
        )
        .def("get_geo_point",&geo_ts_matrix::get_geo_point,(py::arg("self"),py::arg("t"),py::arg("v"),py::arg("e"),py::arg("g")),
            doc_intro("return self[t,v,e,g].mid_point of type GeoPoint"),
             py::return_value_policy<py::copy_const_reference>()
        )
        .def("concatenate",&geo_ts_matrix::concatenate,(py::arg("self"),py::arg("cc_dt0"),py::arg("concat_interval")),
             doc_intro("Concatenate all the forecasts in the GeoTsMatrix using supplied parameters")
             doc_parameters()
             doc_parameter("cc_dt0","time","skip first period of length cc_dt0 each forecast")
             doc_parameter("concat_interval","time","the nominal length between each ts.time(0) of all time-series")
             doc_returns("tsm","GeoTsMatrix","A new concatenated geo-ts-matix")
         )
        .def(py::self==py::self)
        .def(py::self!=py::self)
        ;
        
    }
    
    void all_geo_stuff() {
        geo_ts();
        geo_query();
        geo_slice();
        geo_grid_spec();
        geo_eval_args();
        geo_ts_config();
        def_ts_matrix();
        def_geo_ts_matrix();
        def_geo_matrix_shape();

    }

}
