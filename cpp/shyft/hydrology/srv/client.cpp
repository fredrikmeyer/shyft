#include <shyft/hydrology/srv/client.h>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/variant.hpp>
#include <shyft/core/core_archive.h>

namespace shyft::hydrology::srv {

using shyft::core::core_iarchive;
using shyft::core::core_oarchive;
using std::chrono::seconds;
using std::chrono::milliseconds;
using shyft::core::scoped_connect;
using std::make_unique;
using std::max;
using std::this_thread::sleep_for;

/** @brief a client
*
* 
* This class take care of message exchange to the remote server,
* using the supplied connection parameters.
* 
* It implements the message protocol of the server, sending
* message-prefix, arguments, waiting for response
* deserialize the response and handle it back to the user.
* 
* @see server
* 
* 
*/
client::client(string host_port,int timeout_ms):c{host_port,timeout_ms}{}


namespace {
    struct r_void{};// just to fix void fx's 
    
    template<message_type::type mtype,class R, class ...A>
    R exchange_msg(client&c, A&& ...args) {
        scoped_connect sc(c.c);
        R r{};
        for (int retry = 0; retry < 3; ++retry) {
            try {
                auto& io = *( c.c.io);
                msg::write_type(mtype,io);
                if constexpr (sizeof ...(args) >0) { // if there are any args to forward to server...
                    core_oarchive oa(io, core_arch_flags);// only then create the io stream
                    ( (oa<< std::forward<decltype(args)>(args) ),...); // unpack the args, use std:: forward to ensure we do not tamper with type of args
                }
                auto response_type = msg::read_type(io);
                if (response_type == message_type::SERVER_EXCEPTION) {
                    auto re = msg::read_exception(io);
                    throw re;
                } else if (response_type == mtype) {
                    if constexpr (!std::is_same<R,r_void>::value){
                        core_iarchive ia(io, core_arch_flags);
                        ia >> r;
                    }
                    return r;

                } else {
                    throw runtime_error(string("Got unexpected response:") + to_string((int)response_type));
                }
                                
            } catch (const dlib::socket_error&) {
                c.c.reopen();
            }
        }
        throw runtime_error("Failed to establish connection with " + c.c.host_port);
    }
}

/** @brief typical client pattern
*
* @param mid the model-id to which we update the model-info on
* @return true if succeeded (model exists, and was removed)
* 
*/

string client::get_version_info() {
    return exchange_msg<message_type::VERSION_INFO,string>(*this);
}

bool client::create_model(string const& mid, rmodel_type mtype, vector <core::geo_cell_data> const& gcd){
    return exchange_msg<message_type::CREATE_MODEL,bool>(*this,mid,mtype,gcd);
}

bool client::set_state(string const& mid, state_variant_t const& csv){
    return exchange_msg<message_type::SET_STATE,bool>(*this,mid,csv);
}

state_variant_t client::get_state(string const& mid, const shyft::api::cids_t& cids){
    return exchange_msg<message_type::GET_STATE,state_variant_t>(*this,mid,cids);
}

gta_t client::get_time_axis(string const&mid) {
    return exchange_msg<message_type::GET_TIME_AXIS,gta_t>(*this,mid);
}

parameter_variant_t client::get_region_parameter(string const& mid) {
    return exchange_msg<message_type::GET_REGION_PARAMETER,parameter_variant_t>(*this,mid);
}

parameter_variant_t client::get_catchment_parameter(string const& mid,int64_t cid) {
    return exchange_msg<message_type::GET_CATCHMENT_PARAMETER,parameter_variant_t>(*this,mid,cid);
}

bool client::set_region_parameter(string const&mid,parameter_variant_t const& pv) {
    return exchange_msg<message_type::SET_REGION_PARAMETER,bool>(*this,mid,pv);
}

bool client::set_catchment_parameter(string const&mid,parameter_variant_t const& pv,size_t cid) {
    return exchange_msg<message_type::SET_CATCHMENT_PARAMETER,bool>(*this,mid,pv,cid);
}
//        void remove_catchment_parameter(string const&mid,size_t cid);

void client::remove_catchment_parameter(string const&mid,size_t cid) {
    exchange_msg<message_type::REMOVE_CATCHMENT_PARAMETER,r_void>(*this,mid,cid);
}

bool client::has_catchment_parameter(string const&mid,size_t cid) {
    return exchange_msg<message_type::HAS_CATCHMENT_PARAMETER,bool>(*this,mid,cid);
}

std::vector<shyft::core::geo_cell_data> client::get_geo_cell_data(string const &mid) {
    return exchange_msg<message_type::GET_GEOCELLDATA,std::vector<shyft::core::geo_cell_data>>(*this,mid);
}

shyft::api::a_region_environment client::get_region_env(string const &mid) {
    return exchange_msg<message_type::GET_REGION_ENV,shyft::api::a_region_environment>(*this,mid);
}

shyft::core::interpolation_parameter client::get_interpolation_parameter(string const &mid) {
    return exchange_msg<message_type::GET_INTERPOLATION_PARAMETER,shyft::core::interpolation_parameter>(*this,mid);
}

bool client::remove_model(string const&mid) {
    return exchange_msg<message_type::REMOVE_MODEL,bool>(*this,mid);
}

string client::get_description(string const& mid) {
    return exchange_msg<message_type::GET_DESCRIPTION,string>(*this,mid);
}
bool client::set_description(string const& mid,string const&description) {
    return exchange_msg<message_type::SET_DESCRIPTION,bool>(*this,mid,description);
}
bool client::rename_model(string const& old_mid, string const& new_mid) {
    return exchange_msg<message_type::RENAME_MODEL,bool>(*this,old_mid,new_mid);
}

bool client::clone_model(string const& old_mid, string const& new_mid) {
    return exchange_msg<message_type::CLONE_MODEL,bool>(*this,old_mid,new_mid);
}

bool client::copy_model(string const& old_mid, string const& new_mid) {
    return exchange_msg<message_type::COPY_MODEL,bool>(*this,old_mid,new_mid);
}

vector<string> client::get_model_ids() {
    return exchange_msg<message_type::GET_MODEL_IDS,vector<string>>(*this);    
}

bool client::run_interpolation(string const& mid, const shyft::core::interpolation_parameter& ip_parameter, const shyft::time_axis::generic_dt& ta, const shyft::api::a_region_environment& r_env, bool best_effort) {
    return exchange_msg<message_type::RUN_INTERPOLATION,bool>(*this,mid,ip_parameter,ta,r_env,best_effort);
}

bool client::run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps) {
    return exchange_msg<message_type::RUN_CELLS,bool>(*this,mid,int64_t(use_ncore),int64_t(start_step),int64_t(n_steps));
}

q_adjust_result client::adjust_q(string const& mid, const shyft::api::cids_t& cids, double wanted_q,size_t start_step,double scale_range,double scale_eps,size_t max_iter,size_t n_steps) {
    return exchange_msg<message_type::ADJUST_Q,q_adjust_result>(*this,mid,cids,wanted_q,int64_t(start_step),scale_range,scale_eps,int64_t(max_iter),int64_t(n_steps));
}

apoint_ts client::get_discharge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_DISCHARGE,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_temperature(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_TEMPERATURE,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_precipitation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_PRECIPITATION,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_snow_swe(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_SNOW_SWE,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_charge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_CHARGE,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_snow_sca(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_SNOW_SCA,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_radiation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_RADIATION,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_wind_speed(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_WIND_SPEED,apoint_ts>(*this,mid,cids,ix_type);
}

apoint_ts client::get_rel_hum(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type) {
    return exchange_msg<message_type::GET_REL_HUM,apoint_ts>(*this,mid,cids,ix_type);
}

bool client::set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list) {
    return exchange_msg<message_type::SET_CATCHMENT_CALCULATION_FILTER,bool>(*this,mid,catchment_id_list);
}

bool client::revert_to_initial_state(string const& mid) {
    return exchange_msg<message_type::REVERT_TO_INITIAL_STATE,bool>(*this,mid);
}

bool client::set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off) {
    return exchange_msg<message_type::SET_STATE_COLLECTION,bool>(*this,mid,catchment_id,on_or_off);
}

bool client::set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off) {
    return exchange_msg<message_type::SET_SNOW_SCA_SWE_COLLECTION,bool>(*this,mid,catchment_id,on_or_off);
}

bool client::is_cell_env_ts_ok(string const& mid) {
    return exchange_msg<message_type::IS_CELL_ENV_TS_OK,bool>(*this,mid);
}

bool client::is_calculated(string const& mid, size_t cid) {
    return exchange_msg<message_type::IS_CALCULATED,bool>(*this,mid,cid);
}

bool client::set_initial_state(string const& mid) {
    return exchange_msg<message_type::SET_INITIAL_STATE,bool>(*this,mid);
}


bool client::start_calibration(string const& mid,parameter_variant_t const& p_start, parameter_variant_t const& p_min,parameter_variant_t const& p_max, vector<target_specification> const& spec, calibration_options opt){
    return exchange_msg<message_type::START_CALIBRATION,bool>(*this,mid,p_start,p_min,p_max,spec,opt);
}
calibration_status client::check_calibration(string const& mid){
    return exchange_msg<message_type::CHECK_CALIBRATION,calibration_status>(*this,mid);
}
bool client::cancel_calibration(string const& mid) {
    return exchange_msg<message_type::CANCEL_CALIBRATION,bool>(*this,mid);
}
bool client::fx(string const&mid, string const& fx_args) {
    return exchange_msg<message_type::DO_FX,bool>(*this,mid,fx_args);
}

/** @brief close, until needed again, the server connection
*
*/
void client::close() {
    c.close();//just close-down connection, it will auto-open if needed
}
}
