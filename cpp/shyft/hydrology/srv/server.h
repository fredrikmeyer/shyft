#pragma once


/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/

#include <string>
#include <cstdint>
#include <exception>
#include <memory>
#include <map>
#include <atomic>
#include <functional>

#ifdef __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#endif
#include <boost/thread/locks.hpp>
#include <boost/thread/shared_mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#ifdef __GNUC__
#pragma GCC diagnostic pop
#endif

#include <shyft/srv/fast_server_iostream.h>
#include <shyft/srv/fast_iosockstream.h>

#include <shyft/hydrology/srv/msg_types.h>


namespace shyft::hydrology::srv {

    using std::vector;
    using std::shared_ptr;
    using std::make_shared;
    using std::string;
    using std::to_string;
    using std::runtime_error;
    using std::mutex;
    using std::unique_lock;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;
    using gta_t=shyft::time_axis::generic_dt;
    using shyft::core::q_adjust_result;
    using shyft::core::region_model;
    using shyft::core::model_calibration::target_specification;
    using namespace shyft::core;
    using shyft::api::a_region_environment;
    // we use boost sync primitives  since they support upgrade locks
    using srv_shared_mutex=boost::shared_mutex;
    using srv_condition_variable=boost::condition_variable;
    using srv_shared_lock=boost::shared_lock<srv_shared_mutex>;
    using srv_upgradable_lock=boost::upgrade_lock<srv_shared_mutex>;
    using srv_unique_lock=boost::unique_lock<srv_shared_mutex>;
    using srv_upgrade_lock=boost::upgrade_to_unique_lock<srv_shared_mutex>;
    using server_iostream_t=shyft::srv::fast_server_iostream;
    using iosockstream_t=shyft::srv::fast_iosockstream;
    using fx_call_back_t = std::function<bool(string,string)>; ///< the type of callback provided, 1st arg model-id, 2nd arg fx-verb

    /** @brief The server side model context
     * @details Keeps the server side data, as a minimum the hydrology model,
     * and optionally other stuff like the optimization engine(if started)
     * with the results.
     */
    struct model_context {
        enum class ctx_state:int8_t {
            idle,           // Model is available in full to the user
            optimizing     // Model is currently running an optimizing procedure, limited availability
            //, done // as in there is an optimization result ?
        };
        
        model_context()=delete;
        model_context(model_context const&)=delete;
        model_context(model_context &&)=delete;
        model_context& operator=(model_context const&)=delete;
        model_context& operator=(model_context &&)=delete;
        
        model_context(model_variant_t ma):m{std::move(ma)} {}
        string description;///< any useful description, usually a json structured text.
        // the stuff that keep the context of a drms model
        srv_shared_mutex mtx; ///< need to protect the rm during access
        srv_condition_variable run_ready; /// while running/calibrating, cv wait using above mtx
        std::atomic<ctx_state> state{ctx_state::idle}; ///< idle,optimizing ..
        shared_ptr<calibration_variant_t> mc; ///< only if calibration is invoked/ongoing
        std::future<parameter_variant_t> calibration;///< future of current calibration run(if any)
        model_variant_t m;   ///< the region model model we are working with.
    };
    using model_context_=std::shared_ptr<model_context>;
    
    /** safely support getting a model with unique lock (no glitch) */
    struct locked_model_context {
        srv_unique_lock l;
        model_context_ c;
    };
    struct shared_model_context {
        srv_shared_lock l;
        model_context_ c;        
    };
    
    /** @brief a server for serializable (parts of) hydrology forecasting models,
    *
    * Currently using dlib server_iostream, 
    */
    struct server : server_iostream_t {
        
        server() =default;    
        server(server&&)=delete;
        server(const server&) =delete;
        server& operator=(const server&)=delete;
        server& operator=(server&&)=delete;
        ~server() =default;
        
        srv_shared_mutex srv_mx;///< protect server context
        std::map<string,model_context_> model_map;///< key=mid, model.. shared_ptr
        //std::map<string,shared_ptr<srv_shared_mutex> mx_map;///< key=mid, value- shared mutext for model
        std::atomic_size_t alive_connections{0};///< keep a count of alive server-alive_connections for diagnostics
        fx_call_back_t fx_cb;///< the user provided callback(mid,fx_args)
        /** start the server in background, return the listening port used in case it was set unspecified */
        int start_server();
        
        string do_get_version_info();
        
        /**  create a model based on geo_cell_data,  default parameter of specified type
         * TODO: consider using table-driven meta-programming approach to minimize maintenance
         */
        
        model_variant_t make_shared_model_of_type(rmodel_type mtype,vector <core::geo_cell_data> const& gcd);
        model_context_ make_context(rmodel_type mtype,vector <core::geo_cell_data> const& gcd);
        
        /** @brief creates a new model, with model-id and specified type */
        bool do_create_model(string const& mid, 
                             rmodel_type mtype, 
                             vector <core::geo_cell_data> const& gcd);
        

        /** @brief remove (free up mem etc) of region-model  model-id */
        bool do_remove_model(string const& mid);
        
        /** @brief get models, returns a string list with model identifiers */
        vector<string> do_get_model_ids() ;

        
        /** @brief given model id, safely get a shared_ptr to that */
        model_variant_t get_model(string mid) ;
        
        model_context_ get_context(string const& mid);///< get a unlocked model context
        locked_model_context get_locked(string const& mid); ///< get exclusive locked model context(you want to modify it)
        shared_model_context get_shared(string const& mid);///< get shared read-only locked model context.(its not enforced by const yet)
        
        /** @brief returns a shared_ptr to the mutex for model id */
        //srv_shared_mutex & get_model_mx(string mid);
        gta_t do_get_time_axis(string const& mid);
        /** @brief rename a model */
        bool do_rename_model(string old_mid, string new_mid);
        
        bool do_set_state(string const& mid, state_variant_t const& csv);
        
        state_variant_t do_get_state(string const& mid, shyft::api::cids_t& cids);
        
        bool do_set_region_parameter(string const&mid, parameter_variant_t const&p);

        bool do_set_catchment_parameter(string const&mid, parameter_variant_t const&p,size_t cid);

        bool do_run_interpolation(string const& mid, 
                                       const shyft::core::interpolation_parameter& ip_parameter,
                                       const shyft::time_axis::generic_dt& ta,
                                       const shyft::api::a_region_environment& r_env,
                                       bool best_effort);

        vector<core::geo_cell_data> do_get_geo_cell_data(string const &mid);
        shyft::api::a_region_environment do_get_region_env(string const &mid);
        shyft::core::interpolation_parameter do_get_interpolation_parameter(string const &mid);
        string do_get_descript(string const &mid);
        bool do_set_descript(string const &mid,string const& description);

        bool do_run_cells(string const& mid,size_t use_ncore,int start_step,int n_steps);
        
        q_adjust_result do_adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1);


        apoint_ts do_get_discharge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_temperature(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_precipitation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_snow_swe(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_charge(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_snow_sca(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_radiation(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_wind_speed(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        apoint_ts do_get_rel_hum(string const& mid, const shyft::api::cids_t& cids, shyft::core::stat_scope ix_type=shyft::core::stat_scope::cell_ix);
        
        bool do_set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list);
        
        bool do_revert_state(string const& mid);
        
        bool do_clone_model(string const& old_mid, string const& new_mid) ;
        
        bool do_copy_model(string const& old_mid, string const& new_mid);
        
        bool do_set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off);
        
        bool do_set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off);
        
        bool do_is_cell_env_ts_ok(string const& mid);
        
        bool do_set_initial_state(string const& mid) ;
        
        bool do_is_calculated(string const& mid, size_t cid);

        parameter_variant_t do_get_region_parameter(string const& mid);
        parameter_variant_t do_get_catchment_parameter(string const& mid,int64_t cid);
        void do_remove_catchment_parameter(string const& mid,int64_t cid);
        bool do_has_catchment_parameter(string const& mid,int64_t cid);
        
        bool do_start_calibration(string const& mid,parameter_variant_t const& p_start, parameter_variant_t const& p_min,parameter_variant_t const& p_max, vector<target_specification> const& spec, calibration_options opt);
        calibration_status do_check_calibration(string const& mid);
        bool do_cancel_calibration(string const& mid);
        bool do_fx_cb(string const& mid,string const &fx_args);
        //bool do_start_calibration(string const& mid,vector<target_specification_t> const &tspec, parameter_variant_t const& p0,size_t max_iterations,utctime time_limit);
            
        /**@brief handle one client connection 
        *
        * Reads messages/requests from the clients,
        * - act and perform request,
        * - return response
        * for as long as the client keep the connection 
        * open.
        * 
        */
        void on_connect(
            std::istream & in,
            std::ostream & out,
            const std::string & foreign_ip,
            const std::string & local_ip,
            unsigned short foreign_port,
            unsigned short local_port,
            dlib::uint64 /*connection_id*/
        );    
        
    };
    
}
