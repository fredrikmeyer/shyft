/** This file is part of Shyft. Copyright 2015-2018 SiH, JFB, OS, YAS, Statkraft AS
See file COPYING for more details **/
#pragma once

#include <string>
#include <cstdint>
#include <exception>
#include <memory>
#include <vector>
#include <thread>

#include <shyft/hydrology/srv/msg_types.h> // looking for header files, plus common defs of variant.
#include <shyft/hydrology/target_specification.h>

namespace shyft::hydrology::srv {
    using std::vector;
    using std::string;
    using std::string_view;
    using std::to_string;
    using std::runtime_error;
    using std::exception;
    using std::unique_ptr;
    using std::shared_ptr;
    using shyft::core::srv_connection;
    using shyft::time_series::dd::apoint_ts;
    using shyft::core::q_adjust_result;
    using gta_t=shyft::time_axis::generic_dt;
    using shyft::core::model_calibration::target_specification;

    /** @brief a client
    *
    * 
    * This class take care of message exchange to the remote server,
    * using the supplied connection parameters.
    * 
    * It implements the message protocol of the server, sending
    * message-prefix, arguments, waiting for response
    * deserialize the response and handle it back to the user.
    * 
    * @see server
    * 
    * 
    */
    struct client {
        srv_connection c;
        //client()=delete;
        client(string host_port,int timeout_ms=1000);
        
        /** @brief typical client pattern
        *
        * @param mid the model-id to which we update the model-info on
        * @return true if succeeded (model exists, and was removed)
        * 
        */

        string get_version_info();

        bool create_model(string const& mid, 
                          rmodel_type mtype, 
                          vector <core::geo_cell_data> const& gcd);
        
        bool set_state(string const& mid, state_variant_t const& csv);

        state_variant_t get_state(string const& mid, const shyft::api::cids_t& cids);
        
        bool set_region_parameter(string const&mid,parameter_variant_t const& pv);
        
        bool set_catchment_parameter(string const&mid,parameter_variant_t const& pv,size_t cid);
        void remove_catchment_parameter(string const&mid,size_t cid);
        bool has_catchment_parameter(string const& mid,size_t cid);
        
        bool remove_model(string const&mid);
        
        bool rename_model(string const& old_mid, string const& new_mid);
        
        bool clone_model(string const& old_mid, string const& new_mid);
        
        bool copy_model(string const& old_mid, string const& new_mid);
        
        vector<string> get_model_ids();
        
        bool run_interpolation(string const& mid, const shyft::core::interpolation_parameter& ip_parameter, const shyft::time_axis::generic_dt& ta, const shyft::api::a_region_environment& r_env, bool best_effort);
        bool run_cells(string const& mid,size_t use_ncore=0,int start_step=0,int n_steps=0);
        
        q_adjust_result adjust_q(string const& mid, const shyft::api::cids_t& indexes, double wanted_q,size_t start_step=0,double scale_range=3.0,double scale_eps=1e-3,size_t max_iter=300,size_t n_steps=1);
        apoint_ts get_discharge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_temperature(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_precipitation(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_snow_swe(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_charge(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_snow_sca(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_radiation(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_wind_speed(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        
        apoint_ts get_rel_hum(string const& mid, const shyft::api::cids_t& indexes, shyft::core::stat_scope ix_type);
        bool set_catchment_calculation_filter(string const& mid, const std::vector<int64_t>& catchment_id_list);
        
        bool revert_to_initial_state(string const& mid);
        
        bool set_state_collection(string const& mid, int64_t catchment_id,bool on_or_off);
        
        bool set_snow_sca_swe_collection(string const& mid, int64_t catchment_id,bool on_or_off);
        
        bool is_cell_env_ts_ok(string const& mid);
        
        bool is_calculated(string const& mid, size_t cid);
        
        bool set_initial_state(string const& mid);
        
        gta_t get_time_axis(string const& mid);
        parameter_variant_t get_region_parameter(string const& mid);
        parameter_variant_t get_catchment_parameter(string const& mid, int64_t cid);
        
        bool start_calibration(string const& mid,parameter_variant_t const& p_start, parameter_variant_t const& p_min,parameter_variant_t const& p_max, vector<target_specification> const& spec, calibration_options opt);
        calibration_status check_calibration(string const& mid);
        bool cancel_calibration(string const& mid);
        
        std::vector<shyft::core::geo_cell_data> get_geo_cell_data(string const &mid);
        shyft::api::a_region_environment get_region_env(string const &mid);
        shyft::core::interpolation_parameter get_interpolation_parameter(string const &mid);
        string get_description(string const& mid);
        bool set_description(string const& mid,string const&description);
        bool fx(string const&mid, string const&fx_args);
        /** @brief close, until needed again, the server connection
        *
        */
        void close();
    };
}
