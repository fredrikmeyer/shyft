#pragma once
#include <shyft/srv/db_io.h>
#include <shyft/core/subscription.h>
#include <string>

namespace shyft::srv {

        using std::string;
        using std::vector;
        using std::runtime_error;
        using std::shared_ptr;
        using shyft::core::utcperiod;

        using shyft::core::subscription::manager_;

    struct db_file : db_io{
        string root_dir;///< the root-directory for models

        db_file()=delete;
        db_file(const db_file&)=delete;
        db_file(string const&root_dir);
        ~db_file(){};
        vector<model_info> get_model_infos(vector<int64_t>const & mids) override;
        vector<model_info> get_model_infos(vector<int64_t>const & mids, utcperiod per) override;
        bool update_model_info(int64_t mid,model_info const &mi) override;
        int64_t store_model_blob( string const& m, model_info const &mi) override;
        string read_model_blob(int64_t mid) const override;
        int64_t remove_model(int64_t mid) override;
        int64_t find_max_model_id(bool fill_cache=false);

      private:
        std::optional<model_info> read_model_info(string const&fn) const;
        std::optional<model_info> read_model_info(int64_t mid) const;

    };
}
