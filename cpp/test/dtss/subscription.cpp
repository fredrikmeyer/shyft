#include "test_pch.h"
#include <shyft/dtss/dtss_subscription.h>
#include <shyft/time_series/dd/compute_ts_vector.h>
#include <unordered_map>

using namespace shyft::core;
using namespace shyft::time_axis;
using namespace shyft::time_series::dd;

using std::vector;
using std::make_shared;
using std::forward;
using std::map;
using std::string;
//using std::string_view;
using std::to_string;
using std::runtime_error;
using std::end;
using std::unordered_map;
using std::array;
using std::recursive_mutex;

using std::scoped_lock;
using std::begin;
using std::end;
using std::atomic_int64_t;

namespace test::dtss {
    //NOTE: this is just example code to study the mechanisms
    //      that we later implement on time_series::dd ipoint_ts and friends
    // 
    string shyft_url(string ts_name) {
        return string{"shyft://test/"}+ts_name;
    }
    
    ats_vector make_test_tsv(size_t n_ts) {
        ats_vector r;
        calendar utc;
        gta_t ta{utc.time(2019,10,1),deltahours(1),24};
        for(size_t i=0;i<n_ts;++i)
            r.emplace_back(3.0*apoint_ts{shyft_url(to_string(i))});
        return r;
    }
   
}
using namespace test::dtss;
using namespace shyft::dtss::subscription;

TEST_SUITE("dtss") {
    using shyft::core::subscription::manager;
    TEST_CASE("subscription_manager") {
        auto tsv=make_test_tsv(3); 
        vector<vector<ts_bind_info>> ts_terminals;
        
        // 0.  using already tested functions to extract the unbound terminals
        for(auto const&ts:tsv) 
            ts_terminals.emplace_back(ts.find_ts_bind_info());
        REQUIRE_EQ(ts_terminals.size(),tsv.size());
        
        for(auto const&bi:ts_terminals) {
            CHECK_EQ(1,bi.size());
        }
        // -- now test the subscription_manager : 
        
        auto sm = make_shared<manager>();
        auto ts_value=1.0;
        auto fx=[&ts_value](ats_vector expr)->ats_vector {
            calendar utc;
            gta_t ta{utc.time(2019,10,1),deltahours(1),24};
            for(auto &ts:expr) {
                auto biv=ts.find_ts_bind_info();
                for(auto&bi:biv)
                    bi.ts.bind(apoint_ts{ta,ts_value,shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE});
                ts.do_bind();
            }
            return ats_vector{deflate_ts_vector<apoint_ts>(expr)};// compute and flatten expression
        };
        size_t master_notify_count=0;// we count number of master cb here
        sm->master_changed_cb=[&master_notify_count]() {++master_notify_count;};// each real change should propagate here
        REQUIRE_EQ(0u,sm->total_change_count.load());
        CHECK_EQ(sm->total_unsubscribe_count.load(),0u);
        string request_id{"1"};
        /* */ {
            auto o=make_shared<ts_expression_observer>(sm,request_id,tsv,fx); // observe these expressions.
            REQUIRE_EQ(true,o->has_changed());
            CHECK_EQ(true,o->recalculate());
            //o->published_version=o->terminal_version();
            REQUIRE_EQ(false,o->has_changed());
            vector<string> ts_c01{shyft_url("0"),shyft_url("1")};
            vector<string> ts_unreleated{shyft_url("9")};
            sm->notify_change(ts_unreleated);
            REQUIRE_EQ(0u,master_notify_count);// nothing should change here
            CHECK_EQ(false,o->has_changed());// nothing should happen
            sm->notify_change(ts_c01);
            CHECK_EQ(true,o->has_changed());// now it should be changed.
            REQUIRE_EQ(1u,master_notify_count);// we should have 1 master cb
            ts_value=2.0;
            CHECK_EQ(true,o->recalculate());
            sm->notify_change(ts_c01);//propagate a change once more
            CHECK_EQ(false,o->recalculate());// second time, no change since last published view
            CHECK_EQ(sm->total_unsubscribe_count.load(),0u);

            REQUIRE_EQ(2u,master_notify_count);// we should have 2 master cb, because it was a change(even though not in the values)
            o=nullptr;// 
            CHECK_EQ(sm->active.size(),0u);// no more subs at this point
            CHECK_EQ(sm->total_unsubscribe_count.load(),tsv.size());
        }
        sm=nullptr;//defuse everything.
    }
    TEST_CASE("boost_signal2") {
        /* THIS IS  working, but not precicely what we need, 
         * HOWEVER: it's very strong functionality, thread and tracking-safe (ref. .use_count()==2 above.. it's a bit unsatisfying)'
        using registration_manager=boost::signals2::signal<void(string const&)>;
        //using registration_request=registration_manager::slot_type;
        using scoped_connection=boost::signals2::scoped_connection;
        registration_manager mgr;
        mgr.connect(0,[](string const&m){std::cout<<"Change event handler 1:"<<m<<std::endl;});
        mgr.connect(1,[](string const&m){std::cout<<"Change event handler 2:"<<m<<std::endl;});
        mgr("Hello world");
        mgr.disconnect(1);
        mgr("Jatta");
        {
            scoped_connection c(mgr.connect(1,[](string const&m){std::cout<<"Scoped Change event handler2:"<<m<<std::endl;}));
            mgr("in scope");
        }
        mgr("out of scope");
        //-- tracking observers through shared_ptr (requirement)
        auto c1=boost::shared_ptr<client_sub>(new client_sub{string{"c1"}});
        auto c2=make_shared<client_sub>(string{"c2"});
        mgr.disconnect(0);
        mgr.connect(0, registration_manager::slot_type(&client_sub::ts_changed,c1.get(),_1).track(c1));
        mgr.connect(0, registration_manager::slot_type(&client_sub::ts_changed,c2.get(),_1).track_foreign(c2));
        mgr("tracking 1");
        c1=nullptr;
        mgr("tracking 2");
        c2=nullptr;
        mgr("tracking 3, not displayed");*/
    }
}
