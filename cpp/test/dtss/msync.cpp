#include "test_pch.h"
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>
#include <shyft/dtss/ts_subscription.h>

#include <test/test_utils.h>

using namespace shyft::dtss;
using namespace shyft::time_series;
using std::make_unique;

namespace test {
    geo::ts_matrix create_forecast(geo::ts_db_config const&cfg,utctime fc_length,gta_t const&ta);
}

namespace {
    // to make life easier
    utctime _t(int s) noexcept {return shyft::core::from_seconds(s);}
    utctime _ms(int s) noexcept {return shyft::core::from_seconds(s/1000.0);}
    utcperiod _p(int b, int e) noexcept {return utcperiod(_t(b),_t(e));}

    /**
     * Arrange the test case with a 
     * (1) master dtss, (with shyft://test container setup)
     * (2) master client to work at the master side
     * (3) slave dtss with connect through msync to the master
     * (4) slave client to work from outside to the slave dtss
     */

    struct msync_setup {
        static constexpr auto stair_case=ts_point_fx::POINT_AVERAGE_VALUE;    
        const string local_only{"127.0.0.1"};

        test::utils::temp_dir tmpdir{"shyft.msync.store."};
    
        unique_ptr<server> master;                                         
        int master_port_no;
        unique_ptr<client> mc;

        unique_ptr<server> slave;
        int slave_port_no;
        unique_ptr<client> sc;

        
        size_t dstm_count{0};   
        msync_setup() {

            master=make_unique<server>();
            master->set_listening_ip(local_only);
            master->add_container("test",(tmpdir/"test").string());
            master->add_container("",tmpdir.string());//prep for geo
            master->set_can_remove(true);
            master_port_no=master->start_server();
            mc=make_unique<client>("localhost:" + std::to_string(master_port_no));

            slave= make_unique<server>([this](id_vector_t const&tsv,utcperiod p){return dstm_resolve(tsv,p);});
            slave->set_listening_ip(local_only);
            slave->set_can_remove(true);
            slave->set_master("localhost",master_port_no,0.001,10,0.1);
            slave_port_no=slave->start_server();
            sc=make_unique<client>("localhost:" + std::to_string(slave_port_no));

        }
        
        ///< callback from the slave dtss to resolve dstm:// queries
        ts_vector_t dstm_resolve(id_vector_t const& ts_ids,utcperiod p) {
            ++dstm_count;
            ts_vector_t r;
            for(auto id:ts_ids)
                r.push_back(apoint_ts{gta_t{p.start, _t(10),3},vector<double>{2.0,1.0,3.0},stair_case});
            return r;
        };

        ~msync_setup(){
            // controlled tear down in order
            sc.reset();
            mc.reset();
            slave->clear();
            master->clear();
        }


    };
    
}

TEST_SUITE("dtss_msync") {
    
TEST_CASE("ts_observer") {
    using shyft::core::subscription::manager;
    using shyft::dtss::subscription::ts_observer;
    auto sm= make_shared<manager>();
    auto o= make_shared<ts_observer>(sm,"subscription_request_id");
    
    FAST_CHECK_EQ(false, o->recalculate());
    FAST_CHECK_EQ(0u,o->find_changed_ts().size());
    
    auto p0=_p(0,10);
    auto p1=_p(5,15);
    auto p2=_p(0,15);
    
    o->subscribe_to(id_vector_t{"a","b"},p0);
    o->subscribe_to(id_vector_t{"b","c"},p1);// should extend first set, and then create a new  set for c in p1
    
    FAST_CHECK_EQ(3u,o->terminals.size());
    FAST_CHECK_EQ(3u,sm->active.size()); // ensure the sm also got it
    FAST_CHECK_EQ(0u,o->find_changed_ts().size());
    
    sm->notify_change(id_vector_t{"a"});
    auto cs1=o->find_changed_ts();      // there should be only one change
    FAST_CHECK_EQ(0u,o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(1u,cs1.size());
    FAST_CHECK_EQ(1u,cs1[p0].size());   // for this specific period,
    FAST_CHECK_EQ(string("a"),cs1[p0][0]); // and exactly equal to a
    
    sm->notify_change(id_vector_t{"c"});
    cs1=o->find_changed_ts();
    FAST_CHECK_EQ(0u,o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(1u,cs1.size());
    FAST_CHECK_EQ(1u,cs1[p1].size());   // for this specific period,
    FAST_CHECK_EQ(string("c"),cs1[p1][0]); // and exactly equal to c

    sm->notify_change(id_vector_t{"a","c","b"});
    cs1=o->find_changed_ts();
    FAST_CHECK_EQ(0u,o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(3u,cs1.size());
    
    FAST_CHECK_EQ(1u,cs1[p0].size());   // for this specific period,
    FAST_CHECK_EQ(string("a"),cs1[p0][0]); // and exactly equal to c

    FAST_CHECK_EQ(1u,cs1[p1].size());   // for this specific period,
    FAST_CHECK_EQ(string("c"),cs1[p1][0]); // and exactly equal to c

    FAST_CHECK_EQ(1u,cs1[p2].size());   // for this specific period,.. extended!
    FAST_CHECK_EQ(string("b"),cs1[p2][0]); // and exactly equal to b
    
    o->unsubscribe(id_vector_t{"b"}); // unsub one
    FAST_CHECK_EQ(1u,sm->total_unsubscribe_count.load()); // ensure we can detect this event

    sm->notify_change(id_vector_t{"a","c","b"});
    cs1=o->find_changed_ts();
    FAST_CHECK_EQ(0u,o->find_changed_ts().size()); // second call should return 0
    FAST_CHECK_EQ(2u,cs1.size());
    FAST_CHECK_EQ(1u,cs1[p0].size());   // for this specific period,
    FAST_CHECK_EQ(string("a"),cs1[p0][0]); // and exactly equal to c
    FAST_CHECK_EQ(1u,cs1[p1].size());   // for this specific period,
    FAST_CHECK_EQ(string("c"),cs1[p1][0]); // and exactly equal to c
    
    o->unsubscribe(id_vector_t{"a","b","c"}); // unsub all ++
    sm->notify_change(id_vector_t{"a","c","b","d"});
    cs1=o->find_changed_ts();
    FAST_CHECK_EQ(0u,cs1.size());
    FAST_CHECK_EQ(3u,sm->total_unsubscribe_count.load());// ensure we can detect unsubs
}

TEST_CASE("ts_sub_item"){
    ts_sub_item a(_p(2,4),"a");
    FAST_CHECK_EQ(a.p,_p(2,4));
    FAST_CHECK_EQ(a.id,"a");
    a.extend_period(_p(2,3));
    FAST_CHECK_EQ(a.p,_p(2,4));//the extend was within existing period
    a.extend_period(_p(3,6));
    FAST_CHECK_EQ(a.p,_p(2,6));//the extend increased upper
    a.extend_period(_p(1,2));
    FAST_CHECK_EQ(a.p,_p(1,6));//the extend lower bound
    a.extend_period(_p(0,7));
    FAST_CHECK_EQ(a.p,_p(0,7));// both upper and lower
    a.extend_period(_p(10,12));
    FAST_CHECK_EQ(a.p,_p(0,12));// handling extend with disjoint periods    
}

TEST_CASE("slave_mode_messages") {
    using namespace shyft::dtss;
    using dts_server=server;
    using shyft::dtss::ts_db;
    using shyft::time_series::ts_point_fx;
    dts_server s{};
    s.set_listening_ip("127.0.0.1");

    test::utils::temp_dir default_container("slave");
    string c2("c2"), c3("c3");
    s.add_container(c2,(default_container/c2).string());
    s.add_container(c3,(default_container/c3).string());

    auto port = s.start_server();
    //MESSAGE("Starting dtss server on port: " << port);
    //MESSAGE("Starting dtss client port: " << port);
    client c("127.0.0.1:" + std::to_string(port));

    constexpr auto check = [](auto const& v, auto const& c){
        return std::any_of(v.begin(), v.end(), [&c](auto const& e) {return e==c;});
    };

    //MESSAGE("Get container names on server");
    auto names = s.do_get_container_names();
    FAST_CHECK_UNARY(check(names, c2));
    FAST_CHECK_UNARY(check(names, c3));

    // Now test method from client side
    //MESSAGE("Get container names from client request");
    names = c.get_container_names();
    FAST_CHECK_UNARY(check(names, c2));
    FAST_CHECK_UNARY(check(names, c3));

    // Do slave read directly on "master" server
    gta_t ta(utctime(0),utctime(10),3);
    apoint_ts c2_a(ta,vector<double>{1,2,3},ts_point_fx::POINT_AVERAGE_VALUE);
    apoint_ts c3_a(ta,vector<double>{1,2,3},ts_point_fx::POINT_AVERAGE_VALUE);
    string c2_name = "shyft://c2/a";
    string c3_name = "shyft://c3/a";
    vector<apoint_ts> a_tsv{apoint_ts(c2_name, c2_a), apoint_ts(c3_name, c3_a)};
    ts_vector_t tsv{a_tsv};
    //MESSAGE("Store tsv to server");
    c.store_ts(tsv, true, true);
    //MESSAGE("Read tsv to server");
    auto r = c.read(id_vector_t{c2_name, c3_name}, ta.total_period(), true, true);
    ts_vector_t expected_r{vector<apoint_ts>{c2_a, c2_a}};
    FAST_CHECK_EQ(r.result, expected_r);

    // Subscribe
    //MESSAGE("Client read subscription");
    auto sub_r = c.read_subscription(); 
    FAST_CHECK_UNARY(sub_r.size() == 0u); // ensure that if there are no changes, we get zero back
    
    c2_a.set(0, 3);
    ts_vector_t tsv_2{vector<apoint_ts> {apoint_ts(c2_name, c2_a)}};
    c.store_ts(tsv_2, false, true); // now store something, that will change the stuff we are subscribing to.
    auto rr = c.read(id_vector_t{c3_name}, ta.total_period(), true, true);// read c3, but get c2 changes as piggyback!
    sub_r=rr.updates;// these are the updates piggybacked to the read response
    FAST_CHECK_EQ(sub_r.size(), 1u);
    FAST_CHECK_EQ(sub_r[0].id(), c2_name);
    FAST_CHECK_EQ(sub_r[0], c2_a);
    FAST_REQUIRE_EQ(rr.result.size(),1u);// just ensure we also got the stuff we tried to read.
    FAST_CHECK_EQ(rr.result[0],c3_a);

    sub_r = c.read_subscription();
    FAST_CHECK_EQ(sub_r.size(), 0u);


    c2_a.set(0, 4);
    c.store_ts(ts_vector_t {vector<apoint_ts> {apoint_ts(c2_name, c2_a)}}, false, true);
    sub_r = c.read_subscription();
    FAST_CHECK_UNARY(sub_r.size() == 1);
    //MESSAGE("Client unsubscribe");
    c.unsubscribe(vector<string>{c2_name});
    //MESSAGE("Before set");
    c2_a.set(0, 5);
    //MESSAGE("Client change and store ts");
    c.store_ts(ts_vector_t {vector<apoint_ts> {apoint_ts(c2_name, c2_a)}}, false, true);
    //MESSAGE("Client read");
    sub_r = c.read_subscription();
    FAST_CHECK_UNARY(sub_r.size() == 0);

    //MESSAGE("Closing client and server");
    c.close();
    s.clear();

}

TEST_CASE_FIXTURE(msync_setup,"master_slave_ops") {
    using namespace shyft::time_series;
 
    string fpattern{"shyft://test/a.*"};
    FAST_CHECK_EQ(0u,sc->find(fpattern).size()); // find should be redirected to master, and return 0
    
    // store some time_series so that we have something to work on.
        gta_t ta1(_t(0),_t(10),3);
        apoint_ts a1("shyft://test/a1");
        apoint_ts a2("shyft://test/a2");
        apoint_ts d1("dstm://M1/U1.production.schedule");
        apoint_ts a1_f1(ta1,vector<double>{1.0,2.0,3.0},stair_case);
        apoint_ts a2_f1(ta1,vector<double>{4.0,4.0,4.0},stair_case);
        apoint_ts d1_f1{gta_t{_t(0), _t(10),3},vector<double>{2.0,1.0,3.0},stair_case};
        
        ts_vector_t tsv1;
        tsv1.push_back(apoint_ts(a1.id(),a1_f1));
        tsv1.push_back(apoint_ts(a2.id(),a2_f1));
        sc->store_ts(tsv1,true,true);// overwrite, and cache it
        FAST_CHECK_EQ(2u,sc->find(fpattern).size()); // find should be redirected to master, and return 2
    
    // now read back the the timeseries through the slave dtss
        ts_vector_t e1;
        e1.push_back(a1);
        e1.push_back(a2);
        e1.push_back(d1);// also include some dstm:// items that are available locally only
        ts_vector_t r1=sc->evaluate(e1, ta1.total_period(),true,true,ta1.total_period());// read and cache it! (note: both at master and slave dtss)
        FAST_REQUIRE_EQ(3,r1.size());
        FAST_CHECK_EQ(r1[0],a1_f1);
        FAST_CHECK_EQ(r1[1],a2_f1);
        FAST_CHECK_EQ(r1[2],d1_f1);
        
        FAST_CHECK_EQ(2u,slave->msync->subs.size()); // we should have two subscriptions by now
        
    // verify get_ts_info
        auto tsi= sc->get_ts_info(a1.id());
        FAST_CHECK_EQ(tsi.name,"a1");
    
    // make changes for a1 at the server (without slave knowing about it!), and prove it propagates to the slave dtss
        ts_vector_t tsv2= vector<apoint_ts>{apoint_ts(a1.id(),a2_f1)};
        mc->store_ts(tsv2,false,true);// no overwrite, and cache it on the master server
        auto cs0=sc->get_cache_stats();// so that we can detect that we are hitting locally

        //wait for a while, to let the msync worker update it
        std::this_thread::sleep_for(_ms(30));// wait .. should be enough
        ts_vector_t r2=sc->evaluate(e1, ta1.total_period(),true,true,ta1.total_period());// read and cache it! (note: both at master and slave dtss)
        auto cs1=sc->get_cache_stats();// get cache stats after, we expect more hits.
        FAST_CHECK_GE(cs1.hits,cs0.hits);//
        FAST_REQUIRE_EQ(3,r2.size());
        FAST_CHECK_EQ(r2[0],a2_f1); //they should both be equal now, ref. storage.
        FAST_CHECK_EQ(r2[1],a2_f1);
        FAST_CHECK_EQ(r2[2],d1_f1);
    // flush slave cache, and verify that msync worker reduces the subs to minimum
        auto obs= make_shared<dtss::subscription::ts_observer>(slave->sm,"xtra");//
        obs->subscribe_to(id_vector_t{a2.id()},_p(0,10));// now we simulate that there is a sub for the a2.
        slave->flush_cache();// get rid of the cache, this should remove all need for a2 locally
        std::this_thread::sleep_for(_ms(300));// wait more than minium defer time 
        FAST_CHECK_EQ(1u,slave->msync->subs.size());
        obs->unsubscribe(id_vector_t{a2.id()});
        FAST_CHECK_EQ(0u,obs->terminals.size());
        FAST_CHECK_EQ(0u,obs->published_version.size());
        slave->flush_cache();// a1 could be in cache
        std::this_thread::sleep_for(_ms(300));// wait more than minium defer time
        FAST_CHECK_EQ(0u,slave->msync->subs.size()); // now there should be no need for timeseries
        obs.reset();//causes release of resources/subs
    // verify we can merge store
        sc->merge_store_ts(ts_vector_t{vector<apoint_ts>{apoint_ts{a2.id(),a1_f1}}},true);
    // verify we can remove stuff
        sc->remove(a1.id());
        FAST_CHECK_EQ(1u,sc->find(fpattern).size()); // find should be redirected to master, and return 1
    
}

TEST_CASE_FIXTURE(msync_setup,"master_slave_geo_ops") {
    using namespace shyft::time_series;
 
    string fpattern{"shyft://test/a.*"};
    string prefix="shyft://";
    string tc{"geo"};// important in our case, since we are using this as prefix for all geo stuff.
    geo::grid_spec gs{32633,{{0,0,0},{1000,0,1},{1000,1000,2},{0,1000,3}}}; // 4 points, a grid.
    vector<utctime> t0_times{};// initially empty 
    vector<string> variables {string("temperature"),string("precipitation")}; // two variables
    utctime fc_dt=from_seconds(2*24*3600);//forecast length 2x 24hours
    utctime max_fc_dt=fc_dt;
    int64_t n_ens=2;
    geo::ts_db_config_ gdb=make_shared<geo::ts_db_config>(
        prefix,tc,"a test geo container using shyft native time-series",gs,t0_times,max_fc_dt,n_ens,variables
    );
  //MESSAGE("add geo ts db");
    sc->add_geo_ts_db(gdb);
    
    auto t0_dt=from_seconds(24*3600);// new forecast everty 24 hour
    gta_t t0_ta{from_seconds(0),t0_dt,3};
    auto tsm1=test::create_forecast(*gdb,fc_dt,t0_ta);
    
  
  //MESSAGE("store to geo ts db");

    sc->geo_store(gdb->name,tsm1,true,false);// replace,and no cache
    auto p=gs.points[2];auto d=1;
    geo::query g_q1{gs.epsg,vector<geo_point>{{p.x-d,p.y-d,p.z},{p.x+d,p.y-d,p.z},{p.x,p.y+d,p.z}}};
    gta_t q1_ta{t0_ta.time(1),t0_dt,1};// pick the second forecast
    geo::eval_args q1{tc,vector<string>{variables[1]},vector<int64_t>{1},q1_ta,utctime{0},g_q1,false,utctime{0}};// pick precipitation,second geo-point, second ens, and second time.

  //MESSAGE("evaluate on geo ts db");
    
    auto r1=sc->geo_evaluate(q1,false,false);// read query 1.
  //MESSAGE("r1.shape, n_t0="<<r1.shape.n_t0<<",n_v="<<r1.shape.n_v<<",n_e="<<r1.shape.n_e<<",n_g="<<r1.shape.n_g<<"\n");
    auto xdb=sc->get_geo_ts_db_info()[0];
    auto s1=xdb->compute(q1);// compute the slice of the query
    //MESSAGE("s1., t="<<s1.t.size()<<",n_v="<<s1.v.size()<<",n_e="<<s1.e.size()<<",n_g="<<s1.g.size()<<"\n");
    FAST_CHECK_EQ(r1.shape,geo::detail::ix_calc{1,1,1,1});//verify we got exactly the right dim back.
    FAST_CHECK_EQ(r1.tsv.size(),1u);
    FAST_CHECK_EQ(r1.tsv[0].mid_point,p);// should hit exactly this point.
    auto s1_t0_ix=t0_ta.index_of(s1.t[0]);
    auto q1_ts= tsm1.ts(s1_t0_ix,s1.v[0],s1.e[0],s1.g[0]);// use the computed slice to get out the ts from the source
    FAST_CHECK_EQ(q1_ts.time_axis()==r1.tsv[0].ts.time_axis(),true);
    FAST_CHECK_EQ(q1_ts==r1.tsv[0].ts,true);
}

TEST_CASE_FIXTURE(msync_setup,"master_slave_repair_connections"*doctest::may_fail()*doctest::timeout(4.0)) {
    using namespace shyft::time_series;
    string fpattern{"shyft://test/a.*"};

    FAST_CHECK_EQ(0u,sc->find(fpattern).size()); // find should be redirected to master, and return 0

    // store some time_series so that we have something to work on.
    gta_t ta1(_t(0),_t(10),3);
    apoint_ts a1("shyft://test/a1");
    apoint_ts a2("shyft://test/a2");
    apoint_ts d1("dstm://M1/U1.production.schedule");
    apoint_ts a1_f1(ta1,vector<double>{1.0,2.0,3.0},stair_case);
    apoint_ts a2_f1(ta1,vector<double>{4.0,4.0,4.0},stair_case);
    apoint_ts d1_f1{gta_t{_t(0), _t(10),3},vector<double>{2.0,1.0,3.0},stair_case};

    ts_vector_t tsv1;
    tsv1.push_back(apoint_ts(a1.id(),a1_f1));
    tsv1.push_back(apoint_ts(a2.id(),a2_f1));
    sc->store_ts(tsv1,true,true);// overwrite, and cache it
    FAST_CHECK_EQ(2u,sc->find(fpattern).size()); // find should be redirected to master, and return 2
    ts_vector_t e1;
    e1.push_back(a1);
    e1.push_back(a2);
    e1.push_back(d1);// also include some dstm:// items that are available locally only
    ts_vector_t r1=sc->evaluate(e1, ta1.total_period(),true,true,ta1.total_period());// read and cache it! (note: both at master and slave dtss)
    FAST_REQUIRE_EQ(3,r1.size());
    FAST_CHECK_EQ(r1[0],a1_f1);
    FAST_CHECK_EQ(r1[1],a2_f1);
    FAST_CHECK_EQ(r1[2],d1_f1);

    FAST_CHECK_EQ(2u,slave->msync->subs.size()); // we should have two subscriptions by now
    //-- now break the connection to the master,
    //// forcing the master to stop
    MESSAGE("stop dtss.master");
    //master->set_graceful_close_timeout(2);
    master->clear();// now it stops communciation
    std::this_thread::sleep_for(_ms(500));// wait more than minium defer time
    master->set_listening_ip(local_only);
    master->set_listening_port(master_port_no);
    MESSAGE("restart dtss.master@"<<master_port_no<<" is running "<<master->is_running());
    auto unsafe_start= std::async(std::launch::async,[&](){master->start_server();});///master->start_server();
    if( unsafe_start.wait_for(_ms(1500))==std::future_status::ready) {
        std::this_thread::sleep_for(_ms(500));// wait more than minium defer time
        MESSAGE("dtss.master, test it's reconnected..");
        if(master->alive_connections != 1) { // bail out, avoid hang.
            MESSAGE("test hickup, bail out");
            sc->close();
            //mc->clear();
            master->clear();
        }
        FAST_REQUIRE_EQ((std::size_t)1,(std::size_t)master->alive_connections); // Note: Casting from atomic_size_t to size_t avoids error "undefined reference to symbol quadmath_snprintf" without having to link with quadmath (it is related to Boost.Math and its Float128 feature) on Arch Linux with GCC 12.2 and Boost 1.80 (not sure what triggers it)
        tsv1.clear();
        tsv1.push_back(apoint_ts(a1.id(),a2_f1));
        tsv1.push_back(apoint_ts(a2.id(),a1_f1));//flip content
        mc->store_ts(tsv1,false,true);// update, and cache it, at the master..
        std::this_thread::sleep_for(_ms(100));// wait more than minium defer time, allow poll master to run
        // then read
        r1=sc->evaluate(e1, ta1.total_period(),true,true,ta1.total_period());// read and cache it! (note: both at master and slave dtss)
        FAST_REQUIRE_EQ(3,r1.size());
        FAST_CHECK_EQ(r1[0],a2_f1);
        FAST_CHECK_EQ(r1[1],a1_f1);
        FAST_CHECK_EQ(r1[2],d1_f1);
    } else {
        MESSAGE("dtss failed to start due to some issues related to dlib and ports available");
    }
}

}
