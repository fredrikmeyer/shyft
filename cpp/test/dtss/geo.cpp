#include "test_pch.h"
#include <boost/algorithm/string/predicate.hpp>
#include <shyft/dtss/geo.h>
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/web_api_generator.h>
#include <shyft/dtss/dtss.h>
#include <shyft/dtss/dtss_client.h>

#include <test/web_api/test_parser.h>
#include <test/energy_market/serialize_loop.h>
#include <test/test_utils.h>
using namespace shyft::core;
using namespace shyft::dtss;
using namespace shyft;
using namespace std;
using shyft::web_api::grammar::geo_ts_url_grammar;    
using shyft::web_api::generator::geo_ts_url_generator;
using geo_ts_id=shyft::dtss::geo::ts_id;
using shyft::time_series::dd::gta_t;
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::ts_point_fx;
namespace test {
    geo::ts_matrix create_forecast(geo::ts_db_config const&cfg,utctime fc_length,gta_t const&ta) {
        auto r=cfg.create_ts_matrix_t0(ta.size());
        utctime fc_dt=from_seconds(3600);
        size_t n_fc_steps=fc_length/fc_dt;
        //calendar utc;
        for(size_t t=0;t<r.shape.n_t0;++t) {
            gta_t fc_ta(ta.time(t),fc_dt,n_fc_steps);
            for(size_t v=0;v<r.shape.n_v;v++) {
                for(size_t e=0;e<r.shape.n_e;++e) {
                    for(size_t g=0;g<r.shape.n_g;++g) {
                        vector<double> vv(n_fc_steps, double(g*1000.0+ t*100.0+e*10+v));
                        r._set_ts(t,v,e,g,apoint_ts(fc_ta,vv,ts_point_fx::POINT_AVERAGE_VALUE));
                        //MESSAGE("geo_ts/"<<t<<"/"<<v<<"/"<<e<<"/"<<g<<": t="<<utc.to_string(fc_ta.time(0))<<",n="<<n_fc_steps);
                    }
                }
            }
        }
        return r;
    }
}

TEST_SUITE("dtss_geo") {

TEST_CASE("geo_ts_url_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    geo_ts_id tidx{"arome",1,2,3,from_seconds(4)};
 
    auto ok=generate(sink,geo_ts_url_generator<decltype(sink)>(),tidx);
    CHECK_EQ(true,ok);
    CHECK_EQ(ps,"geo://arome/1/2/3/4");
    SUBCASE("geo_ts_url_gen_speed") {
        size_t n=1000*1000;
        //vector<string> urls;urls.reserve(n);
        geo_ts_id tidx{"t",1,2000,100,utctime_now()};
        //char b[100];// works, but does not get it faster, for test, assume db.name + generated ints is less than 100
        string ps;ps.reserve(100);
        auto  sink=std::back_inserter(ps);
        geo_ts_url_generator<decltype(sink)> geo_ts_url_;
        auto t0=utctime_now();
        for(size_t i=0;i<n;++i){
            ps.resize(0);// reset string for each url
            boost::spirit::karma::generate(sink,geo_ts_url_,tidx);
            //faster than this: sprintf(b,"geo://%s/%zu/%zu/%zu/%ld",tidx.geo_db.c_str(),tidx.v,tidx.g,tidx.e,tidx.secs());
            //urls.emplace_back(ps);// this also takes time, so we leave it out, to get the clean generator speed
        }
        auto t_used= utctime_now()-t0;
        //CHECK_EQ(urls.size(),n);
        double g_speed=double(n)/to_seconds(t_used)/1e6;
        MESSAGE("geo_ts_url_gen speed :"<<g_speed<<" Mega[urls/s]");
    }
}

TEST_CASE("geo_ts_url_grammar") {
    geo_ts_url_grammar<const char*> geo_ts_url_grammar_;
    geo_ts_id a;
    auto ok= test::phrase_parser("geo://arome/1/2/3/4",geo_ts_url_grammar_,a);
    CHECK_EQ(true,ok);
    CHECK_EQ(a,geo_ts_id{"arome",1,2,3,from_seconds(4)});
    SUBCASE("geo_ts_url_parser_speed") {
        size_t n=1000*1000;
        auto t0=utctime_now();
        for(size_t i=0;i<n;++i)
            test::phrase_parser("geo://arome/1/2/3/4",geo_ts_url_grammar_,a);
        double p_speed= double(n)/to_seconds(utctime_now()-t0)/1e6;
        MESSAGE("geo_ts_url_parser speed :"<<p_speed<<" Mega[urls/s]");
    }
}
TEST_CASE("geo_query") {
    geo::query a;
    FAST_CHECK_EQ(a.polygon.size(),0);
    FAST_CHECK_EQ(a.epsg,0);
    geo::query b;
    FAST_CHECK_EQ(a,b);
    b.epsg=32333;
    FAST_CHECK_NE(a,b);
    b.polygon.emplace_back(1.0,2.0,3.0);
    a.polygon.emplace_back(1.0,2.0,3.0);
    a.epsg=32333;
    FAST_CHECK_EQ(a,b);
    a.polygon.emplace_back(3.0,4.0,5.0);
    a.polygon.emplace_back(1.0,1.0,6.0);
    FAST_CHECK_NE(a,b);
    geo::query c{32333,{{1.0,2.0,3.0},{3.0,4.0,5.0},{1.0,1.0,6.0}}};
    FAST_CHECK_EQ(a,c);
    auto fx=[](){geo::query x{32333,{{1.0,2.0,3.0},{1.0,4.0,3.0}}};};
    CHECK_THROWS_AS(fx(),std::runtime_error );
    auto gx=[](){geo::query x{-32333,{{1.0,2.0,3.0},{0.0,0.0,0.0},{1.0,4.0,3.0}}};};
    CHECK_THROWS_AS(gx(),std::runtime_error );
    auto d=test::serialize_loop(c);
    FAST_CHECK_EQ(c,d);
}
TEST_CASE("geo_grid_spec") {
    geo::grid_spec a;
    FAST_CHECK_EQ(a.points.size(),0);
    FAST_CHECK_EQ(a.epsg,0);
    geo::grid_spec b;
    FAST_CHECK_EQ(a,b);
    b.epsg=32333;
    FAST_CHECK_NE(a,b);
    b.points.emplace_back(1.0,2.0,3.0);
    a.points.emplace_back(1.0,2.0,3.0);
    a.epsg=32333;
    FAST_CHECK_EQ(a,b);
    a.points.emplace_back(3.0,4.0,5.0);
    a.points.emplace_back(1.0,1.0,6.0);
    FAST_CHECK_NE(a,b);
    geo::grid_spec c{32333,{{1.0,2.0,3.0},{3.0,4.0,5.0},{1.0,1.0,6.0}}};
    FAST_CHECK_EQ(a,c);
    auto fx=[](){geo::query x{-32333,{{1.0,2.0,3.0},{1.0,4.0,3.0}}};};
    CHECK_THROWS_AS(fx(),std::runtime_error );
    auto d=test::serialize_loop(c);
    FAST_CHECK_EQ(c,d);
}
TEST_CASE("geo_grid_spec_find_geo_match_ix") {
    geo::grid_spec a{32633,{{0.0,0.0,0.0},{1000.0,0.0,1.0},{1000.0,1000.0,2.0},{0.0,1000.0,3.0}}};// a non-closed square polygon
    
    auto r0=a.find_geo_match_ix(geo::query{});// should return all
    FAST_CHECK_EQ(r0,geo::ix_vector{0,1,2,3});
    FAST_CHECK_EQ(a.find_geo_match_ix(geo::query{32633,{{0,0,0},{500,0,0},{500,1000,0},{0,1000,0}}} ),geo::ix_vector{0,3});
    FAST_CHECK_EQ(a.find_geo_match_ix(geo::query{32633,{{0,0,0},{500,0,0},{500,999,0},{0,999,0}}} ),geo::ix_vector{0});
    FAST_CHECK_EQ(a.find_geo_match_ix(geo::query{32633,{{1,0,0},{500,0,0},{500,999,0},{1,999,0}}} ),geo::ix_vector{});
    
}
TEST_CASE("geo_eval_args") {
    geo::eval_args a;
    FAST_CHECK_EQ(a.geo_ts_db_id.size(),0);
    FAST_CHECK_EQ(a.variables.size(),0);
    FAST_CHECK_EQ(a.ens.size(),0);
    FAST_CHECK_EQ(a.ta.size(),0);
    FAST_CHECK_EQ(a.cc_dt0,utctime(0));
    FAST_CHECK_EQ(a.concat,false);
    SUBCASE("geo_eval_args_eq") {
        geo::eval_args b;
        FAST_CHECK_EQ(a,b);
        a.geo_ts_db_id="a";FAST_CHECK_NE(a,b);a.geo_ts_db_id="";
        a.variables.push_back("a");FAST_CHECK_NE(a,b);a.variables.clear();
        a.ens.push_back(1);FAST_CHECK_NE(a,b);a.ens.clear();
        a.ta=gta_t{utctime(0),utctime(10),10};FAST_CHECK_NE(a,b);a.ta=gta_t{};
        a.geo_range.epsg=32633;FAST_CHECK_NE(a,b);a.geo_range.epsg=b.geo_range.epsg;
        a.cc_dt0=utctime(1);FAST_CHECK_NE(a,b);a.cc_dt0=utctime(0);
        a.concat=true;FAST_CHECK_NE(a,b);a.concat=false;
    }
    geo::query q{32633,{{0,0,0},{100,0,0},{100,100,0}}};
    vector<string> vars{ string("t"),string("p")};
    gta_t ta{from_seconds(0),from_seconds(3600),10};
    vector<int64_t> ens{0,2};
    auto cc_dt0=from_seconds(3600);
    string db_id("a");
    geo::eval_args c{db_id,vars,ens,ta,from_seconds(1),q,true,cc_dt0};
    FAST_CHECK_EQ(c.geo_ts_db_id,db_id);
    FAST_CHECK_EQ(c.variables,vars);
    FAST_CHECK_EQ(c.ens,ens);
    FAST_CHECK_EQ(c.ta,ta);
    FAST_CHECK_EQ(c.geo_range,q);
    FAST_CHECK_EQ(c.cc_dt0,cc_dt0);
    FAST_CHECK_EQ(c.concat,true);
    FAST_CHECK_EQ(c.ts_dt,from_seconds(1));
    auto d=test::serialize_loop(c);
    FAST_CHECK_EQ(c,d);
}
TEST_CASE("geo_slice") {
    geo::slice a;
    FAST_CHECK_EQ(a.size(),0);
    FAST_CHECK_EQ(a.v.size(),0);
    FAST_CHECK_EQ(a.g.size(),0);
    FAST_CHECK_EQ(a.e.size(),0);
    FAST_CHECK_EQ(a.t.size(),0);
    a.v.push_back(1);
    a.g.push_back(0);a.g.push_back(1);
    a.e.push_back(0);a.e.push_back(2);a.e.push_back(3);
    a.t.emplace_back(0);a.t.emplace_back(2),a.t.emplace_back(3);a.t.emplace_back(4);
    FAST_CHECK_EQ(a.size(),1*2*3*4);
    //auto d=test::serialize_loop(a);
    //FAST_CHECK_EQ(a,d);
    a.size_to_zero();
    FAST_CHECK_EQ(a.size(),0);
}

TEST_CASE("geo_ts_db_config") {
    geo::ts_db_config a;
    FAST_CHECK_EQ(a.prefix.size(),0);
    FAST_CHECK_EQ(a.name.size(),0);
    FAST_CHECK_EQ(a.descr.size(),0);
    FAST_CHECK_EQ(a.grid,geo::grid_spec{});
    FAST_CHECK_EQ(a.variables.size(),0);
    FAST_CHECK_EQ(a.n_ensembles,0);
    FAST_CHECK_EQ(a.t0_times.size(),0);
    FAST_CHECK_EQ(a.dt,utctime(0));
    FAST_CHECK_EQ(a.t0_time_axis(),gta_t{});
    FAST_CHECK_EQ(a.find_geo_match_ix(geo::query{}).size(),0);
    // very eq
    geo::ts_db_config b;
    FAST_CHECK_EQ(a,b);
    b.prefix="a://";FAST_CHECK_NE(a,b);b.prefix="";
    b.name="a";FAST_CHECK_NE(a,b);b.name="";
    b.descr="a";FAST_CHECK_NE(a,b);b.descr="";
    b.grid.epsg=32633;FAST_CHECK_NE(a,b);b.grid.epsg=0;
    b.t0_times.emplace_back(123);FAST_CHECK_NE(a,b);b.t0_times.clear();
    b.dt=utctime(123);FAST_CHECK_NE(a,b);b.dt=utctime(0);
    b.n_ensembles=3;FAST_CHECK_NE(a,b);b.n_ensembles=0;
    b.variables.push_back("a");FAST_CHECK_NE(a,b);b.variables.clear();
    geo::grid_spec gs{32633,{{0,0,0},{1000,0,1},{1000,1000,2},{0,1000,3}}};
    vector<utctime> t0{from_seconds(0),from_seconds(3600),from_seconds(2*3600)};
    vector<string> vars{"t","p","rh","ws","rad"};
    // verify ct
    geo::ts_db_config c{"geo://","name","descr",gs,t0,from_seconds(3600*3),3,vars};
    FAST_CHECK_EQ(c.name,"name");
    FAST_CHECK_EQ(c.descr,"descr");
    FAST_CHECK_EQ(c.grid,gs);
    FAST_CHECK_EQ(c.variables,vars);
    FAST_CHECK_EQ(c.n_ensembles,3);
    FAST_CHECK_EQ(c.t0_times.size(),3);
    FAST_CHECK_EQ(c.dt,from_seconds(3*3600));
    FAST_CHECK_EQ(c.t0_time_axis(),gta_t{t0,t0.back()+c.dt});
    // verify fx
    geo::query q2{32633,{{0,0,0},{500,0,0},{500,1000,0},{0,1000,0}}};
    FAST_CHECK_EQ(c.find_geo_match_ix(q2),geo::ix_vector{0,3});
    FAST_CHECK_EQ(t0[0], c.get_associated_t0(t0[0]+from_seconds(3),true));
    FAST_CHECK_EQ(t0[0]-from_seconds(3), c.get_associated_t0(t0[0]-from_seconds(3),true));
    FAST_CHECK_EQ(t0[0]+from_seconds(3), c.get_associated_t0(t0[0]+from_seconds(3),false));
    FAST_CHECK_EQ(t0[1], c.get_associated_t0(t0[1],true));
    FAST_CHECK_EQ(t0[2], c.get_associated_t0(t0[2]+from_seconds(100*3600),true));
    
    gta_t rta{t0[0],from_seconds(3600),2};
    geo::eval_args ea{"name",vector<string>{"p","rh"},vector<int64_t>{0,1,2},rta,from_seconds(0),q2,false,utctime(0)};
    auto slice_ea=c.compute(ea);
    FAST_CHECK_EQ(slice_ea,geo::slice{geo::ix_vector{1,2},{0,3},{0,1,2},{from_seconds(0),from_seconds(3600)},c.dt});
    auto rv=c.create_ts_matrix(slice_ea);
    FAST_CHECK_EQ(rv.shape.n_t0,ea.ta.size());// first dim 2 t0
    FAST_CHECK_EQ(rv.shape.n_v,2);// 2 variables
    FAST_CHECK_EQ(rv.shape.n_e,3);// 3 n_ensembles
    FAST_CHECK_EQ(rv.shape.n_g,2);// 2 geo_points
    SUBCASE("geo_cfg_bounding_box") {
        auto bbox=c.bounding_box(slice_ea);
        FAST_CHECK_EQ(bbox[0],geo_point(0,0,0));
        FAST_CHECK_EQ(bbox[1],geo_point(0,1000,3));
    }
    auto d=test::serialize_loop(c);
    FAST_CHECK_EQ(c,d);

    SUBCASE("geo_cfg_convex_hull") {
    // Query polygon covers three points in the db, excludes the point at ix 3 (0,1000).
    geo::query ch_q{32633,{{0,0,0},{1000,0,0},{1000,1000,0},{500,100}}};
    geo::eval_args ch_ea{"name",vector<string>{"p","rh"},vector<int64_t>{0,1,2},rta,from_seconds(0),ch_q,false,utctime(0)};
    auto ch_slice=c.compute(ch_ea);
    auto hull=c.convex_hull(ch_slice);
    FAST_CHECK_EQ(hull.size(),4);
    FAST_CHECK_EQ(hull[0],geo_point(0,0));
    FAST_CHECK_EQ(hull[1],geo_point(1000,1000));
    FAST_CHECK_EQ(hull[2],geo_point(1000,0));
    FAST_CHECK_EQ(hull[3],geo_point(0,0));

    // Query polygon contains two points in the db. Convex hull degenerates to a line.
    geo::query ch_q2{32633,{{0,0,0},{1000,0,0},{1000,500,0}}};
    geo::eval_args ch_ea2{"name",vector<string>{"p","rh"},vector<int64_t>{0,1,2},rta,from_seconds(0),ch_q2,false,utctime(0)};
    auto ch_slice2=c.compute(ch_ea2);
    auto hull2=c.convex_hull(ch_slice2);
    FAST_CHECK_EQ(hull2.size(),4); // boost geometry anyway returns 4 points
    FAST_CHECK_EQ(hull2[0],geo_point(0,0));
    FAST_CHECK_EQ(hull2[1],geo_point(1000,0));
    FAST_CHECK_EQ(hull2[2],geo_point(0,0));
    FAST_CHECK_EQ(hull2[3],geo_point(0,0));

    // Query polygon contains a single point in the db. Convex hull degenerates to a point.
    geo::query ch_q3{32633,{{0,0,0},{500,0,0},{500,500,0}}};
    geo::eval_args ch_ea3{"name",vector<string>{"p","rh"},vector<int64_t>{0,1,2},rta,from_seconds(0),ch_q3,false,utctime(0)};
    auto ch_slice3=c.compute(ch_ea3);
    auto hull3=c.convex_hull(ch_slice3);
    FAST_CHECK_EQ(hull3.size(),4);  // boost geometry anyway returns 4 points
    FAST_CHECK_EQ(hull3[0],geo_point(0,0));
    FAST_CHECK_EQ(hull3[1],geo_point(0,0));
    FAST_CHECK_EQ(hull3[2],geo_point(0,0));
    FAST_CHECK_EQ(hull3[3],geo_point(0,0));

    // Query polygon contains no point in the db. Convex hull is invalid
    geo::query ch_q4{32633,{{-1,-1,0},{-500,0,0},{-500,-500,0}}};
    geo::eval_args ch_ea4{"name",vector<string>{"p","rh"},vector<int64_t>{0,1,2},rta,from_seconds(0),ch_q4,false,utctime(0)};
    auto ch_slice4=c.compute(ch_ea4);
    auto hull4=c.convex_hull(ch_slice4);
    FAST_CHECK_EQ(hull4.size(),0);

    
    }

}
TEST_CASE("geo_ts_id") {
    geo::ts_id a;
    FAST_CHECK_EQ(a.geo_db.size(),0);
    FAST_CHECK_EQ(a.v,0u);
    FAST_CHECK_EQ(a.g,0u);
    FAST_CHECK_EQ(a.e,0u);
    FAST_CHECK_EQ(a.t,no_utctime);
    geo::ts_id b;
    FAST_CHECK_EQ(a,b);
    b.geo_db="a";FAST_CHECK_NE(a,b);b.geo_db="";
    b.v=1u;FAST_CHECK_NE(a,b);b.v=0u;
    b.g=1u;FAST_CHECK_NE(a,b);b.g=0u;
    b.e=1u;FAST_CHECK_NE(a,b);b.e=0u;
    b.t=utctime(1);FAST_CHECK_NE(a,b);b.t=no_utctime;
    b.t=from_seconds(3600);
    FAST_CHECK_EQ(b.secs(),3600);
}
TEST_CASE("geo_detail_ix_calc") {
    geo::detail::ix_calc c{1,2,3,4};//t0,v,e,g dims
    FAST_CHECK_EQ(c.size(),1*2*3*4);
    FAST_CHECK_EQ(c.flat(0,1,2,3),c.size()-1);
    FAST_CHECK_EQ(c.flat(0,0,0,0),0);
    FAST_CHECK_EQ(c.flat(0,0,1,2),c.n_v*c.n_e*c.n_g*0+c.n_e*c.n_g*0 +c.n_g*1 + 2);
    FAST_CHECK_EQ(c.ix(0),geo::detail::geo_ix{0,0,0,0});
    FAST_CHECK_EQ(c.ix(1),geo::detail::geo_ix{0,0,0,1});
    FAST_CHECK_EQ(c.ix(2),geo::detail::geo_ix{0,0,0,2});
    FAST_CHECK_EQ(c.ix(3),geo::detail::geo_ix{0,0,0,3});
    FAST_CHECK_EQ(c.ix(5),geo::detail::geo_ix{0,0,1,1});
    FAST_CHECK_EQ(c.ix(6),geo::detail::geo_ix{0,0,1,2});
    FAST_CHECK_EQ(c.ix(12),geo::detail::geo_ix{0,1,0,0});
    FAST_CHECK_EQ(c.ix(12*2),geo::detail::geo_ix{1,0,0,0});
    auto d=test::serialize_loop(c);
    FAST_CHECK_EQ(c,d);

}
TEST_CASE("geo_ts_matrix") {

    geo::ts_matrix a;
    FAST_CHECK_EQ(a.shape.size(),0);
    FAST_CHECK_EQ(a,geo::ts_matrix{});
    geo::ts_matrix b(1,1,1,1);
    FAST_CHECK_EQ(b.shape.size(),1);
    FAST_CHECK_NE(a,b);
    apoint_ts ts1{gta_t{from_seconds(0),from_seconds(10),1},1.0,ts_point_fx::POINT_AVERAGE_VALUE};
    b.set_ts(0,0,0,0,ts1);
    auto const &ts2=b.ts(0,0,0,0);
    FAST_CHECK_EQ(ts1,ts2);
    auto d=test::serialize_loop(b);
    FAST_CHECK_EQ(b,d);
    
}
TEST_CASE("geo_geo_ts_matrix") {

    geo::geo_ts_matrix a;
    FAST_CHECK_EQ(a.shape.size(),0);
    FAST_CHECK_EQ(a,geo::geo_ts_matrix{});
    geo::geo_ts_matrix b(1,1,1,1);
    FAST_CHECK_EQ(b.shape.size(),1);
    FAST_CHECK_NE(a,b);
    apoint_ts ts1{gta_t{from_seconds(0),from_seconds(10),1},1.0,ts_point_fx::POINT_AVERAGE_VALUE};
    b.set_ts(0,0,0,0,ts1);
    auto const &ts2=b.ts(0,0,0,0);
    FAST_CHECK_EQ(ts1,ts2);
    geo_point p1{1,2,3};
    b.set_geo_point(0,0,0,0,p1);
    geo_point p2=b.get_geo_point(0,0,0,0);
    FAST_CHECK_EQ(p1,p2);
    auto d=test::serialize_loop(b);
    FAST_CHECK_EQ(b,d);
}
TEST_CASE("geo_ts_matrix_concatenate") {
    utctime fc_dt=from_seconds(6*3600);
    utctime dt=from_seconds(3600);
    size_t n_fc_steps=36;
    gta_t fc_ta(from_seconds(0),fc_dt,2);
    geo::geo_ts_matrix r(fc_ta.size(),1,1,1); // Might be to simple, we could/should add 2,3,4 to ensure dimensions are done right

    //Create a set of overlapping point time axis
    std::vector<std::vector<utctime>> fc_point_values; fc_point_values.reserve(fc_ta.size());
    std::vector<gta_t> fc_point_ta; fc_point_ta.reserve(fc_ta.size());
    for(size_t t=0;t<fc_ta.size();++t) {
	std::vector<utctime> tvals; tvals.reserve(n_fc_steps);
	// Create sequence with steps increasing exponentially from dt and upwards to ~dt+3.5hrs
	for(size_t i=0;i<n_fc_steps;++i) tvals.push_back(fc_ta.time(t)+i*dt+from_seconds(i*i*10));
	utctime last_time_point = tvals[n_fc_steps-1]+from_seconds(1);
	gta_t ta(tvals, last_time_point);  // Create time axis with 1s last interval
	fc_point_ta.push_back(ta);
	tvals.push_back(last_time_point);
	fc_point_values.push_back(tvals);  // Keep last time point to ease assertion later
    }

    for(size_t ta_type_ctr=0;ta_type_ctr<2;++ta_type_ctr) {
	// run with fixed_ta when ta_type_ctr == 0, else point_ta
	// create start matrix
	for(size_t t=0;t<fc_ta.size();++t) {
	    gta_t ts_ta = ta_type_ctr == 0 ? gta_t(fc_ta.time(t),dt,n_fc_steps) : fc_point_ta[t];
	    for(size_t v=0;v<r.shape.n_v;v++) {
		for(size_t e=0;e<r.shape.n_e;++e) {
		    for(size_t g=0;g<r.shape.n_g;++g) {
			vector<double> vv(n_fc_steps, double(g*1000.0+ t*100.0+e*10+v));
			r._set_ts(t,v,e,g,apoint_ts(ts_ta,vv,ts_point_fx::POINT_AVERAGE_VALUE));
			r._set_geo_point(t,v,e,g,geo_point(1,2,3));
			//MESSAGE("geo_ts/"<<t<<"/"<<v<<"/"<<e<<"/"<<g<<": t="<<utc.to_string(ts_ta.time(0))<<",n="<<n_fc_steps);
		    }
		}
	    }
	}
	//MESSAGE("About to do concat..");
	auto b=r.concatenate(utctime(0),fc_dt);
	FAST_CHECK_EQ(b.shape.n_t0,1);

	FAST_CHECK_EQ(b.ts(0,0,0,0).time_axis().size(),6+n_fc_steps);
	if (ta_type_ctr == 0)
	    FAST_CHECK_EQ(b.ts(0,0,0,0).time_axis(),gta_t(from_seconds(0),dt,6+n_fc_steps));
	else {
	    // Create manual concat of raw time axis
	    vector<utctime> expected_points; expected_points.reserve(6+n_fc_steps);
	    for(auto i=0; i<6; ++i) expected_points.push_back(fc_point_values[0][i]);
	    for(auto i=0u; i<n_fc_steps+1; ++i) expected_points.push_back(fc_point_values[1][i]);
	    auto expected_ta = ta::point_dt(expected_points);
	    FAST_CHECK_EQ(b.ts(0,0,0,0).time_axis().p, expected_ta);
	}
	FAST_CHECK_EQ(b.ts(0,0,0,0).value(0),doctest::Approx(0.0));
	FAST_CHECK_EQ(b.ts(0,0,0,0).value(6),doctest::Approx(100.0));
	FAST_CHECK_EQ(b.get_geo_point(0,0,0,0),geo_point(1,2,3));
    }
}
TEST_CASE("dtss_geo_find_cfg") {
    test::utils::temp_dir root_dir("shyft.geo_find_cfg");
    fs::path root(root_dir);
    
    auto mk_cfg=[](fs::path p, string sub_dir, string /*cfg_file_name*/) {
        auto geo_dir=p/sub_dir;
        if(!fs::exists(geo_dir))
            fs::create_directory(geo_dir);
        auto cfg_file= geo_dir/server::geo_cfg_file;
        ofstream o(cfg_file.c_str(),std::ios::binary);
        auto cfg = make_shared<geo::ts_db_config>();
        cfg->name=sub_dir;
        core_oarchive oa(o,core_arch_flags);
        oa<<core_nvp("geo_cfg",cfg);
    };
    
    fs::create_directory(root);
    server srv{};
    srv.add_container("",root.string());
    string geo1("g1"),geo2("g2");
    mk_cfg(root,geo1,server::geo_cfg_file);
    mk_cfg(root,geo2,server::geo_cfg_file);
    fs::create_directory(root/string("tsdb"));
    fs::create_directory(root/string("tsdb")/string("some_sub_dir"));
    auto geo_cfg_1=srv.geo_ts_db_scan(root/geo1);
    FAST_CHECK_EQ((*geo_cfg_1)->name, geo1);
    auto geo_cfg_2=srv.geo_ts_db_scan(root/geo2);
    FAST_CHECK_EQ((*geo_cfg_2)->name, geo2);
}

TEST_CASE("dtss_geo_basics") { /*
    This test simply create and host a dtss on auto-port,
    then uses shyft:// prefix to test
    all internal operations that involve mapping to the
    shyft geo ts-db requests.
    e.g.: we map geo://something to shyft://something
    in the geo-read callback
    
    */
    using namespace shyft::dtss;
    using namespace shyft::time_series::dd;
    using shyft::time_series::point_ts;
    using shyft::time_series::ts_point_fx;

    auto utc=make_shared<calendar>();
    for(size_t prefix_test=0;prefix_test<2;++prefix_test) {
        // make dtss server
        string prefix=prefix_test==0?"shyft://":"extgeo://";
        bool is_internal_db = prefix=="shyft://";
        test::utils::temp_dir tmpdir(prefix_test?"shyft.dtss_geo.internal.":"shyft.dtss_geo.extgeo.");
        server geo_dtss{};
        vector<geo::slice> slice_requested;
        vector<geo::ts_matrix> store_cb_tsm;
        vector<bool> store_cb_replace;
        //-- in this case there are only two needed callbacks.
        //   here 
        geo_dtss.geo_read_cb=[&geo_dtss,&slice_requested](geo::ts_db_config_ const &cfg,geo::slice const&gs)->geo::ts_matrix {

            geo::ts_matrix r=cfg->create_ts_matrix(gs);// create the result.
            string ts_path;ts_path.reserve(100);// we will regenerate this mill times, avoid malloc between
            auto  sink=std::back_inserter(ts_path);
            geo_ts_id tidx{cfg->name,0,0,0,from_seconds(0)};//reuse this in loop, reset ts_path
            geo_ts_url_generator<decltype(sink)> url_gen("");//drop the prefix, we want naked urls in this case
            its_db &ts_db=geo_dtss.internal(cfg->name);// assume it succeeds here
            calendar utc;
            auto ts_dt = gs.ts_dt>utctime(0)?gs.ts_dt:cfg->dt;
            for(size_t i=0;i<gs.t.size();++i) {
                utcperiod read_period{gs.t[i],gs.t[i]+ts_dt};
                // v,e,g dimensions share read-period
                tidx.t=gs.t[i];
                for(size_t v=0;v<gs.v.size();++v) {
                    tidx.v=v;
                    for(size_t e=0;e<gs.e.size();++e) {
                        tidx.e=e;
                        for(size_t g=0;g<gs.g.size();++g) {
                            ts_path.resize(0);// reset to 0, keep mem
                            tidx.g=g;
                            generate(std::back_inserter(ts_path),url_gen,tidx);
                            apoint_ts rts(
                                make_shared<const gpoint_ts>(ts_db.read(ts_path,read_period))
                            );
                            //MESSAGE("geo_read/"<<i<<"/"<<v<<"/"<<e<<"/"<<g<<": rp="<<utc.to_string(read_period)<<", n="<<rts.size());
                            r.set_ts(i,v,e,g,rts);
                        }
                    }
                }
            }
            slice_requested.push_back(gs);
            return r;
        };
        // store callback
        geo_dtss.geo_store_cb=[&geo_dtss,&store_cb_tsm,&store_cb_replace](geo::ts_db_config_ cfg, geo::ts_matrix const&tsm,bool replace)->void {
            string ts_path;ts_path.reserve(100);
            auto  sink=std::back_inserter(ts_path);
            geo_ts_id tidx{cfg->name,0,0,0,from_seconds(0)};//reuse this in loop, reset ts_path
            geo_ts_url_generator<decltype(sink)> url_gen("");//drop the prefix, we want naked urls in this case
            its_db & ts_db=geo_dtss.internal(cfg->name);//kept ref. hmm. could consider shared something here
            calendar utc;
            for(size_t i=0;i<tsm.shape.n_t0;++i) {
                for(size_t v=0;v<tsm.shape.n_v;++v) {
                    tidx.v=v;
                    for(size_t e=0;e<tsm.shape.n_e;++e) {
                        tidx.e=e;
                        for(size_t g=0;g<tsm.shape.n_g;++g) {
                            ts_path.resize(0);// reset to 0, keep mem
                            tidx.g=g;
                            apoint_ts const &ts=tsm._ts(i,v,e,g);
                            auto gts = dynamic_cast<gpoint_ts const *>(ts.ts.get());
                            tidx.t= cfg->get_associated_t0(ts.time(0),!replace); // concat=!replace pr. def
                            generate(std::back_inserter(ts_path),url_gen,tidx);
                            //MESSAGE("geo_db."<<cfg->name<<".save("<<ts_path<<",gts?"<<(gts?"Yes":"no")<<" n="<<gts->rep.size());
                            ts_db.save(ts_path,gts->rep,replace);
                        }
                    }
                }
            }

            store_cb_tsm.push_back(tsm);
            store_cb_replace.push_back(replace);
            
        };
        
        string tc{"geo"};// important in our case, since we are using this as prefix for all geo stuff.
        if(is_internal_db)
            geo_dtss.add_container("",tmpdir.string());// force use of internal root/default container
        else
            geo_dtss.add_container(tc,tmpdir.string());
        
        geo::grid_spec gs{32633,{{0,0,0},{1000,0,1},{1000,1000,2},{0,1000,3}}}; // 4 points, a grid.
        vector<utctime> t0_times{};// initially empty 
        vector<string> variables {string("temperature"),string("precipitation")};
        utctime fc_dt=from_seconds(2*24*3600);//forecast length 2x 24hours
        utctime max_fc_dt=fc_dt;
        geo::ts_db_config_ gdb=make_shared<geo::ts_db_config>(
            prefix,tc,"a test geo container using shyft native time-series",gs,t0_times,max_fc_dt,2,variables
        );
        //geo_dtss.add_geo_ts_db(gdb);
        geo_dtss.set_listening_ip("127.0.0.1");
        int port_no=geo_dtss.start_server();
        string host_port = string("localhost:") + to_string(port_no);
        // make corresponding client that we will use for the test.
        client dtss(host_port);
        /* SUBCASE("geo_store_evaluate")*/ {
            dtss.add_geo_ts_db(gdb);// add via client interface
            auto g0 = dtss.get_geo_ts_db_info();
            REQUIRE_EQ(g0.size(),1);
            FAST_CHECK_EQ(g0[0]->name,tc);
            FAST_CHECK_EQ(g0[0]->variables,variables);
            FAST_CHECK_EQ(g0[0]->t0_times.size(),0);
            auto find_pattern="shyft://"+tc+"/.*"; //explained: in the above code we map the extgeo using the internal shyft:// stuff, so we just fake external read
            //MESSAGE("find_pattern:"<<find_pattern);
            auto f0 = dtss.find(find_pattern);
            CHECK_EQ(f0.size(),0);// expect zero for internal(should ignore geo.cfg in search) (the geo.cfg file), and zero for external 
            auto t0_dt=from_seconds(24*3600);// new forecast everty 24 hour
            gta_t t0_ta{from_seconds(0),t0_dt,3};
            auto tsm1=test::create_forecast(*gdb,fc_dt,t0_ta);
            dtss.geo_store(gdb->name,tsm1,true,true);
            auto f1 = dtss.find(find_pattern);
            CHECK_EQ(f1.size(),tsm1.shape.size());//expect exact stored size, the internal db have one file more, the cfg file
            auto g1 = dtss.get_geo_ts_db_info();// fetch updated information after store
            REQUIRE_EQ(g1.size(),1);
            FAST_CHECK_EQ(g1[0]->name,tc);
            FAST_CHECK_EQ(g1[0]->variables,variables);
            FAST_CHECK_EQ(g1[0]->t0_times.size(),t0_ta.size());// this indicates we stored t0_ta forecasts.
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().id_count,tsm1.shape.size());// verify we got the items cached
            // now as we have 3 forecasts stored, let's read back the results
            
            //eval_args(string const &geo_ts_db_id, vector<string> const & variables,vector<int64_t> const & ens,gta_t  const &ta, query const & geo_range,bool concat,utctime cc_dt0);
            // all defaults should return entire geo-db
            geo::eval_args ea{tc,{},{},gta_t{},utctime{0},geo::query{},false,utctime(0)};
            auto gr1=dtss.geo_evaluate(ea,true,true);
            FAST_CHECK_EQ(gr1.shape.n_t0,t0_ta.size());
            FAST_REQUIRE_EQ(tsm1.shape,gr1.shape);
            // also check the contents
            auto verify_expected_read_all_result=[&tsm1,&gdb](geo::geo_ts_matrix const &gr1) {
            for(size_t t=0;t<gr1.shape.n_t0;++t)
                for(size_t v=0;v<gr1.shape.n_v;++v)
                    for(size_t e=0;e<gr1.shape.n_e;++e)
                        for(size_t g=0;g<gr1.shape.n_g;++g) {
                            //MESSAGE("check "<<t<<"/"<<v<<"/"<<e<<"/"<<g);
                            apoint_ts a=tsm1.ts(t,v,e,g);
                            //MESSAGE("a="<<a.size());
                            apoint_ts b=gr1.ts(t,v,e,g);
                            //MESSAGE("b="<<b.size());
                            FAST_CHECK_EQ(a,b);
                            // check geo-point value ref. gdb
                            FAST_CHECK_EQ(gr1.get_geo_point(t,v,e,g),gdb->grid.points[g]);
                        }
            };
            verify_expected_read_all_result(gr1);
            //
            // This section is to verify the cache hits/misses, and partial hits.
            //
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().hits,tsm1.shape.size());// verify we got the hits, indicating that store with cache was working
            auto gr2=dtss.geo_evaluate(ea,false,false);
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().hits,tsm1.shape.size());// not using cache, so no more hits
            verify_expected_read_all_result(gr2);//again, don't trust the code.
            geo_dtss.flush_cache();// get rid of the cache, because we want to test cache-miss and read
            auto gr3=dtss.geo_evaluate(ea,true,true);//also add items to cache while reading
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().hits,tsm1.shape.size());// no hits, because we did flush cache
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().misses,tsm1.shape.size());// misses all lookups, because we did flush cache
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().id_count,tsm1.shape.size());// now the cache should be restored
            verify_expected_read_all_result(gr3);// verify we still got correct answer.
            size_t n_removed=0;
            /* remove some of the items in cache, so that we get partial misses */ {
                for(size_t i=0;i<f1.size();++i){
                    if(boost::starts_with(f1[i].name,"geo/1/")) { // remove all variables with ix=1, so we only need to refill those
                        geo_dtss.ts_cache.remove(gdb->prefix+f1[i].name);
                        n_removed++;
                        //MESSAGE("removed: "<<gdb->prefix+f1[i].name);
                    }
                }
            }
            geo_dtss.ts_cache.clear_cache_stats();// so we start counting from 0, makes test below easy
            auto gr4=dtss.geo_evaluate(ea,true,true);// then try read again, we should have misses here
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().hits,tsm1.shape.size()-n_removed);// no hits, because we did flush cache
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().misses,n_removed);// misses all lookups, because we did flush cache
            FAST_CHECK_EQ(geo_dtss.get_cache_stats().id_count,tsm1.shape.size());// now the cache should be restored
            // and the latest evaluation slice, should reflect that we just needed a refill
            if(!is_internal_db) { // if we are using extgeo:// the callbacks above is alive, and we can check behaviour
                vector<utctime> t0_times;for(size_t i=0;i<t0_ta.size();++i) t0_times.push_back(t0_ta.time(i));// 
                geo::slice half_refill_slice{{1},{0,1,2,3},{0,1},t0_times,gdb->dt};//  just one variable
                FAST_REQUIRE_EQ(slice_requested.back().size(),half_refill_slice.size());
                geo::slice full_refill_slice{{0,1},{0,1,2,3},{0,1},t0_times,gdb->dt};// full request for others
                FAST_CHECK_EQ(slice_requested.front(),full_refill_slice);
            } else { // using internal geo db: then the stored geo.cfg file should equal to the one in memory, t0 should be updated etc.
                auto geo_cfg=geo_dtss.geo_ts_db_scan(fs::path(tmpdir.string())/tc);
                CHECK(geo_cfg);
                FAST_CHECK_EQ(**geo_cfg,*g1[0]);
                // when we start  dtss on the this root, it should auto-configure when we add the '' root path
                // let's just try that here:
                server tst_dtss;// ok a server
                tst_dtss.add_container(tc,(tmpdir/tc).string());// add the container, and let the magic happen
                FAST_CHECK_EQ(tst_dtss.geo.size(),1);// yes, it's there, good.
                FAST_CHECK_EQ(*tst_dtss.geo[tc],*g1[0]);// and it should equal the one above
                
            }
            // now try the extend existing forecast, let's pick the latest
            // by choosing t0 after the last forecast,
            // since we have new forecast every fc_dt, 
            // - we generate the new forecast with t0 = t0_previous+ fc_dt
            //   that's realistic case, e.g. for incrementally building arome or EC cc forecasts
            gta_t t0_extend(t0_ta.time(t0_ta.size()-1) + t0_dt , fc_dt,1); // just one new forecast.
            auto tsm_ext=test::create_forecast(*gdb,fc_dt,t0_extend);
            dtss.geo_store(gdb->name,tsm_ext,false,true);// replace=false -> concat, +update cache
            // then check that we still have only 3 time-points in the database (because we just added more data to latest forecast
            auto g_ext = dtss.get_geo_ts_db_info();// fetch updated information after store
            FAST_REQUIRE_EQ(g_ext.size(),1);// ok, so it's not messed up
            FAST_CHECK_EQ(g_ext[0]->t0_times.size(),t0_ta.size());// should be no change here.
            //-- now read the last forecast part, it should be 48+24 hours long
            geo::eval_args ea_ext{tc,{},{},gta_t{t0_ta.time(t0_ta.size()-1),t0_dt,1},deltahours(48+24),geo::query{},false,utctime(0)};
            auto gr_ext=dtss.geo_evaluate(ea_ext,true,false);// use cache, don't update it further
            FAST_REQUIRE_EQ(gr_ext.shape.n_t0,1);// we should hit exactly one forecast, that we ask for.
            FAST_CHECK_EQ(gr_ext.ts(0,0,0,0).size(),48+24);// the length, we got the extended range that we put into the eval-args above
            if(is_internal_db) { // finally check that we can remove internal db
                dtss.remove_geo_ts_db(tc);
                auto f_zero = dtss.find(find_pattern);
                FAST_CHECK_EQ(f_zero.size(),0);

                auto g_zero=dtss.get_geo_ts_db_info();
                FAST_CHECK_EQ(g_zero.size(),0);
            }
        }
        dtss.close();
        geo_dtss.clear();
    }
    
}
TEST_CASE("geo_internal_read") {
    /** 
     * ensure that given a geo db with >1 dimensions in geo,ens,variable and time
     * when we read partial slices, the actual results
     * are precisely the values that are stored.
     * As pointed out by Cecilie, this was not the case in the initial implementation
     * due to a simple bug, that escaped the previous tests, since they where not 
     * specific for the overall functionality(worked if reading all values).
     */
    using namespace shyft::dtss;
    using namespace shyft::time_series::dd;
    using namespace shyft::core;
    using shyft::time_series::point_ts;
    using shyft::time_series::ts_point_fx;

    auto utc=make_shared<calendar>();
    test::utils::temp_dir tmpdir("shyft.dtss_geo.internal.read");
    /* scope tmp-dir */{
        server geo_dtss{};
        string prefix="shyft://";
        string tc{"geo"};// important in our case, since we are using this as prefix for all geo stuff.
        geo_dtss.add_container("",tmpdir.string());// force use of internal root/default container
        geo::grid_spec gs{32633,{{0,0,0},{1000,0,1},{1000,1000,2},{0,1000,3}}}; // 4 points, a grid.
        vector<utctime> t0_times{};// initially empty 
        vector<string> variables {string("temperature"),string("precipitation")}; // two variables
        utctime fc_dt=from_seconds(2*24*3600);//forecast length 2x 24hours
        utctime max_fc_dt=fc_dt;
        int64_t n_ens=2;
        geo::ts_db_config_ gdb=make_shared<geo::ts_db_config>(
            prefix,tc,"a test geo container using shyft native time-series",gs,t0_times,max_fc_dt,n_ens,variables
        );
        
        geo_dtss.set_listening_ip("127.0.0.1");
        int port_no=geo_dtss.start_server();
        string host_port = string("localhost:") + to_string(port_no);
        // make corresponding client that we will use for the test.
        /* scope client  */{
            client dtss(host_port);
            dtss.add_geo_ts_db(gdb);// add via client interface
            auto t0_dt=from_seconds(24*3600);// new forecast everty 24 hour
            gta_t t0_ta{from_seconds(0),t0_dt,3};
            auto tsm1=test::create_forecast(*gdb,fc_dt,t0_ta);
            dtss.geo_store(gdb->name,tsm1,true,false);// replace,and no cache
            auto p=gs.points[2];auto d=1;
            geo::query g_q1{gs.epsg,vector<geo_point>{{p.x-d,p.y-d,p.z},{p.x+d,p.y-d,p.z},{p.x,p.y+d,p.z}}};
            gta_t q1_ta{t0_ta.time(1),t0_dt,1};// pick the second forecast
            geo::eval_args q1{tc,vector<string>{variables[1]},vector<int64_t>{1},q1_ta,utctime{0},g_q1,false,utctime{0}};// pick precipitation,second geo-point, second ens, and second time.
            auto r1=dtss.geo_evaluate(q1,false,false);// read query 1.
            //MESSAGE("r1.shape, n_t0="<<r1.shape.n_t0<<",n_v="<<r1.shape.n_v<<",n_e="<<r1.shape.n_e<<",n_g="<<r1.shape.n_g<<"\n");
            auto xdb=dtss.get_geo_ts_db_info()[0];
            auto s1=xdb->compute(q1);// compute the slice of the query
            //MESSAGE("s1., t="<<s1.t.size()<<",n_v="<<s1.v.size()<<",n_e="<<s1.e.size()<<",n_g="<<s1.g.size()<<"\n");
            FAST_CHECK_EQ(r1.shape,geo::detail::ix_calc{1,1,1,1});//verify we got exactly the right dim back.
            FAST_CHECK_EQ(r1.tsv.size(),1u);
            FAST_CHECK_EQ(r1.tsv[0].mid_point,p);// should hit exactly this point.
            auto s1_t0_ix=t0_ta.index_of(s1.t[0]);
            auto q1_ts= tsm1.ts(s1_t0_ix,s1.v[0],s1.e[0],s1.g[0]);// use the computed slice to get out the ts from the source
            FAST_CHECK_EQ(q1_ts.time_axis()==r1.tsv[0].ts.time_axis(),true);
            FAST_CHECK_EQ(q1_ts==r1.tsv[0].ts,true);
            dtss.close();
        }
        geo_dtss.clear();
    }
    
}
}
