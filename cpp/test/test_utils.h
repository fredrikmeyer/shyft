#pragma once
#include <shyft/core/fs_compat.h>
#include <shyft/time/utctime_utilities.h>
#include <thread>
// Require you to include test_pch.h first.

namespace test::utils {
    
class temp_dir {
    fs::path p;
  public:
      
    temp_dir(const char *tmp_pth="shyft.") {
        p = fs::temp_directory_path()/(std::string(tmp_pth)+std::to_string(shyft::core::utctime_now().count())+std::to_string(std::hash<std::thread::id>()(std::this_thread::get_id())));
    }
    
    operator fs::path() const {return p;}
    fs::path operator/(std::string c) const {return p/c;}
    fs::path operator/(const char*c) const {return p/std::string(c);}
    fs::path operator/(fs::path const&c ) const {return p/c;}
    
    std::string string() const {return p.string();}
    
    ~temp_dir() {
        #ifdef _WIN32
            for (int i=0,n=10; i<n; ++i) {
                if (i > 0) MESSAGE("Unable to remove " << p.string() << ", retrying " << i << "/" << n-1);
                std::this_thread::sleep_for(std::chrono::duration<int, std::milli>(200));
                try {
                    fs::remove_all(p);
                    if (i > 0) MESSAGE("Successfully removed " << p.string());
                    return;
                } catch (...) {}
            }
            //TODO: CHECK_MESSAGE(false, "Failed to remove " << p.string());
            WARN_MESSAGE(false, "Failed to remove " << p.string());
        #else
            fs::remove_all(p);
        #endif
    }
};

}
