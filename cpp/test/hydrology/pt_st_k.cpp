
#include "test_pch.h"
#include <shyft/hydrology/stacks/pt_st_k.h>
#include <shyft/hydrology/cell_model.h>
#include <shyft/hydrology/stacks/pt_st_k_cell_model.h>
#include "hydro_mocks.h"
#include <shyft/time_series/point_ts.h>
#include <shyft/time/utctime_utilities.h>
#include <energy_market/serialize_loop.h>
// Some typedefs for clarity
using namespace shyft::core;
using namespace shyft::time_series;
using namespace shyft::core::pt_st_k;

using namespace shyfttest::mock;
using namespace shyfttest;

namespace pt = shyft::core::priestley_taylor;
namespace st = shyft::core::snow_tiles;
namespace gm = shyft::core::glacier_melt;
namespace kr = shyft::core::kirchner;
namespace ae = shyft::core::actual_evapotranspiration;
namespace pc = shyft::core::precipitation_correction;
namespace ta = shyft::time_axis;
typedef TSPointTarget<ta::point_dt> catchment_t;

namespace shyfttest {
    namespace mock {
        // need specialization for ptstk_response_t above
        template<> template<>
        void ResponseCollector<ta::fixed_dt>::collect<response>(size_t idx, const response& response) {
            _snow_output.set(idx, response.snow.outflow);
        }
        template <> template <>
        void DischargeCollector<ta::fixed_dt>::collect<response>(size_t idx, const response& response) {
            // q_avg is given in mm, so compute the totals
            avg_discharge.set(idx, destination_area*response.kirchner.q_avg / 1000.0 / 3600.0);
        }
    };
}; // End namespace shyfttest
TEST_SUITE("pt_st_k") {
TEST_CASE("pt_st_k/call_stack") {
    xpts_t temp;
    xpts_t prec;
    xpts_t rel_hum;
    xpts_t wind_speed;
    xpts_t radiation;

    calendar cal;
    utctime t0 = cal.time(YMDhms(2014, 8, 1, 0, 0, 0));
    size_t n_ts_points = 3*24;
    utctimespan dt  = deltahours(1);
    utctime t1 = t0 + n_ts_points*dt;
    shyfttest::create_time_series(temp, prec, rel_hum, wind_speed, radiation, t0, dt, n_ts_points);

    auto model_dt = deltahours(24);
    vector<utctime> times;
    for (utctime i=t0; i <= t1; i += model_dt)
        times.emplace_back(i);
    ta::fixed_dt time_axis(t0, dt, n_ts_points);
	ta::fixed_dt state_time_axis(t0, dt, n_ts_points + 1);
    // Initialize parameters
    //std::vector<double> s = {1.0, 1.0, 1.0, 1.0, 1.0}; // Zero cv distribution of snow (i.e. even)
    //std::vector<double> a = {0.0, 0.25, 0.5, 0.75, 1.0};
    pt::parameter pt_param;
    st::parameter snow_param;
    ae::parameter ae_param;
    kr::parameter k_param;
    pc::parameter p_corr_param;

    // Initialize the state vectors
    kr::state kirchner_state {5.0};
    st::state snow_state; //(10.0, 0.5);
    // should work without..snow_state.distribute(snow_param);
    // Initialize response


    // Initialize collectors
    shyfttest::mock::ResponseCollector<ta::fixed_dt> response_collector(1000*1000, time_axis);
    shyfttest::mock::StateCollector<ta::fixed_dt> state_collector(state_time_axis);

    state state {snow_state, kirchner_state};
    parameter parameter(pt_param, snow_param, ae_param, k_param, p_corr_param);
    geo_cell_data geo_cell_data;
    pt_st_k::run<direct_accessor, response>(geo_cell_data, parameter, time_axis,0,0, temp,
                                              prec, wind_speed, rel_hum, radiation, state,
                                              state_collector, response_collector);

    auto snow_swe = response_collector.snow_swe();
    for (size_t i = 0; i < snow_swe.size(); ++i)
        TS_ASSERT(std::isfinite(snow_swe.get(i).v) && snow_swe.get(i).v >= 0);
}
TEST_CASE("pt_st_k/lake_reservoir_response") {
    calendar cal;
    utctime t0 = cal.time(2014, 8, 1, 0, 0, 0);
     utctimespan dt=deltahours(1);
     const int n=150;// need to run some steps to observe kirchner response
     ta::fixed_dt tax(t0,dt,n);
     ta::fixed_dt tax_state(t0, dt, n + 1);
     pt::parameter pt_param;
     st::parameter st_param;
     ae::parameter ae_param;
     kr::parameter k_param;
     pc::parameter p_corr_param;
        st_param.lwmax=0.0;// ensure everything melts..
     parameter parameter{pt_param, st_param, ae_param, k_param, p_corr_param};

     pts_t temp(tax,-15.0,POINT_AVERAGE_VALUE);temp.set(4,20.0); // freezing cold, except time-step 4 where we melt
     pts_t prec(tax,3.0,POINT_AVERAGE_VALUE);prec.set(0,0.0);prec.set(4, 0.0);// rain except 1.step we use to get initial kirchner response
     pts_t rel_hum(tax,0.8,POINT_AVERAGE_VALUE);
     pts_t wind_speed(tax,2.0,POINT_AVERAGE_VALUE);
     pts_t radiation(tax,300.0,POINT_AVERAGE_VALUE);

     kr::state kirchner_state{1};// 1 mm
     st::state gs_state;
     //gs_state.lwc=100.0;
     //gs_state.acc_melt=100.0;

     state s0{gs_state, kirchner_state};// need a equal state for the second run
     state s1{gs_state, kirchner_state};

     geo_cell_data gcd(geo_point(1000,1000,100));
     land_type_fractions ltf(0.1,0.2,0.3,0.0,0.4);//0.1 glac 0.2 lake, 0.3 reservoir , 0.4 unspec
     gcd.set_land_type_fractions(ltf);

     pt_st_k::state_collector sc;
     pt_st_k::all_response_collector rc;
     const double cell_area=1000*1000;
     sc.collect_state=true;
     sc.initialize(parameter,tax_state,0,0,cell_area,gcd.land_type_fractions_info().snow_storage());
     rc.initialize(tax,0,0,cell_area);

     parameter.msp.reservoir_direct_response_fraction=0.0;// all rsv goes to kirchner
     pt_st_k::run<direct_accessor,pt_st_k::response>(gcd,parameter,tax,0,0,temp,prec,wind_speed,rel_hum,radiation,s0,sc,rc);
     CHECK_EQ(rc.avg_discharge.value(0), doctest::Approx(0.266).epsilon(0.01)); // first with 0 precip, nothing should happen
     CHECK_EQ(rc.charge_m3s.value(0),doctest::Approx(-0.266 - mmh_to_m3s(rc.ae_output.value(0),cell_area) ).epsilon(0.01));// 
     
     CHECK_EQ(rc.avg_discharge.value(n-1), doctest::Approx( 0.5*mmh_to_m3s(prec.value(1),cell_area)).epsilon(0.01));
     CHECK_EQ(rc.charge_m3s.value(n-1),doctest::Approx( 0.5*mmh_to_m3s(prec.value(1),cell_area) ).epsilon(0.01));//steady state, expect storage over 
     
     parameter.msp.reservoir_direct_response_fraction=1.0;// all rsv goes directly to output, lake goes to kirchner
     sc.initialize(parameter,tax_state,0,0,cell_area,gcd.land_type_fractions_info().snow_storage());
     rc.initialize(tax,0,0,cell_area);
     pt_st_k::run<direct_accessor,pt_st_k::response>(gcd,parameter,tax,0,0,temp,prec,wind_speed,rel_hum,radiation,s1,sc,rc);
     CHECK_EQ(rc.avg_discharge.value(0), doctest::Approx(0.266).epsilon(0.01)); // first with 0 precip, nothing should happen
    auto expected_1 = 
          0.266 // estimate of kirchner output from 1st step.
        + 0.3*mmh_to_m3s(prec.value(1)-rc.pe_output.value(1),cell_area); // estimate of direct response for 1st timestep
     CHECK_EQ(rc.avg_discharge.value(1), doctest::Approx(expected_1).epsilon(0.05)); // precip on rsv direct effect
     //-- verify snow storage on non lake/rsv only
     auto snow_swe=sc.swe(parameter.st,gcd.land_type_fractions_info().snow_storage());
     CHECK_EQ(snow_swe.value(0),doctest::Approx(0.0).epsilon(0.0001));// linear, and 0.0 first point
     CHECK_EQ(snow_swe.value(1),doctest::Approx(0.0).epsilon(0.0001));// linear, and 0.0 second point
     CHECK_EQ(snow_swe.value(2),doctest::Approx(1.5).epsilon(0.0001));// linear, and 1.5  mm, 3.0/2 mm third  point
     CHECK_EQ(snow_swe.value(3),doctest::Approx(3.0).epsilon(0.0001));// linear, and 1.5  mm, 3.0/2 mm third  point
     CHECK_EQ(rc.snow_swe.value(0),doctest::Approx(0.0).epsilon(0.0001));// stair, avg,, and 0.0 first interval
     CHECK_EQ(rc.snow_swe.value(1),doctest::Approx(1.5).epsilon(0.0001));// stair avg, 1.5mm  second interval
     CHECK_EQ(rc.snow_swe.value(2),doctest::Approx(3.0).epsilon(0.0001));// stair avg, 3.0mm  third interval
     CHECK_EQ(rc.snow_outflow.value(0),doctest::Approx(0.0).epsilon(0.0001)); // zero outflow
     CHECK_EQ(rc.snow_outflow.value(1),doctest::Approx(0.0).epsilon(0.0001)); // zero outflow
     CHECK_EQ(rc.snow_outflow.value(2),doctest::Approx(0.0).epsilon(0.0001)); // zero outflow
     CHECK_EQ(rc.snow_outflow.value(3),doctest::Approx(0.0).epsilon(0.0001)); // zero outflow
     CHECK_EQ(rc.snow_outflow.value(4),doctest::Approx(0.1157407407).epsilon(0.0001)); // some response, due to 20 deg C.
     CHECK_EQ(rc.snow_outflow.value(5),doctest::Approx(0).epsilon(0.0001)); // zero outflow
     auto expected_2 = 
         0.2*mmh_to_m3s(prec.value(1)-rc.ae_output.value(1),cell_area) // because this is on lake(frozen, but pressed out)
        +0.3*mmh_to_m3s(prec.value(1)-rc.pe_output.value(1),cell_area);// this is direct response

     CHECK_EQ(rc.avg_discharge.value(n-1), doctest::Approx(expected_2 ).epsilon(0.01));
     // re-init: verify we keep state:
     pt_st_k::run<direct_accessor,pt_st_k::response>(gcd,parameter,tax,1,2,temp,prec,wind_speed,rel_hum,radiation,s1,sc,rc);
     snow_swe=sc.swe(parameter.st,gcd.land_type_fractions_info().snow_storage());
     CHECK_EQ(snow_swe.value(0),doctest::Approx(0.0).epsilon(0.0001));// linear, and 0.0 first point
     CHECK_EQ(rc.snow_swe.value(0),doctest::Approx(0.0).epsilon(0.0001));// stair, avg,, and 0.0 first interval
     CHECK_EQ(rc.snow_outflow.value(0),doctest::Approx(0.0).epsilon(0.0001)); // zero outflow
     //verify we do complete re-init if we change parameter
     parameter.st=st::parameter{1.999,0.1,0.9,1.1,0.12,0.51,{0.2, 0.2, 0.2, 0.2, 0.2}};
     sc.initialize(parameter,tax_state,1,3,cell_area,gcd.land_type_fractions_info().snow_storage());
     REQUIRE_EQ(sc.lw.size(),5);
     REQUIRE_EQ(sc.fw.size(),5);
     for(size_t i=0;i<5;++i) {
        CHECK(std::isnan(sc.lw[i].value(0)));
        CHECK(std::isnan(sc.fw[i].value(0)));
     }
    
    //-- now verify that if we start melting
    // (1) it melts as usual from snow-coverable fraction
    // (2) when the snow has melted to an area less than the glacier, 
    //     we start getting a non-zero glacier-melt response.
    //     
    // Arrange the test: 
    //
    land_type_fractions ltf2(0.2,0.2,0.3,0.0,0.3);//0.2 glac 0.2 lake, 0.3 reservoir , 0.3 unspec
    gcd.set_land_type_fractions(ltf2);
    parameter.st= st_param;
    parameter.st.cx=10.0;//  ensure we melt fast
    temp.fill_range(20.0,0,n);// hot 20 deg/C that force melt
    prec.fill_range(0.0,0,n);// no rain, just summer
    sc.initialize(parameter,tax_state,0,0,cell_area,gcd.land_type_fractions_info().snow_storage());// ensure we nan out response and state
    rc.initialize(tax,0,0,cell_area);
    // Act: run usint state s1 (should have a lot of snow in it)
    pt_st_k::run<direct_accessor,pt_st_k::response>(gcd,parameter,tax,0,0,temp,prec,wind_speed,rel_hum,radiation,s1,sc,rc);
    // Assert: 
    FAST_CHECK_GT(rc.snow_swe.value(0),rc.snow_swe.value(n-1));
    FAST_CHECK_GT(rc.snow_sca.value(0),rc.snow_sca.value(n-1));
    FAST_CHECK_LT(rc.snow_sca.value(n-1),ltf.glacier());

    for(size_t i=0;i<n;++i) { // loop over steps and verify that there are no glacier response until uncovered bare glacier
        auto sca_cell= rc.snow_sca.value(i)*ltf2.snow_storage();
        //MESSAGE("sca "<<i<<":"<<sca_cell<<" swe:"<<rc.snow_swe.value(i)<<" gm="<<rc.glacier_melt.value(i));
        if(sca_cell >= ltf2.glacier()) { // glacier is covered by snow, no melt
            FAST_CHECK_EQ(rc.glacier_melt.value(i),doctest::Approx(0.0));
        } else {// parts of the glacier is now exposed, so it should start melting 
            FAST_CHECK_GT(rc.glacier_melt.value(i),0.0);
        }
    }

 }
 TEST_CASE("pt_st_k/serialization") {
     pt::parameter pt_param{0.21,1.33};
     st::parameter gs_param{1.999,0.1,0.9,1.1,0.12,0.51,{0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.09, 0.11, 0.09, 0.1}};
     ae::parameter ae_param{1.6};
     kr::parameter k_param{-2.539,0.80,-0.2};
     pc::parameter p_corr_param{1.5};
     parameter a{pt_param, gs_param, ae_param, k_param, p_corr_param};
     auto b=test::serialize_loop(a);
     FAST_CHECK_EQ(b,a);
 }
}
