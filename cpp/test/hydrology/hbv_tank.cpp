#include "test_pch.h"
#include <shyft/hydrology/methods/hbv_tank.h>


using namespace shyft::core;
TEST_SUITE("hbv_tank"){
TEST_CASE("hbv_tank/equal_operator") {
    double uz1 = 25;
    double kuz2 = 0.5;
    double kuz1 = 0.3;
    double perc = 0.8;
    double klz = 0.02;
    
    hbv_tank::parameter p(uz1,kuz2, kuz1, perc, klz);
    hbv_tank::parameter p1(uz1+1.0,kuz2, kuz1, perc, klz);
    hbv_tank::parameter p2(uz1,kuz2+1.0, kuz1, perc, klz);
    hbv_tank::parameter p3(uz1,kuz2, kuz1+1.0, perc, klz);
    hbv_tank::parameter p4(uz1,kuz2, kuz1, perc+1.0, klz);
    hbv_tank::parameter p5(uz1,kuz2, kuz1, perc, klz+1.0);
    
    TS_ASSERT(p != p1);
    TS_ASSERT(p != p2);
    TS_ASSERT(p != p3);
    TS_ASSERT(p != p4);
    TS_ASSERT(p != p5);
    
    p1.uz1 = uz1;
    p2.kuz2 = kuz2;
    p3.kuz1 = kuz1;
    p4.perc = perc;
    p5.klz = klz;
    
    TS_ASSERT(p == p1);
    TS_ASSERT(p == p2);
    TS_ASSERT(p == p3);
    TS_ASSERT(p == p4);
    TS_ASSERT(p == p5);
}

TEST_CASE("hbv_tank/regression") {
    hbv_tank::parameter p;
    hbv_tank::calculator<hbv_tank::parameter> calc(p);
    hbv_tank::state s;
    hbv_tank::response r;
    calendar utc;
    utctime t0 = utc.time(2015, 1, 1);

    calc.step(s, r, t0, t0 + deltahours(1), 0);
    TS_ASSERT_DELTA(r.outflow, 6.216, 0.0); //TODO: verify some more numbers
    TS_ASSERT_DELTA(s.uz, 13.2, 0.0);
    TS_ASSERT_DELTA(s.lz, 10.584, 0.0001);
    calc.step(s, r, t0, t0 + deltahours(1), 20.0);
    TS_ASSERT_DELTA(s.uz, 20.8, 0.0002);
    TS_ASSERT_DELTA(r.outflow, 11.82768, 0.00005);
}
}
