#include "test_pch.h"
#include <shyft/hydrology/srv/client.h>
#include <shyft/hydrology/srv/server.h>
#include <shyft/hydrology/region_model.h>
#include <shyft/hydrology/stacks/pt_gs_k.h>  // looking for the ptgsk cell modelj
#include <shyft/hydrology/stacks/pt_ss_k.h>
#include <shyft/hydrology/stacks/pt_st_k.h>
#include <shyft/hydrology/stacks/pt_hps_k.h>
#include <shyft/hydrology/stacks/r_pt_gs_k.h>
#include <shyft/hydrology/stacks/r_pm_gs_k.h>


#include <shyft/hydrology/api/api.h>  // looking for region environment
#include <shyft/hydrology/api/api_state.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/variant.hpp>
#include <variant>

using namespace shyft::core;
using namespace shyft::hydrology::srv;
using std::string;
using std::to_string;
using shyft::time_series::dd::apoint_ts;
using shyft::core::q_adjust_result;


template <class T>
static T serialize_loop(const T& o, int c_a_flags = core_arch_flags) {
    std::stringstream xmls;
    core_oarchive oa(xmls, c_a_flags);
    oa << core_nvp("o", o);
    xmls.flush();
    core_iarchive ia(xmls, c_a_flags);
    T o2;
    ia>>core_nvp("o", o2);
    return o2;
}

template <class cell, rmodel_type rmt>
void test_the_story(server &s ,client &c, int mid=0) {
    //using cellstate_t = shyft::api::cell_state_with_id<typename cell::state_t>;
    using region_model_t_= shared_ptr<region_model<cell,a_region_environment>>;
    using p_type=typename cell::parameter_t;
    using p_type_=shared_ptr<p_type>;
    string m0="m_"+std::to_string(mid);
    string m1=m0+"1";
    string m2=m0+"2";
    string m3=m0+"3";
    string m4=m0+"4";
        
    // create model test
    //MESSAGE("drms test:"<<mid);
    auto n_cells=10u;
    vector<geo_cell_data> gcd;gcd.reserve(n_cells);
    auto epsg_utm33n=32633;
    geo_point p0{150500,6800500,1325};
    for(size_t i=0;i<n_cells;++i) {
        // https://norgeskart.no/?tk&_ga=2.214456735.905437161.1593696328-144001353.1593696328#!?zoom=13&project=norgeskart&sok=8.48496784,61.17732199@4326&layers=1002&lat=6800000.00&lon=150000.00&markerLat=6799999.999918256&markerLon=149999.9998161777&panel=searchOptionsPanel
        auto dx=i*1000.0;
        auto dy=i*100.0;
        auto dz=i*50.0;
        geo_point p1_1{dx+150000,dy+6800000,dz+1325};
        geo_point p1_2{dx+151000,dy+6800000,dz+1600};
        geo_point p1_3{dx+150000,dy+6801000,dz+1400};
        //geo_cell_data c1{p1_1,p1_2,p1_3,epsg_utm33n,1};//default route and landtype
        
        gcd.emplace_back(p1_1,p1_2,p1_3,epsg_utm33n,1);
    }
    auto success = c.create_model(m1, rmt, gcd);
    //-- verify we get exception trying to create it once more!
    CHECK_THROWS_AS(c.create_model(m1,rmt,gcd),std::runtime_error);
    auto gcd_remote=c.get_geo_cell_data(m1);
    CHECK_EQ(gcd_remote,gcd);
    CHECK_EQ(success, true);
    CHECK_EQ(s.model_map.size(), 1);
    // test that get_model_ids now returns 1 model
    auto mids=c.get_model_ids();
    REQUIRE_EQ(mids.size(),1);
    CHECK_EQ(mids[0],m1);
    
    // set state test
    shyft::api::cids_t cids;
    state_variant_t s0 = c.get_state(m1,cids);
    boost::apply_visitor(
        [](auto const&sv)->void {
            
            for(auto &sid:*sv)
                sid.state.kirchner.q=0.8;
        }
        ,s0);
    success = c.set_state(m1, s0);
    
    CHECK_EQ(success, true);
    CHECK_EQ(c.set_initial_state(m1),true);
    
    CHECK_EQ(c.set_state_collection(m1, -1, false), true);
    CHECK_EQ(c.set_snow_sca_swe_collection(m1, -1, true), true);
    
    // test set environment
    calendar utc;
    int ts_size = 1*24*32;// 365;
    auto ta = time_axis::generic_dt(utc.time(2019,6,1),deltahours(1),ts_size);
    api::a_region_environment r_env;
    //auto p0=c1.mid_point();
    auto fx_sine=[](time_axis::generic_dt&ta,double w, double offset, double ampl){
        vector<double> r;r.reserve(ta.size());
        auto t0=ta.time(0);
        for(size_t i=0;i<ta.size();++i) {
            r.emplace_back(offset + ampl*sin(w*to_seconds(ta.time(i)-t0)));
        }
        return apoint_ts(ta,r,time_series::POINT_AVERAGE_VALUE);
    };
    apoint_ts ts(ta,0.8,time_series::POINT_AVERAGE_VALUE);
    constexpr double w=1/(24*3600);
    for(size_t x=0;x<5;++x) {
        for(size_t y=0;y<3;++y) {
            auto p1=p0;
            p1.x += x*1000;
            p1.z += (x+y)*10;
            p1.y += y*500;
            double tgrad=1.0 - (x+y)/150;
            r_env.temperature->push_back(api::TemperatureSource(p1,fx_sine(ta,1*w,tgrad*15.0,1.0)));
            r_env.precipitation->push_back(api::PrecipitationSource(p1,fx_sine(ta,0.001*w,2.0,1.0).abs().evaluate()));
            r_env.radiation->push_back(api::RadiationSource(p1,fx_sine(ta,0.5*w,0.0,500.0).abs().evaluate()));
            r_env.wind_speed->push_back(api::WindSpeedSource(p1,fx_sine(ta,0.5*w,2.0,4.0).abs().evaluate()));
            r_env.rel_hum->push_back(api::RelHumSource(p1,fx_sine(ta,0.5*w,0.7,0.2).abs().evaluate()));
        }
    }
    interpolation_parameter ip_param;
    ip_param.use_idw_for_temperature=true;
    ip_param.temperature_idw.gradient_by_equation=true;
    success = c.run_interpolation(m1, ip_param, ta, r_env, true);
    CHECK_EQ(success, true);
    REQUIRE_EQ(c.is_cell_env_ts_ok(m1), true); // check cell time axis
    auto ip_param_remote= c.get_interpolation_parameter(m1);
    CHECK_EQ(ip_param_remote, ip_param);
    auto re_remote=c.get_region_env(m1);
    CHECK_EQ(re_remote,r_env);
    parameter_variant_t p=make_shared<p_type>();
    CHECK_EQ(true,c.set_region_parameter(m1,p));
    auto &mm =s.model_map[m1];
    CHECK_EQ(false,boost::get<region_model_t_>(mm->m)->has_catchment_parameter(1));
    
    // fx-callback functionality
    auto my_fx_cb=[&s](string const&mid, string const &fx_args)->bool {
        auto m=boost::get<region_model_t_>(s.get_model(mid));// ensure we can get a model..
        CHECK_UNARY(m!=nullptr);//verify we actually did get the model
        return fx_args=="true"?true:false;
    };
    s.fx_cb=my_fx_cb; // arm the callback here
    CHECK_EQ(true,c.fx(m1,"true"));
    CHECK_EQ(false,c.fx(m1,"!true"));
    s.fx_cb=nullptr;

    CHECK_EQ(true,c.set_catchment_parameter(m1,p,1));// catchm. id 1
    // verify we did set the catchment parameter
    auto m1_variant=s.model_map.find(string(m1));
    REQUIRE(m1_variant != s.model_map.end());
    CHECK_EQ(true,boost::get<region_model_t_>(m1_variant->second->m)->has_catchment_parameter(1));
    // test run cells
    success = c.run_cells(m1);
    CHECK_EQ(success, true);

        
        // test get statistics
        auto cell_index = shyft::core::stat_scope::cell_ix;
        apoint_ts ts_discharge = c.get_discharge(m1, cids, cell_index);
        CHECK_EQ(ts_discharge.time_axis(), ta);
        CHECK_EQ(c.get_charge(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_snow_swe(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_snow_sca(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_temperature(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_precipitation(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_radiation(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_wind_speed(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_rel_hum(m1, cids, cell_index).time_axis(), ta);

        
    // test adjust state
    double wanted_state = ts_discharge.value(0)*1.20;  // increase by 20%
    q_adjust_result res = c.adjust_q(m1, cids, wanted_state);
    CHECK_EQ(res.diagnostics, "");
    double obtained_state = res.q_r;
    CHECK_EQ(abs(obtained_state-wanted_state)<0.01*wanted_state, true); 
    apoint_ts ts_discharge2 = c.get_discharge(m1, cids,shyft::core::stat_scope::cell_ix);
    CHECK_NE(ts_discharge2.value(0), ts_discharge.value(0));
    CHECK(c.has_catchment_parameter(m1,1));
    c.remove_catchment_parameter(m1,1);
    CHECK(!c.has_catchment_parameter(m1,1));
    
    ///////////
    // CALIBRATION
    // Check initial state, no calibration ongoing.
        FAST_CHECK_EQ(false,c.cancel_calibration(m1));// no onging calibration, should return false, nothing cancelled
        auto cs=c.check_calibration(m1);// no onging calibration, should return false, nothing cancelled
        FAST_CHECK_EQ(false,cs.running);
        FAST_CHECK_EQ(0u,cs.p_trace.size());
        FAST_CHECK_EQ(0u,cs.f_trace.size());
        FAST_CHECK_EQ(0u,cs.p_result.which());
        cids=vector<int64_t>{1};
        vector<target_specification> spec {{ts_discharge*1.2,cids,1.0}};
        auto p_start=make_shared<p_type>();
        auto p_min =make_shared<p_type>();p_min->kirchner.c1 *= 0.9;p_min->p_corr.scale_factor *= 0.9;
        auto p_max =make_shared<p_type>();p_max->kirchner.c1 *= 1.1;p_max->p_corr.scale_factor *= 1.9; 
        calibration_options opt{optimizer_method::BOBYQA,1000};
        auto c_start=c.start_calibration(m1,p_start,p_min,p_max,spec,opt);
        FAST_CHECK_EQ(true,c_start);
        do {
            cs=c.check_calibration(m1);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        } while(cs.running);
        FAST_CHECK_EQ(false,cs.running);
        FAST_CHECK_GE(cs.f_trace.size(),1);
        auto p_opt=boost::get<p_type_>(cs.p_result);
        CHECK(p_opt != nullptr);

    // test catchment calculation filter
    std::vector<int64_t> catchment_ids;
    success = c.set_catchment_calculation_filter(m1, catchment_ids);
    CHECK_EQ(success, true);
                
    // test revert states
    
    success = c.revert_to_initial_state(m1);
    CHECK_EQ(success, true);
    
    // rename model test
    CHECK_THROWS_AS(c.rename_model(m2,m0), std::runtime_error); // renaming non-existent model
    CHECK_THROWS_AS(c.rename_model(m1,m1), std::runtime_error); // renaming to name already in use
    CHECK_EQ(c.rename_model(m1,m0), true);
    mids=c.get_model_ids();
    CHECK_EQ(mids.size(),1);
    CHECK_EQ(mids[0],m0);
    
    // clone model test
    CHECK_THROWS_AS(c.clone_model(m1,m3), std::runtime_error); 
    CHECK_THROWS_AS(c.clone_model(m0,m0), std::runtime_error);
    CHECK_EQ(c.clone_model(m0,m3), true);
    mids=c.get_model_ids();
    CHECK_EQ(mids.size(),2);
    CHECK_EQ(true,c.remove_model(m3));

    // copy model test
    CHECK_THROWS_AS(c.copy_model(m1,m4), std::runtime_error); 
    CHECK_THROWS_AS(c.copy_model(m0,m0), std::runtime_error);
    c.revert_to_initial_state(m0);
    CHECK_EQ(c.copy_model(m0,m4), true);
    auto m4_temp=c.get_temperature(m4, cids, cell_index);
    auto m0_temp=c.get_temperature(m0, cids, cell_index);
    CHECK_EQ(m4_temp,m0_temp);
    // check that m4 state is equal to m0 state
    state_variant_t m4_s0 = c.get_state(m4,cids);
        boost::apply_visitor(
            [](auto const&sv)->void {
                for(auto &sid:*sv)
                    FAST_REQUIRE_EQ(sid.state.kirchner.q,doctest::Approx(0.8));
            }
            ,m4_s0);
    
    mids=c.get_model_ids();
    CHECK_EQ(mids.size(),2);
    CHECK_EQ(true,c.remove_model(m4));
    
    // test we can remove model
    CHECK_EQ(true,c.remove_model(m0));
    mids=c.get_model_ids();// verify it's empty..
    CHECK_EQ(mids.size(),0);
    // and verify it throws if you try to remove non-existent model.
    CHECK_THROWS_AS(c.remove_model(m0),std::runtime_error);
}

template <class cell, rmodel_type rmt>
void stress_test(client &c, int mid,size_t n_cells=10) {
    string m0="m_"+std::to_string(mid);
    string m1=m0+"1";
    string m2=m0+"2";
    string m3=m0+"3";
    string m4=m0+"4";
        
    // create model test
    vector<geo_cell_data> gcd;gcd.reserve(n_cells);
    auto epsg_utm33n=32633;
    geo_point p0{150500,6800500,1325};
    for(size_t i=0;i<n_cells;++i) {
        // https://norgeskart.no/?tk&_ga=2.214456735.905437161.1593696328-144001353.1593696328#!?zoom=13&project=norgeskart&sok=8.48496784,61.17732199@4326&layers=1002&lat=6800000.00&lon=150000.00&markerLat=6799999.999918256&markerLon=149999.9998161777&panel=searchOptionsPanel
        auto dx=i*1000.0;
        auto dy=i*100.0;
        auto dz=i*50.0;
        geo_point p1_1{dx+150000,dy+6800000,dz+1325};
        geo_point p1_2{dx+151000,dy+6800000,dz+1600};
        geo_point p1_3{dx+150000,dy+6801000,dz+1400};
        //geo_cell_data c1{p1_1,p1_2,p1_3,epsg_utm33n,1};//default route and landtype
        
        gcd.emplace_back(p1_1,p1_2,p1_3,epsg_utm33n,1);
    }
    auto success = c.create_model(m1, rmt, gcd);
    //-- verify we get exception trying to create it once more!
    CHECK_THROWS_AS(c.create_model(m1,rmt,gcd),std::runtime_error);
    
    CHECK_EQ(success, true);
    // test that get_model_ids now returns 1 model
    auto mids=c.get_model_ids();
    CHECK_GE(mids.size(),1);
    
    // set state test
    shyft::api::cids_t cids;
    state_variant_t s0 = c.get_state(m1,cids);
    boost::apply_visitor(
        [](auto const&sv)->void {
            
            for(auto &sid:*sv)
                sid.state.kirchner.q=0.8;
        }
        ,s0);
    
    CHECK_EQ(c.set_state(m1, s0), true);
    CHECK_EQ(c.set_initial_state(m1),true);
    
    CHECK_EQ(c.set_state_collection(m1, -1, false), true);
    CHECK_EQ(c.set_snow_sca_swe_collection(m1, -1, true), true);
    
    // test set environment
    calendar utc;
    int ts_size = 1*24*365;
    auto ta = time_axis::generic_dt(utc.time(2019,6,1),deltahours(1),ts_size);
    api::a_region_environment r_env;
    //auto p0=c1.mid_point();
    apoint_ts ts(ta,0.8,time_series::POINT_AVERAGE_VALUE);
    for(size_t x=0;x<n_cells/2;++x) {
        for(size_t y=0;y<n_cells/2;++y) {
            auto p1=p0;
            p1.x += x*1000;
            p1.z += (x+y)*10;
            p1.y += y*500;
            
            r_env.temperature->push_back(api::TemperatureSource(p1,ts));
            r_env.precipitation->push_back(api::PrecipitationSource(p1,ts));
            r_env.radiation->push_back(api::RadiationSource(p1,ts));
            r_env.wind_speed->push_back(api::WindSpeedSource(p1,ts));
            r_env.rel_hum->push_back(api::RelHumSource(p1,ts));
        }
    }
    interpolation_parameter ip_param;
    ip_param.use_idw_for_temperature=true;
    ip_param.temperature_idw.gradient_by_equation=true;
    CHECK(c.run_interpolation(m1, ip_param, ta, r_env, true));
    CHECK_EQ(c.is_cell_env_ts_ok(m1), true); // check cell time axis
    CHECK_EQ(c.get_time_axis(m1),ta);// check time-axis is now as determined by interpolation ta

    parameter_variant_t p=make_shared<typename cell::parameter_t>();
    CHECK_EQ(true,c.set_region_parameter(m1,p));
    auto pr=c.get_region_parameter(m1);
    auto p1= *boost::get<std::shared_ptr<typename cell::parameter_t>>(p);
    auto p2= *boost::get<std::shared_ptr<typename cell::parameter_t>>(pr);
    CHECK_EQ(p1,p2);// verify we can get back region parameter
    boost::get<std::shared_ptr<typename cell::parameter_t>>(p)->kirchner.c1 *=1.001;
    CHECK_EQ(true,c.set_catchment_parameter(m1,p,1));// catchm. id 1
    pr=c.get_catchment_parameter(m1,1);
    p1= *boost::get<std::shared_ptr<typename cell::parameter_t>>(p);
    p2= *boost::get<std::shared_ptr<typename cell::parameter_t>>(pr);
    CHECK_EQ(p1,p2);// verify we can get bac
    // test run cells
    CHECK(c.run_cells(m1));
        
    // test get statistics
    auto cell_index = shyft::core::stat_scope::cell_ix;
    apoint_ts ts_discharge = c.get_discharge(m1, cids, cell_index);
    for(size_t i=0;i<10;++i) {
        CHECK_EQ(ts_discharge.time_axis(), ta);
        CHECK_EQ(c.get_charge(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_snow_swe(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_snow_sca(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_temperature(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_precipitation(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_radiation(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_wind_speed(m1, cids, cell_index).time_axis(), ta);
        CHECK_EQ(c.get_rel_hum(m1, cids, cell_index).time_axis(), ta);
    }
        
    // test adjust state
    double wanted_state = ts_discharge.value(0)*1.20;  // increase by 20%
    q_adjust_result res = c.adjust_q(m1, cids, wanted_state);
    CHECK_EQ(res.diagnostics, "");
    double obtained_state = res.q_r;
    CHECK_EQ(abs(obtained_state-wanted_state)<0.01*wanted_state, true); 
    apoint_ts ts_discharge2 = c.get_discharge(m1, cids,shyft::core::stat_scope::cell_ix);
    CHECK_NE(ts_discharge2.value(0), ts_discharge.value(0));
    
    // test catchment calculation filter
    std::vector<int64_t> catchment_ids;
    CHECK(c.set_catchment_calculation_filter(m1, catchment_ids));
    CHECK(c.revert_to_initial_state(m1));
    
    // rename model test
    CHECK_THROWS_AS(c.rename_model(m2,m0), std::runtime_error); // renaming non-existent model
    CHECK_THROWS_AS(c.rename_model(m1,m1), std::runtime_error); // renaming to name already in use
    CHECK_EQ(c.rename_model(m1,m0), true);
    
    // clone model test
    CHECK_THROWS_AS(c.clone_model(m1,m3), std::runtime_error); 
    CHECK_THROWS_AS(c.clone_model(m0,m0), std::runtime_error);
    CHECK_EQ(c.clone_model(m0,m3), true);
    CHECK_EQ(true,c.remove_model(m3));

    // copy model test
    CHECK_THROWS_AS(c.copy_model(m1,m4), std::runtime_error); 
    CHECK_THROWS_AS(c.copy_model(m0,m0), std::runtime_error);
    c.revert_to_initial_state(m0);
    CHECK_EQ(c.copy_model(m0,m4), true);
    auto m4_temp=c.get_temperature(m4, cids, cell_index);
    auto m0_temp=c.get_temperature(m0, cids, cell_index);
    CHECK_EQ(m4_temp,m0_temp);
    // check that m4 state is equal to m0 state
    state_variant_t m4_s0 = c.get_state(m4,cids);
        boost::apply_visitor(
            [](auto const&sv)->void {
                for(auto &sid:*sv)
                    FAST_REQUIRE_EQ(sid.state.kirchner.q,doctest::Approx(0.8));
            }
            ,m4_s0);
    
    CHECK_EQ(true,c.remove_model(m4));
    CHECK_EQ(true,c.remove_model(m0));
    CHECK_THROWS_AS(c.remove_model(m0),std::runtime_error);
}

#define SUBSECTION(x) 

TEST_SUITE("drms") {

TEST_CASE("drms_complete") {
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    //MESSAGE("Start testing");
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        client c(host_port);
        auto result = c.get_version_info();
        // check result
        CHECK_EQ(result, s.do_get_version_info());
        
        auto mids=c.get_model_ids();
        CHECK_EQ(mids.size(),0); // it should be 0 models to start with
        int mid=0;
        test_the_story<shyft::core::pt_gs_k::cell_discharge_response_t,rmodel_type::pt_gs_k_opt>(s,c,++mid);
        test_the_story<shyft::core::pt_gs_k::cell_complete_response_t,rmodel_type::pt_gs_k>(s,c,++mid);
        test_the_story<shyft::core::pt_st_k::cell_discharge_response_t,rmodel_type::pt_st_k_opt>(s,c,++mid);
        test_the_story<shyft::core::pt_st_k::cell_complete_response_t,rmodel_type::pt_st_k>(s,c,++mid);
        test_the_story<shyft::core::pt_ss_k::cell_discharge_response_t,rmodel_type::pt_ss_k_opt>(s,c,++mid);
        test_the_story<shyft::core::pt_ss_k::cell_complete_response_t,rmodel_type::pt_ss_k>(s,c,++mid);
        test_the_story<shyft::core::pt_hps_k::cell_discharge_response_t,rmodel_type::pt_hps_k_opt>(s,c,++mid);
        test_the_story<shyft::core::pt_hps_k::cell_complete_response_t,rmodel_type::pt_hps_k>(s,c,++mid);
        test_the_story<shyft::core::r_pt_gs_k::cell_discharge_response_t,rmodel_type::r_pt_gs_k_opt>(s,c,++mid);
        test_the_story<shyft::core::r_pt_gs_k::cell_complete_response_t,rmodel_type::r_pt_gs_k>(s,c,++mid);
        test_the_story<shyft::core::r_pm_gs_k::cell_discharge_response_t,rmodel_type::r_pm_gs_k_opt>(s,c,++mid);
        test_the_story<shyft::core::r_pm_gs_k::cell_complete_response_t,rmodel_type::r_pm_gs_k>(s,c,++mid);
        c.close();
    } catch (exception const&ex) {
        DOCTEST_MESSAGE(ex.what());
        CHECK_EQ(true,false);
    }
    s.clear();
    //MESSAGE("waiting..");
    //std::this_thread::sleep_for(from_seconds(40));
    //MESSAGE("done.");
    
}

TEST_CASE("drms_stress") {
    server s;
    s.set_listening_ip("127.0.0.1");
    auto port_no = s.start_server();
    REQUIRE_GT(port_no,0);// require vs. test.abort this part of test if we fail here
    auto test_length_in_seconds=from_seconds(3);
    try {
        auto  host_port = string("localhost:") + to_string(port_no);
        size_t n_threads=10;
        vector<future<void>> w;
        for(size_t i=0;i<n_threads;++i) {
            w.push_back(std::async(std::launch::async,
              [host_port,i,test_length_in_seconds] () {
                client c(host_port);
                auto result = c.get_version_info();
                auto mids=c.get_model_ids();
                auto t0=utctime_now()+test_length_in_seconds;
                while(utctime_now()<t0)
                    stress_test<shyft::core::pt_gs_k::cell_discharge_response_t,rmodel_type::pt_gs_k_opt>(c,i);
                c.close();
              })
            );
        }
        
        for(auto &f:w)
            f.get();
        
    } catch (exception const&ex) {
        DOCTEST_MESSAGE(ex.what());
        CHECK_EQ(true,false);
    }
    s.clear();
}
    
TEST_CASE("drms_server") {
    if(auto const *args=getenv("SHYFT_DRMS_SERVER")) {
        int port{0};
        if(sscanf(args,"%d",&port)==1 && port>=0 ) {
            MESSAGE("Starting drms server on port "<<port);
            MESSAGE("type q<enter> to terminate");
            server s;
            s.set_listening_port(port);
            auto used_port=s.start_server();
            MESSAGE("Server started on port "<<used_port);
            string cmd;
            while(true) {
                std::cin >> cmd;
                if(cmd=="q")
                    break;
            }
        } else {
            MESSAGE("Not able to get valid port number from args:"<<args);
        }
    } else {
        MESSAGE("Skipping manual drms-test, use SHYFT_DRMS_SERVER=<port_no> to start a c++ native drms-server");
    }
}    
    
}
