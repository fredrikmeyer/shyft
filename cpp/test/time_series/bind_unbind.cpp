#include "test_pch.h"
#include <shyft/time_series/time_series_dd.h>  //get all of them

using namespace shyft::time_series;
using namespace shyft::time_series::dd;
using namespace shyft::core;
using namespace shyft::time_axis;

void verify_bind_unbind(apoint_ts x) {
    FAST_CHECK_EQ(true,x.needs_bind());
    auto binfos=x.find_ts_bind_info();
    apoint_ts ats{gta_t{from_seconds(0),from_seconds(1),3},1.0,ts_point_fx::POINT_AVERAGE_VALUE};
    for(auto & bi:binfos) {
        bi.ts.bind(ats);
    }
    x.do_bind();
    FAST_CHECK_EQ(false,x.needs_bind());
    x.do_unbind();
    FAST_CHECK_EQ(true,x.needs_bind());
}
TEST_SUITE("time_series") {
TEST_CASE("bind_unbind") {
    apoint_ts a{"a"};
    apoint_ts b{"b"};
    gta_t ta{from_seconds(0),from_seconds(1),2};
    SUBCASE("ref")          {verify_bind_unbind(a);}//  aplain ref. can be bound//unboud
    SUBCASE("abs")          {verify_bind_unbind(a.abs());}
    SUBCASE("accumulate")   {verify_bind_unbind(a.accumulate(ta));}
    SUBCASE("glacier_melt") {verify_bind_unbind(create_glacier_melt_ts_m3s(a,b,1e6,6.3));}
    SUBCASE("average")      {verify_bind_unbind(a.average(ta));}
    SUBCASE("bucket")       {verify_bind_unbind(a.bucket_to_hourly(8,-1.2));}
    SUBCASE("convolve")     {verify_bind_unbind(a.convolve_w({0.3,0.5,0.4},convolve_policy::CENTER));}
    SUBCASE("decode")       {verify_bind_unbind(a.decode(1,3));}
    SUBCASE("derivative")   {verify_bind_unbind(a.derivative(derivative_method::center_diff));}
    SUBCASE("extend")       {verify_bind_unbind(a.extend(b,extend_ts_split_policy::EPS_VALUE,extend_ts_fill_policy::EPF_FILL,from_seconds(1),0.0));}
    SUBCASE("icepackrec")   {verify_bind_unbind(a.ice_packing_recession(b,ice_packing_recession_parameters{}));}
    SUBCASE("icepack")      {verify_bind_unbind(a.ice_packing(ice_packing_parameters{},ice_packing_temperature_policy::ALLOW_ANY_MISSING));}
    SUBCASE("inside")       {verify_bind_unbind(a.inside(0.0,100.0,0.0,0.0,0.0));}
    SUBCASE("integral")     {verify_bind_unbind(a.integral(ta));}
    SUBCASE("krls")         {verify_bind_unbind(a.krls_interpolation(from_seconds(3600),0.0,0.7,100));}
    //terminal value, not possible to bind SUBCASE("periodic")     {verify_bind_unbind(a.periodic(ta));}
    SUBCASE("qac_ts")       {verify_bind_unbind(a.quality_and_ts_correction(qac_parameter{},b));}
    SUBCASE("rating_curve") {verify_bind_unbind(a.rating_curve(rating_curve_parameters{}));}
    SUBCASE("repeat_ts")    {verify_bind_unbind(a.repeat(ta));}
    SUBCASE("statistics_ts"){verify_bind_unbind(a.statistics(ta,0));}
    SUBCASE("time-shift")   {verify_bind_unbind(a.time_shift(from_seconds(3)));}
    SUBCASE("spline")       {verify_bind_unbind(a.transform_spline(spline_parameter{}));}
    SUBCASE("use_tax_from") {verify_bind_unbind(a.use_time_axis_from(b));}
    SUBCASE("ts_op_ts")     {verify_bind_unbind(a+b);}
    SUBCASE("x_op_ts")      {verify_bind_unbind(3+b);}
    SUBCASE("ts_op_x")      {verify_bind_unbind(b*3);}
    SUBCASE("ts.log")       {verify_bind_unbind(a.log());}
    SUBCASE("ts.pow")       {verify_bind_unbind(a.pow(b));}


}
}
