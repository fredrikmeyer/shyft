#include "test_pch.h"

#include <cmath>
#include <vector>
#include <random>
#include <shyft/time/utctime_utilities.h>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

using shyft::time_series::dd::gta_t;
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::ts_point_fx;

namespace {
    using namespace shyft::core;

    static inline utctime time(std::string const &s) {
        return create_from_iso8601_string(s);
    }
    static double random_x() {
        static std::random_device rd;  //Will be used to obtain a seed for the random number engine
        static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
        static constexpr int max_range{10000};
        static std::uniform_int_distribution<> distrib(-max_range, max_range);
        return distrib(gen)/double(max_range);
    }

    const double y_noise=0.1;
    std::vector<double> mk_noise(gta_t const &ta) {
        std::vector<double> r;r.reserve(ta.size());
        const double x_step=2*3.1415/double(ta.size());
        for(size_t i=0;i<ta.size();++i) 
            r.push_back(std::sin(i*x_step) + y_noise*random_x());
        return r;
    };
 
}
namespace shyft::time_series::dd {

    /** @brief compute partition,overlap in size given time magnitudes
     *
     * @param partition in seconds
     * @param overlap in seconds
     * @param dt fixed dt of the time-axis
     * @note does not check for dt=0, that would give numeric seg fault, arg checks callers responsibility!
     * @return tuple, partition,overlap in size_t, dt units
     */
    inline std::tuple<size_t,size_t> partition_overlap_sizes(const utctime partition,const utctime overlap,const utctime dt) {
        return {
            static_cast<size_t>(to_seconds(partition)/to_seconds(dt)),
            static_cast<size_t>(to_seconds(overlap)/to_seconds(dt))
        };
    }

    
    /** 
     * @brief Split a time-axis into partitions with overlap
     * 
     * @details 
     * In the context of computing a fast partitioned krls algorithm.
     * Computes 'n' partial new time-axis, as partitions of the source time-axis,
     * where each time-axis have an overlap 'o'.
     *
     * The time_axis has to be a fixed-delta-t (not neccessarliy enforced?)
     *
     * The start of each partition i'th time-axis is
     * p_start =  i*p
     * 
     * The length of a complete partition is 
     * n0 = p + 2*o
     * 
     * Stability considerations:
     *
     * We want the partitions to start at same 'time', regardless evaluation period.
     * so we align the time of the start of partitions to round(t,p
     * t0 = trunc(t,delta_t*p)
     * e.g. we start on partition rounded boundary.
     * So given delta_t is 1 seconds,
     * and partion(p) is 10, and overlap(o) is 2 then
     * partions starts at 0,10,20,30..
     * and lengths are 14.
     *
     * krls will get lengths of 14 to filter on (we know the ends, o, are unstable)
     *
     * Then we will join those partitions into one resulting ts
     *  skipping the overlap(o), 2 values, and then  take partition (p), 10 values
     *  and join those together.
     *
     * @note for now we leave the alignment to the outside-calling procedure.
     *
     * TODO: work in progress(so not where we want it yet). We need to work this class a bit to ensure that
     *       index/time space computations are consistent across
     *       the usage, as in local_do_bind, evaluate ,and partition_ts(aka value(i), value_at(i).
     *    possibly:
     *       (1) The source time-axis, that we partition into overlapping sequences
     *           do have an 'i0/t0' pair, that starts on a  partition boundary. We could keep those, possibly
     *           internalize all time/index computations.
     *       (2) the slices (start,n) should always refer to the source (so we can do source_ta.slice(start,n))
     *       (3) .compute-slice-from(t), or ix,
     */
    struct partition_ta {

        size_t i{0};///< position in the generator sequence, the state of the generator

        const size_t ta_size;///< time axis size
        const size_t p;///< partition size
        const size_t o;///< overlap size
        const size_t n;///< ta_size/p, number of partitions

        /** @brief construg partition-ta generator
         *
         * @param ta time-axis to partition
         * @param partition number of timesteps in one partition(should be 100..1000), filter window
         * @param overlap in number of timesteps should be like 10%..30% of partition size
         */
        partition_ta(size_t ta_size,
                 size_t partition,
                 size_t overlap)
        :ta_size{ta_size}
        ,p{partition}
        ,o{overlap}
        ,n{ (ta_size-2*overlap)/(partition?partition:1)} { //ensure that we produce this number of complete partitions,guard 1/0
            // enforce pre-conditions
            if(p + 2*o > ta_size)
                throw std::runtime_error("partition_ta: timeaxis size must be >= partition+2*overlap");
            if(2*o >= p)
                throw std::runtime_error("partition_ta: 2x overlap must be less than partition size");
            if(p==0||o==0)
                throw std::runtime_error("partition_ta: pa and o must both be >0");
        }

        size_t next() {return i++;}// advance sequence (no check, we trust you use this generator according to safe pattern)

        /** @return the i'th interval as tuple start_ix,length
         */
        std::tuple<size_t,size_t> value() const {
            size_t const b = i*p; // we begin partition here,(and we do not check i, we trust correct usage of generator)
            size_t e = b + (p + 2*o);//and add the required length
            // not needed, becausen ensures we generate only complete partitions. :: if(e > ta_size) e = ta_size;// limit to end of time time-axis
            if (e + p > ta_size) e = ta_size;// Set the size of e of the last fragment such that we end up at the end of the initial time axis. Thereby the last segment is longer than all the other segments.
            return {b,e-b};
        }
        operator bool () const {return i<n;} // stop condition

        /** @brief compute the partition index of the i'th value index */
        size_t partition_of(size_t ix) const {
            auto p_i = ix/p;
            if(p_i*p + p+ 2*o >ta_size && p_i>0) --p_i;// recall, the last partition might be longer(rather than shorter)
            return p_i;
        }
    };



    /** @brief fast-krls by partition the source into suitable pieces
     *
     * @details
     * The full krls suffers from performance if number of points are large.
     * The training time takes long, and uses a lot of resources.
     * This variant splits the source time-series into partitions, and then runs
     * krls on each of the partion. This uses less memory, scales linear with length of ts.
     *
     */
    struct fast_krls:ipoint_ts {
        ipoint_ts_ref ts;///< the source ts to perform fast krls on
        static constexpr size_t min_n_steps_in_partition{10u};///< a reasonable minimum limit for the filter size
        utctime partition{no_utctime};///< the partition length
        utctime overlap {0};          ///< the overlap
        double  gamma{1e-3};          ///< smaller means faster, but less accurate
        double  tol{0.01};            ///< training tolerance, smaller means more accurate, but slower
        size_t  krls_sz{100000u};     ///< memory used for undelying predictor, 0.1..1Mb
        utctime krls_dt{from_seconds(3*3600)};///< works for hourly ts, should we  use ta.dtx3?

        gta_t   ta;          ///< the computed(when bound) effective time-axis, note that it is not exactly the same as underlying tima-axis
        bool    bound{false};///< false until the ts is bound, and we have computed the effective ta

        void local_do_bind(); ///< we call this compute ta, after underlying ts is bound in the do_bind method
        void local_do_unbind();///< undo bind op
        void bind_check()const; ///< throws if not bound


        // useful constructors
        fast_krls(ipoint_ts_ref const& ts,utctime partition,utctime overlap,double gamma,double tol,size_t krls_sz,utctime krls_dt);
        // std copy ct and assign
        fast_krls()=default;

        ipoint_ts_ref partition_ts(size_t i) const;// helper fx for value(i),value_at(), return computed partition of ix i
        // implement ipoint_ts contract:

        // these are one-liners for now(later we add some checks
        ts_point_fx point_interpretation() const override {bind_check();return ts->point_interpretation();}
        void set_point_interpretation(ts_point_fx v) override {bind_check();ts_as_mutable<ipoint_ts>(ts)->set_point_interpretation(v);}
        const gta_t& time_axis() const override {bind_check();return ta;}
        utcperiod total_period() const override {bind_check();return ta.total_period();}
        size_t index_of(utctime t) const override {bind_check();return ta.index_of(t);}
        size_t size() const override {bind_check();return ta.size();}
        utctime time(size_t i) const override {bind_check();return ta.time(i);};


        double value(size_t i) const override ;
        double value_at(utctime t) const override;
        vector<double> values() const override;
        bool needs_bind() const override;
        void do_bind() override;
        void do_unbind() override;
        ipoint_ts_ref evaluate(eval_ctx& ctx, ipoint_ts_ref const& shared_this) const override;
        shared_ptr< shyft::time_series::dd::ipoint_ts > clone_expr() const override;
        void prepare(eval_ctx&ctx) const override;
        string stringify() const override;
        x_serialize_decl();
    };

    // impl fast_krls.cpp

    fast_krls::fast_krls(ipoint_ts_ref const& ts,utctime partition,utctime overlap,double gamma,double tol,size_t krls_sz,utctime krls_dt)
        :ts{ts},
        partition{partition},
        overlap{overlap},
        gamma{gamma},
        tol{tol},
        krls_sz{krls_sz},
        krls_dt{krls_dt}
    {
        if(!needs_bind()) {
            local_do_bind(); // if everything is ready, we prepare as early as possible
        }
    }


    vector<double> fast_krls::values() const {
        eval_ctx c;
        auto me=make_shared<fast_krls>(*this);// copy ct me, basically a shallow copy
        prepare(c);// prime the context,
        auto r=c.evaluate(me);
        return r->values();
    }

    void fast_krls::bind_check() const {
        if (!bound)
            throw runtime_error("fast_krls:attempting to use unbound timeseries");
    }

    bool fast_krls::needs_bind() const {
        return ts && ts->needs_bind();// could work
    }

    void fast_krls::do_bind() {
        // invoked after all binding is done
        dref(ts).do_bind();
        local_do_bind();
    }
    void fast_krls::do_unbind() {
        // invoked after all binding is done
        dref(ts).do_unbind();
        local_do_unbind();
    }
    void fast_krls::local_do_bind() {
        if(!bound) {
            ta=ts->time_axis();//initial value, just copy the underlying ta.
            if(!ta.is_fixed_dt()) {
                throw std::runtime_error("fast_krls: require source ts do be of fixed delta t resolution");
            }
            if( ta.total_period().timespan() < partition+ 2*overlap) {
                throw std::runtime_error("fast_krls: time-axis total period to be at least partition +2xoverlap");
            }
            auto const [n_steps_in_partition,n_steps_partition_overlap] = partition_overlap_sizes(partition,overlap,ta.f.delta());

            // Sanity check of parameters and bound time-series
            if(n_steps_in_partition<min_n_steps_in_partition || n_steps_partition_overlap==0 || n_steps_partition_overlap > n_steps_in_partition/2 ) {
                throw std::runtime_error("fast_krls: partition and overlap to be resonable (much larger) compared to ts delta t");
            }
            // (1) compute the effective time-axis here
            auto t0= floor(ta.time(0)+partition-utctime{1},partition) +overlap;// to nearest partition boundary overlap(we skip initial overlap)
            if(!ta.total_period().contains(t0))
                throw std::runtime_error("fast_krls: require source ts have 2 partition lengths if not aligned to partition boundary");
            auto const i0=ta.index_of(t0);
            //calendar utc;
            //MESSAGE("source ta: "<<utc.to_string(ta.total_period()));
            ta= ta.slice(i0,ta.size()-i0);// the effective size of ta, it starts at partition boundary
            //MESSAGE("local_do_bind.timeaxis:  i0="<<i0<<", t0= "<<utc.to_string(t0)<<", ta.total_period="<<utc.to_string(ta.total_period()));
            bound=true;
        }
    }
    void fast_krls::local_do_unbind() {
        if(bound) {
            bound=false;
        }
    }

    void fast_krls::prepare(eval_ctx&c) const {
        if(c.ref_counting(this)>1u)
            return;
        if(ts)
            ts->prepare(c);
    }
    ipoint_ts_ref fast_krls::partition_ts(size_t i) const {
        auto const dt=ta.f.delta();
        auto const [n_steps_in_partition,n_steps_partition_overlap] = partition_overlap_sizes(partition,overlap,dt);
        auto source_ta =ts->time_axis();
        partition_ta g{source_ta.size(),n_steps_in_partition,n_steps_partition_overlap};
        g.i=g.partition_of(i);// jump the partition_ta generator to the partition we want
        auto [start,steps] =g.value();
        apoint_ts a{ts};
        auto const i0 =source_ta.index_of(ta.time(0)-overlap);// ensure to get back to start of first partition, so -overlap
        auto s_ta=source_ta.slice(i0+start, steps);
        //calendar utc;MESSAGE("partition_ts("<<i<<")= i0="<<i0<<", start="<<start<<" .. "<<steps<<", t="<<utc.to_string(ta.time(i))<< ", period="<<utc.to_string(s_ta.total_period()));
        return a.average(s_ta).krls_interpolation(krls_dt,gamma,tol,krls_sz).evaluate().ts;// notice the evaluate
    }

    ipoint_ts_ref fast_krls::evaluate(eval_ctx& c, ipoint_ts_ref const& /*shared_this*/) const {
        if(c.is_evaluated(this))
            return c.evaluated[this];
        auto eval_ts=c.evaluate(ts);// the source might be an expression, so we flatten that out here(its now a fast gpoint_ts

        ats_vector v;// we stash up the partitions in this vector.
        auto dt=ta.f.delta();//TODO: use a common function for extracting the fixed delta-t
        auto const[n_steps_in_partition,n_steps_partition_overlap] = partition_overlap_sizes(partition,overlap,dt);
        apoint_ts a{eval_ts};//wrap it into apoint_ts so we can easily can use it
        //TODO: consider using a queue and thread-workers to do the job here. (ref. dtss:: geo read evaluator)
        auto const s_ta=ts->time_axis();
        auto const i0= s_ta.index_of(ta.time(0)-overlap);// where we start on the source time-axis(we starts overlap before the final ta)
        //MESSAGE("evaluate i0="<<i0<<", s_ta.total_period="<<s_ta.total_period().to_string());
        for(partition_ta g(s_ta.size(),n_steps_in_partition,n_steps_partition_overlap);g;g.next()) {
            auto const [start,steps] = g.value();
            auto const g_ta{s_ta.slice(i0+start, steps)};
            //MESSAGE("p"<<g.i<<": "<<start<<",.."<<steps<<", p="<<g_ta.total_period().to_string());
            v.push_back(a.average(g_ta).krls_interpolation(krls_dt,gamma,tol,krls_sz));
        }
        auto r=v.forecast_merge(overlap,partition); // notice: this will skip overlap at start, and keep the longer last elements.
        return r.ts;
    }
    double fast_krls::value(size_t i) const {
        bind_check();
        if(i>=ta.size())
            throw std::runtime_error("fast_krls: index out of range");
        return partition_ts(i)->value_at(ta.time(i));
    }

    double fast_krls::value_at(utctime t) const  {
        bind_check();
        if(!ta.total_period().contains(t))
            return nan;

        return partition_ts(index_of(t))->value_at(t); //TODO: this is unprecise if linear, and we are on the overlap boundary.
    }

    shared_ptr< shyft::time_series::dd::ipoint_ts > fast_krls::clone_expr() const {
        if(needs_bind()) {
            auto r= make_shared<fast_krls>(*this);// first just clone our self,(we will still ref the same src ts
            r->ts=ts->clone_expr();// then clone the source expression
            return r;
        }
        throw runtime_error("fast_krls: attempt to clone bound expr");
    }

    string fast_krls::stringify() const {
        return {"not yet implemented"};
    }

}
using shyft::time_series::dd::partition_ta;
using shyft::time_series::dd::partition_overlap_sizes;

TEST_SUITE("time_series") {
    TEST_CASE("fast_krls_partition_ta") {
        SUBCASE("construct_check") {
            CHECK_THROWS_AS(partition_ta(10,10,2),std::runtime_error);// to small time-axis to work with
            CHECK_THROWS_AS(partition_ta(1000,10,5),std::runtime_error);// 2xoverlap larger or equal to partition size
            CHECK_THROWS_AS(partition_ta(1000,10,0),std::runtime_error);// 2xoverlap larger or equal to partition size
            CHECK_THROWS_AS(partition_ta(1000,0,0),std::runtime_error);// 2xoverlap larger or equal to partition size

        }
        SUBCASE("partition_overlap_sizes") {
            CHECK_EQ(std::make_tuple(10,2),partition_overlap_sizes(from_seconds(10),from_seconds(2),from_seconds(1)));
        }
        SUBCASE("generator") {
            const size_t overlap{2};
            const size_t partition{10};
            for(size_t n_partitions{1};n_partitions<3;++n_partitions) {// test for 1.. 3 partitions(covers all cases really)
                for(size_t e{0};e<partition;++e) { // we test execessive elements 0..partition size.
                    size_t c{0};// count number of iteration the generator gives us
                    for(partition_ta g(partition*n_partitions+2*overlap,partition,overlap);g;g.next()) {
                        auto [b,n]=g.value();
                        FAST_CHECK_EQ(b,c*partition);// start of partition
                        if (n_partitions!=c+1) { // if n_partitions=c+1, then we have the last fragment of the timeseries.
                            FAST_CHECK_EQ(n,partition + 2*overlap);// and the lengths are all constants
                        // prove that partition_of(ix) for ix in index range of b+o..+n  are equal to g.i
                            for(auto ix=b;ix<b+n-2*overlap;++ix) {
                                auto p_i=g.partition_of(ix);
                                if(p_i!=g.i) {
                                    MESSAGE("partition_of("<<ix<<"): "<<p_i<<"!="<<g.i<<", b="<<b<<"..+"<<n<<", for case n_partitions="<<n_partitions<<", excess="<<e);
                                    FAST_CHECK_EQ(g.partition_of(ix),g.i);
                                }
                            }
                        } else {
                            FAST_CHECK_GE(n,partition + 2*overlap);// last partition is longer.
                            for(auto ix=b;ix<b+n;++ix) // prove that we can compute partition of last part, using supplied index
                                FAST_CHECK_EQ(g.partition_of(ix),g.i);
                        }
                        ++c;
                    }
                    FAST_CHECK_EQ(c,n_partitions);
                }
            }
        }
        SUBCASE("mk_time_axis") {
            // given
            //
            //  partition[s],
            //  overlap [s]
            //  src.time_axis, with fixed dt, .total_period(), e.g tot_period[s,s]
            // construct an aligned time-axis, ta,
            // where
            //  ta.time(0) is aligned to partition boundary
            //
            // this time-axis, is to be combined with the generator
            // to provide slices that are references to stable partition boundaries
            //
            auto n_time_steps = 300*24u;
            auto dt=deltahours(1);
            gta_t ta {time("2000-01-01T00:00:00Z"), dt, n_time_steps};
            auto p_dt=deltahours(24*10);
            const calendar utc;
            auto p_a = ta.total_period().trim(utc,p_dt,shyft::core::trim_policy::TRIM_IN); // this ensures p_a is contained in total_period, but aligned to partition boundaries..
            auto i0= ta.index_of(p_a.start);// the index where we start
            auto i1= ta.index_of(p_a.end);
            FAST_CHECK_GE(i0,0);
            FAST_CHECK_LT(i1,n_time_steps);
            //MESSAGE("i0="<<i0<<", i1="<<i1);
            CHECK( ta.time(i0)-ta.total_period().start < p_dt); // ensure the trimmed away part is less than a partition_dt
            CHECK( ta.total_period().end-ta.time(i1) < p_dt);

        }
    }
    TEST_CASE("ported_krls_py_demo") {
        auto n_time_steps = 10*24u;
        // partition parameters
        auto n_steps_in_partition=n_time_steps/24u; // here the time-axis is split into equally long segments
        auto n_steps_partition_overlap =n_steps_in_partition/10u;
        auto dt=deltahours(1);
        auto t0 = floor(time("2000-01-01T00:00:00Z"),dt*n_steps_in_partition);
        gta_t ta {t0, dt, n_time_steps};
        // raw signal timeseries a, hourly, with noise signal 
        auto a = apoint_ts(ta,mk_noise(ta) , ts_point_fx::POINT_AVERAGE_VALUE);

        // gamma parameters, suitable for this case
        // it takes some experience to adjust them.
        double gamma{1e-3};// smaller means faster, but less accurate
        double tol=0.01; // training tolerance, smaller means more accurate, but slower
        size_t krls_sz=100000u;// memory used for undelying predictor, 0.1..1Mb
        
        auto krls_dt=3*dt;// should be in range 3..8 hours for hourly ts.
        
        SUBCASE("full_krls_over_total_period") {
            auto m0=utctime_now();
            auto b = a.krls_interpolation(krls_dt,gamma,tol,krls_sz);
            auto m1=utctime_now();
            MESSAGE("krls_full time_used:"<<to_seconds(m1-m0));
        }
        

        
        SUBCASE("partitioned_krls") {
            using shyft::time_series::dd::fast_krls;
            auto fk_ts=std::make_shared<const fast_krls>(a.ts,n_steps_in_partition*dt,n_steps_partition_overlap*dt,gamma, tol,krls_sz,krls_dt);
            apoint_ts fk{fk_ts};
            auto m0=utctime_now();
            auto c{fk.evaluate()};
            auto m1=utctime_now();
            calendar utc;
            MESSAGE("krls_partition time_used:"<<to_seconds(m1-m0));
            auto b = a.krls_interpolation(krls_dt,gamma,tol,krls_sz);// full krls
            FAST_CHECK_EQ(utc.to_string(a.total_period().end),utc.to_string(c.total_period().end)); // We skip the starting overlap
            FAST_CHECK_EQ(a.size(),c.size()+n_steps_partition_overlap); // The resulting ts will be overlap shorter as of now
            auto diff = b-c;
            auto diff_values = diff.values();
            auto diff2 = a-c;
            auto diff2_values = diff2.values();
            FAST_REQUIRE_GT(diff_values.size(),0);
            const double diff_limit=y_noise; //2*0.08;// we have sine amp 1.0, noise 0.1, needs a study to ensure tests cover wanted fx
            for(auto i=0u;i<diff_values.size();++i) {
                WARN(std::fabs(diff_values[i])<=diff_limit); // The difference between b and c should not exceed the noise level. In other words, any value at the overlap (and elsewhere) in c should not exceed the noise level based on the timeseries b.
                if(std::fabs(diff_values[i])>diff_limit) { // just to get printouts of values etc.
                    MESSAGE("diff found at "<<i<<" "<<diff_values[i]);
                    //FAST_CHECK_LT(std::fabs(diff_values[i]),diff_limit); // Each value of the resulting krls-filtered time series should be within the noise range.
                }
                WARN(std::fabs(diff2_values[i])<=2*diff_limit); // The time-series c should have values not deviating more than 2(?)*noise from time-series a.
            }
            /* verify .value(i) gives same as values()[i] */ {
                FAST_CHECK_EQ(fk.time_axis(),c.time_axis());
                for(size_t i=0u;i<c.size();++i) {
                    const double fk_v=fk.value(i);
                    const double c_v=c.value(i);
                    const double diff=fk_v-c_v;
                    if(std::fabs(diff)>0.000001) {
                        MESSAGE("differs at "<<i<<", fk="<<fk_v<<" vs "<<c_v<<" diff="<<diff);
                    }
                    CHECK_EQ(fk_v,doctest::Approx(c_v));
                }
            }

        }
    }
}
