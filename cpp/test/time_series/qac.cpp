#include "test_pch.h"
#include <shyft/time_series/dd/qac_ts.h>
#include <shyft/time_series/dd/apoint_ts.h>


using shyft::time_series::dd::apoint_ts;
using time_axis=shyft::time_series::dd::gta_t;
using shyft::core::utctime;
using shyft::core::calendar;
using shyft::core::deltahours;
using shyft::time_series::ts_point_fx;
using std::vector;
using std::isfinite;

TEST_SUITE("time_series") {
    TEST_CASE("qac_speed") {
            // purpose to verify speed of qac(ts..).values
            // to be as fast as ts
            //--
            auto t0=calendar().time(2010,1,1);
            auto n= 24*365*10;
            auto dt=deltahours(1);
            apoint_ts a_1(time_axis(t0,dt,n),1.0,ts_point_fx::POINT_AVERAGE_VALUE);
            apoint_ts a_nan(time_axis(t0,dt,n),shyft::nan,ts_point_fx::POINT_AVERAGE_VALUE);
            apoint_ts z(time_axis(t0,dt*n,1),0.0,ts_point_fx::POINT_AVERAGE_VALUE);


            auto q_1=a_1.min_max_check_ts_fill(shyft::nan,shyft::nan,dt,z);
            auto t_0 =timing::now();
            auto v_1=q_1.values();
            auto t_1 =elapsed_us(t_0,timing::now());
            auto q_nan=a_nan.min_max_check_ts_fill(shyft::nan,shyft::nan,dt,z);
            auto t_2= timing::now();
            auto v_nan=q_nan.values();
            auto t_3= elapsed_us(t_2,timing::now());

            MESSAGE("No check time:"<<t_1<< ", fill_time:"<<t_3<<" [us]");
            WARN_GT(t_3,t_1);// no fill should be faster than with-fill (approx 2x )
    }

    TEST_CASE("qac_ts_fill_algo") {
        auto   ta = time_axis(0, 1, 5);
        auto ts_src = apoint_ts(ta, vector<double>{1.0, -1.0, 2.0,shyft::nan, 4.0},ts_point_fx::POINT_AVERAGE_VALUE);
        auto  cts = apoint_ts(ta,vector<double>{1.0, 1.8, 2.0, 2.0, 4.0}, ts_point_fx::POINT_AVERAGE_VALUE);
        auto  ts_qac = ts_src.min_max_check_ts_fill(-10.0,10.0,300, cts);
        CHECK_EQ(ts_qac.value(3), doctest::Approx(2.0));
        ts_qac = ts_src.min_max_check_ts_fill( 0.0,10.0,300,cts);
        CHECK_EQ(ts_qac.value(1), doctest::Approx(1.8));  // -1 out, replaced with linear between
        CHECK_EQ(ts_qac.value(3), doctest::Approx(2.0));
        auto v=ts_qac.values();// ensure these are equal
        for(size_t i=0;i<ts_qac.size();++i) {
            CHECK_EQ(v[i],doctest::Approx(ts_qac.value(i)));
        }

    }
    TEST_CASE("qac_ts_fill_stair_case_algo") {
        auto   ta = time_axis(0, 1, 5);
        auto ts_src = apoint_ts(ta, vector<double>{1.0, -1.0, 2.0,shyft::nan, 4.0},ts_point_fx::POINT_AVERAGE_VALUE);
        auto  ts_qac = ts_src.min_max_check_linear_fill(0.0,3.0,300);
        auto qv=ts_qac.values();

        CHECK_EQ(ts_qac.value(0), doctest::Approx(1.0));
        CHECK_EQ(ts_qac.value(1), doctest::Approx(1.0));
        CHECK_EQ(ts_qac.value(2), doctest::Approx(2.0));
        CHECK_EQ(ts_qac.value(3), doctest::Approx(2.0));
        CHECK_EQ(ts_qac.value(4), doctest::Approx(2.0));
        for(size_t i=0;i<ts_qac.size();++i) {
            CHECK_EQ(qv[i],doctest::Approx(ts_qac.value(i)));
        }

    }
    TEST_CASE("qac_dt_max_stair_case") {
        auto t0=calendar().time(2018,1,1);
        auto dt=deltahours(1);
        auto ta = time_axis(vector<utctime>{t0,t0+dt,t0+3*dt},t0+4*dt);
        auto ts_src = apoint_ts(ta, vector<double>{0.0, 1.0, 3.0},ts_point_fx::POINT_AVERAGE_VALUE);
        auto  ts_qac = ts_src.min_max_check_linear_fill(0.0,10.0,dt);
        CHECK_EQ(ts_qac.time_axis().size(),4);// because it need to stretch out a point at t+2dt
        auto v=ts_qac.values();
        CHECK_EQ(v[0],doctest::Approx(0.0));
        CHECK_EQ(v[1],doctest::Approx(1.0));
        CHECK_EQ(ts_qac(t0+2*dt-utctime(1)),doctest::Approx(v[1]));// still valid here
        auto v_e=ts_qac(t0+2*dt+utctime(1));
        CHECK_EQ(isfinite(v_e),false);// exactly invalid here
        v_e=ts_qac(t0+2*dt+utctime(0));
        CHECK_EQ(isfinite(v_e),false);// exactly invalid here
        v_e=ts_qac(t0+2*dt-utctime(1));
        CHECK_EQ(isfinite(v_e),true);// exactly invalid here
        CHECK_EQ(isfinite(v[2]),false);
        CHECK_EQ(v[3],doctest::Approx(3.0));
    }
    TEST_CASE("qac_dt_max_linear") {
        auto t0=calendar().time(2018,1,1);
        auto dt=deltahours(1);
        auto ta = time_axis(vector<utctime>{t0,t0+dt,t0+3*dt,t0+4*dt},t0+5*dt);
        auto ts_src = apoint_ts(ta, vector<double>{0.0, 1.0, 3.0,4.0},ts_point_fx::POINT_INSTANT_VALUE);
        auto  ts_qac = ts_src.min_max_check_linear_fill(0.0,10.0,dt);
        CHECK_EQ(ts_qac.time_axis().size(),5);// because it need to place apoint at t0+dt+epsilon
        auto v=ts_qac.values();
        CHECK_EQ(v[0],doctest::Approx(0.0));
        CHECK_EQ(v[1],doctest::Approx(1.0));
        CHECK_EQ(ts_qac(t0+1*dt-utctime(1)),doctest::Approx(v[1]));// still valid here
        auto v_e=ts_qac(t0+1*dt+utctime(1));
        CHECK_EQ(isfinite(v_e),false);// exactly invalid here
        v_e=ts_qac(t0+1*dt+utctime(0));
        CHECK_EQ(isfinite(v_e),true);// exactly valid here
        v_e=ts_qac(t0+1*dt-utctime(1));
        CHECK_EQ(isfinite(v_e),true);// exactly valid here
        CHECK_EQ(isfinite(v[2]),false);
        CHECK_EQ(v[3],doctest::Approx(3.0));
        CHECK_EQ(v[4],doctest::Approx(4.0));
    }
    TEST_CASE("qac_ts_fill_speed") {
        const size_t n=100000;
        vector<utctime> t;t.reserve(n);
        for(auto tx=0;tx<n;++tx) t.emplace_back(utctime{tx});
        auto ta = time_axis(t,utctime{n});

        auto ts_src = apoint_ts(ta, shyft::nan,ts_point_fx::POINT_AVERAGE_VALUE);
        auto  cts = apoint_ts(ta,1.0, ts_point_fx::POINT_AVERAGE_VALUE);
        auto q1=ts_src.min_max_check_ts_fill(-10.0,10.0,300, cts)*cts*1.0+0.0*cts.pow(1.0);
        auto q2=cts.use_time_axis_from(q1);

        auto t0=shyft::core::utctime_now();
        auto  ts_qac = ts_src.min_max_check_ts_fill(-10.0,10.0,300, q2).evaluate();
        auto t_used=shyft::core::utctime_now()-t0;
        for(auto x:ts_qac.values())
            CHECK_EQ(x, doctest::Approx(1.0));
        MESSAGE("qac used:"<<shyft::core::to_seconds(t_used)*1000.0<<" ms, to fill "<<n<<" values");

    }
}
