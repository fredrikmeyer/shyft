#include "test_pch.h"

#include <shyft/time_series/bin_op_eval.h>

using namespace shyft;
using namespace shyft::time_series;
using ts_t=point_ts<gta_t>;
using std::make_shared;
namespace shyft::time_series {
    namespace detail {

    }
}

namespace test::time_series {
//helpers to keep tests complete and short

template<class TA>
void verify_fxx_step(ts_t &a,TA const&ta) {
    detail::fxx_step x(a,ta);
    x.init(ta.time(0));
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(0)));
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(0)+seconds(5)));
    FAST_CHECK_EQ(doctest::Approx(2.0),x(ta.time(1)));
    FAST_CHECK_EQ(doctest::Approx(3.0),x(ta.time(2)));
    FAST_CHECK_EQ(false,isfinite(x(ta.total_period().end)));// ensure we are false at end point.
    a.set(1,shyft::nan);// insert nan, and check how it works
    x.init(ta.time(0));// re-init
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(0)+seconds(5)));
    FAST_CHECK_EQ(false,isfinite(x(ta.time(1)+seconds(1))));
    FAST_CHECK_EQ(doctest::Approx(3.0),x(ta.time(2)+seconds(5)));
    a.set(1,2.0);// set back value
    x.init(ta.time(0)-seconds(1));// before should give nan for all
    FAST_CHECK_EQ(false,isfinite(x(ta.time(0))));
    FAST_CHECK_EQ(false,isfinite(x(ta.time(1))));
    FAST_CHECK_EQ(false,isfinite(x(ta.time(2))));
}
template<class TA>
void verify_fxx_lin(ts_t &a,TA const&ta) {
    detail::fxx_lin x(a,ta);
    x.init(ta.time(0));
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(0)));
    FAST_CHECK_EQ(doctest::Approx(1.5),x(ta.time(0)+seconds(5)));
    FAST_CHECK_EQ(doctest::Approx(2.0),x(ta.time(1)));
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(0)));
    FAST_CHECK_EQ(doctest::Approx(3.0),x(ta.time(2)));
    FAST_CHECK_EQ(doctest::Approx(3.0),x(ta.time(2)+seconds(5)));
    FAST_CHECK_EQ(doctest::Approx(3.0),x(ta.period(2).end-utctime{1}));//  still valid here
    FAST_CHECK_EQ(false,isfinite(x(ta.period(2).end)));// but not at the end.
    FAST_CHECK_EQ(false,isfinite(x(ta.period(2).end+seconds(3))));

    a.set(1,shyft::nan);// insert nan, and check how it works
    x.init(ta.time(0));
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(0)+seconds(5)));
    FAST_CHECK_EQ(doctest::Approx(1.0),x(ta.time(1)-utctime{1}));// exact cut of at next interval
    FAST_CHECK_EQ(false,isfinite(x(ta.time(1))));
    FAST_CHECK_EQ(false,isfinite(x(ta.time(1)+seconds(1))));
    FAST_CHECK_EQ(doctest::Approx(3.0),x(ta.time(2)+seconds(5)));
    a.set(1,2.0);
}



}
using namespace test::time_series;
TEST_SUITE("time_series") {
    auto calx=make_shared<calendar>();    
    time_axis::fixed_dt f(seconds(0),seconds(10),3);
    time_axis::calendar_dt c(calx,seconds(0),seconds(10),3);
    time_axis::point_dt p(vector<utctime>{seconds(0),seconds(10),seconds(20)},seconds(30));
    gta_t g(seconds(0),seconds(10),3);
    constexpr const auto step=ts_point_fx::POINT_AVERAGE_VALUE;
    constexpr const auto lin= ts_point_fx::POINT_INSTANT_VALUE;
    vector<double> v{1.0,2.0,3.0};
    ts_t a(g,v,step);
    ts_t b(g,v,step);
    
    ts_t a_f(g,v,step);
    ts_t a_l(g,v,lin);
    TEST_CASE("fxx_step") {
        verify_fxx_step(a_f,f);
        verify_fxx_step(a_f,c);
        verify_fxx_step(a_f,p);
    }
    TEST_CASE("fxx_lin") {
        verify_fxx_lin(a_f,f);
        verify_fxx_lin(a_f,c);
        verify_fxx_lin(a_f,p);
    }
    TEST_CASE("fx_bin_op") {
        detail::fxx_step aa(a_f,f);
        detail::fxx_lin  bb(a_f,p);
        
        SUBCASE("empty_ta_gives_empty_v") {
            gta_t empty;
            FAST_CHECK_EQ(0,detail::fxx_bin_op(empty,aa,std::plus<double>(),bb).size());
        }
        SUBCASE("add_two_gives_2x") {
            auto c=time_axis::combine(f,p);
            auto r1=detail::fxx_bin_op(c,aa,std::plus<double>(),bb);
            FAST_CHECK_EQ(3,r1.size());
            for(size_t i=0;i<r1.size();++i) {
                FAST_CHECK_EQ(doctest::Approx(2*v[i]),r1[i]);
            }
        }
        SUBCASE("different_time_axis") {
            gta_t l_ta(vector<utctime>{seconds(0),seconds(2),seconds(10)},seconds(11));
            vector<double> l_v{0.0,2.0,10.0};
            ts_t l(l_ta,l_v,lin);
            gta_t r_ta(seconds(1),seconds(1),3);
            vector<double> r_v{1.0,2.0,3.0};
            ts_t r(r_ta,r_v,step);
            vector<double> e_v{2.0,4.0,6.0};
            gta_t e_ta{vector<utctime>{seconds(1),seconds(2),seconds(3)},seconds(4)};
            detail::fxx_lin aa(l,l_ta.p);
            detail::fxx_step bb(r,r_ta.f);
            auto c=time_axis::combine(l_ta,r_ta);
            auto r1=detail::fxx_bin_op(c.p,aa,std::plus<double>(),bb);
            FAST_CHECK_EQ(r1.size(),e_v.size());
            for(size_t i=0;i<e_v.size();++i) {
                //DOCTEST_MESSAGE(i<<"# "<<to_seconds(c.time(i))<<" rv="<<r1[i]
                //    <<" lin "<<aa.value(c.time(i))<<" + "<< bb.value(c.time(i))<<"= "<<e_v[i]<< "\n"
                //);
                FAST_CHECK_EQ(doctest::Approx(e_v[i]),r1[i]);
            }
        }
    }
    TEST_CASE("one_axis_double_freq") {
        gta_t ta1{seconds(0),seconds(10),2};
        gta_t ta2{seconds(0),seconds(5),4};
        ts_t a(ta1,vector<double>{1.0,    0.0},step);
        ts_t b(ta2,vector<double>{0.0,0.0,1.0,1.0},step);
        auto r=bin_op_eval<ts_t>(a,std::plus<double>(),b);
        ts_t e(ta2,vector<double>{1.0,1.0,1.0,1.0},step);
        //for(size_t i=0;i<e.size();++i) {
        //    DOCTEST_MESSAGE(i<<"# "<<to_seconds(e.time(i))<<" rv="<<r.value(i) <<" e "<<e.value(i) );
        //}
        FAST_CHECK_EQ(r,e);
    }
    TEST_CASE("fxx_step_speed") {
        size_t n=100000; // 100k points ~ 10 years hourly
        gta_t ta_a{seconds(0),seconds(10),n};
        gta_t ta_c{seconds(0),seconds(5),2*n};
        ts_t a{ta_a,vector<double>(n,1.0),step};
        ts_t b{ta_c,vector<double>(n*2,1.0),step};
        size_t n_ts=10;
        double sum=0;
        auto t0=utctime_now();
        for(size_t j=0;j<n_ts;++j) {
            detail::fxx_step fx{a,a.time_axis().f};// 
            fx.init(ta_c.time(0));
            for(size_t i=0;i<ta_c.size();++i) {
                sum += fx(ta_c.f._time(i));
            }
        }
        auto t1=utctime_now();
        CHECK(sum>0.0);
        DOCTEST_MESSAGE("fxx_step access: "<< n_ts*2*n/to_seconds(t1-t0)/1e6<<" mill pts/sec");
        t0=utctime_now();
        for(size_t j=0;j<n_ts;++j) {
            detail::fxx_step fa{a,a.time_axis().f};// 
            detail::fxx_step fb{b,b.time_axis().f};// 
            auto r=detail::fxx_bin_op(ta_c.f,fa,std::plus<double>(),fb);
            sum += double(r.size());
        }
        t1=utctime_now();
        CHECK(sum>0.0);
        DOCTEST_MESSAGE("fxx_step binop new: "<< 2*n_ts*2*n/to_seconds(t1-t0)/1e6<<" mill pts/sec");
    }
    TEST_CASE("fxx_lin_speed") {
        size_t n=100000; // 100k points ~ 10 years hourly
        gta_t ta_a{seconds(0),seconds(10),n};
        gta_t ta_c{seconds(0),seconds(5),2*n};
        ts_t a{ta_a,vector<double>(n,1.0),lin};
        ts_t b{ta_c,vector<double>(n*2,1.0),lin};
        size_t n_ts=10;
        double sum=0;
        auto t0=utctime_now();
        for(size_t j=0;j<n_ts;++j) {
            detail::fxx_lin fx{a,a.time_axis().f};// 
            fx.init(ta_c.time(0));
            for(size_t i=0;i<ta_c.size();++i) {
                sum += fx(ta_c.f._time(i));
            }
        }
        auto t1=utctime_now();
        CHECK(sum>0.0);
        DOCTEST_MESSAGE("fxx_lin  access: "<< n_ts*2*n/to_seconds(t1-t0)/1e6<<" mill pts/sec");
        t0=utctime_now();
        for(size_t j=0;j<n_ts;++j) {
            detail::fxx_lin fa{a,a.time_axis().f};// 
            detail::fxx_lin fb{b,b.time_axis().f};// 
            auto r=detail::fxx_bin_op(ta_c.f,fa,std::plus<double>(),fb);
            sum += double(r.size());
        }
        t1=utctime_now();
        CHECK(sum>0.0);
        DOCTEST_MESSAGE("fxx_lin binop new: "<< 2*n_ts*2*n/to_seconds(t1-t0)/1e6<<" mill pts/sec");
    }

}
