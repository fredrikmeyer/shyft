#include "test_pch.h"
#include <shyft/time_series/time_series_dd.h>

using namespace shyft::core;

using namespace shyft::time_series::dd;
using std::vector;
using std::make_shared;
using shyft::time_series::ts_point_fx;
using clone_exception=std::runtime_error;
using std::dynamic_pointer_cast;

namespace test::time_series {
    using std::dynamic_pointer_cast;    
    
    template <class T,class Fx>    
    bool verify_clone_expr(Fx&&fx) { // fx like [](apoint_ts const&a)->apoint_ts {return a.average(..);}
        gta_t ta(from_seconds(0),from_seconds(10),10);
        apoint_ts x{ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts a{"a"};
        auto o = fx(a);// returns something we can do ->ts on
        auto o_o = dynamic_pointer_cast<T>(o.ts);
        auto c = dynamic_pointer_cast<T>(o.clone_expr().ts);// returns shared_ptr<ipoint_ts>
        CHECK_NE(o_o->ts.get(),c->ts.get());// should be different!
        a.bind(x);
        o.do_bind();
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
        return true;
        // we could verify that after bind on c, that o == c
    }
    template <class T,class Fx>    
    bool verify_clone_lhs_rhs_expr(Fx&&fx) { // fx like [](apoint_ts const&a)->apoint_ts {return a.average(..);}
        gta_t ta(from_seconds(0),from_seconds(10),10);
        apoint_ts x{ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE};
        apoint_ts a{"a"};
        apoint_ts b{"b",x};
        auto o = fx(a,b);
        auto o_o = dynamic_pointer_cast<T>(o.ts);
        auto c_o = dynamic_pointer_cast<T>(o.clone_expr().ts);
        CHECK_NE(o_o->lhs.ts.get(),c_o->lhs.ts.get());// lhs should be a copy
        CHECK_EQ(o_o->rhs.ts.get(),c_o->rhs.ts.get());// rhs should be a ref to same
        a.bind(x);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
        return true;
        // we could verify that after bind on c, that o == c
    }
    
}
using namespace test::time_series;

TEST_SUITE("time_series") {
    gta_t ta(from_seconds(0),from_seconds(10),10);
    apoint_ts x{ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE};
    double xd{42.0};
    TEST_CASE("clone_expr_gpoint_ts") {
        gpoint_ts a(ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE);
        CHECK_THROWS_AS(a.clone_expr(),clone_exception);
    }
    TEST_CASE("clone_expr_periodic_ts") {
        periodic_ts a(vector<double>{1,2},from_seconds(2),ta);
        CHECK_THROWS_AS(a.clone_expr(),clone_exception);
    }

    TEST_CASE("clone_expr_aref_ts") {
        aref_ts a{"a"};
        //gpoint_ts a(ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE);
        auto b=a.clone_expr();
        CHECK_NE(b.get(),&a);
        CHECK_EQ(a.id,dynamic_pointer_cast<aref_ts>(b)->id);
        a.rep=make_shared<gpoint_ts>(ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE);
        CHECK_THROWS_AS(a.clone_expr(),clone_exception);
    }
    TEST_CASE("clone_expr_abin_op_ts") {
        verify_clone_lhs_rhs_expr<const abin_op_ts>([](apoint_ts a,apoint_ts b)->apoint_ts {return a+b;});
    }
    TEST_CASE("clone_expr_use_time_axis_from_ts") {
        verify_clone_lhs_rhs_expr<const use_time_axis_from_ts>([](apoint_ts a,apoint_ts b)->apoint_ts {return a.use_time_axis_from(b);});
    }
    TEST_CASE("clone_expr_extend_ts") {
        verify_clone_lhs_rhs_expr<const extend_ts>([](apoint_ts a,apoint_ts b)->apoint_ts {return a.extend(b,extend_ts_split_policy::EPS_LHS_LAST,extend_ts_fill_policy::EPF_FILL,from_seconds(10),4.0);});
    }
    
    TEST_CASE("clone_expr_abin_op_scalar_ts") {
        apoint_ts a{"a"};
        auto o=xd+a;
        auto c=o.clone_expr();
        auto o_o=dynamic_pointer_cast<const abin_op_scalar_ts>(o.ts);
        auto c_o=dynamic_pointer_cast<const abin_op_scalar_ts>(c.ts);
        CHECK_EQ(o_o->lhs,doctest::Approx(c_o->lhs));// lhs should be a copy
        CHECK_NE(o_o->rhs.ts.get(),c_o->rhs.ts.get());// 
        a.bind(x);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);// apoint_ts always succeeds, so use underlying ts
    }
    TEST_CASE("clone_expr_abin_op_ts_scalar") {
        apoint_ts a{"a"};
        auto o=a+xd;
        auto c=o.clone_expr();
        auto o_o=dynamic_pointer_cast<const abin_op_ts_scalar>(o.ts);
        auto c_o=dynamic_pointer_cast<const abin_op_ts_scalar>(c.ts);
        CHECK_EQ(o_o->rhs,doctest::Approx(c_o->rhs));// lhs should be a copy
        CHECK_NE(o_o->lhs.ts.get(),c_o->lhs.ts.get());// 
        a.bind(x);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);// apoint_ts always succeeds, so use underlying ts
    }
    TEST_CASE("clone_expr_average_ts") {
        verify_clone_expr<const average_ts>([tavg=gta_t{ta}](apoint_ts const&a) {return a.average(tavg);});
    }
    TEST_CASE("clone_expr_stat_ts") {
        verify_clone_expr<const statistics_ts>([tavg=gta_t{ta}](apoint_ts const&a) {return a.statistics(tavg,50);});
    }
    TEST_CASE("clone_expr_integral_ts") {
        verify_clone_expr<const integral_ts>([tavg=gta_t{ta}](apoint_ts const&a) {return a.integral(tavg);});
    }
    TEST_CASE("clone_expr_derivative_ts") {
        verify_clone_expr<const derivative_ts>([](apoint_ts const&a) {return a.derivative();});
    }
    TEST_CASE("clone_expr_accumulate_ts") {
        verify_clone_expr<const accumulate_ts>([ata=gta_t{ta}](apoint_ts const&a) {return a.accumulate(ata);});
    }
    TEST_CASE("clone_expr_repeat_ts") {
        verify_clone_expr<const repeat_ts>([ata=gta_t{ta}](apoint_ts const&a) {return a.repeat(ata);});
    }
    TEST_CASE("clone_expr_abs_ts") {
        verify_clone_expr<const abs_ts>([](apoint_ts const&a) {return a.abs();});
    }
    TEST_CASE("clone_expr_time_shift_ts") {
        verify_clone_expr<const time_shift_ts>([](apoint_ts const&a) {return a.time_shift(from_seconds(10));});
    }
    TEST_CASE("clone_expr_inside_ts") {
        verify_clone_expr<const inside_ts>([](apoint_ts const&a) {return a.inside(1.0,2.0,-1.0,0.0,999.0);});
    }
    TEST_CASE("clone_expr_decode_ts") {
        verify_clone_expr<const decode_ts>([](apoint_ts const&a) {return a.decode(4,3);});
    }
    TEST_CASE("clone_expr_bucket_ts") {
        verify_clone_expr<const bucket_ts>([](apoint_ts const&a) {return a.bucket_to_hourly(4,-13.0);});
    }
    TEST_CASE("clone_expr_aglacier_melt_ts") {
        apoint_ts a{"a"};
        apoint_ts b{"b",x};
        auto o = create_glacier_melt_ts_m3s(a,b,1000,6.0);
        auto o_o = dynamic_pointer_cast<const aglacier_melt_ts>(o.ts);
        auto c_o = dynamic_pointer_cast<const aglacier_melt_ts>(o.clone_expr().ts);
        CHECK_NE(o_o->gm.temperature.get(),c_o->gm.temperature.get());// lhs should be a copy
        CHECK_EQ(o_o->gm.sca_m2.get(),c_o->gm.sca_m2.get());// rhs should be a ref to same
        a.bind(x);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
        // we could verify that after bind on c, that o == c
    }
    TEST_CASE("clone_expr_qac_ts") {
        apoint_ts a{"a"};
        apoint_ts b{"b",x};
        auto o = a.quality_and_ts_correction(qac_parameter{},b);
        auto o_o = dynamic_pointer_cast<const qac_ts>(o.ts);
        auto c_o = dynamic_pointer_cast<const qac_ts>(o.clone_expr().ts);
        CHECK_NE(o_o->ts.get(),c_o->ts.get());
        CHECK_EQ(o_o->cts.get(),c_o->cts.get());
        a.bind(x);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
        // we could verify that after bind on c, that o == c
    }
    TEST_CASE("clone_expr_convolve_w_ts") {
        apoint_ts a{"a"};
        auto o = a.convolve_w(vector<double>{0.1,0.2,0.3,0.2,0.1},shyft::time_series::convolve_policy::BACKWARD);// returns something we can do ->ts on
        auto o_o = dynamic_pointer_cast<const convolve_w_ts>(o.ts);
        auto c = dynamic_pointer_cast<const convolve_w_ts>(o.clone_expr().ts);// returns shared_ptr<ipoint_ts>
        CHECK_NE(o_o->ts_impl.ts.ts.get(),c->ts_impl.ts.ts.get());// should be different!
        a.bind(x);
        o.do_bind();
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
    }
    TEST_CASE("clone_expr_ice_packing_recession_ts") {
        apoint_ts a{"a"};
        apoint_ts b{"b",x};
        auto o = a.ice_packing_recession(b,ice_packing_recession_parameters{});
        auto o_o = dynamic_pointer_cast<const ice_packing_recession_ts>(o.ts);
        auto c_o = dynamic_pointer_cast<const ice_packing_recession_ts>(o.clone_expr().ts);
        CHECK_NE(o_o->flow_ts.ts.get(),c_o->flow_ts.ts.get());
        CHECK_EQ(o_o->ice_packing_ts.ts.get(),c_o->ice_packing_ts.ts.get());
        a.bind(x);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
        // we could verify that after bind on c, that o == c
    }
    TEST_CASE("clone_expr_ice_pack_ts") {
        apoint_ts a{"a"};
        using shyft::time_series::ice_packing_parameters;
        using shyft::time_series::ice_packing_temperature_policy;
        auto o = a.ice_packing(ice_packing_parameters{},ice_packing_temperature_policy::ALLOW_INITIAL_MISSING);
        auto o_o = dynamic_pointer_cast<const ice_packing_ts>(o.ts);
        auto c = dynamic_pointer_cast<const ice_packing_ts>(o.clone_expr().ts);// returns shared_ptr<ipoint_ts>
        CHECK_NE(o_o->ts.temp_ts.ts.get(),c->ts.temp_ts.ts.get());// should be different!
        a.bind(x);
        o.do_bind();
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
    }
    TEST_CASE("clone_expr_rating_curve_ts") {
        apoint_ts a{"a"};
        using shyft::time_series::rating_curve_parameters;
        auto o = a.rating_curve(rating_curve_parameters{});
        auto o_o = dynamic_pointer_cast<const rating_curve_ts>(o.ts);
        auto c = dynamic_pointer_cast<const rating_curve_ts>(o.clone_expr().ts);// returns shared_ptr<ipoint_ts>
        CHECK_NE(o_o->ts.level_ts.ts.get(),c->ts.level_ts.ts.get());// should be different!
        a.bind(x);
        o.do_bind();
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
    }
    //krls_interpolation(utctimespan dt, double rbf_gamma, double tol, size_t size) const;
    TEST_CASE("clone_expr_krls_interpolation_ts") {
        apoint_ts a{"a"};
        auto o = a.krls_interpolation(from_seconds(2), 0.5, 0.001, 5);
        auto o_o = dynamic_pointer_cast<const krls_interpolation_ts>(o.ts);
        auto c = dynamic_pointer_cast<const krls_interpolation_ts>(o.clone_expr().ts);// returns shared_ptr<ipoint_ts>
        CHECK_NE(o_o->ts.ts.get(),c->ts.ts.get());// should be different!
        a.bind(x);
        o.do_bind();
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
    }
    TEST_CASE("clone_expr_anary_op_ts") {
        gta_t ta(from_seconds(0),from_seconds(3600),66*2);
        apoint_ts xx{ta,1.0,ts_point_fx::POINT_AVERAGE_VALUE};
        //TODO: if we put garbage into forecast_merge we get runtime_error thrown out, consider if that is ok
        //      or if we should produce nan's in that case.
        apoint_ts a{"a"};
        apoint_ts b{"b",xx};
        ats_vector v;v.push_back(a);v.push_back(b);//{a,b};
        auto o = v.forecast_merge(from_seconds(0),from_seconds(6*3600));
        auto o_o = dynamic_pointer_cast<const anary_op_ts>(o.ts);
        auto c_o = dynamic_pointer_cast<const anary_op_ts>(o.clone_expr().ts);
        CHECK_NE(o_o->args[0].ts.get(),c_o->args[0].ts.get());
        CHECK_EQ(o_o->args[1].ts.get(),c_o->args[1].ts.get());
        a.bind(xx);
        o.do_bind();
        REQUIRE_EQ(false,o.needs_bind());
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
        // we could verify that after bind on c, that o == c
    }
    TEST_CASE("clone_expr_transform_spline_ts") {
        using shyft::time_series::dd::spline_parameter;
        using shyft::time_series::dd::xy_point_curve;
        apoint_ts a{"a"};
        vector<std::array<double, 2>> f {{0.0, 0.0}, {1.0, 1.0}, {3.0, 5.0}};
        xy_point_curve points {f};
        auto sp = shyft::time_series::dd::spline_interpolator::interpolate(points,interpolation_scheme::SCHEME_LINEAR);
        auto o = a.transform_spline(sp);
        auto o_o = dynamic_pointer_cast<const transform_spline_ts>(o.ts);
        auto c = dynamic_pointer_cast<const transform_spline_ts>(o.clone_expr().ts);// returns shared_ptr<ipoint_ts>
        CHECK_NE(o_o->ts.get(),c->ts.get());// should be different!
        CHECK_EQ(o_o->p,c->p);// should be equal
        a.bind(x);
        o.do_bind();
        CHECK_THROWS_AS(o.ts->clone_expr(),clone_exception);
    }

}    
