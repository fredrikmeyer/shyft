#pragma once
// test utilities for ui web api
#include <memory>
#include <functional>
#include <string>
#include <string_view>
#include <boost/system/error_code.hpp>

namespace boost::asio {
    class io_context;//fwd
}
namespace shyft {
    namespace energy_market {
        namespace ui {
            struct layout_info;
        }

    }
    namespace srv {
        struct model_info;
    }
}

namespace test::ui {
    struct server_impl;
    using std::string;
    using std::string_view;
    using fx_generate_json_api=std::function<string(string const&,string const&)>;
    using boost::system::error_code;
    using shyft::energy_market::ui::layout_info;
    using shyft::srv::model_info;
    
    struct server {
        explicit server(const string& root_dir);
        ~server();

        void start_web_api(string host_ip, int port, string doc_root, int fg_threads, int bg_threads);
        bool web_api_running() const ;
        void stop_web_api();
        void set_listening_ip(string host_ip);
        void set_read_cb(fx_generate_json_api const& fx);
        int  start_server();
        
        //
        void db_store_model(std::shared_ptr<layout_info> const&m,model_info const&mi);
        std::shared_ptr<layout_info> db_read_model(int64_t id);
    private:
        std::unique_ptr<server_impl> impl;
    };

    using fx_request_gen = std::function<string(string const&)>;

    namespace impl {
        struct session;
    }
    struct session : public std::enable_shared_from_this<session> {
        // Resolver and socket require an io_context
        explicit session(boost::asio::io_context& ioc);
        ~session();
        string response() const;
        string diagnostics() const;
        
        void run(string_view host, int port, string_view text, fx_request_gen const& rep_response);
      private:
        std::shared_ptr<impl::session> impl;// must be shared!
    };

    extern unsigned short get_free_port();
}
