#include <doctest/doctest.h>
#include "build_test_system.h"
#include <string>
#include <memory>
#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/aref_ts.h>
#include <shyft/time_series/dd/ipoint_ts.h>
#include <shyft/energy_market/stm/srv/dstm/server.h>

#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/stm_system.h>


TEST_SUITE("stm_system_clone") {
    using std::string;
    using namespace shyft::core;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_series::dd::aref_ts;
    using shyft::time_series::dd::ts_as_mutable;
    using shyft::time_series::dd::ts_as;
    using shyft::time_axis::fixed_dt;
    using shyft::energy_market::stm::srv::dstm::replace_model_key_in_id;
    using shyft::energy_market::stm::srv::dstm::rebind_ts;
    using shyft::energy_market::stm::srv::dstm::server;

    using shyft::energy_market::stm::reservoir;
    using shyft::energy_market::stm::unit;
    using shyft::energy_market::stm::energy_market_area;
    using shyft::energy_market::stm::stm_system;


    TEST_CASE("string_replace") {
        string old_url{"dstm://Mold_model_key/H1/R1/A1"};
        // Check that the string start with dstm://
        CHECK_EQ(0, old_url.rfind("dstm://M"));

        //Find the position of the first "/" after the prefix:
        auto rpos = old_url.find("/", 8);
        CHECK_EQ(rpos, 21);

        // Replace "old_model_key" with "new_mkey"
        string new_url = old_url.replace(8, rpos-8, "new_mkey");
        CHECK_EQ(new_url, "dstm://Mnew_mkey/H1/R1/A1");

    }

    TEST_CASE("aref_ts_id_replace") {
        apoint_ts ats("dstm://Mold_model_key/H1/R1/A1");
        aref_ts* rts = ts_as_mutable<aref_ts>(ats.ts);
        CHECK_EQ(rts != nullptr, true);
        CHECK_EQ(rts->id, "dstm://Mold_model_key/H1/R1/A1");

        auto rpos = rts->id.find("/", 8);
        rts->id = rts->id.replace(8, rpos-8, "new_mkey");
        CHECK_EQ(rts->id, "dstm://Mnew_mkey/H1/R1/A1");

        // Check that the id was changed for ats as well:
        auto arts = ts_as<aref_ts>(ats.ts);
        CHECK_EQ(arts->id, "dstm://Mnew_mkey/H1/R1/A1");
    }

    TEST_CASE("replace_model_key_function") {
        // Main case:
        apoint_ts ts1("dstm://Momk/H2/R3/A14");
        CHECK_EQ(replace_model_key_in_id(ts1, "new_mkey"), true);
        auto rts = ts_as<aref_ts>(ts1.ts);
        CHECK_EQ(rts->id, "dstm://Mnew_mkey/H2/R3/A14");

        // Non-dstm ID
        apoint_ts ts2("shyft://test/not_a_dstm_series");
        CHECK_EQ(replace_model_key_in_id(ts2, "new_mkey"), false);

        // Non-ref ts:
        apoint_ts ts3(fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
        CHECK_EQ(replace_model_key_in_id(ts3, "new_mkey"), false);

    }

    TEST_CASE("rebind_expression") {
        // Case 1: All nodes needs rebind:
        apoint_ts ts1("dstm://Momk/H1/R1/A14");
        apoint_ts ts2("dstm://Momk/H1/R2/A14");
        auto ts3 = ts1 + ts2;
        CHECK_EQ(rebind_ts(ts3, "new_mkey"), true);
        CHECK_EQ(ts1.id(), "dstm://Mnew_mkey/H1/R1/A14");
        CHECK_EQ(ts2.id(), "dstm://Mnew_mkey/H1/R2/A14");

        // Case 2: Not every node needs rebind:
        apoint_ts ts4("dstm://Momk/H1/R1/A3");
        apoint_ts ts5("shyft://test/no_rebind");
        auto ts6 = ts4 * ts5;
        CHECK_EQ(rebind_ts(ts6, "new_mkey"), true);
        CHECK_EQ(ts4.id(), "dstm://Mnew_mkey/H1/R1/A3");
        CHECK_EQ(ts5.id(), "shyft://test/no_rebind");

        // Case 3: None of the nodes needs rebind:
        apoint_ts ts7("shyft://test/no_rebind2");
        auto ts8 = ts7 - ts5;
        CHECK_EQ(rebind_ts(ts8, "new_mkey"), false);

        // Case 4: A single ts:
        apoint_ts ts9("dstm://Momk/H1");
        CHECK_EQ(rebind_ts(ts9, "new_mkey"), true);
        CHECK_EQ(ts9.id(), "dstm://Mnew_mkey/H1");

        // Case 5: A single ts without rebind:
        apoint_ts ts10("shyft://test/no_rebind3");
        CHECK_EQ(rebind_ts(ts10, "new_mkey"), false);
    }
#if 0
    TEST_CASE("rebind_stm_hps") {
        auto hps = test::create_simple_hps(1, "test-hps");
        // Case 1: Nothing to rebind:
        CHECK_EQ(false, rebind_ts(*hps, "new_mkey"));

        // Case 2: Stuff needs rebinding:
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
        rsv->level.regulation_min= apoint_ts("dstm://Momk/H1.level.regulation_min");
        rsv->level.regulation_max = apoint_ts("dstm://Momk/H1/R1") + apoint_ts("dstm://Momk/H1/R2");
        CHECK_EQ(true, rebind_ts(*hps, "new_mkey"));

        CHECK_EQ(rsv->level.regulation_min.id(), "dstm://Mnew_mkey/H1.level.regulation_min");
        auto tsis = rsv->level.regulation_max.find_ts_bind_info();
        CHECK_EQ(tsis.size(), 2);
        CHECK_EQ(tsis[0].ts.id(), "dstm://Mnew_mkey/H1/R1");
        CHECK_EQ(tsis[1].ts.id(), "dstm://Mnew_mkey/H1/R2");
    }
#endif
    TEST_CASE("rebind_stm_system") {
        auto mdl = test::create_simple_system(1, "test-mdl");
        // Let's add a market to it
        auto mkt = std::make_shared<energy_market_area>(1, "test-market", "", mdl);
        // In ids:
        mkt->price = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
        mkt->load = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
        mkt->tsm["price-alternative-1"] = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 48), 1.1);
        mkt->tsm["price-alternative-2"] = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 48), 1.2);
        // In rds:
        mkt->buy = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);

        mdl->market.push_back(mkt);

        // Case 1: Nothing to rebind
        CHECK_EQ(false, rebind_ts(*mdl, "new_mkey"));

        // Case 2: Rebind for market:
        mkt->sale = apoint_ts("dstm://Momk/M1/A5");
        mkt->max_buy = apoint_ts("dstm://Momk/M1/A2");
        mkt->tsm["price-alternative-2"] = apoint_ts("dstm://Momk/M1/A12");
        CHECK_EQ(true, rebind_ts(*mdl, "new_mkey"));
        CHECK_EQ(mkt->sale.id(), "dstm://Mnew_mkey/M1/A5");
        CHECK_EQ(mkt->max_buy.id(), "dstm://Mnew_mkey/M1/A2");
        CHECK_EQ(mkt->tsm["price-alternative-2"].id(), "dstm://Mnew_mkey/M1/A12");
        mkt->sale = apoint_ts();
        mkt->max_buy = apoint_ts();
        mkt->tsm["price-alternative-2"] = apoint_ts();
        CHECK_EQ(false, rebind_ts(*mdl, "new_mkey"));

        // Case 3: Rebind for hps:
        auto hps = mdl->hps[0];
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
        rsv->level.regulation_max = apoint_ts("dstm://Momk/H1/R1/A1");
        CHECK_EQ(true, rebind_ts(*mdl, "new_mkey"));
        CHECK_EQ(rsv->level.regulation_max.id(), "dstm://Mnew_mkey/H1/R1/A1");
    }

    TEST_CASE("stm_srv_clone") {
        server srv;
        auto mdl = test::create_simple_system(1, "test-mdl");
        auto mkt = mdl->market[0];

        // In ids:
        mkt->price = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 2), 1.0);
        mkt->load = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 48), 2.0);
        mkt->max_buy = apoint_ts("dstm://Mold_mdl/M1/A2");
        // In rds:
        mkt->buy = apoint_ts(fixed_dt(from_seconds(0), deltahours(1), 72), 3.0);
        mkt->sale = apoint_ts("dstm://Mold_mdl/M1/A5");

        auto hps = mdl->hps[0];
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
        rsv->level.regulation_max = apoint_ts("dstm://Mold_mdl/H1/R1/A1");
        // Add to server:
        srv.do_add_model("old_mdl", mdl);
        CHECK_EQ(srv.do_get_model_ids().size(), 1);
        // Clone model:
        CHECK_EQ(srv.do_clone_model("old_mdl", "new_mdl"), true);
        CHECK_EQ(srv.do_get_model_ids().size(), 2);


        auto nmdl = srv.do_get_model("new_mdl");
        hps = nmdl->hps[0];
        rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
        CHECK_EQ(rsv->level.regulation_max.id(), "dstm://Mnew_mdl/H1/R1/A1");
        mkt = nmdl->market[0];
        CHECK_EQ(mkt->max_buy.id(), "dstm://Mnew_mdl/M1/A2");
        CHECK_EQ(mkt->sale.id(), "dstm://Mnew_mdl/M1/A5");
    }
}
