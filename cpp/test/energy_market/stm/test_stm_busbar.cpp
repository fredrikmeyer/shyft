#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

namespace {
    struct test_sys {
        stm_system_ sys;
        network_ net;
        transmission_line_ t1;
        transmission_line_ t2;
        busbar_ b;
        power_module_ pm;

        test_sys() {
            sys=make_shared<stm_system>(1,"sys","{}");
            net=make_shared<network>(2,"net","{}", sys);
            sys->networks.push_back(net);
            t1=make_shared<transmission_line>(3, "t1", "{}", net);
            t2=make_shared<transmission_line>(4, "t2", "{}", net);
            net->transmission_lines.push_back(t1);
            net->transmission_lines.push_back(t2);
            b=make_shared<busbar>(6, "b", "{}", net);
            net->busbars.push_back(b);
            pm=make_shared<power_module>(7, "p", "{}", sys);
            sys->power_modules.push_back(pm);
        } 
    };
}

TEST_SUITE("stm_busbar") {

    TEST_CASE("busbar_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto net=make_shared<network>(2,"net","{}", sys);
        auto b=make_shared<busbar>(3, "b1", "{}", net);
        FAST_CHECK_UNARY(b);
        FAST_CHECK_EQ(b->id,3);
        FAST_CHECK_EQ(b->name,"b1");
        FAST_CHECK_EQ(b->json,"{}");
    }
    TEST_CASE("busbar_transmission_line_association") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto net=make_shared<network>(2,"net","{}", sys);
        sys->networks.push_back(net);
        auto t1=make_shared<transmission_line>(3, "t1", "{}", net);
        auto t2=make_shared<transmission_line>(4, "t2", "{}", net);
        net->transmission_lines.push_back(t1);
        net->transmission_lines.push_back(t2);
        auto b1=make_shared<busbar>(5, "b1", "{}", net);
        auto b2=make_shared<busbar>(6, "b2", "{}", net);
        auto b3=make_shared<busbar>(7, "b3", "{}", net);
        net->busbars.push_back(b1);
        net->busbars.push_back(b2);
        net->busbars.push_back(b3);

        // (b1) ---t1--- (b2) ---t2--- (b3)
        b1->add_to_start_of_transmission_line(t1);
        b2->add_to_end_of_transmission_line(t1);
        b2->add_to_start_of_transmission_line(t2);
        b3->add_to_end_of_transmission_line(t2);

        CHECK_EQ(b1->get_transmission_lines_from_busbar().size(), 1);
        CHECK_EQ(b1->get_transmission_lines_to_busbar().size(), 0);
        CHECK_EQ(b2->get_transmission_lines_from_busbar().size(), 1);
        CHECK_EQ(b2->get_transmission_lines_to_busbar().size(), 1);
        CHECK_EQ(b3->get_transmission_lines_from_busbar().size(), 0);
        CHECK_EQ(b3->get_transmission_lines_to_busbar().size(), 1);

        CHECK_EQ(t1->from_bb, b1);
        CHECK_EQ(t1->to_bb, b2);
        CHECK_EQ(t2->from_bb, b2);
        CHECK_EQ(t2->to_bb, b3);
    }
    TEST_CASE("busbar_power_module_association") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto net=make_shared<network>(2,"net","{}", sys);
        sys->networks.push_back(net);
        auto b1=make_shared<busbar>(5, "b1", "{}", net);
        auto b2=make_shared<busbar>(6, "b2", "{}", net);
        net->busbars.push_back(b1);
        net->busbars.push_back(b2);
        auto p1=make_shared<power_module>(8, "p1", "{}", sys);
        auto p2=make_shared<power_module>(9, "p2", "{}", sys);
        sys->power_modules.push_back(p1);
        sys->power_modules.push_back(p2);

        b1->add_to_power_module(p1);
        b2->add_to_power_module(p2);

        CHECK_EQ(p1->connected, b1);
        CHECK_EQ(p2->connected, b2);
    }


    TEST_CASE("busbar_equality") {
        test_sys sys1;
        test_sys sys2;
        CHECK_EQ(*sys1.b, *sys1.b); // Equal to self
        CHECK_EQ(*sys1.b, *sys2.b); // Equal

        SUBCASE("Transmission lines from/to busbar is equal") {
            sys1.b->add_to_start_of_transmission_line(sys1.t1);
            sys1.b->add_to_end_of_transmission_line(sys1.t2);
            sys2.b->add_to_start_of_transmission_line(sys2.t1);
            sys2.b->add_to_end_of_transmission_line(sys2.t2);
            CHECK_EQ(*sys1.b, *sys2.b);
            CHECK_EQ(*sys1.t1, *sys2.t1); // transmission lines equal
            CHECK_EQ(*sys1.t2, *sys2.t2); // transmission lines equal
            CHECK_EQ(*sys1.net, *sys2.net); // networks equal
        }
        SUBCASE("Transmission lines from busbar is not equal") {
            sys1.b->add_to_start_of_transmission_line(sys1.t2);
            CHECK_NE(*sys1.b, *sys2.b);
        }
        SUBCASE("Transmission lines to busbar is not equal") {
            sys1.b->add_to_end_of_transmission_line(sys1.t2);
            CHECK_NE(*sys1.b, *sys2.b);
        }
        SUBCASE("Power modules associations is equal") {
            sys1.b->add_to_power_module(sys1.pm);
            sys2.b->add_to_power_module(sys2.pm);
            CHECK_EQ(*sys1.b, *sys2.b);
            CHECK_EQ(*sys1.pm, *sys2.pm); // Power_modules equal
        }
        SUBCASE("Power modules associations is not equal") {
            sys1.b->add_to_power_module(sys1.pm);
            CHECK_NE(*sys1.b, *sys2.b);
        }
        SUBCASE("id_base is not equal") {
            sys1.b->id = 99;
            CHECK_NE(*sys1.b, *sys2.b);   
        }
    }
}
