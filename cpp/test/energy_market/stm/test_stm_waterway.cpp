#include <doctest/doctest.h>

#include <string>

#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/stm_system.h>

#include <test/energy_market/stm/utilities.h>


using namespace std;
using namespace shyft::energy_market::stm;
using builder = shyft::energy_market::stm::stm_hps_builder;

TEST_SUITE("stm_waterway") {

    TEST_CASE("stm_gate_equality") {
        // Test that stm::wtr::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto gate_1 = builder(hps_1).create_gate(14, "test", "");
        auto gate_2 = builder(hps_2).create_gate(14, "test", "");


        // All attributes left at default, objects should be equal
        CHECK_EQ(*gate_1, *gate_2);

        // Add some attributes to first object, no longer equal
        gate_1->discharge.static_max = test::create_t_double(1, 10.0);
        gate_1->flow_description = test::create_t_xyz(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_NE(*gate_1, *gate_2);

        // Add attributes to other object, now equal again
        gate_2->discharge.static_max = test::create_t_double(1, 10.0);
        gate_2->flow_description = test::create_t_xyz(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_EQ(*gate_1, *gate_2);
    }

    TEST_CASE("stm_waterway_equality") {
        // Test that stm::wtr::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto wtr_1 = builder(hps_1).create_waterway(11, "test", "");
        auto wtr_2 = builder(hps_2).create_waterway(11, "test", "");

        auto gate_1 = builder(hps_1).create_gate(14, "test", "");
        auto gate_2 = builder(hps_2).create_gate(14, "test", "");

        // All attributes left at default, objects should be equal
        CHECK_EQ(*wtr_1, *wtr_2);

        // Add gate to one waterway, no longer equal
        waterway::add_gate(wtr_1, gate_1);
        CHECK_NE(*wtr_1, *wtr_2);

        // Add gate to the other waterway, equal again
        waterway::add_gate(wtr_2, gate_2);
        CHECK_EQ(*wtr_1, *wtr_2);

        // Add some attributes to first object, no longer equal
        wtr_1->head_loss_coeff = test::create_t_double(1, 0.01);
        CHECK_NE(*wtr_1, *wtr_2);

        // Add attributes to other object, now equal again
        wtr_2->head_loss_coeff = test::create_t_double(1, 0.01);
        CHECK_EQ(*wtr_1, *wtr_2);

        // Add gate attribute to first object, no longer equal
        gate_1->discharge.static_max = test::create_t_double(1, 10.0);
        CHECK_NE(*wtr_1, *wtr_2);

        // Add gate attribute to other object, now equal again
        gate_2->discharge.static_max = test::create_t_double(1, 10.0);
        CHECK_EQ(*wtr_1, *wtr_2);

    }
    TEST_CASE("wtr_initial_dev_calc") {
        calendar utc;
        auto t0=utc.time(2022,1,1,0,0,0);
        utctime dt=shyft::core::deltahours(1);
        apoint_ts ref{ generic_dt{t0,dt,4}, vector<double>{1.0,2.0,3.0,4.0}, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE };
        apoint_ts act{ generic_dt{t0,dt,3}, vector<double>{1.0,2.0,4.0}, shyft::time_series::ts_point_fx::POINT_AVERAGE_VALUE };
        auto t1=act.total_period().end;
        auto acc_dev= initial_acc_deviation(t1,act, ref);
        FAST_CHECK_EQ(acc_dev, doctest::Approx(3600.0*1.0));
        FAST_CHECK_EQ(initial_acc_deviation(t0,ref,act),doctest::Approx(0.0));
        FAST_CHECK_EQ(initial_acc_deviation(shyft::core::no_utctime,ref,act),doctest::Approx(0.0));
        FAST_CHECK_EQ(initial_acc_deviation(t0,apoint_ts{},act),doctest::Approx(0.0));
        FAST_CHECK_EQ(initial_acc_deviation(t0,ref,apoint_ts{}),doctest::Approx(0.0));
        FAST_CHECK_EQ(initial_acc_deviation(t0,apoint_ts{},apoint_ts{}),doctest::Approx(0.0));
        FAST_CHECK_EQ(initial_acc_deviation(shyft::core::no_utctime,apoint_ts{},apoint_ts{}),doctest::Approx(0.0));
        FAST_CHECK_EQ(initial_acc_deviation(t1,ref.time_shift(dt),act),doctest::Approx(-3600*3.0));
        FAST_CHECK_EQ(initial_acc_deviation(t1,ref.time_shift(-dt),act),doctest::Approx(3600*2.0));

    }
}
