#include <doctest/doctest.h>
#include <memory>
#include "../serialize_loop.h"
#include <shyft/energy_market/stm/srv/task/stm_task.h>


TEST_SUITE("stm_task") {
    using namespace shyft::energy_market::stm::srv;

    TEST_CASE("model_ref_class") {
        model_ref r1{"host", 123, 456, "key"};
        CHECK_EQ(r1, model_ref{"host", 123, 456, "key"});
        CHECK_EQ(model_ref{}, model_ref{});

        CHECK_NE(r1, model_ref{"guest", 123, 456, "key"});
        CHECK_NE(r1, model_ref{"host", 321, 456, "key"});
        CHECK_NE(r1, model_ref{"host", 123, 456, "lock"});
        CHECK_NE(r1, model_ref{"host", 123, 654, "key"});
    }

    TEST_CASE("stm_case") {
        stm_case r2(1, "testcase", no_utctime);
        CHECK_EQ(r2, stm_case{1, "testcase", no_utctime});
        CHECK_NE(r2, stm_case{2, "testcase", no_utctime});
        CHECK_NE(r2, stm_case{1, "testcase", shyft::core::from_seconds(1)});

        // Adding json:
        r2.json = "misc.";
        CHECK_EQ(r2, stm_case{1, "testcase", no_utctime, "misc."});
        CHECK_NE(r2, stm_case{1, "testcase", no_utctime, "central."});

        // Adding labels:
        r2.labels.push_back("test");
        r2.labels.push_back("test2");
        CHECK_EQ(r2, stm_case{1, "testcase", no_utctime, "misc.", {"test", "test2"}});
        CHECK_EQ(r2, stm_case{1, "testcase", no_utctime, "misc.", {"test2", "test"}});
        CHECK_NE(r2, stm_case{1, "testcase", no_utctime, "misc.", {"production"}});

        // Adding model_refs:
        auto mr = std::make_shared<model_ref>("host", 123, 456, "key");
        auto mr2 = std::make_shared<model_ref>();
        r2.add_model_ref(mr);
        r2.add_model_ref(std::make_shared<model_ref>(model_ref()));
        CHECK_EQ(r2, stm_case{1, "testcase", no_utctime, "misc.",
                             {"test", "test2"}, {mr, mr2}});
        CHECK_EQ(r2, stm_case{1, "testcase", no_utctime, "misc.",
                             {"test", "test2"}, {mr2, mr}});
        CHECK_NE(r2, stm_case{1, "testcase", no_utctime, "misc.",
                             {"test", "test2"}, {mr2}});
        CHECK_EQ(mr, r2.model_refs[0]);
        CHECK_EQ(r2.model_refs.size(), 2);

        // Removing model references:
        CHECK(r2.remove_model_ref("key"));
        CHECK_EQ(r2.model_refs.size(), 1);
        CHECK_FALSE(r2.remove_model_ref("nokey"));
        CHECK(r2.remove_model_ref(""));
        CHECK_EQ(r2.model_refs.size(), 0);
    }

    TEST_CASE("stm_task") {
        // 0. Basic setup of stm_session:
        stm_task s1(1, "task0", no_utctime);
        CHECK_EQ(s1, stm_task{1, "task0", no_utctime});

        // 1. Add a run:
        auto r1 = std::make_shared<stm_case>(1, "testcase", no_utctime);
        auto r2 = std::make_shared<stm_case>(1, "testcase2", no_utctime);
        auto r3 = std::make_shared<stm_case>(2, "testcase", no_utctime);
        auto r4 = std::make_shared<stm_case>(2, "testcase2", no_utctime);

        s1.add_case(r1);
        CHECK_EQ(s1.cases.size(), 1);
        CHECK_EQ(*(s1.cases[0]), *r1);
        CHECK_THROWS(s1.add_case(r2));
        CHECK_THROWS(s1.add_case(r3));
        s1.add_case(r4);
        CHECK_EQ(s1.cases.size(), 2);
        CHECK_EQ(*(s1.cases[1]), *r4);

        // 2. Get runs:
        CHECK_EQ(nullptr, s1.get_case(3));
        CHECK_EQ(*r1, *(s1.get_case(1)));
        CHECK_EQ(nullptr, s1.get_case("norun"));
        CHECK_EQ(*r4, *(s1.get_case("testcase2")));
        // 3. remove runs:
        CHECK_FALSE(s1.remove_case(3)); // Trying to remove run with non existent ID
        CHECK_FALSE(s1.remove_case("norun")); // Trying to remove run by non existent name.

        CHECK(s1.remove_case(1));
        CHECK_EQ(s1.cases.size(), 1);
        CHECK(s1.remove_case("testcase2"));
        CHECK_EQ(s1.cases.size(),0);


        //4. update run inplace
        auto u1 = std::make_shared<stm_case>(1, "run_u1", no_utctime);
        auto u2 = std::make_shared<stm_case>(2, "run_u2", no_utctime);
        s1.add_case(u1);
        s1.add_case(u2);
        auto gc = s1.get_case(u1->id);
        gc->name = "run_test";
        gc->json = "{'key': value}";
        gc->labels.insert(gc->labels.end(), {"test", "calibration"});
        s1.update_case(*gc);
        // Make sure we haven't added a case to vector
        CHECK_EQ(s1.cases.size(), 2);
        auto updated_case = s1.get_case(u1->id);
        CHECK_EQ(updated_case->name, "run_test");
        CHECK_EQ(updated_case->json, "{'key': value}");
        CHECK_EQ(updated_case->labels, std::vector<std::string>{"test", "calibration"});



        //r->id = 9999;
        //r->name = "update_testcase";
        //r->json = "{'foo': 1}";
        //r->labels.emplace_back("test");
        //CHECK_FALSE(s1.update_case(*r));

        //auto update_r = s1.get_case(r1->id);





    }

    TEST_CASE("case_serialization") {
        auto ri = std::make_shared<model_ref>("host", 123, 456, "key");
        auto ri2 = std::make_shared<model_ref>();
        stm_case run(1, "testcase", no_utctime, "misc.",
                   {"test", "test2"}, {ri, ri2});

        auto ri3 = test::serialize_loop(*ri);
        CHECK_EQ(*ri, ri3);
        auto run2 = test::serialize_loop(run);
        CHECK_EQ(run, run2);
    }
}
