#include <doctest/doctest.h>

#include <string>

#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/stm_system.h>

#include <test/energy_market/stm/utilities.h>

#include <shyft/energy_market/a_wrap.h>

using namespace std;
using namespace shyft::energy_market::stm;
using builder = shyft::energy_market::stm::stm_hps_builder;

TEST_SUITE("stm_unit") {

    TEST_CASE("stm_unit_equality") {
        // Test that stm::plant::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto unit_1 = builder(hps_1).create_unit(14, "test", "");
        auto unit_2 = builder(hps_2).create_unit(14, "test", "");


        // All attributes left at default, objects should be equal
        CHECK_EQ(*unit_1, *unit_2);

        // Add some attributes to first object, no longer equal
        unit_1->production.static_max = test::create_t_double(1, 10.0);
        unit_1->turbine_description = test::create_t_turbine_description(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_NE(*unit_1, *unit_2);

        // Add attributes to other object, now equal again
        unit_2->production.static_max = test::create_t_double(1, 10.0);
        unit_2->turbine_description = test::create_t_turbine_description(1, vector<double>{100., 0., 0., 100., 1., 1., });
        CHECK_EQ(*unit_1, *unit_2);
    }
	
	using shyft::energy_market::sbi_t;
	using shyft::energy_market::a_wrap;
	
	TEST_CASE("stm_unit_basics") {
        const auto name{ "unit_one"s };
        auto shp=make_shared<stm_hps>(1,"sys");
        stm_hps_builder b(shp);
        string json_string("some json");
        const auto u = b.create_unit(1, name,json_string);
        CHECK_EQ(u->id, 1);
        CHECK_EQ(u->name, name);
        CHECK_EQ(u->json,json_string);
        CHECK_THROWS_AS(b.create_unit(1,name+"a",""),stm_rule_exception);
        CHECK_THROWS_AS(b.create_unit(2,name,""),stm_rule_exception);
        string s;
        sbi_t sbi(s);
        u->production.url_fx(sbi,-1,-1,".schedule");
        CHECK_EQ(s,"/H1/U1.production.schedule");
        
        // verify a_wrap at primary and secondary-nested levels
        a_wrap<apoint_ts> ups(
            [o=&u->production](sbi_t& so,int l, int tl,string_view sv)->void {
               o->url_fx(so,l,tl,sv); 
            },
            "schedule",
            u->production.schedule
        );

        CHECK_EQ(ups.url("",0,-1),".production.schedule");
        CHECK_EQ(ups.url("",0,0),".{attr_id}");
        CHECK_EQ(ups.url("",0,1),".production.schedule");

        CHECK_EQ(ups.url("",1,-1),"/U1.production.schedule");
        CHECK_EQ(ups.url("",1,0),"/U{o_id}.{attr_id}");
        CHECK_EQ(ups.url("",1,1),"/U{o_id}.production.schedule");
        CHECK_EQ(ups.url("",1,2),"/U1.production.schedule");
        
        CHECK_EQ(ups.url("",2,-1),"/H1/U1.production.schedule");
        CHECK_EQ(ups.url("",2,0),"/H{parent_id}/U{o_id}.{attr_id}");
        CHECK_EQ(ups.url("",2,1),"/H{parent_id}/U{o_id}.production.schedule");
        CHECK_EQ(ups.url("",2,2),"/H{parent_id}/U1.production.schedule");
        CHECK_EQ(ups.url("",2,3),"/H1/U1.production.schedule");
        string a_name="unavailability";
        string a_id="{attr_id}";
        a_wrap<apoint_ts> uun(
            [o=u,a_name,a_id](sbi_t& so,int l, int tl,string_view /*sv*/)->void {
                if(l)
                    o->generate_url(so,l-1,tl>0?tl-1:tl); 
            },
            a_name,
            u->unavailability
        );
        CHECK_EQ(uun.url("",0,-1),".unavailability");
        CHECK_EQ(uun.url("",0,0),".{attr_id}");
        CHECK_EQ(uun.url("",0,1),".unavailability");

        CHECK_EQ(uun.url("",1,-1),"/U1.unavailability");
        CHECK_EQ(uun.url("",1,0),"/U{o_id}.{attr_id}");
        CHECK_EQ(uun.url("",1,1),"/U{o_id}.unavailability");
        CHECK_EQ(uun.url("",1,2),"/U1.unavailability");
        u->generator_description=make_shared<t_xy_::element_type>();
        auto gen_desc = make_shared<xy_point_curve_::element_type>();
        using shyft::energy_market::hydro_power::point;
        using shyft::core::from_seconds;
        (*gen_desc).points.push_back(point(1000,0));
        (*gen_desc).points.push_back(point(1010,10));
        (*u->generator_description)[from_seconds(0)]= gen_desc;
        auto blob=stm_hps::to_blob(shp);
        auto o=stm_hps::from_blob(blob);
        auto u2=std::dynamic_pointer_cast<unit>(o->units[0]);
        CHECK_EQ(u2->id,u->id);
        CHECK_EQ(true,u2->generator_description.get()!=nullptr);
        auto const& m1=*u2->generator_description;
        auto const& m2=*u->generator_description;
        CHECK_EQ(m1.size(),m2.size());
        for(auto const&kv:m2) {
            auto f=m1.find(kv.first);
            if(f==m1.end()) {
                MESSAGE("Failed to find key");
                CHECK(f==m1.end());
                continue;
            }
            if( *(*f).second != (*kv.second) ) {
                MESSAGE("Different values..");
                CHECK(false);//
            }
        }
    }
	
    TEST_CASE("stm_power_plant_equality") {
        // Test that stm::plant::operator== checks stm attributes
        auto hps_1 = make_shared<stm_hps>(1, "test_hps_1");
        auto hps_2 = make_shared<stm_hps>(2, "test_hps_2");

        auto plant_1 = builder(hps_1).create_power_plant(11, "test", "");
        auto plant_2 = builder(hps_2).create_power_plant(11, "test", "");

        auto unit_1 = builder(hps_1).create_unit(14, "test", "");
        auto unit_2 = builder(hps_2).create_unit(14, "test", "");

        // All attributes left at default, objects should be equal
        CHECK_EQ(*plant_1, *plant_2);

        // Add unit to one power_plant, no longer equal
        power_plant::add_unit(plant_1, unit_1);
        CHECK_NE(*plant_1, *plant_2);

        // Add unit to the other power_plant, equal again
        power_plant::add_unit(plant_2, unit_2);
        CHECK_EQ(*plant_1, *plant_2);

        // Add some attributes to first object, no longer equal
        plant_1->production.constraint_max = test::create_t_double(1, 0.01);
        CHECK_NE(*plant_1, *plant_2);

        // Add attributes to other object, now equal again
        plant_2->production.constraint_max = test::create_t_double(1, 0.01);
        CHECK_EQ(*plant_1, *plant_2);

        // Add unit attribute to first object, no longer equal
        unit_1->production.static_max = test::create_t_double(1, 10.0);
        CHECK_NE(*plant_1, *plant_2);

        // Add unit attribute to other object, now equal again
        unit_2->production.static_max = test::create_t_double(1, 10.0);
        CHECK_EQ(*plant_1, *plant_2);
    }
	
    TEST_CASE("stm_power_plant_basics") {
        const auto name{ "pp_one"s };
        auto shp=make_shared<stm_hps>(1,"sys");
        stm_hps_builder b(shp);
        string json_string("some json");
        const auto pp = b.create_power_plant(1, name,json_string);
        CHECK_EQ(pp->id, 1);
        CHECK_EQ(pp->name, name);
        CHECK_EQ(pp->json,json_string);
        CHECK_THROWS_AS(b.create_power_plant(1,name+"a",""),stm_rule_exception);
        CHECK_THROWS_AS(b.create_power_plant(2,name,""),stm_rule_exception);
        string s;
        sbi_t sbi(s);
        pp->production.url_fx(sbi,-1,-1,".schedule");
        CHECK_EQ(s,"/H1/P1.production.schedule");
        
        // verify a_wrap at secondary-nested levels
        a_wrap<apoint_ts> pps(
            [o=&pp->production](sbi_t& so,int l, int tl,string_view sv)->void {
               o->url_fx(so,l,tl,sv); 
            },
            "schedule",
            pp->production.schedule
        );

        CHECK_EQ(pps.url("",0,-1),".production.schedule");
        CHECK_EQ(pps.url("",0,0),".{attr_id}");
        CHECK_EQ(pps.url("",0,1),".production.schedule");

        CHECK_EQ(pps.url("",1,-1),"/P1.production.schedule");
        CHECK_EQ(pps.url("",1,0),"/P{o_id}.{attr_id}");
        CHECK_EQ(pps.url("",1,1),"/P{o_id}.production.schedule");
        CHECK_EQ(pps.url("",1,2),"/P1.production.schedule");
        
        CHECK_EQ(pps.url("",2,-1),"/H1/P1.production.schedule");
        CHECK_EQ(pps.url("",2,0),"/H{parent_id}/P{o_id}.{attr_id}");
        CHECK_EQ(pps.url("",2,1),"/H{parent_id}/P{o_id}.production.schedule");
        CHECK_EQ(pps.url("",2,2),"/H{parent_id}/P1.production.schedule");
        CHECK_EQ(pps.url("",2,3),"/H1/P1.production.schedule");
        
		pp->discharge.constraint_max = test::create_t_double(1, 0.01);
        auto blob=stm_hps::to_blob(shp);
        auto o=stm_hps::from_blob(blob);
        auto pp2=std::dynamic_pointer_cast<power_plant>(o->power_plants[0]);
        CHECK_EQ(*pp2, *pp);

        // Test for shared_from_this function
        CHECK_EQ(pp, pp->shared_from_this());
        // Test that returns nullptr hps is missing
        auto pp3 = make_shared<power_plant>(3, "pp3", "", nullptr);
        CHECK_EQ(nullptr, pp3->shared_from_this());
        // Test that returns nullptr if object is missing in the hps
        auto pp4 = make_shared<power_plant>(4, "pp4", "", shp);
        CHECK_EQ(nullptr, pp4->shared_from_this());

        const auto u = b.create_unit(2, "u",json_string);

        // Tests for add_unit function
        //Test successful add
        power_plant::add_unit(pp, u);
        CHECK(pp->units.size() == 1);
        CHECK(pp->units[0] == u);
        CHECK(pp == u->pwr_station_());

        //Test add fails if unit already added
        CHECK_THROWS_AS(power_plant::add_unit(pp, u), runtime_error);

        //Test add fails if power plant misses hps
        auto u2 = make_shared<unit>(2, "u2", "", shp);
        CHECK_THROWS_AS(power_plant::add_unit(pp3, u2), runtime_error);

        // Test that adding fails if the power plant is missing from the hps
        const auto u3 = b.create_unit(3, "t3",json_string);
        CHECK_THROWS_AS(power_plant::add_unit(pp4, u3), runtime_error);

        //Test that adding fails if unit already added to another power plant
        const auto pp5 = b.create_power_plant(5, "pp5",json_string);
        CHECK_THROWS_AS(power_plant::add_unit(pp5, u), runtime_error);

        //Test remove_unit function
        pp->remove_unit(u);
        CHECK(pp->units.size() == 0);
        CHECK(nullptr == u->pwr_station_());

        //Test that removing from empty unit aggregate does nothing
        pp->remove_unit(u);
        CHECK(pp->units.size() == 0);

        //Test that adding a unit back works
        power_plant::add_unit(pp, u);
        CHECK(pp->units.size() == 1);
        CHECK(pp->units[0] == u);
        CHECK(pp == u->pwr_station_());
    }
}
