#include <doctest/doctest.h>
#include <shyft/web_api/energy_market/grammar/ts_url.h>
#include <test/web_api/test_parser.h>
#include "build_test_system.h"
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/dstm/ts_url_resolver.h>

//#include <boost/spirit/include/karma.hpp>

using shyft::web_api::grammar::dstm_path_grammar;
using shyft::web_api::grammar::tp_id;
using shyft::web_api::grammar::tp_id_grammar;

using std::shared_ptr;
using std::make_shared;
// ts stuff
using gta_t=shyft::time_axis::generic_dt;
using shyft::time_series::dd::apoint_ts;
using shyft::core::utctime;
using shyft::core::deltahours;
using shyft::core::calendar;
using namespace shyft::energy_market;

namespace {
    
    struct dstm_fixture {
        
        shared_ptr<stm::srv::dstm::server> dstm;
        stm::stm_system_ sys;
        
        dstm_fixture() {
            dstm=make_shared<stm::srv::dstm::server>();
            sys=test::create_simple_system(1,"sys");
            dstm->do_add_model("sys",sys);
        }
        
    };
    
    void verify_dstm_resolve(dstm_path_grammar<const char*> const& g_,const char*url,const char *expected_ts_id) {
        apoint_ts r;
        FAST_REQUIRE_EQ(true,test::phrase_parser(url, g_, r));
        FAST_CHECK_EQ(expected_ts_id,r.id());
    }
    void verify_dstm_resolve_sz(dstm_path_grammar<const char*> const& g_,const char*url,size_t expected_ts_size) {
        apoint_ts r;
        FAST_REQUIRE_EQ(true,test::phrase_parser(url, g_, r));
        FAST_CHECK_EQ(expected_ts_size,r.size());
    }

    void verify_dstm_resolve_throws(dstm_path_grammar<const char*> const& g_,const char*url) {
        apoint_ts r;
        auto ok=test::phrase_parser(url, g_, r);
        if(!ok)
            std::runtime_error("Ok, it failed on the syntax");
    }

    using boost::spirit::karma::int_;
    using boost::spirit::karma::char_;
    
    
    struct path_echo_resolver {// resolving back to its original url
        path_echo_resolver(size_t *pcount):count(pcount){} 
        path_echo_resolver()=default;
        size_t *count{nullptr};
        apoint_ts operator() ( const std::string& mid, std::vector<tp_id> const& c, const std::string& attr_id) {
            std::string ts_url("dstm://M"+mid);
            for(auto e:c)
                boost::spirit::karma::generate(
                    std::back_inserter(ts_url), // the output
                '/'<< char_<<int_, // the generator
                e.tp,e.id // the input
            );
                
            if(count) ++(*count);
            return apoint_ts(ts_url+"."+attr_id);
        }
    };
    
    void verify_resolve_path(const char *ts_url) {
        dstm_path_grammar<const char*> g_(path_echo_resolver{});
        apoint_ts r;
        FAST_CHECK_EQ(true,test::phrase_parser(ts_url, g_, r));
        FAST_CHECK_EQ(ts_url,r.id());
    }
    
    
    void bad_syntax_path(const char *ts_url) {
        size_t count=0;
        dstm_path_grammar<const char*> g_(path_echo_resolver{&count});
        apoint_ts r;
        if(!phrase_parser(ts_url, g_, r)) // its ok to return false if no initial match
            throw std::runtime_error("failed to parse");
        MESSAGE("incredible, it succeded resolve count is "<<count);
            
    }

}


TEST_SUITE("dstm_ts_url") {
    TEST_CASE("tp_id_parser"){
        SUBCASE("basic"){
            tp_id_grammar<const char*> tp_id_;
            tp_id r;
            FAST_CHECK_UNARY(phrase_parser("T123",tp_id_,r));
            FAST_CHECK_EQ(r,tp_id{'T',123});
            CHECK_THROWS(phrase_parser("T 123",tp_id_,r));
            CHECK(!phrase_parser("_123",tp_id_,r));
            CHECK(!phrase_parser(".123",tp_id_,r));
        }
    }
    
    TEST_CASE("parser") {
        SUBCASE("basic"){
            verify_resolve_path("dstm://Msys/H1/G1.discharge.result");
        }
        SUBCASE("permutations") {
            int g_id=100,c_id=203;
            std::map<char,string> subs;
            subs['H']=string{"RUWPCGA"};
            subs['u']=string{"M"};
            //subs['m']=string{"A"};
            for(auto g:string{"Humcp"}) { // these are the current main types
                for(auto c:subs[g]) { // these are the current component types
                    std::string ts_url("dstm://Mabc");
                    boost::spirit::karma::generate(
                        std::back_inserter(ts_url), // the output
                        '/'<<char_<<int_ << '/'<<char_<<int_<<'.'<<+"some.attribute", // the generator
                        g,g_id,c,c_id  // the input
                    );

                    verify_resolve_path(ts_url.c_str());
                }
            }
        }
        SUBCASE("syntax"){
            CHECK_THROWS(bad_syntax_path("suppe//Msys/H1/G1.discharge.result"));
            CHECK_THROWS(bad_syntax_path("dstm://Zsys/H1/G1.discharge.result"));
            CHECK_THROWS(bad_syntax_path("dstm://Msys/F1/G1x.discharge.result"));
            CHECK_THROWS(bad_syntax_path("dstm://Msys/Hx1/G1.discharge.result"));
            CHECK_THROWS(bad_syntax_path("dstm://Msys/H1/xx1.discharge.result"));
            CHECK_THROWS(bad_syntax_path("dstm://Msys/H1/Ra1.discharge.result"));
            CHECK_THROWS(bad_syntax_path("dstm://Msys/H1/R1."));
        }
    }
    
    TEST_CASE_FIXTURE(dstm_fixture,"hps") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("reservoir") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/H1/R1.level.schedule",0u);
            verify_dstm_resolve(g_,"dstm://Msys/H1/R1.volume.result","shyft://test/r.volume.result");
        }
        SUBCASE("gate") {
            verify_dstm_resolve(g_,"dstm://Msys/H1/G1.discharge.result","shyft://test/g.discharge.result");
        }
        SUBCASE("unit") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/H1/U1.discharge.result",0);
            verify_dstm_resolve_sz(g_,"dstm://Msys/H1/U1.discharge.constraint.min",6);
        }
        SUBCASE("waterway"){
            verify_dstm_resolve(g_,"dstm://Msys/H1/W1.discharge.static_max","shyft://test/w.discharge.static_max");
            verify_dstm_resolve_sz(g_,"dstm://Msys/H1/W1.discharge.result",6);
        }
        SUBCASE("powerplant"){
            verify_dstm_resolve(g_,"dstm://Msys/H1/P2.production.schedule","shyft://test/pp.production.schedule");
        }
        SUBCASE("reservoir_aggregate"){
            verify_dstm_resolve(g_,"dstm://Msys/H1/A2.volume.schedule","shyft://test/ra.volume.schedule");
        }
        SUBCASE("catchment"){
            verify_dstm_resolve(g_,"dstm://Msys/H1/C1.inflow_m3s","shyft://test/ca.inflow_m3s");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Mxsys/H1/R1.level.schedule"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/H2/R1.level.schedule"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/H1/R2.level.schedule"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/H1/R1.xlevel.schedule"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/H1.schedule"));
        }
    }
    TEST_CASE_FIXTURE(dstm_fixture,"unit_group") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/u3.obligation.cost",0u);
            verify_dstm_resolve(g_,"dstm://Msys/u3.obligation.schedule","shyft://test/ug.obligation.schedule");
        }
        SUBCASE("member_level") {
            verify_dstm_resolve(g_,"dstm://Msys/u3/M1.active","shyft://test/ug/m.active");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/u3.obligation.xcost"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/u3/M1/X1.active"));
        }
    }

    TEST_CASE_FIXTURE(dstm_fixture,"market_area") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/m2.load",0u);
            verify_dstm_resolve(g_,"dstm://Msys/m2.price","shyft://test/market.price");
            verify_dstm_resolve(g_,"dstm://Msys/m2.ts.price-alternative","shyft://test/market.price-alternative");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/m1.load"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/m1/X1.active"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/m2.price-alternative"));
        }
    }

    TEST_CASE_FIXTURE(dstm_fixture,"contract") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/c4.revenue",0u);
            verify_dstm_resolve(g_,"dstm://Msys/c4.quantity","shyft://test/contract.volume");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/c1.quantity"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/c1/X1.active"));
        }
    }

    TEST_CASE_FIXTURE(dstm_fixture,"contract_portfolio") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/p5.revenue",0u);
            verify_dstm_resolve(g_,"dstm://Msys/p5.quantity","shyft://test/contract_portfolio.volume");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/p1.volume"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/p1/X1.active"));
        }
    }

    TEST_CASE_FIXTURE(dstm_fixture,"busbar") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve(g_,"dstm://Msys/n6/b8.dummy","shyft://test/busbar.dummy");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/n6/b8.test"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/n6/B7.dummy"));
        }
    }

    TEST_CASE_FIXTURE(dstm_fixture,"transmission_line") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve(g_,"dstm://Msys/n6/t7.capacity","shyft://test/transmission_line.capacity");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/n6/t7.test"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/n6/x7.capacity"));
        }
    }

    TEST_CASE_FIXTURE(dstm_fixture,"power_module") {
        dstm_path_grammar<const char*> g_(stm::srv::dstm::ts_url_resolver(dstm.get()));
        SUBCASE("group_level") {
            verify_dstm_resolve_sz(g_,"dstm://Msys/P9.power.result",0u);
            verify_dstm_resolve(g_,"dstm://Msys/P9.power.schedule","shyft://test/power_module.power.schedule");
        }
        SUBCASE("bad_lookups") {
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/P9.power.test"));
            CHECK_THROWS(verify_dstm_resolve_throws(g_,"dstm://Msys/p9/X1.something"));
        }
    }
}
