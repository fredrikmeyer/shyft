#include <doctest/doctest.h>
#include "build_test_system.h"
#include <shyft/energy_market/stm/srv/dstm/dstm_subscription.h>
#include <shyft/energy_market/a_wrap.h>

using shyft::energy_market::proxy_attr;

TEST_SUITE("dstm") {
    using shyft::core::subscription::manager;
    using shyft::energy_market::stm::subscription::proxy_attr_observer;
    using std::make_shared;
    using std::string;
    
    auto mdl = test::create_simple_system();
    auto url_hps = mdl->hps[0];
    auto rsv = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(url_hps->reservoirs[0]);
    auto u = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(url_hps->units[0]);
    auto pp = std::dynamic_pointer_cast<shyft::energy_market::stm::power_plant>(url_hps->power_plants[0]);
    TEST_CASE("generate_proxy_id") {
        // Check generating proxy id:
        string prefix = "M1";

        auto  inflow_schedule=proxy_attr(rsv->inflow,"schedule",rsv->inflow.schedule);
        CHECK_EQ(inflow_schedule.url(prefix,-1,-1), "M1/H1/R1.inflow.schedule");

        auto  u_p_s=proxy_attr(u->production,"schedule",u->production.schedule);
        CHECK_EQ(u_p_s.url(prefix,-1-1), "M1/H1/U1.production.schedule");

        auto  u_p_c_min=proxy_attr(u->production.constraint,"min",u->production.constraint.min);
        CHECK_EQ(u_p_c_min.url(prefix,-1-1), "M1/H1/U1.production.constraint.min");

        auto p_d_s=proxy_attr(pp->discharge,"schedule",pp->discharge.schedule);
        CHECK_EQ(p_d_s.url("AnyString",-1,-1), "AnyString/H1/P2.discharge.schedule");

        // Generating proxy ID for stm_system:
        auto rp_n_ir=proxy_attr(mdl->run_params,"n_inc_runs",mdl->run_params.n_inc_runs);
        CHECK_EQ(rp_n_ir.url(prefix,-1,-1), "M1.run_params.n_inc_runs");

        auto rp_n_fr=proxy_attr(mdl->run_params,"n_full_runs",mdl->run_params.n_full_runs);
        CHECK_EQ(rp_n_fr.url(prefix,-1,-1), "M1.run_params.n_full_runs");
        
        auto rp_ho=proxy_attr(mdl->run_params,"head_opt",mdl->run_params.head_opt);
        CHECK_EQ(rp_ho.url(prefix,-1,-1), "M1.run_params.head_opt");
        // test opt summary
        auto s_total=proxy_attr(*mdl->summary,"total",mdl->summary->total);
        CHECK_EQ(s_total.url("dstm://M1",-1,-1),"dstm://M1.summary.total");
        auto all_urls=mdl->summary->all_urls("dstm://M1");
        CHECK_EQ(all_urls.size(),36);
        string expected_prefix{"dstm://M1.summary."};
        auto _ok_start=[expected_prefix](string x)->bool {
            if(x.size()< expected_prefix.size()+1)
                return false;
            for(size_t i=0;i<expected_prefix.size();++i)
                if(expected_prefix[i]!= x[i])
                    return false;
            return true;
        };
        for(auto u:all_urls) {
            if(!_ok_start(u))
                MESSAGE("Failed for url: "<<u);
            CHECK_EQ(true,_ok_start(u));
        }
            
    }

    TEST_CASE("proxy_url_partial") {
        
        auto  inflow_schedule=proxy_attr(rsv->inflow,"schedule",rsv->inflow.schedule);
        CHECK_EQ(inflow_schedule.url("",0,-1), ".inflow.schedule");
        CHECK_EQ(inflow_schedule.url("",1,-1), "/R1.inflow.schedule");
        CHECK_EQ(inflow_schedule.url("",2,-1), "/H1/R1.inflow.schedule");

        auto p_c_min=proxy_attr(u->production.constraint,"min",u->production.constraint.min);
        CHECK_EQ(p_c_min.url("",0,-1), ".production.constraint.min");
        CHECK_EQ(p_c_min.url("",1,-1), "/U1.production.constraint.min");
        CHECK_EQ(p_c_min.url("",2,-1), "/H1/U1.production.constraint.min");

        auto p_d_s=proxy_attr(pp->discharge,"schedule",pp->discharge.schedule);
        CHECK_EQ(p_d_s.url("",0,-1), ".discharge.schedule");
        CHECK_EQ(p_d_s.url("",1,-1), "/P2.discharge.schedule");
        CHECK_EQ(p_d_s.url("",2,-1), "/H1/P2.discharge.schedule");
    }

    TEST_CASE("proxy_url_placeholders") {
        auto inflow_schedule=proxy_attr(rsv->inflow,"schedule",rsv->inflow.schedule);
        CHECK_EQ(inflow_schedule.url("",0,0), ".{attr_id}");
        CHECK_EQ(inflow_schedule.url("",1,0), "/R{o_id}.{attr_id}");
        CHECK_EQ(inflow_schedule.url("",1,1), "/R{o_id}.inflow.schedule");
        CHECK_EQ(inflow_schedule.url("",1,2), "/R1.inflow.schedule");
        CHECK_EQ(inflow_schedule.url("",2,0), "/H{parent_id}/R{o_id}.{attr_id}");
        CHECK_EQ(inflow_schedule.url("",2,1), "/H{parent_id}/R{o_id}.inflow.schedule");
        CHECK_EQ(inflow_schedule.url("",2,2), "/H{parent_id}/R1.inflow.schedule");
        CHECK_EQ(inflow_schedule.url("",2,3), "/H1/R1.inflow.schedule");
    }
}
