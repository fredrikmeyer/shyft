#include <doctest/doctest.h>
#include <shyft/energy_market/stm/srv/dstm/compute_node.h>
#include "serialize_loop.h"

using namespace shyft::core;
using namespace shyft::energy_market::stm::srv::dstm;
TEST_SUITE("compute_node") {
    TEST_CASE("basics") {
        compute_node a{"123"};
        CHECK(a.host_port=="123");
        CHECK(a.removed==false);
        a.mid="abc";a.allocated_time=seconds(1);a.last_contact=seconds(2);a.fail_count=123;a.released_time=seconds(4);
        a.removed=true;
        auto b=test::serialize_loop(a);
        CHECK(a==b);
        a.fail_count++;
        CHECK(a!=b);
    }
    TEST_CASE("manager_basics") {
        // 0 case, should have null
        compute_node_manager m;
        CHECK(m.compute_nodes().size()==0);

        // add some compute nodes.
        vector<string> cn_host_port{"a:1234","b:1234","c:3333"};
        m.add_compute_nodes(cn_host_port);
        auto contains=[](auto const& c,auto const& v) {
            return std::end(c) != std::find(std::begin(c),std::end(c),v);
        };

        auto cn=m.compute_nodes();
        REQUIRE(cn.size()==cn_host_port.size());
        for(size_t i=0;i<cn.size();++i) {
            CHECK(contains(cn_host_port,cn[i].host_port));
        }

        // allocate one in a scope:
        {
            scoped_compute_node sc{m,"m1"};
            CHECK(sc.cn->mid=="m1");
            CHECK(utctime_now()-sc.cn->allocated_time < std::chrono::milliseconds(200));
            CHECK(sc.cn->released_time==no_utctime);
            CHECK(sc.cn->last_contact==no_utctime);
            CHECK(sc.cn->removed==false);
            auto t0=utctime_now();
            sc.register_contact(t0);
            sc.register_failure();
            CHECK(sc.cn->last_contact==t0);
            CHECK(sc.cn->fail_count==1);
            CHECK(sc.cn->host_port==cn_host_port[0]);//because they are inserted ordered into the queue.
        }
        // now the scoped compute node is put back it the queue again, and end time should be recorded.
        cn=m.compute_nodes();// get a snapshot
        for(auto const& x:cn) {
            if(x.host_port==cn_host_port[0]) {// got the one we allocated.
                CHECK(x.last_contact != no_utctime );
                CHECK(x.released_time != no_utctime);
                CHECK(x.released_time>= x.allocated_time);
            }
        }
        // remove a node
        vector<string> cn_rm{cn_host_port.back()};
        m.remove_compute_nodes(cn_rm);
        cn=m.compute_nodes();
        CHECK(cn.size()==2);
        for(auto const&x:cn) {
            CHECK(x.host_port != cn_host_port.back());
        }

    }
}
