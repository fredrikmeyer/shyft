#include <doctest/doctest.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>

using namespace shyft::energy_market::stm;
using std::make_shared;

TEST_SUITE("stm_transmission_line") {

    TEST_CASE("transmission_line_construct") {
        auto sys=make_shared<stm_system>(1,"sys","{}");
        auto net=make_shared<network>(2,"net","{}", sys);
        auto t=make_shared<transmission_line>(3, "t", "{}", net);
        FAST_CHECK_UNARY(t);
        FAST_CHECK_EQ(t->id,3);
        FAST_CHECK_EQ(t->name,"t");
        FAST_CHECK_EQ(t->json,"{}");
        FAST_CHECK_EQ(*t, *t);
    }
}
