#include <doctest/doctest.h>
#include "build_test_system.h"
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/graph/graph_utilities.h>

#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/run_parameters.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/reservoir_aggregate.h>
#include <shyft/energy_market/stm/contract.h>
#include <shyft/energy_market/stm/contract_portfolio.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/catchment.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/unit_group.h>

#include <shyft/energy_market/a_wrap.h>

#include <test/energy_market/stm/utilities.h>
#include "../serialize_loop.h"

using namespace shyft::energy_market::hydro_power;
using namespace shyft::energy_market;
using namespace shyft::core;
using test::serialize_loop;
#ifdef WIN32
constexpr bool win_skip=true;// for some not yet figured out reason the comparison test fail on win comparire.
#else
constexpr bool win_skip=false;
#endif
TEST_SUITE("stm_hps") {
TEST_CASE("stm_hydro_power_system serialization") {
    using namespace shyft::energy_market::stm;
    auto a = test::create_stm_hps();
    // just verify that we got a complete hooked up model:
    auto t=a->find_waterway_by_name("lauvastølsvatn-til-kvilldal");
    CHECK(t != nullptr);
    CHECK(t->gates.size() == 1u);// should be one here..
    CHECK(t->gates[0]->wtr_() == t);
    auto t_stm = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(t);
    CHECK(t_stm != nullptr);
    auto t_hps = t->hps_();
    CHECK(t_hps != nullptr);
    auto s_stm = std::dynamic_pointer_cast<shyft::energy_market::stm::stm_hps>(t_hps);
    CHECK(s_stm != nullptr);
    //
    auto b = test::create_stm_hps();
    CHECK(a != nullptr);
    CHECK(b != nullptr);
    CHECK(a->equal_structure(*b) == true);
    SUBCASE("serialization") {
        auto b_as_blob = stm_hps::to_blob(b);
        CHECK(b_as_blob.size() > 0);
        auto b_from_blob = stm_hps::from_blob(b_as_blob);
        CHECK(b_from_blob->equal_structure(*b));
    }
    a->clear();
    b->clear();

}

TEST_CASE("stm_hps_equality"*doctest::skip(win_skip)) {
    //


    using namespace shyft::energy_market::stm;
    using builder = shyft::energy_market::stm::stm_hps_builder;

    auto hps_1 = std::make_shared<stm_hps>(1, "test_hps");
    auto hps_2 = std::make_shared<stm_hps>(1, "test_hps");
    CHECK_EQ(*hps_1, *hps_2);  // Both empty, hence equal

    SUBCASE("waterway content") {
        auto wtr_1 = builder(hps_1).create_waterway(2, "test_wtr", "");
        wtr_1->head_loss_coeff = test::create_t_double(100, 0.001);
        CHECK_NE(*hps_1, *hps_2);  // hps_1 is non-empty

        auto wtr_2 = builder(hps_2).create_waterway(2, "test_wtr", "");
        CHECK_NE(*hps_1, *hps_2);  // since wtr_1 != wtr_2

        wtr_2->head_loss_coeff = test::create_t_double(100, 0.001);
        CHECK_EQ(*hps_1, *hps_2);  // since now wtr_1 == wtr_2
    }

    SUBCASE("reservoir content") {
        auto rsv_1 = builder(hps_1).create_reservoir(3, "test_rsv", "");
        rsv_1->level.regulation_min = test::create_t_double(100, 100.0);
        CHECK_NE(*hps_1, *hps_2);  // hps_1 is non-empty

        auto rsv_2 = builder(hps_2).create_reservoir(3, "test_rsv", "");
        CHECK_NE(*hps_1, *hps_2);  // since rsv_1 != rsv_2

        rsv_2->level.regulation_min = test::create_t_double(100, 100.0);
        CHECK_EQ(*hps_1, *hps_2);  // since now rsv_1 == rsv_2
    }

    SUBCASE("unit_content") {
        auto unit_1 = builder(hps_1).create_unit(4, "test_unit", "");
        unit_1->production.static_max = test::create_t_double(100, 10.);
        CHECK_NE(*hps_1, *hps_2);  // hps_1 is non-empty

        auto unit_2 = builder(hps_2).create_unit(4, "test_unit", "");
        CHECK_NE(*hps_1, *hps_2);  // since unit_1 != unit_2

        unit_2->production.static_max = test::create_t_double(100, 10.);
        CHECK_EQ(*hps_1, *hps_2);  // since now unit_1 == unit_2
    }

    SUBCASE("plant_content") {
        auto plant_1 = builder(hps_1).create_power_plant(5, "test_plant", "");
        plant_1->production.constraint_max = test::create_t_double(100, 10.);
        CHECK_NE(*hps_1, *hps_2);  // hps_1 is non-empty

        auto plant_2 = builder(hps_2).create_power_plant(5, "test_plant", "");
        CHECK_NE(*hps_1, *hps_2);  // since plant_1 != plant_2

        plant_2->production.constraint_max = test::create_t_double(100, 10.);
        CHECK_EQ(*hps_1, *hps_2);  // since now plant_1 == plant_2
    }
}

TEST_CASE("stm_hydro_power_system_topology") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_stm_hps();
    {
        auto blasjo1 = a->find_reservoir_by_name("blåsjø1");
        CHECK(blasjo1 != nullptr);
        auto blasjo_ds = graph::downstream_units(blasjo1);
        auto blasjo_us = graph::upstream_units(blasjo1);
        CHECK(blasjo_ds.size() == 1);
        CHECK(blasjo_us.size() == 0);
        auto vassbotvatn = a->find_reservoir_by_name("vassbotvatn");
        CHECK(vassbotvatn != nullptr);
        auto vassbotvatn_us = graph::upstream_units(vassbotvatn);
        auto vassbotvatn_ds = graph::downstream_units(vassbotvatn);
        CHECK(vassbotvatn_us.size() == 1);
        CHECK(vassbotvatn_us[0]->name == "stølsdal pumpe");
        CHECK(vassbotvatn_ds.size() == 0);
        SUBCASE("remove connection to water-route") {
            hydro_component::disconnect(vassbotvatn, vassbotvatn->upstreams[0].target_());
            CHECK(graph::upstream_units(vassbotvatn).size() == 0);
        }

        // Test reservoir aggregate auxiliary functions
        CHECK(a->find_reservoir_aggregate_by_name("blåsjø")->id == 22);
        CHECK(a->find_reservoir_aggregate_by_id(22)->name == "blåsjø");

        SUBCASE("power_stations up and down streams") {
            auto saurdal = a->find_unit_by_name ("saurdal");
            CHECK(saurdal != nullptr);
            auto rsv_us = graph::upstream_reservoirs(saurdal);
            CHECK(rsv_us.size() == 1);
            CHECK(rsv_us[0]->name == "blåsjø1");

            // NOTE, junction is not yet supported auto rsv_ds = saurdal->downstream_reservoir();
            auto kvilldal = a->find_unit_by_name ("kvilldal_1");
            auto rsv_ds = graph::downstream_reservoirs(kvilldal);
            CHECK(rsv_ds.size() == 1);
            CHECK(rsv_ds[0]->name == "suldalsvatn");
        }
    }
    a->clear();
    a = nullptr;
}

TEST_CASE("stm_system serialization") {
    using namespace shyft::energy_market::stm;
    using stm_unit=shyft::energy_market::stm::unit;
    auto a = make_shared<stm_system>(1, "ull", "aførre");
    auto no1=make_shared<energy_market_area>(1, "NO1", "xx", a);
    a->market.push_back(no1);
    a->market.push_back(make_shared<energy_market_area>(2, "NO2", "xx",a));
    a->market.push_back(make_shared<energy_market_area>(3, "NO3", "xx", a));
    a->market.push_back(make_shared<energy_market_area>(4, "NO4", "xx", a));
    a->hps.push_back(test::create_stm_hps(2,"sørland"));
    a->hps.push_back(test::create_stm_hps(1, "oslo"));
    auto ug=a->add_unit_group(1,"ug1","{}");// make a unit group    
    ug->add_unit(std::dynamic_pointer_cast<stm_unit>(a->hps[0]->units[0]),apoint_ts());// with one unit
    CHECK_THROWS_AS(a->add_unit_group(1,"ug2","{}"),std::runtime_error); // verify we can keep it somewhat unique
    CHECK_THROWS_AS(a->add_unit_group(2,"ug1","{}"),std::runtime_error);
    // add contract, contract-portfolio, plus some relations
    auto c1=make_shared<contract>(5,"C1","xx",a);
    auto pc1=make_shared<contract_portfolio>(6,"P1","yy",a);
    c1->power_plants.push_back(std::dynamic_pointer_cast<stm::power_plant>(a->hps[0]->power_plants[0]));
    a->contracts.push_back(c1);
    a->contract_portfolios.push_back(pc1);
    c1->add_to_portfolio(pc1);
    c1->add_to_energy_market_area(no1);
    // add network, transmission_line, busbar, power_module
    auto n=make_shared<network>(7,"N1","xx", a);
    a->networks.push_back(n);
    auto t=make_shared<transmission_line>(8,"T1","xx", n);
    auto b1=make_shared<busbar>(9,"B1","xx", n);
    n->transmission_lines.push_back(t);
    n->busbars.push_back(b1);
    b1->add_to_start_of_transmission_line(t);
    auto p1=make_shared<power_module>(10, "P1", "xx", a);
    a->power_modules.push_back(p1);
    b1->add_to_power_module(p1);

    auto a_blob = stm_system::to_blob(a);
    auto b = stm_system::from_blob(a_blob);
    CHECK(b != nullptr);
    CHECK_EQ(b->name , a->name);
    CHECK_EQ(b->id, a->id);
    CHECK_EQ(b->json, a->json);
    CHECK_EQ(b->market.size(), a->market.size());
    CHECK_EQ(b->hps.size(), a->hps.size());
    REQUIRE_EQ(b->unit_groups.size(),1);
    CHECK_EQ(b->unit_groups[0]->members.size(),1);
    // verify we got it right with the contracts.
    CHECK_EQ(b->contracts.size(), a->contracts.size());
    CHECK_EQ(b->contracts[0]->get_energy_market_areas().size(),1);
    CHECK_EQ(b->contracts[0]->get_portfolios().size(),1);
    CHECK_EQ(b->contract_portfolios.size(), a->contract_portfolios.size());
    // verify networks, transmission_lines, busbars, power_modules
    CHECK_EQ(b->networks.size(), a->networks.size());
    CHECK_EQ(b->networks[0]->transmission_lines.size(), a->networks[0]->transmission_lines.size());
    CHECK_EQ(b->networks[0]->busbars.size(), a->networks[0]->busbars.size());
    CHECK_EQ(b->networks[0]->busbars[0]->get_transmission_lines_from_busbar().size(), a->networks[0]->busbars[0]->get_transmission_lines_from_busbar().size());
    CHECK_EQ(b->networks[0]->busbars[0]->get_transmission_lines_to_busbar().size(), a->networks[0]->busbars[0]->get_transmission_lines_to_busbar().size());
    CHECK_EQ(b->networks[0]->busbars[0]->get_power_modules().size(), a->networks[0]->busbars[0]->get_power_modules().size());
    CHECK_EQ(b->power_modules.size(), a->power_modules.size()); 
}

TEST_CASE("stm_system proxy_attributes") {
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::attr_traits;

    auto a = make_shared<stm_system>(1, "ulla", "førre");
    // 1. Check non-existence
    //TODO: CHECK_EQ(false, a->run_params.n_inc_runs.exists());
    //TODO: CHECK_EQ(false, a->run_params.n_full_runs.exists());
    //TODO: CHECK_EQ(false, a->run_params.head_opt.exists());
    CHECK_EQ(false, exists(a->run_params.run_time_axis));
    CHECK_EQ(false, exists(a->run_params.fx_log));

    // 2. Set attributes, and check
    a->run_params.n_inc_runs = 2;
    a->run_params.n_full_runs = 3;
    a->run_params.head_opt = true;
    a->run_params.fx_log = {{from_seconds(0), "a string"}, {from_seconds(1), "another string"}};
    generic_dt ta(vector<utctime>{from_seconds(0), from_seconds(1), from_seconds(2), from_seconds(3), from_seconds(4), from_seconds(5)});
    a->run_params.run_time_axis = ta;
    //TODO:CHECK_EQ(true, a->run_params.n_inc_runs.exists());
    //TODO:CHECK_EQ(true, a->run_params.n_full_runs.exists());
    //TODO:CHECK_EQ(true, a->run_params.head_opt.exists());
    CHECK_EQ(true, exists(a->run_params.run_time_axis));
    CHECK_EQ(true, exists(a->run_params.fx_log));
    CHECK_EQ(2, a->run_params.n_inc_runs);
    CHECK_EQ(3, a->run_params.n_full_runs);
    CHECK_EQ(true, a->run_params.head_opt);
    CHECK_EQ(ta, a->run_params.run_time_axis);
    auto& msgs = a->run_params.fx_log;
    CHECK_EQ(msgs.size(), 2);
    CHECK_EQ(msgs[0].second, "a string");
    CHECK_EQ(msgs[1].second, "another string");

    // 3. Serialize and deserialize, and check that values are persisted
    auto a_blob = stm_system::to_blob(a);
    auto b = stm_system::from_blob(a_blob);
    CHECK(b != nullptr);
    //TODO:CHECK_EQ(true, b->run_params.n_inc_runs.exists());
    //TODO:CHECK_EQ(true, b->run_params.n_full_runs.exists());
    //TODO:CHECK_EQ(true, b->run_params.head_opt.exists());
    CHECK_EQ(true, exists(b->run_params.run_time_axis));
    CHECK_EQ(true, exists(b->run_params.fx_log));

    CHECK_EQ(2, b->run_params.n_inc_runs);
    CHECK_EQ(3, b->run_params.n_full_runs);
    CHECK_EQ(true, b->run_params.head_opt);
    CHECK_EQ(ta, b->run_params.run_time_axis);
    auto& bmsgs = b->run_params.fx_log;
    CHECK_EQ(bmsgs.size(), 2);
    CHECK_EQ(bmsgs[0].second, "a string");
    CHECK_EQ(bmsgs[1].second, "another string");

}



}

TEST_SUITE("stm_system_builder") {
    using namespace shyft::energy_market::stm;

    // --- unique ID rule

    TEST_CASE("create_reservoir enforces unique id") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_reservoir(0, "rsv_0", "");
        CHECK_THROWS_AS(builder.create_reservoir(0, "rsv_1", ""), stm_rule_exception);
    }

    TEST_CASE("create_unit enforces unique id") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_unit(0, "unit_0", "");
        CHECK_THROWS_AS(builder.create_unit(0, "unit_1", ""), stm_rule_exception);
    }

    TEST_CASE("create_power_plant enforces unique id") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_power_plant(0, "wtr_0", "");
        CHECK_THROWS_AS(builder.create_power_plant(0, "wtr_1", ""), stm_rule_exception);
    }

    TEST_CASE("create_waterway enforces unique id") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_waterway(0, "wtr_0", "");
        CHECK_THROWS_AS(builder.create_waterway(0, "wtr_1", ""), stm_rule_exception);
    }

    TEST_CASE("create_gate enforces unique id") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        auto w=builder.create_waterway(8, "wtr_xy", "");
        auto g=builder.create_gate(0, "gate_1", "");
        shyft::energy_market::stm::waterway::add_gate(w,g);
        CHECK_THROWS_AS(builder.create_gate(0, "gate_1", ""), stm_rule_exception);
    }

    // --- unique name rule

    TEST_CASE("create_reservoir enforces unique name") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_reservoir(0, "rsv_0", "");
        CHECK_THROWS_AS(builder.create_reservoir(1, "rsv_0", ""), stm_rule_exception);
    }

    TEST_CASE("create_unit enforces unique name") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_unit(0, "unit_0", "");
        CHECK_THROWS_AS(builder.create_unit(1, "unit_0", ""), stm_rule_exception);
    }

    TEST_CASE("create_power_plant enforces unique name") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_power_plant(1, "wtr_0", "");
        CHECK_THROWS_AS(builder.create_power_plant(1, "wtr_0", ""), stm_rule_exception);
    }

    TEST_CASE("create_waterway enforces unique name") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        builder.create_waterway(0, "wtr_0", "");
        CHECK_THROWS_AS(builder.create_waterway(1, "wtr_0", ""), stm_rule_exception);
    }

    TEST_CASE("create_gate enforces unique name") {
        auto hps = make_shared<stm_hps>(0, "test_system");
        auto builder = stm_hps_builder(hps);
        auto w=builder.create_waterway(9, "wtr_xx", "");
        auto g=builder.create_gate(7, "gate_0", "");
        shyft::energy_market::stm::waterway::add_gate(w,g);
        CHECK_THROWS_AS(builder.create_gate(1, "gate_0", ""), stm_rule_exception);
    }

}
