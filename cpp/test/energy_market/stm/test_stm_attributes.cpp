#include <doctest/doctest.h>

#include <memory>
#include <string>

#include <shyft/energy_market/stm/attribute_types.h>

using namespace shyft::energy_market::stm;

using shyft::core::utctime;
using ta = shyft::time_axis::fixed_dt;
using shyft::time_series::POINT_INSTANT_VALUE;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::hydro_power::xy_point_curve_with_z;
using shyft::energy_market::hydro_power::turbine_description;

TEST_SUITE("test_stm_attributes") {

    TEST_CASE("t_xy_") {
        // Test that checking equality does a deep by-value comparison

        auto a = std::make_shared<t_xy_::element_type>();
        auto b = std::make_shared<t_xy_::element_type>();
        auto t = utctime(11);

        CHECK( equal_attribute(a, b) );  // empty attributes are equal

        a->emplace(std::make_pair(t, std::make_shared<xy_point_curve>()));
        CHECK( !equal_attribute(a, b) );

        b->emplace(std::make_pair(t, std::make_shared<xy_point_curve>()));
        CHECK( equal_attribute(a, b) );

        a->at(t)->points.emplace_back(0.0, 110.0);
        CHECK( !equal_attribute(a, b) );

        b->at(t)->points.emplace_back(0.0, 110.0);
        CHECK( equal_attribute(a, b) );
    }

    TEST_CASE("t_xyz_") {
        // Test that checking equality does a deep by-value comparison

        auto a = std::make_shared<t_xyz_::element_type>();
        auto b = std::make_shared<t_xyz_::element_type>();
        auto t = utctime(11);

        CHECK( equal_attribute(a, b) );  // empty attributes are equal

        a->emplace(std::make_pair(t, std::make_shared<xy_point_curve_with_z>()));
        CHECK( !equal_attribute(a, b) );

        b->emplace(std::make_pair(t, std::make_shared<xy_point_curve_with_z>()));
        CHECK( equal_attribute(a, b) );

        a->at(t)->xy_curve.points.emplace_back(10.0, 13.0);
        a->at(t)->z = 1.0;
        CHECK( !equal_attribute(a, b) );

        b->at(t)->xy_curve.points.emplace_back(10.0, 13.0);
        b->at(t)->z = 1.0;
        CHECK( equal_attribute(a, b) );
    }


    TEST_CASE("t_xyz_list_") {
        // Test that checking equality does a deep by-value comparison

        auto a = std::make_shared<t_xyz_list_::element_type>();
        auto b = std::make_shared<t_xyz_list_::element_type>();
        auto t = utctime(11);

        CHECK( equal_attribute(a, b) );  // empty attributes are equal

        a->emplace(std::make_pair(t, std::make_shared<std::vector<xy_point_curve_with_z>>()));
        CHECK( !equal_attribute(a, b) );

        b->emplace(std::make_pair(t, std::make_shared<std::vector<xy_point_curve_with_z>>()));
        CHECK( equal_attribute(a, b) );

        a->at(t)->emplace_back();
        a->at(t)->back().xy_curve.points.emplace_back(10.0, 13.0);
        a->at(t)->back().z = 1.0;
        CHECK( !equal_attribute(a, b) );

        b->at(t)->emplace_back();
        b->at(t)->back().xy_curve.points.emplace_back(10.0, 13.0);
        b->at(t)->back().z = 1.0;
        CHECK( equal_attribute(a, b) );
    }

    TEST_CASE("t_turbine_description_") {
        // Test that checking equality does a deep by-value comparison

        auto a = std::make_shared<t_turbine_description_::element_type>();
        auto b = std::make_shared<t_turbine_description_::element_type>();
        auto t = utctime(11);

        CHECK( equal_attribute(a, b) );  // empty attributes are equal

        a->emplace(std::make_pair(t, std::make_shared<turbine_description>()));
        CHECK( !equal_attribute(a, b) );

        b->emplace(std::make_pair(t, std::make_shared<turbine_description>()));
        CHECK( equal_attribute(a, b) );

        a->at(t)->efficiencies.emplace_back();
        CHECK( !equal_attribute(a, b) );

        b->at(t)->efficiencies.emplace_back();
        CHECK( equal_attribute(a, b) );
    }


    TEST_CASE("apoint_ts") {
        // Test that we can compare both bound and ubound time series
        auto a = apoint_ts("test");
        auto b = 2 * apoint_ts("test");
        auto c = 3 * apoint_ts("test");
        auto d = 2 * apoint_ts("other");
        auto e = apoint_ts(ta{1, 3600, 5}, 1.1, POINT_INSTANT_VALUE);
        auto f = apoint_ts("test", e);

        SUBCASE("both_onbound") {
            // CHECK( equal_attribute(2*a, b) );
            // CHECK( !equal_attribute(b, c) );
            // CHECK( !equal_attribute(b, d) );
        }

        SUBCASE("both_bound") {
            CHECK( equal_attribute(e, f) );
        }


        SUBCASE("bound_and_unbound") {
            // CHECK( !equal_attribute(a, f) );
        }
    }
}
