#include <doctest/doctest.h>
#include <shyft/energy_market/stm/srv/task/server.h>
#include <shyft/energy_market/stm/srv/task/client.h>
#include <shyft/energy_market/stm/srv/task/stm_task.h>
#include <test/test_utils.h>

using std::to_string;
using std::string;
using std::vector;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::no_utctime;
using shyft::core::to_seconds64;
using shyft::core::from_seconds;
using shyft::core::utcperiod;

TEST_SUITE("task_server") {
    using shyft::energy_market::stm::srv::task::server;
    using shyft::energy_market::stm::srv::task::client;
    using shyft::energy_market::stm::srv::stm_task;
    using shyft::energy_market::stm::srv::stm_case;
    using shyft::energy_market::stm::srv::model_ref;
    using shyft::srv::model_info;
    using std::pair;
    //-- keep test directory unique and with auto-cleanup.
    auto run_dir = "stm.run.server.test." + to_string(to_seconds64(utctime_now()));
    test::utils::temp_dir srvdir(run_dir.c_str());

    TEST_CASE("server") {
        // 0. Arrange server and a client running on localhost, tempdir:
        server s0(srvdir.string());
        string host_ip = "127.0.0.1";
        s0.set_listening_ip(host_ip);
        auto s_port = s0.start_server();
        CHECK_GE(s_port, 1024);
        vector<pair<int,string>> cb_x; // arrange to capture callbacks
        s0.fx_cb=[&cb_x](int64_t mid,string arg) {
            cb_x.emplace_back(mid,arg);
            return cb_x.size()==1;// true first time, then false, to verify we propagate back the result
        };
        client c{host_ip + ":" + to_string(s_port)};
        vector<int64_t> mids;
        auto mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 0);
        // 1. Create one stm_task:
        auto task = std::make_shared<stm_task>(1, "testtask", from_seconds(3600));
        task->labels.push_back("test");

        auto r = std::make_shared<stm_case>(1, "testrun", from_seconds(3600));
        r->model_refs.push_back(std::make_shared<model_ref>("host", 123, 456, "key"));
        model_info mi(task->id, task->name, task->created, task->json);
        task->add_case(r);
        // 2. store it to server:
        auto rid = c.store_model(task, mi);
        CHECK_EQ(rid, task->id);
        
        // 2.a make a callback
        CHECK_EQ(true,c.fx(1,"cb"));
        CHECK_EQ(false,c.fx(2,"2"));
        CHECK_EQ(cb_x,vector<pair<int,string>>{std::make_pair(1,"cb"),std::make_pair(2,"2")});

        // 3. verify model info and run:
        auto task2 = c.read_model(task->id);
        CHECK_EQ(*task, *task2);
        mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 1u);
        CHECK_EQ(mi, mis[0]);

        // 4. Filtered get_model_infos search:
        auto mis2 = c.get_model_infos(mids, utcperiod(0, 1000));
        CHECK_EQ(mis2.size(), 0u);
        mis2 = c.get_model_infos(mids, utcperiod(2000, 4000));
        CHECK_EQ(mis2.size(), 1u);
        CHECK_EQ(mi, mis2[0]);

        c.close(); // To force auto-open on next call
        // 5. Check that we can update model info of existing run
        mi.json = string("{\"updated\": true}");
        c.update_model_info(mi.id, mi);
        mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 1u);
        CHECK_EQ(mi, mis[0]);

        // 6. Add run to a task:
        auto r2 = std::make_shared<stm_case>(2, "testrun2", from_seconds(10));
        c.add_case(task->id, r2);
        task2 = c.read_model(task->id);
        CHECK_EQ(task2->cases.size(), 2);
        CHECK_THROWS(c.add_case(1, r2));

        // 7. Get run from a task:
        CHECK_EQ(*r, *(c.get_case(task->id, r->id)));
        CHECK_EQ(nullptr, c.get_case(task->id, 4));
        CHECK_THROWS(c.get_case(-1, r->id));

        CHECK_EQ(*r2, *(c.get_case(task->id, r2->name)));
        CHECK_EQ(nullptr, c.get_case(task->id, "norun"));

        // 8. Modify run inplace
        auto r2_get = c.get_case(task->id, r2->name);
        r2_get->name = "run_test";
        r2_get->json = "{'key': value}";
        r2_get->labels.insert(r2_get->labels.end(), {"test", "calibration"});
        c.update_case(task->id, *r2_get);
        auto r2_updated = c.get_case(task->id, r2_get->name);
        CHECK_EQ(r2_updated->name, "run_test");
        CHECK_EQ(r2_updated->json, "{'key': value}");
        CHECK_EQ(r2_updated->labels, std::vector<std::string>{"test", "calibration"});

        // 9. Add a model_reference to a run:
        auto mr = std::make_shared<model_ref>("host", 12, 34, "testkey");
        c.add_model_ref(task->id, r->id, mr);
        CHECK_EQ(*mr, *(c.get_model_ref(task->id, r->id, "testkey")));
        CHECK_EQ(nullptr, c.get_model_ref(task->id, r->id, "nonkey"));
        CHECK_EQ(nullptr, c.get_model_ref(task->id, -1, "testkey"));
        CHECK_THROWS(c.get_model_ref(-1, r->id, "testkey"));

        // Some invalid inputs:
        CHECK_EQ(c.get_case(task->id, r->id)->model_refs.size(), 2);
        c.add_model_ref(task->id, -1, mr); // Invalid run ID, so should not be added.
        CHECK_EQ(c.get_case(task->id, r->id)->model_refs.size(), 2);

        // 10. Remove model reference from a run:
        CHECK(c.remove_model_ref(task->id, r->id, "testkey"));
        CHECK_EQ(c.get_case(task->id, r->id)->model_refs.size(), 1);
        CHECK_THROWS(c.remove_model_ref(-1, r->id, "testkey"));
        CHECK_FALSE(c.remove_model_ref(task->id, -1, "testkey"));
        CHECK_FALSE(c.remove_model_ref(task->id, r->id, "nonkey"));

        // 11. Remove runs from a task:
        CHECK_FALSE(c.remove_case(task->id, 3));
        CHECK(c.remove_case(task->id, 2));
        task2 = c.read_model(task->id);
        CHECK_EQ(task2->cases.size(), 1);
        CHECK_FALSE(c.remove_case(task->id, 2));

        CHECK_FALSE(c.remove_case(task->id, "norun"));
        CHECK(c.remove_case(task->id, "testrun"));
        task2 = c.read_model(task->id);
        CHECK_EQ(task2->cases.size(), 0);
        CHECK_FALSE(c.remove_case(task->id, "testrun"));
        // Finally, remove the model, and verify we're are back to zero:
        c.remove_model(task->id);
        mis = c.get_model_infos(mids);
        CHECK_EQ(mis.size(), 0u);
        c.close();
        s0.clear();

    }

}
