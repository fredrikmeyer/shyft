#include <build_test_model.h>
#include <doctest/doctest.h>
#include <iostream>
#include <vector>
#include <limits>
#include <memory>

#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/graph/hps_graph_adaptor.h>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <shyft/energy_market/graph/graph_utilities.h>

using namespace shyft::energy_market::hydro_power;
using namespace shyft::energy_market::graph;

namespace test {

    class edge_weight_map: public boost::put_get_helper<int, edge_weight_map> {
    public:
        using category = boost::readable_property_map_tag;
        using value_type = int;
        using reference = int;
        using key_type = boost::graph_traits<hydro_power_system>::edge_descriptor;

        edge_weight_map() = default;

        value_type operator[](const key_type& e) const {
            // We use the connection_role to filter out bypass and flood.
            // If water_route is the source we need to check the role of the connection
            // target -> source
            auto conn = e.second;
            auto source = std::dynamic_pointer_cast<waterway>(e.first);
            auto target = conn.target;
            connection_role role;
            if (source) {
                // We find the corresponding edge target -> source:
                traits::out_edge_iterator ei, ei_end;
                boost::tie(ei, ei_end) = out_edges(target, *(target->hps_()));
                auto it = std::find_if(ei, ei_end,
                    [&source](auto el) { return el.second.target->id == source->id; });
                role = it->second.role;

            } else role = conn.role;
            if (role == connection_role::bypass || role == connection_role::flood) {
                //return std::numeric_limits<int>::max();
                return 1000; // To not get overflow.
            } else
                return 1;
        }
    };

    template<class PredecessorMap>
    class record_predecessor : public boost::dijkstra_visitor<> {
    public:
        record_predecessor(PredecessorMap p): m_predecessor(p) { }

        template<class Edge, class Graph>
            void edge_relaxed(Edge e, Graph& g) {
            put(m_predecessor, target(e, g), source(e,g));
        }
    private:
        PredecessorMap m_predecessor;
    };

    template<class PredecessorMap>
    record_predecessor<PredecessorMap> make_predecessor_recorder(PredecessorMap p) {
        return record_predecessor<PredecessorMap>(p);
    }

    hydro_power_system_ create_vertex_iter_test_hps(size_t n_rsv, size_t n_unit, size_t n_wtr) {
        hydro_power_system_ hps = make_shared<hydro_power_system>(0, "vertex_iter_test_hps");
        auto builder = std::make_unique<hydro_power_system_builder>(hps);

        size_t id = 1;
        for (size_t i = 0; i < n_rsv; ++i,++id)
            builder->create_reservoir(id, "test_reservoir_" + std::to_string(id), "");

        for (size_t i = 0; i < n_unit; ++i,++id)
            builder->create_unit(id, "test_unit_" + std::to_string(id), "");

        for (size_t i = 0; i < n_wtr; ++i,++id)
            builder->create_waterway(id, "test_waterway_" + std::to_string(id), "");

        return hps;
    }

    hydro_power_system_ create_edge_iter_test_hps() {
        auto hps = make_shared<hydro_power_system>(0, "edge_iterator_test_hps");
        auto builder = make_unique<hydro_power_system_builder>(hps);

        size_t id = 1;
        for (size_t i = 0; i < 3; ++i,++id)
            builder->create_reservoir(id, "test_reservoir_" + std::to_string(id), "");

        for (size_t i = 0; i < 3; ++i,++id)
            builder->create_unit(id, "test_unit_" + std::to_string(id), "");

        for (size_t i = 0; i < 3; ++i,++id)
            builder->create_waterway(id, "test_waterway_" + std::to_string(id), "");

        // connect only some of the components
        connect(hps->waterways[1])
            .input_from(hps->reservoirs[1])
            .output_to(hps->units[1]);

        return hps;
    }
}

TEST_SUITE("hps_graph") {
    using std::make_unique;
    auto hps = test::build_hps("test_hps");
    auto index_map = boost::get(boost::vertex_index, *hps);
    auto name_map = boost::get(boost::vertex_name, *hps);
    auto weight_map = test::edge_weight_map();


    TEST_CASE("vertices_throws_if_empty") {

            auto hps = test::create_vertex_iter_test_hps(0, 0, 0);
            CHECK_THROWS_AS(vertices(*hps), std::runtime_error);
    }

    TEST_CASE("vertices_iterator_points_to_first") {
        // Test that that the first iterator returnd by `vertices` points to first vertex

        SUBCASE("first_is_reservoir") {
            auto hps = test::create_vertex_iter_test_hps(1, 0, 0);
            auto [vertices_begin, vertices_end] = vertices(*hps);

            CHECK_EQ(*vertices_begin, hps->reservoirs[0]);
        }

        SUBCASE("first_is_unit") {
            auto hps = test::create_vertex_iter_test_hps(0, 1, 0);
            auto [vertices_begin, vertices_end] = vertices(*hps);

            CHECK_EQ(*vertices_begin, hps->units[0]);
        }

        SUBCASE("first_is_waterway") {
            auto hps = test::create_vertex_iter_test_hps(0, 0, 1);
            auto [vertices_begin, vertices_end] = vertices(*hps);

            CHECK_EQ(*vertices_begin, hps->waterways[0]);
        }
    }

    TEST_CASE("vertex_iter_increment_to_different_type") {
        // Test that decrementing vertex_iterator works as expected
        // when moving from one component vector to another.

        SUBCASE("reservoir_and_unit") {
            auto hps = test::create_vertex_iter_test_hps(1, 1, 0);
            auto& rsv = hps->reservoirs;
            auto& unit = hps->units;

            auto [vi, vertices_end] = vertices(*hps);

            CHECK_EQ(*vi, rsv[0]);
            ++vi;
            CHECK_EQ(*vi, unit[0]);
            ++vi;
            CHECK_EQ(vi, vertices_end);
        }


        SUBCASE("reservoir_and_waterway") {
            auto hps = test::create_vertex_iter_test_hps(1, 0, 1);
            auto& rsv = hps->reservoirs;
            auto& wtr = hps->waterways;

            auto [vi, vertices_end] = vertices(*hps);

            CHECK_EQ(*vi, rsv[0]);
            ++vi;
            CHECK_EQ(*vi, wtr[0]);
            ++vi;
            CHECK_EQ(vi, vertices_end);
        }

        SUBCASE("unit_and_waterway") {
            auto hps = test::create_vertex_iter_test_hps(0, 1, 1);
            auto& unit = hps->units;
            auto& wtr = hps->waterways;

            auto [vi, vertices_end] = vertices(*hps);

            CHECK_EQ(*vi, unit[0]);
            ++vi;
            CHECK_EQ(*vi, wtr[0]);
            ++vi;
            CHECK_EQ(vi, vertices_end);
        }
    }

    TEST_CASE("vertex_iter_decrement_to_different_type") {
        // Test that decrementing vertex_iterator works as expected
        // when moving from one component vector to another.
        SUBCASE("reservoir_and_unit") {
            auto hps = test::create_vertex_iter_test_hps(1, 1, 0);
            auto& rsv = hps->reservoirs;
            auto& unit = hps->units;

            auto [vertices_end, vi] = vertices(*hps);

            --vi;
            CHECK_EQ(*vi, unit[0]);
            --vi;
            CHECK_EQ(*vi, rsv[0]);
        }

        SUBCASE("reservoir_and_waterway") {
            auto hps = test::create_vertex_iter_test_hps(1, 0, 1);
            auto& rsv = hps->reservoirs;
            auto& wtr = hps->waterways;

            auto [vertices_end, vi] = vertices(*hps);

            --vi;
            CHECK_EQ(*vi, wtr[0]);
            --vi;
            CHECK_EQ(*vi, rsv[0]);
        }

        SUBCASE("unit_and_waterway") {
            auto hps = test::create_vertex_iter_test_hps(0, 1, 1);
            auto& unit = hps->units;
            auto& wtr = hps->waterways;

            auto [vertices_end, vi] = vertices(*hps);

            --vi;
            CHECK_EQ(*vi, wtr[0]);
            --vi;
            CHECK_EQ(*vi, unit[0]);
        }
    }

    TEST_CASE("out_edge_iterator"){
        auto hps_ei = make_shared<hydro_power_system>("out_edge_iterator_tester");
        auto builder = make_unique<hydro_power_system_builder>(hps_ei);
        // Add hydro_components:
        auto r1 = builder->create_reservoir(1, "r1", "misc data");
        auto u1 = builder->create_unit(1, "u1", "misc data");
        auto wtr1 = builder->create_waterway(1, "wtr1", "misc data");

        traits::out_edge_iterator ei, eib, ei_end;
        // When nothing's connected:
        CHECK_EQ(out_degree(r1, *hps), 0);
        CHECK_EQ(in_degree(r1, *hps), degree(r1, *hps));
        boost::tie(ei, ei_end) = out_edges(r1, *hps_ei);
        CHECK_THROWS_AS(*ei, std::runtime_error);

        // Make connections, and test iterators:
        connect(wtr1).input_from(r1).output_to(u1);
        CHECK_EQ(out_degree(r1, *hps), 1);
        CHECK_EQ(out_degree(wtr1, *hps), 2);
        CHECK_EQ(out_degree(u1, *hps), 1);
        // For the reservoir
        boost::tie(ei, ei_end) = out_edges(r1, *hps);
        CHECK_EQ((*ei).second, r1->downstreams[0]);
        CHECK_EQ(*(*ei).first, *r1);
        CHECK_EQ((*ei).first, r1);
        ++ei;
        CHECK_EQ(ei, ei_end);
        // For the waterway:
        boost::tie(ei, ei_end) = out_edges(wtr1, *hps);
        CHECK_EQ((*ei).second, wtr1->downstreams[0]);
        ++ei;
        CHECK_EQ((*ei).second, wtr1->upstreams[0]);
        ++ei;
        CHECK_EQ(ei ,ei_end);
        --ei;
        CHECK_EQ((*ei).second, wtr1->upstreams[0]);
        --ei;
        CHECK_EQ((*ei).second, wtr1->downstreams[0]);

        // For the unit (doesn't have downstreams):
        boost::tie(ei, ei_end) = out_edges(u1, *hps);
        CHECK_EQ((*ei).second, u1->upstreams[0]);
        ++ei;
        CHECK_EQ(ei, ei_end);

    }

    TEST_CASE("edges_throws_if_hps_empty") {
        auto hps = test::create_vertex_iter_test_hps(0, 0, 0);
            CHECK_THROWS_AS(edges(*hps), std::runtime_error);
    }

    TEST_CASE("edges_throws_if_hps_has_no_connections") {
        auto hps = test::create_vertex_iter_test_hps(1, 1, 1);
        CHECK_THROWS_AS(edges(*hps), std::runtime_error);
    }

    TEST_CASE("edge_iterator_increment") {
        auto hps = test::create_edge_iter_test_hps();

        auto wtr = hps->waterways[1];
        auto rsv = hps->reservoirs[1];
        auto unit = hps->units[1];

        auto [edge_it, edges_end] = edges(*hps);

        // expected rsv -> wtr
        CHECK_EQ(edge_it->first, rsv);
        CHECK_EQ(edge_it->second.target_(), wtr);

        ++edge_it;

        // expectd unit -> wtr (input)
        CHECK_EQ(edge_it->first, unit);
        CHECK_EQ(edge_it->second.target_(), wtr);

        ++edge_it;

        // expectd wtr -> unit
        CHECK_EQ(edge_it->first, wtr);
        CHECK_EQ(edge_it->second.target_(), unit);

        ++edge_it;

        // expectd wtr -> rsv (input)
        CHECK_EQ(edge_it->first, wtr);
        CHECK_EQ(edge_it->second.target_(), rsv);

        ++edge_it;

        // This should be the end
        CHECK_EQ(edge_it, edges_end);
        CHECK_THROWS_AS(*edge_it, std::runtime_error);
    }

    TEST_CASE("edge_iterator_decrement") {
        auto hps = test::create_edge_iter_test_hps();

        auto wtr = hps->waterways[1];
        auto rsv = hps->reservoirs[1];
        auto unit = hps->units[1];

        auto [edges_begin, edge_it] = edges(*hps);

        --edge_it;

        CHECK_EQ(edge_it->first, wtr);
        CHECK_EQ(edge_it->second.target_(), rsv);

        --edge_it;

        // expectd wtr -> unit
        CHECK_EQ(edge_it->first, wtr);
        CHECK_EQ(edge_it->second.target_(), unit);

        --edge_it;

        // expectd unit -> wtr (input)
        CHECK_EQ(edge_it->first, unit);
        CHECK_EQ(edge_it->second.target_(), wtr);

        --edge_it;

        // This should be the begining
        CHECK_EQ(edge_it, edges_begin);
    }

    TEST_CASE("core_functionality") {
        auto num_nodes = hps->reservoirs.size() + hps->units.size() + hps->waterways.size();
        CHECK_EQ(num_vertices(*hps), num_nodes);

        // 1. Check VertexListGraph stuff:
        traits::vertex_iterator vi, vi_end;
        traits::vertex_descriptor v;
        int counter = 0;
        for(boost::tie(vi, vi_end) = vertices(*hps); vi != vi_end; ++vi) {
            v = *vi;
            reservoir_ rsv = std::dynamic_pointer_cast<reservoir>(v);
            if (rsv) REQUIRE(hps->find_reservoir_by_id(rsv->id));
            unit_ u = std::dynamic_pointer_cast<unit>(v);
            if (u) REQUIRE(hps->find_unit_by_id(u->id));
            waterway_ wtr = std::dynamic_pointer_cast<waterway>(v);
            if (wtr) REQUIRE(hps->find_waterway_by_id(wtr->id));

            // Also check that the property maps for vertices are properly set:
            CHECK_EQ(index_map[v], counter);
            CHECK_EQ(name_map[v], v->name);
            ++counter;
        }
         // And in reverse:
         boost::tie(vi, vi_end) = vertices(*hps);

        // Check IncidenceGraph stuff:
        traits::out_edge_iterator ei, ei_end;
        for(boost::tie(vi, vi_end) = vertices(*hps); vi != vi_end; ++vi) {
            v = *vi;
            CHECK_EQ(v->downstreams.size() + v->upstreams.size(), out_degree(v, *hps));
            int oe_index = 0;
            int offset = v->downstreams.size();
            for (boost::tie(ei, ei_end) = out_edges(v, *hps); ei != ei_end; ++ei) {
                traits::edge_descriptor e = *ei;
                CHECK_EQ(*source(e,*hps), *v);
                if (oe_index < offset)
                    CHECK_EQ(*target(e,*hps), *(v->downstreams[oe_index].target));
                else
                    CHECK_EQ(*target(e, *hps), *(v->upstreams[oe_index-offset].target));
                oe_index++;
            }
        }
    }
    TEST_CASE("rsv_downstream_upstream") {
        using namespace shyft::energy_market::graph;
        auto hpsm = make_shared<hydro_power_system>("name");
        auto hps = make_unique<hydro_power_system_builder>(hpsm);
        auto r1 = hps->create_reservoir(1,"r1", R"(1)");
        auto r2 = hps->create_reservoir(2,"r2", R"(2)");
		auto t1 = hps->create_tunnel(1, "r1-t1", "");
		auto t2 = hps->create_tunnel(2, "t1-r2", "");
        connect(t1).input_from(r1).output_to(t2);
        connect(t2).output_to(r2);
        auto ds_r1=downstream_reservoirs(r1,0);
        auto us_r2=upstream_reservoirs(r2,0);
        CHECK(ds_r1.size()==1u);
        CHECK(us_r2.size()==1u);
        CHECK(ds_r1[0]->id == 2);
        CHECK(us_r2[0]->id == 1);
    }
    TEST_CASE("distance_to_ocean") {
        // Set source node:
        traits::vertex_descriptor src = hps->find_reservoir_by_name("ocean");
        // Set up vector to store distances:
        std::vector<int> d(num_vertices(*hps));
        std::vector<int> expected = {4, 6, 4, 0, // reservoirs
                                     2, 2,       // units
                                     3, 5, 1, 3, 1, 1, 1}; // waterways
        // Distance property map:
        auto distances = boost::make_iterator_property_map(d.begin(),
                                                           index_map);
        // Invoke Dijkstra's algorithm:
        boost::dijkstra_shortest_paths(*hps, src,
            boost::distance_map(distances)
            .weight_map(weight_map));
        // Check all vertex distances:
        traits::vertex_iterator vi, vi_end;
        for (boost::tie(vi ,vi_end) = vertices(*hps); vi != vi_end; ++vi) {
            CHECK_EQ(distances[*vi], expected[index_map[*vi]]);
        }
    }

    TEST_CASE("path_to_ocean") {
        // Set source node:
        traits::vertex_descriptor src = hps->find_reservoir_by_name("ocean");
        // Set vector to store distances:
        std::vector<int> d(num_vertices(*hps));
        auto distances = boost::make_iterator_property_map(d.begin(),
                                                           index_map);
        // Set up predecessor recorder:
        std::vector<traits::vertex_descriptor> p(num_vertices(*hps), nullptr);
        auto predecessors = boost::make_iterator_property_map(p.begin(), index_map);
        // Do Dijkstra's:
        boost::dijkstra_shortest_paths(*hps, src,
            boost::distance_map(distances)
            .weight_map(weight_map)
            .visitor(test::make_predecessor_recorder(predecessors)));

        // We get the path from r2:
        std::vector<int> expected = {1, 7, 0, 6, 4, 8, 3}; // ID are based in index_map
        int counter = 0;
        traits::vertex_descriptor v = hps->find_reservoir_by_name("r2");
        while (v) {
            CHECK_EQ(index_map[v], expected[counter]);
            counter++;
            // Get predecessor:
            v = predecessors[v];
        }
        CHECK_EQ(counter, expected.size());
    }

    TEST_CASE("boost_graph_concepts") {
        // run boost own check that the concepts requirements are satisfied
        SUBCASE("VertexListGraph") {
            BOOST_CONCEPT_ASSERT(( boost::VertexListGraphConcept<hydro_power_system> ));
        }

        SUBCASE("IncidenceGraph") {
            BOOST_CONCEPT_ASSERT(( boost::IncidenceGraphConcept<hydro_power_system> ));
        }

        SUBCASE("BidirectionalGraph") {
            BOOST_CONCEPT_ASSERT(( boost::BidirectionalGraphConcept<hydro_power_system> ));
        }
    }
}
