
#include <doctest/doctest.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/graph/graph_utilities.h>

#include <serialize_loop.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

using namespace shyft::energy_market::hydro_power;
using namespace shyft::energy_market;
using test::serialize_loop;
namespace test_serial {
    
    namespace v0 {
        struct agg { // original version
            int id;
            string name;
            agg()=default;
            agg(int id,string name):id(id),name(name){}
            bool operator==(agg const&o) const {return id==o.id && name==o.name;}
            bool operator!=(agg const&o) const {return !operator==(o);}
            x_serialize_decl();
        };
        
        struct plant {
            int id;
            string name;
            vector<shared_ptr<agg>> aggs;
            bool operator==(plant const&o) const {return id==o.id && name==o.name && aggs==o.aggs;}
            bool operator!=(plant const&o) const {return !operator==(o);}

            x_serialize_decl();
        };
    }
    
    inline namespace v1 {
        struct unit {// new version, same layout
            int id;
            string name;
            bool operator==(unit const&o) const {return id==o.id && name==o.name;}
            bool operator!=(unit const&o) const {return !operator==(o);}

            x_serialize_decl();
        };
        
        struct plant {
            int id;
            string name;
            vector<shared_ptr<unit>> units;
            bool operator==(plant const&o) const {return id==o.id && name==o.name && units==o.units;}
            bool operator!=(plant const&o) const {return !operator==(o);}

            x_serialize_decl();
        };
    }
    

}
x_serialize_export_key(test_serial::v0::agg);
x_serialize_export_key(test_serial::v1::unit);
x_serialize_export_key(test_serial::v0::plant);
x_serialize_export_key(test_serial::v1::plant);
BOOST_CLASS_VERSION(test_serial::v1::unit, 1);
BOOST_CLASS_VERSION(test_serial::v1::plant, 1);

using boost::serialization::make_nvp;

template<class Archive>
void test_serial::v1::unit::serialize(Archive & ar, const unsigned int ) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    ;
}

template<class Archive>
void test_serial::v1::plant::serialize(Archive & ar, const unsigned int ) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("units",units)
    ;
}


template<class Archive>
void test_serial::v0::agg::serialize(Archive & ar, const unsigned int ) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    ;
}

template<class Archive>
void test_serial::v0::plant::serialize(Archive & ar, const unsigned int ) {
    ar
    & make_nvp("id",id)
    & make_nvp("name",name)
    & make_nvp("aggs",aggs)
    ;
}


#define xxx_arch(T) x_serialize_archive(T,boost::archive::binary_oarchive,boost::archive::binary_iarchive)
xxx_arch(test_serial::v0::agg);
xxx_arch(test_serial::v1::unit);
xxx_arch(test_serial::v1::plant);
xxx_arch(test_serial::v0::plant);

namespace test {
template <class T,class R>
static R serialize_loop2(const T& o) {
    using namespace std;
    ostringstream xmls;
    /* scope this, ensure archive is flushed/destroyd */{
        boost::archive::binary_oarchive oa(xmls);
        oa << BOOST_SERIALIZATION_NVP(o); 
    }
    xmls.flush();
    string ss = xmls.str();
   
    R o2;
    istringstream xmli(ss);
    {
        boost::archive::binary_iarchive ia(xmli);
        ia >> BOOST_SERIALIZATION_NVP(o2);
    }
    return o2;
}
}
using std::make_shared;
using std::vector;
using std::string;
using std::shared_ptr;

TEST_SUITE("serialize_version") {
    TEST_CASE("serialize_read_old_version") {
        test_serial::v0::agg a{1,"a1"};
        auto u=test::serialize_loop2<test_serial::v0::agg,test_serial::v1::unit>(a);
        CHECK_EQ(a.id,u.id);
        CHECK_EQ(a.name,u.name);
    }
    TEST_CASE("serialize_read_old_plant_version") {
        test_serial::v0::plant p0{1,"a1",{}};
        p0.aggs.push_back(make_shared<test_serial::v0::agg>(1,string("a")));
        auto p1=test::serialize_loop2<test_serial::v0::plant,test_serial::v1::plant>(p0);
        CHECK_EQ(p0.id,p1.id);
        CHECK_EQ(p0.name,p1.name);
        CHECK_EQ(p1.units.size(),1);
        CHECK_EQ(p1.units[0]->id,1);
        CHECK_EQ(p1.units[0]->name,"a");
        
    }
}

TEST_SUITE("hps") {

TEST_CASE("unit_basics") {
    using namespace shyft::energy_market::hydro_power;
    // note have to .. shared, because we are using
    // shared_from this, and that requires a heap alloc block
    auto p0 = make_shared<unit>();
    auto p0c= make_shared<unit>(0,"","",nullptr);
    CHECK(*p0 == *p0c);
    p0c->name = "abc";
    CHECK(*p0 != *p0c);
    CHECK(p0->equal_structure(*p0c));
    p0->id=123;
    CHECK(!p0->equal_structure(*p0c));

    auto p1 = make_shared<unit>(1,"kvilldal", "xx", nullptr);

    auto p1_s = serialize_loop(p1);
    CHECK(*p1 == *p1_s);

}
TEST_CASE("power_plant_basics") {
    using namespace shyft::energy_market::hydro_power;
    // note have to .. shared, because we are using
    // shared_from this, and that requires a heap alloc block
    auto s = make_shared<hydro_power_system>(1,"s");
    auto p0 = make_shared<power_plant>();
    auto p0c= make_shared<power_plant>(0,"","",nullptr);
    CHECK(*p0 == *p0c);
    CHECK(p0->equal_structure(*p0c));
    p0c->id = 123;
    CHECK(*p0 != *p0c);
    p0c->id=p0->id;
    CHECK(*p0 == *p0c);
    auto a1 = make_shared<unit>(1,"kvilldal", "G1", nullptr);
    auto a2 = make_shared<unit>(1,"kvilldal", "G1", nullptr);
    p0->hps=s;s->power_plants.push_back(p0);
    a1->hps=s;s->units.push_back(a1);
    power_plant::add_unit(p0,a1);
    CHECK(a1->pwr_station_()==p0);
    CHECK(p0->units.size()==1);
    // yes, works: p0->units.clear();//will remove cycle refs.
    
    CHECK(*p0 != *p0c);
    p0c->hps=s;s->power_plants.push_back(p0c);
    a2->hps=s;s->units.push_back(a2);
    power_plant::add_unit(p0c,a2);
    CHECK(*p0 == *p0c);
    a1->id=123;
    CHECK(*p0 != *p0c);
    CHECK(!p0->equal_structure(*p0c));
    a1->id=1;
    CHECK(p0->equal_structure(*p0c));
    p0->remove_unit(a1);
    CHECK(!p0->equal_structure(*p0c));
    CHECK(p0->units.size()==0);
    CHECK(a1->pwr_station_()==nullptr);
    auto p1 = make_shared<power_plant>(1,"kvilldal", "xx", s);
    auto p1_s = serialize_loop(p1);
    CHECK(*p1 == *p1_s);
    CHECK(p1->equal_structure(*p1_s));

}

TEST_CASE("reservoir_basics") {
    using namespace shyft::energy_market::hydro_power;
    auto r0 = make_shared<reservoir>();
    auto r0c = make_shared<reservoir>(0,"", "", nullptr);
    CHECK(*r0 == *r0c);
    r0c->json = "somestuff";
    CHECK(*r0 != *r0c);
    auto r1 = make_shared<reservoir>(1,"blåsjø", "{\"reservoir_data\":[1000, 1100, 1400.0]}", nullptr);
    auto r1_s = serialize_loop(r1);
    CHECK(*r1 == *r1_s);
    r1_s->json = "0.3";
    CHECK(*r1 != *r1_s);
}

TEST_CASE("waterway_basics") {
    using namespace shyft::energy_market::hydro_power;
    auto s = make_shared<hydro_power_system>(1,"s");

    auto w0 = make_shared<waterway>();
    auto w0c = make_shared<waterway>(0,"", "", nullptr);
    CHECK(*w0 == *w0c);
    w0->id = 90;
    CHECK(*w0 != *w0c);
    auto w1 = make_shared<waterway>(2,"abc", "json", nullptr);
    CHECK(*w0 != *w1);
    w1->hps=s;s->waterways.push_back(w1);
    auto g1 = make_shared<gate>(0, "1", "2");
    //g1->hps = s; s->gates.push_back(g1);
    waterway::add_gate(w1, g1);// ensure it's a full structure
    auto w1_s = serialize_loop(w1);
    CHECK(*w1 == *w1_s);
    REQUIRE(w1_s->gates.size() == 1u);
    CHECK(w1_s->gates[0]->wtr_() == w1_s);// ensure that uplink survived serialization
    w1->name = "z3";
    CHECK(*w1 != *w1_s);
    w1->name = w1_s->name;
    CHECK(*w1 == *w1_s);//ensure they are equal before.
    auto g2 = make_shared<gate>(0, "1", "2");
    //g2->hps = s; s->gates.push_back(g2);
    waterway::add_gate(w1, g2);
    CHECK(*w1 != *w1_s);//now different
    w1_s->hps=s;s->waterways.push_back(w1_s);
    auto g3 = make_shared<gate>(0, "1", "2");
    //g3->hps = s; s->gates.push_back(g3);
    waterway::add_gate(w1_s, g3);
    CHECK(*w1 == *w1_s);//now equal again
    auto s0 = w1->gates.size();
    w1->remove_gate(w1->gates[0]);
    CHECK(w1->gates.size() == s0-1u);
    CHECK(*w1 != *w1_s);//now different
}

TEST_CASE("gate_basics") {
    using namespace shyft::energy_market::hydro_power;
    auto g0 = make_shared<gate>();
    auto g0c = make_shared<gate>(0, "", string(""));
    CHECK(*g0 == *g0c);
    auto g1 = make_shared<gate>(1, "2", string("3"));
    CHECK(*g0 != *g1);
    auto g1_s = serialize_loop(g1);
    CHECK(*g1 == *g1_s);
    g1->id++;
    CHECK(*g1 != *g1_s);
}

TEST_CASE("hps_basics") {
    using namespace shyft::energy_market::hydro_power;
    auto s0 = make_shared<hydro_power_system>();
    CHECK(s0->id == 0);
    CHECK(s0->name == "");
    CHECK(s0->created == no_utctime);
    auto s1 = make_shared<hydro_power_system>("s1");
    s0->id=s1->id;
    CHECK(*s0!=*s1);
    s0->name=s1->name;
    CHECK(*s0==*s1);

    CHECK(s1->name == "s1");
    s1->id = 33;
    CHECK(s0->equal_structure(*s1) == true);
}
}

namespace test {
    using namespace shyft::energy_market::hydro_power;
    using namespace std;
    hydro_power_system_ create_hydro_power_system() {
        /*
        Demonstrates how to build an inmemory representation of  HydroPowerSystem
        corresponding to the term 'detailed-hydro' in EMPS,
        using part of the blåsjø/ulla-førre systems

        Returns
        ------ -
        HydroPowerSystem with reservoirs, reservoir aggregates, tunnels, powerplants, including pumps
        */
        auto sorland = make_shared<hydro_power_system>(1,"sørland");
        hydro_power_system_builder hps(sorland);
        auto blasjo = hps.create_reservoir(1,"blåsjø",string("{\"max_vol\":\"3200Mm3\"}"));
        auto saurdal = hps.create_unit (2,"saurdal","{\"max_eff\":\"600MW\"}");
        auto saurdal_ps= hps.create_power_plant (2000,"saurdal","{}");
        power_plant::add_unit(saurdal_ps,saurdal);
        auto tunx = hps.create_tunnel(12,"blåsjø-saurdal","{}");
        connect(tunx)
            .input_from(blasjo)
            .output_to(saurdal);
        auto sandsavatn = hps.create_reservoir(3,"sandsvatn", "{\"reservoir_data\":[560.0, 605.0, 230.0]}");
        auto lauvastolsvatn = hps.create_reservoir(4,"lauvastølsvatn", "{\"reservoir_data\":[590.0, 605.0, 8.3]}");
        auto kvilldal1 = hps.create_unit (51,"kvilldal_1","{\"Ek\":1.3,\"outlet_level_masl\":70}");
        auto kvilldal2 = hps.create_unit (52,"kvilldal_2","{\"Ek\":1.3,\"outlet_level_masl\":70}");
        auto kvilldal = hps.create_power_plant (5000,"kvilldal","{}");
        power_plant::add_unit(kvilldal,kvilldal1);
        power_plant::add_unit(kvilldal,kvilldal2);
        auto t_kvilldal = hps.create_tunnel(510,"kvilldal hovedtunnel", "{\"alpha\":0.000053}");
        auto t_kvill_penstock_1 = hps.create_tunnel(5101,"kvilldal_1_penstock","{}");
        auto t_kvill_penstock_2 = hps.create_tunnel(5102,"kvilldal_2_penstock","{}");
        auto t_saur_kvill = hps.create_tunnel(6,"saurdal-kvilldal-hoved-tunnel","{}");
        auto t_sandsa_kvill = hps.create_tunnel(7,"sandsavatn-til-kvilldal", "{}");
        auto t_sandsa_kvill_g = hps.create_gate(1, "gate1", "{\"info\":\"open or closed\"}");
        waterway::add_gate(t_sandsa_kvill, t_sandsa_kvill_g);
        auto t_lauvas_kvill = hps.create_tunnel(8,"lauvastølsvatn-til-kvilldal", "{}");
        connect(saurdal).output_to(t_saur_kvill);
        connect(t_saur_kvill).output_to(t_kvilldal);
        connect(t_kvilldal).output_to(t_kvill_penstock_1);
        connect(t_kvill_penstock_1).output_to(kvilldal1);
        connect(t_kvilldal).output_to(t_kvill_penstock_2);
        connect(t_kvill_penstock_2).output_to(kvilldal2);

        connect(t_sandsa_kvill).input_from(sandsavatn).output_to(t_kvilldal);
        connect(t_lauvas_kvill).input_from(lauvastolsvatn).output_to(t_kvilldal);

        auto vassbotvatn = hps.create_reservoir(9,"vassbotvatn", "{}");
        auto stoelsdal_pumpe = hps.create_unit (10,"stølsdal pumpe");
        auto stoelsdal = hps.create_power_plant (1,"stølsdal","{}");
        power_plant::add_unit(stoelsdal,stoelsdal_pumpe);
        auto above_vassbotvatn = hps.create_tunnel(11,"stølsdals kraftstasjon(pumpe) til vassbotvatn det pumpes fra", "{}");
        connect(above_vassbotvatn)
            .input_from(stoelsdal_pumpe);
        connect(vassbotvatn).input_from(above_vassbotvatn);
        auto tun_sandsa_stolsdal = hps.create_tunnel(120, "fra sandsvatn til stølsdal pump", "{}");
        connect(tun_sandsa_stolsdal).output_to(stoelsdal_pumpe);
        connect(sandsavatn).output_to(tun_sandsa_stolsdal, connection_role::main);

        auto suldalsvatn = hps.create_reservoir(13,"suldalsvatn","{}");
        auto hylen = hps.create_unit (14,"hylen","{}");
        auto hylen_ps = hps.create_power_plant (1400,"hylen","{}");
        power_plant::add_unit(hylen_ps,hylen);
        connect(hps.create_river(15,"fra kvilldal til suldalsvatn", "{}"))
            .input_from(kvilldal1)
            .input_from(kvilldal2)
            .output_to(suldalsvatn);
            
        connect(hps.create_tunnel(16,"hylen-tunnel", "{}"))
            .input_from(suldalsvatn)
            .output_to(hylen);

        auto havet = hps.create_reservoir(17, "havet","{}");

        connect(hps.create_river(18,"utløp hylen","{}"))
            .input_from(hylen)
            .output_to(havet);

        connect(hps.create_river(19,"bypass suldal til havet","{}"))
            .input_from(suldalsvatn, connection_role::bypass)
            .output_to(havet);

        connect(hps.create_river(20,"flom suldal til havet","{}"))
            .input_from(suldalsvatn, connection_role::flood)
            .output_to(havet);
        return sorland;
    }
}

TEST_SUITE("hps") {
TEST_CASE("hydro_power_system functionality") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_hydro_power_system();
    auto b = test::create_hydro_power_system();
    CHECK(a != nullptr);
    CHECK(b != nullptr);
    CHECK(a->equal_structure(*b) == true);
    SUBCASE("serialization") {
        auto b_as_blob = hydro_power_system::to_blob(b);
        CHECK(b_as_blob.size() > 0);
        auto b_from_blob = hydro_power_system::from_blob(b_as_blob);
        CHECK(b_from_blob->equal_structure(*b));
        CHECK(b_from_blob->equal_content(*b));
        CHECK(*b_from_blob == *b);
        // verify difference of equal, equal equal_content and equal_structure
        b->id++;
        CHECK(*b_from_blob!= *b);//because we require exact equality
        CHECK(b_from_blob->equal_content(*b));//still same content, (except .id )
        CHECK(b_from_blob->equal_structure(*b));//still same structure, (except .id )

    }

    a->clear();
    b->clear();
}

TEST_CASE("hydro_power_system_topology") {
    using namespace shyft::energy_market::hydro_power;
    auto a = test::create_hydro_power_system();
    {
        auto blasjo = a->find_reservoir_by_name("blåsjø");
        CHECK(blasjo != nullptr);
        auto blasjo_ds = graph::downstream_units(blasjo);
        auto blasjo_us = graph::upstream_units(blasjo);
        CHECK(blasjo_ds.size() == 1);
        CHECK(blasjo_ds[0]->name == "saurdal");
        CHECK(blasjo_us.size() == 0);
        auto vassbotvatn = a->find_reservoir_by_name("vassbotvatn");
        CHECK(vassbotvatn != nullptr);
        auto kvilldal_ps =a->find_power_plant_by_name ("kvilldal");
        CHECK(kvilldal_ps!=nullptr);
        CHECK(kvilldal_ps->name =="kvilldal");
        CHECK(kvilldal_ps->units.size()==2);
        auto vassbotvatn_us = graph::upstream_units(vassbotvatn);
        auto vassbotvatn_ds = graph::downstream_units(vassbotvatn);
        CHECK(vassbotvatn_us.size() == 1);
        CHECK(vassbotvatn_us[0]->name == "stølsdal pumpe");
        CHECK(vassbotvatn_ds.size() == 0);
        SUBCASE("remove connection to water-route") {
            hydro_component::disconnect(vassbotvatn, vassbotvatn->upstreams[0].target_());
            CHECK(graph::upstream_units(vassbotvatn).size() == 0);
        }
        SUBCASE("power_stations up and down streams") {
            auto saurdal = a->find_unit_by_name ("saurdal");
            CHECK(saurdal != nullptr);
            auto rsv_us = graph::upstream_reservoirs(saurdal);
            CHECK(rsv_us.size() == 1);
            CHECK(rsv_us[0]->name == "blåsjø");

            // NOTE, junction is not yet supported auto rsv_ds = saurdal->downstream_reservoir();
            auto kvilldal = a->find_unit_by_name ("kvilldal_1");
            auto rsv_ds = graph::downstream_reservoirs(kvilldal);
            CHECK(rsv_ds.size() == 1);
            CHECK(rsv_ds[0]->name == "suldalsvatn");
        }
        SUBCASE("find_by_xx") {
            CHECK(a->find_power_plant_by_name ("saurdal")->id == 2000);
            CHECK(a->find_power_plant_by_id (2000)->name=="saurdal");
            CHECK(a->find_unit_by_name ("saurdal")->id == 2);
            CHECK(a->find_unit_by_id (2)->name == "saurdal");
            CHECK(a->find_reservoir_by_name("blåsjø")->id == 1);
            CHECK(a->find_reservoir_by_id(1)->name == "blåsjø");
            CHECK(a->find_waterway_by_name("blåsjø-saurdal")->id == 12);
            CHECK(a->find_waterway_by_id(12)->name == "blåsjø-saurdal");
        }
    }
    a->clear();
    a = nullptr;
}
}
