#include <map>
#include <functional>
#include <string_view>
#include <iostream>
#include <optional>
#include <doctest/doctest.h>

#include <boost/optional/optional_io.hpp>
#include <boost/serialization/optional.hpp>

#include <shyft/mp.h>
#include <shyft/core/core_archive.h>
#include <shyft/core/core_serialization.h>
#include <shyft/energy_market/url_fx.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/a_wrap.h>
#include <test/energy_market/serialize_loop.h>


using shyft::energy_market::proxy_attr;
//------------------------------------------------
//-- DEMO
//-- sample structs to illustrate functionality
//--
namespace shyft::mp {
    //-- example: create web-api doc strings
    //-- so that we auto generate the 'readable doc' from code
    //-- the mechanics is just overloading web_api_doc_strings_impl for your type X:
    namespace detail{
        template<class T>
        constexpr auto web_api_doc_strings_impl = hana::make_tuple();
        #if 0
        #define DOC_STRING(path,docstring) hana::make_pair(BOOST_HANA_STRING(#path),docstring)
        
        template<> // for type X, example of  specialize for user type X
        constexpr auto web_api_doc_strings_impl<X> =
            hana::make_tuple(DOC_STRING(a,"doc a"),
                            DOC_STRING(b,"doc b"),
                            DOC_STRING(c,"doc c"),
                            DOC_STRING(y.d,"doc y.d"),
                            DOC_STRING(y.e,"doc y.e"),
                            DOC_STRING(y.z.f,"doc y.z.f"),
                            DOC_STRING(y.z.g,"doc y.z.g")
                            );
        #undef DOC_STRING
        static_assert(hana::size(web_api_doc_strings_impl<X>) ==
                        hana::size(hana::unique(hana::transform(web_api_doc_strings_impl<X>,hana::first))),
                        "duplicate id");
        #endif
    }

    constexpr auto web_api_doc_strings = [](auto type){
        return detail::web_api_doc_strings_impl<typename std::remove_cvref_t<decltype(type)>::type>;
    };
    
    constexpr auto to_web_doc_string = [](auto leaf_accessor){
        constexpr auto doc_string_map = //classical approach: (1) use compiler to roll out an array/tuple,
        hana::fuse(hana::make_map)(web_api_doc_strings(root_type(leaf_accessor)));
        return doc_string_map[leaf_accessor_id(leaf_accessor)];// (2) then lookup in the table using some id
    }; 
}
namespace test::mp {
    using namespace shyft::mp;

    using shyft::energy_market::url_fx_t;
    using shyft::energy_market::sbi_t;
    using shyft::energy_market::mk_url_fx;
    //-- main type of object struct, with nested def struct, and use struct z
    struct X {
        
        struct Y {
            struct Z {
                url_fx_t url_fx; // required to provide url fx functionality
                BOOST_HANA_DEFINE_STRUCT(Z,
                    (boost::optional<int64_t>,f), // notice that we need to support boost::optional as well (std::optional does not yet work, but is in progress)
                    (std::string,g)
                );
                Z()=default;
                Z(int64_t f, std::string g):f{f},g{g}{}
                x_serialize_decl();
            };
            url_fx_t url_fx;
            BOOST_HANA_DEFINE_STRUCT(Y,
                (int64_t,d),
                (std::string,e),
                (Z,z)
            );
            Y()=default;
            Y(int64_t d, std::string e, Z const &z):d{d},e{e},z{z}{}
            x_serialize_decl();
        };
        BOOST_HANA_DEFINE_STRUCT(X,
            (int64_t,a),
            (std::string,b),
            (int64_t,c),(Y,y)
        );
        //-- methods goes here:
        X(X const&o):a{o.a},b{o.b},c{o.c},y{o.y} {mk_url_fx(this);}
        X(X&&o):a{o.a},b{o.b},c{o.c},y{std::move(o.y)} {mk_url_fx(this);}
        
        X() {
            std::cout<<"X()"<<std::endl;
            mk_url_fx(this); // required to link-up child url_fx to this class
        };
        X(int64_t a, std::string b,int64_t c,Y const &y):a{a},b{b},c{c},y{y}{
            //std::cout<<"X(a,b,c,y)"<<std::endl;
            mk_url_fx(this);// required to link-up child url_fx to this class (or you get bad fx call exception)
        }
        ~X() {
            //std::cout<<"~X()"<<std::endl;
        }
        //- simulate the base-object url method.     
        void generate_url( sbi_t& rbi, int /*levels*/, int /*template_levels*/) const {
            std::copy(b.begin(),b.end(),rbi);
            //std::copy(std::begin(aid),std::end(aid),rbi);
        }
        x_serialize_decl();
    };

    //-- sanity checks
    static_assert(has_accessors(hana::type_c<X>));
    static_assert(has_accessors(hana::type_c<X::Y>));
    static_assert(has_accessors(hana::type_c<X::Y::Z>));
    static_assert(!has_accessors(hana::type_c<int>));
    static_assert(!has_accessors(hana::type_c<std::optional<int>>));
    // for the type 'X' we need a map between the real attributes, and it's attribute-id.
    //
}

// as per standard in the header-file, we do the usual serialization ext-decl:
//--serialization support
x_serialize_export_key(test::mp::X);
x_serialize_export_key(test::mp::X::Y);
x_serialize_export_key(test::mp::X::Y::Z);

namespace shyft::mp::detail {
    using namespace test::mp;
    #define ATTR_ID(path,id) hana::make_pair(BOOST_HANA_STRING(#path),hana::size_c<id>)
    template<>
    constexpr auto attr_ids_impl<X> =
        hana::make_tuple(ATTR_ID(a,0),
                        ATTR_ID(b,1),
                        ATTR_ID(c,2),
                        ATTR_ID(y.d,3),
                        ATTR_ID(y.e,4),
                        //ATTR_ID(z.f,5),
                        //ATTR_ID(z.g,6)
                        //--nested in Y..:
                        ATTR_ID(y.z.f,5),
                        ATTR_ID(y.z.g,6)
                        );

    static_assert(hana::size(attr_ids_impl<X>) == 
                    hana::size(hana::unique(hana::transform(attr_ids_impl<X>,hana::first))),
                    "duplicate path");
    static_assert(hana::size(attr_ids_impl<X>) == 
                    hana::size(hana::unique(hana::transform(attr_ids_impl<X>,hana::second))),
                    "duplicate id");
    #undef ATTR_ID


    // for the type 'X' we can make web-api doc_string map like this
    // .. similar for the python-exposure, except that we in that case would
    // .. prefer to use std python nested exposure
    // .. so rather use overloading of the &X::attr
    //
    #define DOC_STRING(path,docstring) hana::make_pair(BOOST_HANA_STRING(#path),docstring)
    
    template<>
    constexpr auto web_api_doc_strings_impl<X> =
        hana::make_tuple(DOC_STRING(a,"doc a"),
                        DOC_STRING(b,"doc b"),
                        DOC_STRING(c,"doc c"),
                        DOC_STRING(y.d,"doc y.d"),
                        DOC_STRING(y.e,"doc y.e"),
                        DOC_STRING(y.z.f,"doc y.z.f"),
                        DOC_STRING(y.z.g,"doc y.z.g")
                        );
    #undef DOC_STRING
    static_assert(hana::size(web_api_doc_strings_impl<X>) ==
                    hana::size(hana::unique(hana::transform(web_api_doc_strings_impl<X>,hana::first))),
                    "duplicate id");
}

//---   python doc strings ----------
//-- in the python exposure department, 
//-- use the unique type of make_accessor_ptr_<&t::a> to make mappings for members.
namespace py::expose {
    using namespace shyft::mp;
    using test::mp::X;
    #define def_doc(t,a,d) constexpr auto doc_string(make_accessor_ptr_<&t::a>){return d;}
    def_doc(X,a,"py.doc a")
    def_doc(X,b,"py.doc b")
    def_doc(X,c,"py.doc c")
    def_doc(X,y,"py.doc of the y composite member");
    def_doc(X::Y,d,"py.doc of y.d");
    def_doc(X::Y,e,"py.doc of y.e");
    def_doc(X::Y,z,"py.doc of y.z composite member");
    def_doc(X::Y::Z,f, "py.doc of Z.f ");
    def_doc(X::Y::Z,g, "py.doc of Z.g ");
    #undef def_doc  
}

namespace test::mp {
using namespace boost::serialization;
using namespace shyft::core;


// given class T, and Archive
template <class Archive,class T>
void hana_serialize(T& o, Archive& ar, const unsigned int /*version*/) {
    hana::for_each(hana::accessors<T>(),
        [&](auto &&a){
            ar&core_nvp(hana::to<const char*>(hana::first(a)),hana::second(a)(o));
        }
    );
}

template<class Archive>
void X::Y::Z::serialize(Archive &ar,const unsigned int version) {
    hana_serialize(*this,ar,version);
}

template<class Archive>
void X::Y::serialize(Archive &ar,const unsigned int version) {
    hana_serialize(*this,ar,version);
}

template<class Archive>
void X::serialize(Archive &ar,const unsigned int version) {
    hana_serialize(*this,ar,version);
}


}

x_serialize_instantiate_and_register(test::mp::X)
x_serialize_instantiate_and_register(test::mp::X::Y)
x_serialize_instantiate_and_register(test::mp::X::Y::Z)


///-------------------------------------------------------------------------------------


namespace test::mp {
/// T_apply_Fx(T,int aid, Fx) , T==reservoir, Fx==...
    template <class T,class Fx>
    void T_apply_Fx(T& o,int aid,Fx&& f) {
        constexpr auto sz = leaf_accessor_count(hana::type_c<T>);
        sz.times.with_index([&](auto a) {
            if(aid==a())
                f(leaf_access(o,to_leaf_accessor(hana::type_c<T>,a)));
        });
    }
    
    template <typename T>
    constexpr auto  get_attr=[](auto const &c, std::string const &attr_path)->T {
        MPCONSTEXPR auto C=hana::type_c<std::remove_cvref_t<decltype(c)>>;
        constexpr auto sz=leaf_accessor_count(C);
        T r{};
        sz.times.with_index([&r,&attr_path,&c,&C](auto a) {
                 MPCONSTEXPR auto cattr_path=to_leaf_accessor(C,a);
                if(attr_path == leaf_accessor_id_str(cattr_path)) {
                    MPCONSTEXPR auto leaf_type=accessor_ptr_type(leaf_accessor(cattr_path));
                    if constexpr (leaf_type ==  hana::type_c<T>) {
                        r=leaf_access(c,cattr_path);
                    } else
                        throw std::runtime_error("Specified wrong extract type for attribute path :"+ attr_path);
                }
            }
        );
        return r;
    };

}


#include <sstream>
namespace {
template<class T>
std::ostream &operator<< (std::ostream &os,std::optional<T> const&o) {
    if(o.has_value())
        os<<o.value();
    else
        os<<"null";
    return os;
}
}
TEST_SUITE("shyft_mp") {
    using namespace test::mp;

    TEST_CASE("mp_xyz_print"){
        // example to illustrate iterating over a flattened struct attribute map
        // the fact that it compiles, is the first proof, secondly
        // some minor tests here.
        X x{0,"XX",2,{3,"YY",{5,"zz"}}};// example nested struct
        std::vector<std::string> actual;// for simlicity, collect the iteration as strings,
        hana::for_each( leaf_accessors(hana::type_c<X>),// pull out all the leafs for X (note hat we use type_c<X> here
            [&x,&actual](auto a) { // a is a leaf_accessor,e.g. a tuple<membr1..>, path to the attribute.
                std::stringstream o;
                o
                << "aid_str="<<leaf_accessor_id_str(a)<<"," // print out the string, like y.b
                << "aid="<< to_attr_id(a) << ","
                << "val="<< leaf_access(x,a)<<"," // uses the captured instance, and get out the value of x.attribute
                << "web_doc=" << to_web_doc_string(a)
                ;
                actual.push_back(o.str());
            }
        );
        std::vector<std::string> expected{
            "aid_str=a,aid=0,val=0,web_doc=doc a",
            "aid_str=b,aid=1,val=XX,web_doc=doc b",
            "aid_str=c,aid=2,val=2,web_doc=doc c",
            "aid_str=y.d,aid=3,val=3,web_doc=doc y.d",
            "aid_str=y.e,aid=4,val=YY,web_doc=doc y.e",
            "aid_str=y.z.f,aid=5,val= 5,web_doc=doc y.z.f",
            "aid_str=y.z.g,aid=6,val=zz,web_doc=doc y.z.g"            
        };
        FAST_CHECK_EQ(expected,actual);
    }
    TEST_CASE("mp_web_api_as_strings") {
        // Given we take the approach of object-url.<attr_path>
        // where object-url /M<id>/R<id>.<attr_path>
        // step 1: build map<string,leaf_accessor_path>
        //  so that we can do almost:
        // leaf_access(o,a_map[string("y.d")]) -> gives access to the attr o.y.d
        using a_map=std::map<std::string,int>;
        a_map am;
        int c=0;
        hana::for_each(leaf_accessors(hana::type_c<X>),
            [&am,&c](auto  a) {
                //std::cout<<"am["<<std::string(leaf_accessor_id_str(a))<<"]="<<c<<std::endl;
                am[std::string(leaf_accessor_id_str(a))]=c++;
            }
        );
        //constexpr auto x_map=leaf_accessor_map(hana::type_c<X>); // compile-time attr-map for X, sz entries, each an 'attr_path'
        constexpr auto  sz=leaf_accessor_count(hana::type_c<X>);
        FAST_CHECK_EQ(am.size(),sz);
        std::string attr_str("y.d");
        auto y_d=am[attr_str];// now we exchange a string into a runtime int
        FAST_CHECK_EQ(y_d,3); 
        X x{0,"XX",2,{3,"YY",{5,"zz"}}};// example nested struct
        sz.times.with_index([&y_d,&x](auto a) {
            if(int(a()) == y_d) {
                constexpr auto attr_path=to_leaf_accessor(hana::type_c<X>,a);// recall, takes (type_c<T>,intergral_constant)
                auto  a_value= leaf_access(x,attr_path);
                if constexpr (std::is_same_v<decltype(a_value),int>) {
                    FAST_CHECK_EQ(3,a_value);
                }
            }
        });
        // example with directly walk  through searching for attr_path
        std::string path_to_match("y.d");
        hana::for_each(leaf_accessors(hana::type_c<X>),
            [&path_to_match,&x](auto  a) {
                if(path_to_match==leaf_accessor_id_str(a)) {
                    auto x_a_value= leaf_access(x,a);
                    MESSAGE("match"<<path_to_match<<"="<<x_a_value);
                    //std::cout<<"match "<<path_to_match<< "="<<x_a_value<<std::endl;
                }
            }
        );
        // can we now make the function _similar to_ :
        // member_type & get_attr( T&o, string path); T=X, and path=y.d, member_type ?
        // hmmm..not directly, because only  diff is the return type, .. , member_type.
        // ---
        // but if we can modify what we want:
        // member_type & get_attr( T&o, some_expression<T>(string) ) ??
        // int_c<..> get_path(T,string path) 
        // member_type& get_attr( T&o, get_path<T>("y.d"))
        //---------------
        const int yd=3;//  just for fun
        constexpr auto  Xsz=leaf_accessor_count(hana::type_c<X>);
        Xsz.times.with_index([yd,&x](auto a) {
            if( a() == yd ) {
                constexpr auto attr_path=to_leaf_accessor(hana::type_c<X>,a);
                auto x_y_d_value=leaf_access(x,attr_path);
                MESSAGE("hana integral_constant.times.with_index works nice also "<<x_y_d_value);
            }
        });
        Xsz.times.with_index([&path_to_match,&x](auto a) {
            constexpr auto attr_path=to_leaf_accessor(hana::type_c<X>,a);
            if( leaf_accessor_id_str(attr_path) == path_to_match ) {
                auto x_y_d_value=leaf_access(x,attr_path);
                MESSAGE("hana integral_constant.times.with_index works nice also "<<x_y_d_value);
            }
        });
        
        auto x_y_e= test::mp::get_attr<std::string>(x,"y.e");
        FAST_CHECK_EQ(x.y.e,x_y_e);
        
        
    }
    TEST_CASE("mp_web_api_ex_all_attributes") {
        //-- (WEB-API) RUNTIME MAPPING object-ptr attr_id -> member ref./function 
        //-- from runtime attribute id/index to member fn
        //-- typically when we parse web-requests
        //-- we extract object-id (and type), plus attribute id
        //-- and we would like to do 'something' with that
        //-- attribute.
        constexpr auto sz = leaf_accessor_count(hana::type_c<X>);
        for(auto id = 0ul;id < sz;++id) // notice that this time, the attr-id is runtime variable
            sz.times.with_index([id](auto a) { // mp11::mp_with_index(id,fx), helps us from runtime ix -> compile-time integral constant
                if(id==a()) { // with the integral constant of id (the magic from runtime id to integral-constant goes here)
                    const auto acc=to_leaf_accessor(hana::type_c<X>,a);
                    FAST_CHECK_EQ(id,to_attr_id(acc)); // this time, we just test for the expected result
                }
            });
    }
    TEST_CASE("mp_web_api_ex_specific_aid") {

        //-- example, given type X, instance x, and attribute-id 1..:
        //  0  1   2  3  4    5  6 (aid)
        //  a  b   c  d  e    f  g ( .member)
        X x{0,"XX",2,{3,"YY",{5,"zz"}}};// example nested struct
        constexpr auto sz = leaf_accessor_count(hana::type_c<X>);

        auto aid=4ul;// just for the example, aid refs to a string, with orignal content YY
        sz.times.with_index([aid,&x] (auto a_) {
            if(aid!= a_()) return;
            const auto  a=to_leaf_accessor(hana::type_c<X>,a_);
            auto v=leaf_access(x,a);// reading the value (and in this case it's by value)
            if constexpr (std::is_same_v<std::string,decltype(v)>) {//need constexpr,or visit.. to dispatch to specific type here
                FAST_CHECK_EQ(v,"YY");
                leaf_access(x,a)="4"; // demo assignment
            }
        });
        
        FAST_CHECK_EQ(x.y.e,"4");// verify we did change the string
        // need a f_assign that takes type-overloading int,string..
        struct f_assign_42 {
            void operator()(int64_t &v) { v=42;}
            void operator()(std::string &v) {v="42";}
        };

        //-- demo using the T_apply_Fx template
        T_apply_Fx(x,aid,[](auto&& v){ // this time the v is the ref of the attribute, so we can deal with it directly
            auto vv=v; // like reading it
            if constexpr (std::is_same_v<std::string,decltype(vv)>) {// notice, we still have to care about the type of stuff, constexpr,visit etc.
                v="42";
            }
        }
        );
        FAST_CHECK_EQ(x.y.e,"42");// verify again we did change the string
    }
    TEST_CASE("mp_ex_python_doc_strings"){
        // Example where we decorate attributes with doc-strings,
        // suitable for what we do in the python expose
        std::vector<std::string> actual;
        hana::for_each(hana::accessors<X>(),// iterate over the python type
            [&actual](auto a){
                std::stringstream o;
                o << hana::to<const char *>(hana::first(a))<<".doc=" <<py::expose::doc_string(hana::second(a));
                actual.push_back(o.str());
            }
        );
        std::vector<std::string> expected{
            "a.doc=py.doc a",
            "b.doc=py.doc b",
            "c.doc=py.doc c",
            "y.doc=py.doc of the y composite member"
        };
        FAST_CHECK_EQ(actual,expected);
        //for(auto const&d:actual) std::cout<<d<<std::endl;
    }

    TEST_CASE("mp_nested_struct_url") {
        X::Y::Z z{5,"zz"};
        X::Y y{3,"YY",z};
        X x{0,"/X0",2,y};// example nested struct
        std::string s;
        auto rbi=std::back_inserter(s);
        x.generate_url(rbi,-1,-1);
        FAST_CHECK_EQ(s,"/X0");
        s.clear();
        x.y.url_fx(rbi,-1,-1,"");//generate_url(rbi);
        FAST_CHECK_EQ(s,"/X0.y");
        s.clear();
        x.y.z.url_fx(rbi,-1,-1,".w");//generate_url(rbi);
        FAST_CHECK_EQ(s,"/X0.y.z.w");
        s.clear();
        x.y.z.url_fx(rbi,0,-1,"");//
        FAST_CHECK_EQ(s,".y.z");

    }
    TEST_CASE("mp_unit_url_fx") {
        using namespace shyft::energy_market::stm;
        auto s=std::make_shared<stm_hps>(1,"s");
        auto u=std::make_shared<unit>(1,"u","",s);
        std::string b;
        auto  rbi=std::back_inserter(b);
        u->production.constraint.url_fx(rbi,-1,-1,".min");
        CHECK_EQ(b, "/H1/U1.production.constraint.min");
    }
    TEST_CASE("mp_proxy_a_wrap") {
        X::Y::Z z{5,"zz"};
        X::Y y{3,"YY",z};
        X x{0,"/X0",2,y};// example nested struct
        
        auto x_y_d=proxy_attr(x.y,"d",x.y.d);
        FAST_CHECK_EQ(x_y_d,3);
        auto u=x_y_d.url();
        FAST_CHECK_EQ(true,u.size()>0);
        auto x_a=proxy_attr(x,"a",x.a);
        FAST_CHECK_EQ(x_a,0);
        auto u2=x_a.url();
        FAST_CHECK_EQ(u2,"/X0.a");
        auto x_y_z_f=proxy_attr(x.y.z,"f",x.y.z.f);
        FAST_CHECK_EQ(x_y_z_f.a.value(),5);
        FAST_CHECK_EQ(x_y_z_f.exists(),true);
        x_y_z_f.remove();
        FAST_CHECK_EQ(x_y_z_f.exists(),false);
        auto x2=test::serialize_loop(x);
        FAST_CHECK_EQ(x2.y.z.f,x.y.z.f);
        FAST_CHECK_EQ(x2.a,x.a);
    }
}
