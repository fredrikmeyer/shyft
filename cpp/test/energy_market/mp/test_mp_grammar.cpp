#include <doctest/doctest.h>

#include <shyft/mp.h>
#include <shyft/mp/grammar.h>

#include <string>

using std::string;
using std::vector;
using std::abs;

namespace {
    struct Y {
        BOOST_HANA_DEFINE_STRUCT(Y,
            (int, a),
            (int, b),
            (string, name)
        );

        bool operator==(const Y& y) const {
            return a == y.a && b == y.b && name == y.name;
        }

        bool operator!=(const Y& y) const {
            return !operator==(y);
        }
    };

    struct X {
        BOOST_HANA_DEFINE_STRUCT(X,
            (double, f),
            (double, g),
            (string, surname),
            (Y, y)
        );

        bool operator==(const X& x) const {
            double eps = 0.00001;
            return abs(f - x.f) < eps
            && abs(g - x.g) < eps
            && surname == x.surname
            && y == x.y
            ;
        }

        bool operator!=(const X& x) const {
            return !operator==(x);
        }
    };
}

namespace shyft::mp::grammar {

    template<typename Iterator, typename Skipper=qi::ascii::space_type>
    struct string_grammar : public qi::grammar<Iterator, string(), Skipper> {
        string_grammar() : string_grammar<Iterator, Skipper>::base_type(start, "string_parser")
        {
            start = '"' >> content >> '"';
            content = *(~qi::char_("\\\"") | escape_char);
            escape_char = ( "\\" >> qi::char_("\\\"") )
                | (qi::char_("\\") );
            //std::cout << "Are we even in here!!!!\n";
            //start = qi::lexeme['"' >> +(qi::char_ - '"') >> '"'];
            start.name("quoted_string");
            qi::on_error<qi::fail>(start, error_handler(qi::_4, qi::_3, qi::_2));
        }

        qi::rule<Iterator, string()> content;
        qi::rule<Iterator, char()> escape_char;
        qi::rule<Iterator, string(), Skipper> start;
        phx::function<error_handler_> const error_handler = error_handler_{};
    };
    // Here we do some type-rule specializations:
    template<class Iterator, class Skipper>
    qi::rule<Iterator, double(), Skipper> type_rule<double, Iterator, Skipper> = qi::double_;


    template<class Iterator, class Skipper>
    auto type_rule<string, Iterator, Skipper> = string_grammar<Iterator, Skipper>();
        //qi::lexeme['"' >> *(~qi::char_('"')) >> '"'];

    template<class Iterator, class Skipper>
    qi::rule<Iterator, int(), Skipper> type_rule<int, Iterator, Skipper> = qi::int_;

}

TEST_SUITE("mp_grammar")
{
    namespace mp = shyft::mp;
    namespace hana = boost::hana;
    namespace qi = boost::spirit::qi;
    namespace fu = boost::fusion;
    using namespace hana::literals;

    TEST_CASE("type_rules")
    {
        auto int_ = mp::grammar::type_rule<int, const char*, qi::ascii::space_type>;
        int i;
        CHECK(mp::grammar::phrase_parser("123", int_, i));
        CHECK_EQ(i, 123);

        auto double_ = mp::grammar::type_rule<double, const char*, qi::ascii::space_type>;
        double d;
        CHECK(mp::grammar::phrase_parser("1.23", double_, d));
        CHECK_EQ(d, doctest::Approx(1.23));

        /* TODO: Notice the absence of checks for the string type_rule...
         * First of all, qi::grammars, are noncopyable, so the assignment statement such as
         * auto string_ = mp::grammar::type_rule<string, const char*, qi::ascii::space_type> doesn't work
         *
         * Moreover, it seems that if type_rule<string,...> wasn't defined using a parser expression instead of
         * an instance of a qi::grammar, then it goes immediately out of scope and is deleted (for some reason),
         * making the assignment segfault.
         */
        /*
        auto string_ = mp::grammar::type_rule<string, const char*, qi::ascii::space_type>;
        string s;
        CHECK(mp::grammar::phrase_parser("\"a string\"", string_, s));
        CHECK_EQ(s, "a string");
         */
    }

    TEST_CASE("vector_type_rule")
    {
        auto int_list_ = mp::grammar::type_rule<vector<int>, const char*, qi::ascii::space_type>;
        vector<int> il;
        CHECK(mp::grammar::phrase_parser("[1, 2, 3, 4, 5]", int_list_, il));
        CHECK_EQ(il.size(), 5);
        CHECK_EQ(il, vector<int>{1, 2, 3, 4, 5});
    }
    TEST_CASE("attribute_rule")
    {
        constexpr auto attrs = hana::accessors<Y>();
        constexpr auto attr = attrs[0_c];

        // Test for make_parser-function:
        auto r0 = mp::grammar::make_parser<const char*>(attr);
        int v;
        CHECK(mp::grammar::phrase_parser(R"_("a" : 123)_", r0, v));
        CHECK_EQ(v, 123);

        // We make another rule. Verification that qi sequence operator can parse to fusion::tuple:
        auto r1 = mp::grammar::make_parser<const char*>(attrs[1_c]);
        fu::tuple<int, int> t{};
        CHECK(mp::grammar::phrase_parser(
            R"_("a" : 12,
                "b" : 34)_",
            r0 >> ',' >> r1, t
            )
        );
        CHECK_EQ(fu::get<0>(t), 12);
        CHECK_EQ(fu::get<1>(t), 34);

        // We make one last rule, making the parser result equivalent to an instance of Y:
        auto r2 = mp::grammar::make_parser<const char*>(attrs[2_c]);
        string s;
        CHECK(mp::grammar::phrase_parser(R"_("name":"a string")_", r2, s));
        CHECK_EQ(s, "a string");

        fu::tuple<int, int, string> yt;
        CHECK(mp::grammar::phrase_parser(
            R"_("a" : -1,
                "b" : 26,
                "name" : "a_string")_",
            r0 >> ',' >> r1 >> ',' >> r2, yt
            )
        );
        CHECK_EQ(fu::get<0>(yt), -1);
        CHECK_EQ(fu::get<1>(yt), 26);
        CHECK_EQ(fu::get<2>(yt), "a_string");

        // Finally, we want to unpack our result into a Y object:
        Y res;
        hana::for_each(
            hana::zip(attrs, yt), // The sequence
            [&res](auto m) {
                auto acc = m[0_c];
                auto attr_ptr = hana::second(acc);
                auto val = m[1_c];
                attr_ptr(res) = val;
            }
        );
        CHECK_EQ(res, Y{-1, 26, "a_string"});
    }

    TEST_CASE("make_accessor_parser_sequence")
    {
        constexpr auto attrs = hana::accessors<Y>();
        auto parsers = mp::grammar::make_accessor_parser_sequence<const char*>(attrs);

        auto r0 = parsers[0_c];
        int v0;
        CHECK(mp::grammar::phrase_parser("\"a\": 12", r0, v0));
        CHECK_EQ(v0, 12);

        auto r1 = parsers[1_c];
        int v1;
        CHECK(mp::grammar::phrase_parser("\"b\": -36", r1, v1));
        CHECK_EQ(v1, -36);

        auto r2 = parsers[2_c];
        string v2;
        CHECK(mp::grammar::phrase_parser("\"name\" : \"a name\"", r2, v2));
        CHECK_EQ(v2, "a name");
    }

    TEST_CASE("sequence_parser")
    {
        constexpr auto attrs = hana::accessors<Y>();
        auto parsers = mp::grammar::make_accessor_parser_sequence<const char*>(attrs);

        auto seq_p = mp::grammar::make_sequence_parser(parsers);
        fu::tuple<int, int, string> tup;
        CHECK(mp::grammar::phrase_parser(
            R"_("a": 1, "b": 2, "name": "tyr")_", seq_p, tup
            )
        );
        CHECK_EQ(fu::get<0>(tup), 1);
        CHECK_EQ(fu::get<1>(tup), 2);
        CHECK_EQ(fu::get<2>(tup), "tyr");
    }

    TEST_CASE("fusion_tuple")
    {
        constexpr auto y_tuple = mp::grammar::fusion_tuple<Y>;
        CHECK_EQ(y_tuple, hana::type_c<fu::tuple<int, int, string>>);

        constexpr auto x_tuple = mp::grammar::fusion_tuple<X>;
        CHECK_EQ(x_tuple, hana::type_c<fu::tuple<double, double, string, Y>>);
    }

    TEST_CASE("set_from_tuple")
    {
        constexpr auto y_tuple = mp::grammar::fusion_tuple<Y>;
        using y_tup_t = typename decltype(+y_tuple)::type; // == fu::tuple<int, int, string>

        y_tup_t yt{1, 2, "aname"};
        Y y;
        mp::grammar::set_from_tuple(y, yt);
        CHECK_EQ(y, Y{1, 2, "aname"});
    }

    TEST_CASE("struct_parser")
    {
        SUBCASE("Y")
        {
            Y y;
            mp::grammar::hana_struct_grammar<Y, const char*> p;
            CHECK(mp::grammar::phrase_parser(
                R"_({
                    "a" : 17,
                    "b" : 19,
                    "name" : "yson"
                })_", p, y
                ));
            CHECK_EQ(y.a, 17);
            CHECK_EQ(y.b, 19);
            CHECK_EQ(y.name, "yson");
        }

        SUBCASE("X")
        {
            X x;
            mp::grammar::hana_struct_grammar<X, const char*> p;
            CHECK(mp::grammar::phrase_parser(
                R"_({
                    "f" : 3.14,
                    "g" : 2.78,
                    "surname" : "xson",
                    "y" : {
                        "a" : -43,
                        "b" : 96,
                        "name" : "yson"
                    }
                })_", p, x
                ));
            CHECK_EQ(x.f, doctest::Approx(3.14));
            CHECK_EQ(x.g, doctest::Approx(2.78));
            CHECK_EQ(x.surname, "xson");
            CHECK_EQ(x.y.a, -43);
            CHECK_EQ(x.y.b, 96);
            CHECK_EQ(x.y.name, "yson");
        }
    }

    TEST_CASE("get_rule_from_type")
    {
        auto p0 = mp::grammar::get_parser_from_type<const char*>(hana::type_c<double>);
        double d;
        CHECK(mp::grammar::phrase_parser("3.1415", p0, d));
        CHECK_EQ(d, doctest::Approx(3.1415));

        auto p1 = mp::grammar::get_parser_from_type<const char*>(hana::type_c<string>);
        string s;
        CHECK(mp::grammar::phrase_parser("\"a cheeky little string\"", p1, s));
        CHECK_EQ(s, "a cheeky little string");
    }

    TEST_CASE("parser_sequence")
    {
        auto parsers = mp::grammar::get_parsers_from_types<const char*>(
            hana::make_tuple(hana::type_c<double>, hana::type_c<string>)
        );
        double d;
        CHECK(mp::grammar::phrase_parser(
            "0.245", parsers[0_c], d
            ));
        CHECK_EQ(d, doctest::Approx(0.245));

        string s;
        CHECK(mp::grammar::phrase_parser(
            "\"a string\"", parsers[1_c], s
            ));
        CHECK_EQ(s, "a string");
    }

    TEST_CASE("parser_or") {
        auto types = hana::make_tuple(hana::type_c<double>, hana::type_c<string>, hana::type_c<int>);
        auto parsers = mp::grammar::get_parsers_from_types<const char*>( types );

        auto p = mp::grammar::make_or_parser(parsers);

        boost::variant<double, string, int> var;

        CHECK(mp::grammar::phrase_parser("3.14", p, var));
        CHECK_EQ(boost::get<double>(var), doctest::Approx(3.14));

        CHECK(mp::grammar::phrase_parser("\"a string\"", p, var));
        CHECK_EQ(boost::get<string>(var), "a string");
    }

    TEST_CASE("boost_variant_grammar")
    {
        auto p = mp::grammar::boost_variant_grammar<const char*, qi::ascii::space_type,
            double, string, int>();

        boost::variant<int, string, double> var;

        CHECK(mp::grammar::phrase_parser(
            "3.14", p, var
            ));
        CHECK_EQ(boost::get<double>(var), doctest::Approx(3.14));

        CHECK(mp::grammar::phrase_parser(
            "\"a string\"", p, var
            ));
        CHECK_EQ(boost::get<string>(var), "a string");

        CHECK(mp::grammar::phrase_parser(
            "26", p, var
            ));
        CHECK_EQ(boost::get<double>(var), doctest::Approx(26.0));
    }

    TEST_CASE("get_variant_grammar")
    {
        auto types = hana::make_tuple(hana::type_c<double>, hana::type_c<string>);
        auto p = mp::grammar::get_variant_grammar_object_from_types<const char*>(types);

        using VariantType = typename decltype(mp::get_variant_from_type_set(types))::type;
        VariantType var;

        CHECK(mp::grammar::phrase_parser(
            "3.14", p, var
            ));
        CHECK_EQ(boost::get<double>(var), doctest::Approx(3.14));

        CHECK(mp::grammar::phrase_parser(
            "\"a string\"", p, var
            ));
        CHECK_EQ(boost::get<string>(var), "a string");
    }
}
