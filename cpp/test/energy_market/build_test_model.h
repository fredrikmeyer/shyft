#pragma once
#include <shyft/energy_market/market/model.h>
#include <shyft/energy_market/market/model_area.h>
#include <shyft/energy_market/market/power_line.h>
#include <shyft/energy_market/market/power_module.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/catchment.h>

namespace test {
    using namespace shyft::energy_market::market;
	using namespace shyft::energy_market::hydro_power;
    using shyft::core::utctime_now;
    using std::make_unique;
    
    inline model_ build_model() {
        auto m = make_shared<model>(1,"m1","j");
        m->created = shyft::core::calendar().time(2022,1,30,12,10,0);
        model_builder mb(m);
        auto a1 = mb.create_model_area(1,"a1","j");
        auto a2 = mb.create_model_area(2,"a2","j");
        auto a3 = mb.create_model_area(3,"a3","j");


        auto pm1_1 = mb.create_power_module(1,"production.nuclear","j",a1);
		pm1_1->json = "Extra meta data added to this power module.";
        auto pm1_2 = mb.create_power_module(2,"consumption.temperature dependent","j",a1);
        auto pm1_3 = mb.create_power_module(3,"export.russia","j",a1);
        auto pm2_1 = mb.create_power_module(1,"production.hydro","j",a2);
        auto pm2_2 = mb.create_power_module(2,"consumption.temperature dependent","j",a2);
        auto pm3_1 = mb.create_power_module(1,"production.bio","j",a3);
        auto pm3_2 = mb.create_power_module(2,"production.solar","j",a3);
        auto pm3_3 = mb.create_power_module(3,"production.coal.new","j",a3);
        auto pm3_4 = mb.create_power_module(4,"import.denmark","j",a3);
		auto pm3_5 = mb.create_power_module(5, "production.windpark","j",a3);
        auto pl1 = mb.create_power_line( 1,"1_a1-a2","j",a1, a2);
        auto pl2 = mb.create_power_line( 2,"2_a2-a3","j",a2, a3);
        auto pl3 = mb.create_power_line( 3,"3_a2-a1","j",a2, a1);
        return m;
    }

    inline hydro_power_system_ build_hps(string name) {
        auto hpsm = make_shared<hydro_power_system>(name);
        auto hps = make_unique<hydro_power_system_builder>(hpsm);
        auto r1 = hps->create_reservoir(1,"r1", R"(xxxr)");
        auto r2 = hps->create_reservoir(2,"r2", R"({"reservoir_data":[100, 110, 10]})");
        auto r3 = hps->create_reservoir(3,"r3", R"({"reservoir_data":[100, 110, 10]})");
        auto o = hps->create_reservoir(4,"ocean",R"({"reservoir_data":[0, 0.01, 1e100]})");
        auto p1 = hps->create_unit(1,"the one",R"({"power_station_data":"..."})");
        auto p2 = hps->create_unit(2,"the other", R"({"power_station_data":"..."})");
        auto p = hps->create_power_plant(3,"pp","{}");
        power_plant::add_unit(p,p1);
        power_plant::add_unit(p,p2);
        
		auto t1 = hps->create_tunnel(1, "r1-p1", "{}");
		auto g1 = hps->create_gate(1, "2", "{\"3\":\"x\"}");
		waterway::add_gate(t1, g1);
        connect(t1).input_from(r1).output_to(p1);
        connect(hps->create_river(2,"r2-r1 river","")).input_from(r2).output_to(r1);
        connect(hps->create_river(3,"p to ocean", "")).input_from(p1).output_to(o);
        connect(hps->create_tunnel(4,"r3-p2", "")).input_from(r3).output_to(p2);
        connect(hps->create_river(5,"p2 to ocean", "")).input_from(p2).output_to(o);
        connect(hps->create_river(6,"r3-bypass ocean", "")).input_from(r3, connection_role::bypass).output_to(o);
        connect(hps->create_river(7,"r2-flood ocean", "")).input_from(r2, connection_role::flood).output_to(o);

		auto wms1 = hps->create_catchment(1,"9471-U", "{\"info\":\"some jason metadata\"}");
		return hpsm;
    }
}
