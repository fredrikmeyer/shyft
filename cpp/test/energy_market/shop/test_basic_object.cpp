#ifdef SHYFT_WITH_SHOP
#define _CRT_V12_LEGACY_FUNCTIONALITY
#include "fixture.h"
#include <cmath>
#include <vector>
#include <array>

using std::vector;
using std::array;

TEST_SUITE("simple object")
{
    TEST_CASE_FIXTURE(test_fixture, "Adding reservoir by type index and name")
    {
        const auto reservoir_type_index{0};
        //ShopSetTimeResolution(shopSystem_, "2018091000", "2018091100", "hour"); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).
        SUBCASE("Adding reservoir by type index")
        {
            shop_name_str reservoir_name{"reservoir by index"};
            const auto before_count = ShopGetObjectCount(shopSystem_);
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_index, reservoir_name);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, shop_type::reservoir) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(objectName, reservoir_name) == 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
        }

        SUBCASE("Adding reservoir by type name")
        {
            shop_name_str reservoir_name{"reservoir by name"};
            const auto before_count = ShopGetObjectCount(shopSystem_);
            const auto oix = ShopAddObject(shopSystem_, shop_type::reservoir, reservoir_name);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, shop_type::reservoir) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(objectName, reservoir_name) == 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Adding generator by type index and name")
    {
        const auto generator_type_index{2};
        auto generator_type_name=shop_type::generator;
        shop_time_str t_start{"2018091000"},t_end{ "2018091100"};
        ShopSetTimeResolution(shopSystem_, t_start,t_end, shop_time_unit::hour); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).
        SUBCASE("Adding generator by type index")
        {
            shop_name_str generator_name{"generator by index"};
            const auto before_count = ShopGetObjectCount(shopSystem_);
            const auto oix = ShopAddObject(shopSystem_, generator_type_index, generator_name);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, generator_type_name) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(objectName, generator_name) == 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
        }

        SUBCASE("Adding generator by type name")
        {
            shop_name_str generator_name{"generator by name"};
            const auto before_count = ShopGetObjectCount(shopSystem_);
            const auto oix = ShopAddObject(shopSystem_, generator_type_name, generator_name);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, generator_type_name) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(objectName, generator_name) == 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Reservoir object and id")
    {
        const auto reservoir_type_index{0};
        auto reservoir_type_name=shop_type::reservoir;
        shop_attr_str id{"id"};
        //ShopSetTimeResolution(shopSystem_, "2018091000", "2018091100", "hour"); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).
        SUBCASE("Adding reservoir by type index without id")
        {
            const auto before_count = ShopGetObjectCount(shopSystem_);
            shop_name_str reservoir_name{"reservoir by index"};
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_index, reservoir_name);
            CHECK(oix > 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
            const auto oid = ShopGetObjectId(shopSystem_, oix);
            CHECK(oid == -1); // Object _id_ is not set, ShopAddObject returns object _index_!
            const auto attr_id_index = ShopGetAttributeIndex(shopSystem_, oix, id);
            CHECK(attr_id_index == 0); // There is a valid attribute actually
            CHECK(!ShopCheckAttributeToGet(shopSystem_, attr_id_index)); // But it is not allowed to get
            CHECK(!ShopCheckAttributeToSet(shopSystem_, attr_id_index)); // Nor set
            const int attr_id_value = 123;
            CHECK(!ShopSetIntAttribute(shopSystem_, oix, attr_id_index, attr_id_value)); // And trying to set it fails
            int attr_id_value_read = -999;
            CHECK(!ShopGetIntAttribute(shopSystem_, oix, attr_id_index, attr_id_value_read)); // And also trying to get it fails
            const auto oid2 = ShopGetObjectId(shopSystem_, oix);
            CHECK(oid2 == -1); // Nada
        }
        SUBCASE("Adding reservoir by type name with id")
        {
            const auto before_count = ShopGetObjectCount(shopSystem_);
            shop_name_str reservoir_name{"reservoir by index"};
            const auto reservoir_id{123};
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_name, reservoir_name, reservoir_id);
            CHECK(oix > 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
            const auto oid = ShopGetObjectId(shopSystem_, oix);
            CHECK(oid == reservoir_id);
            const auto attr_id_index = ShopGetAttributeIndex(shopSystem_, oix, id);
            CHECK(attr_id_index == 0); // There is a valid attribute actually
            CHECK(!ShopCheckAttributeToGet(shopSystem_, attr_id_index)); // But it is not allowed to get
            CHECK(!ShopCheckAttributeToSet(shopSystem_, attr_id_index)); // Nor set
            int attr_id_value_read = -999;
            CHECK(!ShopGetIntAttribute(shopSystem_, oix, attr_id_index, attr_id_value_read)); // Trying to get it fails
        }
    }

    TEST_CASE("Add object with long name")
    {
        {
            test_fixture fixture; // Scoped fixture, because log file test below needs it
            // Pushing the limits of object names.
            // When crossing the limit the test will crash and burn with access violation!
            // Findings: Reservoirs, plants and gates can mostly be set with 109 characters, although sometimes that also fails,
            // generators 111, junctions 364, junction_gate and creek_intake 367, etc. 
            // Note: This only tests that adding the object works, there may still be issues further down the road that we do not detect here...
            shop_time_str t_start{ "2018091000" }, t_end{ "2018091100" };
            ShopSetTimeResolution(fixture.shopSystem_, t_start, t_end, shop_time_unit::hour); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).
            const char* shop_types[]{
                shop_type::reservoir,
                shop_type::generator,
                shop_type::plant,
                shop_type::gate,
                shop_type::market,
                shop_type::scenario,
                shop_type::junction,
                shop_type::junction_gate,
                shop_type::creek_intake
            };
            for (size_t i = 0; i < 9; ++i) {
                auto a_type = shop_types[i];
                // Works:
                const char a_name[]{ "99 characters long random string jklfjdsfkljdsklf fkdlsjf sdkfjs dlkfsd jflkds fjsdkl f charactersc" };
                CHECK(ShopAddObject(fixture.shopSystem_, a_type, a_name) > 0);
                // Crashes and burns with access violation for reservoirs, plants and gates:
                //const char a_name2[]{ "110 characters long random string jklfjdsfkljdsklf fkdlsjf sdkfjs dlkfsd jflkds fjsdkl f charactersjffdsfdsjdx" };
                //CHECK(ShopAddObject(shopSystem_, a_type, a_name2) > 0);
                // Find limit (but will end with access violation crash):
                //char strbuf[1024]{};
                //for (size_t i = sizeof(strbuf) - 1; i > 0; --i) {
                //	auto a_name2 = &strbuf[i - 1];
                //	*a_name2 = 'a';
                //	CHECK(ShopAddObject(shopSystem_, a_type, (const char*)a_name2) > 0);
                //}
            }
        }
        // While we're testing limits - also test that we can execute a command with a long object value - a file path of PATH_MAX-1 characters + nullterminator (but at most NAME_MAX in each component)!
        {
            auto dir = std::filesystem::current_path().string();
            char fullname[PATH_MAX]{};
            size_t i = 0, j = 0;
            for (; i < dir.size(); ++i)
                fullname[i] = dir[i];
            for (; i < PATH_MAX - 2; ++i, ++j) {
                if (j % NAME_MAX == 0)
                    fullname[i] = std::filesystem::path::preferred_separator;
                else
                    fullname[i] = 'a';
            }
            if (j % NAME_MAX != 0) // else, if at name max but only 1 character left in path, we need to just leave it at that
                fullname[i] = 'a';
            char* objectList[]{ fullname };
            auto filepath = std::filesystem::path(fullname);
            //MESSAGE(filepath);
            REQUIRE(!std::filesystem::exists(filepath));
            {
                test_fixture fixture; // Scoped fixture, because we need to free shop system before file can be deleted
                CHECK(ShopExecuteCommand(fixture.shopSystem_, "log file", 0, nullptr, 1, objectList));
            }
            //CHECK(std::filesystem::remove(filepath)); // OLD: Before Shop API v13.1.1.f the log file were created immediately by the ShopExecuteCommand call
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Adding object failures")
    {
        const auto invalid_type_index{45325423}; // Random invalid value
        char  invalid_type_name[]={"frakjhlrgadfds"}; // Random invalid value
        //ShopSetTimeResolution(shopSystem_, "2018091000", "2018091100", "hour"); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).
        SUBCASE("Add object with invalid type index")
        {
            const auto before_count = ShopGetObjectCount(shopSystem_);
#if 1 // Version 13.2.1.f (2020.09.15) throws exception: Unknown object type index: 45325423

            CHECK_THROWS(ShopAddObject(shopSystem_, invalid_type_index, "invalid by index"));
#else // OLD: ShopAddObject with invalid type index crashes in shop.lib version from 03.10.2018 (still the case in 13.2.1.d from 2020.06.04)
            const auto oix = ShopAddObject(shopSystem_, invalid_type_index, "invalid by index");
            CHECK(oix == -1);
#endif
            CHECK(ShopGetObjectCount(shopSystem_) == before_count);
        }
        SUBCASE("Add object with invalid type name")
        {
            const auto before_count = ShopGetObjectCount(shopSystem_);
#if 1 // Version 13.2.1.d (2020.06.04) throws exception, though a bit misleading: Unknown object type index: -1
      // Version 13.2.1.f (2020.09.15) throws exception: Unknown object type: "frakjhlrgadfds"
            CHECK_THROWS(ShopAddObject(shopSystem_, invalid_type_name, "invalid by name"));
#else
            const auto oix = ShopAddObject(shopSystem_, invalid_type_name, "invalid by name");
            CHECK(oix == -1);
#endif
            CHECK(ShopGetObjectCount(shopSystem_) == before_count);
        }

        SUBCASE("Add object with empty name")
        {
            const auto before_count = ShopGetObjectCount(shopSystem_);
            shop_name_str a_name{ "" };
#if 1 // Version 13.2.1.d (2020.06.04) throws exception
            CHECK_THROWS(ShopAddObject(shopSystem_, invalid_type_name, a_name));
#else
            const auto oix = ShopAddObject(shopSystem_, invalid_type_name, a_name);
            CHECK(oix == -1); // CHECK(oix == -2); // Note: Shop API version 13.1.1.d returns -1, previous versions returned -2
#endif
            CHECK(ShopGetObjectCount(shopSystem_) == before_count);
        }
        SUBCASE("Adding objects with same name")
        {
            shop_name_str reservoir_name{ "reservoir by name" };
            const auto before_count = ShopGetObjectCount(shopSystem_);
            const auto oix = ShopAddObject(shopSystem_, shop_type::reservoir, reservoir_name);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, shop_type::reservoir) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(objectName, reservoir_name) == 0);
            const auto after_count = ShopGetObjectCount(shopSystem_);
            CHECK(after_count == before_count + 1);
#if 1 // Version 13.2.1.d (2020.06.04) throws exception
            CHECK_THROWS(ShopAddObject(shopSystem_, shop_type::reservoir, reservoir_name));
#else
            const auto oix2 = ShopAddObject(shopSystem_, shop_type::reservoir, reservoir_name);
            CHECK(oix2 == -2);
#endif
            const auto after_count2 = ShopGetObjectCount(shopSystem_);
            CHECK(after_count2 == before_count + 1);
        }
        SUBCASE("Adding licensed object")
        {
            // TODO: Build currently moves license file to working directory,
            // so this test must avoid it being found (rename, set ICC_COMMAND_PATH..?)
            // TODO 2: When not license, trying to add junction gate just crashes, while
            // creek_intake returns -1 but object count is incremented? At least it did,
            // but on further testing it just crashes...
            //{
            //	shop_name_str obj_name{ "some object 1" };
            //	const auto before_count = ShopGetObjectCount(shopSystem_);
            //	const auto oix = ShopAddObject(shopSystem_, shop_type::creek_intake, obj_name);
            //	CHECK(oix == -1);
            //	const auto after_count = ShopGetObjectCount(shopSystem_);
            //	CHECK(after_count == before_count);
            //}
            //{
            //	shop_name_str obj_name{ "some object 2" };
            //	const int shop_type_index_creek_intake = 9;
            //	const auto before_count = ShopGetObjectCount(shopSystem_);
            //	const auto oix = ShopAddObject(shopSystem_, shop_type_index_creek_intake, obj_name);
            //	CHECK(oix == -1);
            //	const auto after_count = ShopGetObjectCount(shopSystem_);
            //	CHECK(after_count == before_count);
            //}
        }
    }

    TEST_CASE("ShopInit gives new instance")
    {
        const auto reservoir_type_index{ 0 };
        //auto reservoir_type_name=shop_type::reservoir;
        shop_name_str reservoir_name{ "reservoir test 1" };
        // First run
        ShopSystem* shopSystem = ShopInit();
        CHECK(shopSystem != nullptr);
        const auto initial_count = ShopGetObjectCount(shopSystem);
        const auto oix1 = ShopAddObject(shopSystem, reservoir_type_index, reservoir_name);
        CHECK(oix1 > 0);
        const auto after_first_run_count = ShopGetObjectCount(shopSystem);
        CHECK(after_first_run_count == initial_count + 1);
        CHECK(ShopFree(shopSystem)); // This destructs the instance of ShopSystem
        // Second run - should get new instance of ShopSystem with no traces of first run
        shopSystem = ShopInit();
        CHECK(shopSystem != nullptr);
        const auto before_second_run_count = ShopGetObjectCount(shopSystem);
        CHECK(before_second_run_count == initial_count);
        const auto oix2 = ShopAddObject(shopSystem, reservoir_type_index, reservoir_name);
        CHECK(oix2 > 0);
        const auto after_second_run_count = ShopGetObjectCount(shopSystem);
        CHECK(after_second_run_count == initial_count + 1);
        // Third run - another instance of ShopSystem before freeing the previous, still should have no traces of first run
        ShopSystem* shopSystem2 = ShopInit();
        CHECK(shopSystem2 != nullptr);
        const auto before_third_run_count = ShopGetObjectCount(shopSystem2);
        CHECK(before_third_run_count == initial_count);
        const auto oix3 = ShopAddObject(shopSystem2, reservoir_type_index, reservoir_name);
        CHECK(oix3 > 0);
        const auto after_third_run_count = ShopGetObjectCount(shopSystem2);
        CHECK(after_third_run_count == initial_count + 1);
        CHECK(ShopFree(shopSystem2));
        CHECK(ShopFree(shopSystem));
    }

    TEST_CASE_FIXTURE(test_fixture, "Reservoir with attributes")
    {
        const auto reservoir_type_index{0};
        auto reservoir_type_name=shop_type::reservoir;
        shop_name_str reservoir_name{ "alta" };
        const auto reservoir_id{ 123 };
        //ShopSetTimeResolution(shopSystem_, "2018091000", "2018091100", "hour"); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).

        SUBCASE("Add reservoir by type name with name and id")
        {
            auto typeName = ShopGetObjectTypeName(shopSystem_, reservoir_type_index);
            CHECK(strcmp(typeName, reservoir_type_name) == 0);
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_name, reservoir_name, reservoir_id);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, reservoir_type_name) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(reservoir_name, objectName) == 0);
            int objectId = ShopGetObjectId(shopSystem_, oix);
            CHECK(objectId == reservoir_id);
        }

        SUBCASE("Add reservoir by type index with name")
        {
            auto typeName = ShopGetObjectTypeName(shopSystem_, reservoir_type_index);
            CHECK(strcmp(typeName, reservoir_type_name) == 0);
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_index, reservoir_name);
            CHECK(oix > 0);
            const char* objectTypeName = ShopGetObjectType(shopSystem_, oix);
            CHECK(strcmp(objectTypeName, reservoir_type_name) == 0);
            const char* objectName = ShopGetObjectName(shopSystem_, oix);
            CHECK(strcmp(reservoir_name, objectName) == 0);
        }

        SUBCASE("Set hrl by name for reservoir")
        {
            shop_attr_str hrl_tag{"hrl"};
            const auto hrl{200.};
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_name, reservoir_name);
            const auto attribute_index = ShopGetAttributeIndex(shopSystem_, oix, hrl_tag);
            CHECK(ShopAttributeIsDefault(shopSystem_, oix, attribute_index));
            CHECK(ShopCheckAttributeToSet(shopSystem_, attribute_index));
            CHECK(ShopSetDoubleAttribute(shopSystem_, oix, attribute_index, hrl));
            CHECK(!ShopAttributeIsDefault(shopSystem_, oix, attribute_index));
            double read_hrl;
            ShopGetDoubleAttribute(shopSystem_, oix, attribute_index, read_hrl);
            CHECK(read_hrl == hrl);
        }
        SUBCASE("Set hrl by index for reservoir")
        {
            shop_attr_str hrl_tag{"hrl"};
            const auto hrl{200.};
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_name, reservoir_name);
            int hrl_index = ShopGetAttributeIndex(shopSystem_, oix, hrl_tag);
            CHECK(ShopAttributeIsDefault(shopSystem_, oix, hrl_index));
            CHECK(ShopCheckAttributeToSet(shopSystem_, hrl_index));
            CHECK(ShopSetDoubleAttribute(shopSystem_, oix, hrl_index, hrl));
            CHECK(!ShopAttributeIsDefault(shopSystem_, oix, hrl_index));
            double read_hrl;
            ShopGetDoubleAttribute(shopSystem_, oix, hrl_index, read_hrl);
            CHECK(read_hrl == hrl);
        }

        SUBCASE("Set p_min by name for reservoir")
        {
            shop_attr_str pmin_tag{"p_min"};
            //const auto pmin{200.};
            const auto oix = ShopAddObject(shopSystem_, reservoir_type_name, reservoir_name);
#if 1 // Version 13.2.1.d (2020.06.04) throws exception
            CHECK_THROWS(ShopGetAttributeIndex(shopSystem_, oix, pmin_tag));
#else
            const auto attribute_index = ShopGetAttributeIndex(shopSystem_, oix, pmin_tag);
            CHECK(attribute_index == -1);
#endif
        }

        SUBCASE("Set reservoir curve")
        {
            const auto id = ShopAddObject(shopSystem_, reservoir_type_name, reservoir_name);

            double a[] = { 10, 20, 30 };
            double b[] = { 11, 22, 33 };
            shop_attr_str flow_descr{"flow_descr"};
            const auto attribute_index = ShopGetAttributeIndex(shopSystem_, id, flow_descr);
            CHECK(ShopCheckAttributeToSet(shopSystem_, attribute_index));
            const auto success_set = ShopSetXyAttribute(shopSystem_, id, attribute_index, 123, 3, a, b);

            // dummy dummy, will just try to get the values out again
            int number_of_points{};
            ShopGetXyAttributeNPoints(shopSystem_, id, attribute_index, number_of_points);

            double ref_value {};

            vector<double> abba(number_of_points);
            const auto x = &abba[0];

            vector<double> babba(number_of_points);
            const auto y = &babba[0];

            const auto success_get = ShopGetXyAttribute(shopSystem_, id, attribute_index, ref_value, number_of_points, x, y);

            CHECK(success_set);
            CHECK(success_get);
        }
    }
    TEST_CASE_FIXTURE(test_fixture, "Generator with attributes")
    {
        shop_name_str generator_name{"alta g1"};
        //const auto generator_type_index{2};
        auto generator_type_name=shop_type::generator;
        shop_time_str t_start{"2018091000"},t_end{ "2018091100"};
        ShopSetTimeResolution(shopSystem_, t_start,t_end, shop_time_unit::hour); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).

        SUBCASE("Add generator by type name with attributes")
        {
            shop_attr_str pmin_tag{"p_min"};
            shop_attr_str pmax_tag{"p_max"};
            const auto pmin{2.};
            const auto pmax{200.};
            const auto id = ShopAddObject(shopSystem_, generator_type_name, generator_name);
            const auto pmin_index = ShopGetAttributeIndex(shopSystem_, id, pmin_tag);
            const auto pmax_index = ShopGetAttributeIndex(shopSystem_, id, pmax_tag);
            CHECK(ShopSetDoubleAttribute(shopSystem_, id, pmin_index, pmin));
            CHECK(ShopSetDoubleAttribute(shopSystem_, id, pmax_index, pmax));
            double pmin_read = -DBL_MAX;
            double pmax_read = -DBL_MAX;
            CHECK(ShopGetDoubleAttribute(shopSystem_, id, pmin_index, pmin_read));
            CHECK(pmin == pmin_read);
            CHECK(ShopGetDoubleAttribute(shopSystem_, id, pmax_index, pmax_read));
            CHECK(pmax == pmax_read);
        }
    }
    TEST_CASE_FIXTURE(test_fixture, "Set and get hourly fixed timeseries")
    {
        // Plain hourly time resolution, with hour as the base time unit!
        shop_time_str start_time{ "2018010100" };
        shop_time_str end_time{ "2018010118" };
        constexpr int n_time_steps{ 18 };

        CHECK(ShopSetTimeResolution(shopSystem_, start_time, end_time, shop_time_unit::hour));

        constexpr const auto market_type_index{ 12 };
        shop_name_str market_name{"TheMarket"};
        const auto market_ix = ShopAddObject(shopSystem_, market_type_index,market_name );
        shop_name_str buy_price{"buy_price"};
        const int attr_ix = ShopGetAttributeIndex(shopSystem_, market_ix,buy_price );

        CHECK(!ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shopSystem_, attr_ix));

        constexpr int n_scen = 1;
        constexpr int n_points = 3;
        array<int, n_points> t{ 0, 6, 12 }; // Hour is base unit: 6 is after 6 hours, 12 is after 12 hours!
        array<array<double, n_points>, 1> y{  {{48.1, 40.1, 35.1}}  };
        int* t_ptr = &t[0];
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr;

        CHECK(ShopSetTxyAttribute(shopSystem_, market_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));
        
        CHECK(ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shopSystem_, attr_ix));

        char start_time_get[18];
        int n_scen_get, n_points_get;
        CHECK(ShopGetTxyAttributeDimensions(shopSystem_, market_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
        CHECK(strncmp(start_time_get, start_time, strlen(start_time))==0);
        CHECK(n_scen_get == n_scen);
        CHECK(n_points_get == n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!

        int* t_get = new int[n_time_steps];
        double* y_get[n_scen];
        y_get[0] = new double[n_time_steps];
        double** y_get_pptr = &y_get[0];
        CHECK(ShopGetTxyAttribute(shopSystem_, market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_t = 0, set_i = 0; get_t < n_points_get; ++get_t) {
            if (set_i < n_points - 1) {
                if (get_t >= t[set_i + 1]) {
                    ++set_i;
                }
            }
            CHECK(y_get[0][get_t] == y[0][set_i]);
        }

        delete[] t_get;
        delete[] y_get[0];

#if 0
        // Additional test of re-setting attribute with 0 values: As of Shop API v0.3.0 this leads to ts filled with garbled data (values like -7.8459077161097183e+298)
        CHECK(ShopSetTxyAttribute(shopSystem_, market_ix, attr_ix, start_time, 0, n_scen, nullptr, nullptr));

        CHECK(ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shopSystem_, attr_ix));

        t_get = new int[n_time_steps];
        y_get[0] = new double[n_time_steps];
        y_get_pptr = &y_get[0];
        CHECK(ShopGetTxyAttribute(shopSystem_, market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_t = 0, set_i = 0; get_t < n_points_get; ++get_t) {
            if (set_i < n_points - 1) {
                if (get_t >= t[set_i + 1]) {
                    ++set_i;
                }
            }
            CHECK(y_get[0][get_t] == y[0][set_i]);
        }

        delete[] t_get;
        delete[] y_get[0];

#endif
    }
   TEST_CASE_FIXTURE(test_fixture, "Set and get timeseries with nan")
    {
        // Similar to above, but now with nan values.
        // Documenting current behavior of shop api:
        //  - Accepts std::nan as input.
        //  - Uses the number 1e40 internally to represent nan values,
        //    so an input of std::nan and an input of 1e40 is equivalent.
        //  - Returns the number 1e40 as a representation of nan in output.
        shop_time_str start_time{ "2018010100" };
        shop_time_str end_time{ "2018010118" };
        constexpr int n_time_steps{ 18 };

        CHECK(ShopSetTimeResolution(shopSystem_, start_time, end_time, shop_time_unit::hour));

        constexpr const auto market_type_index{ 12 };
        shop_name_str market_name{"TheMarket"};
        const auto market_ix = ShopAddObject(shopSystem_, market_type_index,market_name );
        shop_name_str buy_price{"buy_price"};
        const int attr_ix = ShopGetAttributeIndex(shopSystem_, market_ix, buy_price );

        CHECK(!ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shopSystem_, attr_ix));

        constexpr int n_scen = 1;
        constexpr int n_points = 4;
        array<int, n_points> t{ 0, 2, 4, 6 };
        array<array<double, n_points>, 1> y_input{  {{1.23, std::nan(""), 1e40, 9.87}}  };
        array<array<double, n_points>, 1> y_output{  {{1.23, 1e40, 1e40, 9.87}}  }; // Note: Where we put nan shop will actually use 1e40 internally.
        int* t_ptr = &t[0];
        double* y_ptr = &y_input[0][0];
        double** y_pptr = &y_ptr;

        CHECK(ShopSetTxyAttribute(shopSystem_, market_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));
        
        CHECK(ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shopSystem_, attr_ix));

        char start_time_get[18];
        int n_scen_get, n_points_get;
        CHECK(ShopGetTxyAttributeDimensions(shopSystem_, market_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
        CHECK(strncmp(start_time_get, start_time, strlen(start_time))==0);
        CHECK(n_scen_get == n_scen);
        CHECK(n_points_get == n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!

        int* t_get = new int[n_time_steps];
        double* y_get[n_scen];
        y_get[0] = new double[n_time_steps];
        double** y_get_pptr = &y_get[0];
        CHECK(ShopGetTxyAttribute(shopSystem_, market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_t = 0, set_i = 0; get_t < n_points_get; ++get_t) {
            if (set_i < n_points - 1) {
                if (get_t >= t[set_i + 1]) {
                    ++set_i;
                }
            }
            CHECK(y_get[0][get_t] == y_output[0][set_i]);
        }

        delete[] t_get;
        delete[] y_get[0];
    }
    TEST_CASE_FIXTURE(test_fixture, "Set and get hourly point timeseries")
    {
        // Still hourly time resolution, but this time with minute as the base time unit!
        shop_time_str start_time{ "2018010100" };
        shop_time_str end_time{ "2018010118" };
        constexpr int n_time_steps{ 18 };

        constexpr int timeres_base_units = 60;  // Resolution is 1 hour with 60 minute as base unit (set below)!
        constexpr int timeres_n_points_set = 1;
        int timeres_t_set[timeres_n_points_set]{ 0 };
        double timeres_y_set[timeres_n_points_set]{ (double)timeres_base_units }; // 60 minutes resolution!
        ShopSetTimeResolution(shopSystem_,
            start_time, end_time, shop_time_unit::minute, // Minute as base unit!
            timeres_n_points_set, &timeres_t_set[0], &timeres_y_set[0]);

        constexpr const auto market_type_index{ 12 };
        shop_name_str market_name{"TheMarket"};
        const auto market_ix = ShopAddObject(shopSystem_, market_type_index,market_name );
        shop_name_str buy_price{"buy_price"};
        const int attr_ix = ShopGetAttributeIndex(shopSystem_, market_ix,buy_price );

        CHECK(!ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToSet(shopSystem_, attr_ix));

        constexpr int n_scen = 1;
        constexpr int n_points = 3;
        array<int, n_points> t{ 0*timeres_base_units, 6*timeres_base_units, 12*timeres_base_units }; // Minute is base unit: 6*60 is after 6 hours, 12*60 is after 12 hours!
        array<array<double, n_points>, 1> y{ {{ 48.1, 40.1, 35.1 }} };
        int* t_ptr = &t[0];
        double* y_ptr = &y[0][0];
        double** y_pptr = &y_ptr;

        CHECK(ShopSetTxyAttribute(shopSystem_, market_ix, attr_ix, start_time, n_points, n_scen, t_ptr, y_pptr));
        
        CHECK(ShopAttributeExists(shopSystem_, market_ix, attr_ix));
        CHECK(ShopCheckAttributeToGet(shopSystem_, attr_ix));

        char start_time_get[18];
        int n_scen_get, n_points_get;
        CHECK(ShopGetTxyAttributeDimensions(shopSystem_, market_ix, attr_ix, start_time_get, n_points_get, n_scen_get));
        CHECK(strncmp(start_time_get, start_time, strlen(start_time))==0);
        CHECK(n_scen_get == n_scen);
        CHECK(n_points_get == n_time_steps); // NB: We get expanded, so one point for each time steps regardless of what we did set!

        int* t_get = new int[n_time_steps];
        double* y_get[n_scen];
        y_get[0] = new double[n_time_steps];
        double** y_get_pptr = &y_get[0];
        CHECK(ShopGetTxyAttribute(shopSystem_, market_ix, attr_ix, n_points_get, n_scen_get, t_get, y_get_pptr));

        // Compare the expanded time series data we got with the compressed break point data we set
        for (int get_t = 0, set_i = 0; get_t < n_points_get; get_t+=timeres_base_units) {
            if (set_i < n_points - 1) {
                if (get_t >= t[set_i + 1]) {
                    ++set_i;
                }
            }
            CHECK(y_get[0][get_t] == y[0][set_i]);
        }
        delete[] t_get;
        delete[] y_get[0];
    }
}
#endif
