#ifdef SHYFT_WITH_SHOP
#include "fixture.h"

TEST_SUITE("basic time resolution")
{
    TEST_CASE_FIXTURE(test_fixture, "No time resolution set")
    {
        char startTimeOutput[18]{}, endTimeOutput[18]{};
        char timeUnitOutput[18]{};
        int timeResNPoints = 10;
        int timeResTOutput[10]{};
        double timeResYOutput[10]{};
        int nPoints = -1;
        CHECK(!GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
        CHECK(!ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(!ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput, timeResNPoints, &timeResTOutput[0], &timeResYOutput[0]));
    }
    TEST_CASE_FIXTURE(test_fixture, "Time resolution static")
    {
        shop_time_str  startTimeInput = "20010101000000000";
        shop_time_str endTimeInput = "20020101000000000";

        // Setting a time resolution of one year, unit as lowercase "hours"
        CHECK(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::hour));
        // Retrieving fixed time resolution from Shop should give us same as our input.
        shop_time_str startTimeOutput, endTimeOutput;
        shop_time_unit_str timeUnitOutput;
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::hour, timeUnitOutput) == 0);
        // Retrieving it as a dynamic resolution (with only one point at start of period) should also work
        int nPoints{-1};
        startTimeOutput[0] = 0;
        CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
        CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
        REQUIRE(nPoints == 1);
        startTimeOutput[0] = 0; endTimeOutput[0] = 0;
        int timeResNPoints{1};
        int timeResTOutput{0};
        double timeResYOutput{0};
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput, timeResNPoints, &timeResTOutput, &timeResYOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::hour, timeUnitOutput) == 0);
        CHECK(timeResTOutput == 0); // First time step
        CHECK(timeResYOutput == 1.0); // One hour resolution

        // Setting the same time period but minute or second as unit
        CHECK(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::minute));
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::minute, timeUnitOutput) == 0);

        CHECK(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::second));
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::second, timeUnitOutput) == 0);

        // Setting any other units should fail, and retrieving
        // should give our previous setting - no change!

#if 0 // OLD: Before 13.2.1.d
        CHECK(!ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::week));
#else // NEW: 13.2.1.d
        CHECK_THROWS(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::week));
#endif
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::second, timeUnitOutput) == 0);

#if 0 // OLD: Before 13.2.1.d
        CHECK(!ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::month));
#else // NEW: 13.2.1.d
        CHECK_THROWS(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::month));
#endif
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::second, timeUnitOutput) == 0);

#if 0 // OLD: Before 13.2.1.d
        CHECK(!ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::year));
#else // NEW: 13.2.1.d
        CHECK_THROWS(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, shop_time_unit::year));
#endif
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp( shop_time_unit::second, timeUnitOutput) == 0);

        shop_time_unit_str timeUnitInput = "HOUR";
#if 0 // OLD: Before 13.2.1.d
        CHECK(!ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, timeUnitInput));
#else // NEW: 13.2.1.d
        CHECK_THROWS(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, timeUnitInput));
#endif
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::second, timeUnitOutput) == 0);

        shop_time_unit_str timeUnitInputX = "!df_dx"; // Something random...
#if 0 // OLD: Before 13.2.1.d
        CHECK(!ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, timeUnitInputX));
#else // NEW: 13.2.1.d
        CHECK_THROWS(ShopSetTimeResolution(shopSystem_, startTimeInput, endTimeInput, timeUnitInputX));
#endif
        CHECK(ShopGetTimeResolution(shopSystem_, startTimeOutput, endTimeOutput, timeUnitOutput));
        CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
        CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
        CHECK(strcmp(shop_time_unit::second, timeUnitOutput) == 0);
    }

    TEST_CASE_FIXTURE(test_fixture, "Time resolution dynamic constant")
    {
        shop_time_str startTimeInput = "20010101000000000";
        shop_time_str endTimeInput   = "20010102000000000";
        auto  timeUnitHour = shop_time_unit::hour;
        auto  timeUnitMinute = shop_time_unit::minute;
        SUBCASE("Constant one hour resolution")
        {
            constexpr int timeResNPointsInput = 1;
            int timeResTInput[timeResNPointsInput]{0};
            double timeResYInput[timeResNPointsInput]{1.};
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitHour,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 24; // 1 hour intervals in 1 day period gives 24 steps
            int nPoints{-1};
            shop_time_str startTimeOutput{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0]=0;
            shop_time_str endTimeOutput;shop_time_unit_str  timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps+1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput)==0);
            CHECK(strcmp(endTimeInput, endTimeOutput)==0);
            CHECK(strcmp(timeUnitHour, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            for (int i = 0; i < timeResNPointsOutput; ++i) {
                CHECK(timeResTOutput[i] == i);
                CHECK(timeResYOutput[i] == timeResYInput[0]);
            }
        }
        SUBCASE("Constant three hour resolution")
        {
            constexpr int timeResNPointsInput = 1;
            int timeResTInput[timeResNPointsInput]{ 0 };
            double timeResYInput[timeResNPointsInput]{ 3. };
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitHour,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 8; // 3 hour interval in 1 day period gives 8 steps
            int nPoints{ -1 };
            shop_time_str  startTimeOutput{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0] = 0;
            shop_time_str endTimeOutput;shop_time_unit_str timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps+1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
            CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
            CHECK(strcmp(timeUnitHour, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            for (int i = 0; i < timeResNPointsOutput; ++i) {
                CHECK(timeResTOutput[i] == i*timeResYInput[0]);
                CHECK(timeResYOutput[i] == timeResYInput[0]);
            }
        }
        SUBCASE("Constant ten hour resolution")
        {
            constexpr int timeResNPointsInput = 1;
            int timeResTInput[timeResNPointsInput]{ 0 };
            double timeResYInput[timeResNPointsInput]{ 10. };
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitHour,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 3; // 10 hour interval in 1 day gives 3 steps (two 10 hour steps and one with the four remaining hours)
            int nPoints{ -1 };
            char startTimeOutput[18]{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0] = 0;
            shop_time_str endTimeOutput;shop_time_unit_str timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps+1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
            CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
            CHECK(strcmp(timeUnitHour, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            CHECK(timeResTOutput[0] == 0);
            CHECK(timeResYOutput[0] == 10.);
            CHECK(timeResTOutput[1] == 4);
            CHECK(timeResYOutput[1] == 10.);
            CHECK(timeResTOutput[2] == 14);
            CHECK(timeResYOutput[2] == 10.);
            CHECK(timeResTOutput[3] == 24);
            CHECK(timeResYOutput[3] == 10.);
        }
        SUBCASE("Constant fifteen minutes resolution")
        {
            constexpr int timeResNPointsInput = 1;
            int timeResTInput[timeResNPointsInput]{ 0 };
            double timeResYInput[timeResNPointsInput]{ 15. };
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitMinute,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 4*24; // 15 minute interval in 1 day period gives 96 steps
            int nPoints{ -1 };
            char startTimeOutput[18]{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0] = 0;
            shop_time_str endTimeOutput;shop_time_unit_str timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps+1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
            CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
            CHECK(strcmp(timeUnitMinute, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            for (int i = 0; i < timeResNPointsOutput; ++i) {
                CHECK(timeResTOutput[i] == i*timeResYInput[0]);
                CHECK(timeResYOutput[i] == timeResYInput[0]);
            }
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Time resolution dynamic changing")
    {
        shop_time_str startTimeInput = "20010101000000000";
        shop_time_str endTimeInput = "20010102000000000";
        auto timeUnitHour = shop_time_unit::hour;
        auto  timeUnitMinute = shop_time_unit::minute;
        SUBCASE("One hour then three hours")
        {
            constexpr int timeResNPointsInput = 2;
            int timeResTInput[timeResNPointsInput]{ 0, 6 };
            double timeResYInput[timeResNPointsInput]{ 1., 3. };
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitHour,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 12; // 1 hour intervals in 6 steps then 3 hour intervals in 18 hours corresponding to 6 steps, gives total 12 steps
            int nPoints{ -1 };
            shop_time_str startTimeOutput{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0] = 0;
            shop_time_str endTimeOutput;shop_time_unit_str timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
            CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
            CHECK(strcmp(timeUnitHour, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            int i = 0;
            for (; i < 6; ++i) {
                CHECK(timeResTOutput[i] == i*timeResYInput[0]);
                CHECK(timeResYOutput[i] == timeResYInput[0]);
            }
            for (; i < timeResNPointsOutput; ++i) {
                CHECK(timeResTOutput[i] == timeResTInput[1]+(i-6)*timeResYInput[1]);
                CHECK(timeResYOutput[i] == timeResYInput[1]);
            }
        }
        SUBCASE("One hour then three hours then six hours")
        {
            constexpr int timeResNPointsInput = 3;
            int timeResTInput[timeResNPointsInput]{ 0, 6, 12 };
            double timeResYInput[timeResNPointsInput]{ 1., 3., 6. };
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitHour,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 10; // 1 hour intervals in 6 steps then 3 hour intervals in 6 hours corresponding to 2 steps, then 6 hour intervals in 12 hours corresponding to 2 steps, gives total 10 steps.
            int nPoints{ -1 };
            shop_time_str startTimeOutput{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0] = 0;
            shop_time_str endTimeOutput;shop_time_unit_str timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
            CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
            CHECK(strcmp(timeUnitHour, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            int i = 0;
            for (; i < 6; ++i) {
                CHECK(timeResTOutput[i] == i*timeResYInput[0]);
                CHECK(timeResYOutput[i] == timeResYInput[0]);
            }
            for (; i < 8; ++i) {
                CHECK(timeResTOutput[i] == timeResTInput[1]+(i-6)*timeResYInput[1]);
                CHECK(timeResYOutput[i] == timeResYInput[1]);
            }
            for (; i < timeResNPointsOutput; ++i) {
                CHECK(timeResTOutput[i] == timeResTInput[2]+(i-8)*timeResYInput[2]);
                CHECK(timeResYOutput[i] == timeResYInput[2]);
            }
        }
        SUBCASE("15 minutes then one hour then 3 hours")
        {
            constexpr int timeResNPointsInput = 3;
            int timeResTInput[timeResNPointsInput]{ 0, 6*60, 12*60 };
            double timeResYInput[timeResNPointsInput]{ 15., 60., 180. };
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTimeInput, endTimeInput, timeUnitMinute,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            //const int expectedTimeSteps2 = calculateTimeSteps(24*60, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 34; // 15 minute intervals in 6 hours (24 steps) then 1 hour intervals in 6 hours, then 3 hour intervals in 12 hours corresponding, gives total 34 steps.
            int nPoints{ -1 };
            char startTimeOutput[18]{};
            CHECK(GetTimeResolutionDimensions(shopSystem_, startTimeOutput, nPoints));
            CHECK(strcmp("", startTimeOutput) == 0); // TODO: Not sure if second argument is input or output? Seems to be unused..
            REQUIRE(nPoints == expectedTimeSteps+1);
            startTimeOutput[0] = 0;
            shop_time_str endTimeOutput;shop_time_unit_str timeUnitOutput;
            constexpr int timeResNPointsOutput = expectedTimeSteps + 1;
            int timeResTOutput[timeResNPointsOutput]{};
            double timeResYOutput[timeResNPointsOutput]{};
            CHECK(ShopGetTimeResolution(shopSystem_,
                startTimeOutput, endTimeOutput, timeUnitOutput,
                timeResNPointsOutput, &timeResTOutput[0], &timeResYOutput[0]));
            CHECK(strcmp(startTimeInput, startTimeOutput) == 0);
            CHECK(strcmp(endTimeInput, endTimeOutput) == 0);
            CHECK(strcmp(timeUnitMinute, timeUnitOutput) == 0);
            CHECK(nPoints == timeResNPointsOutput);
            int i = 0;
            for (; i < 24; ++i) {
                CHECK(timeResTOutput[i] == i*timeResYInput[0]);
                CHECK(timeResYOutput[i] == timeResYInput[0]);
            }
            for (; i < 30; ++i) {
                CHECK(timeResTOutput[i] == timeResTInput[1]+(i-24)*timeResYInput[1]);
                CHECK(timeResYOutput[i] == timeResYInput[1]);
            }
            for (; i < timeResNPointsOutput; ++i) {
                CHECK(timeResTOutput[i] == timeResTInput[2]+(i-30)*timeResYInput[2]);
                CHECK(timeResYOutput[i] == timeResYInput[2]);
            }
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Time index lookups")
    {
        shop_time_str oneYearBeforeStartTime = "20010101000000000";
        shop_time_str startTime = "20010102000000000";
        shop_time_str startTimePlusOneHour = "20010102010000000";
        shop_time_str startTimePlusTenHours = "20010102100000000";
        shop_time_str startTimePlusTenHoursAndThirtyMinutes = "20010102103000000";
        shop_time_str startTimePlusTenHoursAndFiftyNine = "20010102105959999";
        shop_time_str startTimePlusOneDay = "20010103000000000";
        shop_time_str startTimePlusOneDayAndSixHours = "20010103060000000";
        shop_time_str startTimePlusFourDays = "20010106000000000";
        shop_time_str endTime = "20010109000000000"; // 1 week (168 hours) after startTime
        shop_time_str oneDayAfterEndTime = "20010110000000000";
        shop_time_unit_str timeUnit = "hour";
        constexpr int timePeriodUnits=24*7;
        SUBCASE("Fixed resolution")
        {
            // Setting fixed time resolution of one year
            REQUIRE(ShopSetTimeResolution(shopSystem_, startTime, endTime, timeUnit));
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, oneYearBeforeStartTime) == -1);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTime) == 0);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusOneHour) == 1);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusTenHours) == 10);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusTenHoursAndThirtyMinutes) == 10);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusTenHoursAndFiftyNine) == 10);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, endTime) == timePeriodUnits);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, oneDayAfterEndTime) == -2); // Returns -2 in v13.2.1.d; -1 in previous versions.
        }
        SUBCASE("Dynamic resolution")
        {
            constexpr int timeResNPointsInput = 3;
            int timeResTInput[timeResNPointsInput]{ 0, 24, 72 };
            double timeResYInput[timeResNPointsInput]{ 1., 3., 6. };
            //const int expectedTimeSteps = calculateTimeSteps(24, timeResNPointsInput, timeResTInput, timeResYInput);
            constexpr int expectedTimeSteps = 24+48/3+96/6;
            REQUIRE(ShopSetTimeResolution(shopSystem_,
                startTime, endTime, timeUnit,
                timeResNPointsInput, &timeResTInput[0], &timeResYInput[0]));
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, oneYearBeforeStartTime) == -1);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTime) == 0);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusOneHour) == 1);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusTenHours) == 10);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusTenHoursAndThirtyMinutes) == 10);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusTenHoursAndFiftyNine) == 10);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusOneDay) == 24);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusOneDayAndSixHours) == 26); // On day 2 we are running 3 hour steps, so 6 hours is 2 steps in day 2
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, startTimePlusFourDays) == 44); // 24 steps on day one, 16 steps total on day 2 + 3, 4 steps on day 4 = 44
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, endTime) == expectedTimeSteps);
            CHECK(ShopConvertStringToTimeIndex(shopSystem_, oneDayAfterEndTime) == -2); // Returns -2 in v13.2.1.d; -1 in previous versions.
        }
    }


}
#endif
