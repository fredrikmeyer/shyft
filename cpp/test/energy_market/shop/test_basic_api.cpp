//#define _CRT_SECURE_NO_WARNINGS
#ifdef SHYFT_WITH_SHOP
#include "fixture.h"
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using std::size_t;
using std::pair;
using std::string;
using std::vector;
using std::map;
using namespace std::string_literals;

TEST_SUITE("basic api")
{
    TEST_CASE("ShopInit and ShopFree")
    {
        SUBCASE("Init gives valid ShopSystem and free succeeds")
        {
            ShopSystem* ptr = ShopInit(); // This constructs an instance of ShopSystem
            CHECK(ptr != nullptr);
            CHECK(ShopFree(ptr)); // This destructs the instance of ShopSystem
        }
        SUBCASE("Multiple inits gives multiple separate ShopSystem")
        {
            ShopSystem* ptr1 = ShopInit();
            CHECK(ptr1 != nullptr);
            ShopSystem* ptr2 = ShopInit();
            CHECK(ptr2 != nullptr);
            ShopSystem* ptr3 = ShopInit();
            CHECK(ptr3 != nullptr);
            CHECK(ptr3 != ptr2);
            CHECK(ptr2 != ptr1);
            CHECK(ShopFree(ptr1));
            CHECK(ShopFree(ptr2));
            CHECK(ShopFree(ptr3));
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Get version info")
    {
        // Testing ShopGetVersionInfo, new API function with improved string allocation logic.
        // First: nullptr buffer should give us required length.
        char* s1=nullptr;
        int l1=0;
        CHECK(ShopGetVersionInfo(shopSystem_, s1, l1));
        CHECK(l1>0);
        CHECK(s1==nullptr);

        // Sufficiently sized buffer should give us the version string.
        int l2=l1;
        char* s2 = new char[l2];
        CHECK(ShopGetVersionInfo(shopSystem_, s2, l2));
        CHECK(l1==l2);
        CHECK(s2!=nullptr);
        // Print it, e.g. "13.5.5.g Cplex 12.8.0 Gurobi 7.5 OSI/CBC 2.9 2021-06-17"
        MESSAGE(string{s2}); // Doctest 2.4.9 seem to need it as std string!?

        // Insufficiently sized buffer should fail and give the required length.
        int l3=l2-1;
        char* s3 = new char[l3];
        CHECK(!ShopGetVersionInfo(shopSystem_, s3, l3));
        CHECK(l3 == l2);
    }	

    TEST_CASE_FIXTURE(test_fixture, "Number of object and attribute types")
    {
        const auto ExpectedNumberOfObjectTypes = 38; // 38 as of 2022.08.05 (v14.4.1.1); 35 as of 2022.03.25 (v14.3.4.0); 32 as of 2022.02.28 (v14.3.1.1); 31 as of 2022.02.11 (v14.3.0.2); 29 as of 2021.03.16 (v13.5.4.e); 25 as of 2020.06.04 (v13.2.1.d); 23 as of 2019.02.22 (v0.2.2)
        const auto ExpectedNumberOfAttributes = 1113; // 1113 as of 2022.11.22 (v14.5.0.3); 1109 as of 2022.10.23 (v14.5.0.1) 1098 as of 2022.10.19 (v14.4.3.0); 1095 as of 2022.08.05 (v14.4.1.1); 1028 as of 2022.03.25 (v14.3.4.0); 983 as of 2022.03.03 (v14.3.2.0); 974 as of 2022.02.28 (v14.3.1.1); 956 as of 2022.02.11 (v14.3.0.2); 710 as of 2021.06.17 (v13.5.5.g); 709 as of 2021.03.16 (v13.5.4.e); 655 as of 2020.09.15 (v13.2.1.f); 653 as of 2020.06.04 (v13.2.1.d); 635 as of 2020.03.10 (v13.1.2.c); 634 as of 2020.02.05 (v13.1.1.d); 618 as of 2019.06.18 (v0.3.0)
        auto nObjectTypes = ShopGetObjectTypeCount(shopSystem_);
        CHECK(nObjectTypes == ExpectedNumberOfObjectTypes);
        auto nAttributes = ShopGetAttributeCount(shopSystem_);
        CHECK(nAttributes == ExpectedNumberOfAttributes);
    }

    TEST_CASE_FIXTURE(test_fixture, "List all object type names")
    {
        int nObjectTypes = ShopGetObjectTypeCount(shopSystem_);
        for (int i = 0; i < nObjectTypes; ++i) {
            const char* objectTypeName = ShopGetObjectTypeName(shopSystem_, i);
            CHECK(objectTypeName != nullptr);
            MESSAGE(string{objectTypeName}); // Doctest 2.4.9 seem to need it as std string!?
        }
        CHECK(nObjectTypes == 38); // 38 as of 2022.08.05 (v14.4.1.1); 35 as of 2022.03.25 (v14.3.4.0); 32 as of 2022.02.28 (v14.3.1.1); 31 as of 2022.02.11 (v14.3.0.2); 29 as of 2021.03.16 (v13.5.4.e); 25 as of 2020.06.04 (v13.2.1.d); 23 as of 2019.02.22 (v0.2.2)
    }

    TEST_CASE_FIXTURE(test_fixture, "Object type index and name")
    {
        // Testing ShopGetObjectName
        const int  reservoir_type_index = 0;
        const char* objectTypeName = ShopGetObjectTypeName(shopSystem_, reservoir_type_index);
        CHECK(strcmp(objectTypeName, shop_type::reservoir) == 0);
        int objectTypeIndex = ShopGetObjectTypeIndex(shopSystem_, objectTypeName);
        CHECK(reservoir_type_index == objectTypeIndex);
        const int plant_type_index = 1;
        objectTypeName = ShopGetObjectTypeName(shopSystem_, plant_type_index);
        CHECK(strcmp(objectTypeName, shop_type::plant) == 0);
        objectTypeIndex = ShopGetObjectTypeIndex(shopSystem_, objectTypeName);
        CHECK(plant_type_index == objectTypeIndex);
        const int  generator_type_index = 2;
        objectTypeName = ShopGetObjectTypeName(shopSystem_, generator_type_index);
        CHECK(strcmp(objectTypeName, shop_type::generator) == 0);
        objectTypeIndex = ShopGetObjectTypeIndex(shopSystem_, objectTypeName);
        CHECK(generator_type_index == objectTypeIndex);
    }

    TEST_CASE_FIXTURE(test_fixture, "Get attribute info")
    {
        int hrl_attribute_index = 8; // "hrl"
        char reservoir_type_name[] = "reservoir";
        char aname[64]; char afunc[64]; char atype[64]; char xunit[64]; char yunit[64];
        bool aparam, ain, aout;
        ShopGetAttributeInfo(shopSystem_, hrl_attribute_index, reservoir_type_name, aparam, aname, afunc, atype, xunit, yunit, ain, aout);
        CHECK(aparam == false);
        CHECK(ain == true);
        CHECK(aout == false);
        CHECK(strcmp(aname, "hrl") == 0);
        CHECK(strcmp(atype, "double") == 0);
        CHECK(strcmp(xunit, "METER") == 0);
        CHECK(strcmp(yunit, "METER") == 0);
    }

    TEST_CASE_FIXTURE(test_fixture, "Default objects")
    {
        // There are initially one object of type "scenario" with name "S1",
        // and one object of type "objective" with name "average_objective"
        auto nObjects = ShopGetObjectCount(shopSystem_);
        int index = 0;
        CHECK(nObjects == 5); // 5 as of 2022.02.11 (v14.3.0.2); 3 in older versions
        auto typeName = ShopGetObjectType(shopSystem_, index);
        const auto typeNameScenario = "scenario"s;
        CHECK(typeName == typeNameScenario);
        auto objectName = ShopGetObjectName(shopSystem_, index);
        const auto objectNameScenario = "S1"s;
        CHECK(objectName == objectNameScenario);
        ++index;
        typeName = ShopGetObjectType(shopSystem_, index);
        const auto typeNameObjective = "objective"s; // ShopGetObjectTypeName(system, (int)shop_objects::objective);
        CHECK(typeName == typeNameObjective);
        objectName = ShopGetObjectName(shopSystem_, index);
        const auto objectNameObjective = "average_objective"s;
        CHECK(objectName == objectNameObjective);
        ++index;
#if 1
        // NOTE: Shop v14
        typeName = ShopGetObjectType(shopSystem_, index);
        MESSAGE(string{typeName}); // Doctest 2.4.9 seem to need it as std string!?
        const auto typeNameGlobalSettings = "global_settings"s;
        CHECK(typeName == typeNameGlobalSettings);
        objectName = ShopGetObjectName(shopSystem_, index);
        MESSAGE(string{objectName}); // Doctest 2.4.9 seem to need it as std string!?
        const auto objectNameGlobalSettings = "global_settings"s;
        CHECK(objectName == objectNameGlobalSettings);
        ++index;
        typeName = ShopGetObjectType(shopSystem_, index);
        MESSAGE(string{typeName}); // Doctest 2.4.9 seem to need it as std string!?
        const auto typeNameLpModel = "lp_model"s;
        CHECK(typeName == typeNameLpModel);
        objectName = ShopGetObjectName(shopSystem_, index);
        MESSAGE(string{objectName}); // Doctest 2.4.9 seem to need it as std string!?
        const auto objectNameLpModel = "lp_model"s;
        CHECK(objectName == objectNameLpModel);
        ++index;
#endif
        typeName = ShopGetObjectType(shopSystem_, index);
        MESSAGE(string{typeName}); // Doctest 2.4.9 seem to need it as std string!?
        const auto typeNameSystem = "system"s; // ShopGetObjectTypeName(system, (int)shop_objects::objective);
        CHECK(typeName == typeNameSystem);
        objectName = ShopGetObjectName(shopSystem_, index);
        MESSAGE(string{objectName}); // Doctest 2.4.9 seem to need it as std string!?
        const auto objectNameSystem = "system"s;
        CHECK(objectName == objectNameSystem);

        // Then there are API methods for creating these default objects
        //CHECK(ShopAddFirstScenario(shopSystem_)); // NOTE: Removed in Shop v14
        CHECK(ShopAddAverageObjective(shopSystem_));

        // But calling them just results in duplicates, so should not be used?
        nObjects = ShopGetObjectCount(shopSystem_);
        CHECK(nObjects == 6); // 6 as of 2022.02.11 (v14.3.0.2); 5 in older versions

        // Original objects at same indices
        index = 0;
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameScenario);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameScenario);
        ++index;
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameObjective);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameObjective);
        ++index;
#if 1
        // NOTE: Shop v14
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameGlobalSettings);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameGlobalSettings);
        ++index;
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameLpModel);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameLpModel);
        ++index;
#endif
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameSystem);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameSystem);
        ++index;

        // Added duplicate objects at following indices
#if 0
        // NOTE: Not in Shop v14
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameScenario);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameScenario);
        ++index;
#endif
        typeName = ShopGetObjectType(shopSystem_, index);
        CHECK(typeName == typeNameObjective);
        objectName = ShopGetObjectName(shopSystem_, index);
        CHECK(objectName == objectNameObjective);
    }

    TEST_CASE_FIXTURE(test_fixture, "Get object type attribute indices")
    {
        // Testing the changed logic in version 03.10.2018
        const int  reservoir_type_index = 0;
        // If attributeIndexList != NULL and nAttributes < 0:
        // Write all attribute indices into attributeIndexList and the count into nAttributes.
        { // By object type name
            int nAttributes = -1;
            int attributeIndexList[1024]{};
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, shop_type::reservoir, nAttributes, attributeIndexList));
            CHECK(nAttributes > 0);
            CHECK(nAttributes <= 1024);
        }
        { // By object type index
            int nAttributes = -1;
            int attributeIndexList[1024]{};
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, reservoir_type_index, nAttributes, attributeIndexList));
            CHECK(nAttributes > 0);
            CHECK(nAttributes <= 1024);
        }
        // If attributeIndexList != NULL and nAttributes >= 0:
        // Write minimum of nAttributes and total number of attribute indices into attributeIndexList.
        // Write the count into nAttributes.
        { // By object type name
            int nAttributes = 10;
            int attributeIndexList[1024]{};
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, shop_type::reservoir, nAttributes, attributeIndexList));
            CHECK(nAttributes == 10);
        }
        { // By object type index
            int nAttributes = 10;
            int attributeIndexList[1024]{};
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, reservoir_type_index, nAttributes, attributeIndexList));
            CHECK(nAttributes == 10);
        }
        // If attributeIndexList==NULL:
        // Write no attribute indices into, but only the count of into nAttributes.
        { // By object type name
            int nAttributes;
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, shop_type::reservoir, nAttributes, nullptr));
            CHECK(nAttributes == 75); // NB: Hard-coded for current version! 75 as of 2022.12.13 (v14.5.0.3); 72 as of 2022.02.11 (v14.3.0.2); 62 in older versions
        }
        { // By object type index
            int nAttributes;
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, reservoir_type_index, nAttributes, nullptr));
            CHECK(nAttributes == 75); // NB: Hard-coded for current version! 75 as of 2022.12.13 (v14.5.0.3); 72 as of 2022.02.11 (v14.3.0.2); 62 in older versions
        }

#if 0   // NEW: After upgrading to version 14.4.1.1 there is none ("cut" was removed)
        // Additional test with object type that has no attributes, it should still return true,
        // but with number of attributes zero.
        // NB: Hard coded for current version where we know "cut" has no attributes!
        {
            int nAttributes = -1;
            int attributeIndexList[1024]{};
            CHECK(ShopGetObjectTypeAttributeIndices(shopSystem_, shop_type::cut, nAttributes, attributeIndexList));
            CHECK(nAttributes == 0);
        }
#endif

        // Additional test with invalid object type, return value should now be false.
        {
            int nAttributes = -1;
            int attributeIndexList[1024]{};
            shop_type_str not_valid_type{ "fdkafhjdsalfklds"};
            CHECK(!ShopGetObjectTypeAttributeIndices(shopSystem_,not_valid_type, nAttributes, attributeIndexList));
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Methods for getting attribute types"
        * doctest::description("Testing and comparing different methods for retrieving attribute types")
        * doctest::should_fail(true)
        * doctest::skip(true)
    ){
        // Testing if ShopGetObjectTypeAttributeIndices for each object type gives us all the same attributes
        // as looping on all attribute indices from 0 to ShopGetAttributeCount.
        // NO: Conclusion is that ShopGetObjectTypeAttributeIndices gives a precise list of attributes that
        //     are current and relevant for clients! It excludes internal attributes such as id, water_course,
        //     and number attributes describing other array attributes (num_inputs for junction, num_penstock
        //     for plant, etc). This matches the official documentation (SHOP API specification), although that
        //     describes an WCF service interface and not this C/C++ library interface.
        char objectTypeName[64]; char attributeName[64];
        char attributeDataFuncName[1024]; char attributeDatatype[64];
        char xUnit[64]; char yUnit[64];
        bool isObjectParam, isInput, isOutput;
        vector<string> objectTypes;
        map<int,string> attributes;
        auto checkAttributes = [&attributes, &objectTypes](ShopSystem* system){
            INFO("Object type " << objectTypes.back());
            int nAttributeIndices;
            int attributeIndices[1024];
            ShopGetObjectTypeAttributeIndices(system, const_cast<char*>(objectTypes.back().c_str()), nAttributeIndices, attributeIndices);
            size_t nAttributes = attributes.size();
            CHECK_MESSAGE(nAttributes==nAttributeIndices, "Different number of attributes returned by ShopGetObjectTypeAttributeIndices and ShopGetAttributeCount");
            string attributeNames;
            for (const auto& it : attributes) { attributeNames += it.second + " (" + std::to_string(it.first) +  ");"; }
            INFO("All attributes found for object type by looping until ShopGetAttributeCount: " << attributeNames);
            for (int i = 0; i < nAttributeIndices; ++i) {
                int attributeIndex = attributeIndices[i];
                string attributeName = ShopGetAttributeName(system, attributeIndex);
                auto it = std::find_if(begin(attributes), end(attributes), [&attributeName](const pair<int,string>& entry){return entry.second==attributeName;});
                if (it != end(attributes)) {
                    attributes.erase(it);
                } else {
                    FAIL_CHECK("Attribute " << attributeName << " (" << attributeIndex << ") was returned by ShopGetObjectTypeAttributeIndices but not within ShopGetAttributeCount");
                }
            }
            size_t nAttributesRemaining = attributes.size();
            string missingAttributeNames;
            for (const auto& it : attributes) { missingAttributeNames += it.second + " (" + std::to_string(it.first) +  ");"; }
            CHECK_MESSAGE(nAttributesRemaining==0, "" << nAttributesRemaining << " of the attributes was not returned by ShopGetObjectTypeAttributeIndices: " << missingAttributeNames);
        };
        for (int i = 0, n = ShopGetAttributeCount(shopSystem_); i < n; ++i) {
            ShopGetAttributeInfo(shopSystem_, i, objectTypeName, isObjectParam, attributeName, attributeDataFuncName, attributeDatatype, xUnit, yUnit, isInput, isOutput);
            if (objectTypes.size()>0 && objectTypes.back() != objectTypeName) {
                checkAttributes(shopSystem_);
                attributes.clear();
            }
            if (objectTypes.size()==0 || objectTypes.back() != objectTypeName) {
                if (std::find(begin(objectTypes), end(objectTypes), objectTypeName) != end(objectTypes)) {
                    FAIL_CHECK("Object type " << objectTypeName << " found in multiple separate sections of attributes within ShopGetAttributeCount");
                }
                objectTypes.emplace_back(objectTypeName);
            }
            auto it = std::find_if(begin(attributes), end(attributes), [&attributeName](const pair<int,string>& entry){return entry.second==attributeName;});
            if (it != end(attributes)) {
                FAIL_CHECK("Attribute " << attributeName << " with index " << i << " already exists with index " << it->first);
            }
            attributes[i] = attributeName;
        }
        if (attributes.size() > 0) {
            checkAttributes(shopSystem_);
        }
    }
}
#endif
