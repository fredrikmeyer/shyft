#ifdef SHYFT_WITH_SHOP
#include "fixture.h"
#include <shyft/energy_market/stm/shop/api/shop_api.h>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
#include <filesystem>

namespace doctest {
// Must be inlined to avoid multiple definitions since they are fully
// specialized function templates.
template<> inline String toString(const shop_time_str& value) {
    return String(value);
}
}
/** For testing only, create bare-bone external data structures to test the shop c++ proxy api */
namespace shop_test {
using std::vector;
using std::abs;
inline bool approx_equal(const double&a, const double &b) { return abs(a - b) < 1e-6; }
struct xy_p { 
    double x; double y; 
    bool operator==(const xy_p &o) const {
        return approx_equal(x,o.x) && approx_equal(y,o.y);
    }
    bool operator!=(const xy_p&o) const { return !operator==(o); }
};

struct xy {
    double z;
    vector<xy_p> p;
    bool operator==(const xy&o) const {
        return z == o.z && p == o.p;
    }
    bool operator!=(const xy&o) const { return !operator==(o); }
};

struct ty_p {
    int t; double y;
    bool operator==(const ty_p &o) const {
        return t == o.t && approx_equal(y ,o.y);
    }
    bool operator!=(const ty_p&o) const { return !operator==(o); }
};

struct txy {
    time_t start;
    vector<ty_p> p;
    bool operator==(const txy&o) const {
        // this implements semantically equal,(same f(t) given stair-case, and +oo extension of last point)
        if (start != o.start) return false;
        size_t i = 0; size_t j = 0;
        if (p.size() == 0 && o.p.size() == 0)
            return true;// empty ts,->equal
        if (p.size() == 0 || o.p.size() == 0)
            return false; // if size>0, both must define size
        if (p[i] != o.p[i])// first point must start at same time
            return false;
        while (true) {
            if (!approx_equal(p[i].y, o.p[j].y))
                return false;
            if (i + 1 == p.size() && j + 1 == o.p.size())
                return true;// all done
            // consider next point for both lists
            size_t n_i = i + 1 < p.size() ? i + 1 : i;
            size_t n_j = j + 1 < o.p.size() ? j + 1 : j;

            if (i + 1 < p.size()) // possible to advance i
                if ((p[i + 1].t <= o.p[n_j].t) || (n_j + 1 == o.p.size())) i++;// do it if n_j is at end, or p still has more points

            if (j + 1 < o.p.size()) 
                if ((o.p[j + 1].t <= p[n_i].t)||(n_i+1==p.size()))  j++;
        }
    }
    bool operator!=(const txy&o) const { return !operator==(o); }
};
}

/** Inject converters for the shop_test::xy and txy external data-types */
namespace shop::data {
using std::vector;
using xy = shop_test::xy;
using xy_p = shop_test::xy_p;
using txy = shop_test::txy;
using ty_p = shop_test::ty_p;

//-- some utility macros to help doing zip/unzip vector -> point and x,y
template <class E, class Xe, class Ye, class Ux, class Uy>
static vector<E> _zip_dv(std::size_t n, unique_ptr<Xe[]> xv, unique_ptr<Ye[]> yv) {
    std::vector<E> r; r.reserve(n);
    for (std::size_t i = 0; i < n; ++i) r.emplace_back(E{ Ux::to_base(xv[i]), Uy::to_base(yv[i]) });
    return r;
}
template<class V, class C, class Fx>
static unique_ptr<V[]> _xtract_v(const C& c, Fx&& _x) {
    auto r = make_unique<V[]>(c.size());
    transform(begin(c), end(c), r.get(), _x);
    return r;
}

// The xy <-> shop::data::XY adapter
template <class Ux, class Uy>
struct xy_factory<xy,Ux,Uy> {
    static xy create(double z, size_t n, unique_ptr<double[]> xv, unique_ptr<double[]> yv) {
        return { z, _zip_dv<xy_p,double,double,Ux,Uy>(n, move(xv), move(yv)) };
    }
    static XY convert_to_shop(const xy& o) {
        return { o.z,int(o.p.size()), _xtract_v<double>(o.p,[](const xy_p& i) {return Ux::from_base(i.x); }), _xtract_v<double>(o.p,[](const xy_p&i) {return Uy::from_base(i.y);}) };
    }
};

// The txy <-> shop::data::TXY adapter
template <class U>
struct txy_factory<txy, U> {
    static txy create(const std::vector<time_t>& t, std::size_t n, std::unique_ptr<int[]> xv, std::unique_ptr<double[]> yv) {
        return { t[0], _zip_dv<ty_p,int,double,shop::proxy::unit::no_unit,U>(n, move(xv), move(yv)) };
    }
    static TXY convert_to_shop(const txy& o, const std::vector<time_t>&) {
        shop_time start_time(o.start);
        return TXY{ start_time,int(o.p.size()), _xtract_v<int>(o.p,[](const ty_p&i) {return i.t; }),_xtract_v<double>(o.p,[](const ty_p&i) {return U::from_base(i.y);}) };
    }
};

}



TEST_SUITE("shop_proxy") {


    TEST_CASE("shop_time") {
        using shop::data::shop_time;
        using std::string;
        shop_time t1(0L);
        shop_time t2("19700101000000000", true);
        CHECK(time_t(0) == time_t(t1));
        CHECK(time_t(0) == time_t(t2));
        CHECK(t1 == t2);
        shop_time t3(3600 + 60 + 1);
        CHECK(t1 != t3);
        shop_time t4("19700101010101000",true);
        CHECK(t3 == t4);
        char* t3_s = static_cast<char*>(t3);
        char* t3_ss = t3;// implicit cast works
        CHECK(t3_s == t3_ss);
        CHECK_EQ(t3_ss , string("19700101010101000"));
        shop_time t5(-3600L);
        shop_time t6("19691231230000000",true);
        CHECK_EQ(t5, t6);
    }
    TEST_CASE("shop_time_axis_fixed_hour") {
        using shop_time = shop::data::shop_time;
        using api = shop::api<xy, txy>;
        api s{};
        shop_time start_time("20180101000000000", true);
        shop_time end_time("20180101180000000", true);
        time_t t_step = 3600;
        s.set_time_axis(time_t(start_time), time_t(end_time), t_step);
        CHECK(s.time_axis_defined);
        time_t t_begin = (time_t)start_time;
        time_t t_end = (time_t)end_time;
        size_t i = 0;
        for (time_t t = t_begin; t < t_end; t += t_step) {
            CHECK(t == s.time_axis[i++]);
        }
        CHECK(i == 18);
    }
    TEST_CASE("shop_time_axis_fixed_quarter") {
        using shop_time = shop::data::shop_time;
        using api = shop::api<xy, txy>;
        api s{};
        shop_time start_time("20180101000000000", true);
        shop_time end_time("20180101110000000", true);
        time_t t_step = 900;
        s.set_time_axis(time_t(start_time), time_t(end_time), t_step);
        CHECK(s.time_axis_defined);
        time_t t_begin = (time_t)start_time;
        time_t t_end = (time_t)end_time;
        size_t i = 0;
        for (time_t t = t_begin; t < t_end; t += t_step) {
            CHECK(t == s.time_axis[i++]);
        }
        CHECK(i == 44);
    }
    TEST_CASE("shop_time_axis_dynamic") {
        using shop_time = shop::data::shop_time;
        using api = shop::api<xy, txy>;
        api s{};
        shop_time t_keypoints[]{
            { "20180101000000000", true },
            { "20180101010000000", true },
            { "20180101030000000", true },
            { "20180101070000000", true },
            { "20180101190000000", true },
        };
        time_t step_lengths[]{
            900,
            1800,
            3600,
            10800
        };
        size_t n_step_changes = std::extent<decltype(step_lengths)>::value;
        std::vector<time_t> t_axis;
        std::vector<shop_time> t_axis_s;
        time_t t{ 0 };
        for (size_t i = 0; i < n_step_changes; ++i) {
            time_t step{ step_lengths[i] };
            t = time_t(t_keypoints[i]);
            time_t t_next{ t_keypoints[i + 1] };
            for (; t < t_next; t += step) {
                t_axis.push_back(t);
                t_axis_s.emplace_back(t);
            }
        }
        t_axis.push_back(t);
        t_axis_s.emplace_back(t);
        s.set_time_axis(t_axis);
        CHECK(s.time_axis_defined);
        CHECK(t_axis == s.time_axis);
    }
    TEST_CASE("shop_XY") {
        using shop::data::XY;
        using shop::data::xy_factory;
        using shop::proxy::unit::no_unit;
        using shop_test::xy;
        using shop_test::xy_p;
        xy a; 
        a.z = 123.0; 
        a.p.push_back(xy_p{0.0,0.0});
        a.p.push_back(xy_p{1.0,1.0});
        auto a_s = xy_factory<xy, no_unit, no_unit>::convert_to_shop(a);
        auto a_ = xy_factory<xy, no_unit, no_unit>::create(a_s.ref, a_s.n, move(a_s.x), move(a_s.y));
        CHECK(a == a_);

    }
    TEST_CASE("shop_TXY") {
        using shop::data::TXY;
        using shop::data::txy_factory;
        using shop::proxy::unit::no_unit;
        using shop_test::txy;
        using shop_test::ty_p;
        const time_t t_start = 3600;
        const time_t t_step = 3600;
        const size_t n = 13;
        txy a{ t_start,{{0,60.0},{2,64.0},{4,68.0},{6,62.0},{8,60.0},{12,50.0}} };
        std::vector<time_t> t_points;
        for (size_t i = 0; i <= n; ++i) { t_points.push_back(t_start + i * t_step); }
        auto a_s = txy_factory<txy, no_unit>::convert_to_shop(a, t_points);
        auto a_ = txy_factory<txy, no_unit>::create(t_points, a_s.n, move(a_s.t), move(a_s.y));
        CHECK(a == a_);
    }
    TEST_CASE("shop_api_scaling") {
        using std::vector;
        using shop_time = shop::data::shop_time;
        using api = shop::api<xy, txy>;
        using reservoir = shop::reservoir<api>;

        api s{};
        set_shop_library_path_for_test(s);
        shop_time start_time("20180101000000000", true);
        shop_time end_time("20180101180000000", true);
        s.set_time_axis(time_t(start_time), time_t(end_time), 3600);
        auto r1 = s.create<reservoir>("r1");

        // lrl is in m: no scaling (factor=1)
        CHECK(decltype(r1.lrl)::y_unit::is_base);
        CHECK(decltype(r1.lrl)::y_unit::to_base(1.0) == 1.0);
        double vin = decltype(r1.lrl)::y_unit::to_base(87.6);
        r1.lrl = vin;
        double vout = r1.lrl;
        CHECK(vin == vout);

        // max_vol is in MM3: scaling from/to mega (factor=1000000)
        CHECK(decltype(r1.max_vol)::y_unit::is_base == false);
        CHECK(decltype(r1.max_vol)::y_unit::to_base(1.0) == 1000000.0);
        vin = decltype(r1.max_vol)::y_unit::to_base(12.3);
        r1.max_vol = vin;
        vout = r1.max_vol;
        CHECK(vin == vout);
    }
    TEST_CASE("shop_api_mini_sys") {
        using namespace shop::data;
        using std::vector;
        using shop_test::xy;
        using shop_test::xy_p;
        using shop_test::txy;
        using shop_test::ty_p;
        using api = shop::api<xy, txy>;
        using reservoir = shop::reservoir<api>;
        using market = shop::market<api>;
        using gate = shop::gate<api>;
        using power_station = shop::power_plant<api>;
        using aggregate = shop::unit<api>;

        const int mega = 1000000;
        //using power_station=shop_test::pow
        api s{};
        set_shop_library_path_for_test(s);
        shop_time start_time("20180101000000000", true);
        shop_time end_time("20180101180000000", true);
        s.set_time_axis(time_t(start_time), time_t(end_time), 3600);
        auto t0 = static_cast<time_t>(start_time);
        auto m1 = s.create<market>("m1");

        m1.buy_price = txy{ t0,{{0*shop_time_resolution_unit,48.1/mega},{6*shop_time_resolution_unit,40.1/mega},{12*shop_time_resolution_unit,35.1/mega}} };
        m1.sale_price = txy{ t0,{{0*shop_time_resolution_unit,48.0/mega},{6*shop_time_resolution_unit,40.0/mega},{12*shop_time_resolution_unit,35.0/mega}} };
        m1.max_buy = txy{ t0,{{0*shop_time_resolution_unit,10.0*mega},{6*shop_time_resolution_unit,9999.0*mega}} };
        m1.max_sale = txy{ t0,{{0*shop_time_resolution_unit,10.0*mega},{6*shop_time_resolution_unit,9999.0*mega}} };
        m1.load = txy{ t0,{{0*shop_time_resolution_unit,90.0*mega},{1*shop_time_resolution_unit,80.0*mega},{4*shop_time_resolution_unit,60.0*mega},{5*shop_time_resolution_unit,10.0*mega},{6*shop_time_resolution_unit,0.0*mega}} };

        auto r1 = s.create<reservoir>("r1");
        auto f1 = s.create<gate>("flood1");
        auto b1 = s.create<gate>("bypass1");
        auto ps1 = s.create<power_station>("ps1");
        auto a1 = s.create<aggregate>("a1");
        s.connect_plant_generator(ps1.id, a1.id);
        s.connect_reservoir_plant(r1.id, shop::connection::main, ps1.id);
        s.connect_reservoir_gate(r1.id, shop::connection::flood, f1.id);
        s.connect_reservoir_gate(r1.id, shop::connection::bypass, b1.id);
        r1.lrl = 80.0;
        r1.hrl = 100.0;
        r1.max_vol = 16.0*mega;
        r1.vol_head = xy{ 0.0,{{0.0*mega,80.0},{2.0*mega,90.0},{3.0*mega,95.0},{5.0*mega,100.0},{16.0*mega,105.0}} };
        r1.flow_descr = xy{ 0.0,{{100.0,0.0},{101.5,25.0},{103.0,80.0},{104.0,150.0}} };
        r1.start_head = 90.0;
        r1.energy_value_input = 36.5/mega;
        txy r1_inflow{ t0,{{0*shop_time_resolution_unit,60.0},{2*shop_time_resolution_unit,64.0},{4*shop_time_resolution_unit,68.0},{6*shop_time_resolution_unit,62.0},{8*shop_time_resolution_unit,60.0},{12*shop_time_resolution_unit,50.0}} };
        r1.inflow = r1_inflow;
        txy r1_inflow_q = r1.inflow;
        CHECK_EQ(r1_inflow, r1_inflow_q);

        ps1.outlet_line = 10.0;
        ps1.main_loss = vector<double>{ 0.0003 };
        ps1.penstock_loss = vector<double>{ 0.00005 };

        a1.penstock = 1;
        a1.p_min = 20.0*mega;
        a1.p_max = 80.0*mega;
        a1.p_nom = 80.0*mega;
        xy a1_gen_eff{ 0.0,{{20.0*mega,96.0},{40.0*mega,98.0},{60.0*mega,99.0},{80.0*mega,98.0}} };
        a1.gen_eff_curve = a1_gen_eff;
        CHECK_EQ(a1_gen_eff, a1.gen_eff_curve);
        vector<xy> a1_t_eff_curves{ xy{ 70.0,{{20.0,70.0},{40.0,85.0},{60.0,92.0},{80.0,94.0},{100.0,92.0},{110.0,90.0}} } };
        a1.turb_eff_curves = a1_t_eff_curves;
        CHECK_EQ(a1_t_eff_curves, a1.turb_eff_curves.get());// verify we get back what we inserted
        f1.max_discharge = 150.0;
        b1.max_discharge = 1000.0;
        CHECK_UNARY(b1.max_discharge.exists());
        CHECK_UNARY(a1.gen_eff_curve.exists());
        s.execute_cmd("set method", "primal");
        s.execute_cmd("set code", "full");
        s.start_sim("3");
        s.execute_cmd("set code", "incremental");
        s.start_sim("3");
#if 0
        s.execute_cmd("return simres", "", "shop_results.txt");
        s.execute_cmd("return simres", "gen", "shop_results_gen.txt");
        s.execute_cmd("save series", "", "shop_series.txt");
#endif
        txy a1_discharge = a1.discharge;
        txy r1_storage = r1.storage;
        txy r1_head = r1.head;
        CHECK(a1_discharge.p.size() > 0);

        txy a1_discharge_expected{ t0,{
                { 0*shop_time_resolution_unit,109.999999999999985},
                { 1*shop_time_resolution_unit,106.211137839743059},
                { 2*shop_time_resolution_unit,108.030504241042137},
                { 3*shop_time_resolution_unit,109.844023935151682},
                { 4*shop_time_resolution_unit, 90.001245740748956},
                { 5*shop_time_resolution_unit, 34.595414337173963},
                { 6*shop_time_resolution_unit, 64.795773053430536},
                { 7*shop_time_resolution_unit, 64.801543614850288},
                { 8*shop_time_resolution_unit, 64.807332402348407},
                { 9*shop_time_resolution_unit, 80.000000000000000},
                {10*shop_time_resolution_unit, 80.000000000000000},
                {11*shop_time_resolution_unit, 80.000000000000000},
                {12*shop_time_resolution_unit,  0.000000000000000},
                {13*shop_time_resolution_unit,  0.000000000000000},
                {14*shop_time_resolution_unit,  0.000000000000000},
                {15*shop_time_resolution_unit, 64.634098592882608},
                {16*shop_time_resolution_unit, 64.663389702550261},
                {17*shop_time_resolution_unit, 64.692905995253028},
            }
        };
        CHECK_EQ(a1_discharge, a1_discharge_expected);
        txy r1_head_expected{ t0,{
                { 0*shop_time_resolution_unit,90.000000000000000 },
                { 1*shop_time_resolution_unit,89.099999999999994 },
                { 2*shop_time_resolution_unit,88.268199518884628 },
                { 3*shop_time_resolution_unit,87.475650442545870 },
                { 4*shop_time_resolution_unit,86.650458011713141 },
                { 5*shop_time_resolution_unit,86.254435588379649 },
                { 6*shop_time_resolution_unit,86.855718130310521 },
                { 7*shop_time_resolution_unit,86.805394215348769 },
                { 8*shop_time_resolution_unit,86.754966430281470 },
                { 9*shop_time_resolution_unit,86.668434447039203 },
                {10*shop_time_resolution_unit,86.308434447039189 },
                {11*shop_time_resolution_unit,85.948434447039204 },
                {12*shop_time_resolution_unit,85.588434447039190 },
                {13*shop_time_resolution_unit,86.488434447039196 },
                {14*shop_time_resolution_unit,87.388434447039202 },
                {15*shop_time_resolution_unit,88.288434447039193 },
                {16*shop_time_resolution_unit,88.025020672367304 },
                {17*shop_time_resolution_unit,87.761079657721410 },
                {18*shop_time_resolution_unit,87.496607349806851 }
            }
        };
        CHECK_EQ(r1_head, r1_head_expected);
        txy r1_storage_expected{ t0,{
                { 0*shop_time_resolution_unit,2000000.000000000},
                { 1*shop_time_resolution_unit,1820000.000000000},
                { 2*shop_time_resolution_unit,1653639.903776925},
                { 3*shop_time_resolution_unit,1495130.088509173},
                { 4*shop_time_resolution_unit,1330091.602342627},
                { 5*shop_time_resolution_unit,1250887.117675931},
                { 6*shop_time_resolution_unit,1371143.626062104},
                { 7*shop_time_resolution_unit,1361078.843069754},
                { 8*shop_time_resolution_unit,1350993.286056293},
                { 9*shop_time_resolution_unit,1333686.889407839},
                {10*shop_time_resolution_unit,1261686.889407839},
                {11*shop_time_resolution_unit,1189686.889407839},
                {12*shop_time_resolution_unit,1117686.889407839},
                {13*shop_time_resolution_unit,1297686.889407839},
                {14*shop_time_resolution_unit,1477686.889407839},
                {15*shop_time_resolution_unit,1657686.889407839},
                {16*shop_time_resolution_unit,1605004.134473461},
                {17*shop_time_resolution_unit,1552215.931544280},
                {18*shop_time_resolution_unit,1499321.469961369}
            }
        };
        CHECK_EQ(r1_storage, r1_storage_expected);
    }

}
#endif
