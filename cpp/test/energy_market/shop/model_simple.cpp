#include "model_simple.h"
#include <shyft/energy_market/stm/unit_group.h>

//-- using common for the helper functions

    using std::make_shared;
    using std::string;
    using namespace std::string_literals;
    using namespace shyft::energy_market;
    using namespace shyft::energy_market::stm;
    using namespace shyft::energy_market::stm::shop;
    using shyft::core::utctime;
    using shyft::core::utctimespan;
    using shyft::core::to_seconds64;
    using shyft::time_series::ts_point_fx;
    using taxis=shyft::time_axis::point_dt;// for convenience, all time-axis are just time_axis::point_dt.
    using shyft::dtss::ts_vector_t;
    using shyft::dtss::id_vector_t;
    
    namespace { // common constants for functions
        constexpr auto stair_case=ts_point_fx::POINT_AVERAGE_VALUE;
        constexpr int mega = 1000000;
    }

stm_system_ build_simple_model_discharge_group(utctime t_begin, utctime t_end, utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n , bool with_results) {

    int id = 1;
    stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
    auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
    market->price = apoint_ts{
            taxis{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
            { 48.0/mega, 40.0/mega, 35.0/mega, 35.0/mega },
            stair_case};
    market->max_buy = apoint_ts{
            taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
            { 9999.0*mega, 9999.0*mega },
            stair_case};
    market->max_sale = apoint_ts{
            taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
            { 9999.0*mega, 9999.0*mega },
            stair_case};
    //market->load = apoint_ts{
    //        taxis{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
    //        { 90.0*mega, 80.0*mega, 60.0*mega, 10.0*mega, 0.0*mega },
    //        stair_case};
    stm->market.push_back(market);

    stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);

    reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

    // Optional lrl/hrl/maxvol (defaults to storage_description/spill_description limits)
    if (!use_defaults) {
        rsv->level.regulation_min = make_constant_ts(t_begin,t_end,80.0, n);
        rsv->level.regulation_max = make_constant_ts(t_begin,t_end, 100.0, n);
        rsv->volume.static_max = make_constant_ts(t_begin,t_end, 16.0*mega, n);
    }

    rsv->volume_level_mapping = make_shared<map<utctime, xy_point_curve_>>();
    rsv->volume_level_mapping.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
            std::vector<double>{ 0.0*mega, 2.0*mega, 3.0*mega, 5.0*mega, 16.0*mega },
            std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }
                                             )
    );


    rsv->water_value.endpoint_desc = apoint_ts{
            taxis{{ t_begin, t_end - 1 * t_step, t_end }},
            { 0.0, 36.5/mega },
            stair_case};

    rsv->level.realised = apoint_ts{
            taxis{{ t_begin - 1 * t_step, t_end }},
            90.0,
            stair_case};

    rsv->inflow.schedule = apoint_ts{
            taxis{{ t_begin, t_begin + 2 * t_step, t_begin + 4 * t_step, t_begin + 6 * t_step, t_begin + 8 * t_step, t_begin + 12 * t_step, t_end }},
            { 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 },
            stair_case};

    if (with_results) {
        rsv->volume.result = make_constant_ts(t_begin, t_end, 2.0*mega, n);
        rsv->level.result = make_constant_ts(t_begin, t_end, 90.0, n);
    }
    waterway_ wtr_flood = builder.create_river(id++, "waterroute flood river"s, ""s);
    // Flow description is now modelled in gate:
    wtr_flood->discharge.static_max = make_constant_ts(t_begin,t_end, 150.0, n);
    gate_ gt = make_shared<gate>(1, "waterway flood gate", "");
    waterway::add_gate(wtr_flood, gt);
    gt->flow_description = make_shared<map<utctime, xyz_point_curve_list_>>();
    xy_point_curve curve(
            { 100.0, 101.5, 103.0, 104.0 },
            { 0.0, 25.0, 80.0, 150.0 }
    );
    double z = 0.0;
    xy_point_curve_with_z z_curve(curve, z);
    std::vector<xy_point_curve_with_z> z_curves = {z_curve};
    gt->flow_description.get()->emplace(t_begin, make_shared<xyz_point_curve_list>(z_curves));

    waterway_ wtr_tunnel = builder.create_tunnel(id++, "waterroute input tunnel"s, ""s);
    wtr_tunnel->head_loss_coeff = make_constant_ts(t_begin,t_end, 0.00030, n);

    waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    wtr_penstock->head_loss_coeff = make_constant_ts(t_begin,t_end,0.00005, n);

    waterway_ wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    waterway_ wtr_shaft = builder.create_tunnel(id++, "waterroute shaft"s, ""s);
    waterway_ wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    waterway_ wtr_river = builder.create_river(id++, "waterroute river"s, ""s);

    // Create buypass with gate
    waterway_ wtr_bypass = builder.create_river(id++, "waterroute bypass"s, ""s);
    gate_ gtbp = make_shared<gate>(2, "waterway bypass gate", "");
    waterway::add_gate(wtr_bypass, gtbp);

    unit_ gu = builder.create_unit(id++, "aggregate"s, ""s);
    power_plant_ ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, gu);

    ps->outlet_level = make_constant_ts(t_begin,t_end, 10.0, n);

    // Optional p_min/p_max/p_nom (defaults to generator_description limits)
    if (!use_defaults) {
        gu->production.static_min = make_constant_ts(t_begin,t_end, 20.0*mega, n);
        gu->production.static_max = make_constant_ts(t_begin,t_end, 80.0*mega, n);
        gu->production.nominal = make_constant_ts(t_begin,t_end, 80.0*mega, n);
    }

    gu->generator_description = make_shared<map<utctime, xy_point_curve_>>();
    gu->generator_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
            std::vector<double>{ 20.0*mega, 40.0*mega, 60.0*mega, 80.0*mega },
            std::vector<double>{ 96.0, 98.0, 99.0, 98.0 })
    );

    gu->turbine_description = make_shared<map<utctime, turbine_description_>>();
    turbine_description_ td = make_shared<hydro_power::turbine_description>();

    td->efficiencies.push_back(hydro_power::turbine_efficiency{
        {
            {{{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 },
              { 70.0, 85.0, 92.0, 94.0,  92.0,  90.0 }}, 70.0}
        }
    });

    gu->turbine_description.get()->emplace(t_begin, td);
    if (with_results) {
        gu->production.result = apoint_ts{
            taxis{{t_begin, t_begin + 1 * t_step, t_begin + 5 * t_step, t_begin + 12 * t_step, t_end}},
            {72131305.142618537, 70000000.013653448, 19999997.546957157, 0.0},
            stair_case};
        gu->discharge.result = apoint_ts {
            taxis{{t_begin, t_begin + 4*t_step, t_begin + 5*t_step, t_begin + 11*t_step, t_begin + 13*t_step, t_begin + 17*t_step, t_end}},
            {110.0, 85.2149, 89.2149, 80, 0.0, 64.4466},
            stair_case};
    }

    //connect(wtr_flood).input_from(rsv, hydro_power::flood).output_to(wtr_river);
    connect(wtr_flood).input_from(rsv, hydro_power::flood);
    connect(wtr_bypass).input_from(rsv, hydro_power::bypass).output_to(wtr_river);
    connect(wtr_tunnel).input_from(rsv).output_to(wtr_penstock);
    if (always_inlet_tunnels) {
        waterway_ wtr_inlet = builder.create_tunnel(id++, "waterroute inlet"s, ""s);
        connect(wtr_penstock).output_to(wtr_inlet);
        connect(wtr_inlet).output_to(gu);
    } else { // Since we only have one aggregate we have the option to connect the penstock directly to it
        connect(wtr_penstock).output_to(gu);
    }
    connect(gu).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(wtr_shaft);
    connect(wtr_shaft).output_to(wtr_tailrace);
    connect(wtr_tailrace).output_to(wtr_river);

#if 0
    // Additional topology, for manual testing
    reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir2"s, ""s);
    rsv2->storage_description = make_shared<map<utctime, xy_point_curve_>>();
    rsv2->storage_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
        std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

    waterway_ wtr_rsv_rsv2 = builder.create_river(id++, "wtr_rsv_rsv2"s, ""s);
    gate_ wtr_rsv_rsv2_gt1 = builder.create_gate(id++, "wtr_rsv_rsv2_gt1", "");
    gate_ wtr_rsv_rsv2_gt2 = builder.create_gate(id++, "wtr_rsv_rsv2_gt2", "");
    gate_ wtr_rsv_rsv2_gt3 = builder.create_gate(id++, "wtr_rsv_rsv2_gt3", "");
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt1);
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt2);
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt3);
    connect(wtr_rsv_rsv2).input_from(rsv2, hydro_power::main).output_to(rsv);

    waterway_ wtr_bypass = builder.create_river(id++, "waterroute bypass river"s, ""s);
    wtr_bypass->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_bypass->discharge.static_max.get()->emplace(t_begin, 150.0);
    connect(wtr_bypass).input_from(rsv, hydro_power::bypass).output_to(wtr_river);

    waterway_ wtr_flood2 = builder.create_river(id++, "waterroute flood river 2"s, ""s);
    wtr_flood2->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_flood2->discharge.static_max.get()->emplace(t_begin, 150.0);
    connect(wtr_flood2).input_from(rsv2, hydro_power::flood).output_to(rsv);

    waterway_ wtr_bypass2 = builder.create_river(id++, "waterroute bypass river 2"s, ""s);
    wtr_bypass2->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_bypass2->discharge.static_max.get()->emplace(t_begin, 150.0);
    gate_ wtr_bypass2_gt1 = builder.create_gate(id++, "wtr_bypass2_gt1", "");
    gate_ wtr_bypass2_gt2 = builder.create_gate(id++, "wtr_bypass2_gt2", "");
    gate_ wtr_bypass2_gt3 = builder.create_gate(id++, "wtr_bypass2_gt3", "");
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt1);
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt2);
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt3);
    connect(wtr_bypass2).input_from(rsv2, hydro_power::bypass).output_to(rsv);
#endif

    stm->hps.push_back(hps);
    return stm;
}

stm_system_ build_simple_model(utctime t_begin, utctime t_end, utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n , bool with_results) {

    int id = 1;
    stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
    auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
    market->price = apoint_ts{
        taxis{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
        { 48.0/mega, 40.0/mega, 35.0/mega, 35.0/mega },
        stair_case};
    market->max_buy = apoint_ts{
        taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
        { 10.0*mega, 9999.0*mega },
        stair_case};
    market->max_sale = apoint_ts{
        taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
        { 10.0*mega, 9999.0*mega },
        stair_case};
    market->load = apoint_ts{
        taxis{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
        { 90.0*mega, 80.0*mega, 60.0*mega, 10.0*mega, 0.0*mega },
        stair_case};
    stm->market.push_back(market);
    auto ema_ug=stm->add_unit_group(id++,"NO1.units","",unit_group_type::production);
    market->set_unit_group(ema_ug);

    stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);

    reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

    // Optional lrl/hrl/maxvol (defaults to storage_description/spill_description limits)
    if (!use_defaults) {
        rsv->level.regulation_min = make_constant_ts(t_begin,t_end,80.0, n);
        rsv->level.regulation_max = make_constant_ts(t_begin,t_end, 100.0, n);
        rsv->volume.static_max = make_constant_ts(t_begin,t_end, 16.0*mega, n);
    }

    rsv->volume_level_mapping = make_shared<map<utctime, xy_point_curve_>>();
    rsv->volume_level_mapping.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 0.0*mega, 2.0*mega, 3.0*mega, 5.0*mega, 16.0*mega },
        std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }
        )
    );


    rsv->water_value.endpoint_desc = apoint_ts{
        taxis{{ t_begin, t_end - 1 * t_step, t_end }},
        { 0.0, 36.5/mega },
        stair_case};

    rsv->level.realised = apoint_ts{
        taxis{{ t_begin - 1 * t_step, t_end }},
        90.0,
        stair_case};

    rsv->inflow.schedule = apoint_ts{
        taxis{{ t_begin, t_begin + 2 * t_step, t_begin + 4 * t_step, t_begin + 6 * t_step, t_begin + 8 * t_step, t_begin + 12 * t_step, t_end }},
        { 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 },
        stair_case};

    if (with_results) {
        rsv->volume.result = make_constant_ts(t_begin, t_end, 2.0*mega, n);
        rsv->level.result = make_constant_ts(t_begin, t_end, 90.0, n);
    }
    waterway_ wtr_flood = builder.create_river(id++, "waterroute flood river"s, ""s);
    // Flow description is now modelled in gate:
    wtr_flood->discharge.static_max = make_constant_ts(t_begin,t_end, 150.0, n);
    gate_ gt = make_shared<gate>(1, "waterway flood gate", "");
    waterway::add_gate(wtr_flood, gt);
    gt->flow_description = make_shared<map<utctime, xyz_point_curve_list_>>();
    xy_point_curve curve(
         { 100.0, 101.5, 103.0, 104.0 },
         { 0.0, 25.0, 80.0, 150.0 }
        );
    double z = 0.0;
    xy_point_curve_with_z z_curve(curve, z);
    std::vector<xy_point_curve_with_z> z_curves = {z_curve};
    gt->flow_description.get()->emplace(t_begin, make_shared<xyz_point_curve_list>(z_curves));

    waterway_ wtr_tunnel = builder.create_tunnel(id++, "waterroute input tunnel"s, ""s);
    wtr_tunnel->head_loss_coeff = make_constant_ts(t_begin,t_end, 0.00030, n);

    waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    wtr_penstock->head_loss_coeff = make_constant_ts(t_begin,t_end,0.00005, n);

    waterway_ wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    waterway_ wtr_shaft = builder.create_tunnel(id++, "waterroute shaft"s, ""s);
    waterway_ wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    waterway_ wtr_river = builder.create_river(id++, "waterroute river"s, ""s);

    unit_ gu = builder.create_unit(id++, "aggregate"s, ""s);
    power_plant_ ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, gu);
    ema_ug->add_unit(gu,make_constant_ts(t_begin,t_end,1.0,n));// add unit to ema

    ps->outlet_level = make_constant_ts(t_begin,t_end, 10.0, n);

    // Optional p_min/p_max/p_nom (defaults to generator_description limits)
    if (!use_defaults) {
        gu->production.static_min = make_constant_ts(t_begin,t_end, 20.0*mega, n);
        gu->production.static_max = make_constant_ts(t_begin,t_end, 80.0*mega, n);
        gu->production.nominal = make_constant_ts(t_begin,t_end, 80.0*mega, n);
    }

    gu->generator_description = make_shared<map<utctime, xy_point_curve_>>();
    gu->generator_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 20.0*mega, 40.0*mega, 60.0*mega, 80.0*mega },
        std::vector<double>{ 96.0, 98.0, 99.0, 98.0 })
    );

    gu->turbine_description = make_shared<map<utctime, turbine_description_>>();
    turbine_description_ td = make_shared<hydro_power::turbine_description>();

    td->efficiencies.push_back(hydro_power::turbine_efficiency{
        {
            {{{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 },
              { 70.0, 85.0, 92.0, 94.0,  92.0,  90.0 }}, 70.0}
        }
    });

    gu->turbine_description.get()->emplace(t_begin, td);
    if (with_results) {
        gu->production.result = apoint_ts{
            taxis{{t_begin, t_begin + 1*t_step, t_begin + 5*t_step, t_begin + 12*t_step, t_end}},
            {7.23905e+07, 70000000.013653448, 19999997.546957157, 0.0},
            stair_case};
        gu->discharge.result = apoint_ts {
            taxis{{t_begin, t_begin + 4*t_step, t_begin + 5*t_step, t_begin + 11*t_step, t_begin + 13*t_step, t_begin + 17*t_step, t_end}},
            {110.0, 85.1225, 34.5649, 64.9223, 0.0, 64.4833},
            stair_case};
    }
    connect(wtr_flood).input_from(rsv, hydro_power::flood).output_to(wtr_river);
    connect(wtr_tunnel).input_from(rsv).output_to(wtr_penstock);
    if (always_inlet_tunnels) {
        waterway_ wtr_inlet = builder.create_tunnel(id++, "waterroute inlet"s, ""s);
        connect(wtr_penstock).output_to(wtr_inlet);
        connect(wtr_inlet).output_to(gu);
    } else { // Since we only have one aggregate we have the option to connect the penstock directly to it
        connect(wtr_penstock).output_to(gu);
    }
    connect(gu).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(wtr_shaft);
    connect(wtr_shaft).output_to(wtr_tailrace);
    connect(wtr_tailrace).output_to(wtr_river);

#if 0
    // Additional topology, for manual testing
    reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir2"s, ""s);
    rsv2->storage_description = make_shared<map<utctime, xy_point_curve_>>();
    rsv2->storage_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
        std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

    waterway_ wtr_rsv_rsv2 = builder.create_river(id++, "wtr_rsv_rsv2"s, ""s);
    gate_ wtr_rsv_rsv2_gt1 = builder.create_gate(id++, "wtr_rsv_rsv2_gt1", "");
    gate_ wtr_rsv_rsv2_gt2 = builder.create_gate(id++, "wtr_rsv_rsv2_gt2", "");
    gate_ wtr_rsv_rsv2_gt3 = builder.create_gate(id++, "wtr_rsv_rsv2_gt3", "");
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt1);
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt2);
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt3);
    connect(wtr_rsv_rsv2).input_from(rsv2, hydro_power::main).output_to(rsv);

    waterway_ wtr_bypass = builder.create_river(id++, "waterroute bypass river"s, ""s);
    wtr_bypass->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_bypass->discharge.static_max.get()->emplace(t_begin, 150.0);
    connect(wtr_bypass).input_from(rsv, hydro_power::bypass).output_to(wtr_river);

    waterway_ wtr_flood2 = builder.create_river(id++, "waterroute flood river 2"s, ""s);
    wtr_flood2->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_flood2->discharge.static_max.get()->emplace(t_begin, 150.0);
    connect(wtr_flood2).input_from(rsv2, hydro_power::flood).output_to(rsv);

    waterway_ wtr_bypass2 = builder.create_river(id++, "waterroute bypass river 2"s, ""s);
    wtr_bypass2->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_bypass2->discharge.static_max.get()->emplace(t_begin, 150.0);
    gate_ wtr_bypass2_gt1 = builder.create_gate(id++, "wtr_bypass2_gt1", "");
    gate_ wtr_bypass2_gt2 = builder.create_gate(id++, "wtr_bypass2_gt2", "");
    gate_ wtr_bypass2_gt3 = builder.create_gate(id++, "wtr_bypass2_gt3", "");
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt1);
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt2);
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt3);
    connect(wtr_bypass2).input_from(rsv2, hydro_power::bypass).output_to(rsv);
#endif

    stm->hps.push_back(hps);
    return stm;
}

stm_system_ build_simple_model_n_units_with_reserves(utctime t_begin, utctime t_end, utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n ,size_t n_units) {
    int id = 10;
    stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
    auto ema_ug=stm->add_unit_group(id++,"NO1.units","",unit_group_type::production);// unit group for e market area
    /* build market */ { auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
        market->price = apoint_ts{
            taxis{{ t_begin, t_begin + 6 * t_step, t_begin+7*t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
            { 48.0/mega, 40.0/mega, 30.0/mega,48.0/mega, 35.0/mega },
            stair_case};
        market->max_buy = apoint_ts{
            taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
            { 10.0*mega, 220.0*mega },
            stair_case};
        market->max_sale = apoint_ts{
            taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
            { 10.0*mega, 220.0*mega },
            stair_case};
        market->load = apoint_ts{
            taxis{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
            { 90.0*mega, 80.0*mega, 60.0*mega, 10.0*mega, 0.0*mega },
            stair_case};
        market->set_unit_group(ema_ug);
        stm->market.push_back(market);
    }
    /* build operational reserve groups with reserves 
     * We build two groups for the first part of  the period,
     * then one group for the last period of the period.
     */
        auto t_end_reserve=t_end - 4*t_step;
        apoint_ts reserve_mask(taxis{{t_begin,t_end_reserve,t_end}},{1.0,0.0},stair_case);
        auto fcr_n_up=stm->add_unit_group(id++,"fcr_n_up","");
        // assemble the unit group constaint, .limit is the value to reach, .flag is active/not-active, .cost = penalty cost for breaking ..
        fcr_n_up->group_type = stm::unit_group_type::fcr_n_up;// just for now
        fcr_n_up->obligation.schedule = 2.0*mega*reserve_mask;//make_constant_ts(t_begin,t_end,2.0*mega,n);
        //fcr_n_up->constraint.flag =  reserve_mask;
        // .cost .. would then most likely a result time-series accumulated for the group?
        fcr_n_up->obligation.cost =  make_constant_ts(t_begin,t_end,12000.0/mega,n); // 1200 Money/W
        
        auto afrr_up=stm->add_unit_group(id++,"afrr_up","");
        // assemble the unit group constaint, .limit is the value to reach, .flag is active/not-active, .cost = penalty cost for breaking ..
        afrr_up->group_type = stm::unit_group_type::afrr_up;// just for now
        afrr_up->obligation.schedule = 10.0*mega*reserve_mask;//make_constant_ts(t_begin,t_end,20.0*mega,n);
        //afrr_up->obligation.flag =  reserve_mask;
        // .cost .. would then most likely a result time-series accumulated for the group?
        afrr_up->obligation.cost =  make_constant_ts(t_begin,t_end,12000.0/mega,n); // 1200 Money/W
        
        auto reserve_mask2=1.0-reserve_mask;
        auto afrr_up2=stm->add_unit_group(id++,"afrr_up2","");
        afrr_up2->group_type = stm::unit_group_type::afrr_up;// just for now
        afrr_up2->obligation.schedule = 10.0*mega*reserve_mask2;//make_constant_ts(t_begin,t_end,20.0*mega,n);
        //afrr_up2->obligation.flag =  reserve_mask2;
        // .cost .. would then most likely a result time-series accumulated for the group?
        afrr_up2->obligation.cost =  make_constant_ts(t_begin,t_end,1200.0/mega,n); // 1200 Money/W

    /* build the detailed hydro-power-system */
    stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);

    reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);
    // Optional lrl/hrl/maxvol (defaults to storage_description/spill_description limits)
    if (!use_defaults) {
        rsv->level.regulation_min = make_constant_ts(t_begin,t_end,80.0, n);
        rsv->level.regulation_max = make_constant_ts(t_begin,t_end, 100.0, n);
        rsv->volume.static_max = make_constant_ts(t_begin,t_end, 160.0*mega, n);
    }

    rsv->volume_level_mapping = make_shared<map<utctime, xy_point_curve_>>();
    rsv->volume_level_mapping.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 0.0*mega, 20.0*mega, 30.0*mega, 50.0*mega, 160.0*mega },
        std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }
        )
    );


    rsv->water_value.endpoint_desc = apoint_ts{taxis{{ t_begin, t_end - 1 * t_step, t_end }},{ 0.0, 36.5/mega },stair_case};
    rsv->level.realised = apoint_ts{taxis{{ t_begin - 1 * t_step, t_end }},100.0,stair_case};

    rsv->inflow.schedule = apoint_ts{
        taxis{{ t_begin, t_begin + 2 * t_step, t_begin + 4 * t_step, t_begin + 6 * t_step, t_begin + 8 * t_step, t_begin + 12 * t_step, t_end }},
        { 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 },
        stair_case};

    waterway_ wtr_flood = builder.create_river(id++, "flood_river"s, ""s);
    // Flow description is now modelled in gate:
    wtr_flood->discharge.static_max = make_constant_ts(t_begin,t_end, 150.0, n);
    gate_ gt = make_shared<gate>(1, "flood_gate", "");
    waterway::add_gate(wtr_flood, gt);
    gt->flow_description = make_shared<map<utctime, xyz_point_curve_list_>>();
    xy_point_curve curve(
        std::vector<double> { 100.0, 101.5, 103.0, 104.0 },
        std::vector<double> { 0.0, 25.0, 80.0, 150.0 }
        );
    double z = 0.0;
    xy_point_curve_with_z z_curve(curve, z);
    std::vector<xy_point_curve_with_z> z_curves = {z_curve};
    gt->flow_description.get()->emplace(t_begin, make_shared<xyz_point_curve_list>(z_curves));

    waterway_ wtr_tunnel = builder.create_tunnel(id++, "tunnel"s, ""s);
    wtr_tunnel->head_loss_coeff = make_constant_ts(t_begin,t_end, 0.000020, n);
    waterway_ wtr_tailrace = builder.create_tunnel(id++, "tailrace"s, ""s);
    waterway_ wtr_river = builder.create_river(id++, "river"s, ""s);
    
    connect(wtr_tailrace).output_to(wtr_river);
    connect(wtr_flood).input_from(rsv, hydro_power::flood).output_to(wtr_river);
    connect(wtr_tunnel).input_from(rsv);
    
    power_plant_ ps = builder.create_power_plant(id++, "plant"s, ""s);
    ps->outlet_level = make_constant_ts(t_begin,t_end, 10.0, n);
    auto n_ug_members=(n_units+1)/2; // ratio n_ug_members/n_units: 1/1, 1/2, 2/3, 2/4, 3/5, 3/6, 4/7, 4/8 etc..
    auto is_active=make_constant_ts(t_begin,t_end_reserve,1.0, n);// not active last 4 steps
    for(size_t i=0;i<n_units;++i) {
        unit_ gu = builder.create_unit(id++, "unit_"s+to_string(i), ""s);
        ema_ug->add_unit(gu,make_constant_ts(t_begin,t_end,1.0,n));// add unit to ema unit_group
        const double p_min=20.0*mega;
        const double p_max=80.0*mega;
        if(i < n_ug_members) {
            fcr_n_up->add_unit(gu,is_active);
            afrr_up->add_unit(gu,is_active);
        }
        afrr_up2->add_unit(gu,reserve_mask2);// only active last part of  the period
        power_plant::add_unit(ps, gu);
        waterway_ wtr_penstock = builder.create_tunnel(id++, "penstock"s+to_string(i), ""s);
        wtr_penstock->head_loss_coeff = make_constant_ts(t_begin,t_end,0.00005, n);
        // Optional p_min/p_max/p_nom (defaults to generator_description limits)
        if (!use_defaults) {
            gu->production.static_min = make_constant_ts(t_begin,t_end, p_min, n);
            gu->production.static_max = make_constant_ts(t_begin,t_end, p_max, n);
            gu->production.nominal = make_constant_ts(t_begin,t_end, p_max, n);
        }

        gu->generator_description = make_shared<map<utctime, xy_point_curve_>>();
        gu->generator_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
            std::vector<double>{ 20.0*mega, 40.0*mega, 60.0*mega, 80.0*mega },
            std::vector<double>{ 96.0, 98.0, 99.0, 98.0 }));

        gu->turbine_description = make_shared<map<utctime, turbine_description_>>();
        turbine_description_ td = make_shared<hydro_power::turbine_description>();

        td->efficiencies.push_back(hydro_power::turbine_efficiency{
            {
                {{{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 },
                { 70.0+i, 85.0+i, 92.0+i, 94.0+i, 92.0+i, 90.0+i }}, 70.0}// +i: tweak better efficiencies for higher numbered units
            }
        });

        gu->turbine_description.get()->emplace(t_begin, td);
        // reserve properties
        gu->reserve.fcr_static_min= make_constant_ts(t_begin,t_end, p_min-10.0*mega,n);
        gu->reserve.fcr_static_max= make_constant_ts(t_begin,t_end, p_max+10.0*mega,n);
        gu->reserve.droop.schedule = make_constant_ts(t_begin,t_end,8.0,n);// setting 8 %
        //gu->reserve.fcr_n.up.schedule = make_constant_ts(t_begin,t_end,1*mega,n);
        gu->reserve.fcr_n.down.schedule = make_constant_ts(t_begin,t_end,1*mega,n);
        //gu->reserve.afrr.up.schedule = make_constant_ts(t_begin,t_end,2*mega,n);
        gu->reserve.afrr.down.schedule = make_constant_ts(t_begin,t_end,2*mega,n);
        
        connect(wtr_tunnel).output_to(wtr_penstock);
        if (always_inlet_tunnels) {
            waterway_ wtr_inlet = builder.create_tunnel(id++, "inlet"s+to_string(i), ""s);
            connect(wtr_penstock).output_to(wtr_inlet);
            connect(wtr_inlet).output_to(gu);
        } else { // Since we only have one aggregate we have the option to connect the penstock directly to it
            connect(wtr_penstock).output_to(gu);
        }
        connect(gu).output_to(wtr_tailrace);
    }

    stm->hps.push_back(hps);
    return stm;
}

#if 0
// currently done in python tests, so when we find opportunity to
// do this, we create a similar system, with two market areas, etc.
stm_system_ build_simple_model_unit_group_market_area_reference(utctime t_begin, utctime t_end, utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n , bool with_results)
{
    // TODO: Implement
}
#endif
void check_time_series_at_points(const apoint_ts& ts1, const apoint_ts& ts2, const utctime t0, const utctimespan dt, const std::vector<int> n_vec) {
    for (auto n : n_vec) {
        CHECK_EQ(ts1(t0 + n*dt), doctest::Approx(ts2(t0 + n*dt)));
    }
}

void check_results(const stm_system_& stm1, const stm_system_& stm2, const utctime t0, const utctime /*t_end*/, const utctimespan t_step) {
    auto hps1 = stm1->hps[0];
    auto hps2 = stm2->hps[0];
    //1. CHECK reservoir:
    auto rsv1 = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps1->find_reservoir_by_name("reservoir"));
    CHECK(rsv1);
    auto rsv2 = std::dynamic_pointer_cast<shyft::energy_market::stm::reservoir>(hps2->find_reservoir_by_name("reservoir"));
    // 1.1 volume
    CHECK_EQ(true, exists(rsv1->volume.result));
    CHECK_EQ(true, exists(rsv2->volume.result));
    check_time_series_at_points(rsv1->volume.result, rsv2->volume.result, t0, t_step, {0}); // Currently mimicking the tests in test_model_simple.cpp

    // 1.2 level
    CHECK_EQ(true, exists(rsv1->level.result));
    CHECK_EQ(true, exists(rsv2->level.result));
    check_time_series_at_points(rsv1->level.result, rsv2->level.result, t0, t_step, {0});

    // 2 unit:
    auto u1 = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(hps1->find_unit_by_name("aggregate"));
    CHECK(u1);
    auto u2 = std::dynamic_pointer_cast<shyft::energy_market::stm::unit>(hps2->find_unit_by_name("aggregate"));
    CHECK(u2);
    // 2.1 production:
    CHECK_EQ(true, exists(u1->production.result));
    CHECK_EQ(true, exists(u2->production.result));
    check_time_series_at_points(u1->production.result, u2->production.result, t0, t_step, {0, 1, 5, 12});
    // 2.2 discharge
    CHECK_EQ(true, exists(u1->discharge.result));
    CHECK_EQ(true, exists(u2->discharge.result));
    check_time_series_at_points(u1->discharge.result, u2->discharge.result, t0, t_step, {0, 4, 5, 11, 13, 17});
}

void check_results_with_discharge_group(const stm_system_& stm1, const stm_system_& stm2, const utctime /* t0 */, const utctime /* t_end */, const utctimespan /*t_step*/) {
        //check_results(stm1, stm2, t0, t_end, t_step);

        // Find river waterway from refernece system without discharge group
        auto hps_ref = stm2->hps.front();
        auto ww_ref = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(hps_ref->find_waterway_by_name("waterroute river"));
        CHECK_EQ(true, exists(ww_ref->discharge.result));
        CHECK_EQ(false, exists(ww_ref->discharge.penalty.result.accumulated_max));
        CHECK_EQ(false, exists(ww_ref->discharge.penalty.result.accumulated_min));

        // hps with discharge_group
        auto hps = stm1->hps.front();

        // Check waterway attrs are set from shop discharge group
        auto ww = std::dynamic_pointer_cast<shyft::energy_market::stm::waterway>(hps->find_waterway_by_name("waterroute river"));
        CHECK_EQ(true, exists(ww->discharge.result));
        CHECK_EQ(true, exists(ww->discharge.penalty.result.accumulated_max));
        CHECK_EQ(true, exists(ww->discharge.penalty.result.accumulated_min));

}

/** @brief Builds the same model as build_simple_model, but now
 * all time series attached to the model are unbound references to 
 * times series stored in a dtss.
 */
stm_system_ build_simple_model_with_dtss(shyft::dtss::server& dtss, utctime t_begin, utctime t_end, utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n)
{
    
    ts_vector_t tsv;
    tsv.reserve(16);
    
    int id = 1;
    stm_system_ stm = make_shared<stm_system>(id++, "shop_system", "for_testing");
    auto ema_ug=stm->add_unit_group(id++,"NO1.units","",unit_group_type::production);// unit group for e market area
    apoint_ts ug_schedule{make_constant_ts(t_begin, t_end, 10.0, n)};
    apoint_ts ug_cost{make_constant_ts(t_begin, t_end, 80.0, n)};
    ema_ug->obligation.cost=apoint_ts("shyft://test/ug_cost");
    ema_ug->obligation.schedule=apoint_ts("shyft://test/ug_schedule");
    tsv.push_back(apoint_ts("shyft://test/ug_schedule",ug_schedule));
    tsv.push_back(apoint_ts("shyft://test/ug_cost",ug_cost));
    //------------------------
    // stm.market
    //------------------------
    auto market = make_shared<energy_market_area>(id++, "NO1", "xx", stm);
    apoint_ts no1_price = apoint_ts{
        taxis{{ t_begin, t_begin + 6 * t_step, t_begin + 12 * t_step, t_end, t_end + t_step}},
        { 48.0/mega, 40.0/mega, 35.0/mega, 35.0/mega },
        stair_case};
    tsv.emplace_back(apoint_ts("shyft://test/no1_price", no1_price));
    market->price = apoint_ts("shyft://test/no1_price");
    market->tsm["planned_revenue"] = apoint_ts("shyft://test/no1_price")*100.0;
    
    apoint_ts no1_max_buy(taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
        { 10.0*mega, 9999.0*mega },
        stair_case);
    tsv.emplace_back(apoint_ts{"shyft://test/no1_max_buy", no1_max_buy});
    market->max_buy = apoint_ts("shyft://test/no1_max_buy");
    
    apoint_ts no1_max_sale(taxis{{ t_begin, t_begin + 6 * t_step, t_end }},
        { 10.0*mega, 9999.0*mega },
        stair_case);
    tsv.emplace_back(apoint_ts{"shyft://test/no1_max_sale", no1_max_sale});
    market->max_sale = apoint_ts{"shyft://test/no1_max_sale"};
    
    apoint_ts no1_load(taxis{{ t_begin, t_begin + 1 * t_step, t_begin + 4 * t_step, t_begin + 5 * t_step, t_begin + 6 * t_step, t_end }},
        { 90.0*mega, 80.0*mega, 60.0*mega, 10.0*mega, 0.0*mega },
        stair_case);
    tsv.emplace_back(apoint_ts{"shyft://test/no1_load", no1_load});
    market->load = apoint_ts{"shyft://test/no1_load"};
    
    stm->market.push_back(market);

    //----------------------
    // stm.hps
    //----------------------
    stm_hps_ hps = make_shared<stm_hps>(id++, "shop_system_for_testing");
    stm_hps_builder builder(hps);

    reservoir_ rsv = builder.create_reservoir(id++, "reservoir"s, ""s);

    // Optional lrl/hrl/maxvol (defaults to storage_description/spill_description limits)
    if (!use_defaults) {
        auto rsv_lrl = make_constant_ts(t_begin, t_end, 80.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_lrl", rsv_lrl});
        rsv->level.regulation_min = apoint_ts{"shyft://test/rsv_lrl"};

        auto rsv_hrl = make_constant_ts(t_begin, t_end, 100.0, n);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_hrl", rsv_hrl});
        rsv->level.regulation_max = apoint_ts{"shyft://test/rsv_hrl"};

        auto rsv_vol_max = make_constant_ts(t_begin, t_end, 16.0*mega, n);
        tsv.emplace_back(apoint_ts{"shyft://test/rsv_vol_max", rsv_vol_max});
        rsv->volume.static_max = apoint_ts{"shyft://test/rsv_vol_max"};
    }

    rsv->volume_level_mapping = make_shared<map<utctime, xy_point_curve_>>();
    rsv->volume_level_mapping.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 0.0*mega, 2.0*mega, 3.0*mega, 5.0*mega, 16.0*mega },
        std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

    apoint_ts rsv_endpoint_desc(taxis{{ t_begin, t_end - 1 * t_step, t_end }},
        { 0.0, 36.5/mega },
        stair_case);
    tsv.emplace_back(apoint_ts{"shyft://test/rsv_endpoint_desc", rsv_endpoint_desc});
    rsv->water_value.endpoint_desc = apoint_ts{"shyft://test/rsv_endpoint_desc"};

    apoint_ts rsv_level_realised(taxis{{ t_begin - 1 * t_step, t_end }},
        90.0,
        stair_case);
    tsv.emplace_back(apoint_ts{"shyft://test/rsv_level_realised", rsv_level_realised});
    rsv->level.realised = apoint_ts{"shyft://test/rsv_level_realised"};

    apoint_ts rsv_inflow(taxis{{ t_begin, t_begin + 2 * t_step, t_begin + 4 * t_step, t_begin + 6 * t_step, t_begin + 8 * t_step, t_begin + 12 * t_step, t_end }},
        { 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 },
        stair_case);
    tsv.emplace_back(apoint_ts{"shyft://test/rsv_inflow", rsv_inflow});
    rsv->inflow.schedule = apoint_ts{"shyft://test/rsv_inflow"};

    waterway_ wtr_flood = builder.create_river(id++, "waterroute flood river"s, ""s);
    apoint_ts wtr_flood_discharge_static_max = make_constant_ts(t_begin, t_end, 150.0, n);
    tsv.emplace_back(apoint_ts{"shyft://test/wtr_flood_discharge_max_static", wtr_flood_discharge_static_max});
    wtr_flood->discharge.static_max = apoint_ts{"shyft://test/wtr_flood_discharge_max_static"};

    gate_ gt = std::make_shared<gate>(1, "waterway flood gate", "");
    waterway::add_gate(wtr_flood, gt);
    gt->flow_description = make_shared<map<utctime, xyz_point_curve_list_>>();

    xy_point_curve curve(
        { 100.0, 101.5, 103.0, 104.0 },
        { 0.0, 25.0, 80.0, 150.0 }
    );
    double z = 0.0;
    xy_point_curve_with_z z_curve(curve, z);
    std::vector<xy_point_curve_with_z> z_curves = {z_curve};

    gt->flow_description.get()->emplace(t_begin, make_shared<xyz_point_curve_list>(z_curves));

    waterway_ wtr_tunnel = builder.create_tunnel(id++, "waterroute input tunnel"s, ""s);
    apoint_ts wtr_tunnel_head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00030, n);
    tsv.emplace_back(apoint_ts{"shyft://test/wtr_tunnel_head_loss_coeff", wtr_tunnel_head_loss_coeff});
    wtr_tunnel->head_loss_coeff = apoint_ts{"shyft://test/wtr_tunnel_head_loss_coeff"};

    waterway_ wtr_penstock = builder.create_tunnel(id++, "waterroute penstock"s, ""s);
    apoint_ts wtr_penstock_head_loss_coeff = make_constant_ts(t_begin, t_end, 0.00005, n);
    tsv.emplace_back(apoint_ts{"shyft://test/wtr_penstock_head_loss_coeff", wtr_penstock_head_loss_coeff});
    wtr_penstock->head_loss_coeff = apoint_ts{"shyft://test/wtr_penstock_head_loss_coeff"};

    waterway_ wtr_outlet = builder.create_tunnel(id++, "waterroute outlet"s, ""s);
    waterway_ wtr_shaft = builder.create_tunnel(id++, "waterroute shaft"s, ""s);
    waterway_ wtr_tailrace = builder.create_tunnel(id++, "waterroute tailrace"s, ""s);
    waterway_ wtr_river = builder.create_river(id++, "waterroute river"s, ""s);

    unit_ gu = builder.create_unit(id++, "aggregate"s, ""s);
    power_plant_ ps = builder.create_power_plant(id++, "plant"s, ""s);
    power_plant::add_unit(ps, gu);
    apoint_ts ug_active{make_constant_ts(t_begin,t_end,1.0,n)};
    tsv.emplace_back(apoint_ts{"shyft://test/ug_active",ug_active});
    ema_ug->add_unit(gu,apoint_ts{"shyft://test/ug_active"});// add unit to ema
    apoint_ts ps_outlet_level = make_constant_ts(t_begin, t_end, 10.0, n);
    tsv.emplace_back(apoint_ts{"shyft://test/ps_outlet_level", ps_outlet_level});
    ps->outlet_level = apoint_ts{"shyft://test/ps_outlet_level"};

    // Optional p_min/p_max/p_nom (defaults to generator_description limits)
    if (!use_defaults) {
        apoint_ts gu_production_min_static = make_constant_ts(t_begin, t_end, 20.0*mega, n);
        tsv.emplace_back(apoint_ts{"shyft://test/gu_production_min_static", gu_production_min_static});
        gu->production.static_min = apoint_ts{"shyft://test/gu_production_min_static"};

        apoint_ts gu_production_max_static = make_constant_ts(t_begin, t_end, 80.0*mega, n);
        tsv.emplace_back(apoint_ts{"shyft://test/gu_production_max_static", gu_production_max_static});
        gu->production.static_max = apoint_ts{"shyft://test/gu_production_max_static"};

        apoint_ts gu_production_nominal = make_constant_ts(t_begin, t_end, 80.0*mega, n);
        tsv.emplace_back(apoint_ts{"shyft://test/gu_production_nominal", gu_production_nominal});
        gu->production.nominal = apoint_ts{"shyft://test/gu_production_nominal"};
    }

    gu->generator_description = make_shared<map<utctime, xy_point_curve_>>();
    gu->generator_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 20.0*mega, 40.0*mega, 60.0*mega, 80.0*mega },
        std::vector<double>{ 96.0, 98.0, 99.0, 98.0 }));

    gu->turbine_description = make_shared<map<utctime, turbine_description_>>();
    turbine_description_ td = make_shared<hydro_power::turbine_description>();

    td->efficiencies.push_back( hydro_power::turbine_efficiency{
        {  
            {{{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 },
              { 70.0, 85.0, 92.0, 94.0, 92.0, 90.0 }},70.0}
        }
    } );
    
    gu->turbine_description.get()->emplace(t_begin, td);

    connect(wtr_flood).input_from(rsv, hydro_power::flood).output_to(wtr_river);
    connect(wtr_tunnel).input_from(rsv).output_to(wtr_penstock);
    if (always_inlet_tunnels) {
        waterway_ wtr_inlet = builder.create_tunnel(id++, "waterroute inlet"s, ""s);
        connect(wtr_penstock).output_to(wtr_inlet);
        connect(wtr_inlet).output_to(gu);
    } else { // Since we only have one aggregate we have the option to connect the penstock directly to it
        connect(wtr_penstock).output_to(gu);
    }
    connect(gu).output_to(wtr_outlet);
    connect(wtr_outlet).output_to(wtr_shaft);
    connect(wtr_shaft).output_to(wtr_tailrace);
    connect(wtr_tailrace).output_to(wtr_river);

#if 0
    // Additional topology, for manual testing
    reservoir_ rsv2 = builder.create_reservoir(id++, "reservoir2"s, ""s);
    rsv2->storage_description = make_shared<map<utctime, xy_point_curve_>>();
    rsv2->storage_description.get()->emplace(t_begin, make_shared<hydro_power::xy_point_curve>(
        std::vector<double>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
        std::vector<double>{ 80.0, 90.0, 95.0, 100.0, 105.0 }));

    waterway_ wtr_rsv_rsv2 = builder.create_river(id++, "wtr_rsv_rsv2"s, ""s);
    gate_ wtr_rsv_rsv2_gt1 = builder.create_gate(id++, "wtr_rsv_rsv2_gt1", "");
    gate_ wtr_rsv_rsv2_gt2 = builder.create_gate(id++, "wtr_rsv_rsv2_gt2", "");
    gate_ wtr_rsv_rsv2_gt3 = builder.create_gate(id++, "wtr_rsv_rsv2_gt3", "");
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt1);
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt2);
    waterway::add_gate(wtr_rsv_rsv2, wtr_rsv_rsv2_gt3);
    connect(wtr_rsv_rsv2).input_from(rsv2, hydro_power::main).output_to(rsv);

    waterway_ wtr_bypass = builder.create_river(id++, "waterroute bypass river"s, ""s);
    wtr_bypass->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_bypass->discharge.static_max.get()->emplace(t_begin, 150.0);
    connect(wtr_bypass).input_from(rsv, hydro_power::bypass).output_to(wtr_river);

    waterway_ wtr_flood2 = builder.create_river(id++, "waterroute flood river 2"s, ""s);
    wtr_flood2->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_flood2->discharge.static_max.get()->emplace(t_begin, 150.0);
    connect(wtr_flood2).input_from(rsv2, hydro_power::flood).output_to(rsv);

    waterway_ wtr_bypass2 = builder.create_river(id++, "waterroute bypass river 2"s, ""s);
    wtr_bypass2->discharge.static_max = make_shared<map<utctime, double>>();
    wtr_bypass2->discharge.static_max.get()->emplace(t_begin, 150.0);
    gate_ wtr_bypass2_gt1 = builder.create_gate(id++, "wtr_bypass2_gt1", "");
    gate_ wtr_bypass2_gt2 = builder.create_gate(id++, "wtr_bypass2_gt2", "");
    gate_ wtr_bypass2_gt3 = builder.create_gate(id++, "wtr_bypass2_gt3", "");
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt1);
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt2);
    waterway::add_gate(wtr_bypass2, wtr_bypass2_gt3);
    connect(wtr_bypass2).input_from(rsv2, hydro_power::bypass).output_to(rsv);
#endif
    dtss.do_store_ts(tsv, true, true);
    stm->hps.push_back(hps);
    return stm;
}




 std::vector<shyft::energy_market::stm::shop::shop_command> optimization_commands(std::size_t run_id, bool write_files)
{
    using namespace shyft::energy_market::stm::shop;
    std::vector<shop_command> commands{
        shop_command::set_method_primal(),
        shop_command::set_code_full(),
        shop_command::start_sim(3),
        shop_command::set_code_incremental(),
        shop_command::start_sim(3),
    };
    if (write_files) {
        char filename[PATH_MAX];
        sprintf(filename, "shop_log_%zu.txt", run_id);
        commands.insert(commands.begin(), shop_command::log_file(filename));
        sprintf(filename, "shop_results_%zu.txt", run_id);
        commands.push_back(shop_command::return_simres(filename));
        sprintf(filename, "shop_series_%zu.txt", run_id);
        commands.push_back(shop_command::save_series(filename));
        sprintf(filename, "shop_series_%zu.xml", run_id);
        commands.push_back(shop_command::save_xmlseries(filename));
        sprintf(filename, "shop_genres_%zu.txt", run_id);
        commands.push_back(shop_command::return_simres_gen(filename));
    }
    return commands;
}
