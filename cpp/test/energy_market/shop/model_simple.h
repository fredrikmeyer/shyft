#pragma once
#include <doctest/doctest.h>
#include <shyft/energy_market/stm/shop/shop_system.h>
#include <shyft/dtss/dtss.h>
#include <shyft/time/utctime_utilities.h>
#include <memory>
#include <vector>
#include <string>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/waterway.h>

#ifndef PATH_MAX //#if _WIN32
#define PATH_MAX _MAX_PATH
#endif
using shyft::time_series::dd::apoint_ts;
using shyft::time_series::POINT_AVERAGE_VALUE;
using shyft::time_series::dd::gta_t;
using shyft::core::utctime;

static inline apoint_ts make_constant_ts(utctime t, utctime t_end , double value, size_t n=1) {
    return apoint_ts(gta_t(t,t_end-t,n),value,POINT_AVERAGE_VALUE);
}
//SIH: Just to make the tests run through
static bool inline  exists(apoint_ts const&o) {return o.ts?true:false;}

shyft::energy_market::stm::stm_system_ build_simple_model_discharge_group(shyft::core::utctime t_begin, shyft::core::utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n = 1, bool with_results=false);
shyft::energy_market::stm::stm_system_ build_simple_model(shyft::core::utctime t_begin, shyft::core::utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n = 1, bool with_results=false);
shyft::energy_market::stm::stm_system_ build_simple_model_n_units_with_reserves(shyft::core::utctime t_begin, shyft::core::utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n = 1,size_t n_units=2);
// later when we create c++ tests, we would have this function:
//shyft::energy_market::stm::stm_system_ build_simple_model_unit_group_market_area_reference(utctime t_begin, utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n , bool with_results);

void check_time_series_at_points(const apoint_ts& ts1, const apoint_ts& ts2, const shyft::core::utctime t0, const shyft::core::utctimespan dt, const std::vector<int> n_vec) ;

void check_results(const shyft::energy_market::stm::stm_system_& stm1, const shyft::energy_market::stm::stm_system_& stm2, const shyft::core::utctime t0, const shyft::core::utctime /*t_end*/, const shyft::core::utctimespan t_step) ;
void check_results_with_discharge_group(const shyft::energy_market::stm::stm_system_& stm1, const shyft::energy_market::stm::stm_system_& stm2, const shyft::core::utctime t0, const shyft::core::utctime /*t_end*/, const shyft::core::utctimespan t_step) ;

/** @brief Builds the same model as build_simple_model, but now
 * all time series attached to the model are unbound references to 
 * times series stored in a dtss.
 */
shyft::energy_market::stm::stm_system_ build_simple_model_with_dtss(shyft::dtss::server& dtss, shyft::core::utctime t_begin, shyft::core::utctime t_end, shyft::core::utctimespan t_step, bool always_inlet_tunnels, bool use_defaults, size_t n=1);

std::vector<shyft::energy_market::stm::shop::shop_command> optimization_commands(std::size_t run_id, bool write_files);
