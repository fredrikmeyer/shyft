#ifdef SHYFT_WITH_SHOP
#define _CRT_V12_LEGACY_FUNCTIONALITY
#include "fixture.h"
#include "shop_model_simple.h"
#include <array>
#include <iostream>
#include <utility>
#include <future>
#include <filesystem> // current_path
#include <utility>
#include <shyft/energy_market/stm/shop/shop_log_hook.h>

//
// Generic (not test-specific) code for managing threads that uses shop.lib.
//

struct shop_logger {
    virtual void info(const char* msg) = 0;
    virtual void warning(const char* msg) = 0;
    virtual void error(const char* msg) = 0;
    virtual void exit() = 0;
    virtual ~shop_logger(){};
};

class shop_controller {
    class shop_thread {
        friend class shop_controller;
        struct private_deleter {
            void operator()(shop_thread* p) { delete p; }
        };
        struct shop_system {
            ShopSystem* system;
            shop_system(const shop_system&) = delete;
            shop_system(shop_system&&) = delete;
            shop_system& operator=(const shop_system&) = delete;
            shop_system& operator=(shop_system&&) = delete;

            shop_system() {
                system = ShopInit();
                set_shop_library_path_for_test(system);
            }
            ~shop_system() { ShopFree(system); }
        };
        std::size_t id_;
        std::function<void(ShopSystem*, std::size_t)> runner_;
        std::unique_ptr<shop_logger> logger_;
        std::thread thread_;
        shop_thread(std::size_t id, const std::function<void(ShopSystem*, std::size_t)>& runner, std::unique_ptr<shop_logger> logger)
            : id_{id}, runner_{runner}, logger_{std::move(logger)} {
        }
        ~shop_thread() {
            join();
        }
        void start() {
            thread_ = std::thread(&shop_thread::run, this);
        }
        void join() {
            if (thread_.joinable())
                thread_.join();
        }
        void run() const {
            shop_system shop;
            runner_(shop.system, id_);
        }
    };
    std::vector<std::unique_ptr<shop_thread, shop_thread::private_deleter>> threads;
    std::shared_mutex mutex;
public:
    shop_controller() {
        shyft::energy_market::stm::shop::shop_log_hook::info = std::bind(&shop_controller::log_info, this, std::placeholders::_1);
        shyft::energy_market::stm::shop::shop_log_hook::warning = std::bind(&shop_controller::log_warning, this, std::placeholders::_1);
        shyft::energy_market::stm::shop::shop_log_hook::error = std::bind(&shop_controller::log_error, this, std::placeholders::_1);
        shyft::energy_market::stm::shop::shop_log_hook::exit = std::bind(&shop_controller::log_exit, this);
    }
    ~shop_controller() {
        shyft::energy_market::stm::shop::shop_log_hook::info = nullptr;
        shyft::energy_market::stm::shop::shop_log_hook::warning = nullptr;
        shyft::energy_market::stm::shop::shop_log_hook::error = nullptr;
        shyft::energy_market::stm::shop::shop_log_hook::exit = nullptr;
    }
    void spawn(std::size_t id, const std::function<void(ShopSystem*, std::size_t)>& runner, std::unique_ptr<shop_logger> logger) {
        std::unique_lock<std::shared_mutex> lock(mutex);
        auto st = new shop_thread(id, runner, std::move(logger));
        if (auto it = std::find_if(threads.begin(), threads.end(), [&cid = std::as_const(id)](const auto& v) { return !v; }); it != threads.end())
            it->reset(st);
        else
            threads.emplace_back(st);
        st->start();
    }
    void join(std::size_t id) {
        if (std::shared_lock<std::shared_mutex> lock(mutex); true) {
            if (auto it = std::find_if(threads.begin(), threads.end(), [&cid = std::as_const(id)](const auto& v) { return v && v->id_ == cid; }); it != threads.end())
                (*it)->join();
        }
        if (std::unique_lock<std::shared_mutex> lock(mutex); true) {
            if (auto it = std::find_if(threads.begin(), threads.end(), [&cid = std::as_const(id)](const auto& v) { return v && v->id_ == cid; }); it != threads.end())
                delete it->release();
        }
    }
    void log_info(const char* msg) {
        std::shared_lock<std::shared_mutex> lock(mutex);
        const auto thread_id = std::this_thread::get_id();
        if (auto it = std::find_if(threads.cbegin(), threads.cend(), [&thread_id](const auto& v) { return v && v->thread_.get_id() == thread_id; }); it != threads.cend())
            (*it)->logger_->info(msg);
    }
    void log_warning(const char* msg) {
        std::shared_lock<std::shared_mutex> lock(mutex);
        auto thread_id = std::this_thread::get_id();
        if (auto it = std::find_if(threads.cbegin(), threads.cend(), [&thread_id](const auto& v) { return v && v->thread_.get_id() == thread_id; }); it != threads.cend())
            (*it)->logger_->warning(msg);
    }
    void log_error(const char* msg) {
        std::shared_lock<std::shared_mutex> lock(mutex);
        const auto thread_id = std::this_thread::get_id();
        if (auto it = std::find_if(threads.cbegin(), threads.cend(), [&thread_id](const auto& v) { return v && v->thread_.get_id() == thread_id; }); it != threads.cend())
            (*it)->logger_->error(msg);
    }
    void log_exit() {
        std::shared_lock<std::shared_mutex> lock(mutex);
        const auto thread_id = std::this_thread::get_id();
        if (auto it = std::find_if(threads.cbegin(), threads.cend(), [&thread_id](const auto& v) { return v && v->thread_.get_id() == thread_id; }); it != threads.cend())
            (*it)->logger_->exit();
    }
};

//
// Test-specific code
//

// Implementation of shop_thread::logger where each instance (owned by a individual threads) writes
// to a separate file, and therefore does not need to implement synchronization between threads.
struct shop_file_logger : shop_logger
{
    std::size_t id;
    std::ofstream file;
    char filename[40];
    shop_file_logger(std::size_t id) : id{id} {
        sprintf(filename, "shop_run_log_%zu.txt", id);
        file.open(filename);
    }
    void info(const char* msg) override {
        file << "[INFO] " << msg;
    }
    void warning(const char* msg) override {
        file << "[WARNING] " << msg;
    }
    void error(const char* msg) override {
        file << "[ERROR] " << msg;
    }
    void exit() override {
        file << "[EXIT]";
    }
};

// Implementation of shop_thread::logger where each instance (owned by a individual threads) writes
// to a output stream that is not owned, and therefore may be shared in which case we need to
// synchronization between threads.
struct shop_stream_logger : shop_logger
{
    static std::mutex mutex; // Since all instances, one for each thread, are logging to shared resource std::cout we must synchronize them.
    std::size_t id;
    std::ostream& os;
    shop_stream_logger(std::size_t id, std::ostream& os) : id{id}, os{os} {}
    void info(const char* msg) override {
        std::lock_guard<std::mutex> lock(mutex);
        os << "[" << id << "] [INFO] " << msg;
    }
    void warning(const char* msg) override {
        std::lock_guard<std::mutex> lock(mutex);
        os << "[" << id << "] [WARNING] " << msg;
    }
    void error(const char* msg) override {
        std::lock_guard<std::mutex> lock(mutex);
        os << "[" << id << "] [ERROR] " << msg;
    }
    void exit() override {
        std::lock_guard<std::mutex> lock(mutex);
        os << "[" << id << "] [EXIT]";
    }
};
// Define static member of shop_stream_logger.
std::mutex shop_stream_logger::mutex;

static auto set_model_input(const model_builder& builder, const model_simple& model)
{
    builder.SetGetCompareDouble(model.reservoir_ix, "start_head", 90.0, false); // TODO: false because ShopAttributeExists crashes on this one!
    builder.SetGetCompareDouble(model.reservoir_ix, "energy_value_input", 36.5, false);
    builder.SetGetCompareTimeSeries(model.reservoir_ix, model.start_time, model.n_time_steps, "inflow",
        6,
        std::array<int,6>{ 0, 2, 4, 6, 8, 12 },
        std::array<std::array<double,6>,model_builder::n_scen>{ {{ 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 }} },
        true);
}

static auto optimize_model(const model_builder& builder, const model_simple& model, std::size_t run_id, bool silent)
{
    ShopSetSilentConsole(builder.shop, silent);
    if (!silent) {
        // Set custom log file
        char filename[PATH_MAX];
        sprintf(filename, "shop_log_%zu.txt", run_id);
        char* objectList[]{ filename };
        shop_name_str log_file{"log file"};
        CHECK(ShopExecuteCommand(builder.shop, log_file, 0, nullptr, 1, objectList));
    }
    // Optimization commands
    {
        shop_name_str primal{"primal"} ;
        char* optionList[]{primal};
        shop_name_str set_method{"set method"};
        CHECK(ShopExecuteCommand(builder.shop, set_method, 1, optionList, 0, nullptr));
    }
    {
        shop_name_str full{"full"};
        char* optionList[]{full};
        shop_name_str set_code{"set code"};
        CHECK(ShopExecuteCommand(builder.shop, set_code, 1, optionList, 0, nullptr));
    }
    {
        shop_name_str three{"3"};
        char* objectList[]{three};
        shop_name_str start_sim{"start sim"};
        CHECK(ShopExecuteCommand(builder.shop, start_sim, 0, nullptr, 1, objectList));
    }
    {
        shop_name_str incremental{"incremental"};
        char* optionList[]{incremental};
        shop_name_str set_code{"set code"};
        CHECK(ShopExecuteCommand(builder.shop, set_code, 1, optionList, 0, nullptr));
    }
    {
        shop_name_str three{"3"};
        char* objectList[]{three};
        shop_name_str start_sim{"start sim"};
        CHECK(ShopExecuteCommand(builder.shop, start_sim, 0, nullptr, 1, objectList));
    }
    if (!silent) {
        // Optional write result files for manual inspection
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_results_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str return_simres{"return simres"};
            CHECK(ShopExecuteCommand(builder.shop, return_simres , 0, nullptr, 1, objectList));
        }
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_series_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str save_series{"save series"};
            CHECK(ShopExecuteCommand(builder.shop, save_series, 0, nullptr, 1, objectList));
        }
        { // NB: XML series requires license!
            char filename[PATH_MAX];
            sprintf(filename, "shop_series_%zu.xml", run_id);
            char* objectList[]{ filename };
            shop_name_str save_xmlseries{"save xmlseries"};
            CHECK(ShopExecuteCommand(builder.shop,save_xmlseries , 0, nullptr, 1, objectList));
        }
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_results_generators_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str gen{"gen"};
            char* optionList[]{ gen };
            shop_name_str return_simres{"return simres"};
            CHECK(ShopExecuteCommand(builder.shop, return_simres , 1, optionList, 1, objectList));
        }
    }
    // Quit command not relevant?
    //{
    //	CHECK(ShopExecuteCommand(shopSystem, "quit", 0, nullptr, 0, nullptr));
    //}

    //
    // Results (read from API)
    //
    builder.GetCheckResultSeries(model.reservoir_ix, model.start_time, model.n_time_steps, "storage",
        1,
        std::array<int,1>{ 0 },
        std::array<std::array<double,1>,model_builder::n_scen>{ {{ 2.0 }} },
        true);
    builder.GetCheckResultSeries(model.reservoir_ix, model.start_time, model.n_time_steps, "head",
        1,
        std::array<int,1>{ 0 },
        std::array<std::array<double,1>,model_builder::n_scen>{ {{ 90.0 }} },
        true);
    builder.GetCheckResultSeries(model.generator_ix, model.start_time, model.n_time_steps, "discharge",
        6,
        array<int,6>{ 0, 4, 9, 11, 12, 17 },
        array<array<double,6>,model_builder::n_scen>{ {{ 110.0, 90.0012457407 /*before v14: 85.100016353481479*/ /*before v13.1.1.d: 90.0*/, 80.0, 80.0, 0.0, 64.6929059953 }} },
        true);
    builder.GetCheckResultSeries(model.market_ix, model.start_time, model.n_time_steps, "load_penalty",
        3,
        std::array<int,3>{ 0, 1, 17 },
        std::array<std::array<double,3>, model_builder::n_scen>{ {{ 7.60947301814789512, 0.0, 0.0 }} },
        false); // TODO: false because ShopAttributeExists crashes on this one!
}

static void run_shop_thread(ShopSystem* shop_system, std::size_t id)
{
    try {
        const bool silent = true;
        model_builder builder(shop_system);
        model_simple model(builder);
        set_model_input(builder, model);
        optimize_model(builder, model, id, silent);
    } catch (...) {
        std::cerr << "Ooops, exception in shop-thread" << std::endl;
        //hmmm.. this is absolutely possible, so thats why we use future, not threads
    }
}

static void run_threads(const std::size_t num_threads)
{
    shop_controller controller;

    //std::ofstream shared_log_stream("shop_run_log.txt"); // Shared file stream
    //std::ostream& shared_log_stream = std::cout; // Shared console stream
    std::ofstream shared_null_stream; // Shared but closed stream
    for (std::size_t i = 0; i < num_threads; ++i) {
        //controller.spawn(i, run_optimize_thread, std::make_unique<shop_file_logger>(i));
        //controller.spawn(i, run_optimize_thread, std::make_unique<shop_stream_logger>(i,std::cout));
        controller.spawn(i, run_shop_thread, std::make_unique<shop_stream_logger>(i, shared_null_stream));
    }
    // Could explicitely wait for all threads here, but it will also be done automatically
    // as the controller object goes out of scope.
    //for (int i = 0; i < num_threads; ++i) {
    //	controller.join(i);
    //}
}

TEST_SUITE("model_simple_multithread"
    * doctest::skip()) // Skip for now since it is not stable enough (e.g. log reports "License file corrupt")
{
    TEST_CASE("Optimize single thread")
    {
        run_threads(1);
    }
    TEST_CASE("Optimize three threads")
    {
        run_threads(3);
    }
}
#endif
