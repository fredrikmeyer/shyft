//
// Build minimalistic SHOP model using API directly. Using no licensed features.
//
#pragma once
//#define _CRT_SECURE_NO_WARNINGS
#define _CRT_V12_LEGACY_FUNCTIONALITY
#include "shop_model_builder.h"
#include <array>
using std::array;

struct model_simple
{
    shop_time_str start_time;
    shop_time_str end_time;
    shop_time_unit_str time_unit;
    int n_time_steps;
    int market_ix, reservoir_ix, plant_ix, generator_ix;

    model_simple(const model_builder& builder) :
        //
        // System
        //
        start_time{ "2018010100" },
        end_time{ "2018010118" },
        time_unit{ "hour" },
        n_time_steps{ 18 }
    {
        CHECK(ShopSetTimeResolution(builder.shop, start_time, end_time, time_unit)); // In general, time resolution must be set before any objects are added, since time series will be initialized. Adding reservoirs and plants works though, but adding generators crashes (segmentation violation).

        //
        // Market
        //
        constexpr const auto market_type_index{ 12 };
        auto market_type_name = shop_type::market;
        shop_name_str market_name{ "TheMarket" };
        market_ix = ShopAddObject(builder.shop, market_type_index, market_name);
        const char* objectTypeName = ShopGetObjectType(builder.shop, market_ix);
        CHECK(strcmp(objectTypeName, market_type_name) == 0);
        CHECK(strcmp(market_name, ShopGetObjectName(builder.shop, market_ix)) == 0);
        builder.SetGetCompareTimeSeries(market_ix, start_time, n_time_steps, "buy_price",
            3,
            array<int, 3>({ 0, 6, 12 }),
            array<array<double, 3>, model_builder::n_scen>({ {{ 48.1, 40.1, 35.1 }} }),
            true);
        builder.SetGetCompareTimeSeries(market_ix, start_time, n_time_steps, "sale_price",
            3,
            array<int, 3>({ 0, 6, 12 }),
            array<array<double, 3>, model_builder::n_scen>({ {{ 48.0, 40.0, 35.0 }} }),
            true);
        builder.SetGetCompareTimeSeries(market_ix, start_time, n_time_steps, "max_buy",
            2,
            array<int, 2>({ 0, 6 }),
            array<array<double, 2>,model_builder::n_scen>({ {{ 10.0, 9999.0 }} }),
            true);
        builder.SetGetCompareTimeSeries(market_ix, start_time, n_time_steps, "max_sale",
            2,
            array<int, 2>({ 0, 6 }),
            array<array<double, 2>, model_builder::n_scen>({ {{ 10.0, 9999.0 }} }),
            true);
        builder.SetGetCompareTimeSeries(market_ix, start_time, n_time_steps, "load",
            5,
            array<int, 5>({ 0, 1, 4, 5, 6 }),
            array<array<double, 5>, model_builder::n_scen>({ {{ 90.0, 80.0, 60.0, 10.0, 0.0 }} }),
            false); // TODO: false because ShopAttributeExists after ShopSetTxyAttribute crashes on this one!

        //
        // Topology
        //

        // Create reservoir
        constexpr const auto reservoir_type_index{ 0 };

        shop_name_str reservoir_name{ "TheReservoir" };
        reservoir_ix = ShopAddObject(builder.shop, reservoir_type_index, reservoir_name);
        CHECK(strcmp(shop_type::reservoir, ShopGetObjectType(builder.shop, reservoir_ix)) == 0);
        CHECK(strcmp(reservoir_name, ShopGetObjectName(builder.shop, reservoir_ix)) == 0);

        // Create flood and bypass gates
        constexpr const auto gate_type_index{ 5 };

        shop_name_str  flood_gate_name{ "f_TheReservoir_Sea" };
        shop_name_str bypass_gate_name{ "b_TheReservoir_Sea" };
        const auto flood_gate_ix = ShopAddObject(builder.shop, gate_type_index, flood_gate_name);
        CHECK(strcmp(shop_type::gate, ShopGetObjectType(builder.shop, flood_gate_ix)) == 0);
        CHECK(strcmp(flood_gate_name, ShopGetObjectName(builder.shop, flood_gate_ix)) == 0);
        const auto bypass_gate_ix = ShopAddObject(builder.shop, gate_type_index, bypass_gate_name);
        CHECK(strcmp(shop_type::gate, ShopGetObjectType(builder.shop, bypass_gate_ix)) == 0);
        CHECK(strcmp(bypass_gate_name, ShopGetObjectName(builder.shop, bypass_gate_ix)) == 0);

        // Create plant
        constexpr const auto plant_type_index{ 1 };

        shop_name_str  plant_name{ "ThePlant" };
        plant_ix = ShopAddObject(builder.shop, plant_type_index, plant_name);
        CHECK(strcmp(shop_type::plant, ShopGetObjectType(builder.shop, plant_ix)) == 0);
        CHECK(strcmp(plant_name, ShopGetObjectName(builder.shop, plant_ix)) == 0);

        // Create generator
        constexpr const auto generator_type_index{ 2 };

        shop_name_str generator_name{ "ThePlant_G1" };
        generator_ix = ShopAddObject(builder.shop, generator_type_index, generator_name);
        CHECK(strcmp(shop_type::generator, ShopGetObjectType(builder.shop, generator_ix)) == 0);
        CHECK(strcmp(generator_name, ShopGetObjectName(builder.shop, generator_ix)) == 0);

        // Connect plant to generator
        CHECK(ShopAddRelation(builder.shop, plant_ix, shop_relation::main, generator_ix));
        {
            int nRelations{ -999 };
            int relatedIndexList[100]{};
            CHECK(ShopGetRelations(builder.shop, plant_ix, shop_relation::main, nRelations, &relatedIndexList[0]));
            CHECK(nRelations == 1);
            CHECK(relatedIndexList[0] == generator_ix);
            shop_attr_str num_gen{ "num_gen" };
            int attr_ix = ShopGetAttributeIndex(builder.shop, plant_ix, num_gen);
            CHECK(ShopCheckAttributeToGet(builder.shop, attr_ix));
            int nGen = -1;
            CHECK(ShopGetIntAttribute(builder.shop, plant_ix, attr_ix, nGen));
            CHECK(nGen == 1);
        }

        // Connect reservoir to plant/flood/bypass
        CHECK(ShopAddRelation(builder.shop, reservoir_ix, shop_relation::main, plant_ix));
        {
            int nRelations{ -999 };
            int relatedIndexList[100]{};
            CHECK(ShopGetRelations(builder.shop, reservoir_ix,shop_relation::main, nRelations, &relatedIndexList[0]));
            CHECK(nRelations == 1);
            CHECK(relatedIndexList[0] == plant_ix);
        }
        CHECK(ShopAddRelation(builder.shop, reservoir_ix, shop_relation::spill, flood_gate_ix));
        {
            int nRelations{ -999 };
            int relatedIndexList[100]{};
            CHECK(ShopGetRelations(builder.shop, reservoir_ix,shop_relation::spill, nRelations, &relatedIndexList[0]));
            CHECK(nRelations == 1);
            CHECK(relatedIndexList[0] == flood_gate_ix);
        }
        CHECK(ShopAddRelation(builder.shop, reservoir_ix, shop_relation::bypass, bypass_gate_ix));
        {
            int nRelations{ -999 };
            int relatedIndexList[100]{};
            CHECK(ShopGetRelations(builder.shop, reservoir_ix, shop_relation::bypass, nRelations, &relatedIndexList[0]));
            CHECK(nRelations == 1);
            CHECK(relatedIndexList[0] == bypass_gate_ix);
        }

        // Set reservoir properties
        builder.SetGetCompareDouble(reservoir_ix, "lrl", 80.0, true);
        builder.SetGetCompareDouble(reservoir_ix, "hrl", 100.0, true);
        builder.SetGetCompareDouble(reservoir_ix, "max_vol", 16.0, true);
        builder.SetGetCompareXY(reservoir_ix, "vol_head", 0.0,
            5,
            array<double, 5>{ 0.0, 2.0, 3.0, 5.0, 16.0 },
            array<double, 5>{ 80.0, 90.0, 95.0, 100.0, 105.0 },
            true);
        builder.SetGetCompareXY(reservoir_ix, "flow_descr", 0.0,
            4,
            array<double, 4>{ 100.0, 101.5, 103.0, 104.0 },
            array<double, 4>{ 0.0, 25.0, 80.0, 150.0 },
            true);

        // Set plant properties
        builder.SetGetCompareDouble(plant_ix, "outlet_line", 10.0, true);
        builder.SetGetCompareDoubleArray(plant_ix, "main_loss", 1, array<double, 1>{ 0.00030 }, true);
        builder.SetGetCompareDoubleArray(plant_ix, "penstock_loss", 1, array<double, 1>{ 0.00005 }, true);

        // Generator properties
        builder.SetGetCompareInt(generator_ix, "penstock", 1, false);
        builder.SetGetCompareDouble(generator_ix, "p_min", 20.0, true);
        builder.SetGetCompareDouble(generator_ix, "p_max", 80.0, true);
        builder.SetGetCompareDouble(generator_ix, "p_nom", 80.0, true);
        builder.SetGetCompareXY(generator_ix, "gen_eff_curve", 0.0,
            4,
            array<double, 4>{ 20.0, 40.0, 60.0, 80.0 },
            array<double, 4>{ 96.0, 98.0, 99.0, 98.0 },
            true);
        builder.SetGetCompareXYArray(generator_ix, "turb_eff_curves",
            1, // 1 curve
            array<double, 1>{70.0}, // Reference value 70.0 in the first (and only) curve
            array<int, 1>{6}, // 6 points in the first (and only) curve
            array<array<double, 6>, 1>{ {{ 20.0, 40.0, 60.0, 80.0, 100.0, 110.0 }} }, // X values for the first (and only) curve
            array<array<double, 6>, 1>{ {{ 70.0, 85.0, 92.0, 94.0, 92.0, 90.0 }} }, // Y values for the first (and only) curve
            false); // TODO: false because ShopAttributeExists after ShopSetXyArrayAttribute crashes this one!
        // No startcost:
        // In ASCII file startcost can be set as scalar value as part of the basic generator attributes,
        // and then there is an optional licensed feature to set it as time series. But in API there is only the time series
        // attribute, and setting it requires valid SHOP_TIME_STARTCOST license. Therefore, for a minimalistic model like
        // this we chose to not set any startcost.

        // Flood and bypass gate attributes
        builder.SetGetCompareDouble(flood_gate_ix, "max_discharge", 150.0, true);
        builder.SetGetCompareDouble(bypass_gate_ix, "max_discharge", 1000.0, true);
    }
};
