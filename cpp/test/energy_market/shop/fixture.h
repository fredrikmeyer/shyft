#pragma once
#include <shop_lib_interface.h> // External Shop library
#include <doctest/doctest.h> // Doctest testing framework
#include <string>
#include <fstream>
#include <limits.h>
#include <string.h>
#include <float.h>
#include <functional>
#include <shared_mutex>
#include <cstdlib>
//#include <boost/dll/runtime_symbol_info.hpp> // program_location
#include <filesystem> // current_path
//#include <iostream> // cout/cerr

#ifndef PATH_MAX //#if _WIN32
#define PATH_MAX _MAX_PATH
#define NAME_MAX (_MAX_FNAME-1)
#endif

//
// Definitions copied from shop_proxy.h
//

using shop_time_str      = char[ 18]; // date/time, format "YYYYMMDDHHMMSSxxx", which is 17 characters + 1 null terminator
using shop_time_unit_str = char[  7]; // time unit, currently longest values are "minute" and "second"
using shop_name_str      = char[100]; // object name, qualified guessed size based on testing
using shop_type_str      = char[ 19]; // type names, currently longes value is "needle_combination"
using shop_attr_str      = char[ 31]; // attribute name, longest value is "tailrace_loss_from_bypass_flag" (attribute of plant)
using shop_relation_str  = char[ 32]; // relation, currently longest value is "needle_combination_of_generator"
using shop_cmd_opt_str   = char[ 21]; // option argument of commands, qualified guessed size based on known command options
using shop_cmd_obj_str   = char[260]; // object argument of commands, qualified guessed size based on testing, and since value can be file path a value matching MAX_PATH should be allowed

struct shop_time_unit {
    static constexpr shop_time_unit_str second{ "second" };
    static constexpr shop_time_unit_str minute{ "minute" };
    static constexpr shop_time_unit_str hour{ "hour" };
    static constexpr shop_time_unit_str day{ "day" };
    static constexpr shop_time_unit_str week{ "week" };
    static constexpr shop_time_unit_str month{ "month" };
    static constexpr shop_time_unit_str year{ "year" };
};

struct shop_relation {
    //static constexpr shop_relation_str generator_of_plant{ "generator_of_plant" }; // Note: Deprecated in Shop v14, should use connection_standard instead.
    //static constexpr shop_relation_str pump_of_plant{ "pump_of_plant" }; // Note: Deprecated in Shop v14, should use connection_standard instead.
    static constexpr shop_relation_str needle_combination_of_generator{ "needle_combination_of_generator" };
    static constexpr shop_relation_str main{ "connection_standard" };
    static constexpr shop_relation_str spill{ "connection_spill" };
    static constexpr shop_relation_str bypass{ "connection_bypass" };
};

//
// Additional definitions for unit tests
//

struct shop_type {
    static constexpr shop_type_str reservoir{"reservoir"};
    static constexpr shop_type_str generator{"generator"};
    static constexpr shop_type_str plant{"plant"};
    static constexpr shop_type_str thermal{"thermal"};
    static constexpr shop_type_str gate{"gate"};
    static constexpr shop_type_str market{"market"};
    static constexpr shop_type_str scenario{"scenario"};
    static constexpr shop_type_str junction{ "junction" };
    static constexpr shop_type_str junction_gate{ "junction_gate" };
    static constexpr shop_type_str creek_intake{ "creek_intake" };
    static constexpr shop_type_str tunnel{ "tunnel" };
    static constexpr shop_type_str cut{ "cut" };
};

template<class T> // T = shop::api from shop_proxy.h
static void set_shop_library_path_for_test(T& api) {
    // Copied from shop_system constructor:
    if (!std::getenv("ICC_COMMAND_PATH")) {
        // Shop API v0.3.0 needs to be told path to solver interface library from either API function ShopAddDllPath
        // or env.var. ICC_COMMAND_PATH (but core solver library can be in PATH, application directory, working directory etc).
        // Any license file must be either in working directory or path given by env.var. ICC_COMMAND_PATH (but ShopAddDllPath
        // is not being considered).
        // Here we default to load solver interface library from current working directory, when ICC_COMMAND_PATH is not set,
        // so that solver interface library file and the license file both must be in path given by either of these.
        api.set_library_path(std::filesystem::current_path().string().c_str());
    }
}

static void set_shop_library_path_for_test(ShopSystem* shopSystem) {
    // See comment above.
    if (!std::getenv("ICC_COMMAND_PATH")) {
        ShopAddDllPath(shopSystem, std::filesystem::current_path().string().c_str());
    }
}

//
// Common test fixture
//

struct test_fixture {
    // Test fixture class used to create a centralized place of calling ShopInit at beginning
    // and ShopFree at end of each test case (that uses this fixture). Could have achived
    // the same by doing this at start and end of a test case and then implement subcases
    // of this, but since we have a lot of cases we avoid the extra nesting level by refactoring
    // it out to a fixture.
    ShopSystem* shopSystem_;
    bool silent_;
    test_fixture() {
        //MESSAGE("Setting up test fixture by allocating Shop system");
        shopSystem_ = ShopInit();
        silent_ = ShopSetSilentConsole(shopSystem_, true);
        set_shop_library_path_for_test(shopSystem_);
    }
    ~test_fixture() {
        //MESSAGE("Tearing down test fixture by releasing Shop system");
        (void)ShopFree(shopSystem_);
    }

    // disallow all other stuff.
    test_fixture(const test_fixture&)=delete;
    test_fixture & operator=(shop_lib_system&)=delete;
    test_fixture(test_fixture&&)=delete;
    test_fixture & operator=(shop_lib_system&&)=delete;
};

struct shop_log_hook {
    static std::function<void(const char*)> info;
    static std::function<void(const char*)> warning;
    static std::function<void(const char*)> error;
    static std::function<void()> exit;
};
