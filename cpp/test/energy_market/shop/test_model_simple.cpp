#ifdef SHYFT_WITH_SHOP
#define _CRT_V12_LEGACY_FUNCTIONALITY
#include "fixture.h"
#include <string>
#include <array>
#include "shop_model_simple.h"
#include "shop_model_simple_tunnel.h"

using std::string;
using std::array;

template<class T>
static auto set_basic_input(const model_builder& builder, const T& model)
{
    builder.SetGetCompareDouble(model.reservoir_ix, "start_head", 90.0, false); // TODO: false because ShopAttributeExists after ShopSetDoubleAttribute crashes on this one!
    builder.SetGetCompareDouble(model.reservoir_ix, "energy_value_input", 36.5, false);
    builder.SetGetCompareTimeSeries(model.reservoir_ix, model.start_time, model.n_time_steps, "inflow",
        6,
        array<int,6>{ 0, 2, 4, 6, 8, 12 },
        array<array<double,6>,model_builder::n_scen>{ {{ 60.0, 64.0, 68.0, 62.0, 60.0, 50.0 }} },
        true);
}
template<class T>
static auto check_basic_results(const model_builder& builder, const T& model)
{
    // Reservoir volume
    builder.GetCheckResultSeries(model.reservoir_ix, model.start_time, model.n_time_steps+1, "storage",
        1,
        array<int,1>{ 0 },
        array<array<double,1>,model_builder::n_scen>{ {{ 2.0 }} },
        true);
    // Reservoir head
    // Check:
    //   - Head in first time step should match the start reservoir we used!
    builder.GetCheckResultSeries(model.reservoir_ix, model.start_time, model.n_time_steps+1, "head",
        1,
        array<int,1>{ 0 },
        array<array<double,1>,model_builder::n_scen>{ {{ 90.0 }} },
        true);
    // Reservoir incr_cost
    // Bypass gate discharge
    // Flood gate discharge
    // Plant prod_unbalance
    // Plant cons_unbalance
    // Plant production
    // Plant discharge
    // Plant gross_production
    // Plant gross_discharge
    // Plant consumption
    // Plant upflow
    // Plant eff_head
    // Plant max_prod
    // Plant marg_cost
    // Plant operation_cost
    // Generator production
    builder.GetCheckResultSeries(model.generator_ix, model.start_time, model.n_time_steps, "production",
        4,
        array<int,4>{ 0, 1, 5, 12 },
        array<array<double,4>,model_builder::n_scen>{ {{ 72.39, 70.0, 20.0, 0.0 }} },
        true);
    // Generator discharge
    builder.GetCheckResultSeries(model.generator_ix, model.start_time, model.n_time_steps, "discharge",
        6,
        array<int,6>{ 0, 4, 9, 11, 12, 17 },
        array<array<double,6>,model_builder::n_scen>{ {{ 110.0, 90.0012457407 /*before v14: 85.100016353481479*/ /*before v13.1.1.d: 90.0*/, 80.0, 80.0, 0.0, 64.6929059953 }} },
        true);
    // Area incr_cost
    // Case incr_cost
    // Area sale
    // Area buy
    // Case sale
    // Case buy
    // Case balance
    // Reservoir penalty
    // Reservoir(?) End penalty (NB: Scalar value, not time series!)
    // Area load_penalty
    // Case load_penalty
    builder.GetCheckResultSeries(model.market_ix, model.start_time, model.n_time_steps, "load_penalty",
        3,
        array<int,3>{ 0, 1, 17 },
        array<array<double,3>, model_builder::n_scen>{ {{ 7.60947301814789512, 0.0, 0.0 }} },
        false); // TODO: false because ShopAttributeExists returns false on this one, even if there are values!
}

template<class T>
static auto optimize_model(const model_builder& builder, const T& , size_t run_id, bool silent)
{
    if (!silent) {
        // Not silent console, as it silences the log file as well
        ShopSetSilentConsole(builder.shop, false);
        // Set custom log file
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_log_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str log_file{"log file"};
            CHECK(ShopExecuteCommand(builder.shop, log_file, 0, nullptr, 1, objectList));
        }
    }
    // Optimization commands
    {
        shop_name_str primal{"primal"} ;
        char* optionList[]{primal};
        shop_name_str set_method{"set method"};
        CHECK(ShopExecuteCommand(builder.shop, set_method, 1, optionList, 0, nullptr));
    }
    {
        shop_name_str full{"full"};
        char* optionList[]{full};
        shop_name_str set_code{"set code"};
        CHECK(ShopExecuteCommand(builder.shop, set_code, 1, optionList, 0, nullptr));
    }
    {
        shop_name_str three{"3"};
        char* objectList[]{three};
        shop_name_str start_sim{"start sim"};
        CHECK(ShopExecuteCommand(builder.shop, start_sim, 0, nullptr, 1, objectList));
    }
    {
        shop_name_str incremental{"incremental"};
        char* optionList[]{incremental};
        shop_name_str set_code{"set code"};
        CHECK(ShopExecuteCommand(builder.shop, set_code, 1, optionList, 0, nullptr));
    }
    {
        shop_name_str three{"3"};
        char* objectList[]{three};
        shop_name_str start_sim{"start sim"};
        CHECK(ShopExecuteCommand(builder.shop, start_sim, 0, nullptr, 1, objectList));
    }
    if (!silent) {
        // Optional write result files for manual inspection
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_results_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str return_sim_res{"return simres"};
            CHECK(ShopExecuteCommand(builder.shop, return_sim_res , 0, nullptr, 1, objectList));
        }
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_series_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str save_series{"save series"};
            CHECK(ShopExecuteCommand(builder.shop, save_series, 0, nullptr, 1, objectList));
        }
        { // NB: XML series requires license!
            char filename[PATH_MAX];
            sprintf(filename, "shop_series_%zu.xml", run_id);
            char* objectList[]{ filename };
            shop_name_str save_xml_series{"save xmlseries"};
            CHECK(ShopExecuteCommand(builder.shop, save_xml_series, 0, nullptr, 1, objectList));
        }
        {
            char filename[PATH_MAX];
            sprintf(filename, "shop_results_generators_%zu.txt", run_id);
            char* objectList[]{ filename };
            shop_name_str gen_option{"gen"};
            char* optionList[]{ gen_option };
            shop_name_str return_sim_res{"return simres"};
            CHECK(ShopExecuteCommand(builder.shop, return_sim_res, 1, optionList, 1, objectList));
        }
    }
    // Quit command not relevant?
    //{
    //	CHECK(ShopExecuteCommand(shopSystem, "quit", 0, nullptr, 0, nullptr));
    //}
}

TEST_SUITE("model simple")
{
    const bool silent = true;

    TEST_CASE_FIXTURE(test_fixture, "Build")
    {
        model_builder builder(shopSystem_);
        model_simple model(builder);
    }

    TEST_CASE_FIXTURE(test_fixture, "Build and optimize once")
    {
        model_builder builder(shopSystem_);
        model_simple model(builder);
        set_basic_input(builder, model);
        optimize_model(builder, model, 1, silent);
        check_basic_results(builder, model);
    }

    TEST_CASE_FIXTURE(test_fixture, "Build once and optimize twice" * doctest::skip(true))
    {
        // NOTE: According to Sintef optimizing more than once is not a supported use case!
        //       One needs to initialize a new API object, and then build and set input on it.
        model_builder builder(shopSystem_);
        model_simple model(builder);
        set_basic_input(builder, model);
        optimize_model(builder, model, 1, silent);
        check_basic_results(builder, model);
        optimize_model(builder, model, 2, silent);
        check_basic_results(builder, model);
    }

    TEST_CASE("Build and optimize twice")
    {
        // Test case running the same test twice in a row - in same process.
        // (This is just a convenience test when running individual test cases from IDE,
        // where each case is executed as a separate process. When running the exe test runner
        // directly all tests are run in same process, and we could just have added two
        // separate test cases running the same test).
        size_t run_id = 1;
        {
            test_fixture fix;
            model_builder builder(fix.shopSystem_);
            model_simple model(builder);
            set_basic_input(builder, model);
            optimize_model(builder, model, run_id++, silent);
            check_basic_results(builder, model);
        }
        // TODO: As of 22.11.2018 the second run fails because there is 0.0 load penalty in first
        // time step instead of 7.61, and there are inconsistencies with reported balance/unbalance
        // in part of the result files that are not exposed through the API!!
        {
            test_fixture fix;
            model_builder builder(fix.shopSystem_);
            model_simple model(builder);
            set_basic_input(builder, model);
            optimize_model(builder, model, run_id++, silent);
            check_basic_results(builder, model);
        }
    }

    TEST_CASE_FIXTURE(test_fixture, "Maxprod restriction")
    {
        model_builder builder(shopSystem_);
        model_simple model(builder);
        set_basic_input(builder, model);

        // Note: Shop API version 0.3.0 gives a warning, and on console even when silent mode is enabled:
        //       WARNING: 1206
        //       Model testing gave 1 warnings.
        // It is a bug that the warning is reported on console when silent mode is on.
        // The reason for the warning in the first place is explanation by SINTEF:
        //    Det er egentlig ingen grunn til at shop skal skrive ut warning fordi man har lagt på en max restriksjon,
        //    MEN det skrives ut warnings når man endrer grensene relativt mye i forhold til hva som er spesifisert på
        //    virkningsgradskurven.Dette er gjort fordi det skal være lettere å oppdage feil hvis man har spesifisert
        //    helt feil grenser. Advarselen skrives ut når man endrer grensene 25 % fra det som er angitt i virkningsgradskurven.
        //    Her endres det (80 - 60) / 80 = 0.25 så det er akkurat på grensen for hva det skrives ut advarsel for.

        // Set restriction for maximum production
        // With basic input the plant produces around 70MW the first 4 hours, so we try to set max constraint to 50 first hour,
        // then NaN which should be treated as no constraint, and then 50MW in third hour!
        builder.SetGetCompareTimeSeries(model.generator_ix, model.start_time, model.n_time_steps, "max_p_constr",
            3,
            array<int,3>{ 0, 1, 2 },
            array<array<double,3>,model_builder::n_scen>{ {{ 60, std::numeric_limits<double>::quiet_NaN(), 50.0 }} },
            true);
        optimize_model(builder, model, 1, silent);
        // Check the resulting production
        builder.GetCheckResultSeries(model.generator_ix, model.start_time, model.n_time_steps, "production",
            3,
            array<int,3>{ 0, 1, 2 },
            array<array<double,3>,model_builder::n_scen>{ {{ 60.0, 70.0, 50.0 }} },
            true);
    }

    TEST_CASE_FIXTURE(test_fixture, "Build and optimize with tunnel")
    {
        model_builder builder(shopSystem_);
        model_simple_tunnel model(builder);
        set_basic_input(builder, model);
        optimize_model(builder, model, 3, silent);

        // Not identical results as when modelling without tunnel, and also wants to check results on tunnel itself!
        //check_basic_results(builder, model);

        // Reservoir volume
        builder.GetCheckResultSeries(model.reservoir_ix, model.start_time, model.n_time_steps+1, "storage",
            1,
            array<int,1>{ 0 },
            array<array<double,1>,model_builder::n_scen>{ {{ 2.0 }} },
            true);
        // Reservoir head
        // Check:
        //   - Head in first time step should match the start reservoir we used!
        builder.GetCheckResultSeries(model.reservoir_ix, model.start_time, model.n_time_steps+1, "head",
            1,
            array<int,1>{ 0 },
            array<array<double,1>,model_builder::n_scen>{ {{ 90.0 }} },
            true);
        // Generator production
        builder.GetCheckResultSeries(model.generator_ix, model.start_time, model.n_time_steps, "production",
            4,
            array<int,4>{ 0, 1, 5, 12 },
            array<array<double,4>,model_builder::n_scen>{ {{ 72.3905, 70.0, 20.0, 0.0 }} },
            true);
        // Generator discharge
        builder.GetCheckResultSeries(model.generator_ix, model.start_time, model.n_time_steps, "discharge",
            6,
            array<int,6>{ 0, 3, 4, 11, 12, 17 },
            array<array<double,7>,builder.n_scen>{ {{ 110.0, 109.844, 85.1225, 64.9223, 0.0, 64.4833 }} },
            true);
        builder.GetCheckResultSeries(model.market_ix, model.start_time, model.n_time_steps, "load_penalty",
            3,
            array<int,3>{ 0, 4, 17 },
            array<array<double,3>, model_builder::n_scen>{ {{ 7.60947, 0.0, 0.0 }} },
            false); // TODO: false because ShopAttributeExists returns false on this one, even if there are values!

        // Tunnel flow (equal to generator discharge)
        builder.GetCheckResultSeries(model.tunnel_ix, model.start_time, model.n_time_steps, "flow",
            6,
            array<int,6>{ 0, 3, 4, 11, 12, 17 },
            array<array<double,7>,builder.n_scen>{ {{ 110.0, 109.844, 85.1225, 64.9223, 0.0, 64.4833 }} },
            true);
    }
}
#endif
