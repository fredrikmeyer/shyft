#include <doctest/doctest.h>

#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include "model_simple.h"
#include <shyft/energy_market/stm/srv/dstm/server.h>
#include <shyft/energy_market/stm/srv/dstm/server_logger.h>
#include <shyft/energy_market/stm/srv/dstm/client.h>
#include <shyft/energy_market/stm/srv/dstm/context.h>
#include <shyft/core/fs_compat.h>
#include <test/test_utils.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/energy_market/a_wrap.h>

#include <shyft/energy_market/stm/shop/shop_log_entry.h>

#include <csignal>

using std::string;
using std::to_string;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;
using shyft::core::from_seconds;

namespace test {
    using std::string;
    using std::string_view;
    using std::vector;
    using std::make_shared;
    using shyft::energy_market::stm::srv::dstm::model_state;

    struct test_server: shyft::energy_market::stm::srv::dstm::server {
        shyft::web_api::energy_market::request_handler bg_server;
        std::future<int> web_srv;///< mutex,

        //-- to verify fx-callback
        string fx_mid;
        string fx_arg;
        bool fx_handler(string mid,string json_arg) {
            fx_mid=mid;
            fx_arg=json_arg;
            return true;
        }

        explicit test_server() : server() {
            bg_server.srv = this;
            this->fx_cb=[this](string m,string a)->bool {return this->fx_handler(m,a);};
        }

        explicit test_server(const string& root_dir) : server() {
            bg_server.srv = this;
            dtss->add_container("test", root_dir);
        }

        void start_web_api(string host_ip,int port,string doc_root,int fg_threads,int bg_threads) {
            if(!web_srv.valid()) {
                web_srv= std::async(std::launch::async,
                    [this,host_ip,port,doc_root,fg_threads,bg_threads]()->int {
                        return shyft::web_api::run_web_server(
                        bg_server,
                        host_ip,
                        static_cast<unsigned short>(port),
                        make_shared<string>(doc_root),
                        fg_threads,
                        bg_threads);

                    }
                );
            }
        }
        bool web_api_running() const {return web_srv.valid();}
        void stop_web_api() {
            if(web_srv.valid()) {
                std::raise(SIGINT);
                (void) web_srv.get();
            }
        }
    };

    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;

    unsigned short get_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return  acceptor.local_endpoint().port();
    }

    // Sends a WebSocket message and prints the response, from examples made by boost.beast/Vinnie Falco
    class session : public std::enable_shared_from_this<session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string text_;
        string response_;
        string fail_;
        std::function<string(string const&)> report_response;

        // Report a failure
        void
        fail(error_code ec, char const* what) {
            fail_= string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec,diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit
        session(boost::asio::io_context& ioc)
            : resolver_(ioc)
            , ws_(ioc)
        {
        }
        string response() const {return response_;}
        string diagnostics() const {return fail_;}
        // Start the asynchronous operation
        template <class Fx>
        void
        run(string_view host, int port, string_view text, Fx&& rep_response) {
            // Save these for later
            host_ = host;
            text_ = text;
            port_ = std::to_string(port);
            report_response=rep_response;
            resolver_.async_resolve(host_,port_,// Look up the domain name
                                    [me=shared_from_this()](error_code ec,tcp::resolver::results_type results) {
                                        me->on_resolve(ec,results);
                                    }
            );
        }

        void
        on_resolve(error_code ec,tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(),results.begin(),results.end(),
                // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
                                       std::bind(&session::on_connect,shared_from_this(),std::placeholders::_1)
            );
        }

        void
        on_connect(error_code ec){
            fail_on_error(ec, "connect")

            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                                [me=shared_from_this()](error_code ec) {me->on_handshake(ec);}
            );
        }

        void
        on_handshake(error_code ec) {
            fail_on_error(ec, "handshake")
            if(text_.size()) {
                ws_.async_write( // Send the message
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // empty text to send means we are done and should close connection
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_write(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "write")

            ws_.async_read(buffer_, // Read a message into our buffer
                           [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                               me->on_read(ec,bytes_transferred);
                           }
            );
        }

        void
        on_read(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read")
            response_=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());
            text_= report_response(response_);
            if(text_.size()) { // more messages to send?
                ws_.async_write( // send the message..
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // else close it.
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_close(error_code ec) {
            fail_on_error(ec,"close")
        }
        #undef fail_on_error
    };
}

TEST_SUITE("shop_web_api") {

    const bool always_inlet_tunnels = false;
    const bool use_defaults = false;
    const auto t_begin = shyft::core::create_from_iso8601_string("2018-01-01T01:00:00Z");
    const auto t_end = shyft::core::create_from_iso8601_string("2018-01-01T19:00:00Z");
    const auto t_step = shyft::core::deltahours(1);
    const auto n_steps = (size_t)((t_end - t_begin) / t_step);
    const shyft::time_axis::generic_dt ta{ t_begin, t_step, n_steps };

    using shyft::energy_market::stm::shop::shop_log_entry;

    TEST_CASE("shop_get_log") {
        dlib::set_all_logging_levels(dlib::LALL);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "shop.web_api.test." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // note..: ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem
        // However, this directory is required, but never used in this test.

        // Set up test server:
        using std::vector;
        test::test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        // Store some models:
        auto mdl = build_simple_model(t_begin, t_end, t_step, always_inlet_tunnels, use_defaults);
        a.do_add_model("simple", mdl);
        // Add some messages:
        auto ctx = a.do_get_context("simple");
        ctx->add_shop_log({
            shop_log_entry(shop_log_entry::log_severity::information, "An informational message", 1145, from_seconds(0)),
            shop_log_entry(shop_log_entry::log_severity::diagnosis_warning, "A diagnosis warning message", 3301, from_seconds(1))
        });

        int port=test::get_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true, a.web_api_running());
        vector<string> requests{
            R"_(get_log {"request_id": "1", "model_key": "simple"})_",
            R"_(get_state {"request_id": "2", "model_key": "simple"})_"
        };
        vector<string> responses;
        responses.reserve(requests.size());
        size_t r = 0;
        {
            boost::asio::io_context ioc;
            auto s1 = std::make_shared<test::session>(ioc);
            s1->run(host_ip, port, requests[r],
                [&responses, &r, &requests](string const&web_response)->string {
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size() ? string("") : requests[r];
                }
            );

            ioc.run();
            s1.reset();
        }

        vector<string> expected{
            R"_({"request_id":"1","result":[{"time":0.0,"severity":"INFORMATION","code":1145,"message":"An informational message"},{"time":1.0,"severity":"DIAGNOSIS WARNING","code":3301,"message":"A diagnosis warning message"}]})_",
            R"_({"request_id":"2","result":{"state":"idle"}})_"
        };

        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }
}
