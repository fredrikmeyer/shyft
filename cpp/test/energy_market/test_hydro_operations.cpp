#include <doctest/doctest.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/hydro_power/hydro_component.h>
#include <shyft/energy_market/hydro_power/reservoir.h>
#include <shyft/energy_market/hydro_power/power_plant.h>
#include <shyft/energy_market/hydro_power/waterway.h>
#include <shyft/energy_market/hydro_power/hydro_power_system.h>
#include <shyft/energy_market/hydro_power/hydro_operations.h>
#include <shyft/energy_market/graph/graph_utilities.h>

#include <build_test_model.h>
#include <serialize_loop.h>
#include <shyft/core/core_archive.h>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

using namespace shyft::energy_market::hydro_power;
using namespace shyft::energy_market;
using test::serialize_loop;

namespace test {
    using namespace shyft::energy_market::hydro_power;
    using namespace std;

    hydro_power_system_ create_svartisen_hps() {
        /*
        A HydroPowerSystem created using parts of Svartisen system.
        Returns
        ------ -
        HydroPowerSystem with reservoirs, tunnels, powerplants, including pumps
        */
        auto svartisen = make_shared<hydro_power_system>(0, "Svartisen");
        hydro_power_system_builder hps(svartisen);
        auto varrevaejkaj = hps.create_reservoir(71301, "Varrevæjkaj", string("{\"max_vol\":\"3200Mm3\"}"));
        auto langvatn = hps.create_reservoir(71306, "Langvatn", string("{\"max_vol\":\"3100Mm3\"}"));
        auto reinoksvatn = hps.create_reservoir(71304, "Reinoksvatn", string("{\"max_vol\":\"3100Mm3\"}"));
        auto livsejavri = hps.create_reservoir(71303, "Livsejavri", string("{\"max_vol\":\"3100Mm3\"}"));
        auto fossvatn = hps.create_reservoir(71302, "Fossvatn", string("{\"max_vol\":\"3100Mm3\"}"));

        auto nord_ovf = hps.create_reservoir(67304, "Nord-Ovf", string("{\"max_vol\":\"3100Mm3\"}"));
        auto syd_ovf = hps.create_reservoir(67305, "Syd-Ovf", string("{\"max_vol\":\"3100Mm3\"}"));
        auto ost_ovf = hps.create_reservoir(67306, "Ost-Ovf", string("{\"max_vol\":\"3100Mm3\"}"));
        auto svartisenmag = hps.create_reservoir(67302, "Svartisenmag", string("{\"max_vol\":\"3100Mm3\"}"));

        auto havet = hps.create_reservoir(0, "havet", "{}");

        auto kobbelv_hydr = hps.create_unit(71305, "Kobbelv-Kobbelv-Hydr", "{}");
        auto kobbelv_hydr_ps = hps.create_power_plant(71305, "Kobbelv-Kobbelv-Hydr", "{}");
        power_plant::add_unit(kobbelv_hydr_ps, kobbelv_hydr);
        auto svartisen_hydr = hps.create_unit(67302, "Svartisenmag", "{}");
        auto svartisen_hydr_ps = hps.create_power_plant(71302, "Svartisenmag", "{}");
        power_plant::add_unit(svartisen_hydr_ps, svartisen_hydr);

        auto varrevaejkaj_wr = hps.create_waterway(37, "Main water route from Varrevæjkaj", string("{}"));
        auto langvatn_wr = hps.create_waterway(38, "Mian water route from Langvatn", string("{}"));
        auto reinoksvatn_wr = hps.create_waterway(33, "Main water route Reinoksvatn", string("{}"));
        auto livsejavri_wr = hps.create_waterway(32, "Main water route from Livsejavri", string("{}"));
        auto fossvatn_wr = hps.create_waterway(39, "Main water route from Fossvatn", string("{}"));
        auto kobbelv_hydr_wr_1 = hps.create_waterway(42, "Main water route to Kobbelv-Kobbelv-Hydr", string("{}"));
        auto kobbelv_hydr_wr_2 = hps.create_waterway(34, "Main water route from Kobbelv-Kobbelv-Hydr to Ocean", string("{}"));

        auto nord_ovf_wr = hps.create_waterway(25, "Main water route from Nord-Ovf", string("{}"));
        auto syd_ovf_wr = hps.create_waterway(26, "Main water route from Syd-Ovf", string("{}"));
        auto ost_ovf_wr = hps.create_waterway(27, "Main water route from Ost-Ovf", string("{}"));
        auto svartisenmag_wr_1 = hps.create_waterway(35, "Main water route from Svartisenmag to Svartisenmag-output", string("{}"));
        auto svartisenmag_wr_2 = hps.create_waterway(28, "Main water route from Svartisenmag-output to Ocean", string("{}"));

        kobbelv_hydr_wr_1->output_to(kobbelv_hydr_wr_1, kobbelv_hydr);
        kobbelv_hydr->output_to(kobbelv_hydr, kobbelv_hydr_wr_2);

        svartisenmag_wr_1->output_to(svartisenmag_wr_1, svartisen_hydr);
        svartisenmag_wr_2->input_from(svartisenmag_wr_2, svartisen_hydr);

        for (auto &res : hps.hps->reservoirs) {
            for (auto &wr : hps.hps->waterways) {
                if (wr->name.find(res->name) != string::npos && res->name.compare("havet") != 0 && res->id != 67302) 
                    res->output_to(res, wr, connection_role::main);
                if (res->id == 71302 && (wr->id == 33 || wr->id == 32)) res->input_from(res, wr);
                if (res->id == 0 && (wr->id == 34 || wr->id==28))  res->input_from(res, wr);
                if (res->id == 67302 && (wr->id == 25 || wr->id == 26 || wr->id == 27)) res->input_from(res, wr);
                if (res->id == 67302 && wr->id == 35) res->output_to(res, wr, connection_role::main);
            }
        }
        for (auto &wr1 : hps.hps->waterways) {
            if (wr1->id == 37 || wr1->id == 38 || wr1->id == 39) {
                for (auto &wr2 : hps.hps->waterways) {
                    if (wr2->id == 42) {
                        wr1->output_to(wr1, wr2);
                        break;
                    }
                }
            }
        }
        return svartisen;
    }


    hydro_power_system_ create_with_loop_hps() {
        /*
        A HydroPowerSystem that contains topological loops
            r0--(w1)-[u1]-(w2)-r1--(w2)-{r0}
        Returns
        ------ -
        HydroPowerSystem with reservoirs, tunnels, powerplants, including pumps
        */
        auto h = make_shared<hydro_power_system>(0, "Svartisen");
        hydro_power_system_builder hps(h);
        auto r1=hps.create_reservoir(10,"r0");
        auto w1=hps.create_waterway(1,"w1");
        auto w2=hps.create_waterway(2,"w2");
        auto w3=hps.create_waterway(3,"w3");
        auto w4=hps.create_waterway(3,"w4");
        auto u1=hps.create_unit(100,"u1");
        auto up=hps.create_unit(101,"pump");
        auto r2=hps.create_reservoir(11,"r1");
        auto z=hps.create_reservoir(0,"havet");
        
        hydro_connect(w1).input_from(r1,connection_role::main).output_to(u1);      
        hydro_connect(w2).input_from(u1).output_to(r1);
        hydro_connect(w3).input_from(r2,connection_role::main).output_to(r2);
        return h;
    }
}
TEST_SUITE("hydro_operations") {
    TEST_CASE("hydro_power_system_op_functionality") {
        using namespace shyft::energy_market::hydro_power;
        auto a = test::create_svartisen_hps();
        auto b = test::create_svartisen_hps();
        CHECK(a != nullptr);
        CHECK(b != nullptr);
        CHECK(a->equal_structure(*b) == true);
        SUBCASE("serialization") {
            auto b_as_blob = hydro_power_system::to_blob(b);
            CHECK(b_as_blob.size() > 0);
            auto b_from_blob = hydro_power_system::from_blob(b_as_blob);
            CHECK(b_from_blob->equal_structure(*b));
        }
        a->clear();
        b->clear();
    }

    TEST_CASE("hydro_power_system_topology_operations") {
        using namespace shyft::energy_market::hydro_power;
        auto a = test::create_svartisen_hps();
        {
            auto svartisenmag = a->find_reservoir_by_name("Svartisenmag");
            CHECK(svartisenmag != nullptr);
            auto svartisenmag_ds = graph::downstream_units(svartisenmag);
            auto svartisenmag_us = graph::upstream_units(svartisenmag);
            CHECK(svartisenmag_ds.size() == 1);
            CHECK(svartisenmag_ds[0]->name == "Svartisenmag");
            CHECK(svartisenmag_us.size() == 0);
            
            auto kobbelv_ps = a->find_power_plant_by_name("Kobbelv-Kobbelv-Hydr");
            CHECK(kobbelv_ps != nullptr);
            CHECK(kobbelv_ps->name == "Kobbelv-Kobbelv-Hydr");
            CHECK(kobbelv_ps->units.size() == 1);
            
            auto fossvatn = a->find_reservoir_by_name("Fossvatn");
            CHECK(fossvatn != nullptr);
            auto fossvatn_us = fossvatn->upstreams;
            auto fossvatn_ds = fossvatn->downstreams;
            CHECK(fossvatn_us.size() == 2);
            CHECK(fossvatn_ds.size() == 1);

            SUBCASE("find_by_xx") {
                CHECK(a->find_power_plant_by_name("Kobbelv-Kobbelv-Hydr")->id == 71305);
                CHECK(a->find_power_plant_by_id(71305)->name == "Kobbelv-Kobbelv-Hydr");
                CHECK(a->find_unit_by_name("Kobbelv-Kobbelv-Hydr")->id == 71305);
                CHECK(a->find_unit_by_id(71305)->name == "Kobbelv-Kobbelv-Hydr");
                CHECK(a->find_reservoir_by_name("Langvatn")->id == 71306);
                CHECK(a->find_reservoir_by_id(71306)->name == "Langvatn");
                CHECK(a->find_waterway_by_name("Main water route from Nord-Ovf")->id == 25);
                CHECK(a->find_waterway_by_id(25)->name == "Main water route from Nord-Ovf");
            }
        }
        a->clear();
        a = nullptr;
    }

    TEST_CASE("path to ocean") {
        using namespace shyft::energy_market::hydro_power;
        auto a = test::create_svartisen_hps();
        {
            auto nord_ovf = a->find_reservoir_by_name("Nord-Ovf");
            CHECK(nord_ovf != nullptr);
            auto path = hydro_operations::get_path_to_ocean(nord_ovf);
            CHECK(path.size() == 7);
            CHECK(path[1]->id == 25);
            CHECK(std::dynamic_pointer_cast<waterway>(path[1]) != nullptr);
            CHECK(path[2]->id == 67302);
            CHECK(std::dynamic_pointer_cast<reservoir>(path[2]) != nullptr);
            
            auto reinoksvatn = a->find_reservoir_by_name("Reinoksvatn");
            CHECK(reinoksvatn != nullptr);
            auto path_1 = hydro_operations::get_path_to_ocean(reinoksvatn);
            CHECK(path_1.size() == 8);
            CHECK(path_1[1]->id == 33);
            CHECK(std::dynamic_pointer_cast<waterway>(path_1[1]) != nullptr);
            CHECK(path_1[2]->id == 71302);
            CHECK(std::dynamic_pointer_cast<reservoir>(path_1[2]) != nullptr);
        
            auto fossvatn = a->find_reservoir_by_name("Fossvatn");
            CHECK(hydro_operations::is_connected(fossvatn, reinoksvatn) == true);
            CHECK(hydro_operations::is_connected(fossvatn, nord_ovf) == false);

            auto path_2 = hydro_operations::get_path_between(reinoksvatn, fossvatn);
            CHECK(path_2.size() == 3);
            CHECK(path_2[1]->id == 33);
            CHECK(std::dynamic_pointer_cast<waterway>(path_2[1]) != nullptr);
            CHECK(path_2[2]->id == 71302);
            CHECK(std::dynamic_pointer_cast<reservoir>(path_2[2]) != nullptr);
        }	
        a->clear();
        a = nullptr;
        
    }
    TEST_CASE("hps_with_loops") {
        using namespace shyft::energy_market::hydro_power;
        auto s = test::create_with_loop_hps();
        for(auto const &r:s->reservoirs) {
            auto p=hydro_operations::get_path_to_ocean(r);
            CHECK(p.size()>=0);
        }
    }
    TEST_CASE("extract watercourses") {
        using namespace shyft::energy_market::hydro_power;
        using ho = hydro_operations;
        auto a = test::create_svartisen_hps();
        {
            auto watercourses = ho::extract_water_courses(a);
            CHECK(watercourses.size() == 2);
            CHECK(watercourses[0] != nullptr);
            CHECK(watercourses[1] != nullptr);
            
            // Check Kobbelv
            CHECK(watercourses[0]->power_plants.size() == 1);
            CHECK(watercourses[0]->units.size() == 1);
            CHECK(watercourses[0]->reservoirs.size() == 6);
            CHECK(watercourses[0]->waterways.size() == 7);

            // Check Svartisen
            CHECK(watercourses[1]->power_plants.size() == 1);
            CHECK(watercourses[1]->units.size() == 1);
            CHECK(watercourses[1]->reservoirs.size() == 5);
            CHECK(watercourses[1]->waterways.size() == 5);
        }
        a->clear();
        a = nullptr;
    }
}
