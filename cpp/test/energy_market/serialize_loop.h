#pragma once
#include <boost/serialization/serialization.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

//-- notice that boost serialization require us to
//   include shared_ptr/vector .. etc.. wherever it's needed

#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <sstream>
#include <string>

namespace test {
template <class T>
static T serialize_loop(const T& o) {
    std::ostringstream xmls;
	/* scope this, ensure archive is flushed/destroyd */{
        boost::archive::binary_oarchive oa(xmls);
        oa << BOOST_SERIALIZATION_NVP(o); 
	}
    xmls.flush();
    std::string ss = xmls.str();
   
    T o2;
	std::istringstream xmli(ss);
	{
		boost::archive::binary_iarchive ia(xmli);
        ia >> BOOST_SERIALIZATION_NVP(o2);
	}
    return o2;
}
}

