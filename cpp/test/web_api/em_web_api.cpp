#include "test_pch.h"

#include <shyft/web_api/targetver.h>
#include <memory>
#include <dlib/logger.h>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/network.h>
#include <shyft/energy_market/stm/transmission_line.h>
#include <shyft/energy_market/stm/busbar.h>
#include <shyft/energy_market/stm/power_module.h>

#include <shyft/energy_market/constraints.h>
#include <shyft/core/fs_compat.h>

#include "build_stm_system.h"
#include "mocks.h"
#include <test/test_utils.h>
#include <csignal>

//#include <iostream>


using std::string;
using std::string_view;
using std::make_shared;
using std::vector;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::dd::gta_t;
using shyft::time_series::ts_point_fx;
using shyft::core::from_seconds;
using shyft::energy_market::core::absolute_constraint;
using namespace shyft::energy_market::stm;

namespace shyft::energy_market::test {
    using test_server=::mocks::dstm_server;

    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;
    // Sends a WebSocket message and prints the response, from examples made by boost.beast/Vinnie Falco
    class session : public std::enable_shared_from_this<session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string text_;
        string response_;
        string fail_;
        std::function<string(string const&)> report_response;

        // Report a failure
        void
        fail(error_code ec, char const* what) {
            fail_= string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec,diag) if((ec)) return fail((ec),(diag));
    public:
        // Resolver and socket require an io_context
        explicit
        session(boost::asio::io_context& ioc)
            : resolver_(ioc)
            , ws_(ioc)
        {
        }
        string response() const {return response_;}
        string diagnostics() const {return fail_;}
        // Start the asynchronous operation
        template <class Fx>
        void
        run(string_view host, int port, string_view text, Fx&& rep_response) {
            // Save these for later
            host_ = host;
            text_ = text;
            port_ = std::to_string(port);
            report_response=rep_response;
            resolver_.async_resolve(host_,port_,// Look up the domain name
                                    [me=shared_from_this()](error_code ec,tcp::resolver::results_type results) {
                                        me->on_resolve(ec,results);
                                    }
            );
        }

        void
        on_resolve(error_code ec,tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve")
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(),results.begin(),results.end(),
                // for some reason, does not compile: [me=shared_from_this()](error_code ec)->void {me->on_connect(ec);}
                                       std::bind(&session::on_connect,shared_from_this(),std::placeholders::_1)
            );
        }

        void
        on_connect(error_code ec){
            fail_on_error(ec, "connect")

            ws_.async_handshake(host_, "/", // Perform the websocket handshake
                                [me=shared_from_this()](error_code ec) {me->on_handshake(ec);}
            );
        }

        void
        on_handshake(error_code ec) {
            fail_on_error(ec, "handshake")
            if(text_.size()) {
                ws_.async_write( // Send the message
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // empty text to send means we are done and should close connection
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_write(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "write")

            ws_.async_read(buffer_, // Read a message into our buffer
                           [me=shared_from_this()](error_code ec,size_t bytes_transferred) {
                               me->on_read(ec,bytes_transferred);
                           }
            );
        }

        void
        on_read(error_code ec,std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read")
            response_=boost::beast::buffers_to_string(buffer_.data());
            buffer_.consume(buffer_.size());
            text_= report_response(response_);
            if(text_.size()) { // more messages to send?
                ws_.async_write( // send the message..
                    boost::asio::buffer(text_),
                    [me=shared_from_this()](error_code ec,size_t bytes_transferred){
                        me->on_write(ec,bytes_transferred);
                    }
                );
            } else { // else close it.
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) {me->on_close(ec);}
                );
            }
        }

        void
        on_close(error_code ec) {
            fail_on_error(ec,"close")
        }
        #undef fail_on_error
    };

    unsigned short get_free_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return  acceptor.local_endpoint().port();
    }

}

using std::to_string;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;

using std::dynamic_pointer_cast;

TEST_SUITE("em_web_api") {
    const double mega=1e6;
    const double eur_mega=1/1e6;
    constexpr auto add_market=[](stm_system_ const& mdl) {
            utctime t0{0};
            utctime dt=calendar::HOUR;
            gta_t ta(t0,dt,6);
            auto mkt= make_shared<energy_market_area>(1,"NOX","{}",mdl);
            mkt->price= apoint_ts(ta,vector<double>{30.1*eur_mega,30.2*eur_mega,31.3*eur_mega,31.4*eur_mega,31.5*eur_mega,31.6*eur_mega},ts_point_fx::POINT_AVERAGE_VALUE);
            mkt->load = apoint_ts(ta,vector<double>{1.1*mega,1.2*mega,1.3*mega,1.4*mega,1.5*mega,1.6*mega},ts_point_fx::POINT_AVERAGE_VALUE);
            mkt->sale = apoint_ts(ta,vector<double>{1.1*mega,1.2*mega,1.3*mega,1.4*mega,1.5*mega,1.6*mega},ts_point_fx::POINT_AVERAGE_VALUE);
            mkt->tsm["planned_revenue"] = 10.0*mkt->sale; // add user speficied attribute
            mdl->market.push_back(mkt);
    };
    constexpr auto add_unit_group=[](stm_system_ const& mdl) {
        auto ug=mdl->add_unit_group(1,"ug1","{}");
        utctime t0{0};
        utctime dt=calendar::HOUR;
        gta_t ta(t0,dt,6);
        auto u0=dynamic_pointer_cast<unit>(mdl->hps[0]->find_unit_by_id(1));
        FAST_REQUIRE_UNARY(u0!=nullptr);
        u0->production.result=apoint_ts(ta,vector<double>{0.0,20.0*mega,30.0*mega,40.0*mega,0.0,0.0},ts_point_fx::POINT_AVERAGE_VALUE);
        u0->discharge.result = apoint_ts(ta,vector<double>{0.0,21.0,31.0,41.0,0.0,0.0},ts_point_fx::POINT_AVERAGE_VALUE);
        ug->add_unit(u0,apoint_ts(ta,vector<double>{0.0,1.0,1.0,1.0,1.0,0.0},ts_point_fx::POINT_AVERAGE_VALUE));
        ug->group_type=unit_group_type::fcr_n_up;
        ug->obligation.schedule=apoint_ts(ta,vector<double>{0.0,2.0*mega,3.0*mega,4.0*mega,0.0,0.0},ts_point_fx::POINT_AVERAGE_VALUE);
    };
    constexpr auto add_network_and_power_modules=[](stm_system_ const& mdl) {
        auto n = make_shared<network>(50,"net","{}",mdl);
        mdl->networks.push_back(n);
        auto t1 = make_shared<transmission_line>(51,"t1", "{}",n);
        n->transmission_lines.push_back(t1);
        auto b1 = make_shared<busbar>(52,"b1","{}",n);
        auto b2 = make_shared<busbar>(53,"b2","{}",n);
        n->busbars.push_back(b1);
        n->busbars.push_back(b2);
        b1->add_to_start_of_transmission_line(t1);
        b2->add_to_end_of_transmission_line(t1);
        auto p1 = make_shared<power_module>(54,"p1","{}",mdl);
        auto p2 = make_shared<power_module>(55,"p2","{}",mdl);
        mdl->power_modules.push_back(p1);
        mdl->power_modules.push_back(p2);
        b1->add_to_power_module(p1);
        b2->add_to_power_module(p2);
    };

    TEST_CASE("read_model") {
        dlib::set_all_logging_levels(dlib::LALL);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // note..: ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem
        // However, this directory is required, but never used in this test.

        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        // Store some models:
        auto mdl = test::web_api::create_stm_system();
        a.do_add_model("sorland", mdl);

        auto mdl2 = test::web_api::create_simple_system(2,"simple");
        mdl2->run_params.n_inc_runs = 2;
        mdl2->run_params.n_full_runs = 3;
        mdl2->run_params.head_opt = true;
        //-- ensure to add a market so we can verify we can get attributes from the market.
        add_market(mdl2);
        add_unit_group(mdl2);
        add_network_and_power_modules(mdl2);
        a.do_add_model("simple", mdl2);

        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        //We'll also dress the model up with some time-series:
        //utctimespan dt{deltahours(1)};
        //fixed_dt ta{_t(0),dt,6};
        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{R"_(read_model {"request_id": "1", "model_key": "simple"})_",
                                R"_(read_attributes {"request_id": "2",
                                        "model_key": "simple",
                                        "time_axis": {"t0":0.0,"dt": 3600,"n": 2},
                                        "hps":[{
                                                "hps_id": 1,
                                                    "reservoirs": [{
                                                        "component_id": 1,
                                                        "attribute_ids": ["level.result"]
                                                    }],
                                                    "units": [{
                                                        "component_id": 1,
                                                        "attribute_ids": ["discharge.constraint.max"]
                                                    }],
                                                    "catchments": [{
                                                        "component_id": 12,
                                                        "attribute_ids": ["component", "doesnt", "exist"]
                                                    }],
                                                    "waterways":[{
                                                        "component_id": 1,
                                                        "attribute_ids": ["discharge.result", "discharge.realised"]
                                                    }],
                                                    "gates": [{
                                                        "component_id": 1,
                                                        "attribute_ids": ["discharge.result","discharge.realised"]
                                                    }]
                                                }
                                                ],
                                        "markets":[{
                                                "component_id": 1,
                                                "attribute_ids": ["price","load","sale","buy","ts.planned_revenue"]
                                            }],
                                        "unit_groups":[
                                           {
                                              "component_id":1,
                                              "attribute_ids":["group_type","obligation.schedule","flow","production"],
                                              "members": [{
                                                 "component_id":1,
                                                 "attribute_ids":["active"]
                                              }]
                                           }
                                        ],
                                        "networks":[
                                            {
                                                "component_id":50,
                                                "transmission_lines":[{
                                                    "component_id":51,
                                                    "attribute_ids":["capacity"]
                                                }],
                                                "busbars":[{
                                                    "component_id":52,
                                                    "attribute_ids":["dummy"]
                                                }]
                                            }
                                        ],
                                        "power_modules":[{
                                            "component_id":54,
                                            "attribute_ids":["power.schedule"]
                                        }]
                                   })_",
                                R"_(get_model_infos {"request_id": "3"})_",
                                R"_(get_hydro_components {"request_id": "4",
                                        "model_key": "simple",
                                        "hps_id": 1
                                })_",
                                R"_(get_hydro_components {"request_id": "5",
                                        "model_key": "simple",
                                        "hps_id": 1,
                                        "available_data": true
                                })_",
                                R"_(run_params {"request_id" : "6", "model_key" : "simple"})_",
                                R"_(get_log {"request_id": "7", "model_key": "simple"})_",
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            //cout<<"Starting client session\n";
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
            //cout<<"Stopping web_api\n";
        }
        vector<string> expected{R"_({"request_id":"1","result":{"model_key":"simple","id":2,"name":"simple","json":"","hps":[{"id":1,"name":"simple"}],"unit_groups":[{"id":1,"name":"ug1"}],"markets":[{"id":1,"name":"NOX"}],"contracts":[],"contract_portfolios":[],"networks":[{"id":50,"name":"net"}],"power_modules":[{"id":54,"name":"p1"},{"id":55,"name":"p2"}]}})_",
                                R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.result","data":{"id":"","pfx":false,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1.0,1.0]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"discharge.constraint.max","data":"not found"}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":false,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[5.0,5.0]}},{"attribute_id":"discharge.realised","data":"not found"}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[20.0,21.0]}}],"component_id":1}],"catchments":[{"component_data":"Unable to find component","component_id":12}]}],"markets":[{"component_data":[{"attribute_id":"price","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[3.01e-05,3.02e-05]}},{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1.1e06,1.2e06]}},{"attribute_id":"buy","data":"not found"},{"attribute_id":"sale","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1.1e06,1.2e06]}},{"attribute_id":"ts.planned_revenue","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[1.1e07,1.2e07]}}],"component_id":1}],"power_modules":[{"component_data":[{"attribute_id":"power.schedule","data":"not found"}],"component_id":54}],"networks":[{"component_id":50,"transmission_lines":[{"component_data":[{"attribute_id":"capacity","data":"not found"}],"component_id":51}],"busbars":[{"component_data":[{"attribute_id":"dummy","data":"not found"}],"component_id":52}]}],"unit_groups":[{"component_id":1,"component_data":[{"attribute_id":"group_type","data":1},{"attribute_id":"obligation.schedule","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0.0,2.0e06]}},{"attribute_id":"production","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0.0,2.0e07]}},{"attribute_id":"flow","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0.0,21.0]}}],"members":[{"component_data":[{"attribute_id":"active","data":{"id":"","pfx":true,"time_axis":{"t0":0.0,"dt":3600.0,"n":2},"values":[0.0,1.0]}}],"component_id":1}]}]}})_",
                                R"_({"request_id":"3","result":[{"model_key":"simple","id":2,"name":"simple"},{"model_key":"sorland","id":1,"name":"stm_system"}]})_",
                                R"_({"request_id":"4","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"id":1,"name":"simple_res"}],"units":[{"id":1,"name":"simple_unit"}],"power_plants":[{"id":2,"name":"simple_pp","units":[1]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]}]}})_",
                                R"_({"request_id":"5","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"id":1,"name":"simple_res","set_attrs":["level.result","inflow.schedule"]}],"units":[{"id":1,"name":"simple_unit","set_attrs":["production.result","discharge.result","discharge.constraint.min"]}],"power_plants":[{"id":2,"name":"simple_pp","units":[1],"set_attrs":[]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}],"set_attrs":["discharge.result"]}]}})_",
                                R"_({"request_id":"6","result":{"model_key":"simple","values":[{"attribute_id":"n_inc_runs","data":2},{"attribute_id":"n_full_runs","data":3},{"attribute_id":"head_opt","data":true},{"attribute_id":"run_time_axis","data":{"t0":null,"dt":0.0,"n":0}},{"attribute_id":"fx_log","data":[]}]}})_",
        #ifdef SHYFT_WITH_SHOP
                                R"_({"request_id":"7","result":[]})_",
        #else
                                R"_({"request_id":"7","result":"Shyft is not built with SHOP, and therefore unable to get shop log"})_",
        #endif
                                };
        REQUIRE_EQ(expected.size(), responses.size());
        
        
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }

    TEST_CASE("set_attributes") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem
        // However, this directory is required, but never used in this test.

        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        // Store some models:
        auto mdl = test::web_api::create_stm_system();
        a.do_add_model("sorland", mdl);

        auto mdl2 = test::web_api::create_simple_system(2,"simple");
        add_market(mdl2);
        add_unit_group(mdl2);
        add_network_and_power_modules(mdl2);
        a.do_add_model("simple", mdl2);

        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{R"_(set_attributes {"request_id": "1",
                                        "model_key": "simple",
                                        "hps": [
                                                {
                                                    "hps_id": 1,
                                                    "reservoirs": [
                                                        {
                                                            "component_id": 1,
                                                            "attribute_data": [
                                                                {
                                                                    "attribute_id": "level.regulation_min",
                                                                    "value": {
                                                                        "id": "abcd",
                                                                        "pfx": true,
                                                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                                        "values": [0.1, 0.2, null, 0.4]
                                                                    }
                                                                },
                                                                {
                                                                    "attribute_id": "volume_level_mapping",
                                                                    "value": {
                                                                        "1969-12-31T23:00:00Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)],
                                                                        "1970-01-01T00:00:02Z": [(1.0, 1.0), (1.5, 2.0)]
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ],
                                                    "units": [
                                                        {
                                                            "component_id": 1,
                                                            "attribute_data": [
                                                                {
                                                                    "attribute_id": "production.result",
                                                                    "value": {
                                                                        "1970-01-01T00:00:00Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)],
                                                                        "1970-01-01T00:00:01Z": [(1.0, 1.0), (1.5, 2.0)]
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ],
                                                    "gates": [
                                                        {
                                                            "component_id": 1,
                                                            "attribute_data": [
                                                                {
                                                                    "attribute_id": "opening.schedule",
                                                                    "value": {
                                                                        "id": "abcd",
                                                                        "pfx": true,
                                                                        "time_axis": {"time_points": [0, 2, 3, 4, 5]},
                                                                        "values": [0.1, 0.2, null, 0.4]
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ],
                                    "markets": [
                                        { 
                                          "component_id": 1,
                                          "attribute_data": [
                                               {
                                                    "attribute_id": "price",
                                                    "value": {
                                                        "id": "abcd",
                                                        "pfx": false,
                                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                        "values": [1000.0, 2000.0, 3000.0, 4000.0]
                                                    }
                                               }
                                          ]
                                        }
                                    ],
                                    "power_modules": [
                                        { 
                                          "component_id": 54,
                                          "attribute_data": [
                                               {
                                                    "attribute_id": "power.schedule",
                                                    "value": {
                                                        "id": "abcd",
                                                        "pfx": false,
                                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                        "values": [1111.0, 2222.0, 3333.0, 4444.0]
                                                    }
                                               }
                                          ]
                                        }
                                    ],
                                    "networks": [
                                        { 
                                          "component_id": 50,
                                          "transmission_lines" : [
                                            { 
                                                "component_id": 51,
                                                "attribute_data": [
                                                    {
                                                            "attribute_id": "capacity",
                                                            "value": {
                                                                "id": "abcd",
                                                                "pfx": false,
                                                                "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                                "values": [1.0, 2.0, 3.0, 4.0]
                                                            }
                                                    }
                                                ]
                                             }      
                                          ],
                                          "busbars" : [
                                            { 
                                                "component_id": 52,
                                                "attribute_data": [
                                                    {
                                                            "attribute_id": "dummy",
                                                            "value": {
                                                                "id": "abcd",
                                                                "pfx": false,
                                                                "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                                "values": [1.0, 2.0, 3.0, 4.0]
                                                            }
                                                    }
                                                ]
                                             }      
                                          ]
                                        }
                                    ],
                                    "unit_groups": [
                                        { 
                                          "component_id": 1,
                                          "attribute_data": [
                                               {
                                                    "attribute_id": "obligation.schedule",
                                                    "value": {
                                                        "id": "abcd",
                                                        "pfx": false,
                                                        "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                        "values": [1000.0, 2000.0, 3000.0, 4000.0]
                                                    }
                                               }
                                          ],
                                          "members" : [
                                            { 
                                                "component_id": 1,
                                                "attribute_data": [
                                                    {
                                                            "attribute_id": "active",
                                                            "value": {
                                                                "id": "abcd",
                                                                "pfx": false,
                                                                "time_axis": {"time_points": [1, 2, 3, 4, 5]},
                                                                "values": [1.0, 1.0, 0.0, 1.0]
                                                            }
                                                    }
                                                ]
                                             }      
                                          ]
                                        }
                                    ]
                                    
                                    }
                                )_",
                                R"_(read_attributes {"request_id": "2",
                                        "model_key": "simple",
                                        "time_axis": {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                        "hps": [{
                                            "hps_id": 1,
                                            "reservoirs": [{
                                                "component_id": 1,
                                                "attribute_ids": ["level.regulation_min", "volume_level_mapping"]
                                            }]
                                        }]                                    
                                })_",
                                R"_(set_attributes {"request_id": "A4",
                                        "model_key": "simple",
                                        "hps":[{
                                        "hps_id": 1,
                                        "reservoirs": [
                                            {
                                                "component_id": 1,
                                                "attribute_data": [
                                                    {
                                                        "attribute_id": "production.schedule",
                                                        "value": {
                                                        "id": "abcd",
                                                        "pfx" : true,
                                                        "time_axis": {"time_points": [946684800]},
                                                        "values" : [ 210 ]
                                                    }
                                                ]
                                            }
                                        ]}]
                                })_",
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            //cout<<"Starting client session\n";
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
            //cout<<"Stopping web_api\n";
        }
        vector<string> expected{
            R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"volume_level_mapping","status":"OK"},{"attribute_id":"level.regulation_min","status":"OK"}]}],"units":[{"component_id":1,"status":[{"attribute_id":"production.result","status":"type mismatch"}]}],"gates":[{"component_id":1,"status":[{"attribute_id":"opening.schedule","status":"OK"}]}]}],"markets":[{"component_id":1,"status":[{"attribute_id":"price","status":"OK"}]}],"power_modules":[{"component_id":54,"status":[{"attribute_id":"power.schedule","status":"OK"}]}],"networks":[{"component_id":50,"transmission_lines":[{"component_id":51,"status":[{"attribute_id":"capacity","status":"OK"}]}],"busbars":[{"component_id":52,"status":[{"attribute_id":"dummy","status":"OK"}]}]}],"unit_groups":[{"component_id":1,"status":[{"attribute_id":"obligation.schedule","status":"OK"}],"members":[{"component_id":1,"status":[{"attribute_id":"active","status":"OK"}]}]}]}})_",
            R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"-3600.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]],"2.0":[[1.0,1.0],[1.5,2.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"time_points":[1.0,2.0,3.0,4.0,5.0]},"values":[0.1,0.2,null,0.4]}}],"component_id":1}]}]}})_",
            R"_(request_parse:time_axis::point_dt() needs at least two time-points)_"
                               };
        REQUIRE_EQ(expected.size(), responses.size());
        
        
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {

                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }


    TEST_CASE("dstm_fx") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test.fx." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a;
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();

        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{
            R"_(fx {"request_id": "1", "model_key": "simple","fx_arg":"{'optimize':'this'}"})_",
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
        }
        vector<string> expected{R"_({"request_id":"1","diagnostics":""})_",};
        REQUIRE_EQ(expected.size(), responses.size());
        FAST_CHECK_EQ(a.fx_mid(),"simple");
        FAST_CHECK_EQ(a.fx_arg(),"{'optimize':'this'}");
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                CHECK_EQ(responses[i], expected[i]);
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }
    
     TEST_CASE("merge_set") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test.merge." +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server a(doc_root);
        string host_ip{"127.0.0.1"};
        a.set_listening_ip(host_ip);
        a.start_server();
        
        auto mdl = test::web_api::create_simple_system(2,"simple");
        auto hps = mdl->hps[0];
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
        // Create some different types of  time series:
        auto ts1 = apoint_ts("shyft://test/ts1", apoint_ts(gta_t(0, 1, 5), 1.0, ts_point_fx::POINT_AVERAGE_VALUE));
        auto ts2 = apoint_ts("shyft://test/ts2",
            apoint_ts(gta_t({from_seconds(0.), from_seconds(1.), from_seconds(5.), from_seconds(6.)}),
            2.0, ts_point_fx::POINT_AVERAGE_VALUE));
        vector<apoint_ts> tsv = {ts1, ts2};
        rsv->level.regulation_min = apoint_ts(ts1.id());
        rsv->level.regulation_max = apoint_ts(ts2.id());
        a.dtss_do_store_ts(tsv, false, true);//no cache
        a.do_add_model("simple", mdl);
        int port=get_free_port();
        a.start_web_api(host_ip, port, doc_root, 1, 1);

        std::this_thread::sleep_for(std::chrono::milliseconds(700));
        REQUIRE_EQ(true,a.web_api_running());
        vector<string> requests{
            R"_(set_attributes {"request_id": "1",
                                        "model_key": "simple",
                                        "merge": true,
                                        "cache_on_write": false,
                                        "hps":[{
                                            "hps_id": 1,
                                            "reservoirs": [
                                                {
                                                    "component_id": 1,
                                                    "attribute_data": [
                                                        {
                                                            "attribute_id": "level.result",
                                                            "value": {
                                                                "id": "abcd",
                                                                "pfx": true,
                                                                "time_axis": {"time_points": [1,2,3,4,5]},
                                                                "values": [0.1, 0.2, null, 0.4]
                                                            }
                                                        }
                                                    ]
                                                }
                                            ]
                                        }]
                                })_",
            R"_(read_attributes {"request_id": "2",
                                        "model_key": "simple",
                                        "time_axis": {"t0" : 0, "dt": 1,"n":0},
                                        "read_period":[0,20000],
                                        "cache": false,
                                        "hps":[{
                                                "hps_id": 1,
                                                "reservoirs": [{
                                                    "component_id": 1,
                                                    "attribute_ids": ["level.result"]
                                                }]
                                        }]
                                })_",
            R"_(set_attributes {"request_id": "faulty_merge",
                "model_key": "simple",
                "merge": true,
                "cache_on_write": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_min",
                                    "value": {
                                        "id": "abcd",
                                        "pfx": true,
                                        "time_axis": {"time_points": [1.5, 2]},
                                        "values": [0.]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
            R"_(set_attributes {
                "request_id": "merge_proper",
                "model_key": "simple",
                "merge": true,
                "cache_on_write": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_max",
                                    "value": {
                                        "id"        : "abcd",
                                        "pfx"       : true,
                                        "time_axis" : {"time_points": [3.5, 4.5, 5.5]},
                                        "values"    : [-1., -1.]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
            R"_(read_attributes {
                "request_id": "read_merged",
                "model_key": "simple",
                "time_axis": {"time_points": [3.5, 4.0, 4.5, 5.0, 5.5, 6.0]},
                "cache": false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [{
                        "component_id": 1,
                        "attribute_ids": ["level.regulation_max"]
                    }]
               }]
            })_",
            R"_(set_attributes {
                "request_id": "no_merge",
                "model_key": "simple",
                "merge": false,
                "recreate": false,
                "cache_on_write":false,
                "hps":[{
                    "hps_id": 1,
                    "reservoirs": [
                        {
                            "component_id": 1,
                            "attribute_data": [
                                {
                                    "attribute_id": "level.regulation_max",
                                    "value": {
                                        "id"        : "abcd",
                                        "pfx"       : true,
                                        "time_axis" : {"time_points": [3.5, 4.5, 5.5]},
                                        "values"    : [3.0, 3.0]
                                    }
                                }
                            ]
                        }
                    ]
                }]
            })_",
            R"_(read_attributes {
                "request_id": "read_unmerged",
                "model_key": "simple",
                "time_axis": {"time_points": [2.0, 3.5, 5.5, 6.0]},
                "cache": false,
                "hps":[{
                        "hps_id": 1,
                        "reservoirs": [{
                            "component_id": 1,
                            "attribute_ids": ["level.regulation_max"]
                        }]
                }]
            })_"
        };
        vector<string> responses;
        size_t r=0;
        {
            boost::asio::io_context ioc;
            auto s1=std::make_shared<session>(ioc);
            s1->run(host_ip, port, requests[r],//
            [&responses,&r,&requests](string const&web_response)->string{
                    responses.push_back(web_response);
                    ++r;
                    return r >= requests.size()?string(""):requests[r];
                }
            );
            // Run the I/O service. The call will return when
            // the socket is closed.
            ioc.run();
            s1.reset();
        }
        vector<string> expected{R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.result","status":"OK"}]}]}]}})_",
            //R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.result","data":{"id":"","pfx":false,"time_axis":{"time_points":[0.0,1.0,2.0,3.0,4.0,3600.0,7200.0,10800.0,14400.0,18000.0,20000.0]},"values":[1.0,0.1,0.2,null,0.4,1.0,1.0,1.0,1.0,1.0]}}],"component_id":1}]}]}})_",
            R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.result","data":{"id":"","pfx":false,"time_axis":{"time_points":[0.0,1.0,2.0,3.0,4.0,3600.0,7200.0,10800.0,14400.0,18000.0,21600.0]},"values":[1.0,0.1,0.2,null,0.4,1.0,1.0,1.0,1.0,1.0]}}],"component_id":1}]}]}})_",
            R"_(request_parse:dtss_store: cannot merge with different ta type)_",
            R"_({"request_id":"merge_proper","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.regulation_max","status":"stored to dtss"}]}]}]}})_",
            R"_({"request_id":"read_merged","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.regulation_max","data":{"id":"","pfx":true,"time_axis":{"time_points":[3.5,4.0,4.5,5.0,5.5,6.0]},"values":[-1.0,-1.0,-1.0,2.0,2.0]}}],"component_id":1}]}]}})_",
            R"_({"request_id":"no_merge","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.regulation_max","status":"stored to dtss"}]}]}]}})_",
            R"_({"request_id":"read_unmerged","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.regulation_max","data":{"id":"","pfx":true,"time_axis":{"time_points":[2.0,3.5,5.5,6.0]},"values":[2.0,3.0,2.0]}}],"component_id":1}]}]}})_"
        };
        REQUIRE_EQ(expected.size(), responses.size());
        for(size_t i=0; i<expected.size(); i++) {
            if (!expected[i].empty()) {
                if(i==2) {
                    CHECK_EQ(true,responses[i].starts_with(expected[i]));
                }else {
                    CHECK_EQ(responses[i], expected[i]);
                }
            } else {
                CHECK_EQ(responses[i].size(), 10);
            }
        }
        a.stop_web_api();
    }

    TEST_CASE("attributes_from_dtss") {
        dlib::set_all_logging_levels(dlib::LNONE);
        //-- keep test directory, unique, and with auto-cleanup.
        auto dirname = "em.web_api.test.dtss" +to_string(to_seconds64(utctime_now()) );
        test::utils::temp_dir tmpdir(dirname.c_str());
        string doc_root=(tmpdir/"doc_root").string();
        //dir_cleanup wipe{tmpdir}; // note..: ~path raises a SIGSEGV fault when boost is built with different compile flags than shyft.
        // See also: https://stackoverflow.com/questions/19469887/segmentation-fault-with-boostfilesystem

        // Set up test server:
        using namespace shyft::energy_market::test;
        using std::vector;
        test_server srv(doc_root);
        string host_ip{"127.0.0.1"};
        srv.set_listening_ip(host_ip);
        srv.start_server();
        auto mdl = test::web_api::create_simple_system(2,"simple");
        auto hps = mdl->hps[0];
        // We set an expression as an attribute:
        auto tsv = mocks::mk_expressions("test");
        auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
        rsv->level.realised = apoint_ts(tsv[0].id()); // A terminal
        rsv->level.schedule = apoint_ts(tsv[1].id()); // An expression
        // rsv->inflow and rsv->level are set as local attributes. Here we let rsv->volume be an unbound expression
        string prefix = "dstm://Msimple";
        string lvl_url;
        lvl_url.reserve(20);
        lvl_url += prefix;

        {
            auto rbi = std::back_inserter(lvl_url);
            rsv->generate_url(rbi);
        }
        lvl_url += ".level.result";

        string inflow_url;
        inflow_url.reserve(20);
        inflow_url += prefix;
        {
            auto rbi = std::back_inserter(inflow_url);
            rsv->generate_url(rbi);
        }
        inflow_url += ".inflow.schedule";

        apoint_ts inflow( inflow_url );
        apoint_ts lvl( lvl_url );
        rsv->volume.result = inflow + lvl;
        
        auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
        auto u = std::dynamic_pointer_cast<unit>(pp->units[0]);
        u->production.result = apoint_ts(tsv[2].id()); // A terminal
        pp->production.schedule = apoint_ts(tsv[3].id()); // An expression
        srv.dtss_do_store_ts(tsv, true, true);
        
        srv.do_add_model("simple", mdl);
        SUBCASE("dstm_ts_url") {
            auto p = rsv->inflow.schedule.total_period();
            string ts_id1;
            ts_id1.reserve(20);
            ts_id1 += prefix;
            auto rbi = std::back_inserter(ts_id1);
            rsv->generate_url(rbi);
            ts_id1 += ".inflow.schedule";
            //rsv->inflow.schedule.generate_url(rbi);

            string ts_id2;
            ts_id2.reserve(20);
            ts_id2 += prefix;
            rbi = std::back_inserter(ts_id2);
            rsv->generate_url(rbi);
            ts_id2 += ".level.result";
            //rsv->level.result.generate_url(rbi);
            vector<string> ts_ids = {ts_id1, ts_id2};
            auto tsv = srv.dtss_read_callback(ts_ids, p);
            CHECK_EQ(rsv->inflow.schedule, tsv[0]);
            CHECK_EQ(rsv->level.result, tsv[1]);
        }

        SUBCASE("dstm_read_with_callbacks") {
            int port=get_free_port();
            srv.start_web_api(host_ip, port, doc_root, 1, 1);


            std::this_thread::sleep_for(std::chrono::milliseconds(700));
            REQUIRE_EQ(true,srv.web_api_running());
            vector<string> requests{R"_(read_attributes {"request_id": "1",
                                            "model_key": "simple",
                                            "time_axis": {"t0":1800,"dt": 3600, "n": 2},
                                            "hps":[{
                                                    "hps_id": 1,
                                                    "reservoirs": [{
                                                        "component_id": 1,
                                                        "attribute_ids": ["level.realised","level.schedule","volume.result"]
                                                    }],
                                                    "units": [{
                                                        "component_id": 1,
                                                        "attribute_ids": ["production.result"]
                                                    }],
                                                    "power_plants": [{
                                                        "component_id": 2,
                                                        "attribute_ids": ["production.schedule"]
                                                    }]
                                            }]
                                        })_",
                                    R"_(set_attributes {"request_id": "2",
                                            "model_key": "simple",
                                            "merge": false,
                                            "recreate": true,
                                            "hps":[{
                                                "hps_id": 1,
                                                "reservoirs": [
                                                    {
                                                        "component_id": 1,
                                                        "attribute_data": [
                                                            {
                                                                "attribute_id": "level.realised",
                                                                "value": {
                                                                    "id": "abcd",
                                                                    "pfx": true,
                                                                    "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                                    "values"    : [ 0.1, 0.1, 0.1, 0.1 ]
                                                                }
                                                            },
                                                            {
                                                                "attribute_id": "level.result",
                                                                "value": {
                                                                    "id": "abcd",
                                                                    "pfx": true,
                                                                    "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                                    "values"    : [ 0.2, 0.2, 0.2, 0.2 ]
                                                                }
                                                            },
                                                            {
                                                                "attribute_id": "inflow.schedule",
                                                                "value" : {
                                                                    "id": "abcd",
                                                                    "pfx": true,
                                                                    "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                                    "values"    : [ 0.3, 0.3, 0.3, 0.3 ]
                                                                }
                                                            },
                                                            {
                                                                "attribute_id": "volume.result",
                                                                "value" : {
                                                                    "id": "abcd",
                                                                    "pfx": true,
                                                                    "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                                    "values"    : [ 0.4, 0.4, 0.4, 0.4 ]
                                                                }
                                                            }
                                                        ]
                                                    }
                                                ]
                                           }]
                                    })_",
                                    R"_(read_attributes {"request_id": "3",
                                            "model_key": "simple",
                                            "time_axis": {"t0":1,"dt": 1,"n": 2},
                                            "hps":[{
                                                "hps_id": 1,
                                                "reservoirs": [{
                                                    "component_id": 1,
                                                    "attribute_ids": ["level.realised", "level.result", "volume.result"]
                                                }]
                                           }]
                                    })_",
            };
            vector<string> responses;
            size_t r=0;
            {
                boost::asio::io_context ioc;
                auto s1=std::make_shared<session>(ioc);
                s1->run(host_ip, port, requests[r],//
                [&responses,&r,&requests](string const&web_response)->string{
                        responses.push_back(web_response);
                        ++r;
                        return r >= requests.size()?string(""):requests[r];
                    }
                );
                // Run the I/O service. The call will return when
                // the socket is closed.
                ioc.run();
                s1.reset();
                //cout<<"Stopping web_api\n";
            }
            vector<string> expected{
            //R"_({"request_id":"1","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"pfx":true,"data":[[1800.0,-2000.0],[5400.0,-2000.0]]}},{"attribute_id":"level.schedule","data":{"pfx":true,"data":[[1800.0,-8000.0],[5400.0,-8000.0]]}},{"attribute_id":"volume.result","data":{"pfx":true,"data":[[1800.0,2.75],[5400.0,null]]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"production.result","data":{"pfx":true,"data":[[1800.0,-3000.0],[5400.0,-3000.0]]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":"production.schedule","data":{"pfx":true,"data":[[1800.0,-12000.0],[5400.0,-12000.0]]}}],"component_id":2}]}})_",
            //R"_({"request_id":"1","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"pfx":true,"data":[[1800.0,-2000.0],[5400.0,-2000.0]]}},{"attribute_id":"level.schedule","data":{"pfx":true,"data":[[1800.0,-8000.0],[5400.0,-8000.0]]}},{"attribute_id":"volume.result","data":{"pfx":false,"data":[[1800.0,2.5],[5400.0,3.0]]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"production.result","data":{"pfx":true,"data":[[1800.0,-3000.0],[5400.0,-3000.0]]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":"production.schedule","data":{"pfx":true,"data":[[1800.0,-12000.0],[5400.0,-12000.0]]}}],"component_id":2}]}})_",
            //R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"pfx":true,"data":[[1800.0,-2000.0],[5400.0,-2000.0]]}},{"attribute_id":"level.schedule","data":{"pfx":true,"data":[[1800.0,-8000.0],[5400.0,-8000.0]]}},{"attribute_id":"volume.result","data":{"pfx":false,"data":[[1800.0,2.5],[5400.0,3.0]]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"production.result","data":{"pfx":true,"data":[[1800.0,-3000.0],[5400.0,-3000.0]]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":"production.schedule","data":{"pfx":true,"data":[[1800.0,-12000.0],[5400.0,-12000.0]]}}],"component_id":2}]}]}})_",    
            R"_({"request_id":"1","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-2000.0,-2000.0]}},{"attribute_id":"level.schedule","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-8000.0,-8000.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":false,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[2.5,3.0]}}],"component_id":1}],"units":[{"component_data":[{"attribute_id":"production.result","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-3000.0,-3000.0]}}],"component_id":1}],"power_plants":[{"component_data":[{"attribute_id":"production.schedule","data":{"id":"","pfx":true,"time_axis":{"t0":1800.0,"dt":3600.0,"n":2},"values":[-12000.0,-12000.0]}}],"component_id":2}]}]}})_",
            R"_({"request_id":"2","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.realised","status":"stored to dtss"},{"attribute_id":"level.result","status":"OK"},{"attribute_id":"volume.result","status":"Time series is an expression. Cannot be set."},{"attribute_id":"inflow.schedule","status":"OK"}]}]}]}})_",
            //R"_({"request_id":"3","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"pfx":true,"data":[[1.0,0.1],[2.0,0.1]]}},{"attribute_id":"level.result","data":{"pfx":true,"data":[[1.0,0.2],[2.0,0.2]]}},{"attribute_id":"volume.result","data":{"pfx":true,"data":[[1.0,0.5],[2.0,0.5]]}}],"component_id":1}]}})_"
            //R"_({"request_id":"3","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"pfx":true,"data":[[1.0,0.1],[2.0,0.1]]}},{"attribute_id":"level.result","data":{"pfx":true,"data":[[1.0,0.2],[2.0,0.2]]}},{"attribute_id":"volume.result","data":{"pfx":true,"data":[[1.0,0.5],[2.0,0.5]]}}],"component_id":1}]}]}})_"
            R"_({"request_id":"3","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"level.realised","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":2},"values":[0.1,0.1]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":2},"values":[0.2,0.2]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":2},"values":[0.5,0.5]}}],"component_id":1}]}]}})_"
                
            };
            REQUIRE_EQ(expected.size(), responses.size());


            for(size_t i=0; i<expected.size(); i++) {
                if (!expected[i].empty()) {
                    CHECK_EQ(responses[i], expected[i]);
                } else {
                    CHECK_EQ(responses[i].size(), 10);
                }
            }
            srv.stop_web_api();
        }
    }
}
