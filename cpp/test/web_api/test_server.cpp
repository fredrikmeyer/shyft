#include "test_pch.h"
#include <test/web_api/test_server.h>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <boost/beast/websocket.hpp>
#include <boost/beast/core/multi_buffer.hpp>
#include <boost/beast/core/buffers_to_string.hpp>


namespace test {
namespace {
    unsigned short get_free_port() {
        using namespace boost::asio;
        io_service service;
        ip::tcp::acceptor acceptor(service, ip::tcp::endpoint(ip::tcp::v4(), 0));// pass in 0 to get a free port.
        return  acceptor.local_endpoint().port();
    }
}

test_server::test_server():server(
    [&](id_vector_t const &ts_ids,utcperiod p) ->ts_vector_t { return this->test_read_cb(ts_ids,p); },
    [&](std::string search_expression) -> vector<ts_info> { return this->test_find_cb(search_expression); },
    [&](const ts_vector_t& tsv)->void { this->test_store_cb(tsv); }
) {
    bg_server.srv=this;
}



int
test_server::start_web_api(string host_ip,int port,string doc_root,int fg_threads,int bg_threads,bool tls_only) {
    std::scoped_lock<decltype(x_srv)> sl(x_srv);
    if(web_srv.valid()) {
        throw std::runtime_error("start_web_api: already running");
    }
    bg_server.running=false;
    if(port<=0)
        port=get_free_port();
    web_srv= std::async(std::launch::async,
        [this,host_ip,port,doc_root,fg_threads,bg_threads,tls_only]()->int {
            return web_api::start_web_server(
                bg_server,
                host_ip,
                static_cast<unsigned short>(port),
                make_shared<string>(doc_root),
                fg_threads,
                bg_threads,
                tls_only
            );

        }
    );
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    } while(!bg_server.running);
    return port;
}

bool test_server::web_api_running() const {
    std::scoped_lock<decltype(x_srv)> sl(x_srv);
    return web_srv.valid();
}
void test_server::stop_web_api() {
    std::scoped_lock<decltype(x_srv)> sl(x_srv);
    if(web_srv.valid()) {
        std::raise(SIGINT);
        (void) web_srv.get();
    }
}

test_server::~test_server(){
    stop_web_api();
}

// when needed, do something here:
ts_vector_t test_server::test_read_cb(vector<string> const&,utcperiod const&) {
    // recordthe event here
    ts_vector_t r;
    // TODO: make result return
    return r;
}

vector<ts_info> test_server::test_find_cb(string ) {
    vector<ts_info> r;
    //TODO: fill in results
    return r;
}

void test_server::test_store_cb(ts_vector_t const& ) {
    //TODO: record the what happen
}


}
