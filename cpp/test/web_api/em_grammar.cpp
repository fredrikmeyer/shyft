#include "test_pch.h"
#include <shyft/web_api/web_api_grammar.h>
#include <shyft/web_api/energy_market/grammar.h>
#include <shyft/web_api/energy_market/grammar_type_rules.h>
#include <shyft/web_api/energy_market/request_handler.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/energy_market/stm/attribute_types.h>
#include <shyft/energy_market/constraints.h>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/variant.hpp>
#include <vector>
#include <map>
#include <algorithm>
#include "test_parser.h"
//#include <iostream>

using namespace shyft::core;
using std::vector;
using std::map;
using std::string;
using std::shared_ptr;
using std::pair;
using shyft::core::utctime;
namespace qi = boost::spirit::qi;
namespace phx = boost::phoenix;

using shyft::energy_market::hydro_power::point;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::hydro_power::xy_point_curve_with_z;
using shyft::energy_market::hydro_power::turbine_efficiency;
using shyft::energy_market::hydro_power::turbine_description;

using shyft::energy_market::core::absolute_constraint;
using shyft::energy_market::core::penalty_constraint;

using shyft::energy_market::stm::t_xy_;
using shyft::energy_market::stm::t_xyz_;
using shyft::energy_market::stm::t_xyz_list_;
using shyft::energy_market::stm::t_turbine_description_;
using xyz_list = std::vector<xy_point_curve_with_z>;
using shyft::time_series::dd::apoint_ts;


using shyft::web_api::energy_market::attribute_value_type;

namespace test {
    t_xy_ make_t_xy(const xy_point_curve & curve1, const xy_point_curve & curve2) {
        calendar utc;
        utctime t1 = utc.time(2018,2,18,1,2,3);
        utctime t2 = utc.time(2018,3, 1, 0, 0, 0);
        auto a = std::make_shared< std::map<utctime, std::shared_ptr<xy_point_curve> > >();
        (*a)[t1] = std::make_shared<xy_point_curve>(curve1);
        (*a)[t2] = std::make_shared<xy_point_curve>(curve2);
        return a;
    }
    
    t_xyz_ make_t_xyz(const xy_point_curve_with_z& curve1, const xy_point_curve_with_z& curve2){
        calendar utc;
        utctime t1 = utc.time(2018,2,18,1,2,3);
        utctime t2 = utc.time(2018,3, 1, 0, 0, 0);
        auto a = std::make_shared< std::map<utctime, std::shared_ptr<xy_point_curve_with_z> > >();
        (*a)[t1] = std::make_shared<xy_point_curve_with_z>(curve1);
        (*a)[t2] = std::make_shared<xy_point_curve_with_z>(curve2);
        return a;
    }
    
    t_xyz_list_ make_t_xyz_list(const vector<xy_point_curve_with_z>& curves){
        calendar utc;
        utctime t1 = utc.time(2018,2,18,1,2,3);
        auto a = std::make_shared< std::map<utctime, std::shared_ptr<xyz_list> > >();
        (*a)[t1] = std::make_shared<xyz_list>(curves);
        return a;
    }
    
    t_turbine_description_ make_t_turbine_description(const turbine_description& tur_descr){
        calendar utc;
        utctime t1 = utc.time(2018,2,18,1,2,3);
        auto a = std::make_shared< std::map<utctime, std::shared_ptr<turbine_description>>>();
        (*a)[t1] = std::make_shared<turbine_description>(tur_descr);
        return a;
    }
     
     struct check_equality : public boost::static_visitor<bool> {
 
        bool operator()(apoint_ts const& ts1, apoint_ts const& ts2) const {
            CHECK_EQ(ts1, ts2);
            return ts1 == ts2;
        }
        
        bool operator()(string const& s1, string const& s2) const {
            CHECK_EQ(s1, s2);
            return s1 == s2;
        }
         
        template<typename T>
        bool operator()(shared_ptr<map<utctime,shared_ptr<T>>> const& val1, shared_ptr<map<utctime,shared_ptr<T>>> const& val2) const {
            CHECK_EQ(val1->size(), val2->size());
            for (auto const& kv : *val1){
                auto t = kv.first;
                auto item_val = kv.second;
                CHECK_EQ(*item_val, *((*val2)[t]));
                if (*item_val != *((*val2)[t])) return false;
            }
            return true;
        }

        template<typename T>
        bool operator()(T const& val1, T const& val2) const {
            CHECK_EQ(val1, val2);
            return val1 == val2;
        }
                
        template<typename T, typename U>
        bool operator()(T const& /*value*/, U const& /*other*/) const {
            CHECK_EQ(true, false);
            return false;
        }
    };
        
}

TEST_SUITE("em_grammar") {
    using namespace shyft::time_series;
    using namespace shyft::time_axis;

    // Basic types used in tests
    xy_point_curve points1({1.0, 1.5, 2.0, 2.5, 3.0}, {3.0, 2.5, 2.0, 1.5, 1.0});
    xy_point_curve_with_z curve1(points1, 1.57);
    
    xy_point_curve points2({1.0, 1.5}, {1.0, 2.0});
    xy_point_curve_with_z curve2(points2, 0.88);
    vector<xy_point_curve_with_z> curves = {curve1, curve2};

    turbine_efficiency tur_eff1(curves, 5.0, 10.0, 10.0, 4.9, 10.1);
    turbine_description tur_descr1({tur_eff1});
    
    // time maps for tests:
    calendar utc;
    utctime t1 = utc.time(2018,2,18,1,2,3);
    utctime t2 = utc.time(2018,3, 1, 0, 0, 0);
    auto test_txy = test::make_t_xy(points1, points2);
    auto test_txyz = test::make_t_xyz(curve1, curve2);
    auto test_txyz_list = test::make_t_xyz_list(curves);
    auto test_ttur_descr = test::make_t_turbine_description(tur_descr1);
    apoint_ts test_ts("abcd",
    apoint_ts(
        generic_dt(vector<utctime>{from_seconds(1),from_seconds(2),from_seconds(3),from_seconds(4),from_seconds(5)}),
        vector<double>{0.1,0.2,shyft::nan,0.4},
        POINT_AVERAGE_VALUE)
    );
    apoint_ts default_ts;
    generic_dt test_ta(vector<utctime>{from_seconds(1),from_seconds(2),from_seconds(3),from_seconds(4),from_seconds(5)});

    vector<pair<utctime, string>> test_messages{{from_seconds(1), "a test message"}, {from_seconds(2), "another message"}};

    
    TEST_CASE("integer_list_grammar") {
        shyft::web_api::grammar::integer_list_grammar<const char*> int_list_;
        using std::vector;
        vector<int> test;
        auto ok = test::phrase_parser("[1, 2, 3, 4, 5]", int_list_, test);
        CHECK_EQ(true, ok);
        CHECK_EQ(test, vector<int>{1, 2, 3, 4, 5});
        
        // Check empty list:
        test = {};
        ok = test::phrase_parser(R"_([])_", int_list_, test);
        CHECK_EQ(true, ok);
        CHECK_EQ(test.size(), 0);
    }
    
    TEST_CASE("grammar/xy_point") {
        shyft::web_api::grammar::xy_point_grammar<const char*> xy_;
        shyft::energy_market::hydro_power::point a(1.0, 2.0);
        shyft::energy_market::hydro_power::point b;
        CHECK_EQ(true, test::phrase_parser(
        R"_(
                (1.0, 2.0)
        )_", xy_, b));
        CHECK_EQ(a, b);        
    }
    
    TEST_CASE("grammar/xy_point_list") {
        shyft::web_api::grammar::xy_point_list_grammar<const char*> xy_list_;
        xy_point_curve b;
        CHECK_EQ(true, test::phrase_parser(
        R"_(
                [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
        )_", xy_list_, b));
        CHECK_EQ(5, b.points.size());
        CHECK_EQ(points1, b);
    }
    
    TEST_CASE("grammar/xy_point_curve_with_z") {
        shyft::web_api::grammar::xyz_curve_grammar<const char*> xyz_curve_;
        
        xy_point_curve_with_z b;
        CHECK_EQ(true,
            test::phrase_parser(
            R"_(
                { "z": 1.57,
                  "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                }
            )_", xyz_curve_, b));
        CHECK_EQ(curve1, b);
    }
    
    TEST_CASE("grammar/turbine_efficiency") {
        shyft::web_api::grammar::turbine_efficiency_grammar<const char*> turbine_efficiency_;

        turbine_efficiency b;
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "production_min": 5.0,
                        "production_max": 10.0,
                        "production_nominal": 10.0,
                        "fcr_min": 4.9,
                        "fcr_max": 10.1,
                        "efficiency_curves": [
                            {"z": 1.57,
                             "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                            },
                            {"z": 0.88,
                             "points": [(1.0, 1.0), (1.5, 2.0)]        
                            }
                        ]
                })_", turbine_efficiency_, b)
        );
        CHECK_EQ(tur_eff1, b);                        
    }
    
    TEST_CASE("grammar/turbine_description") {
        shyft::web_api::grammar::turbine_description_grammar<const char*> turbine_description_;

        turbine_description b;
        CHECK_EQ(true,
                 test::phrase_parser(
                 R"_({
                    "turbine_efficiencies": [
                        {
                        "production_min": 5.0,
                        "production_max": 10.0,
                        "production_nominal": 10.0,
                        "fcr_min": 4.9,
                        "fcr_max": 10.1,
                        "efficiency_curves": [
                            {"z": 1.57,
                             "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                            },
                            {"z": 0.88,
                             "points": [(1.0, 1.0), (1.5, 2.0)]        
                            }
                        ]
                        }
                    ]
                }
                )_", turbine_description_, b)   
                );
        CHECK_EQ(tur_descr1, b);
    }
    
    TEST_CASE("grammar/t_map_xy_list") {
        shyft::web_api::grammar::t_xy_grammar t_map_;

        t_xy_ b;
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)],
                        "2018-03-01T00:00:00Z": [(1.0, 1.0), (1.5, 2.0)]
                })_", t_map_, b)
        );
        CHECK_EQ(*((*test_txy)[t1]), *((*b)[t1]));
        CHECK_EQ(*((*test_txy)[t2]), *((*b)[t2]));        
    }
    
    TEST_CASE("grammar/t_map_xyz_curve") {
        shyft::web_api::grammar::t_xyz_grammar t_map_;

        t_xyz_ b;
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": 
                            {"z": 1.57,
                             "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                            },
                        "2018-03-01T00:00:00Z":
                            {"z": 0.88,
                             "points": [(1.0, 1.0), (1.5, 2.0)]
                            }
                })_", t_map_, b)
        );
        CHECK_EQ(*((*test_txyz)[t1]), *((*b)[t1]));
        CHECK_EQ(*((*test_txyz)[t2]), *((*b)[t2]));        
    }

    TEST_CASE("grammar/t_map_xyz_curve_list") {
        // Set up parser for t_xyz_list:
        shyft::web_api::grammar::t_xyz_list_grammar t_map_;
        
        t_xyz_list_ b;
        // Parse and compare results:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": 
                            [{"z": 1.57,
                             "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                            },
                            {"z": 0.88,
                             "points": [(1.0, 1.0), (1.5, 2.0)]
                            }]
                })_", t_map_, b)
        );
        CHECK_EQ(2, (*b)[t1]->size());
        auto aitem = *((*test_txyz_list)[t1]);
        for (const auto& item : *((*b)[t1])) {
            auto match_it = std::find(aitem.begin(), aitem.end(), item);
            CHECK_NE(match_it, aitem.end());
        }
        
    }
    
    TEST_CASE("grammar/t_map_turbine_description") {
        // Set up parser for t_turbine_description:
        shyft::web_api::grammar::t_turbine_description_grammar t_map_;
        
        t_turbine_description_ b;
        // Parse and compare results:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": 
                            {
                                "turbine_efficiencies": [
                                    {
                                        "production_min": 5.0,
                                        "production_max": 10.0,
                                        "production_nominal": 10.0,
                                        "fcr_min": 4.9,
                                        "fcr_max": 10.1,
                                        "efficiency_curves": [
                                            {"z": 1.57,
                                            "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                                            },
                                            {"z": 0.88,
                                            "points": [(1.0, 1.0), (1.5, 2.0)]        
                                            }
                                        ]
                                    }
                                ]
                            }
                })_", t_map_, b)
        );
        CHECK_EQ(*((*test_ttur_descr)[t1]), *((*b)[t1]));
    }

    TEST_CASE("grammar/absolute_constraint") {
        shyft::web_api::grammar::absolute_constraint_grammar<const char*> ac_;
        absolute_constraint ac;
        // Check parsing:
        CHECK_EQ(true,
            test::phrase_parser(
                R"_({
                    "limit": {
                        "id"        : "abcd" ,
                        "pfx"       : true,
                        "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                        "values"    : [ 0.1, 0.2,null,0.4 ]
                    },
                    "flag": {
                        "id"        : "abcd",
                        "pfx"       : true,
                        "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5]},
                        "values"    : [ 0.1, 0.2, null, 0.4 ]
                    }
                })_",
                ac_, ac
                )
        );
        CHECK_EQ(ac, absolute_constraint(test_ts, test_ts));

    }

    TEST_CASE("grammar/penalty_constraint") {
        shyft::web_api::grammar::penalty_constraint_grammar<const char*> pc_;
        penalty_constraint pc;

        // Parse an compare result:
        CHECK_EQ(true,
            test::phrase_parser(
                R"_({
                    "limit": {
                        "id"        : "abcd" ,
                        "pfx"       : true,
                        "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                        "values"    : [ 0.1, 0.2,null,0.4 ]
                    },
                    "flag": {
                        "id"        : "abcd",
                        "pfx"       : true,
                        "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5]},
                        "values"    : [ 0.1, 0.2, null, 0.4 ]
                    },
                    "cost": {
                        "id"        : "abcd",
                        "pfx"       : true,
                        "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5]},
                        "values"    : [ 0.1, 0.2, null, 0.4 ]
                    },
                    "penalty": {
                        "id"        : "abcd",
                        "pfx"       : true,
                        "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5]},
                        "values"    : [ 0.1, 0.2, null, 0.4 ]
                    }
                })_",
                pc_, pc
            )
        );

        CHECK_EQ(pc, penalty_constraint(test_ts, test_ts, test_ts, test_ts));
    }
    
    TEST_CASE("grammar/timestamped_string") {
        shyft::web_api::grammar::t_str_grammar<const char*> t_str_;
        std::pair<utctime, string> a;

        CHECK_EQ(true,
                test::phrase_parser(R"_((1, "a test string"))_", t_str_, a));

        CHECK_EQ(from_seconds(1), a.first);
        CHECK_EQ("a test string", a.second);
    }

    TEST_CASE("grammar/attribute_value_type_parser") {
        // Set up parser:
        shyft::web_api::grammar::attribute_value_type_grammar<const char*> proxy_;
        
        attribute_value_type b;
        // Parse and compare:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                "id"        : "abcd" ,
                "pfx"       : true,
                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                "values"    : [ 0.1, 0.2,null,0.4 ]
                })_", proxy_, b)
        );
        boost::apply_visitor( test::check_equality(), b, (attribute_value_type) test_ts);

        // time axis:
        CHECK_EQ(true,
            test::phrase_parser(
                R"_({"time_points" : [1, 2, 3, 4, 5]})_",
                proxy_, b)
        );
        boost::apply_visitor( test::check_equality(), b, (attribute_value_type) test_ta);
        // int:
        CHECK_EQ(true,
            test::phrase_parser(
                R"_(103)_",
                proxy_, b)
        );
        uint16_t ans = 103;
        boost::apply_visitor(test::check_equality(), b, (attribute_value_type) ans);

        // string:
        CHECK(test::phrase_parser(
            "\"A string\"", proxy_, b
            ));
        boost::apply_visitor(test::check_equality(), b, (attribute_value_type) string("A string"));
        // bool:
        CHECK_EQ(true,
            test::phrase_parser(
                R"_(false)_", proxy_, b
            )
        );
        boost::apply_visitor(test::check_equality(), b, (attribute_value_type) false);

        // t_xy_:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)],
                        "2018-03-01T00:00:00Z": [(1.0, 1.0), (1.5, 2.0)]
                })_", proxy_, b)
        );
        
        boost::apply_visitor( test::check_equality(), b, (attribute_value_type) test_txy);

        // t_xyz_list:
        // Parse and compare results:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": 
                            [{"z": 1.57,
                             "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                            },
                            {"z": 0.88,
                             "points": [(1.0, 1.0), (1.5, 2.0)]
                            }]
                })_", proxy_, b)
        );
        boost::apply_visitor( test::check_equality(), b, (attribute_value_type) test_txyz_list);
        
        // t_turbine_description_:
        // Parse and compare results:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_({
                        "2018-02-18T01:02:03Z": 
                            {
                                "turbine_efficiencies": [
                                    {
                                        "production_min": 5.0,
                                        "production_max": 10.0,
                                        "production_nominal": 10.0,
                                        "fcr_min": 4.9,
                                        "fcr_max": 10.1,
                                        "efficiency_curves": [
                                            {"z": 1.57,
                                            "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                                            },
                                            {"z": 0.88,
                                            "points": [(1.0, 1.0), (1.5, 2.0)]      
                                            }
                                        ]
                                    }
                                ]
                            }
                })_", proxy_, b)
        );
        boost::apply_visitor(test::check_equality(), b, (attribute_value_type) test_ttur_descr);

        // messages:
        CHECK_EQ(true,
            test::phrase_parser(
                R"_([(1, "a test message"),(2, "another message")])_",
                proxy_, b)
        );

        boost::apply_visitor(test::check_equality(), b, (attribute_value_type) test_messages);
    }

    TEST_CASE("grammar/attribute_list") {
        // Set up parser:
        shyft::web_api::grammar::attribute_value_list_grammar<const char*> attr_list_;
        
        vector<attribute_value_type> b;
        // Parse and compare:
        CHECK_EQ(true,
                test::phrase_parser(
                R"_([
                        {
                            "id"        : "abcd" ,
                            "pfx"       : true,
                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                            "values"    : [ 0.1, 0.2,null,0.4 ]
                        },
                        {
                            "2018-02-18T01:02:03Z": 
                                [{"z": 1.57,
                                "points": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                                },
                                {"z": 0.88,
                                "points": [(1.0, 1.0), (1.5, 2.0)]
                                }]
                        }
                    ])_", attr_list_, b)
        );
        // Check elements:
        CHECK_EQ(b.size(), 2);
        test::check_equality visitor;
        vector<attribute_value_type> expected = {test_ts, test_txyz_list};
        auto all_equal = std::equal( b.begin(), b.end(),
                        expected.begin(),
                        boost::apply_visitor(visitor));
        CHECK_EQ(true, all_equal);
    }
}
