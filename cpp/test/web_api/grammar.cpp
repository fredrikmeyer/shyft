#include "test_pch.h"
#include <shyft/web_api/web_api_grammar.h>
#include "test_parser.h"

using namespace shyft::core;

namespace shyft::web_api {


}

namespace shyft::web_api::grammar {


}
using shyft::time_series::dd::apoint_ts;


using std::vector;
using std::string;
using shyft::core::utctime;
using namespace shyft::web_api;
using namespace shyft;
namespace qi=boost::spirit::qi;
namespace ka=boost::spirit::karma;
namespace phx=boost::phoenix;

TEST_SUITE("web_api_grammar") {

    TEST_CASE("quoted_string_grammar") {
        shyft::web_api::grammar::quoted_string_grammar<const char*> string_;
        string s;
        // 1. A simple string
        CHECK_EQ(true, test::phrase_parser("\"A simple string\"", string_, s));
        CHECK_EQ(s, "A simple string");

        // 2. An empty string:
        s.clear();
        CHECK_EQ(true, test::phrase_parser("\"\"", string_, s));
        CHECK_EQ(s, "");
        // 3. A string with escaped characters:
        s.clear();
        CHECK_EQ(true, test::phrase_parser("\"A \\\"string\\\" within a string\"", string_, s));
        CHECK_EQ(s, R"_(A "string" within a string)_");

        s.clear();
        CHECK_EQ(true, test::phrase_parser("\"A \\\"longer escaped string\\\" and an escaped \\\\\"", string_, s));
        CHECK_EQ(s, R"_(A "longer escaped string" and an escaped \)_");

        s.clear();
        CHECK_EQ(true, test::phrase_parser("\"A string with\\n a newline.\"", string_, s));
        CHECK_EQ(s, R"_(A string with\n a newline.)_");

    }

    TEST_CASE("utctime_grammar") {
        shyft::web_api::grammar::utctime_grammar<const char*> utctime_;
        utctime a,b,n;calendar utc;
        CHECK_EQ(true,test::parser("3600",utctime_,a));
        CHECK_EQ(true,test::parser("\"2018-02-18T01:02:03Z\"",utctime_,b));
        CHECK_EQ(true,test::parser("null",utctime_,n));

        CHECK_EQ(from_seconds(3600),a);
        CHECK_EQ(utc.time(2018,2,18,1,2,3),b);
        CHECK_EQ(n,shyft::core::no_utctime);
        CHECK_THROWS_AS(test::parser("\"2018-02+18T01:02:03Z\"",utctime_,b),std::runtime_error);

    }

    TEST_CASE("utcperiod_grammar") {
        shyft::web_api::grammar::utcperiod_grammar<const char*> utcperiod_;
        utcperiod a,b;calendar utc;
        CHECK_EQ(true,test::phrase_parser("[ 3600, \"1970-01-01T02:00:00Z\"]",utcperiod_,a));
        CHECK_EQ(a,utcperiod(from_seconds(3600),from_seconds(7200)));

    }

    TEST_CASE("read_ts_request_grammar") {
        shyft::web_api::grammar::read_ts_request_grammar<const char*> r_ts_req_;
        using shyft::web_api::read_ts_request;
        read_ts_request a;
        CHECK_EQ(true,test::phrase_parser(
        R"_(
        read {
            "request_id": "a",
            "read_period": ["2018-10-09T00:00:00Z", "2018-11-01T02:00:00Z"],
            "clip_period": ["2018-10-09T01:00:00Z", "2018-11-01T00:00:00Z"],
            "cache": true,
            "ts_ids":["a","b","c"]
        }
        )_"
        ,r_ts_req_,a));
        read_ts_request b;
        CHECK_EQ(true,test::phrase_parser(
        R"_(
        read {
            "request_id": "a",
            "read_period": ["2018-10-09T00:00:00Z", "2018-11-01T02:00:00Z"],
            "clip_period": ["2018-10-09T01:00:00Z", "2018-11-01T00:00:00Z"],
            "cache": true,
            "ts_ids":["a","b","c"],
            "subscribe": true
        }
        )_"
        ,r_ts_req_,b));

        calendar utc;
        read_ts_request e_a{
            std::string("a"),
            utcperiod(utc.time(2018,10,9),utc.time(2018,11,1,2)),
            utcperiod(utc.time(2018,10,9,1),utc.time(2018,11,1,0)),
            true,
            std::vector<std::string>{"a","b","c"},
            false
        };
        auto e_b{e_a};e_b.subscribe=true;
        CHECK_EQ(e_a,a);
        CHECK_EQ(e_b,b);
        read_ts_request c;
        CHECK_EQ(true,test::phrase_parser(
            R"_(
            read {
                "request_id": "a",
                "read_period": ["2018-10-09T00:00:00Z", "2018-11-01T02:00:00Z"],
                "clip_period": ["2018-10-09T01:00:00Z", "2018-11-01T00:00:00Z"],
                "cache": true,
                "ts_ids":["a","b","c"],
                "subscribe": true,
                "ts_fmt": true
            }
            )_"
            ,r_ts_req_,c));
        
        read_ts_request e_c{
            std::string("a"),
            utcperiod(utc.time(2018,10,9),utc.time(2018,11,1,2)),
            utcperiod(utc.time(2018,10,9,1),utc.time(2018,11,1,0)),
            true,
            std::vector<std::string>{"a","b","c"},
            true,
            true
        };
        CHECK_EQ(e_c,c);

    }

    TEST_CASE("find_ts_request_grammar") {
        shyft::web_api::grammar::find_ts_request_grammar<const char*> f_ts_req_;
        using shyft::web_api::find_ts_request;
        find_ts_request a;
        CHECK_EQ(true,test::phrase_parser(
        R"_(find { "request_id": "a", "find_pattern": "shyft://some/pattern"})_"
        ,f_ts_req_,a));
        find_ts_request e{
            std::string("a"),
            std::string("shyft://some/pattern")
        };
        CHECK_EQ(e,a);
    }

    TEST_CASE("web_ts_points_grammar") {
        shyft::web_api::grammar::ts_points_grammar<const char*> ts_points_;
        using shyft::web_api::grammar::ts_points;
        ts_points a;
        CHECK_EQ(true,test::phrase_parser(
            "[[1.0,0.1],[2.0,0.2]]"
            ,ts_points_
            ,a
            )
        );
        ts_points e{std::tuple<utctime,double>{from_seconds(1),0.1},std::tuple<utctime,double>{from_seconds(2),0.2}};
        CHECK_EQ(a,e);
    }
    TEST_CASE("web_ts_points_grammar_empty") {
        shyft::web_api::grammar::ts_points_grammar<const char*> ts_points_;
        using shyft::web_api::grammar::ts_points;
        ts_points a;
        CHECK_EQ(true,test::phrase_parser(
            "[]"
            ,ts_points_
            ,a
            )
        );

        CHECK_EQ(a.size(),0u);
    }
    TEST_CASE("web_ts_points_grammar_w_nan") {
        shyft::web_api::grammar::ts_points_grammar<const char*> ts_points_;
        using shyft::web_api::grammar::ts_points;
        ts_points a;
        CHECK_EQ(true,test::phrase_parser(
            "[[1.0,0.1],[2.0,null]]"
            ,ts_points_
            ,a
            )
        );
        ts_points e{std::tuple<utctime,double>{from_seconds(1.0),0.1},std::tuple<utctime,double>{from_seconds(2.0),shyft::nan}};
        CHECK_EQ(a[0],e[0]);
        CHECK_EQ(std::get<0>(a[1]),std::get<0>(e[1]));
        CHECK_EQ(false,std::isfinite(std::get<1>(a[1])));

    }
    TEST_CASE("time_axis_grammar") {
        shyft::web_api::grammar::time_axis_grammar<const char*> ta_dt_;
        shyft::time_axis::generic_dt a;
        CHECK_EQ(true,test::phrase_parser(
            R"_({"t0":1.0,"dt": 2.0,"n": 10})_"
            ,ta_dt_
            ,a
            )
        );
        shyft::time_axis::generic_dt e{from_seconds(1.0),from_seconds(2.0),10};
        CHECK_EQ(a,e);
        CHECK_THROWS(test::phrase_parser(
            R"_({"t0":1.0,"dt": 0,"n": 10})_"
            ,ta_dt_
            ,a
        ));
        CHECK_EQ(true,test::phrase_parser(
            R"_({"calendar" : "Europe/Oslo", "t0" : "2018-01-01T00:00:00Z", "dt" : 86400, "n" : 20})_"
            ,ta_dt_
            ,a
            )
        );
        auto osl=std::make_shared<shyft::core::calendar>("Europe/Oslo");
        e=shyft::time_axis::generic_dt{osl,calendar().time(2018,1,1),from_seconds(86400.0),20};
        CHECK_EQ(a,e);
        CHECK_THROWS(test::phrase_parser(
            R"_({"calendar" : "Europe/Oslo", "t0" : "2018-01-01T00:00:00Z", "dt" : 0, "n" : 20})_"
            ,ta_dt_
            ,a
            )
        );
        CHECK_EQ(true,test::phrase_parser(
            R"_({ "time_points" : [ 1, 2, 3, 4 ]})_"
            ,ta_dt_
            ,a
            )
        );
        e=shyft::time_axis::generic_dt{std::vector<utctime>{from_seconds(1.0),from_seconds(2.0),from_seconds(3.0),from_seconds(4.0)}};
        CHECK_EQ(a,e);
    }
    TEST_CASE("time_points_grammar") {
        shyft::web_api::grammar::time_points_grammar<const char*> pts_;
        vector<utctime> a;
        CHECK_EQ(true,test::phrase_parser(
            "[1.0,2.0,3.0]"
            ,pts_
            ,a
            )
        );
        vector<utctime> e{from_seconds(1),from_seconds(2),from_seconds(3)};
        CHECK_EQ(a,e);
    }

    TEST_CASE("ts_values_grammar") {
        shyft::web_api::grammar::ts_values_grammar<const char*> vl_;
        vector<double> a;
        CHECK_EQ(true,test::phrase_parser(
            "[1.0,null,3.0]"
            ,vl_
            ,a
            )
        );
        vector<double> e{1.0,shyft::nan,3.0};
        CHECK_EQ(a[0],e[0]);
        CHECK_EQ(a[2],e[2]);
        CHECK_EQ(false,std::isfinite(a[1]));
        a.clear();//hmm. turns out it does nothing at zero elements. could be problem
        CHECK_EQ(true,test::phrase_parser(
            "[]"
            ,vl_
            ,a
            )
        );
        CHECK_EQ(a.size(),0u);
    }

    TEST_CASE("apoint_ts_grammar") {
        using shyft::time_series::dd::apoint_ts;
        using namespace shyft::time_series;
        using namespace shyft::time_axis;
        shyft::web_api::grammar::apoint_ts_grammar<const char*> apoint_ts_;
        apoint_ts a;
        auto ok_parse=test::phrase_parser(
                R"_({
                "id"        : "abcd" ,
                "pfx"       : true,
                "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                "values"    : [ 0.1, 0.2,null,0.4 ]
                })_",
                apoint_ts_,a
            );
        CHECK_EQ(ok_parse,true);
        apoint_ts e("abcd",
                    apoint_ts(
                        generic_dt(vector<utctime>{from_seconds(1),from_seconds(2),from_seconds(3),from_seconds(4),from_seconds(5)}),
                        vector<double>{0.1,0.2,shyft::nan,0.4},
                        POINT_AVERAGE_VALUE
                    )
                   );
        auto ts_equal=e==a;
        CHECK_EQ(ts_equal,true);
        CHECK_EQ(a.id(),"abcd");
    }
    TEST_CASE("ats_vector_grammar") {
        using shyft::time_series::dd::apoint_ts;
        using shyft::time_series::dd::ats_vector;
        using namespace shyft::time_series;
        using namespace shyft::time_axis;
        shyft::web_api::grammar::ats_vector_grammar<const char*> atsv_;
        ats_vector a;
        auto ok_parse=test::phrase_parser(
                R"_([
                    {
                    "id"        : "abcd" ,
                    "pfx"       : true,
                    "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                    "values"    : [ 0.1, 0.2,null,0.4 ]
                    },
                    {
                    "id"        : "efgh" ,
                    "pfx"       : false,
                    "time_axis" : {"time_points" : [ 1, 2, 3, 4 ]},
                    "values"    : [ 0.1, 0.2,0.3 ]
                    }
                ])_",
                atsv_,a
            );
        CHECK_EQ(ok_parse,true);
        REQUIRE_EQ(a.size(),2u);
        apoint_ts e1("abcd",
                    apoint_ts(
                        generic_dt(vector<utctime>{from_seconds(1),from_seconds(2),from_seconds(3),from_seconds(4),from_seconds(5)}),
                        vector<double>{0.1,0.2,shyft::nan,0.4},
                        POINT_AVERAGE_VALUE
                    )
                   );
        apoint_ts e2("efgh",
                    apoint_ts(
                        generic_dt(vector<utctime>{from_seconds(1),from_seconds(2),from_seconds(3),from_seconds(4)}),
                        vector<double>{0.1,0.2,0.3},
                        POINT_INSTANT_VALUE
                    )
                   );
        auto ts1_equal=e1==a[0];
        CHECK_EQ(ts1_equal,true);
        auto ts2_equal=e2==a[1];
        CHECK_EQ(ts2_equal,true);
    }
    TEST_CASE("store_ts_grammar") {
        shyft::web_api::grammar::store_ts_request_grammar<const char*> sts_;
        using shyft::web_api::store_ts_request;
        using shyft::time_series::dd::ats_vector;
        store_ts_request a;
        CHECK_EQ(true,test::phrase_parser(
             R"_(store_ts {
                "request_id"  : "a_store_request_1",
                "merge_store" : false,
                "recreate_ts" : false,
                "cache"       : true,
                "tsv"         : [
                                {
                                    "id": "a",
                                    "pfx":true,
                                    "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                    "values": [1,2,3]
                                }
                             ,
                                {
                                    "id": "b",
                                    "pfx":false,
                                    "time_axis": { "t0": "2019-01-01T00:00:00Z","dt": 3600,"n":3 },
                                    "values": [4,5,6]
                                }
                            ]
            })_"
            ,sts_
            ,a
            )
        );
        CHECK_EQ(a.request_id,"a_store_request_1");
        CHECK_EQ(a.merge_store,false);
        CHECK_EQ(a.recreate_ts,false);
        CHECK_EQ(a.cache,true);
        CHECK_EQ(a.tsv.size(),2u);
    }


    TEST_CASE("web_api_ts_info_parse") {
        grammar::ts_info_grammar<const char *> tsi_;
        ts_info o;
        CHECK(
            grammar::phrase_parser(
                R"_(
                    {
                        "name": "abc.ts",
                        "pfx":false,
                        "delta_t": 3600,
                        "olson_tz_id": "Europe/Oslo",
                        "data_period":  [ "2010-01-01T00:00:00Z","2010-02-01T00:00:00Z"],
                        "created": "2021-10-01T00:00:00Z",
                        "modified": "2021-10-02T00:00:00Z"
                    }
                )_",
                tsi_,o
            )
        );
        calendar utc;
        ts_info e{"abc.ts",time_series::ts_point_fx::POINT_INSTANT_VALUE,from_seconds(3600),"Europe/Oslo",utcperiod(utc.time(2010,1,1),utc.time(2010,2,1)),utc.time(2021,10,1),utc.time(2021,10,2)};
        CHECK_EQ(e,o);
        CHECK(
            grammar::phrase_parser(
                R"_(
                    {
                        "name": "abc.ts",
                        "pfx":false,
                        "delta_t": 3600,
                        "olson_tz_id": "Europe/Oslo",
                        "data_period":  [ "2010-01-01T00:00:00Z","2010-02-01T00:00:00Z"],
                        "created": null,
                        "modified": "2021-10-02T00:00:00Z"
                    }
                )_",
                tsi_,o
            )
        );
        e.created=shyft::core::no_utctime;
        CHECK_EQ(e,o);

    }
    TEST_CASE("ts_infos") {
        vector<ts_info> tsi;
        grammar::ts_infos_grammar<const char*> tsi_;
        CHECK(grammar::phrase_parser("[]",tsi_,tsi));
        CHECK_EQ(tsi.size(),0u);
        vector<ts_info> o;
        CHECK(
            grammar::phrase_parser(
                R"_([
                    {
                        "name": "abc.ts",
                        "pfx":false,
                        "delta_t": 3600,
                        "olson_tz_id": "Europe/Oslo",
                        "data_period":  [ "2010-01-01T00:00:00Z","2010-02-01T00:00:00Z"],
                        "created": "2021-10-01T00:00:00Z",
                        "modified": "2021-10-02T00:00:00Z"
                    }
                    ]
                )_",
                tsi_,o
            )
        );
        CHECK_EQ(o.size(),1);
    }

    TEST_CASE("request_reply_grammar") {
        request_reply rr;
        grammar::request_reply_grammar<const char*> rr_;
        CHECK(grammar::phrase_parser(R"_({"request_id":"abc","diagnostics":"def"})_",rr_,rr));
        CHECK_EQ(rr,request_reply{"abc","def"});
    }

    TEST_CASE("tsv_reply_grammar") {
        tsv_reply rr;
        grammar::tsv_reply_grammar<const char*> rr_;
        CHECK(grammar::phrase_parser(R"_({"request_id":"abc","tsv":[]})_",rr_,rr));
        CHECK_EQ(rr,tsv_reply{"abc",{}});
        CHECK(grammar::phrase_parser(R"_({"request_id":"abc","tsv":[{"id":"ts1","pfx":false,"time_axis":{"t0":0,"dt":10,"n":3},"values":[1,2,3]}]})_",rr_,rr));
        CHECK_EQ(rr.tsv.size(),1);
        CHECK_EQ(rr.request_id,"abc");
        apoint_ts aa{"abc",apoint_ts{time_axis::generic_dt{from_seconds(0),from_seconds(10),3},{1,2,3},time_series::ts_point_fx::POINT_INSTANT_VALUE}};
        CHECK_EQ(rr.tsv[0], aa);
    }

    TEST_CASE("web_api_reply") {
        grammar::web_reply rr;
        grammar::web_reply_grammar<const char*> rr_;
        CHECK(grammar::phrase_parser(R"_({"request_id":"abc","tsv":[]})_",rr_,rr));
        auto tsvr=boost::get<tsv_reply>(rr);
        CHECK_EQ(tsvr.request_id,"abc");

        CHECK(grammar::phrase_parser(R"_({"request_id":"def","result":[]})_",rr_,rr));
        auto fr=boost::get<find_reply>(rr);
        CHECK_EQ(fr.request_id,"def");

        CHECK(grammar::phrase_parser(R"_({"request_id":"err","diagnostics":"krmt"})_",rr_,rr));
        auto err=boost::get<request_reply>(rr);
        CHECK_EQ(err.request_id,"err");
        CHECK_EQ(err.ex_info,"krmt");

    }

    TEST_CASE("web_request_grammar") {
        // test the composition of requests

        shyft::web_api::grammar::web_request_grammar<const char*> web_req_;
        using shyft::web_api::grammar::web_request;
        using shyft::web_api::find_ts_request;
        using shyft::web_api::info_request;
        using shyft::web_api::read_ts_request;
        using shyft::web_api::average_ts_request;
        using shyft::web_api::percentile_ts_request;
        using shyft::web_api::store_ts_request;

        web_request a;
        SUBCASE("find") {
            CHECK_EQ(true,test::phrase_parser(
            R"_(find {"request_id":"a","find_pattern": "shyft://some/pattern"})_"
            ,web_req_,a));
            find_ts_request e{
                std::string("a"),
                std::string("shyft://some/pattern")
            };
            auto af=boost::get<find_ts_request>(a);
            CHECK_EQ(af,e);
        }
        SUBCASE("info") {
            CHECK_EQ(true,test::phrase_parser(
            R"_(info {"request_id":"a"})_"
            ,web_req_,a));
            info_request ei{
                std::string("a")
            };
            auto fi=boost::get<info_request>(a);
            CHECK_EQ(fi,ei);
        }
        SUBCASE("store") {
            CHECK_EQ(true,test::phrase_parser(
                 R"_(store_ts {
                    "request_id"  : "a_store_request_1",
                    "merge_store" : false,
                    "recreate_ts" : false,
                    "cache"       : true,
                    "tsv"         : [
                                    {
                                        "id": "a",
                                        "pfx":true,
                                        "time_axis": { "t0": "2018-01-01T00:00:00Z","dt": 3600,"n":3 },
                                        "values": [1,2,3]
                                    }
                                 ,
                                    {
                                        "id": "b",
                                        "pfx":false,
                                        "time_axis": { "t0": "2019-01-01T00:00:00Z","dt": 3600,"n":3 },
                                        "values": [4,5,6]
                                    }
                                ]
                })_"
                ,web_req_
                ,a
                )
            );
            auto aa=boost::get<store_ts_request>(a);
            CHECK_EQ(aa.request_id,"a_store_request_1");
            CHECK_EQ(aa.merge_store,false);
            CHECK_EQ(aa.recreate_ts,false);
            CHECK_EQ(aa.cache,true);
            CHECK_EQ(aa.tsv.size(),2u);
        }
        SUBCASE("read") {
            CHECK_EQ(true,test::phrase_parser(
                R"_(
                read {
                    "request_id": "a",
                    "read_period": ["2018-10-09T00:00:00Z", "2018-11-01T02:00:00Z"],
                    "clip_period": ["2018-10-09T01:00:00Z", "2018-11-01T00:00:00Z"],
                    "cache": true,
                    "ts_ids":["a","b","c"]
                }
                )_"
                ,web_req_,a)
            );
            auto aa = boost::get<read_ts_request>(a);
            calendar utc;
            read_ts_request e{
                std::string("a"),
                utcperiod(utc.time(2018,10,9),utc.time(2018,11,1,2)),
                utcperiod(utc.time(2018,10,9,1),utc.time(2018,11,1,0)),
                true,
                std::vector<std::string>{"a","b","c"},
                {}
                
            };
            CHECK_EQ(e,aa);
        }
        SUBCASE("speed_indicator") {
            auto t0=utctime_now();
            constexpr size_t n{10000};
            size_t success_count{0};
            for(size_t i=0;i<n;++i) {
                if(test::phrase_parser(
                    R"_(
                    read {
                        "request_id": "a",
                        "read_period": ["2018-10-09T00:00:00Z", "2018-11-01T02:00:00Z"],
                        "clip_period": ["2018-10-09T01:00:00Z", "2018-11-01T00:00:00Z"],
                        "cache": true,
                        "ts_ids":["a","b","c"]
                    }
                    )_"
                    ,web_req_,a) )
                ++success_count;
            }
            CHECK_EQ(success_count,n);
            auto dt=utctime_now()-t0;
            WARN_LE(to_seconds(dt),1.0);
            //std::cout<<"parses/sec:"<<static_cast<size_t>(double(n)/to_seconds(dt))<<std::endl;// it's like 1 mill parses/sec.
        }
    }
}
