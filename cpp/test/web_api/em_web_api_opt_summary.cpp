#include "test_pch.h"

#include <shyft/web_api/targetver.h>
#include <memory>
#include <dlib/logger.h>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/energy_market/constraints.h>

#include <shyft/core/fs_compat.h>
#include <shyft/energy_market/stm/unit_group.h>

#include "build_stm_system.h"
#include "mocks.h"
#include <shyft/energy_market/constraints.h>
#include <test/test_utils.h>
#include <csignal>

//#include <iostream>


using std::string;
using std::string_view;
using std::make_shared;
using std::vector;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::dd::gta_t;
using shyft::time_series::ts_point_fx;
using shyft::core::from_seconds;
using shyft::energy_market::core::absolute_constraint;
using namespace shyft::energy_market::stm;

namespace shyft::energy_market::test {
    using test_server=::mocks::dstm_server;

    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;

    extern unsigned short get_free_port();

 
    /** engine that perform a publish-subscribe against a specified host
     *
     * Same pattern as used in test for the dtss web_api (in cpp/test/web_api/web_server.cpp)
     */
    class opt_summary_session : public std::enable_shared_from_this<opt_summary_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        test_server* const srv; ///< Hold the server so we can use its dtss.
        int num_waits=0; ///<How many expected releases of subscribed read pattern
        // report a failure
        void fail(error_code ec, char const* what) {
            fail_ = string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec, diag) if((ec)) return fail((ec), (diag));
    public:
        // Resolver and socket require an io_context
        explicit opt_summary_session(boost::asio::io_context& ioc, test_server* const srv): resolver_(ioc), ws_(ioc), srv{srv} {}
        vector<string> responses_;
        string diagnostics() const { return fail_; }

        // Start the asynchronous operation
        void run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_, port_, // Look up the domain name
                                    [me=shared_from_this()](error_code ec, tcp::resolver::results_type results) {
                                        me->on_resolve(ec, results);
                                    }
                      );
        }

        void on_resolve(error_code ec, tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve");
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(), results.begin(), results.end(),
                                       std::bind(&opt_summary_session::on_connect, shared_from_this(), std::placeholders::_1)
                         );
        }

        void on_connect(error_code ec){
            fail_on_error(ec, "connect");
            ws_.async_handshake(host_, "/", // Perform websocket handshake
                [me=shared_from_this()](error_code ec) {
                    me->send_initial(ec);
                }
            );
        }

        void send_initial(error_code ec) {
            fail_on_error(ec, "send_initial");
            ws_.async_write(
                boost::asio::buffer(R"_(opt_summary {"request_id": "initial", "model_key": "simple", "subscribe": true})_"),
                [me=shared_from_this()](error_code ec, size_t bytes_transferred){
                    me->start_read(ec, bytes_transferred);
                }
            );
        }

        void start_read(error_code ec, size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "start_read");
            ws_.async_read(buffer_,
                        [me=shared_from_this()](error_code ec2, size_t bytes_transferred2){
                            me->on_read(ec2, bytes_transferred2);
                        }
                    );
        }

        void on_read(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read");
            string response = boost::beast::buffers_to_string(buffer_.data());
            responses_.push_back(response);
            //std::cout << "RUNPARAMS Got response: " << response << "\n";
            buffer_.consume(buffer_.size());
            if(response.find("finale") != string::npos) {
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) { me->on_close(ec); }
                    );
            } else {
                if (response.find("initial")!=string::npos) {
                    if (num_waits == 0) { // First time we update a model. Here via a simple notify change
                        ++num_waits;
                        auto mdl = srv->do_get_model("simple");
                        auto & pa = mdl->summary->total;
                        pa = 3;
                        string pa_url;
                        pa_url.reserve(30);
                        pa_url += "dstm://Msimple";
                        {
                            auto rbi = std::back_inserter(pa_url);
                            mdl->summary->generate_url(rbi);
                            *rbi++ = '.';
                        }
                        pa_url += "total";
                        std::cout << "Trying to notify '" << pa_url << "'\n";
                        srv->sm_notify_change(pa_url);
                    } else {
                        ws_.async_write(
                            boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"initial"})_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    }
                }

                //-- anyway, always continue to read (unless we hit the final request-id sent with the unsubscribe message
                ws_.async_read(buffer_,
                    [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                        me->on_read(ec, bytes_transferred);
                    }
                );
            }
        }

        void on_close(error_code ec) {
            fail_on_error(ec, "close");
        }

    #undef fail_on_error
    };
}

using std::to_string;
using shyft::core::utctime;



TEST_SUITE("em_web_api") {

    TEST_CASE("opt_summary_subscribe") {
        using namespace shyft::energy_market::test;
        string host_ip{"127.0.0.1"};
        int port = get_free_port();
        test::utils::temp_dir tmp_dir("shyft.energy_market.web_api.sub.opt.");
        string doc_root = (tmp_dir/"doc_root").string();
        shyft::energy_market::test::test_server srv(doc_root);
        srv.set_listening_ip(host_ip);
        //REQUIRE(srv.dtss != nullptr);
        srv.start_server();
        try {
            // Store a simple model that has some attributes
            auto mdl = test::web_api::create_simple_system(2,"simple");
            mdl->run_params.n_inc_runs = 2;
            mdl->run_params.n_full_runs = 3;
            mdl->run_params.head_opt = true;

            srv.do_add_model("simple", mdl);

            srv.start_web_api(host_ip, port, doc_root, 1, 1);
            REQUIRE_EQ(true, srv.web_api_running());
            std::this_thread::sleep_for(std::chrono::milliseconds(700));
            boost::asio::io_context ioc;
            auto s1 = std::make_shared<opt_summary_session>(ioc, &srv);
            s1->run(host_ip, port);
            ioc.run();
            // Set up expected responses and comparisons.
            vector<string> expected_responses{
                string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"reservoir.end_value","data":null},{"attribute_id":"reservoir.sum_ramping_penalty","data":null},{"attribute_id":"reservoir.sum_limit_penalty","data":null},{"attribute_id":"reservoir.end_limit_penalty","data":null},{"attribute_id":"reservoir.hard_limit_penalty","data":null},{"attribute_id":"waterway.vow_in_transit","data":null},{"attribute_id":"waterway.sum_discharge_fee","data":null},{"attribute_id":"waterway.discharge_group_penalty","data":null},{"attribute_id":"waterway.discharge_group_ramping_penalty","data":null},{"attribute_id":"gate.ramping_penalty","data":null},{"attribute_id":"gate.discharge_cost","data":null},{"attribute_id":"gate.discharge_constraint_penalty","data":null},{"attribute_id":"spill.cost","data":null},{"attribute_id":"spill.physical_cost","data":null},{"attribute_id":"spill.nonphysical_cost","data":null},{"attribute_id":"spill.physical_volume","data":null},{"attribute_id":"spill.nonphysical_volume","data":null},{"attribute_id":"bypass.cost","data":null},{"attribute_id":"ramping.ramping_penalty","data":null},{"attribute_id":"reserve.violation_penalty","data":null},{"attribute_id":"reserve.sale_buy","data":null},{"attribute_id":"reserve.obligation_value","data":null},{"attribute_id":"unit.startup_cost","data":null},{"attribute_id":"unit.schedule_penalty","data":null},{"attribute_id":"plant.production_constraint_penalty","data":null},{"attribute_id":"plant.discharge_constraint_penalty","data":null},{"attribute_id":"plant.schedule_penalty","data":null},{"attribute_id":"plant.ramping_penalty","data":null},{"attribute_id":"market.sum_sale_buy","data":null},{"attribute_id":"market.load_penalty","data":null},{"attribute_id":"market.load_value","data":null},{"attribute_id":"total","data":null},{"attribute_id":"sum_penalties","data":null},{"attribute_id":"minor_penalties","data":null},{"attribute_id":"major_penalties","data":null},{"attribute_id":"grand_total","data":null}]}})_"),
                string(R"_({"request_id":"initial","result":{"model_key":"simple","values":[{"attribute_id":"reservoir.end_value","data":null},{"attribute_id":"reservoir.sum_ramping_penalty","data":null},{"attribute_id":"reservoir.sum_limit_penalty","data":null},{"attribute_id":"reservoir.end_limit_penalty","data":null},{"attribute_id":"reservoir.hard_limit_penalty","data":null},{"attribute_id":"waterway.vow_in_transit","data":null},{"attribute_id":"waterway.sum_discharge_fee","data":null},{"attribute_id":"waterway.discharge_group_penalty","data":null},{"attribute_id":"waterway.discharge_group_ramping_penalty","data":null},{"attribute_id":"gate.ramping_penalty","data":null},{"attribute_id":"gate.discharge_cost","data":null},{"attribute_id":"gate.discharge_constraint_penalty","data":null},{"attribute_id":"spill.cost","data":null},{"attribute_id":"spill.physical_cost","data":null},{"attribute_id":"spill.nonphysical_cost","data":null},{"attribute_id":"spill.physical_volume","data":null},{"attribute_id":"spill.nonphysical_volume","data":null},{"attribute_id":"bypass.cost","data":null},{"attribute_id":"ramping.ramping_penalty","data":null},{"attribute_id":"reserve.violation_penalty","data":null},{"attribute_id":"reserve.sale_buy","data":null},{"attribute_id":"reserve.obligation_value","data":null},{"attribute_id":"unit.startup_cost","data":null},{"attribute_id":"unit.schedule_penalty","data":null},{"attribute_id":"plant.production_constraint_penalty","data":null},{"attribute_id":"plant.discharge_constraint_penalty","data":null},{"attribute_id":"plant.schedule_penalty","data":null},{"attribute_id":"plant.ramping_penalty","data":null},{"attribute_id":"market.sum_sale_buy","data":null},{"attribute_id":"market.load_penalty","data":null},{"attribute_id":"market.load_value","data":null},{"attribute_id":"total","data":3.0},{"attribute_id":"sum_penalties","data":null},{"attribute_id":"minor_penalties","data":null},{"attribute_id":"major_penalties","data":null},{"attribute_id":"grand_total","data":null}]}})_"),

                string(R"_({"request_id":"finale","subscription_id":"initial","diagnostics":""})_")
            };
            auto responses = s1->responses_;
            s1.reset();


            REQUIRE_EQ(responses.size(), expected_responses.size());
            for (auto i = 0u; i < responses.size(); ++i) {
                bool found_match=false; // order of responses might differ for the two last
                for(auto j=0u;j<responses.size() && !found_match;++j) {
                    found_match= responses[j]==expected_responses[i];
                }
                if(!found_match) {
                    MESSAGE("failed for the "<< i << "th response: "<<expected_responses[i]<<"!="<<responses[i]);
                    CHECK(found_match);
                }
            }
        } catch(...) {}
        srv.stop_web_api();
    }
}
