#include "test_pch.h"

#include <shyft/web_api/targetver.h>
#include <memory>
#include <dlib/logger.h>
#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_service.hpp>

#include <shyft/time_series/time_axis.h>
#include <shyft/time_series/dd/apoint_ts.h>
#include <shyft/time_series/dd/ats_vector.h>

#include <shyft/energy_market/stm/unit_group.h>
#include <shyft/energy_market/stm/unit.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/power_plant.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/constraints.h>

#include <shyft/core/fs_compat.h>

#include "build_stm_system.h"
#include "mocks.h"
#include <test/test_utils.h>
#include <csignal>

//#include <iostream>


using std::string;
using std::string_view;
using std::make_shared;
using std::vector;
using shyft::time_series::dd::ats_vector;
using shyft::time_series::dd::gta_t;
using shyft::time_series::ts_point_fx;
using shyft::core::from_seconds;
using shyft::energy_market::core::absolute_constraint;
using namespace shyft::energy_market::stm;

namespace shyft::energy_market::test {
    using test_server=::mocks::dstm_server;

    //-- test client
    using tcp = boost::asio::ip::tcp;               // from <boost/asio/ip/tcp.hpp>
    namespace websocket = boost::beast::websocket;  // from <boost/beast/websocket.hpp>
    using boost::system::error_code;
    // Sends a WebSocket message and prints the response, from examples made by boost.beast/Vinnie Falco

    extern unsigned short get_free_port();

    /** engine that perform a publish-subscribe against a specified host
     * 
     * Same pattern as used in test for the dtss web_api (in cpp/test/web_api/web_server.cpp)
     */
    class pub_sub_session : public std::enable_shared_from_this<pub_sub_session> {
        tcp::resolver resolver_;
        websocket::stream<tcp::socket> ws_;
        boost::beast::multi_buffer buffer_;
        string host_;
        string port_;
        string fail_;
        test_server* const srv; ///< Hold the server so we can use its dtss.
        int num_waits=0; ///<How many expected releases of subscribed read pattern
        // report a failure
        void fail(error_code ec, char const* what) {
            fail_ = string(what) + ": " + ec.message() + "\n";
        }
        #define fail_on_error(ec, diag) if((ec)) return fail((ec), (diag));
    public:
        // Resolver and socket require an io_context
        explicit pub_sub_session(boost::asio::io_context& ioc, test_server* const srv): resolver_(ioc), ws_(ioc), srv{srv} {}
        vector<string> responses_;
        string diagnostics() const { return fail_; }
        
        // Start the asynchronous operation
        void run(string_view host, int port) {
            // Save these for later
            host_ = host;
            port_ = std::to_string(port);
            resolver_.async_resolve(host_, port_, // Look up the domain name
                                    [me=shared_from_this()](error_code ec, tcp::resolver::results_type results) {
                                        me->on_resolve(ec, results);
                                    }
                      );
        }
        
        void on_resolve(error_code ec, tcp::resolver::results_type results) {
            fail_on_error(ec, "resolve");
            // Make the connection on the IP address we get from a lookup
            boost::asio::async_connect(ws_.next_layer(), results.begin(), results.end(),
                                       std::bind(&pub_sub_session::on_connect, shared_from_this(), std::placeholders::_1)
                         );
        }
        
        void on_connect(error_code ec){
            fail_on_error(ec, "connect");
            ws_.async_handshake(host_, "/", // Perform websocket handshake
                [me=shared_from_this()](error_code ec) {
                    me->send_read_and_subscribe(ec);
                }
            );
        }
        
        void send_read_and_subscribe(error_code ec) {
            fail_on_error(ec, "send_read_and_subscribe");
            ws_.async_write(
                boost::asio::buffer(R"_(
                    read_attributes {"request_id": "subscribe_and_read",
                                     "model_key": "simple",
                                     "time_axis": {"t0" : 1, "dt": 1,  "n": 4},
                                     "read_period": ["1970-01-01T00:00:00Z", "1970-01-01T03:00:00Z"],
                                     "subscribe": true,
                                     "cache": true,
                                     "hps": [{  "hps_id": 1,
                                                "reservoirs": [{
                                                    "component_id": 1,
                                                    "attribute_ids": ["level.regulation_min", "volume_level_mapping", "volume.result", "level.result","ts.included"]
                                                }],
                                                "gates": [{
                                                    "component_id": 1,
                                                    "attribute_ids": ["discharge.result"]
                                                }],
                                                "waterways": [{
                                                    "component_id": 1,
                                                     "attribute_ids": ["discharge.realised","ts.expr_diff"]
                                                }]
                                             }
                                     ],
                                    "markets":
                                        [{
                                          "component_id": 1,
                                          "attribute_ids": ["load","ts.expr_dtss","ts.expr_dstm"]
                                        }]
                                    
                                }
                )_"),
                [me=shared_from_this()](error_code ec, size_t bytes_transferred){
                    me->start_read(ec, bytes_transferred);
                }
            );
        }
        
        void start_read(error_code ec, size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "start_read");
            ws_.async_read(buffer_,
                        [me=shared_from_this()](error_code ec2, size_t bytes_transferred2){
                            me->on_read(ec2, bytes_transferred2);
                        }
                    );
        }
        
        void on_read(error_code ec, std::size_t bytes_transferred) {
            boost::ignore_unused(bytes_transferred);
            fail_on_error(ec, "read");
            string response = boost::beast::buffers_to_string(buffer_.data());
            responses_.push_back(response);
            buffer_.consume(buffer_.size());
            //std::cout << "PUBSUB Got response: " << response << "\n";
            if(response.find("finale") != string::npos) {
                //std::cout << "PUBSUB close \n";
                ws_.async_close(websocket::close_code::normal,
                                [me=shared_from_this()](error_code ec) { me->on_close(ec); }
                    );
            } else {
                if (response.find("subscribe_and_read")!=string::npos) {
                    //std::cout << "PUBSUB num_waits "<<num_waits<<std::endl;
                    if (num_waits == 0) { // First time we send an update to the model
                        ++num_waits;
                        ws_.async_write(// Send an update of the attribute
                            boost::asio::buffer(R"_(
                                set_attributes {"request_id": "change_attribute",
                                    "model_key": "simple",
                                    "hps":[{
                                        "hps_id": 1,
                                        "reservoirs": [
                                            {
                                                "component_id": 1,
                                                "attribute_data": [
                                                    {
                                                        "attribute_id": "volume_level_mapping",
                                                        "value" : {
                                                            "1970-01-01T00:00:01Z": [(1.0, 3.0), (1.5, 2.5), (2.0, 2.0), (2.5, 1.5), (3.0, 1.0)]
                                                        }
                                                    },
                                                    {
                                                        "attribute_id": "level.result",
                                                        "value" : {
                                                            "id"        : "abcd" ,
                                                            "pfx"       : true,
                                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                            "values"    : [ 3.0, 3.0, 3.0, 3.0 ]
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }]
                                })_"),
                                [me=shared_from_this()](error_code, size_t) {
                                    // Nothing to do here
                                }
                        );
                    } else if (num_waits == 1) {// Second time we send an update to the model
                        ++num_waits;
                        ws_.async_write(// We send an update to the reference to a time series stored in dtss which a subscribed attribute depends on
                            boost::asio::buffer(R"_(
                                set_attributes {"request_id": "second_change",
                                    "model_key": "simple",
                                    "hps":[{
                                        "hps_id": 1,
                                        "reservoirs": [
                                            {
                                                "component_id": 1,
                                                "attribute_data": [
                                                    {
                                                        "attribute_id": "level.schedule",
                                                        "value" : {
                                                            "id": "abcd",
                                                            "pfx": true,
                                                            "time_axis": {
                                                                "t0": 0.0,
                                                                "dt": 3600.0,
                                                                "n": 4
                                                            },
                                                            "values": [5.0, 5.0, 5.0, 5.0]
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }]
                                })_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    } else if (num_waits == 2) { // Third time we send an update to the model. This time using the dtss.
                        ++num_waits;
                        ats_vector tsv;
                        calendar utc;
                        auto t = utc.time(1970, 1, 1);
                        auto dt = shyft::core::deltahours(1);
                        int n = 10;
                        time_axis::fixed_dt ta(t, dt, n);
                        tsv.push_back(apoint_ts("shyft://test/ts4aa", apoint_ts(ta, 0.2, shyft::time_series::POINT_AVERAGE_VALUE))); //shyft::time_series::POINT_INSTANT_VALUE)));
                        
                        srv->dtss_do_store_ts(tsv, true, false);
                    } else if(num_waits==3) { // change ts-only, ref Ibrahim web-demo-case
                        ++num_waits;
                        ws_.async_write(// Send an update of the attribute
                            boost::asio::buffer(R"_(
                                set_attributes {"request_id": "ts_only_change",
                                    "model_key": "simple",
                                    "merge": true,
                                    "hps":[{
                                        "hps_id": 1,
                                        "reservoirs": [
                                            {
                                                "component_id" : 1,
                                                "attribute_data" : [
                                                    {
                                                        "attribute_id" : "level.regulation_min",
                                                        "value" : {
                                                            "id"        : "abcd" ,
                                                            "pfx"       : true,
                                                            "time_axis" : {"time_points" : [ 1, 2, 3, 4, 5 ]},
                                                            "values"    : [ 4.0, 4.0, 4.0, 4.0 ]
                                                        }
                                                    },
                                                    {
                                                        "attribute_id" : "ts.included",
                                                        "value" : {
                                                            "id"        : "abcd" ,
                                                            "pfx"       : true,
                                                            "time_axis": {"t0": 0.0, "dt": 3600.0,"n": 4},
                                                            "values"    : [ 1.0, 2.0, 3.0, 4.0 ]
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }]
                                })_"),
                                [me=shared_from_this()](error_code, size_t) {
                                    // Nothing to do here
                                }
                        );
                        
                    } else if (num_waits == 4) { // Third time we send an update to the model. This time using the dtss.
                        ++num_waits;
                        ats_vector tsv;
                        calendar utc;
                        auto t = utc.time(1970, 1, 1);
                        auto dt = shyft::core::deltahours(1);
                        int n = 10;
                        time_axis::fixed_dt ta(t, dt, n);
                        tsv.push_back(apoint_ts("shyft://test/ts4aa", apoint_ts(ta, 10.0, shyft::time_series::POINT_AVERAGE_VALUE)));// was POINT_INSTANT_VALUE
                        srv->dtss_do_store_ts(tsv, true, false);
                    } else {
                        ws_.async_write(
                            boost::asio::buffer(R"_(unsubscribe {"request_id":"finale", "subscription_id":"subscribe_and_read"})_"),
                            [me=shared_from_this()](error_code, size_t) {
                                // Nothing to do here
                            }
                        );
                    }
                }
                
                //-- anyway, always continue to read (unless we hit the final request-id sent with the unsubscribe message
                ws_.async_read(buffer_,
                    [me=shared_from_this()](error_code ec, size_t bytes_transferred) {
                        me->on_read(ec, bytes_transferred);
                    }
                );
            }
        }
        
        void on_close(error_code ec) {
            fail_on_error(ec, "close");
        }
        
    #undef fail_on_error
    };


}

using std::to_string;
using std::dynamic_pointer_cast;
using shyft::core::utctime;
using shyft::core::utctime_now;
using shyft::core::to_seconds64;



TEST_SUITE("em_web_api") {
    const double mega=1e6;
    const double eur_mega=1/1e6;
    constexpr auto add_market=[](stm_system_ const& mdl) {
            utctime t0{0};
            utctime dt=calendar::HOUR;
            gta_t ta(t0,dt,6);
            auto mkt= make_shared<energy_market_area>(1,"NOX","{}",mdl);
            mkt->price=apoint_ts(ta,vector<double>{30.1*eur_mega,30.2*eur_mega,31.3*eur_mega,31.4*eur_mega,31.5*eur_mega,31.6*eur_mega},ts_point_fx::POINT_AVERAGE_VALUE);
            mkt->load=apoint_ts(ta,vector<double>{1.1*mega,1.2*mega,1.3*mega,1.4*mega,1.5*mega,1.6*mega},ts_point_fx::POINT_AVERAGE_VALUE);
            mdl->market.push_back(mkt);
            return mkt;
    };

 
    TEST_CASE("em_web_api/publish_subscribe") {
        using namespace shyft::energy_market::test;
        string host_ip{"127.0.0.1"};
        int port = get_free_port();
        test::utils::temp_dir tmp_dir("shyft.energy_market.web_api.sub.");
        string doc_root = (tmp_dir/"doc_root").string();
        shyft::energy_market::test::test_server srv(doc_root);
        srv.set_listening_ip(host_ip);
        //REQUIRE(srv.dtss != nullptr);
        srv.start_server();
        try {
            // Store a simple model that has some attributes
            auto mdl = test::web_api::create_simple_system(2,"simple");
            auto mkt=add_market(mdl);

            auto hps = mdl->hps[0];
            // We set an expression as an attribute:
            auto tsv = mocks::mk_expressions("test");// get back a tsv with test/ts4aa,test/ts4,test/ts6aa,test/ts6, where the aa is a concrete ts, the non aa is a bound expr.
            
            auto rsv = std::dynamic_pointer_cast<reservoir>(hps->find_reservoir_by_id(1));
            auto wtr = std::dynamic_pointer_cast<waterway>(hps->find_waterway_by_id(1));
            // rsv->inflow and rsv->level are set as local attributes. Here we let rsv->volume be an unbound expression
            string inflow_url;
            inflow_url.reserve(30);
            inflow_url += "dstm://Msimple";
            {
                auto rbi = std::back_inserter(inflow_url);
                rsv->generate_url(rbi);
                inflow_url += ".inflow.schedule";
            }
            apoint_ts inflow( inflow_url);

            string lvl_url;
            lvl_url.reserve(30);
            lvl_url += "dstm://Msimple";
            {
                auto rbi = std::back_inserter(lvl_url);
                rsv->generate_url(rbi);
                lvl_url += ".level.result";
            }
            apoint_ts lvl(lvl_url);

            mkt->tsm["expr_dtss"] = 10.0*apoint_ts(tsv[0].id()); // expression as dtss ref.
            mkt->tsm["expr_dstm"] = 10.0*lvl; // expression as dstm ref.
            wtr->tsm["expr_diff"] = lvl/10.0; // another ref, at wtr level

            rsv->volume.result = (inflow + lvl)*apoint_ts(tsv[0].id()); // A more complicated expression, containing both local attributes and stored time series
            rsv->inflow.schedule = apoint_ts(tsv[0].time_axis(), 2.0, shyft::time_series::POINT_AVERAGE_VALUE);
            rsv->level.result = apoint_ts(tsv[0].time_axis(), 1.0, shyft::time_series::POINT_AVERAGE_VALUE);
            rsv->level.regulation_min = apoint_ts(tsv[0].time_axis(), -0.01, shyft::time_series::POINT_AVERAGE_VALUE); // Attribute not contained in an expression


            rsv->level.schedule = apoint_ts(tsv[0].id());
            rsv->level.constraint.min = apoint_ts(tsv[0].id()); // Reference to a time series stored in the dtss
            rsv->tsm["included"] = apoint_ts(tsv[0].id()); // Reference to a time series stored in the dtss, but user attr.

            auto pp = std::dynamic_pointer_cast<power_plant>(hps->find_power_plant_by_id(2));
            auto u = std::dynamic_pointer_cast<unit>(pp->units[0]);
            u->production.result = apoint_ts(tsv[2].id()); // A terminal
            pp->production.schedule = apoint_ts(tsv[3].id()); // An expression
            srv.dtss_do_store_ts(tsv, true, false);

            srv.do_add_model("simple", mdl);

            srv.start_web_api(host_ip, port, doc_root, 1, 1);
            REQUIRE_EQ(true, srv.web_api_running());
            std::this_thread::sleep_for(std::chrono::milliseconds(700));
            boost::asio::io_context ioc;
            auto s1 = std::make_shared<pub_sub_session>(ioc, &srv);
            s1->run(host_ip, port);
            ioc.run();
            // Set up expected responses and comparisons.
            vector<string> expected_responses{
                string(R"_({"request_id":"subscribe_and_read","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":"not found"},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-0.01,-0.01,-0.01,-0.01]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.0,1.0,1.0,1.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-6000.0,-6000.0,-6000.0,-6000.0]}},{"attribute_id":"ts.included","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-2000.0,-2000.0,-2000.0,-2000.0]}}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"ts.expr_diff","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.1,0.1,0.1,0.1]}}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[20.0,20.0,20.0,20.0]}}],"component_id":1}]}],"markets":[{"component_data":[{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.1e06,1.1e06,1.1e06,1.1e06]}},{"attribute_id":"ts.expr_dtss","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-20000.0,-20000.0,-20000.0,-20000.0]}},{"attribute_id":"ts.expr_dstm","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[10.0,10.0,10.0,10.0]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"change_attribute","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"volume_level_mapping","status":"OK"},{"attribute_id":"level.result","status":"OK"}]}]}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"1.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-0.01,-0.01,-0.01,-0.01]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[3.0,3.0,3.0,3.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-10000.0,-10000.0,-10000.0,-10000.0]}},{"attribute_id":"ts.included","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-2000.0,-2000.0,-2000.0,-2000.0]}}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"ts.expr_diff","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.3,0.3,0.3,0.3]}}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[20.0,20.0,20.0,20.0]}}],"component_id":1}]}],"markets":[{"component_data":[{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.1e06,1.1e06,1.1e06,1.1e06]}},{"attribute_id":"ts.expr_dtss","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-20000.0,-20000.0,-20000.0,-20000.0]}},{"attribute_id":"ts.expr_dstm","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[30.0,30.0,30.0,30.0]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"second_change","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.schedule","status":"stored to dtss"}]}]}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"1.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-0.01,-0.01,-0.01,-0.01]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[3.0,3.0,3.0,3.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[25.0,25.0,25.0,25.0]}},{"attribute_id":"ts.included","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[5.0,5.0,5.0,5.0]}}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"ts.expr_diff","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.3,0.3,0.3,0.3]}}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[20.0,20.0,20.0,20.0]}}],"component_id":1}]}],"markets":[{"component_data":[{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.1e06,1.1e06,1.1e06,1.1e06]}},{"attribute_id":"ts.expr_dtss","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[50.0,50.0,50.0,50.0]}},{"attribute_id":"ts.expr_dstm","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[30.0,30.0,30.0,30.0]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"1.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[-0.01,-0.01,-0.01,-0.01]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[3.0,3.0,3.0,3.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.0,1.0,1.0,1.0]}},{"attribute_id":"ts.included","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.2,0.2,0.2,0.2]}}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"ts.expr_diff","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.3,0.3,0.3,0.3]}}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[20.0,20.0,20.0,20.0]}}],"component_id":1}]}],"markets":[{"component_data":[{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.1e06,1.1e06,1.1e06,1.1e06]}},{"attribute_id":"ts.expr_dtss","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[2.0,2.0,2.0,2.0]}},{"attribute_id":"ts.expr_dstm","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[30.0,30.0,30.0,30.0]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"ts_only_change","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_id":1,"status":[{"attribute_id":"level.regulation_min","status":"OK"},{"attribute_id":"ts.included","status":"stored to dtss"}]}]}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"1.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[4.0,4.0,4.0,4.0]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[3.0,3.0,3.0,3.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[5.0,5.0,5.0,5.0]}},{"attribute_id":"ts.included","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.0,1.0,1.0,1.0]}}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"ts.expr_diff","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.3,0.3,0.3,0.3]}}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[20.0,20.0,20.0,20.0]}}],"component_id":1}]}],"markets":[{"component_data":[{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.1e06,1.1e06,1.1e06,1.1e06]}},{"attribute_id":"ts.expr_dtss","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[10.0,10.0,10.0,10.0]}},{"attribute_id":"ts.expr_dstm","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[30.0,30.0,30.0,30.0]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"subscribe_and_read","result":{"model_key":"simple","hps":[{"hps_id":1,"reservoirs":[{"component_data":[{"attribute_id":"volume_level_mapping","data":{"1.0":[[1.0,3.0],[1.5,2.5],[2.0,2.0],[2.5,1.5],[3.0,1.0]]}},{"attribute_id":"level.regulation_min","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[4.0,4.0,4.0,4.0]}},{"attribute_id":"level.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[3.0,3.0,3.0,3.0]}},{"attribute_id":"volume.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[5.0,5.0,5.0,5.0]}},{"attribute_id":"ts.included","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.0,1.0,1.0,1.0]}}],"component_id":1}],"waterways":[{"component_data":[{"attribute_id":"discharge.realised","data":"not found"},{"attribute_id":"ts.expr_diff","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[0.3,0.3,0.3,0.3]}}],"component_id":1}],"gates":[{"component_data":[{"attribute_id":"discharge.result","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[20.0,20.0,20.0,20.0]}}],"component_id":1}]}],"markets":[{"component_data":[{"attribute_id":"load","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[1.1e06,1.1e06,1.1e06,1.1e06]}},{"attribute_id":"ts.expr_dtss","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[10.0,10.0,10.0,10.0]}},{"attribute_id":"ts.expr_dstm","data":{"id":"","pfx":true,"time_axis":{"t0":1.0,"dt":1.0,"n":4},"values":[30.0,30.0,30.0,30.0]}}],"component_id":1}]}})_"),
                string(R"_({"request_id":"finale","subscription_id":"subscribe_and_read","diagnostics":""})_")
            };
            auto responses = s1->responses_;
            s1.reset();
            
        
            REQUIRE_EQ(responses.size(), expected_responses.size());
            for (auto i = 0u; i < responses.size(); ++i) {
                bool found_match=false; // order of responses might differ for the two last
                for(auto j=0u;j<responses.size() && !found_match;++j) {
                    found_match= responses[j]==expected_responses[i];
                }
                if(!found_match) {
                    MESSAGE("failed for the "<< i << "th response: "<<expected_responses[i]<<"!="<<responses[i]);
                    CHECK(found_match);
                }
            }
        } catch(...) {}
        srv.stop_web_api();
    }
}
