#include "test_pch.h"
#include "build_stm_system.h"
#include <boost/variant/apply_visitor.hpp>
#include <boost/optional/optional_io.hpp>

#include <shyft/web_api/json_struct.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/hydro_power/xy_point_curve.h>
#include <shyft/time/utctime_utilities.h>

using std::make_shared;
using std::vector;
using std::string;

using shyft::energy_market::stm::reservoir;
using shyft::web_api::energy_market::attribute_value_type;
using shyft::web_api::energy_market::attribute_value_compare;
using shyft::web_api::energy_market::json;

using shyft::energy_market::stm::t_xy_;
using shyft::energy_market::stm::t_xyz_;
using shyft::energy_market::stm::t_xyz_list_;
using shyft::energy_market::stm::t_turbine_description_;

using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::hydro_power::point;
TEST_SUITE("em_web_api") {
    auto hps = test::web_api::create_simple_hps(1, "test_hps");
    auto rsv = std::dynamic_pointer_cast<reservoir>(hps->reservoirs[0]);
    
    shyft::core::calendar cal;
    
    TEST_CASE("attribute_value_type_compare") {
        attribute_value_type p;
        auto a1 = rsv->inflow.schedule;
        p = a1;
        attribute_value_compare comp;
        CHECK_EQ(true, boost::apply_visitor(comp, p, (attribute_value_type) a1));
        CHECK_EQ(false, boost::apply_visitor(comp, p, (attribute_value_type) rsv->level.result)); // To a different set proxy attribute
        //CHECK_EQ(false, boost::apply_visitor(comp, p, (attribute_value_type) rsv->level.regulation_min));   // Compare with an unset variable
        // t_xy_
        t_xy_ a2;
        a2=make_shared<t_xy_::element_type>();
        (*a2)[cal.time(1970,1,1,0,0,3)]=make_shared<xy_point_curve>(vector<point>{point{1.0,2.0}});
        (*a2)[cal.time(1970,1,1,0,0,4)]=make_shared<xy_point_curve>(vector<point>{point{3.0,4.0}});
        CHECK_EQ(false, boost::apply_visitor(comp, p, (attribute_value_type) a2)); // Check different types
        p = a2;
        CHECK_EQ(true,boost::apply_visitor(comp, p, (attribute_value_type) a2));  // Check a t-map.
    }
    
    TEST_CASE("json") {
        json j;
        
        j["a"] = string("a test string");
        CHECK_EQ(boost::get<string>(j["a"]), "a test string");
        CHECK_EQ(boost::get<string>(j.required("a")), "a test string");
        CHECK_EQ(j.required<string>("a"), "a test string");
        CHECK_THROWS_AS(j.required<int>("a"), std::runtime_error);
        CHECK_THROWS_AS(j.required("b"), std::runtime_error);
        
        // We now recast j["a"] to be an int, checking that []-operator returns an lvalue
        j["a"] = 3;
        CHECK_EQ(boost::get<int>(j["a"]), 3);
        CHECK_EQ(j.required<int>("a"), 3);
        
        CHECK_EQ(3, *(j.optional<int>("a")));
        CHECK_EQ(boost::none, j.optional<string>("a")); // Correct key, but wrong type
        CHECK_EQ(boost::none, j.optional<int>("b"));    // Invalid key.
    }
    
}
