#include "test_pch.h"

#include <shyft/web_api/web_api_generator.h>
#include <shyft/web_api/energy_market/generators.h>
#include <shyft/web_api/energy_market/generators/hydro_power.h>
#include <shyft/web_api/energy_market/srv/generators.h>
#include <shyft/web_api/generators/proxy_attr.h>
#include <shyft/energy_market/constraints.h>
#include <shyft/energy_market/stm/stm_system.h>
#include <shyft/energy_market/stm/market.h>
#include <shyft/energy_market/stm/reservoir.h>
#include <shyft/energy_market/stm/waterway.h>
#include <shyft/energy_market/stm/unit.h>

#include "test_parser.h"

// play-ground for stuff that will be promoted once it's ok'
namespace shyft::web_api::generator {
}

namespace test::web_api {
    using namespace shyft::energy_market::hydro_power;
    using std::make_shared;
    using std::make_unique;

    inline hydro_power_system_ build_hps(string name) {
        auto hpsm = make_shared<hydro_power_system>(name);
        auto hps = make_unique<hydro_power_system_builder>(hpsm);
        auto r1 = hps->create_reservoir(1,"r1", R"({"info":"xxxr"})");
        auto r2 = hps->create_reservoir(2,"r2", R"({"reservoir_data":[100, 110, 10]})");
        auto r3 = hps->create_reservoir(3,"r3", R"({"reservoir_data":[100, 110, 10]})");
        auto o = hps->create_reservoir(4,"ocean",R"({"reservoir_data":[0, 0.01, 1e100]})");
        auto p1 = hps->create_unit (1,"the one",R"({"power_station_data":"..."})");
        auto p2 = hps->create_unit (2,"the other", R"({"power_station_data":"..."})");
        auto p = hps->create_power_plant (3,"pp","{}");
        power_plant::add_unit(p,p1);
        power_plant::add_unit(p,p2);

        auto t1 = hps->create_tunnel(1, "r1-p1", "");
        auto g1 = hps->create_gate(1, "2", "{\"3\":\"x\"}");
        waterway::add_gate(t1, g1);
        connect(t1).input_from(r1).output_to(p1);
        connect(hps->create_river(2,"r2-r1 river","")).input_from(r2).output_to(r1);
        connect(hps->create_river(3,"p to ocean", "")).input_from(p1).output_to(o);
        connect(hps->create_tunnel(4,"r3-p2", "")).input_from(r3).output_to(p2);
        connect(hps->create_river(5,"p2 to ocean", "")).input_from(p2).output_to(o);
        connect(hps->create_river(6,"r3-bypass ocean", "")).input_from(r3, connection_role::bypass).output_to(o);
        connect(hps->create_river(7,"r2-flood ocean", "")).input_from(r2, connection_role::flood).output_to(o);

        auto wms1 = hps->create_catchment(1,"9471-U", "some jason metadata");
        return hpsm;
    }
}

using namespace shyft::core;
using shyft::srv::model_info;
using shyft::energy_market::hydro_power::power_plant;
using shyft::energy_market::hydro_power::hydro_connection;
using shyft::energy_market::hydro_power::waterway;
using shyft::energy_market::hydro_power::reservoir;
using shyft::energy_market::hydro_power::unit;
using shyft::energy_market::hydro_power::connection_role;
using shyft::energy_market::hydro_power::hydro_component;

using std::string;using std::vector;using std::make_shared;
using shyft::energy_market::hydro_power::xy_point_curve;
using shyft::energy_market::stm::t_xy_;
using shyft::energy_market::stm::t_xyz_;
using shyft::energy_market::stm::t_xyz_list_;
using shyft::energy_market::stm::t_turbine_description_;
using shyft::energy_market::core::absolute_constraint;
using shyft::energy_market::core::penalty_constraint;

using namespace shyft::web_api::generator;
namespace shyft::web_api::generator {
template struct emit_object<std::back_insert_iterator<std::string>>;
}
TEST_SUITE("em/web_api/generator") {
TEST_CASE("em_double") {
    string ps;
    auto sink = std::back_inserter(ps);
    double a = 2.35;
    emit(sink, a);
    CHECK_EQ(ps, "2.35");
    a = 151946000000;
    ps.clear();
    emit(sink, a);
    CHECK_EQ(ps, "151946000000.0");

    ps.clear();
    a = 0.00000123;
    emit(sink, a);
    CHECK_EQ(ps, "0.00000123");
}
TEST_CASE("em_xy_generator") {
    string ps;
    auto sink=std::back_inserter(ps);
    xy_point a{1.0,2.0};
    auto ok=generate(sink,xy_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ("[1.0,2.0]",ps);

    vector<xy_point> aa;
    ps.clear();
    ok=generate(sink, xy_point_curve_generator<decltype(sink)>(),aa);
    REQUIRE(ok);
    CHECK_EQ(R"_([])_",ps);
    //ok=generate(sink,xyv_generator<decltype(sink)>(),aa);
    //REQUIRE(ok);
    //CHECK_EQ(R"_([[1.0,2.0],[3.0,4.0]])_",ps);
    ps.clear();
    aa.push_back(a);aa.push_back(xy_point{3.0,4.0});
    xy_point_curve aaa;aaa.points=aa;
    ok=generate(sink,xy_point_curve_generator<decltype(sink)>(),aa);
    REQUIRE(ok);
    CHECK_EQ(R"_([[1.0,2.0],[3.0,4.0]])_",ps);
}
TEST_CASE("em_xyz_generator") {
    string ps;
    auto sink=std::back_inserter(ps);
    xy_point_curve_with_z a;
    auto ok=generate(sink,xy_point_curve_with_z_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ(R"_({"z":0.0,"points":[]})_",ps);
    ps.clear();
    a.z=1.0;
    a.xy_curve.points.push_back(xy_point{3.0,4.0});
    ok=generate(sink,xy_point_curve_with_z_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ(R"_({"z":1.0,"points":[[3.0,4.0]]})_",ps);
    // step up to list
    xyz_point_curve_list aa;
    ps.clear();
    ok=generate(sink,xyz_point_curve_list_generator<decltype(sink)>(),aa);
    REQUIRE(ok);
    CHECK_EQ(R"_([])_",ps);
    aa.push_back(a);
    ps.clear();
    ok=generate(sink,xyz_point_curve_list_generator<decltype(sink)>(),aa);
    REQUIRE(ok);
    CHECK_EQ(R"_([{"z":1.0,"points":[[3.0,4.0]]}])_",ps);

}
TEST_CASE("em_turbine_eff_generator") {
    string ps;
    auto sink=std::back_inserter(ps);
    turbine_efficiency a;
    auto ok=generate(sink,turbine_efficiency_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ(R"_({"production_min":0.0,"production_max":0.0,"production_nominal":0.0,"fcr_min":0.0,"fcr_max":0.0,"efficiency_curves":[]})_",ps);
    ps.clear();
    a.production_nominal=100.0;
    a.production_max=100.0;
    a.production_min=9.0;
    a.fcr_max=101.0;
    a.fcr_min=9.1;
    a.efficiency_curves.push_back(xy_point_curve_with_z{});
    ok=generate(sink,turbine_efficiency_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ(R"_({"production_min":9.0,"production_max":100.0,"production_nominal":100.0,"fcr_min":9.1,"fcr_max":101.0,"efficiency_curves":[{"z":0.0,"points":[]}]})_",ps);
}

TEST_CASE("em_turbine_description_generator") {
    string ps;
    auto sink=std::back_inserter(ps);
    turbine_description a;
    auto ok=generate(sink,turbine_description_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ(R"_({"turbine_effiencies":[]})_",ps);
    ps.clear();
    a.efficiencies.push_back(turbine_efficiency{});
    ok=generate(sink,turbine_description_generator<decltype(sink)>(),a);
    REQUIRE(ok);
    CHECK_EQ(R"_({"turbine_effiencies":[{"production_min":0.0,"production_max":0.0,"production_nominal":0.0,"fcr_min":0.0,"fcr_max":0.0,"efficiency_curves":[]}]})_",ps);
}



TEST_CASE("model_info_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    model_info a{
        1,
        string("aname"),
        utc.time(2018,1,2,3,4,5),
        string("somejson")
    };
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_({"id":1,"name":"aname","created":1514862245.0,"json":"somejson"})_",ps);
}
TEST_CASE("em_t_xy_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xy_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xy_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,3)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{1.0,2.0}});
    (*a)[utc.time(1970,1,1,0,0,4)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{3.0,4.0}});
    emit(sink,a);
    CHECK_EQ(R"_({"3.0":[[1.0,2.0]],"4.0":[[3.0,4.0]]})_",ps);// a map, with two entries
}
TEST_CASE("em_t_xyz_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xyz_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xyz_::element_type>();
    (*a)[utc.time(1970,1,1,0,0,6)]=make_shared<xy_point_curve_with_z>(xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},1.0);
    (*a)[utc.time(1970,1,1,0,0,7)]=make_shared<xy_point_curve_with_z>(xy_point_curve{vector<xy_point>{xy_point{3.0,4.0}}},2.0);
    emit(sink,a);
    CHECK_EQ(R"_({"6.0":{"z":1.0,"points":[[1.0,2.0]]},"7.0":{"z":2.0,"points":[[3.0,4.0]]}})_",ps);// a map, with two entries
}
TEST_CASE("em_t_xyz_list_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_xyz_list_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_xyz_list_::element_type>();
    auto c1=make_shared<xyz_point_curve_list>();
    xy_point_curve_with_z e1{xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},3.0};
    c1->push_back(e1);
    (*a)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a);
    CHECK_EQ(R"_({"6.0":[{"z":3.0,"points":[[1.0,2.0]]}]})_",ps);
}

TEST_CASE("em_t_str_generator") {
    string ps;
    auto sink = std::back_inserter(ps);
    pair<utctime, string> a{from_seconds(1), "test message"};
    emit(sink, a);
    CHECK_EQ(R"_((1.0,"test message"))_", ps);
}


TEST_CASE("em_t_turbine_description_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    t_turbine_description_ a;
    emit(sink,a);
    CHECK_EQ(R"_(null)_",ps);
    ps.clear();
    a=make_shared<t_turbine_description_::element_type>();
    auto c1=make_shared<turbine_description>();
    c1->efficiencies.push_back(turbine_efficiency{});
    (*a)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a);
    CHECK_EQ(R"_({"6.0":{"turbine_effiencies":[{"production_min":0.0,"production_max":0.0,"production_nominal":0.0,"fcr_min":0.0,"fcr_max":0.0,"efficiency_curves":[]}]}})_",ps);

}

TEST_CASE("attribute_value_type_generator") {
    string ps;
    auto sink = std::back_inserter(ps);
    string var_ps;
    auto var_sink = std::back_inserter(var_ps);

    calendar utc;

    // t_xy_
    t_xy_ a2;
    // Emit regular:
    ps.clear();
    a2=make_shared<t_xy_::element_type>();
    (*a2)[utc.time(1970,1,1,0,0,3)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{1.0,2.0}});
    (*a2)[utc.time(1970,1,1,0,0,4)]=make_shared<xy_point_curve>(vector<xy_point>{xy_point{3.0,4.0}});
    emit(sink,a2);
    // Emit as variant:
    var_ps.clear();
    attribute_value_type b = a2;
    emit(var_sink, b);
    CHECK_EQ(var_ps, ps);

    // t_turbine_description_
    // Emit regular:
    t_turbine_description_ a3;
    ps.clear();
    a3=make_shared<t_turbine_description_::element_type>();
    auto c1=make_shared<turbine_description>();
    c1->efficiencies.push_back(turbine_efficiency{});
    (*a3)[utc.time(1970,1,1,0,0,6)]=c1;
    emit(sink,a3);
    // Emit as variant:
    b = a3;
    var_ps.clear();
    emit(var_sink,b);
    CHECK_EQ(var_ps, ps);

    // apoint_ts:
    // Emit regular:
    size_t n=10;
    gta_t ta(from_seconds(0),from_seconds(1),n);
    vector<double> v(n,1.2);
    v[1]= shyft::nan;
    apoint_ts a4(ta,v,ts_point_fx::POINT_AVERAGE_VALUE);

    ps.clear();
    emit(sink, a4);
    // Emit as variant:
    b = a4;
    var_ps.clear();
    emit(var_sink, b);
    CHECK_EQ(var_ps, ps);

    // t_xyz_list_:
    // Emit regular:
    t_xyz_list_ a5;
    ps.clear();
    a5=make_shared<t_xyz_list_::element_type>();
    auto c2=make_shared<xyz_point_curve_list>();
    xy_point_curve_with_z e1{xy_point_curve{vector<xy_point>{xy_point{1.0,2.0}}},3.0};
    c2->push_back(e1);
    (*a5)[utc.time(1970,1,1,0,0,6)]=c2;
    emit(sink,a5);

    // Emit as variant:
    b = a5;
    var_ps.clear();
    emit(var_sink, b);
    CHECK_EQ(var_ps, ps);

    // absolute_constraint:
    ps.clear();
    absolute_constraint a6;
    emit(sink, a6);
    CHECK_EQ(ps, R"_({"limit":{"pfx":false,"data":[]},"flag":{"pfx":false,"data":[]}})_");
    // Emit at
    b = a6;
    var_ps.clear();
    emit(var_sink, b);
    CHECK_EQ(ps, var_ps);

    // penalty constraint:
    ps.clear();
    penalty_constraint a7;
    emit(sink, a7);
    CHECK_EQ(ps, R"_({"limit":{"pfx":false,"data":[]},"flag":{"pfx":false,"data":[]},"cost":{"pfx":false,"data":[]},"penalty":{"pfx":false,"data":[]}})_");
    b = a7;
    var_ps.clear();
    emit(var_sink, b);
    CHECK_EQ(ps, var_ps);

}


TEST_CASE("model_info_vector_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    calendar utc;
    std::vector<model_info> a{
        model_info{
            1,
            string("aname"),
            utc.time(2018,1,2,3,4,5),
            string("somejson")
        },
        model_info{}
    };
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"#([{"id":1,"name":"aname","created":1514862245.0,"json":"somejson"},{"id":0,"name":"","created":null,"json":""}])#",ps);
    ps.clear();
    a.clear();
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ("[]",ps);
}

TEST_CASE("hyd_connect_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    hydro_connection a{connection_role::main,nullptr};
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_({"role":"main","target":null})_",ps);
    ps.clear();
    a.role=connection_role::input;
    a.target=std::make_shared<waterway>(123,"wx");
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_({"role":"input","target":"W123"})_",ps);
}
TEST_CASE("hyd_connect_vector_generator") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto wr=std::make_shared<waterway>(123,"wx");
    auto rsv=std::make_shared<reservoir>(456,"rx");
    std::vector<hydro_connection> a{
        {connection_role::main,wr},
        {connection_role::main,nullptr},
        {connection_role::input,rsv}
    };
    shyft::web_api::generator::emit(sink,a);
    CHECK_EQ(R"_([{"role":"main","target":"W123"},{"role":"main","target":null},{"role":"input","target":"R456"}])_",ps);
}
TEST_CASE("hydro_component_gen") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto s=std::make_shared<hydro_power_system>(1,"s0");
    auto wru=std::make_shared<waterway>(1,"wru","",s);
    auto wr=std::make_shared<waterway>(3,"wrd","",s);
    auto rsv=std::make_shared<reservoir>(2,"rx","json",s);
    s->waterways.push_back(wru);
    s->waterways.push_back(wr);
    s->reservoirs.push_back(rsv);
    hydro_component::connect(rsv,connection_role::main,wr);
    hydro_component::connect(wru,rsv);
    shyft::web_api::generator::emit(sink,std::dynamic_pointer_cast<hydro_component>(rsv));
    CHECK_EQ(R"_({"id":2,"name":"rx","json":"json","upstreams":[{"role":"input","target":"W1"}],"downstreams":[{"role":"main","target":"W3"}]})_",ps);
    ps.clear();
    shyft::web_api::generator::emit(sink,std::dynamic_pointer_cast<hydro_component>(wr));
    CHECK_EQ(R"_({"id":3,"name":"wrd","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[]})_",ps);

}

TEST_CASE("power_station_gen") {
    string ps;
    auto  sink=std::back_inserter(ps);
    auto s=std::make_shared<hydro_power_system>(1,"s0");
    auto a1=std::make_shared<unit>(1,"a1","",s);
    auto a2=std::make_shared<unit>(2,"a2","", s);
    auto p=std::make_shared<power_plant>(3,"p1","",s);
    s->power_plants.push_back(p);
    power_plant::add_unit(p,a1);
    power_plant::add_unit(p,a2);
    shyft::web_api::generator::emit(sink,p);
    CHECK_EQ(R"_({"id":3,"name":"p1","json":"","units":[1,2]})_",ps);
}

TEST_CASE("web_api_hps_gen") {
    auto hps=test::web_api::build_hps("A");
    std::string ps;
    auto sink=std::back_inserter(ps);
    shyft::web_api::generator::emit(sink,hps);
    string e_ps=R"_({"id":0,"name":"A","created":null,"units":[{"type":"A","hydro_component":{"id":1,"name":"the one","json":"{\"power_station_data\":\"...\"}","upstreams":[{"role":"input","target":"W1"}],"downstreams":[{"role":"main","target":"W3"}]}},{"type":"A","hydro_component":{"id":2,"name":"the other","json":"{\"power_station_data\":\"...\"}","upstreams":[{"role":"input","target":"W4"}],"downstreams":[{"role":"main","target":"W5"}]}}],"waterways":[{"type":"W","hydro_component":{"id":1,"name":"r1-p1","json":"","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]},"gates":[{"id":1,"name":"2","json":"{\"3\":\"x\"}"}]},{"type":"W","hydro_component":{"id":2,"name":"r2-r1 river","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[{"role":"main","target":"R1"}]},"gates":[]},{"type":"W","hydro_component":{"id":3,"name":"p to ocean","json":"","upstreams":[{"role":"input","target":"A1"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":4,"name":"r3-p2","json":"","upstreams":[{"role":"input","target":"R3"}],"downstreams":[{"role":"main","target":"A2"}]},"gates":[]},{"type":"W","hydro_component":{"id":5,"name":"p2 to ocean","json":"","upstreams":[{"role":"input","target":"A2"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":6,"name":"r3-bypass ocean","json":"","upstreams":[{"role":"input","target":"R3"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]},{"type":"W","hydro_component":{"id":7,"name":"r2-flood ocean","json":"","upstreams":[{"role":"input","target":"R2"}],"downstreams":[{"role":"main","target":"R4"}]},"gates":[]}],"reservoirs":[{"type":"R","hydro_component":{"id":1,"name":"r1","json":"{\"info\":\"xxxr\"}","upstreams":[{"role":"input","target":"W2"}],"downstreams":[{"role":"main","target":"W1"}]}},{"type":"R","hydro_component":{"id":2,"name":"r2","json":"{\"reservoir_data\":[100, 110, 10]}","upstreams":[],"downstreams":[{"role":"main","target":"W2"},{"role":"flood","target":"W7"}]}},{"type":"R","hydro_component":{"id":3,"name":"r3","json":"{\"reservoir_data\":[100, 110, 10]}","upstreams":[],"downstreams":[{"role":"main","target":"W4"},{"role":"bypass","target":"W6"}]}},{"type":"R","hydro_component":{"id":4,"name":"ocean","json":"{\"reservoir_data\":[0, 0.01, 1e100]}","upstreams":[{"role":"input","target":"W3"},{"role":"input","target":"W5"},{"role":"input","target":"W6"},{"role":"input","target":"W7"}],"downstreams":[]}}],"power_plants":[{"id":3,"name":"pp","json":"{}","units":[1,2]}]})_";
    CHECK_EQ(e_ps,ps);
}


}
#if 0
namespace experiment {
    using shyft::energy_market::core::proxy_attr_m;
    using namespace shyft::energy_market;
    //using namespace shyft::web_api::generator;
     // -- experiment section:
     // -- goal: want one line pr. attr for set/get/exist.
     //             declarative
     //             if possible, derived from the proxy-types in the stm::reservoir class.
     //
     //--ok, now lets' pretend we got a web-request, with 16606, and rsv_attr::hrl, lrl'
     ///  web-request: reservoir(oid).attr(aid) [.a_value_type.]
     ///  web-request: ot, oid, aid, value?
     /// -reply:: json (type-script-type)
     // for req:requests:
     ///  generate_json( req.o_d, req.a_id ,req.type_id)
     //

    /** @brief proxy_attr_exists
     *
     * @detail just an example, the function could be generate_json etc..
     *
     * @tparam C the class, like stm::reservoir
     * @tparam E the enum class, like stm::rsv_attr
     * @tparam E e the enum member, like stm::rsv_attr::lrl
     * @param o the const object ref to class C, e.g. stm::reservoir
     * @return true if the C::o have attribute e set to a value
     */

    template<typename C, typename E, E  e>
    bool proxy_attr_exists(C const & o) noexcept {
      return proxy_attr_m<C,E,e>::extract_from(o).exists();
    }





    /**  fx to compile time roll out the table map [int alias rsv_attr] proxy_attr_exists fx pointers.
     * @note that this template works for any stm class with proxy stuff in it
    */
    template<typename C,typename E, int...a>
    constexpr std::array<bool(*)(C const&),sizeof...(a)>
    cmake_attr_exists_table(std::integer_sequence<int,a...>) {
        return {{&proxy_attr_exists<C,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    }

    // finally, easy to create the function that does the runtime-job task
    // assuming
    // C::e_attr enum is defined
    // C::e_attr_seq_t is defined
    template <class C>
    bool object_attr_exists(C const&r, typename C::e_attr a) {
        static constexpr auto _attr_exists_fx =cmake_attr_exists_table<C,typename C::e_attr>(typename C::e_attr_seq_t{});
        return _attr_exists_fx[int(a)](r);
    }

    // now for the emitter:
    /** @brief emits a proxy attribute
     *
     * @detail using the generator::emit(oi,T), or emit_null if the
     *  proxy_attr is null (!exists()).
     *
     */
    template<typename C,typename OutputIterator, typename E, E e>
    void emit_attr(C const &o,OutputIterator &oi) noexcept {
        auto const& a=  proxy_attr_m<C,E,e>::extract_from(o);
        if(a.exists()) {
            shyft::web_api::generator::emit(oi,a.get());
        } else {
            shyft::web_api::generator::emit_null(oi);
        }
    }

    template<typename C,typename OutputIterator, typename E, int...a>
    constexpr std::array<void(*)(C const&,OutputIterator&),sizeof...(a)>
    cmake_emit_attr_table(std::integer_sequence<int,a...>) {
        return {{&emit_attr<C,OutputIterator,E,static_cast<E>(a)>...}};// generates a list of fx-pointers from the above exists template
    }

    template<class C,typename OutputIterator>
    void emit_attr_for_object(C const&o,typename C::e_attr a, OutputIterator&oi) {
        static constexpr auto _attr_emit_fx =cmake_emit_attr_table<C,OutputIterator,typename C::e_attr>(typename C::e_attr_seq_t{});
        _attr_emit_fx[int(a)](o,oi);
    }


}

TEST_SUITE("stm_mp") {
TEST_CASE("stm_attribute_study") {
    // try to figure out the best possible query/response
    // patterns
    using namespace shyft::energy_market;
    using std::runtime_error;
    using shyft::core::utctime;
    using shyft::time_series::dd::apoint_ts;
    using shyft::time_axis::point_dt;
    using t_xy=stm::t_xy_::element_type;

    utctime t0{ 0 };
    utctime t1{ 1 };
    auto ta = point_dt(vector<utctime>{t0}, t1);
    //--arrange the test
    auto sys=make_shared<stm::stm_system>(1,"first","");
    auto ull=make_shared<stm::stm_hps>(1,"Ulla-førre");
    ull->ids = make_shared<stm::hps_ds>();// attach empty ids
    sys->hps.push_back(ull);
    sys->market.push_back(make_shared<stm::energy_market_area>(1,"NO1","null",sys));
    auto b = make_shared<stm::reservoir>(16606,"blåsjø","",ull);
    ull->reservoirs.push_back(b);
    CHECK_EQ(b->level.regulation_max.exists(),false);// none set yet.

    auto t_d = apoint_ts(ta, 1233.0);
    b->level.regulation_max = t_d;
    apoint_ts y=b->level.regulation_max;
    //-- assert
     CHECK_EQ(y(t0),doctest::Approx(1233.0));
     auto t_r_vol=make_shared<t_xy>();
     auto vol_curve=make_shared<xy_point_curve>(vector<double>{0.0,3500.0},vector<double>{1200.0,1234.0});
     (*t_r_vol)[t0]= vol_curve;
     b->volume_level_mapping=t_r_vol;
     CHECK_EQ(b->volume_level_mapping.exists(),true);

     //--- ok now we are ready for the  experiment section:
     // imagine we got these two in the web-request.
     // knowing from request it was a reservoir, located in this system
     int r_attr=int(stm::rsv_attr::level_regulation_max);// from the request,
     int64_t oid=16606;
     // just imagine we looked up b from oid
     auto r=std::dynamic_pointer_cast<stm::reservoir>(sys->hps[0]->find_reservoir_by_id(oid));
     // this we like (could be any fx we want to apply to object.proxy_attr):
     CHECK_EQ(true,experiment::object_attr_exists(*r,static_cast<stm::rsv_attr>(r_attr)));
     CHECK_EQ(false,experiment::object_attr_exists(*r,stm::rsv_attr::level_regulation_min));
     CHECK_EQ(true,experiment::object_attr_exists(*r,stm::rsv_attr::volume_level_mapping));
     string ps;
     auto  sink=std::back_inserter(ps);
     experiment::emit_attr_for_object(*r,stm::rsv_attr::level_regulation_max,sink);
     CHECK_EQ(true,ps.size()>0);
     // this works: std::cout<<"hrl:"<<ps<<"\n";

}

}
#endif
