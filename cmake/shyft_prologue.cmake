include_guard()

find_package(Git)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})

#NOTE:
#  set up about info
#  - jeh
set(SHYFT_AUTHOR             "ref readme authors and contributors")
set(SHYFT_EMAIL              "sigbjorn.helset@gmail.com")
set(SHYFT_URL                "https://gitlab.com/shyft-os/shyft")
set(SHYFT_DESCRIPTION        "Shyft is a cross-platform open source toolbox for the energy-market developed at Statkraft in cooperation with the Department of Geosciences at the University of Oslo")
set(SHYFT_LICENCE            "LGPL3")
set(SHYFT_CONTAINER_REGISTRY "registry.gitlab.com/shyft-os/shyft")

if(Git_FOUND)
  #NOTE:
  #  compute version, release and codename based on git tag
  #  - jeh
  execute_process(
    COMMAND "${GIT_EXECUTABLE}" "describe" "--always" "--tags" "HEAD"
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    OUTPUT_VARIABLE _GIT_OUTPUT
    ERROR_VARIABLE _GIT_ERROR
    RESULT_VARIABLE _GIT_RESULT
    OUTPUT_STRIP_TRAILING_WHITESPACE)

  if("${_GIT_RESULT}" EQUAL 0)
    if(${_GIT_OUTPUT} MATCHES "^([0-9]+)\\.([0-9]+)\\.([0-9]+)-([0-9]+).*$")
      set(SHYFT_VERSION_MAJOR   ${CMAKE_MATCH_1})
      set(SHYFT_VERSION_MINOR   ${CMAKE_MATCH_2})
      set(SHYFT_VERSION_PATCH   ${CMAKE_MATCH_3})
      set(SHYFT_RELEASE         ${CMAKE_MATCH_4})
      set(SHYFT_VERSION         "${CMAKE_MATCH_1}.${CMAKE_MATCH_2}.${CMAKE_MATCH_3}")
      set(SHYFT_TAG "${SHYFT_VERSION}-${SHYFT_RELEASE}")
    else()
      message(WARNING "ShyftConfig: Invalid version string \"${_GIT_OUTPUT}\"")
    endif()
  else()
    message(WARNING "ShyftConfig: Git error \"${_GIT_RESULT}\" when trying to find version")
    message(WARNING "${_GIT_ERROR}")
  endif()

  execute_process(
    COMMAND "${GIT_EXECUTABLE}" "tag" "-l" "--format=%(subject)" ${SHYFT_TAG}
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}"
    OUTPUT_VARIABLE _GIT_OUTPUT
    ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
  set(SHYFT_CODENAME ${_GIT_OUTPUT})
else()
  message(WARNING "ShyftConfig: Unable to find version and codename without git")
endif()

if("${SHYFT_TAG}" STREQUAL "")
  set(SHYFT_VERSION_MAJOR   1)
  set(SHYFT_VERSION_MINOR   0)
  set(SHYFT_VERSION_PATCH   0)
  set(SHYFT_RELEASE         0)
  message(WARNING "ShyftConfig: Missing version string, setting '${SHYFT_VERSION_MAJOR}.${SHYFT_VERSION_MINOR}.${SHYFT_VERSION_PATCH}-${SHYFT_RELEASE}' as default")
endif()
set(SHYFT_VERSION "${SHYFT_VERSION_MAJOR}.${SHYFT_VERSION_MINOR}.${SHYFT_VERSION_PATCH}")
set(SHYFT_TAG     "${SHYFT_VERSION}-${SHYFT_RELEASE}")

if("${SHYFT_CODENAME}" STREQUAL "")
  set(SHYFT_CODENAME "dev")
  message(STATUS "ShyftConfig: Missing codename, setting '${SHYFT_CODENAME}' as default")
endif()

message(STATUS "ShyftConfig: ${SHYFT_TAG} / ${SHYFT_CODENAME}")

#NOTE:
#  compute host info
#  - jeh
cmake_host_system_information(RESULT SHYFT_HOST QUERY OS_NAME)
string(TOUPPER ${SHYFT_HOST} SHYFT_HOST)

if("${SHYFT_HOST}" STREQUAL "LINUX")
  file(STRINGS /etc/os-release SHYFT_HOST_ID REGEX "^ID=.*$")
  string(REPLACE "ID=" "" SHYFT_HOST_ID ${SHYFT_HOST_ID})
  string(TOUPPER ${SHYFT_HOST_ID} SHYFT_HOST_ID)
  if(${SHYFT_HOST_ID} STREQUAL "ARCH")
    set(SHYFT_HOST_VERSION_CODENAME "LATEST")
  endif()
elseif("${SHYFT_HOST}" STREQUAL "WINDOWS")
  set(SHYFT_HOST_ID "W")
  set(SHYFT_HOST_VERSION_CODENAME "10")
elseif("${SHYFT_HOST}" STREQUAL "MACOS")
  set(SHYFT_HOST_ID "DARWIN")
else()
  message(FATAL_ERROR "ShyftConfig: Unsupported host ${SHYFT_HOST}")
endif()

cmake_host_system_information(RESULT SHYFT_HOST_PLATFORM QUERY OS_PLATFORM)
string(TOUPPER ${SHYFT_HOST_PLATFORM} SHYFT_HOST_PLATFORM)

string(JOIN "-" SHYFT_HOST_SIGNATURE ${SHYFT_HOST} ${SHYFT_HOST_ID} ${SHYFT_HOST_VERSION_CODENAME} ${SHYFT_HOST_PLATFORM})
message(STATUS "ShyftConfig: Detected ${SHYFT_HOST_SIGNATURE}")
