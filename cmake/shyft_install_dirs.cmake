include_guard()

include(GNUInstallDirs)
if(NOT SHYFT_INSTALL_INCLUDEDIR)
  set(SHYFT_INSTALL_INCLUDEDIR ${CMAKE_INSTALL_INCLUDEDIR}/shyft-${SHYFT_VERSION})
endif()
if(NOT SHYFT_INSTALL_BINDIR)
  set(SHYFT_INSTALL_BINDIR ${CMAKE_INSTALL_BINDIR})
endif()
if(NOT SHYFT_INSTALL_LIBDIR)
  set(SHYFT_INSTALL_LIBDIR ${CMAKE_INSTALL_LIBDIR})
endif()
if(NOT SHYFT_INSTALL_DOCDIR)
  set(SHYFT_INSTALL_DOCDIR ${CMAKE_INSTALL_DOCDIR}-${SHYFT_VERSION})
endif()
if(NOT SHYFT_INSTALL_CMAKEDIR)
  set(SHYFT_INSTALL_CMAKEDIR ${CMAKE_INSTALL_LIBDIR}/cmake/shyft-${SHYFT_VERSION})
endif()

if(SHYFT_WITH_PYTHON)
  if(NOT SHYFT_INSTALL_PYDIR)
    if("${SHYFT_HOST}" STREQUAL "LINUX")
      if("${SHYFT_HOST_ID}" STREQUAL "ARCH")
        find_package(Python3 REQUIRED COMPONENTS Interpreter Development)
        set(SHYFT_INSTALL_PYDIR ${Python3_SITELIB})
        string(REGEX REPLACE "^/usr/" "" SHYFT_INSTALL_PYDIR ${SHYFT_INSTALL_PYDIR})
      endif()
    elseif("${SHYFT_HOST}" STREQUAL "WINDOWS")
        find_package(Python3 REQUIRED COMPONENTS Interpreter Development)
        set(SHYFT_INSTALL_PYDIR ${Python3_SITELIB})
    elseif("${SHYFT_HOST}" STREQUAL "MACOS")
        find_package(Python3 REQUIRED COMPONENTS Interpreter Development)
        set(SHYFT_INSTALL_PYDIR ${Python3_SITELIB})
    else()
      message(FATAL_ERROR "ShyftHost: unsupported platform: ${SHYFT_HOST}")
    endif()
  endif()
endif()
