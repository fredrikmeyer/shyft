# default options that we usually keep on
option(SHYFT_WITH_HYDROLOGY      "Enable hydrology parts"         ON)
option(SHYFT_WITH_ENERGY_MARKET  "Enable energy market parts"     ON)
# auto detected, or set, so we leave the line under commented out
#option(SHYFT_WITH_SHOP           "Enable shop in energymarket"  )
option(SHYFT_WITH_PYTHON         "Enable python bindings"         ON)
option(SHYFT_WITH_TESTS          "Enable testing"                 ON)
option(SHYFT_BUNDLE_DEPS         "Add 3rd party libs to shyft/lib" ON)
# Provide means of controlling the the default build using env, suitable for ci/cd variables
set(SHYFT_WITH_SHOP_DEFAULT OFF)
if(DEFINED ENV{SHYFT_WITH_SHOP})
    set(SHYFT_WITH_SHOP_DEFAULT "$ENV{SHYFT_WITH_SHOP}")
endif()
option(SHYFT_WITH_SHOP           "Add Sintef Shop, require license" ${SHYFT_WITH_SHOP_DEFAULT})


option(SHYFT_WITH_SHOP_API_GEN   "Add api gen executable" OFF)
# more specific options for quality etc.
option(SHYFT_WITH_COVERAGE       "Enable coverage"               OFF)
# options releated to creating packages
option(SHYFT_WITH_CMAKE_CONFIG   "Generate cmake package config" OFF)
option(SHYFT_WITH_CPACK          "Enable cpack"                  OFF)
option(SHYFT_WITH_CONTAINERS     "Enable container targets"      OFF)

set(SHYFT_DEFAULT_BUILD_TYPE "Release")

# put in place default build type
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
    message(STATUS "No build type specified. Defaulting to '${SHYFT_DEFAULT_BUILD_TYPE}'.")
    set(CMAKE_BUILD_TYPE ${SHYFT_DEFAULT_BUILD_TYPE} CACHE STRING "Choose the type of build." FORCE)
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release")
endif()

if (NOT DEFINED SHYFT_DEPENDENCIES_DIR)
  if(DEFINED ENV{SHYFT_DEPENDENCIES_DIR})
    set(SHYFT_DEPENDENCIES_DIR $ENV{SHYFT_DEPENDENCIES_DIR})
  else()
    set(SHYFT_DEPENDENCIES_DIR "${PROJECT_SOURCE_DIR}/../shyft_dependencies")
  endif()
endif()
# Our code requires an absolute directory for the dependencies
get_filename_component(SHYFT_DEPENDENCIES ${SHYFT_DEPENDENCIES_DIR} ABSOLUTE)
message(STATUS "SHYFT_DEPENDENCIES directory: " ${SHYFT_DEPENDENCIES})
message(STATUS "  You can override the above via the $SHYFT_DEPENDENCIES_DIR environment variable")

# shyft uses boost, dlib, armadillo and doctest, and python +  numpy
set(CMAKE_PREFIX_PATH "${SHYFT_DEPENDENCIES}" ${CMAKE_PREFIX_PATH})


IF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  # Default is to install into directory containing shyft dependencies
  SET(CMAKE_INSTALL_PREFIX "${SHYFT_DEPENDENCIES}" CACHE PATH "Install directory" FORCE)
ENDIF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
SET(inst_root ${CMAKE_INSTALL_PREFIX}/)  # Alias since some submodules use inst_root variable
SET(SHYFT_PYTHON_DIR ${CMAKE_SOURCE_DIR}/python)  # where python stuff are located exists

message(STATUS "Shyft python extensions will be installed inplace python/shyft, and --component development specific install destination is: ${CMAKE_INSTALL_PREFIX}")

