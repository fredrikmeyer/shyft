@echo off
@rem simple build script that
@rem a) build shyft core debug and deploy it to SHYFT_DEPENDENCIES
@rem b) build shyft full release
@rem preconditions: shyft dependencies are built, and env. variables according to this is set(typical dev.setup)

pushd "%~dp0"\..
mkdir build
cd build
if not %errorlevel%==0 (
    goto finale
) else (
    cmake  .. -A x64 -T host=x64
)
if not %errorlevel%==0 (
    goto finale
) else (
    cmake --build . --config Release --target install -j 8
)

:finale
popd
exit /b %errorlevel%
