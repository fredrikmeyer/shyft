#!/bin/bash
set -o errexit # exit on failure set -e
set -o nounset # exit on undeclared vars set -u
set -o pipefail # exit status of the last command that threw non-zero exit code returned
# Debugging set -x
# set -o xtrace

export SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../..)}
build_support_dir=$(readlink --canonicalize --no-newline `dirname ${0}`)
# to align the cmake support:
export SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
armadillo_name=armadillo-11.4.2
dlib_ver=${SHYFT_DLIB_VERSION:-19.24}
dlib_name=dlib-${dlib_ver}
boost_ver=${SHYFT_BOOST_VERSION:-1_80}_0
numpy_ver=${SHYFT_BOOST_NUMPY_VERSION:-1.23}
mkl_ver=${SHYFT_MKL_VER:-2022.0.0}
openssl_ver=${SHYFT_OPENSSL_VER:-1.1.1o}
cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER  -DCMAKE_SYSTEM_VERSION=10.0"
miniconda_ver=latest
echo ---------------
echo Windows Update/build shyft-dependencies
echo SHYFT_WORKSPACE........: ${SHYFT_WORKSPACE}
echo SHYFT_DEPENDENCIES_DIR.: ${SHYFT_DEPENDENCIES_DIR}
echo PACKAGES...............: miniconda ${miniconda_ver} w/shyft_env,
echo .......................: doctest, boost_${boost_ver}, ${armadillo_name}, ${dlib_name}, numpy=${numpy_ver}, openssl, otlv4 mkl ${mkl_ver}  ssl ${openssl_ver}
WGET='curl -L -O'

# A helper function to compare versions
function version { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# the current versions we are building
mkdir -p "${SHYFT_DEPENDENCIES_DIR}"
cmake_ms_flags="-A x64 -T host=x64"
cd "${SHYFT_DEPENDENCIES_DIR}"


if [ ! -d doctest ]; then
    echo Building doctest
    git clone https://github.com/doctest/doctest
    pushd doctest
    cmake ${cmake_ms_flags} -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -B build && cmake -P build/cmake_install.cmake
    popd
fi;
echo Done doctest

if [ ! -d date ]; then
    echo Building Howard Hinnant date extensions to chrono
    git clone https://github.com/HowardHinnant/date
    pushd date
    cmake ${cmake_ms_flags} . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DCMAKE_CXX_STANDARD=17 -B build
    cmake --build build --target install
    popd
fi;
echo Done HowardHinnant date extensions

cd "${SHYFT_WORKSPACE}"

if [[ ! "${PATH}" == *"condabi"* ]]; then
    if [ ! -d miniconda/Scripts ]; then
        echo Missing python install. try to make one using miniconda
        if [ -d miniconda ]; then
            rm -rf miniconda
        fi;
        if [ ! -f Miniconda3-${miniconda_ver}-Windows-x86_64.exe ]; then
            ${WGET}  http://repo.continuum.io/miniconda/Miniconda3-${miniconda_ver}-Windows-x86_64.exe
        fi;
    if [ ! -f Miniconda3-Windows-x86_64.exe ]; then
        cp Miniconda3-${miniconda_ver}-Windows-x86_64.exe Miniconda3-Windows-x86_64.exe
    fi;
    echo 'start /wait "" .\Miniconda3-Windows-x86_64.exe /InstallationType=JustMe /S /D=%cd%\miniconda' >install_miniconda.cmd
    ./install_miniconda.cmd
    # Update conda to latest version, assume we start with 4.3 which
    # requires PATH to be set
    OLDPATH=${PATH}
    export PATH="${SHYFT_WORKSPACE}/miniconda:${SHYFT_WORKSPACE}/miniconda/condabin:${SHYFT_WORKSPACE}/miniconda/Scripts:${SHYFT_WORKSPACE}/miniconda/Dlls:$PATH"
    old_conda_version=$(conda --version | sed "s/conda \(.*\)/\1/")
    echo "Old conda version is ${old_conda_version}"
    #activate
    conda config --set always_yes yes --set changeps1 no
    conda update conda
    new_conda_version=$(conda --version | sed "s/conda \(.*\)/\1/")
    echo "New conda version is ${new_conda_version}"
    py_build_packs="conda-build conda-verify setuptools anaconda-client twine pip"
    py_dashboard_packs="bokeh sphinx sphinx_rtd_theme pydot lxml"
    py_shyft_packs="pyyaml 'numpy>=1.20' mkl==${mkl_ver} openssl==${openssl_ver} netcdf4 matplotlib requests pytest coverage pytest-cov pip twine anaconda-client shapely  pyproj pillow"
    py_cf_packs="sphinx-autodoc-typehints 'pyside2>=5.13.2' pint"
    conda install --channel main --channel intel ${py_build_packs} 'numpy>=1.20' mkl-devel==${mkl_ver} openssl==${openssl_ver}
    conda create  -n shyft_38  --channel main --channel conda-forge  python=3.8   ${py_shyft_packs} ${py_dashboard_packs} ${py_cf_packs}
    conda create  -n shyft_39  --channel main --channel conda-forge  python=3.9   ${py_shyft_packs} ${py_dashboard_packs} ${py_cf_packs}
    conda create  -n shyft_310 --channel main --channel conda-forge  python=3.10  ${py_shyft_packs} ${py_dashboard_packs} ${py_cf_packs}
    source ${SHYFT_WORKSPACE}/miniconda/etc/profile.d/conda.sh
    conda activate base
  else
    export PATH="${SHYFT_WORKSPACE}/miniconda:${SHYFT_WORKSPACE}/miniconda/Scripts:${SHYFT_WORKSPACE}/miniconda/Dlls:$PATH"
  fi;
fi;
echo Done minconda/python using `type -p python`
py_root=`type -p python |  sed  -e 's_/python__'`
export BOOST_PYTHONHOME=`type -p python |  sed  -e 's_/python__' -e 's/^\///' -e 's_/_\\\\_g' -e 's/^./\0:/'`
echo Setting BOOST_PYTHONHOME to  ${BOOST_PYTHONHOME}
conda_lib_dir=${py_root}/Library
echo openssl from python install located at ${conda_lib_dir}
if [ -d ${conda_lib_dir}/include/openssl ]; then
    if [ ! -d ${SHYFT_DEPENDENCIES_DIR}/include/openssl ]; then
        cp -R ${conda_lib_dir}/include/openssl ${SHYFT_DEPENDENCIES_DIR}/include
        cp ${conda_lib_dir}/lib/libssl.lib ${SHYFT_DEPENDENCIES_DIR}/lib
        cp ${conda_lib_dir}/lib/libcrypto.lib ${SHYFT_DEPENDENCIES_DIR}/lib
    else
        echo Done - openssl library seems to be in place, skip
    fi;
else
    echo "opensll not found in " ${conda_lib_dir} ", use conda to install it first"
    exit 1
fi;
if [ ! -f ${SHYFT_DEPENDENCIES_DIR}/lib/mkl_rt.lib ]; then
    if [  -f ${conda_lib_dir}/lib/mkl_rt.lib   ]; then
        # just so that we get the interface lib in place for our cmake find_lib
        # the runtime (.dll) are fetched from conda bin dir regardless
        cp ${conda_lib_dir}/lib/mkl_rt.lib ${SHYFT_DEPENDENCIES_DIR}/lib
    else
        echo "mkl_rt  found in " ${conda_lib_dir}/lib ", use conda to install mkl-devel package"
        exit 1
    fi;
else
    echo mkl_rt.lib for interface to intel mkl are in place
fi;
# then, with the mkl_rt lib in place, we can try to build armadillo with mkl
cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d ${armadillo_name} ]; then
    echo Building ${armadillo_name}
    if [ ! -f ${armadillo_name}.tar.xz ]; then
        ${WGET}  http://sourceforge.net/projects/arma/files/${armadillo_name}.tar.xz
    fi;
    7z -y -bd e ${armadillo_name}.tar.xz >/dev/null
    7z -y -bd x ${armadillo_name}.tar >/dev/null
    pushd ${armadillo_name}
    cmake -B build ${cmake_ms_flags} -DCMAKE_PREFIX_PATH=${conda_lib_dir} -DCMAKE_INSTALL_PREFIX=${SHYFT_DEPENDENCIES_DIR} -DCMAKE_INSTALL_LIBDIR=lib -DDETECT_HDF5=0 ${cmake_common}
    cmake --build build --config Release --target install
    popd
fi;
echo Done ${armadillo_name}

cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d "${dlib_name}" ]; then
    echo Building "${dlib_name}"
    dlib_archive="v${dlib_ver}.zip"
    if [ ! -f "${dlib_archive}" ]; then
        ${WGET} "https://github.com/davisking/dlib/archive/${dlib_archive}"
    fi;
    7z -y -bd x "${dlib_archive}" >/dev/null
    pushd "${dlib_name}"
    dlib_cfg="-DDLIB_PNG_SUPPORT=0 -DDLIB_GIF_SUPPORT=0 -DDLIB_LINK_WITH_SQLITE3=0 -DDLIB_NO_GUI_SUPPORT=1 -DDLIB_DISABLE_ASSERTS=1 -DDLIB_JPEG_SUPPORT=0 -DDLIB_USE_BLAS=0 -DDLIB_USE_LAPACK=0 -DBUILD_SHARED_LIBS=0"
    cmake -B build ${cmake_ms_flags}  -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common} ${dlib_cfg}
    cmake --build build --config Release --target install
    cmake --build build --config Debug --target install
    popd
fi;
echo Done ${dlib_name}

cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d boost_${boost_ver} ]; then
    echo Building boost_${boost_ver}
    if [ ! -f boost_${boost_ver}.tar.gz ]; then
        ${WGET} https://boostorg.jfrog.io/artifactory/main/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
    fi;
    echo extracting archive pass1
    7z -y -bd e boost_${boost_ver}.tar.gz >/dev/null
    echo unzipping all the files..
    7z -y -bd x boost_${boost_ver}.tar >/dev/null
    echo building the stuff
    pushd boost_${boost_ver}
    echo "call bootstrap.bat" > bboost.cmd
    ./bboost.cmd
    py_envs=`echo ${py_root}/envs | sed -e 's@^/.@\0:@g' -e 's@^/@@g'`  # here we could tweak using virtual-env, conda or system.. match with cmake
    # have to help boost figure out right python versions bin,inc and libs.
    # first remove any python that was found with the bootstrap step above
    mv -f project-config.jam x.jam
    cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
    echo "# injected by shyft build/build_dependencies.sh to map explicit python versions" >>project-config.jam
    for pv in 3.8 3.9 3.10; do # same store for all , notice boost seems to need numpy as well
        e=${pv/./}
        echo "using python" >>project-config.jam
        echo "   : ${pv} "  >>project-config.jam
        echo "   : ${py_envs}/shyft_${e}/python ">>project-config.jam
        echo "   : ${py_envs}/shyft_${e}/include ${py_envs}/shyft_${e}/lib/site-packages/numpy/core/include" >>project-config.jam
        echo "   : ${py_envs}/shyft_${e}/libs ;" >>project-config.jam
    done
    echo "b2 -d0 -j 6 define=BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE link=shared variant=release threading=multi runtime-link=shared address-model=64 --with-regex --with-system --with-filesystem --with-date_time --with-python --with-serialization --with-chrono --with-thread --with-test --with-program_options --with-atomic --with-math python=3.8,3.9,3.10 --prefix=%cd%\.. install" > bboost2.cmd
    ./bboost2.cmd
    popd
fi;
echo  Done boost_${boost_ver}
export WIN_SHYFT_DEPENDENCIES_DIR=`echo ${SHYFT_DEPENDENCIES_DIR} |   sed -e 's@^/.@\0:@g' -e 's@^/@@g'`
cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d pybind11 ]; then
    git clone https://github.com/pybind/pybind11.git
    pushd pybind11
    echo set PYTHONHOME=%BOOST_PYTHONHOME% >bdlib.cmd
    echo cmake -B build ${cmake_ms_flags} -DCMAKE_INSTALL_PREFIX=${WIN_SHYFT_DEPENDENCIES_DIR} -DPYBIND11_TEST=0 ${cmake_common} >>bdlib.cmd
    echo cmake -P build/cmake_install.cmake >>bdlib.cmd
    ./bdlib.cmd
    popd
fi;
echo Done pybind11
if [ ! -d leveldb ]; then
    git clone https://github.com/google/leveldb
    pushd leveldb
    cmake -B build -DCMAKE_BUILD_TYPE=Release -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF -A x64 -T host=x64
    cmake --build build --config Release -- -m
    cmake --install build --prefix ${SHYFT_DEPENDENCIES_DIR}
    popd
fi;
echo Done leveldb

echo Doing the otl header-only otlv4.h
if [ ! -f include/otlv4.h ]; then
    echo ..missing, then download and install otlv4.h
    ${WGET} http://otl.sourceforge.net/otlv4_h2.zip
    7z -y -bd -oinclude x otlv4_h2.zip >/dev/null
fi;
echo Done otlv4.h
