#!/bin/bash

set -e # exit on failure set -e 
set -o errexit 
# set -o nounset # exit on undeclared vars, we allow they to be unset in this script
set -o pipefail # exit status of the last command that threw non-zero exit code returned
#set -x

build_pkg() {
  target=$1
  openssl_v=$(${CONDA_EXE} list -f openssl|grep openssl| awk -F' ' '{print $2}')
  mkl_v=$(${CONDA_EXE} list -f mkl|grep mkl| awk -F' ' '{print $2}')
  echo "Build pkg "  "${SHYFT_VERSION}" "${target} with openssl ${openssl_v} mkl ${mkl_v}"
  # ensure to force openssl/mkl version
  sed -i -e s/"- openssl$"/"- openssl==${openssl_v}"/  ${target}/meta.yaml
  sed -i -e s/"- mkl "/"- mkl==${mkl_v} "/  ${target}/meta.yaml
  if ! ${CONDA_EXE} build  ${conda_build_opts} ${target}; then
   echo "Failed to build package, exit"
   (exit 1)
  fi
}

upload_pkg() {
    target=$1

    if [ $target == "pypi" ]; then
        echo -- upload wheels to pypi --
        if [ -d dist/"${SHYFT_VERSION}" ]; then
          for f in dist/${SHYFT_VERSION}/*.whl; do
              if  [[ "$OSTYPE" == "linux-gnu"* ]]; then
                  mname="$(echo $f | sed 's/linux/manylinux1/')"
                  mv $f "${mname}" ;
              else
                  mname="$f"
              fi
              if  [[ "${mname}" == *"time"* ]]; then
                  if [[ "${SHYFT_TS_PYPI_TOKEN}" != "" ]]; then
                      if ! twine upload "${mname}" -u=__token__ -p=${SHYFT_TS_PYPI_TOKEN} --disable-progress-bar; then
                          echo "Failed to upload linux packages ${mname} to PyPi"
                      fi
                  else
                      echo "SHYFT_TS_PYPI_TOKEN not set, skipping upload"
                  fi
              else
                  if [[ "${SHYFT_PYPI_TOKEN}" != "" ]]; then
                      if ! twine upload "${mname}" -u=__token__ -p=${SHYFT_PYPI_TOKEN} --disable-progress-bar; then
                          echo "Failed to upload linux packages ${mname} to PyPi"
                      fi
                  else
                      echo "SHYFT_PYPI_TOKEN not set, skipping upload"
                  fi
              fi
          done
        else
          echo "Error: missing dist directory dist/${SHYFT_VERSION}"
          (exit 1)
        fi
    else
        pkg_file=$(${CONDA_EXE} build ${conda_build_opts} --output "${target}" 2>/dev/null)
        if [[ "${token_arg}" != "" ]]; then
          if ! anaconda ${token_arg} upload --no-progress --skip-existing --user ${user_arg}  --label ${label_name} ${pkg_file}; then
            echo "Failed to upload ${pkg_file}"
            (exit 1)
          fi
          # also upload corresponding pip packages located in dist/${SHYFT_VERSION} after local build is run
          # in this case we also have to change pkg. name since conda does not
          # allow same pkg name as conda pkgs (not a separate namespace)
          if [[ "${target}" == *"time_series"* ]]; then
             python setup.py -q bdist_wheel --conda-pip --ts-only --dist-dir=dist/${SHYFT_VERSION}
             rm -rf build/lib build/bdist.*
             wheel_file="$(ls dist/${SHYFT_VERSION}/shyft.time_series_pip*)"
          else
             python setup.py -q bdist_wheel --conda-pip --dist-dir=dist/${SHYFT_VERSION}
             rm -rf build/lib build/bdist.*
             wheel_file="$(ls dist/${SHYFT_VERSION}/shyft_pip-*)"
          fi
          if ! anaconda ${token_arg} upload --no-progress --skip-existing --user ${user_arg}  --label ${label_name} --package-type pypi ${wheel_file}; then
            echo "Failed to upload pypi wheel-file ${wheel_file}"
            rm -f "${wheel_file}"
            (exit 1)
          fi
          rm -f "${wheel_file}"
        else
          echo "INFO: skipping anaconda client upload since token is not specified"
        fi
        if [[ "${conda_cli_token_arg}" != "" ]]; then
          if ! ${CONDA_EXE} repo  ${conda_cli_token_arg} upload  --no-progress  --channel ${conda_cli_channel_arg}  ${pkg_file}; then
            echo "Failed to upload ${pkg_file}"
            (exit 1)
          fi
          # also upload corresponding pip packages located in dist/${SHYFT_VERSION} after local build is run
          # in this case we also have to change pkg. name since conda does not
          # allow same pkg name as conda pkgs (not a separate namespace)
          if [[ "${target}" == *"time_series"* ]]; then
             python setup.py -q bdist_wheel --conda-pip --ts-only --dist-dir=dist/${SHYFT_VERSION}
             rm -rf build/lib build/bdist.*
             wheel_file="$(ls dist/${SHYFT_VERSION}/shyft.time_series_pip*)"
          else
             python setup.py -q bdist_wheel --conda-pip --dist-dir=dist/${SHYFT_VERSION}
             rm -rf build/lib build/bdist.*
             wheel_file="$(ls dist/${SHYFT_VERSION}/shyft_pip-*)"
          fi
          if ! ${CONDA_EXE} repo ${conda_cli_token_arg} upload --no-progress --channel ${conda_cli_channel_arg} --package-type pypi ${wheel_file}; then
            echo "Failed to upload pypi wheel-file ${wheel_file}"
            rm -f "${wheel_file}"
            (exit 1)
          fi
          rm -f "${wheel_file}"
        else
          echo "INFO: skipping anaconda client upload since token is not specified"
        fi

    fi
}

init() {
  label_ci_ref=${CI_COMMIT_BRANCH:=master}  # we build tags, so if no branch, its master
  label_name=${label_ci_ref//master/main}
  SHYFT_ROOT=$(readlink --canonicalize --no-newline `dirname ${0}`/..)
  cd "${SHYFT_ROOT}"
  SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../..)}
  echo WS: ${SHYFT_WORKSPACE}
  SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
  git_branch=${label_ci_ref}
  np_version=$(python -c "import numpy;print(numpy.version.version[:-2])")
  py_version=$(python -c "from sysconfig import get_python_version;print(get_python_version())")
  SHYFT_VERSION=$(git describe --always --tags HEAD | cut -d - -f 1)
  export SHYFT_VERSION
  echo SHYFT_VERSION="${SHYFT_VERSION}"
  conda_build_opts="--python=${py_version} --numpy ${np_version} --quiet --no-test --no-copy-test-source-files --no-anaconda-upload --no-activate --no-verify --cache-dir conda-cache --output-folder dist/conda"
  # change directory from working dir to python where all the python stuff is.
  cd python
  mkdir -p dist/conda >/dev/null # ensure it exists.
}

make_local_dist() {
  echo ----------------------------
  echo Creating local install to ${SHYFT_DEPENDENCIES_DIR}/py/${py_version}/${SHYFT_VERSION}
  mkdir -p ${SHYFT_DEPENDENCIES_DIR}/py/${py_version}/${SHYFT_VERSION}
  python setup.py -q bdist_wheel --dist-dir="dist/${SHYFT_VERSION}"
  rm -rf build/lib build/bdist.*
  python setup.py -q bdist_wheel --ts-only --dist-dir=dist/${SHYFT_VERSION}
  rm -rf build/lib build/bdist.*
  pip install --no-deps --no-index --upgrade --target "${SHYFT_DEPENDENCIES_DIR}/py/${py_version}/${SHYFT_VERSION}" --find-links="dist/${SHYFT_VERSION}" shyft
  # create/update symlink to newest version, ensure to replace / in branch name with _
  git_branch_dir=${git_branch//\//_}
  (pushd "${SHYFT_DEPENDENCIES_DIR}/py/${py_version}">/dev/null && rm -rf "${git_branch_dir}" && ln -s "${SHYFT_VERSION}" "${git_branch_dir}" && popd>/dev/null)
}

#####################
# Main entry point
#
init "$@"

if [ $# -ne 1 ]; then
  echo "usage:" "$0" "(local_dist|build|upload|upload_pypi)"
  echo " - local_dist: bdist_wheel and pip install to SHYFT_DEPENDENCIES_DIR/py_version/shyft_version"
  echo " - build: create conda packages, result placed into dist/conda"
  echo " - upload: upload to conda-site using user and token as specified with SHYFT_OS_ANACONDA_USER, SHYFT_OS_ANACONDA_TOKEN"
  echo " - upload_pypi: upload to pypi.org using SHYFT_TS_PYPI_TOKEN and SHYFT_PYPI_TOKEN"
fi


case $1 in
'local_dist')
  make_local_dist
;;
'build')
  echo -----------------------------
  echo Build conda_recipe  time_series,all
  build_pkg conda_recipe/time_series
  build_pkg conda_recipe/all
;;

'upload')
  echo -----------------------------
  echo upload conda_recipe  time_series,all
  if [[ "$SHYFT_OS_ANACONDA_TOKEN" != "" ]]; then
    token_arg="--token $SHYFT_OS_ANACONDA_TOKEN"
  else
    token_arg=""
    echo "INFO: SHYFT_OS_ANACONDA_TOKEN not specified, will skip anaconda client based uploads"
  fi
  if [[ "$SHYFT_OS_ANACONDA_USER" != "" ]]; then
    user_arg="$SHYFT_OS_ANACONDA_USER"
  else
    user_arg="shyft-os"
    echo "INFO: SHYFT_OS_ANACONDA_USER not specified, using default shyft-os"
  fi
  if [[ "$SHYFT_CONDA_CLI_TOKEN" != "" ]]; then
    conda_cli_token_arg="--token $SHYFT_CONDA_CLI_TOKEN"
    if [[ "$SHYFT_CONDA_CLI_CHANNEL" != "" ]]; then
      conda_cli_channel_arg="$SHYFT_CONDA_CLI_CHANNEL"
    else
      conda_cli_token_arg=""
      echo "WARNING: SHYFT_CONDA_CLI_TOKEN specified, requires SHYFT_CONDA_CLI_CHANNEL, disabling upload for now"
    fi
  else
    conda_cli_token_arg=""
    echo "INFO: SHYFT_CONDA_CLI_TOKEN not specified, will skip conda-repo-cli based uploads"
  fi
  upload_pkg conda_recipe/time_series
  upload_pkg conda_recipe/all

;;

'upload_pypi')
  upload_pkg pypi

;;
*)
  echo "Uknown command: " $1
  ;;
esac
