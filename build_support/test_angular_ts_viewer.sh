#!/bin/bash

set -e # exit on failure set -e 
set -o errexit 
set -o nounset # exit on undeclared vars set -u 
set -o pipefail # exit status of the last command that threw non-zero exit code returned

# Debugging set -x 
# set -o xtrace

exec 3>&1 4>&2
RDIR="$(readlink --canonicalize --no-newline "$(dirname ${0})"/..)"
WDIR="${RDIR}/examples/web/demo/angular-ts-viewer"
cd "${WDIR}"
echo "Make coverage directory (otherwise there will be no coverage report)"
mkdir coverage
echo "Running 'npm install' in WDIR=${WDIR}"
cd "${WDIR}"
if ! npm install --silent --force --no; then
  echo Failed to run npm install --silent --no, check log unsupported packages etc.
  npm install --force --no
  (exit 1)
fi
export CHROMIUM_BIN=/usr/bin/chromium

echo "Runnning 'ng test'"
ng test --no-watch --code-coverage --karma-config=karma-ci.conf.js

