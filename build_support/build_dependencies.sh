#!/bin/bash

set -o errexit # exit on failure set -e set -o nounset # exit on undeclared vars set -u
set -o pipefail # exit status of the last command that threw non-zero exit code returned
# Debugging set -x
# set -o xtrace

export SHYFT_WORKSPACE=${SHYFT_WORKSPACE:=$(readlink --canonicalize --no-newline `dirname ${0}`/../..)}
build_support_dir=$(readlink --canonicalize --no-newline `dirname ${0}`)
# to align the cmake support:
SHYFT_DEPENDENCIES_DIR=${SHYFT_DEPENDENCIES_DIR:=${SHYFT_WORKSPACE}/shyft_dependencies}
armadillo_name=armadillo-11.4.2
dlib_ver=19.24
dlib_name="dlib-${dlib_ver}"
boost_ver=1_80_0
pybind11_ver=v2.4.3
miniconda_ver=latest

cmake_common="-DCMAKE_INSTALL_MESSAGE=NEVER"
echo ---------------
echo Update/build shyft dependencies
echo SHYFT_WORKSPACE........: ${SHYFT_WORKSPACE}
echo SHYFT_DEPENDENCIES_DIR.: ${SHYFT_DEPENDENCIES_DIR}
echo PACKAGES...............: miniconda ${miniconda_ver} w/shyft_env, doctest, boost_${boost_ver}, ${armadillo_name}, ${dlib_name} ,otlv4,Howard Hinnant date

# A helper function to compare versions
function version { echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# the current versions we are building
mkdir -p "${SHYFT_DEPENDENCIES_DIR}"
cd "${SHYFT_DEPENDENCIES_DIR}"

if [ ! -d ${armadillo_name} ]; then 
    echo Building ${armadillo_name}
    if [ ! -f ${armadillo_name}.tar.xz ]; then 
        wget  http://sourceforge.net/projects/arma/files/${armadillo_name}.tar.xz
    fi;
    tar -xf ${armadillo_name}.tar.xz
    pushd ${armadillo_name}
    cmake -B build  -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DDETECT_HDF5=false -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common}
    cmake --build build --config Release --target install
    popd
fi;
echo Done ${armadillo_name}

if [ ! -d ${dlib_name} ]; then
    echo Building ${dlib_name}
    dlib_archive="v${dlib_ver}.tar.gz"
    if [ ! -f "${dlib_archive}" ]; then
        wget "https://github.com/davisking/dlib/archive/${dlib_archive}"
    fi;
    tar -xf "${dlib_archive}"
    pushd ${dlib_name}
    dlib_cfg="-DDLIB_PNG_SUPPORT=0 -DDLIB_GIF_SUPPORT=0 -DDLIB_LINK_WITH_SQLITE3=0 -DDLIB_NO_GUI_SUPPORT=1 -DDLIB_DISABLE_ASSERTS=1 -DDLIB_JPEG_SUPPORT=0 -DDLIB_USE_BLAS=0 -DDLIB_USE_LAPACK=0 -DBUILD_SHARED_LIBS=ON"
    cmake -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" -DCMAKE_INSTALL_LIBDIR=lib ${cmake_common} ${dlib_cfg} && cmake --build build --config Release --target install
    popd
fi;
echo Done ${dlib_name}

if [ ! -d doctest ]; then
    echo Building doctest
    git clone https://github.com/doctest/doctest
    pushd doctest
    cmake -B build -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DDOCTEST_WITH_TESTS=0 -DCMAKE_CXX_STANDARD=17
    cmake -P build/cmake_install.cmake
    popd
fi;
echo Done doctest

if [ ! -d date ]; then
    echo Building Howard Hinnant date extensions to chrono
    git clone https://github.com/HowardHinnant/date
    pushd date
    cmake . -DCMAKE_INSTALL_PREFIX="${SHYFT_DEPENDENCIES_DIR}" ${cmake_common} -DCMAKE_CXX_STANDARD=17
    cmake --build . --target install
    popd
fi;
echo Done HowardHinnant date extensions

cd "${SHYFT_WORKSPACE}"

os_type="Linux-x86_64"
if [[ $OSTYPE == *"darwin"* ]]; then
    os_type="MacOSX-x86_64"
fi

if [ ! "${CONDA_PREFIX:-}" ]; then
  if [ ! -d miniconda/bin ]; then
    echo Building miniconda
    if [ -d miniconda ]; then
        rm -rf miniconda
    fi;
    if [ ! -f miniconda.sh ]; then
        wget  -O miniconda.sh http://repo.continuum.io/miniconda/Miniconda3-${miniconda_ver}-${os_type}.sh
    fi;
    bash miniconda.sh -b -p "${SHYFT_WORKSPACE}"/miniconda

    # Update conda to latest version, assume we start with 4.3 which
    # requires PATH to be set
    OLDPATH=${PATH}
    export PATH="${SHYFT_WORKSPACE}/miniconda/bin:$PATH"

    old_conda_version=$(conda --version | sed "s/conda \(.*\)/\1/")
    echo "Old conda version is ${old_conda_version}"
    if [[ $(version "${old_conda_version}") -ge $(version "4.4") ]]; then
      PATH=$OLDPATH
      source "${SHYFT_WORKSPACE}"/miniconda/etc/profile.d/conda.sh
        conda activate
    else
        source activate
    fi
    conda config --set always_yes yes --set changeps1 no
    conda update conda

    new_conda_version=$(conda --version | sed "s/conda \(.*\)/\1/")
    echo "New conda version is ${new_conda_version}"
    if [[ $(version "${old_conda_version}") -lt $(version "4.4") &&
          $(version "${new_conda_version}") -ge $(version "4.4") ]]; then
        PATH=$OLDPATH
        source "${SHYFT_WORKSPACE}"/miniconda/etc/profile.d/conda.sh
        conda activate
    fi

    py_build_packs="conda-build conda-verify setuptools anaconda-client pip twine 'numpy>=1.20'"
    conda install ${py_build_packs} # needed for conda build and upload build steps
    py_dashboard_packs="bokeh pydot sphinx sphinx_rtd_theme lxml"
    py_shyft_packs="pyyaml 'numpy>=1.20' netcdf4 matplotlib requests pytest coverage pytest-cov pip twine anaconda-client shapely  pyproj pillow"
    py_cf_packs="sphinx-autodoc-typehints 'pyside2>=5.13.2' pint"
    conda create -n shyft_38 --channel main --channel conda-forge python=3.8 ${py_shyft_packs}  ${py_dashboard_packs} ${py_cf_packs}
    conda create -n shyft_39 --channel main --channel conda-forge python=3.9 ${py_shyft_packs}  ${py_dashboard_packs} ${py_cf_packs}
    conda create -n shyft_310 --channel main --channel conda-forge python=3.10 ${py_shyft_packs}  ${py_dashboard_packs} ${py_cf_packs}
    conda activate base
    for e in shyft_38 shyft_39 shyft_310 ; do  # we need the ld lib path set to ensure libpython3.x.so.1.0 is found in inline dev.
        conda env config vars  set LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/projects/miniconda/envs/${e}/lib -n ${e}
    done
  fi;
  echo Done minconda
  #export PATH="${SHYFT_WORKSPACE}/miniconda/bin:$PATH"
fi;
source "${SHYFT_WORKSPACE}/miniconda/etc/profile.d/conda.sh"
echo Enabled miniconda


cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d boost_${boost_ver} ]; then
    echo Building boost_${boost_ver}
    if [ ! -f boost_${boost_ver}.tar.gz ]; then
        wget -O boost_${boost_ver}.tar.gz https://boostorg.jfrog.io/artifactory/main/release/${boost_ver//_/.}/source/boost_${boost_ver}.tar.gz
    fi;
    tar -xf boost_${boost_ver}.tar.gz
    pushd boost_${boost_ver}
    ./bootstrap.sh --prefix="${SHYFT_DEPENDENCIES_DIR}"
    py_root=${SHYFT_WORKSPACE}/miniconda/envs  # here we could tweak using virtual-env, conda or system.. match with cmake
    # have to help boost figure out right python versions bin,inc and libs.
    # first remove any python that was found with the bootstrap step above
    mv -f project-config.jam x.jam
    cat x.jam | sed -e 's/using python/#using python/g' >project-config.jam
    echo "# injected by shyft build/build_dependencies.sh to map explicit python versions" >>project-config.jam
    for pv in 3.8 3.9 3.10; do
        e=${pv/./}
        echo "using python ">>project-config.jam
        echo "    : ${pv} ">>project-config.jam
        echo "    : ${py_root}/shyft_${e}/bin/python " >>project-config.jam
        echo "    : ${py_root}/shyft_${e}/include/python${pv} ${py_root}/shyft_${e}/lib/python${pv}/site-packages/numpy/core/include " >>project-config.jam
        echo "    : ${py_root}/shyft_${e}/lib ;" >>project-config.jam
    done
    boost_packages="--with-system --with-filesystem --with-date_time --with-python --with-serialization --with-chrono --with-regex --with-thread --with-atomic --with-test --with-program_options --with-math python=3.10,3.9,3.8"
    ./b2 -j4 -d0 link=shared variant=release threading=multi ${boost_packages}
    ./b2 -j4 -d0 install link=shared variant=release threading=multi   ${boost_packages}
    popd
fi;
echo  Done boost_${boost_ver}

cd "${SHYFT_DEPENDENCIES_DIR}"
if [ ! -d pybind11 ]; then
    git clone https://github.com/pybind/pybind11.git
    pushd pybind11
    git checkout master
    git pull
    git checkout ${pybind11_ver} > /dev/null
    cmake -B build -DPYTHON_EXECUTABLE=$(which python) -DCMAKE_INSTALL_PREFIX=${SHYFT_DEPENDENCIES_DIR} -DPYBIND11_TEST=0 ${cmake_common} && cmake -P build/cmake_install.cmake
    popd
fi;
echo Done pybind11
cd ${SHYFT_DEPENDENCIES_DIR}

if [ ! -d leveldb ]; then
    git clone https://github.com/google/leveldb
    pushd leveldb
    sed -i "s/add_library(leveldb \"\")/add_library(leveldb \"SHARED\")/g" CMakeLists.txt
    sed -e '/fno-rtti/d' -i CMakeLists.txt
    BUILD_SHARED_LIBS=on cmake -B build -DCMAKE_BUILD_TYPE=Release -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF
    cmake -B build -DLEVELDB_BUILD_TESTS=OFF -DLEVELDB_BUILD_BENCHMARKS=OFF
    cmake --build build
    cmake --install build --prefix ${SHYFT_DEPENDENCIES_DIR}
    popd
fi;
echo Done leveldb

echo Doing the otl header-only otlv4.h
if [ ! -f include/otlv4.h ]; then
    echo ..missing, then download and install otlv4.h
    wget -O otlv4_h2.zip http://otl.sourceforge.net/otlv4_h2.zip
    unzip otlv4_h2.zip -d include
fi;
echo Done otlv4.h



