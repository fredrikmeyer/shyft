import os
import platform
import subprocess
from os import path, chdir, getcwd

import sys
from setuptools import setup, find_packages, Distribution
from sysconfig import get_platform

shyft_license: str = 'LGPL v3'
shyft_author: str = 'shyft'
shyft_author_email: str = 'sigbjorn.helset@gmail.com'
shyft_url: str = 'https://gitlab.com/shyft-os/shyft'

shyft_root = path.dirname(path.realpath(__file__))
chdir(shyft_root)


ext_s: str = '.pyd' if 'Windows' in platform.platform() else '.so'

# Conda does not allow same name for conda-pkg and pypi-pkg
# so we need a post-fix to the name of it
conda_pip: str = ''
conda_pip_opt: str = '--conda-pip'
if conda_pip_opt in sys.argv:
    conda_pip = '_pip'
    sys.argv.remove(conda_pip_opt)  # have to remove it from opts to avoid setup.py complain

def check_opt(opt_str: str) -> bool:
    if opt_str not in sys.argv:
        return False
    print(f"with {opt_str}")
    sys.argv.remove(opt_str)
    return True


# Allow build/setup different package options, including all
with_ts: bool = True
with_hydrology: bool = True
with_dashboard: bool = True
with_energy_market: bool = True

# Allow to build only one partial package
ts_only: bool = False
dashboard_only: bool = False
hydrology_only: bool = False
energy_market_only: bool = False

ts_only = check_opt('--ts-only')
hydrology_only = check_opt('--hydrology-only')
dashboard_only = check_opt('--dashboard-only')
energy_market_only = check_opt('--energy-market-only')
complete_suite = not any([ts_only, hydrology_only, dashboard_only, energy_market_only])


if len([x for x in [ts_only, hydrology_only, dashboard_only, energy_market_only] if x]) > 1:
    print("If --xxx_only, we require just one --xxx-only option flag!")
    raise RuntimeError(f"Multiple --xxx-only options specified")

# Setup the required packages and deps, according to args
if ts_only:
    with_dashboard, with_hydrology, with_energy_market = False, False, False
elif dashboard_only:
    with_hydrology, with_energy_market = False, False
elif hydrology_only:
    with_dashboard, with_energy_market = False, False
elif energy_market_only:
    with_dashboard, with_hydrology = False, False


def ensure_extensions_is_built():
    global hydrology_only, complete_suite, energy_market_only, hydrology_only, dashboard_only
    ext_names = ['shyft/time_series/_time_series' + ext_s]  # current minimum core pgk
    if hydrology_only or complete_suite:
        ext_names.extend(['shyft/hydrology/_api' + ext_s,
             'shyft/hydrology/pt_st_k/_pt_st_k' + ext_s,
             'shyft/hydrology/pt_gs_k/_pt_gs_k' + ext_s,
             'shyft/hydrology/pt_hs_k/_pt_hs_k' + ext_s,
             'shyft/hydrology/pt_hps_k/_pt_hps_k' + ext_s,
             'shyft/hydrology/pt_ss_k/_pt_ss_k' + ext_s,
             'shyft/hydrology/r_pm_gs_k/_r_pm_gs_k' + ext_s,
             'shyft/hydrology/hbv_stack/_hbv_stack' + ext_s,
             ]
        )
    elif energy_market_only or complete_suite:
        ext_names.extend([
            'shyft/energy_market/core/_core' + ext_s,
            'shyft/energy_market/ltm/_ltm' + ext_s,
            'shyft/energy_market/stm/_stm' + ext_s,
            'shyft/energy_market/stm/shop/_shop' + ext_s
            ]
        )

    needs_build_ext = not all([path.exists(ext_name) for ext_name in ext_names])

    if needs_build_ext:
        print('One or more extension modules needs build, attempting auto build')
        for f in ext_names:
            if not path.exists(f):
                print(f'{f}: needs rebuild')

        if "Windows" in platform.platform():
            try:
                print(subprocess.check_output(["call ../build_support/win_build_shyft.cmd"], universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT))
            except:
                print('\nbuild_support/win_build_shyft.cmd build FAILED.\nplease inspect log and build preconditions on gitlab.com/shyft-os/shyft')
                exit()
        elif "Linux" in platform.platform():
            try:
                # For Linux, use the cmake approach for compiling the extensions
                print(subprocess.check_output("sh ../build_support/build_shyft.sh", shell=True))
            except:
                print("Problems compiling shyft, try building with the build_api.sh "
                      "or build_api_cmake.sh (Linux only) script manually...")
                exit()
        else:
            print("Only windows and Linux supported")
            exit()
    else:
        print('Extension modules are already built in place')


ensure_extensions_is_built()
VERSION = os.environ.get("SHYFT_VERSION")

if not VERSION:
    try:
        from shyft.time_series import __version__ as built_version
        VERSION=built_version()
    except :
        if path.exists('../VERSION'):
            VERSION = open('../VERSION').read().strip()
            print(f'VERSION from file:{VERSION}')
        else:
            raise RuntimeError(f"Not able to determine version in dir {shyft_root}")

print(f'Building Shyft version {VERSION} in {shyft_root}, cwd={getcwd()}')


class BinaryDistribution(Distribution):
    """Distribution which always forces a binary package with platform name"""

    def has_ext_modules(self):
        return True


requires = ["numpy"]
install_requires = ["numpy"]
tests_require = ["pytest"]
extras_require = {}
entry_points = {}
packages = ['shyft.time_series', 'shyft.utilities', 'shyft.pyapi']
package_data = {'shyft.time_series': ['_*.so', '_*.pyd',
                                      '../lib/libshyft_core*',
                                      '../lib/boost*.dll', '../lib/libboost*.s*',
                                      '../lib/libarma*.s*',
                                      '../lib/libcrypt*.s*',
                                      '../lib/libssl*.s*',
                                      '../lib/libdlib*.s*',
                                      '../lib/libleveldb*.s*',
                                      '../lib/libopen*.dll', '../lib/*win64_MT.dll'
                                      ]}

package_data[''] = []

pkg_dash_extra = ['../util/layoutgraph/layout_configuration.yml']
pkg_pyext_extra = ['_*.so', '_*.pyd']
pkg_hydro_extra = ['../lib/*hydrology*']
pkg_em_extra = ['../energy_market/service/install/*.s*']
pkg_em_ts_extra = ['../lib/SHOP_license.dat', '../lib/*cplex*.*', '../lib/*em_*', '../lib/libstm_*']

if with_dashboard:
    packages.extend([f'shyft.{p}' for p in find_packages('shyft') if p.startswith('dashboard') or p.startswith('util')])
    package_data['shyft.time_series'].extend(pkg_dash_extra)
    extras_require['dashboard'] = ['bokeh', 'pint', 'pydot']
    entry_points['console_scripts'] = [
            'shyft-dashboard-examples = shyft.dashboard.entry_points.start_bokeh_examples:main',
            'shyft-dashboard-visualisation = shyft.dashboard.entry_points.visualize_apps:main'
        ]
    entry_points['shyft_dashboard_apps'] = [
            'dtss_viewer_app = shyft.dashboard.apps.dtss_viewer.dtss_viewer_app:DtssViewerApp'
        ]

if with_hydrology:
    packages.extend([f'shyft.{p}' for p in find_packages('shyft') if p.startswith('hydrology')
                     or p.startswith('repository')
                     or p.startswith('orchestration')
                     or p.startswith('viz')
                     or p.startswith('api')
                     ])
    package_data[''] = pkg_pyext_extra
    package_data['shyft.time_series'].extend(pkg_hydro_extra)
    extras_require['repositories'] = ['netcdf4', 'shapely', 'pyyaml', 'pyproj']
    extras_require['viz'] = ['matplotlib']
    extras_require['notebooks'] = ['jupyter']

if with_energy_market:
    packages.extend([f'shyft.{p}' for p in find_packages('shyft') if p.startswith('energy_market')])
    package_data['shyft.time_series'].extend(pkg_em_ts_extra)
    package_data[''] = list(set(package_data[''] + pkg_pyext_extra + pkg_em_extra))
    extras_require['energy_market_ui'] = ['pyside2']

if ts_only:
    name = f'shyft.time_series{conda_pip}'
    description = 'Open Shyft for advanced high performance time-series handling'
elif dashboard_only:
    name = f'shyft.dashboard{conda_pip}'
    description = 'Open Shyft dashboard with shyft.time_series',
elif hydrology_only:
    name = f'shyft.hydrology{conda_pip}'
    description = 'Open Shyft hydrology for distributed hydrological forecasting, including shyft.time_series'
elif energy_market_only:
    name = f'shyft.energy_market{conda_pip}'
    description = 'Open Shyft for the energy-market including shyft.time_series'
else:
    name = f'shyft{conda_pip}'
    description = 'Open Shyft, - An framework providing python enabled tools and services for the energy-market, hydrological forecasting and advanced time-series',

setup(
    name=name,
    version=VERSION,
    author=shyft_author,
    author_email=shyft_author_email,
    url=shyft_url,
    description=description,
    license=shyft_license,
    packages=packages,
    package_data=package_data,
    requires=requires,
    install_requires=install_requires,
    tests_require=tests_require,
    platforms=[get_platform()],
    zip_safe=False,
    distclass=BinaryDistribution,
    extras_require=extras_require,
    entry_points=entry_points
)
