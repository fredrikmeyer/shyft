
from shyft.dashboard.maps.map_viewer import MapViewer
from shyft.dashboard.maps.layer_data import LayerDataHandle
from shyft.dashboard.maps.map_layer import MapLayer, MapLayerType

#from shyft.dashboard.test.maps.test_map_fixtures import basemap_factory

import bokeh.plotting as bokeh_plotting


def test_map_viewer_base_map(base_map):
    test_file = 'test_map_viewer.html'
    bokeh_plotting.output_file(test_file)

    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer.fig_axes_ranges.set_axes_bounds(26621 + 22373 * 0.5, 6599653 + 18731 * 0.5,
                                               26621 + 22373 * 0.5 + 100, 6599653 + 18731 * 0.5 + 100)

    #bokeh_plotting.show(map_viewer.layout)


def test_map_viewer_adding_layers(base_map):
    test_file = 'test_map_viewer.html'
    bokeh_plotting.output_file(test_file)

    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer.fig_axes_ranges.set_axes_bounds(26621 + 22373 * 0.5, 6599653 + 18731 * 0.5,
                                               26621 + 22373 * 0.5 + 100, 6599653 + 18731 * 0.5 + 100)
    map_viewer.fig_axes_ranges.padding = 10
    assert not map_viewer.layers
    layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT, glyph_fixed_kwargs={'radius': 5},
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'},
                      )
    assert layer1 in map_viewer.layers
    assert layer1.renderer == map_viewer.layers[layer1]
    layer1.updated_data({'x': [26621 + 22373 * 0.5 + 50],
                         'y': [6599653 + 18731 * 0.5 + 50]})
    map_viewer.update_axes()

    #bokeh_plotting.show(map_viewer.layout)


def test_map_viewer_update_layers(base_map):
    test_file = 'test_map_viewer.html'
    bokeh_plotting.output_file(test_file)

    map_viewer = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer.fig_axes_ranges.set_axes_bounds(26621, 6599653,
                                               26721, 6599753)
    map_viewer.fig_axes_ranges.padding = 10
    assert not map_viewer.layers
    layer1 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                      glyph_fixed_kwargs={'radius': 5},
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'},
                      )
    assert layer1 in map_viewer.layers
    assert layer1.renderer == map_viewer.layers[layer1]

    print(map_viewer.fig_axes_ranges.axes_bounds)
    pad = map_viewer.fig_axes_ranges.padding
    assert map_viewer.fig_axes_ranges.axes_bounds == (26621-pad, 6599653-pad, 26721+pad, 6599753+pad)

    layer2 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                      glyph_fixed_kwargs={'radius': 5},
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'},
                      )
    layer3 = MapLayer(map_viewer=map_viewer, name='first layer', layer_type=MapLayerType.POINT,
                      glyph_fixed_kwargs={'radius': 5},
                      glyph_variable_kwargs={'x': 'x', 'y': 'y'},
                      update_axes=False
                      )

    min_x = 26621 + 22373*0.5 + 50
    min_y = 6599653 + 18731*0.5 + 50
    max_x = 26621 + 22373*0.5 + 100
    max_y = 6599653 + 18731*0.5 + 100

    updated_data = {'x': [min_x, max_x],
                    'y': [min_y, max_y]}
    layer_data_handle1 = LayerDataHandle(map_layer=layer1, data=updated_data)

    updated_data = {'x': [min_x-100, max_x+100],
                    'y': [min_y-100, max_y+100]}
    layer_data_handle3 = LayerDataHandle(map_layer=layer3, data=updated_data)

    map_viewer.receive_layer_data_handles([layer_data_handle1, layer_data_handle3])

    # target bounds should still be layer_data_handle1 since layer3 should not be considere in update
    target_bounds = (min_x-map_viewer.fig_axes_ranges.padding, min_y-map_viewer.fig_axes_ranges.padding,
                     max_x+map_viewer.fig_axes_ranges.padding, max_y+map_viewer.fig_axes_ranges.padding)

    assert map_viewer.fig_axes_ranges.axes_bounds == target_bounds

    map_viewer2 = MapViewer(width=300, height=300, base_map=base_map)
    map_viewer2.receive_layer_data_handles([layer_data_handle1])

    #bokeh_plotting.show(map_viewer.layout)

