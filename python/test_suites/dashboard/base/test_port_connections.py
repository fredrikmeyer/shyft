import warnings
from typing import Optional, Union

import pytest

from shyft.dashboard.base.ports import (States, Sender, Receiver,
                                        PortConnectionError,
                                        PortConnectionWarning, connect_ports,
                                        StatePorts, connect_state_ports,
                                        connect_ports_and_state_ports,
                                        disconnect_ports,
                                        PortError)


def test_receiver_port_type_annotation():

    def receive_func_simple(x: int) -> None:
        pass

    with pytest.raises(PortError) as e_info:
         Receiver(parent='parent', name='receiver_A', func=receive_func_simple, signal_type=str)


def test_connect_ports():
    warnings.catch_warnings(record=True)

    def _receive_something_simple(x: int) -> None:
        pass

    def _receive_something_complex(x: Optional[Union[float, int]]) -> None:
        pass

    send_something_simple = Sender(parent='parent', name='sender_A', signal_type=int)
    receive_something_simple = Receiver(parent='parent', name='receiver_A', func=_receive_something_simple,
                                        signal_type=int)

    send_something_complex = Sender(parent='parent', name='sender_B', signal_type=Optional[Union[float, int]])
    receive_something_complex = Receiver(parent='parent', name='receiver_B', func=_receive_something_complex,
                                     signal_type=Optional[Union[float, int]])

    # test if wrong passed classes are correct
    with pytest.raises(PortConnectionError) as e_info:
        connect_ports(send_something_simple, 'a')

    assert not send_something_simple.connected

    # test if sender and receiver are not same
    with pytest.raises(PortConnectionError) as e_info:
        connect_ports(send_something_simple, receive_something_complex)

    assert not send_something_simple.connected

    # test simple type
    assert connect_ports(send_something_simple, receive_something_simple)

    # test connect function
    assert send_something_simple.connected
    assert receive_something_simple.connected

    # test complex types
    assert connect_ports(send_something_complex, receive_something_complex)

    # connect complex types twice to check if warning is raised
    with pytest.warns(PortConnectionWarning) as record:
        # check that return type is true
        assert connect_ports(send_something_complex, receive_something_complex)

    # check that only one warning was raised
    assert len(record) == 1
    assert send_something_complex.port_name in record[0].message.args[0]
    assert receive_something_complex.port_name in record[0].message.args[0]


def test_connect_state_ports():
    class FooBar:
        def __init__(self, assert_state) -> None:
            self.assert_state = assert_state
            self.call_count = 0
            self.state_port = StatePorts(parent=self, _receive_state=self._receive_state)

        def _receive_state(self, state: States) -> None:
            assert state == self.assert_state
            self.call_count = self.call_count + 1

    foo = FooBar(assert_state=States.DEACTIVE)
    bar = FooBar(assert_state=States.ACTIVE)
    connect_state_ports(foo.state_port, bar.state_port)
    foo.state_port.send_state(States.ACTIVE)
    assert bar.call_count == 1
    foo.state_port.receive_state(States.DEACTIVE)
    assert foo.call_count == 1


def test_connect_ports_and_state_ports():
    class FooBar:

        def __init__(self) -> None:
            self.call_count = 0
            self.state_port = StatePorts(parent=self, _receive_state=self._receive_state)
            self.state = States.DEACTIVE

            self.send_foobar = Sender(parent=self, name='send_foobar', signal_type=str)
            self.receive_foobar = Receiver(parent=self, name='receive_foobar', func=self._receive_foobar,
                                           signal_type=str)

        def _receive_state(self, state: States) -> None:
            self.state = state
            self.call_count = self.call_count + 1

        def activate_send_foobar(self) -> None:
            self.call_count = self.call_count + 1
            if self.state == States.ACTIVE:
                self.state_port.send_state(States.ACTIVE)
                self.send_foobar('foobar')
            else:
                self.send_foobar('')

        def _receive_foobar(self, foobar: str) -> None:
            self.call_count = self.call_count + 1
            assert self.state == States.ACTIVE
            assert foobar == 'foobar'

    foo = FooBar()
    bar = FooBar()
    assert connect_ports_and_state_ports(foo.send_foobar, bar.receive_foobar)

    foo.state_port.receive_state(States.ACTIVE)
    assert foo.call_count == 1
    assert bar.call_count == 0

    foo.activate_send_foobar()
    assert foo.call_count == 2
    assert bar.call_count == 2


def test_circular_connection():

    class Foo:

        def __init__(self) -> None:
            """
            Class with 2 ports, once it receives something it send s it right away
            """
            self.send_foobar = Sender(parent=self, name='send_foobar', signal_type=str)
            self.receive_foobar = Receiver(parent=self, name='receive_foobar', func=self._receive_foobar,
                                           signal_type=str)

        def _receive_foobar(self, foobar: str) -> None:
            self.send_foobar(foobar)

    foo = Foo()
    # connect ports should work fine since check of loops is only possible on runtime
    assert connect_ports(foo.send_foobar, foo.receive_foobar)

    with pytest.raises(PortConnectionError) as e_info:
        foo.send_foobar('foobar')


def test_connect_and_disconnect_ports():
    call_count = [0]

    def _receive_number(obj: int) -> None:  # create a simple receiving function
        assert isinstance(obj, int)
        call_count[0] += 1

    # create a receiver port
    receive_number = Receiver(parent='parent', name='receiver_A', func=_receive_number, signal_type=int)

    # create a sender port
    # Note: the signal_type=int matches the type annotation of obj in the receive_func_simple
    send_number = Sender(parent='parent', name='sender_A', signal_type=int)

    # connect the 2 ports
    connect_ports(port_sender=send_number, port_receiver=receive_number)

    # send a number --> receive it, and print it to console
    send_number(20)
    assert call_count[0] == 1

    # disconnect the 2 ports
    disconnect_ports(port_sender=send_number, port_receiver=receive_number)

    send_number(20)
    assert call_count[0] == 1
