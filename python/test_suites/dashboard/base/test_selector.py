import logging
import time
from typing import Dict, List, Tuple

from shyft.dashboard.base.ports import (States, Receiver, Sender, connect_ports)
from shyft.dashboard.base.selector_model import (processing_wrapper, SelectorModelBase)
from shyft.dashboard.base.selector_presenter import (SelectorPresenter)
from shyft.dashboard.base.selector_views import TwoSelect, Select, MultiSelect




def test_selector_presenter_single_select(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    view = Select(title="test single select")
    pres = SelectorPresenter(name='single select', view=view, logger=logger) #,

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _receive_selection_to_assert(selected_value: List[str]):
        assert isinstance(selected_value, list)
        assert {'foo'} == set(selected_value)
        call_count[0] += 1

    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert selection', func=_receive_selection_to_assert,
                                           signal_type=List[str])
    # connect our function to selector model
    connect_ports(pres.send_selection_to_model, receive_selection_to_assert)

    # setting options without running callback
    options = ['foo', 'bar']
    pres.set_selector_options(options, callback=False, sort=False)
    assert call_count[0] == 0
    options.append(pres.default)
    assert set(pres.selector_options) == set(options)

    # set selection with callback
    pres.set_selector_value('foo', callback=True)
    assert call_count[0] == 1

    # set selection without the callback
    pres.set_selector_value('bar', callback=False)
    assert call_count[0] == 1
    assert set(pres.selector_values) == {'bar'}

    # set selection with callback and list as argument
    pres.set_selector_value(['foo'], callback=True)
    assert call_count[0] == 2

    # set selection without callback and list as argument
    pres.set_selector_value(['bar'], callback=False)
    assert call_count[0] == 2
    assert set(pres.selector_values) == {'bar'}

    # set options with running the callback
    options = ['bar', 'foo', 'foo-bar', 'mega']
    pres.set_selector_options(options, callback=True, sort=False, selected_value='foo')
    options.append(pres.default)

    assert set(pres.selector_options) == set(options)
    assert call_count[0] == 3

    pres.set_selector_value('not in options', callback=False)
    assert call_count[0] == 3
    assert log_list[-1] == "SelectorPresenter 'single select' with view: Select 'test single select': '['not in options']' is not in options of selector"


def test_selector_presenter_two_select():

    view = TwoSelect(title="two select view")
    pres = SelectorPresenter(name='two select', view=view)

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _receive_selection_to_assert(selected_value: List[str]):
        assert isinstance(selected_value, list)
        assert len(selected_value) == 2
        assert {'foo', 'bar'} == set(selected_value)
        call_count[0] += 1

    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert selection', func=_receive_selection_to_assert,
                                           signal_type=List[str])
    # connect our function to selector model
    connect_ports(pres.send_selection_to_model, receive_selection_to_assert)

    # setting options without running callback
    options = ['foo', 'bar', 'mega']
    pres.set_selector_options(options, callback=False, sort=False)
    # add default
    options.append(pres.default)
    assert set(pres.selector_options) == set(options)
    # both selector options should be equal options + default
    assert set(view._select1.options) == set(options)
    assert set(view._select2.options) == set(options)

    # set selection with callback
    pres.set_selector_value(['foo', 'bar'], callback=True)
    assert call_count[0] == 1
    # both selector options different
    assert set(view._select1.options) == {pres.default, 'foo', 'mega'}  # no bar
    assert set(view._select2.options) == {pres.default, 'bar', 'mega'}  # no foo

    # set selection without the callback
    pres.set_selector_value(['bar', pres.default], callback=False)
    assert call_count[0] == 1
    assert pres.selector_values == ['bar', pres.default]
    # both selector options different
    assert set(view._select1.options) == {pres.default, 'foo', 'bar', 'mega'}  #
    assert set(view._select2.options) == {pres.default, 'foo', 'mega'}  # no foo

    # set options with running the callback
    options = ['bar', 'foo', 'foo-bar', 'mega']
    pres.set_selector_options(options, callback=True, sort=False, selected_value=['foo', 'bar'])
    options.append(pres.default)
    assert set(pres.selector_options) == set(options)
    assert call_count[0] == 2
    assert set(view._select1.options) == {pres.default, 'foo', 'mega', 'foo-bar'}  # no bar
    assert set(view._select2.options) == {pres.default, 'bar', 'mega', 'foo-bar'}  # no foo


def test_selector_presenter_two_select_with_tuple_options():

    view = TwoSelect(title="two select view")
    pres = SelectorPresenter(name='two select', view=view)

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _receive_selection_to_assert(selected_value: List[str]):
        assert isinstance(selected_value, list)
        assert len(selected_value) == 2
        assert {'foo', 'bar'} == set(selected_value)
        call_count[0] += 1

    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert selection', func=_receive_selection_to_assert, signal_type=List[str])
    # connect our function to selector model
    connect_ports(pres.send_selection_to_model, receive_selection_to_assert)

    # setting options without running callback
    options = [('foo', 'choose foo'), ('bar', 'choose bar'), ('mega', 'choose mega')]
    pres.set_selector_options(options, callback=False, sort=False)
    # add default
    options.append(pres.default)
    assert set(pres.selector_options) == set(options)
    # both selector options should be equal options + default
    assert set(view._select1.options) == {pres.default, 'choose foo', 'choose bar', 'choose mega'}
    assert set(view._select2.options) == {pres.default, 'choose foo', 'choose bar', 'choose mega'}

    # set selection with callback
    pres.set_selector_value(['foo', 'bar'], callback=True)
    assert call_count[0] == 1
    # both selector options different
    assert set(view._select1.options) == {pres.default, 'choose foo', 'choose mega'}  # no bar
    assert set(view._select2.options) == {pres.default, 'choose bar', 'choose mega'}  # no foo


def test_selector_presenter_multi_select():

    view = MultiSelect(title="test multiselect",)
    pres = SelectorPresenter(name='multi select', view=view)  # ,

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _receive_selection_to_assert(selected_value: List[str]):
        assert isinstance(selected_value, list)
        assert {'foo', 'bar'} == set(selected_value)
        call_count[0] += 1

    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert selection', func=_receive_selection_to_assert, signal_type=List[str])
    # connect our function to selector model
    connect_ports(pres.send_selection_to_model, receive_selection_to_assert)

    # setting options without running callback
    options = ['foo', 'bar', 'mega']
    pres.set_selector_options(options, callback=False, sort=False)
    options.append(pres.default)
    assert set(pres.selector_options) == set(options)

    # set selection with callback
    pres.set_selector_value(['foo', 'bar'], callback=True)
    assert call_count[0] == 1

    # set selection without the callback
    pres.set_selector_value('bar', callback=False)
    assert call_count[0] == 1
    assert pres.selector_values == ['bar']

    # set options with running the callback
    options = ['bar', 'foo', 'foo-bar', 'mega']
    pres.set_selector_options(options, callback=True, sort=False, selected_value=['foo', 'bar'])
    options.append(pres.default)
    assert set(pres.selector_options) == set(options)
    assert call_count[0] == 2


def test_no_blank_option_for_select_when_default_set_to_none():

    view = MultiSelect(title="test multiselect no blank")
    pres = SelectorPresenter(name='multi select', default=None, view=view)

    options = ['foo', 'bar', 'mega']
    pres.set_selector_options(options, callback=False, sort=False)
    selector_options = pres.selector_options

    assert '' not in selector_options
    assert len(selector_options) == len(options)


def test_custom_selector_example(logger_and_list_handler):
    logger, log_list = logger_and_list_handler

    class MyDataSelector(SelectorModelBase):

        def __init__(self, presenter: SelectorPresenter, sleep_time, logger=None) -> None:
            super().__init__(presenter=presenter, logger=logger)

            # sleep time to simulate tedious loading
            self.sleep_time = sleep_time  # s

            # the current domain model object
            self.current_dom_object = {}

            # add ports for receiving dom and sending selection
            self.receive_dom = Receiver(parent=self, name='receive dom obj', func=self._receive_dom_objects,
                                        signal_type=Dict[str, float])
            self.send_modified_dom = Sender(parent=self, name='send processed dom obj', signal_type=Dict[str, float])

        # --- definition of all abstract methods ---
        def on_change_selected(self, new_values: List[str]):
            # send state information that the state is going to change and deactivate all connected widgets
            self.state_port.send_state(States.DEACTIVE)
            # process selection
            processed_values = self.process_selection_evaluation(new_values)
            if processed_values:
                # update the state of the connected widgets that they are ready to receive the result
                self.state_port.send_state(States.ACTIVE)
                # send the result
                self.send_modified_dom(processed_values)

        def _receive_state(self, state: States) -> None:
            if state == States.ACTIVE:
                self.state = state
                self.presenter.state_ports.receive_state(States.ACTIVE)
                # Not sending active state since this only done if we can send data to the next widget
            elif state == States.DEACTIVE:
                self.state = state
                self.presenter.state_ports.receive_state(States.DEACTIVE)
                self.state_port.send_state(state)
            else:
                self.logger.error(f"ERROR: {self} - not handel for received state {state} implemented")
                self.state_port.send_state(state)

        # --- definition of the custom processing receiver and sender functions ---
        @processing_wrapper  # decorator @processing_wrapper: changes state of the presentation model, which notifies user
        def loading_function(self, new_dom: Dict[str, float]) -> List[Tuple[str, str]]:
            # There are two possibilities for setting up the option list for the presentation mode:
            #
            # 1. List[label]: Each drop down element is represented by a label. The value returned by the widget to the
            # on_change_selected callback (below) is equal to the label
            # 2. List[(key, label)]: For each drop down element we create a tuple with 2 strings: (key, label),
            # where label is what you see in the bokeh-widget.
            # Key is the value returned by the widget to the on_change_selected callback (below).
            self.current_dom_object = new_dom
            return [(k, f'{k}: {v}, return {v}*11+5') for k, v in new_dom.items()]

        @processing_wrapper  # decorator @processing_wrapper: changes state of the presentation model, which notifies user
        def process_selection_evaluation(self, new_values: List[str]) -> Dict[str, float]:
            # process what to do with the new value
            processed_values = {n: self.current_dom_object[n] * 11 + 5 for n in new_values if
                                n in self.current_dom_object}
            # mimick a tedious load function we sleep for a while
            time.sleep(self.sleep_time)
            # return processed value
            return processed_values

        def _receive_dom_objects(self, dom_obj: Dict[str, float]):
            # if the state is not active do nothing
            if self.state == States.DEACTIVE:
                return
            # update the selector_view options without sending the callback function
            self.presenter.set_selector_options(self.loading_function(dom_obj), callback=False, sort=True,
                                                sort_reverse=False, selected_value=None)

    view = Select(title='Custom select model', width=400)
    # create presentation model
    presenter = SelectorPresenter(name="Custom DOM selector model", view=view)
    selector_model = MyDataSelector(presenter=presenter, sleep_time=0)

    call_count = [0]

    # create a port function to print our dom objt to the console
    def _receive_selection_to_assert(dom_obj: Dict[str, float]):
        assert 'foo' in dom_obj.keys()
        assert 2359 == dom_obj['foo']
        call_count[0] += 1
    # create a assert port
    receive_selection_to_assert = Receiver(parent='parent', name='assert_dom', func=_receive_selection_to_assert, signal_type=Dict[str, float])
    # connect our function to selector model
    connect_ports(selector_model.send_modified_dom, receive_selection_to_assert)

    # create a dom obj
    dom = {'foo': 214, 'bar': 585, 'foo-bar': 452854, 'bar-foo': 1423}
    # feed in the dom object
    selector_model.receive_dom(dom)
    assert selector_model.presenter.selector_options == ['',
                                                         ('bar-foo', f"bar-foo: {dom['bar-foo']}, return {dom['bar-foo']}*11+5"),
                                                         ('bar', f"bar: {dom['bar']}, return {dom['bar']}*11+5"),
                                                         ('foo-bar', f"foo-bar: {dom['foo-bar']}, return {dom['foo-bar']}*11+5"),
                                                         ('foo', f"foo: {dom['foo']}, return {dom['foo']}*11+5")]

    # trigger the selection
    presenter.set_selector_value('foo', callback=True)
    assert call_count[0] == 1

    # test if activate and deactivate is working properly

    # deactivate widget
    selector_model.state_port.receive_state(States.DEACTIVE)
    assert selector_model.presenter.selector_options == [selector_model.presenter.default]
    # check if it does somthing when deactivated
    presenter.set_selector_value('foo', callback=True)
    assert call_count[0] == 1
    assert selector_model.presenter.selector_options == [selector_model.presenter.default]
    assert log_list[-1] == "SelectorPresenter 'Custom DOM selector model' with view: Select 'Custom select model': '['foo']' is not in options of selector"

    # reactivate widget and check again
    selector_model.state_port.receive_state(States.ACTIVE)
    assert selector_model.presenter.selector_options == ['',
                                                         ('bar-foo', f"bar-foo: {dom['bar-foo']}, return {dom['bar-foo']}*11+5"),
                                                         ('bar', f"bar: {dom['bar']}, return {dom['bar']}*11+5"),
                                                         ('foo-bar', f"foo-bar: {dom['foo-bar']}, return {dom['foo-bar']}*11+5"),
                                                         ('foo', f"foo: {dom['foo']}, return {dom['foo']}*11+5")]
    # check if it does somthing when deactivated
    selector_model.receive_dom(dom)
    presenter.set_selector_value('foo', callback=True)
    assert call_count[0] == 2
