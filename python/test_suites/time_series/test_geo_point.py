from shyft import time_series as api

"""Verify and illustrate GeoPoint exposure to python
   The GeoPoint plays an important role when you create
   cells, and when you have geo located time-series that
   you feed into shyft
 """


def test_create():
    p1 = api.GeoPoint(1, 2, 3)

    assert round(abs(p1.x - 1), 7) == 0
    assert round(abs(p1.y - 2), 7) == 0
    assert round(abs(p1.z - 3), 7) == 0


def test_create_default():
    p2 = api.GeoPoint()
    assert round(abs(p2.x - 0), 7) == 0
    assert round(abs(p2.y - 0), 7) == 0
    assert round(abs(p2.z - 0), 7) == 0


def test_difference_of_two():
    a = api.GeoPoint(1, 2, 3)
    b = api.GeoPoint(3, 4, 8)
    d = api.GeoPoint_difference(b, a)
    assert round(abs(d.x - (b.x - a.x)), 7) == 0


def test_xy_distance():
    a = api.GeoPoint(1, 2, 3)
    b = api.GeoPoint(3, 4, 8)
    d = api.GeoPoint_xy_distance(b, a)
    assert round(abs(d - 2.8284271247), 7) == 0


def test_create_from_x_y_z_vector():
    x = api.DoubleVector([1.0, 4.0, 7.0])
    y = api.DoubleVector([2.0, 5.0, 8.0])
    z = api.DoubleVector([3.0, 6.0, 9.0])
    gpv = api.GeoPointVector.create_from_x_y_z(x, y, z)
    for i in range(3):
        assert round(abs(gpv[i].x - (3*i + 1)), 7) == 0
        assert round(abs(gpv[i].y - (3*i + 2)), 7) == 0
        assert round(abs(gpv[i].z - (3*i + 3)), 7) == 0


