from shyft.time_series import Calendar, deltahours, TimeAxisFixedDeltaT, TimeSeries, point_interpretation_policy, time_shift


def test_time_shift():
    c = Calendar()
    t0 = c.time(2016, 1, 1)
    t1 = c.time(2017, 1, 1)
    dt = deltahours(1)
    n = 240
    ta = TimeAxisFixedDeltaT(t0, dt, n)
    ts0 = TimeSeries(ta=ta, fill_value=3.0, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    tsa = TimeSeries('a')

    ts1 = time_shift(tsa, t1 - t0)
    assert ts1.needs_bind()
    ts1_blob = ts1.serialize()
    ts1 = TimeSeries.deserialize(ts1_blob)
    tsb = ts1.find_ts_bind_info()
    assert len(tsb) == 1
    tsb[0].ts.bind(ts0)
    ts1.bind_done()
    assert not ts1.needs_bind()

    ts2 = 2.0*ts1.time_shift(t0 - t1)  # just to verify it still can take part in an expression

    for i in range(ts0.size()):
        assert round(abs(ts0.value(i) - ts1.value(i)), 3) == 0, "expect values to be equal"
        assert round(abs(ts0.value(i)*2.0 - ts2.value(i)), 3) == 0, "expect values to be double value"
        assert ts0.time(i) + (t1 - t0) == ts1.time(i), "expect time to be offset delta_t different"
        assert ts0.time(i) == ts2.time(i), "expect time to be equal"
