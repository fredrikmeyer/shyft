from shyft.time_series import (TimeSeries, TsVector, TimeAxis, time, POINT_AVERAGE_VALUE)


def test_ts_clone_expression():
    a = TimeSeries("a")
    e = a*3.0
    b = e.clone_expression()
    assert e.needs_bind()

    x = TimeSeries(TimeAxis(time(0), time(3600), 10), fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    y = TimeSeries(TimeAxis(time(0), time(3600), 10), fill_value=2.0, point_fx=POINT_AVERAGE_VALUE)
    a.bind(x)
    e.bind_done()
    assert not e.needs_bind()

    bi = b.find_ts_bind_info()
    assert len(bi) == 1
    bi[0].ts.bind(y)
    b.bind_done()
    assert not b.needs_bind()
    assert e != b, 'ts should be different since we did copy the expressional part of it'
    b1=b.evaluate()
    b.unbind()
    assert b.needs_bind()
    bi=b.find_ts_bind_info()
    assert len(bi) == 1
    bi[0].ts.bind(y)
    b.bind_done()
    b2=b.evaluate()
    assert b1==b2


def test_tsv_clone_expression():
    a = TimeSeries("a")
    e = TsVector([a*3.0])
    b = e.clone_expression()
    assert e[0].needs_bind()

    x = TimeSeries(TimeAxis(time(0), time(3600), 10), fill_value=1.0, point_fx=POINT_AVERAGE_VALUE)
    y = TimeSeries(TimeAxis(time(0), time(3600), 10), fill_value=2.0, point_fx=POINT_AVERAGE_VALUE)
    a.bind(x)
    e[0].bind_done()
    assert not e[0].needs_bind()

    bi = b[0].find_ts_bind_info()
    assert len(bi) == 1
    bi[0].ts.bind(y)
    b[0].bind_done()
    assert not b[0].needs_bind()
    assert e != b, 'ts should be different since we did copy the expressional part of it'
