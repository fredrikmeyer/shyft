import re
import tempfile
import pytest

import numpy as np
from numpy.testing import assert_array_almost_equal

from shyft.time_series import Calendar
from shyft.time_series import DtsClient
from shyft.time_series import DtsServer
from shyft.time_series import IntVector
from shyft.time_series import UtcTimeVector
from shyft.time_series import StringVector
from shyft.time_series import TimeAxis
from shyft.time_series import TimeSeries
from shyft.time_series import TsInfo
from shyft.time_series import TsInfoVector
from shyft.time_series import TsVector
from shyft.time_series import UtcPeriod
from shyft.time_series import deltahours
from shyft.time_series import point_interpretation_policy as point_fx
from shyft.time_series import utctime_now
from shyft.time_series import ts_stringify
from shyft.time_series import __version__ as shyft_lib_version
from shyft.time_series import DtssCfg, shyft_url

def shyft_store_url(name: str) -> str:
    return "shyft://test/{}_øæå".format(name)  # shyft:// maps to internal, test= container-name


fake_store_container = "netcdf://dummy.nc"


def fake_store_url(name: str) -> str:
    return "{}/ts{}".format(fake_store_container, name)  #


class DtssTestCase:
    """Verify and illustrate dtss, distributed ts service

     """

    def __init__(self):
        self.callback_count = 0
        self.find_count = 0
        self.ts_infos = TsInfoVector()
        self.rd_throws = False
        self.cache_reads = False
        self.cache_dtss = None
        utc = Calendar()
        t_now = utctime_now()
        self.stored_tsv = list()

        for i in range(30):
            self.ts_infos.append(
                TsInfo(
                    name=fake_store_url('{0}'.format(i)),
                    point_fx=point_fx.POINT_AVERAGE_VALUE,
                    delta_t=deltahours(1),
                    olson_tz_id='',
                    data_period=UtcPeriod(utc.time(2017, 1, 1), utc.time(2018, 1, 1)),
                    created=t_now,
                    modified=t_now
                )
            )

    def dtss_read_callback(self, ts_ids: StringVector, read_period: UtcPeriod) -> TsVector:
        self.callback_count += 1
        r = TsVector()
        ta = TimeAxis(read_period.start, deltahours(1), int(read_period.timespan()//deltahours(1)))
        if self.rd_throws:
            self.rd_throws = False
            print(f'read throws')
            raise RuntimeError("read-ts-problem")
        for ts_id in ts_ids:
            r.append(TimeSeries(ta, fill_value=1.0, point_fx=point_fx.POINT_AVERAGE_VALUE))

        if self.cache_reads and self.cache_dtss:  # illustrate how the read-callback can ask the dtss to cache it's reads
            self.cache_dtss.cache(ts_ids, r)

        return r

    def dtss_find_callback(self, search_expression: str) -> TsInfoVector:
        self.find_count += 1
        r = TsInfoVector()
        if self.rd_throws:
            print(f'find throws')
            self.rd_throws = False  # we hope py do the right thing, release r here.
            raise ValueError("Any exception is translated to runtime-error")

        prog = re.compile(search_expression)
        for tsi in self.ts_infos:
            if prog.fullmatch(tsi.name):
                r.append(tsi)
        return r

    def dtss_store_callback(self, tsv: TsVector) -> None:
        print(f'storing stuff')
        self.stored_tsv.append(tsv)

    def test_functionality_hosting_localhost(self):

        # setup data to be calculated
        utc = Calendar()
        d = deltahours(1)
        d24 = deltahours(24)
        n = 240
        n24 = 10
        t = utc.time(2016, 1, 1)
        ta = TimeAxis(t, d, n)
        ta24 = TimeAxis(t, d24, n24)
        n_ts = 100
        percentile_list = IntVector([0, 35, 50, 65, 100])
        tsv = TsVector()
        store_tsv = TsVector()  # something we store at server side
        for i in range(n_ts):
            pts = TimeSeries(ta, np.linspace(start=0, stop=1.0, num=ta.size()),
                             point_fx.POINT_AVERAGE_VALUE)
            tsv.append(float(1 + i/10)*pts)
            store_tsv.append(TimeSeries("cache://test/{0}".format(i), pts))  # generate a bound pts to store

        dummy_ts = TimeSeries('dummy://a')
        tsv.append(dummy_ts.integral(ta))
        assert len(ts_stringify(tsv[0])) > 10  # just ensure ts_stringify work on expr.
        # then start the server
        dtss = DtsServer()
        dtss.cb = self.dtss_read_callback
        dtss.find_cb = self.dtss_find_callback
        dtss.store_ts_cb = self.dtss_store_callback
        port_no = dtss.start_server()
        host_port = 'localhost:{0}'.format(port_no)



        dts = DtsClient(StringVector([host_port]), True, 1000)  # as number of hosts
        assert dts.get_server_version() == shyft_lib_version
        # then try something that should work
        dts.store_ts(store_tsv)
        r1 = dts.evaluate(tsv, ta.total_period())
        tsv1x = tsv.inside(-0.5, 0.5)
        tsv1x.append(tsv1x[-1].decode(start_bit=1, n_bits=1))  # just to verify serialization/bind
        tsv1x.append(store_tsv[1].derivative())
        tsv1x.append(store_tsv[1].pow(2.0))  # just for verify pow serialization(well, it's a bin-op..)
        tsv1x.append(store_tsv[1].statistics(ta24,45))  # verify statistics serialization
        r1x = dts.evaluate(tsv1x, ta.total_period())
        r1c = dts.evaluate(tsv, ta.total_period(), clip_result=ta.period(1))  # how to clip result to a period
        r2 = dts.percentiles(tsv, ta.total_period(), ta24, percentile_list)
        r3 = dts.find(r'netcdf://dummy\.nc/ts\d')
        self.rd_throws = True
        ex_count = 0
        try:
            rx = dts.evaluate(tsv, ta.total_period())
        except RuntimeError as e:
            ex_count = 1
            pass
        self.rd_throws = True
        try:
            fx = dts.find('should throw')
        except RuntimeError as e:
            ex_count += 1
            pass

        dts.close()  # close connection (will use context manager later)
        dtss.stop_server()  # close server
        assert ex_count == 2
        assert len(r1) == len(tsv)
        assert self.callback_count == 5
        for i in range(n_ts - 1):
            assert r1[i].time_axis == tsv[i].time_axis
            assert_array_almost_equal(r1[i].values.to_numpy(), tsv[i].values.to_numpy(), decimal=4)

        assert len(r2) == len(percentile_list)
        dummy_ts.bind(TimeSeries(ta, fill_value=1.0, point_fx=point_fx.POINT_AVERAGE_VALUE))
        p2 = tsv.percentiles(ta24, percentile_list)
        # r2 = tsv.percentiles(ta24,percentile_list)

        for i in range(len(p2)):
            assert r2[i].time_axis == p2[i].time_axis
            assert_array_almost_equal(r2[i].values.to_numpy(), p2[i].values.to_numpy(), decimal=1)

        assert self.find_count == 2
        assert len(r3) == 10  # 0..9
        for i in range(len(r3)):
            assert r3[i] == self.ts_infos[i]
        assert r1x is not None
        assert r1c is not None
        for i in range(len(r1c)):  # verify we clipped the result
            assert r1c[i].total_period() == ta.period(1)

        assert 1 == len(self.stored_tsv)
        assert len(store_tsv) == len(self.stored_tsv[0])
        for i in range(len(store_tsv)):
            assert self.stored_tsv[0][i].ts_id() == store_tsv[i].ts_id()

    def check_ts_store(self,c_dir:str, db_type:str, cfg:DtssCfg=None):
        """
        This test verifies the shyft internal time-series store,
        that allow identified time-series to be stored
        in the backend using a directory container specified for the
        location.

        All time-series of the form shyft://<container>/<ts-name>
        is mapped to the configured <container> (aka a directory on the server)

        This applies to expressions, as well as the new
        .store_ts(ts_vector) function that allows the user to
        stash away time-series into the configured back-end container.

        All find-operations of the form shyft://<container>/<regular-expression>
        is mapped to a search in the corresponding directory for the <container>

        :param: db_type like "ts_db" or "ts_ldb" for file and  level db respectively
        :param: cfg in case of level db, the parameters that goes with it.

        :return:
        """
        cfg = cfg if cfg else DtssCfg()

        # setup data to be calculated
        utc = Calendar()
        d = deltahours(1)
        n = 365*24//3
        t = utc.time(2016, 1, 1)
        ta = TimeAxis(t, d, n)
        tar = TimeAxis(utc, t, Calendar.YEAR, 3)  # repeat time-axis for testing serial of repeat-expr.
        n_ts = 10
        store_tsv = TsVector()  # something we store at server side
        tsv = TsVector()  # something we put an expression into, refering to stored ts-symbols

        for i in range(n_ts):
            pts = TimeSeries(ta, np.sin(np.linspace(start=0, stop=1.0*i, num=ta.size())),
                             point_fx.POINT_AVERAGE_VALUE)
            ts_id = shyft_store_url("{0}".format(i))
            tsv.append(float(1.0)*TimeSeries(ts_id))  # make an expression that returns what we store
            store_tsv.append(TimeSeries(ts_id, pts))  # generate a bound pts to store
        # krls with some extra challenges related to serialization
        tsv_krls = TsVector()
        krls_ts = TimeSeries(shyft_store_url("9")).krls_interpolation(dt=d, gamma=1e-3, tolerance=0.001, size=ta.size())
        tsv_krls.append(krls_ts)
        # min_max_check_ts_fill also needs a serial check
        # create a  trivial-case
        ts9 = TimeSeries(shyft_store_url("9"))
        ts_qac = ts9.min_max_check_linear_fill(v_min=-10.0*n_ts, v_max=10.0*n_ts)
        tsv_krls.append(ts_qac)
        tsv_krls.append(ts9)
        tsv_krls.append(ts9.inside(min_v=-0.5, max_v=0.5))
        tsv_krls.append(ts9.repeat(tar))

        # then start the server
        dtss = DtsServer()
        dtss.set_auto_cache(True)
        std_max_items = dtss.cache_max_items
        dtss.cache_max_items = 3000
        tst_max_items = dtss.cache_max_items
        orig_cache_ts_initial_size_estimate = dtss.cache_ts_initial_size_estimate
        dtss.cache_ts_initial_size_estimate=orig_cache_ts_initial_size_estimate//2
        orig_cach_memory_target=dtss.cache_memory_target
        dtss.cache_memory_target= 4000000  # 4Mbytes
        assert dtss.cache_memory_target==4000000
        assert dtss.cache_ts_initial_size_estimate==orig_cache_ts_initial_size_estimate//2

        dtss.set_container("test", c_dir,db_type,cfg)  # notice we set container 'test' to point to c_dir directory
        port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
        host_port = 'localhost:{0}'.format(port_no)

        # also notice that we dont have to setup callbacks in this case (but we could, and they would work)
        #
        # finally start the action
        dts = DtsClient(host_port)
        # then try something that should work
        dts.store_ts(store_tsv)
        r1 = dts.evaluate(tsv, ta.total_period())
        f1 = dts.find(r"shyft://test/\d_.*")  # find all ts with one digit, 0..9 pluss trailer part
        r2 = dts.evaluate(tsv_krls, ta.total_period())
        url_x = shyft_store_url(r'does not exists')
        tsvx = TsVector()
        tsvx.append(TimeSeries(url_x))
        try:
            rx = dts.evaluate(tsvx, ta.total_period())
            assert not True, 'This did not work out'
        except RuntimeError as rex:
            assert rex is not None

        dts.close()  # close connection (will use context manager later)
        dtss.stop_server()  # close server

        # now the moment of truth:
        assert len(r1) == len(tsv)
        for i in range(n_ts - 1):
            assert r1[i].time_axis == store_tsv[i].time_axis
            assert_array_almost_equal(r1[i].values.to_numpy(), store_tsv[i].values.to_numpy(), decimal=4)

        assert len(f1) == 10
        assert len(r2) == len(tsv_krls)
        assert_array_almost_equal(r2[1].values.to_numpy(), r2[2].values.to_numpy(), decimal=4)
        assert 1000000 == std_max_items
        assert 3000 == tst_max_items



    def test_ts_cache(self, c_dir:str):
        """ Verify dtss ts-cache functions exposed to python """
        # setup data to be calculated
        utc = Calendar()
        d = deltahours(1)
        n = 100
        t = utc.time(2016, 1, 1)
        ta = TimeAxis(t, d, n)
        n_ts = 10
        store_tsv = TsVector()  # something we store at server side
        tsv = TsVector()  # something we put an expression into, refering to stored ts-symbols

        for i in range(n_ts):
            pts = TimeSeries(ta, np.sin(np.linspace(start=0, stop=1.0*i, num=ta.size())),
                             point_fx.POINT_AVERAGE_VALUE)
            ts_id = shyft_store_url("{0}".format(i))
            tsv.append(float(1.0)*TimeSeries(ts_id))  # make an expression that returns what we store
            store_tsv.append(TimeSeries(ts_id, pts))  # generate a bound pts to store

        # add one external ts
        tsv.append(TimeSeries(fake_store_url("_any_ts_id_will_do")))
        # then start the server
        dtss = DtsServer()

        dtss.cb = self.dtss_read_callback  # rig external callbacks as well.
        self.callback_count = 0
        self.rd_throws = False
        cache_on_write = True
        dtss.set_auto_cache(True)
        dtss.set_container("test", c_dir)  # notice we set container 'test' to point to c_dir directory
        port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
        host_port = 'localhost:{0}'.format(port_no)

        dts = DtsClient(host_port, auto_connect=False)  # demonstrate object life-time connection
        cs0 = dtss.cache_stats
        dts.store_ts(store_tsv, overwrite_on_write=True, cache_on_write=cache_on_write)
        r1 = dts.evaluate(tsv, ta.total_period(), use_ts_cached_read=True, update_ts_cache=True)
        cs1 = dtss.cache_stats
        ccs1 = dts.cache_stats  # client can also provide cahce-stats

        dtss.flush_cache_all()  # force the cache empty
        dtss.clear_cache_stats()
        cs2 = dtss.cache_stats  # just to ensure clear did work
        r1 = dts.evaluate(tsv, ta.total_period(), use_ts_cached_read=True, update_ts_cache=True)  # second evaluation, cache is empty, will force read(misses)
        cs3 = dtss.cache_stats
        r1 = dts.evaluate(tsv, ta.total_period(), use_ts_cached_read=True, update_ts_cache=True)  # third evaluation, cache is now filled, all hits
        cs4 = dtss.cache_stats
        # now verify explicit caching performed by the python callback
        self.cache_dtss = dtss
        self.cache_reads = True
        dts.cache_flush()  # is the equivalent of
        # dtss.flush_cache_all()
        # dtss.clear_cache_stats()
        # use explicit cache-control instead of global
        dtss.set_auto_cache(False)  # turn off auto caching, we want to test the explicit caching
        r1 = dts.evaluate(tsv, ta.total_period(), use_ts_cached_read=True, update_ts_cache=False)  # evaluation, just misses, but we cache explict the external
        cs5 = dtss.cache_stats  # ok base line a lots of misses
        r1 = dts.evaluate(tsv, ta.total_period(), use_ts_cached_read=True, update_ts_cache=False)
        cs6 = dtss.cache_stats  # should be one hit here

        dts.close()  # close connection (will use context manager later)
        dtss.stop_server()  # close server

        # now the moment of truth:
        assert len(r1) == len(tsv)
        for i in range(n_ts - 1):
            assert r1[i].time_axis == store_tsv[i].time_axis
            assert_array_almost_equal(r1[i].values.to_numpy(), store_tsv[i].values.to_numpy(), decimal=4)

        assert cs0.hits == 0
        assert cs0.misses == 0
        assert cs0.coverage_misses == 0
        assert cs0.id_count == 0
        assert cs0.point_count == 0
        assert cs0.fragment_count == 0

        assert cs1.hits == n_ts
        assert cs1.misses == 1  # because we cache on store, so 10 cached, 1 external with miss
        assert cs1.coverage_misses == 0
        assert cs1.id_count == n_ts + 1
        assert cs1.point_count == (n_ts + 1)*n
        assert cs1.fragment_count == n_ts + 1
        # verify client side cache_stats
        assert ccs1.hits == n_ts
        assert ccs1.misses == 1  # because we cache on store, so 10 cached, 1 external with miss
        assert ccs1.coverage_misses == 0
        assert ccs1.id_count == n_ts + 1
        assert ccs1.point_count == (n_ts + 1)*n
        assert ccs1.fragment_count == n_ts + 1

        assert cs2.hits == 0
        assert cs2.misses == 0
        assert cs2.coverage_misses == 0
        assert cs2.id_count == 0
        assert cs2.point_count == 0
        assert cs2.fragment_count == 0

        assert cs3.hits == 0
        assert cs3.misses == n_ts + 1  # because we cache on store, we don't even miss one time
        assert cs3.coverage_misses == 0
        assert cs3.id_count == n_ts + 1
        assert cs3.point_count == (n_ts + 1)*n
        assert cs3.fragment_count == n_ts + 1

        assert cs4.hits == n_ts + 1  # because previous read filled cache
        assert cs4.misses == n_ts + 1  # remembers previous misses.
        assert cs4.coverage_misses == 0
        assert cs4.id_count == n_ts + 1
        assert cs4.point_count == (n_ts + 1)*n
        assert cs4.fragment_count == n_ts + 1

        assert cs6.hits == 1  # because previous read filled cache
        assert cs6.misses == n_ts*2 + 1  # remembers previous misses.
        assert cs6.coverage_misses == 0
        assert cs6.id_count == 1
        assert cs6.point_count == 1*n
        assert cs6.fragment_count == 1


def test_dtss_cache(tmp_path):
    c = DtssTestCase()
    c.test_ts_cache(str(tmp_path))


def test_ts_store_with_file_db(tmp_path):
    c = DtssTestCase()
    c.check_ts_store(str(tmp_path), "ts_db")


def test_ts_store_with_level_db(tmp_path):
    c = DtssTestCase()
    c.check_ts_store(str(tmp_path), "ts_ldb")
    c.check_ts_store(str(tmp_path), "ts_ldb",DtssCfg(ppf=1000*1000,compress=True, max_file_size=1*1024*1024, write_buffer_size=1*1024*1024))


def test_dtss_cfg():
    a = DtssCfg()
    assert not a.compression
    assert a.ppf > 1
    assert a.max_file_size > 1*1024*1024
    assert a.write_buffer_size > 1*1024*1024
    assert a.ix_cache == 0
    assert a.ts_cache == 0
    a = DtssCfg(ppf=1000*1000,compress=True, max_file_size=5*1024*1024, write_buffer_size=3*1024*1024, ix_cache=1234, ts_cache=12345)
    assert a.compression
    assert a.ppf == 1000*1000
    assert a.max_file_size == 5*1024*1024
    assert a.write_buffer_size == 3*1024*1024
    assert a.test_mode == 0
    assert a.log_level == 200
    assert a.ix_cache == 1234
    assert a.ts_cache == 12345


def test_dtss_functionality_hosting_localhost():
    c = DtssTestCase()
    c.test_functionality_hosting_localhost()


def test_merge_store_ts_points(tmp_path):
    """
    This test verifies the shyft internal time-series store,
    that the merge_store_points function do the required
    semantics.
    """
    c_dir = str(tmp_path)
    # setup data to be calculated
    utc = Calendar()
    d = deltahours(1)
    t = utc.time(2016, 1, 1)
    ta = TimeAxis(UtcTimeVector.from_numpy(np.array([t, t + d, t + 3*d], dtype=np.int64)), t + 4*d)

    n_ts = 10
    store_tsv = TsVector()  # something we store at server side

    for i in range(n_ts):
        ts_id = shyft_store_url("{0}".format(i))
        store_tsv.append(TimeSeries(ts_id, TimeSeries(ta, fill_value=float(i), point_fx=point_fx.POINT_AVERAGE_VALUE)))
    # then start the server
    dtss = DtsServer()
    dtss.set_auto_cache(True)
    dtss.set_container("test", c_dir)  # notice we set container 'test' to point to c_dir directory
    port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
    host_port = 'localhost:{0}'.format(port_no)

    dts = DtsClient(host_port)

    dts.store_ts(store_tsv)  # 1. store the initial time-series, they are required for the merge_store_points function

    tb = TimeAxis(UtcTimeVector.from_numpy(np.array([t - d, t + 3*d, t + 4*d], dtype=np.int64)), t + 5*d)  # make some points, one before, one in the middle and after
    mpv = TsVector()  # merge point vector
    for i in range(n_ts):
        ts_id = shyft_store_url("{0}".format(i))
        mpv.append(TimeSeries(ts_id, TimeSeries(tb, fill_value=-1 - float(i), point_fx=point_fx.POINT_AVERAGE_VALUE)))

    dts.merge_store_ts_points(mpv)

    rts = TsVector()
    rts[:] = [TimeSeries(shyft_store_url(f"{i}")) for i in range(n_ts)]

    r = dts.evaluate(rts, tb.total_period())
    dts.close()  # close connection (will use context manager later)
    dtss.stop_server()  # close server

    for i in range(len(r)):
        assert r[i].time_axis.size() == 5
        assert_array_almost_equal(r[i].values.to_numpy(), np.array([-i - 1, i, i, -i - 1, -i - 1], dtype=np.float64))


def test_dtss_partition_by_average(tmp_path):
    """
    This test illustrates use of partition_by client and server-side.
    The main point here is to ensure that the evaluate period covers
    both the historical and evaluation peri
    """
    c_dir = str(tmp_path)
    # setup data to be calculated
    utc = Calendar()
    d = deltahours(1)
    t = utc.time(2000, 1, 1)
    n = utc.diff_units(t, utc.add(t, Calendar.YEAR, 10), d)
    ta = TimeAxis(t, d, n)
    td = TimeAxis(t, d*24, n//24)
    n_ts = 1
    store_tsv = TsVector()  # something we store at server side
    for i in range(n_ts):
        pts = TimeSeries(ta, np.sin(np.linspace(start=0, stop=1.0*(i + 1), num=ta.size())), point_fx.POINT_AVERAGE_VALUE)
        ts_id = shyft_store_url(f"{i}")
        store_tsv.append(TimeSeries(ts_id, pts))  # generate a bound pts to store

    # start dtss server
    dtss = DtsServer()
    dtss.set_auto_cache(True)
    dtss.set_container("test", c_dir)  # notice we set container 'test' to point to c_dir directory
    cache_on_write = True
    port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
    host_port = 'localhost:{0}'.format(port_no)
    # create dts client
    c = DtsClient(host_port, auto_connect=False)  # demonstrate object life-time connection
    c.store_ts(store_tsv, overwrite_on_write=True, cache_on_write=cache_on_write)

    t_0 = utc.time(2018, 1, 1)
    tax = TimeAxis(t_0, Calendar.DAY, 365)
    ts_h1 = TimeSeries(shyft_store_url(f'{0}'))
    ts_h2 = store_tsv[0]
    ts_p1 = ts_h1.partition_by(utc, t, Calendar.YEAR, 10, t_0).average(tax)
    ts_p2 = ts_h2.partition_by(utc, t, Calendar.YEAR, 10, t_0).average(tax)
    del c
    del dtss


def test_dtss_remove_series(tmp_path):
    c_dir = str(tmp_path)

    # start the server
    dtss = DtsServer()
    dtss.set_container("test", c_dir)  # notice we set container 'test' to point to c_dir directory
    port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
    host_port = 'localhost:{0}'.format(port_no)


    # setup some data
    utc = Calendar()
    d = deltahours(1)
    n = 365*24//3
    t = utc.time(2016, 1, 1)
    ta = TimeAxis(t, d, n)
    tsv = TsVector()
    pts = TimeSeries(ta, np.linspace(start=0, stop=1.0, num=ta.size()), point_fx.POINT_AVERAGE_VALUE)
    ts_url="shyft://test/foo"
    tsv.append(TimeSeries(ts_url, pts))

    # get a client
    client = DtsClient(host_port)
    client.store_ts(tsv)

    # start with no removing
    dtss.set_can_remove(False)

    # we should be disallowed to remove now
    try:
        client.remove(ts_url)
    except Exception as err:
        assert str(err) == "dtss::server: server does not support removing"

    # then try with allowing remove
    dtss.set_can_remove(True)

    # we only support removing shyft-url style data
    try:
        client.remove("protocol://test/foo")
    except Exception as err:
        assert str(err) == "dtss::server: server does not allow removing for non shyft-url type data"

    # now it should work
    client.remove(ts_url)
    del client
    del dtss


def test_get_ts_info(tmp_path):
    """
    Verify we can get specific TsInfo objects for time-series from the server backend.
    """
    c_dir = str(tmp_path)

    # start the server
    dtss = DtsServer()
    dtss.set_container("testing", c_dir)  # notice we set container 'test' to point to c_dir directory
    port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
    host_adr = 'localhost:{0}'.format(port_no)


    # get a client
    client = DtsClient(host_adr)

    try:
        client.get_ts_info(r'shyft://testing/data')
    except Exception as e:
        pass
    else:
        # only end up here if no exceptions
        assert False, 'Could fetch info for non-existing ts info'

    # setup some data
    utc = Calendar()
    d = deltahours(1)
    n = 365*24//3
    t = utc.time(2016, 1, 1)
    ta = TimeAxis(t, d, n)
    tsv = TsVector()
    pts = TimeSeries(ta, np.linspace(start=0, stop=1.0, num=ta.size()), point_fx.POINT_AVERAGE_VALUE)
    tsv.append(TimeSeries(r'shyft://testing/data', pts))
    client.store_ts(tsv)

    info: TsInfo = client.get_ts_info(r'shyft://testing/data')

    assert info.name == r'data'
    assert info.point_fx == point_fx.POINT_AVERAGE_VALUE
    assert info.data_period == ta.total_period()
    del client
    del dtss


def test_dtss_failures(tmp_path):
    """
    Verify that dtss client server connections are auto-magically
    restored and fixed
    """
    c_dir = str(tmp_path)

    # start the server
    dtss = DtsServer()
    dtss.set_container("test", c_dir)  # notice we set container 'test' to point to c_dir directory
    port_no = dtss.start_server()
    host_port = 'localhost:{0}'.format(port_no)
    assert dtss.alive_connections == 0
    # setup some data
    utc = Calendar()
    d = deltahours(1)
    n = 365*24//3
    t = utc.time(2016, 1, 1)
    ta = TimeAxis(t, d, n)
    tsv = TsVector()
    pts = TimeSeries(ta, np.linspace(start=0, stop=1.0, num=ta.size()), point_fx.POINT_AVERAGE_VALUE)
    tsv.append(TimeSeries("shyft://test/foo", pts))

    # get a client
    client = DtsClient(host_port, auto_connect=False)
    assert dtss.alive_connections==0
    client.store_ts(tsv)
    assert dtss.alive_connections==1
    client.close()
    assert dtss.alive_connections == 0
    client.store_ts(tsv)  # should just work, it re-open automagically
    assert dtss.alive_connections == 1
    dtss.stop_server()  # the server is out and away, no chance this would work
    assert dtss.alive_connections == 0
    try:
        client.store_ts(tsv)
        assert False, 'This should throw, because there is no dtss server to help you'
    except Exception as ee:
        assert not False, f'expected {ee} here'

    dtss.set_listening_port(port_no)
    dtss.start_server()
    client.store_ts(tsv)  # this should just work, automagically reconnect
    assert dtss.alive_connections == 1
    client.close()
    assert dtss.alive_connections == 0
    dtss.stop_server()


def test_dtss_throws_on_missing_callbacks(tmp_path):
    """
    Verify that dtss client server connections are auto-magically
    restored and fixed
    """
    c_dir = str(tmp_path)

    # start the server
    dtss = DtsServer()
    dtss.set_container("test", c_dir)  # notice we set container 'test' to point to c_dir directory
    port_no = dtss.start_server()
    host_port = 'localhost:{0}'.format(port_no)
    # setup some data
    utc = Calendar()
    d = deltahours(1)
    n = 365*24//3
    t = utc.time(2016, 1, 1)
    ta = TimeAxis(t, d, n)
    tsv = TsVector()
    pts = TimeSeries(ta, np.linspace(start=0, stop=1.0, num=ta.size()), point_fx.POINT_AVERAGE_VALUE)
    tsv.append(TimeSeries("external://test/foo", pts))

    # get a client
    c = DtsClient(host_port, auto_connect=True)
    with pytest.raises(RuntimeError):
        c.store_ts(tsv)
    with pytest.raises(RuntimeError):
        c.find("external://test/.*")
    with pytest.raises(RuntimeError):
        c.evaluate(TsVector([TimeSeries('ext://gruff/missing')]),ta.total_period())
    del c
    del dtss


@pytest.mark.parametrize("db_type",["ts_ldb","ts_db"])
def test_dtss_find(db_type,tmp_path):
    """
    Verify we can find timeseries using regular expressions
    """
    cfg=DtssCfg()
    c_dir = str(tmp_path)

    # start the server
    dtss = DtsServer()
    container_name = "testing"
    dtss.set_container(container_name, c_dir,db_type,cfg)  # notice we set container to point to c_dir directory
    port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
    host_adr = f'localhost:{port_no}'

    # get a client
    client = DtsClient(host_adr)

    # setup some data
    utc = Calendar()
    d = deltahours(1)
    n = 365*24//3
    t = utc.time(2016, 1, 1)
    ta = TimeAxis(t, d, n)
    pts = TimeSeries(ta, np.linspace(start=0, stop=1.0, num=ta.size()), point_fx.POINT_AVERAGE_VALUE)
    tsv=TsVector([
        TimeSeries(shyft_url(container_name, r'ts1'), pts),
        TimeSeries(shyft_url(container_name, r'ts2'), pts),
        TimeSeries(shyft_url(container_name, r'd1/ts1'), pts),
        TimeSeries(shyft_url(container_name, r'd1/ts2'), pts),
        TimeSeries(shyft_url(container_name, r'd1/s1/ts1'), pts),
        TimeSeries(shyft_url(container_name, r'd1/s2/ts1'), pts),
        TimeSeries(shyft_url(container_name, r'd2/x/ts1'), pts),
        TimeSeries(shyft_url(container_name, r'x/tsx'), pts),

    ])
    client.store_ts(tsv)

    # plain matches
    f1 = client.find(shyft_url(container_name,r'ts1'))
    f2 = client.find(shyft_url(container_name,r'd1/ts1'))
    f3 = client.find(shyft_url(container_name,r'd1/s1/ts1'))
    assert len(f1) == 1 and f1[0].name == 'ts1'
    assert len(f2) == 1 and f2[0].name == 'd1/ts1'
    assert len(f3) == 1 and f3[0].name == 'd1/s1/ts1'

    # matches plain directory path
    f4 = client.find(shyft_url(container_name, r'd1/s1/ts.'))
    assert len(f4) == 1 and f4[0].name == 'd1/s1/ts1'

    # matches any-path
    f5 = client.find(shyft_url(container_name,r'(.*\/)?ts.')) # notice the parenthesis for the groups,  the optional? and the path matching or expression here!
    assert len(f5) == len(tsv)
    # matches specific paths patterns            ( path walk    )  | (final match,including paths)
    f6 = client.find(shyft_url(container_name, r'.*\/x\/ts\d'))  #  finding the */x/ts<digit>, notice path expression match, and ts  that we drop optional?
    assert len(f6) == 1 and f6[0].name == 'd2/x/ts1'

    f7= client.find(shyft_url(container_name,r'd1/ts.'))
    assert len(f7) == 2

    # Test recursion dodging
    assert len( client.find(shyft_url(container_name,r'x/tsx')) ) == 1  # Recurse into x even if tsname contains 'x'
    assert len( client.find(shyft_url(container_name,r'[xyz]/tsx'))) == 1 # Recurse into x even if path is not literal

    del client
    del dtss

@pytest.mark.parametrize("db_type",["ts_ldb","ts_db"])
def test_dtss_store_empty_ts(db_type,tmp_path):
    cfg=DtssCfg()
    c_dir = str(tmp_path)
    # start the server
    dtss = DtsServer()
    container_name = "testing"
    dtss.set_container(container_name, c_dir,db_type,cfg)  # notice we set container to point to c_dir directory
    port_no = dtss.start_server()  # the internal shyft time-series will be stored to that container
    host_adr = f'localhost:{port_no}'

    # get a client
    client = DtsClient(host_adr)

    # setup some data
    utc = Calendar()
    d = deltahours(1)
    n = 0
    t = utc.time(2016, 1, 1)
    #ta = TimeAxis(t, d, n) # TODO: Not supported yet! fails on second attempt to store data due to merge issues.
    ta=TimeAxis([])
    pts = TimeSeries(ta, 0, point_fx.POINT_AVERAGE_VALUE)
    ts_url=shyft_url(container_name, r'ts1')
    tsv=TsVector([
        TimeSeries(ts_url, pts)
    ])
    client.store_ts(tsv,overwrite_on_write=False,cache_on_write=True)
    f1 = client.find(ts_url)
    assert len(f1) == 1
    r1 = client.evaluate(TsVector([TimeSeries(ts_url)]), utcperiod=UtcPeriod(t,t+d))
    assert len(r1[0]) == 0
    ts=TimeSeries(TimeAxis(t,d,1),1.0,point_fx.POINT_AVERAGE_VALUE)
    client.store_ts(TsVector([TimeSeries(ts_url,ts)]), overwrite_on_write=False,cache_on_write=True)
    r2 = client.evaluate(TsVector([TimeSeries(ts_url)]), utcperiod=UtcPeriod(t,t+d))
    assert r2[0] == ts

