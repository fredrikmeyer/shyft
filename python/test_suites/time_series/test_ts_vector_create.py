import numpy as np

from shyft.time_series import TimeAxis, deltahours, create_ts_vector_from_np_array, point_interpretation_policy as ts_point_fx


def test_create_basic():
    a = np.array([[1.1, 1.2, 1.3], [2.1, 2.2, 2.3]], dtype=np.float64)
    ta = TimeAxis(deltahours(0), deltahours(1), 3)
    tsv = create_ts_vector_from_np_array(ta, a, ts_point_fx.POINT_AVERAGE_VALUE)
    assert tsv is not None
    for i in range(2):
        assert np.allclose(tsv[i].values.to_numpy(), a[i])
        assert ta == tsv[i].time_axis
        assert tsv[i].point_interpretation() == ts_point_fx.POINT_AVERAGE_VALUE

    tsv2 = create_ts_vector_from_np_array(ta, a, ts_point_fx.POINT_AVERAGE_VALUE)
    for i in range(len(tsv2)):
        assert tsv2[i] == tsv[i]

    assert tsv2 == tsv

    # create missmatch throws
    b = np.array([[], []], dtype=np.float64)
    try:
        create_ts_vector_from_np_array(ta, b, ts_point_fx.POINT_AVERAGE_VALUE)
        assert False, "Should throw for missmatch time-axis"
    except RuntimeError as e:
        pass
    # create empty ts works
    tb = TimeAxis(0, 0, 0)
    r = create_ts_vector_from_np_array(tb, b, ts_point_fx.POINT_AVERAGE_VALUE)
    assert len(r) == 2
    assert r != tsv
    for ts in r:
        assert not ts
    # create empty returns empty
    c = np.empty(shape=(0, 0), dtype=np.float64)
    z = create_ts_vector_from_np_array(tb, c, ts_point_fx.POINT_AVERAGE_VALUE)
    assert len(z) == 0
