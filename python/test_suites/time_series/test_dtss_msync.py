import tempfile
import pytest
from time import sleep

from shyft.time_series import (Calendar,DtsClient,DtsServer,IntVector,UtcTimeVector,
    StringVector, TimeAxis, TimeSeries , TsInfo, TsInfoVector, TsVector
    ,UtcPeriod,deltahours,point_interpretation_policy as point_fx
    ,ts_stringify,time,convolve_policy,DoubleVector
)


def test_dtss_msync():
    with tempfile.TemporaryDirectory() as c_dir:

        master=DtsServer()
        master.set_container("test", c_dir)
        master_port = master.start_server()

        slave = DtsServer()
        slave.set_master_slave_mode(
            ip="localhost",
            port=master_port,
            master_poll_time=0.02,
            unsubscribe_threshold=10,
            unsubscribe_max_delay=1.0
        )
        port = slave.start_server()
        c = DtsClient(f'localhost:{port}')
        m = DtsClient(f'localhost:{master_port}')

        # ensure initial state is zero
        f = c.find("shyft://test/a.*")
        assert len(f) == 0

        # store some time-series through master connection(slave does not know this)
        n = 3
        ta=TimeAxis(time('2021-04-07T00:00:00Z'),time(3600),100)
        some_tsv=TsVector(
            [TimeSeries(
                f'shyft://test/a{i}',
                TimeSeries(ta,fill_value=float(i),point_fx=point_fx.POINT_AVERAGE_VALUE))
             for i in range(n)]
        )
        m.store_ts(some_tsv, overwrite_on_write=True, cache_on_write=True)

        assert len(c.find("shyft://test/a.*")) == n

        some_expr = TsVector([
            10*TimeSeries(f'shyft://test/a{i}')
             for i in range(n)]
        )

        e1 = c.evaluate(some_expr,ta.total_period(),True,True)  # cache and force an active msync

        assert len(e1) == 3  # we are ok with the length here, trust content

        # now, lets update the master timeseries, with new values
        some_tsv=TsVector(
            [TimeSeries(
                f'shyft://test/a{i}',
                TimeSeries(ta,[100.0*float(i)+float(j) for j in range(len(ta))], point_fx=point_fx.POINT_AVERAGE_VALUE))
             for i in range(n)]
        )
        m.store_ts(some_tsv, overwrite_on_write=False, cache_on_write=True)
        sleep(0.5)  # sleep a bit, to let msync work!

        e2 = c.evaluate(some_expr,ta.total_period(),True,True)  # cache and force an active msync

        # now verify e1 is different from e2 (because e2 has new values
        for i in range(n):
            assert e2[i] == 10.0*some_tsv[i]
        #  close down the show
        c.cache_flush()  # flush cache.
        w=DoubleVector([0.05, 0.15, 0.6, 0.15, 0.05])  # weights
        conv_w=TsVector([ TimeSeries(f'shyft://test/a{i}').convolve_w(w,convolve_policy.USE_NEAREST|convolve_policy.CENTER) for i in range(n)])
        read_period = ta.total_period() # this is how much we want to read, to evaluate a shorter part
        clip_period=ta.slice(start=5,n=10).total_period()  # start at 5th len 10
        e3 = c.evaluate(ts_vector=conv_w, utcperiod=read_period,use_ts_cached_read=True,update_ts_cache=False,clip_result=clip_period)
        assert e3
        c.close()
        slave.clear()
        master.clear()

