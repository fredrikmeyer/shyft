from pathlib import Path
from typing import Tuple, Any
import numpy as np
from shyft.hydrology import shyftdata_dir
from netCDF4 import Dataset
from shapely.geometry import Polygon
from shyft.time_series import (Calendar, UtcPeriod, deltahours, POINT_AVERAGE_VALUE, POINT_INSTANT_VALUE, time)
from shyft.hydrology.repository.netcdf.concat_data_repository import ConcatDataRepository, ConcatDataRepositoryError
from shyft.hydrology.repository.interfaces import ForecastSelectionCriteria
import pytest

utc = Calendar()


def _set_up_paths() -> Tuple[str, str, str]:
    # f1: EC-op with 4 geo_points and including 'x_wind_100m', 'y_wind_100m'
    # f2: EC-ens with onlu one geo_point
    # f3: EC-ens with 21 geo_points
    f1 = Path(shyftdata_dir)/"repository"/"ecmwf_op_merged"/"ecmwf_op_merged_vik_red.nc"
    f2 = Path(shyftdata_dir)/"repository"/"ecmwf_ens_merged"/f"ecmwf_ens_merged_vik_red.nc"
    f3 = Path(shyftdata_dir)/"repository"/"ecmwf_ens_merged"/"ecmwf_ens_merged_box_red.nc"
    return str(f1), str(f2), str(f3)  # str just to keep minimal changes for now


def _concat_epsg_bbox() -> Tuple[int, Any, Polygon]:
    """
    A slice of test-data located in shyft-data repository/
    NOTE: That it returns bpoly=None, and bbox = to the bounding box
    ."""
    epsg = 32633
    bpoly = None
    _, _, f3 = _set_up_paths()
    with Dataset(f3) as dataset:
        x, y = dataset.variables['x'][0], dataset.variables['y'][0]
    d = 20000.
    point_list = [[x - d, y - d], [x + d, y - d], [x + d, y + d], [x - d, y + d], [x - d, y - d]]
    bbox = Polygon(point_list)
    return epsg, bpoly, bbox


def utc_period(n_hours: int) -> Tuple[int, UtcPeriod]:
    # Utc period
    t0 = int(utc.time(2019, 9, 1, 0))
    period = UtcPeriod(t0, t0 + deltahours(n_hours))
    return t0, period


def test_repo_init() -> None:
    """
    Test repo properties
    """
    epsg, bpoly, bbox = _concat_epsg_bbox()
    f_op, _, _ = _set_up_paths()

    ec_op = ConcatDataRepository(str(epsg), filename=f_op)

    assert ec_op.shyft_cs is not None
    assert len(ec_op.variables) > 0
    assert ec_op.grid_spec.epsg == epsg
    assert len(ec_op.grid_spec.points) > 0
    assert ec_op.data_period.timespan() > time(0)
    assert len(ec_op.start_times) > 1


def test_get_timeseries() -> None:
    """
    Simple regression test
    """
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens_pt, f_ens_box = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    n_hours = 48
    t0, period = utc_period(n_hours)

    # check f_op
    ec_op = ConcatDataRepository(EPSG, filename=f_op)
    ec_op_scenario = ec_op.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    assert set(ec_op_scenario) == set(source_names)
    for src in source_names:
        ts = ec_op_scenario[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == period
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 3))

    # with period=None
    ec_op_scenario = ec_op.get_timeseries(source_names, None, geo_location_criteria=bpoly)
    assert set(ec_op_scenario) == set(source_names)
    for src in source_names:
        ts = ec_op_scenario[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == UtcPeriod(int(utc.time(2019, 8, 31, 0)), int(utc.time(2019, 9, 3, 12)))
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(int(utc.time(2019, 8, 31, 0)), int(utc.time(2019, 9, 3, 15)))

    # check f_ens_pt
    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_pt)
    ec_ens_scenario = ec_ens.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    assert set(ec_ens_scenario) == set(source_names)
    assert len(ec_ens_scenario['temperature']) == 1
    for src in source_names:
        ts = ec_ens_scenario[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == period
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 3))

    ec_ens_scenarios = ec_ens.get_timeseries_ensemble(source_names, period, geo_location_criteria=bpoly)
    assert len(ec_ens_scenarios) == 51
    assert len(ec_ens_scenarios[0]['temperature']) == 1
    for scen in ec_ens_scenarios:
        assert set(scen) == set(source_names)
        for src in source_names:
            ts = scen[src][0].ts
            if src in ('precipitation', 'radiation'):
                assert ts.point_interpretation() == POINT_AVERAGE_VALUE
                assert ts.total_period() == period
            else:
                assert ts.point_interpretation() == POINT_INSTANT_VALUE
                assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 3))

    for a, b in zip(ec_ens_scenario['temperature'], ec_ens_scenarios[0]['temperature']):
        assert a.mid_point() == b.mid_point()
        assert a.ts == b.ts

    # check f_ens_bbox
    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_box)
    ec_ens_scenario = ec_ens.get_timeseries(source_names, period, geo_location_criteria=bbox)
    assert len(ec_ens_scenario['temperature']) == 8


def test_get_timeseries_with_extra_intervals() -> None:
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens, _ = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    # Check extend end of ts with forecast
    t0 = int(utc.time(2019, 9, 1, 0))
    n_hours = 9*24
    period = UtcPeriod(t0, t0 + deltahours(n_hours))

    ec_op = ConcatDataRepository(EPSG, filename=f_op)
    ec_op_scenario = ec_op.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    for src in source_names:
        ts = ec_op_scenario[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == period
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 6))


def test_get_timeseries_for_period_with_missing_forecast() -> None:
    with pytest.raises(ConcatDataRepositoryError) as context:
        EPSG, bpoly, bbox = _concat_epsg_bbox()
        f_op, _, _ = _set_up_paths()
        source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

        # Check extend end of ts with forecast
        t0 = int(utc.time(2018, 3, 16, 0))
        n_hours = 48
        period = UtcPeriod(t0, t0 + deltahours(n_hours))

        ec_op = ConcatDataRepository(EPSG, filename=f_op)
        ec_op.get_timeseries(source_names, period, geo_location_criteria=bpoly)
        assert context.exception.args[0] ==  "No forecasts found that intersect period [2018-03-16T00:00:00Z,2018-03-18T00:00:00Z> with restrictions 'nb_lead_intervals_to_drop'=0 and 'nb_lead_intervals'=4"


def test_get_forecast() -> None:
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens_pt, f_ens_box = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    n_hours = 48
    t0, period = utc_period(n_hours)

    # check f_op
    ec_op = ConcatDataRepository(EPSG, filename=f_op)
    ec_op_fcst = ec_op.get_forecast(source_names, None, t0, geo_location_criteria=bpoly)
    assert set(ec_op_fcst) == set(source_names)
    for src in source_names:
        ts = ec_op_fcst[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_op.lead_time[-1])))
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_op.lead_time[-1] + 6)))

    # check get forecast on f_ens_pt
    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_pt)
    ec_ens_fcst = ec_ens.get_forecast(source_names, None, t0, geo_location_criteria=bpoly)
    assert set(ec_ens_fcst) == set(source_names)
    for src in source_names:
        ts = ec_ens_fcst[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1])))
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1] + 6)))

    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_pt)
    ec_ens_fcst = ec_ens.get_forecast(source_names, period, None, geo_location_criteria=bpoly)
    assert set(ec_ens_fcst) == set(source_names)
    for src in source_names:
        ts = ec_ens_fcst[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1])))
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1] + 6)))

    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_pt)
    ec_ens_fcst = ec_ens.get_forecast_ensemble(source_names, period, None, geo_location_criteria=bpoly)
    assert set(ec_ens_fcst[0]) == set(source_names)
    for src in source_names:
        ts = ec_ens_fcst[0][src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1])))
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1] + 6)))

    ec_ens_fcst_ens = ec_ens.get_forecast_ensemble(source_names, None, t0, geo_location_criteria=bpoly)
    assert len(ec_ens_fcst_ens) == 51
    for scen in ec_ens_fcst_ens:
        for src in source_names:
            ts = scen[src][0].ts
            if src in ('precipitation', 'radiation'):
                assert ts.point_interpretation() == POINT_AVERAGE_VALUE
                assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1])))
            else:
                assert ts.point_interpretation() == POINT_INSTANT_VALUE
                assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_ens.lead_time[-1] + 6)))

    # check f_ens_bbox
    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_box)
    ec_ens_fcst_ens = ec_ens.get_forecast_ensemble(source_names, None, t0, geo_location_criteria=bbox)
    assert len(ec_ens_fcst_ens[0]['temperature']) == 8


def test_get_collection() -> None:
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens_pt, f_ens_box = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    n_hours = 24
    t0, period = utc_period(n_hours)

    fsc = [ForecastSelectionCriteria(forecasts_created_within_period=period),
           ForecastSelectionCriteria(forecasts_with_start_within_period=period),
           ForecastSelectionCriteria(forecasts_that_intersect_period=period),
           ForecastSelectionCriteria(forecasts_that_cover_period=period),
           ForecastSelectionCriteria(latest_available_forecasts={'number_of_forecasts': 2, 'forecasts_older_than': t0}),
           ForecastSelectionCriteria(forecasts_at_reference_times=[t0, t0 + 12*3600])]

    # check f_op
    ec_op = ConcatDataRepository(EPSG, filename=f_op)
    for c in fsc:
        ec_op_fcst_col = ec_op.get_forecast_collection(source_names, c, geo_location_criteria=bpoly)
        if c.criterion[0] == 'forecasts_created_within_period':
            assert len(ec_op_fcst_col) == 3  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_with_start_within_period':
            assert len(ec_op_fcst_col) == 3  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_that_intersect_period':
            assert len(ec_op_fcst_col) == 5  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_that_cover_period':
            assert len(ec_op_fcst_col) == 3  # Note: boundary case are included
        else:
            assert len(ec_op_fcst_col) == 2

    # check f_ens
    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_pt)
    for c in fsc:
        ec_ens_fcst_col = ec_ens.get_forecast_collection(source_names, c, geo_location_criteria=bpoly)
        if c.criterion[0] == 'forecasts_created_within_period':
            assert len(ec_ens_fcst_col) == 3  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_with_start_within_period':
            assert len(ec_ens_fcst_col) == 3  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_that_intersect_period':
            assert len(ec_ens_fcst_col) == 5  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_that_cover_period':
            assert len(ec_ens_fcst_col) == 3  # Note: boundary case are included
        else:
            assert len(ec_ens_fcst_col) == 2


def test_get_ensemble_collection() -> None:
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens_pt, f_ens_box = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    n_hours = 24
    t0, period = utc_period(n_hours)

    fsc = [ForecastSelectionCriteria(forecasts_created_within_period=period),
           ForecastSelectionCriteria(forecasts_with_start_within_period=period),
           ForecastSelectionCriteria(forecasts_that_intersect_period=period),
           ForecastSelectionCriteria(forecasts_that_cover_period=period),
           ForecastSelectionCriteria(latest_available_forecasts={'number_of_forecasts': 2, 'forecasts_older_than': t0}),
           ForecastSelectionCriteria(forecasts_at_reference_times=[t0, t0 + 12*3600])]

    # check f_ens
    ec_ens = ConcatDataRepository(EPSG, filename=f_ens_pt)
    for c in fsc:
        ec_ens_fcst_col = ec_ens.get_forecast_ensemble_collection(source_names, c, geo_location_criteria=bpoly)
        if c.criterion[0] == 'forecasts_created_within_period':
            assert len(ec_ens_fcst_col) == 3  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_with_start_within_period':
            assert len(ec_ens_fcst_col) == 3  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_that_intersect_period':
            assert len(ec_ens_fcst_col) == 5  # Note: boundary case are included
        elif c.criterion[0] == 'forecasts_that_cover_period':
            assert len(ec_ens_fcst_col) == 3  # Note: boundary case are included
        else:
            assert len(ec_ens_fcst_col) == 2


def test_fc_periodicity() -> None:
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens_pt, f_ens_box = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    n_hours = 48
    t0, period = utc_period(n_hours)

    fc_periodicity = (2, 0)
    ec_op = ConcatDataRepository(EPSG, filename=f_op, fc_periodicity=fc_periodicity)
    fsc = ForecastSelectionCriteria(forecasts_created_within_period=period)
    ec_op_col = ec_op.get_forecast_collection(source_names, fsc, geo_location_criteria=bpoly)
    assert len(ec_op_col) == 3
    assert [utc.calendar_units(int(ec['temperature'][0].ts.total_period().start)).hour == 0 for ec in ec_op_col]

    fc_periodicity = (2, 1)
    ec_op = ConcatDataRepository(EPSG, filename=f_op, fc_periodicity=fc_periodicity)
    fsc = ForecastSelectionCriteria(forecasts_created_within_period=period)
    ec_op_col = ec_op.get_forecast_collection(source_names, fsc, geo_location_criteria=bpoly)
    assert len(ec_op_col) == 2
    assert [utc.calendar_units(int(ec['temperature'][0].ts.total_period().start)).hour == 12 for ec in ec_op_col]

    fc_periodicity = (2, 0)
    ec_op = ConcatDataRepository(EPSG, filename=f_op, fc_periodicity=fc_periodicity)
    fsc = ForecastSelectionCriteria(forecasts_created_within_period=period)
    ec_op_col = ec_op.get_forecast_collection(source_names, fsc, geo_location_criteria=bpoly)
    ec_op_ts = ec_op.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    ind24 = np.argmax(ec_op.lead_time == 24)
    ind48 = np.argmax(ec_op.lead_time == 48)
    assert np.all(ec_op_ts['temperature'][0].ts.values.to_numpy()[0:ind24] == ec_op_col[0]['temperature'][0].ts.values.to_numpy()[0:ind24])
    assert np.all(ec_op_ts['temperature'][0].ts.time_axis.time_points[0:ind24] == ec_op_col[0]['temperature'][0].ts.time_axis.time_points[0:ind24])
    assert np.all(ec_op_ts['temperature'][0].ts.values.to_numpy()[ind24:ind48] == ec_op_col[1]['temperature'][0].ts.values.to_numpy()[0:ind24])
    assert np.all(ec_op_ts['temperature'][0].ts.time_axis.time_points[ind24:ind48] == ec_op_col[1]['temperature'][0].ts.time_axis.time_points[0:ind24])


def test_one_fcst_file() -> None:
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f4 = Path(shyftdata_dir)/"repository"/"ecmwf_op_merged"/"strange_file_w_slp_prec_and_one_fcst.nc"
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    # Check extend end of ts with forecast
    t0 = int(utc.time(2019, 8, 31, 0))
    n_hours = 5*24
    period = UtcPeriod(t0, t0 + deltahours(n_hours))

    ec_wx = ConcatDataRepository(EPSG, filename=f4)
    ec_wx_scenario = ec_wx.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    assert set(ec_wx_scenario) == set(source_names)
    for src in source_names:
        ts = ec_wx_scenario[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == period
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 3))


def test_nb_lead_intervals_to_drop() -> None:
    # Also tests for irregular time_axis
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, f_ens_pt, _ = _set_up_paths()
    source_names = ("temperature", "wind_speed", "precipitation", "relative_humidity", "radiation")

    t0 = int(utc.time(2019, 9, 5, 0))
    n_hours = 48
    period = UtcPeriod(t0, t0 + deltahours(n_hours))

    fc_periodicity = (4, 0)
    nb_lead_intervals_to_drop = 5*8
    ec_op = ConcatDataRepository(EPSG, filename=f_op, fc_periodicity=fc_periodicity, nb_lead_intervals_to_drop=nb_lead_intervals_to_drop)
    ec_op_scenario = ec_op.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    for src in source_names:
        ts = ec_op_scenario[src][0].ts
        if src in ('precipitation', 'radiation'):
            assert ts.point_interpretation() == POINT_AVERAGE_VALUE
            assert ts.total_period() == period
        else:
            assert ts.point_interpretation() == POINT_INSTANT_VALUE
            assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 3))

def test_windspeed_100m() -> None:
    """
    Simple regression test
    """
    EPSG, bpoly, bbox = _concat_epsg_bbox()
    f_op, _, _ = _set_up_paths()
    source_names = ("wind_speed_100m",)

    n_hours = 48
    t0, period = utc_period(n_hours)

    # get_timeseries
    ec_op = ConcatDataRepository(EPSG, filename=f_op)
    ec_op_scenario = ec_op.get_timeseries(source_names, period, geo_location_criteria=bpoly)
    assert set(ec_op_scenario) == set(source_names)
    ts = ec_op_scenario["wind_speed_100m"][0].ts
    assert ts.point_interpretation() == POINT_INSTANT_VALUE
    assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(n_hours + 3))

    # get_forecast
    ec_op = ConcatDataRepository(EPSG, filename=f_op)
    ec_op_fcst = ec_op.get_forecast(source_names, None, t0, geo_location_criteria=bpoly)
    assert set(ec_op_fcst) == set(source_names)
    ts = ec_op_fcst["wind_speed_100m"][0].ts
    assert ts.point_interpretation() == POINT_INSTANT_VALUE
    assert ts.total_period() == UtcPeriod(t0, t0 + deltahours(int(ec_op.lead_time[-1] + 6)))
