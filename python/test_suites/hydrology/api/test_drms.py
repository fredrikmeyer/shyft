from typing import List
import pytest
import math
import threading
from time import sleep
from random import randrange

from shyft.hydrology import (DrmClient, DrmServer, RegionModelType,
                             GeoPointVector, GeoCellData, GeoCellDataVector, LandTypeFractions,
                             TemperatureSourceVector, TemperatureSource, PrecipitationSourceVector, PrecipitationSource, RadiationSourceVector,
                             RadiationSource, WindSpeedSourceVector, WindSpeedSource, RelHumSourceVector, RelHumSource,
                             GeoPoint, ARegionEnvironment,
                             TargetSpecificationVector, TargetSpecificationPts, TargetSpecCalcType, CatchmentPropertyType,
                             CalibrationOption, CalibrationStatus, OptimizerMethod
                             )
from shyft.hydrology.pt_gs_k import PTGSKStateWithId, PTGSKModel
from shyft.hydrology.pt_ss_k import PTSSKStateWithId, PTSSKModel
from shyft.hydrology.pt_st_k import PTSTKStateWithId, PTSTKModel
from shyft.hydrology.pt_hs_k import PTHSKModel
from shyft.hydrology.pt_hps_k import PTHPSKModel
from shyft.hydrology.r_pm_gs_k import RPMGSKModel
from shyft.hydrology.r_pt_gs_k import RPTGSKModel

from shyft.time_series import (TimeAxis, TimeSeries, Calendar, deltahours, POINT_AVERAGE_VALUE,
                               utctime_now, time, IntVector, __version__ as shyft_version)
from test_suites.hydrology.api.test_region_model_stacks import build_model


def test_server_client():
    s = DrmServer()
    port = s.start_server()
    assert s.is_running()
    assert s.alive_connections == 0
    c = DrmClient(f"localhost:{port}", 1000)
    v = c.get_server_version()
    assert v==shyft_version
    assert c.is_open
    assert DrmClient.total_clients > 0
    assert s.alive_connections == 1
    c.close()
    assert s.alive_connections == 0
    assert not c.is_open
    stack_models = [(PTGSKModel, RegionModelType.PT_GS_K),
                    (PTSSKModel, RegionModelType.PT_SS_K),
                    (PTHSKModel, RegionModelType.PT_HS_K),
                    (PTHPSKModel, RegionModelType.PT_HPS_K),
                    (RPMGSKModel, RegionModelType.R_PM_GS_K),
                    (PTSTKModel, RegionModelType.PT_ST_K),
                    (RPTGSKModel, RegionModelType.R_PT_GS_K),
                    ]

    def my_drms_callback(mid: str, fx_args: str) -> bool:
        """
        This illustrates how to setup a serverside fx callback, that can be invoked from the client to perform
        any useful function
        :param mid is the model identifier that is prepared/locked exclusively for this session.
        :param fx_args is arbitrary string, usually smart to use json, to pass on arguments from the client side.
        :returns True
        _
        """
        m = s.get_model(mid)  # this is now the region model, that you can work with.
        #ms = s.get_model_ids()
        #print(fx_args)
        #for mn in ms:
        #    print(mn)

        return len(fx_args) and m is not None

    s.fx = my_drms_callback # now the server is ready, any call to c.fx(mid,fx_args) will end up here


    for stk, st_id in stack_models:
        model = build_model(stk, stk.parameter_t, 1)
        gcdv = model.extract_geo_cell_data()  # if you have a rm, just get the geo cell data out of it!
        mid = "m1" + str(st_id)
        assert c.create_model(mid, st_id, gcdv)  # notice how simple it is to create a drms model from gcdv!
        mids=s.get_model_ids()
        assert len(mids) >= 1
        m=s.get_model(mids[0])
        assert m
        #  the first arg is the model id, the second arg is the stack type id, the third is the gcdv.
        rgcdv = c.get_geo_cell_data(mid)  #v erify we can get back the gcd
        assert rgcdv == gcdv
        empty_description = c.get_description(mid)
        assert len(empty_description) == 0
        m_description = '{"description":"We allow any text here, but recommend","json":["because","its","python-friendly"]}'
        c.set_description(mid, m_description)
        assert c.get_description(mid) == m_description
        assert c.set_state_collection(mid, -1, False)  # this is how to control state collection
        assert c.set_snow_sca_swe_collection(mid, -1, True)

        s0 = c.get_state(mid)
        assert stk.__name__.split('Model')[0] in s0.__class__.__name__
        assert stk.__name__.split('Model')[0] in c.get_state(mid, []).__class__.__name__
        with pytest.raises(RuntimeError):
            c.get_state()

        for i in range(len(s0)):
            s0[i].state.kirchner.q = 25.0  # ensure to have some water

        assert c.set_state(mid, s0)  # first time this also implies setting the initial state
        assert c.revert_to_initial_state(mid)  # notice that once we have set the state, we can revert to it any time
        assert c.set_current_state_as_initial_state(mid)  # we can revert to initial state any time later

        ts_size = 24
        ta = TimeAxis(Calendar().time(2019, 6, 1), deltahours(1), ts_size)
        ts = TimeSeries(ta, 0.5, POINT_AVERAGE_VALUE)
        pos = GeoPoint(500, 500, 0)

        r_env = ARegionEnvironment()
        r_env.temperature = TemperatureSourceVector([TemperatureSource(pos, ts)])
        r_env.precipitation = PrecipitationSourceVector([PrecipitationSource(pos, ts)])
        r_env.radiation = RadiationSourceVector([RadiationSource(pos, ts)])
        r_env.wind_speed = WindSpeedSourceVector([WindSpeedSource(pos, ts)])
        r_env.rel_hum = RelHumSourceVector([RelHumSource(pos, ts)])

        ta0 = c.get_time_axis(mid)
        assert len(ta0) == 0, 'expect initial time-axis of 0 length'
        assert c.run_interpolation(mid, model.interpolation_parameter, ta, r_env, True)  # easy!
        assert c.get_time_axis(mid) == ta  # how to get current time-axis, and it should be equal to the ip ta
        r_interpolation_parameter = c.get_interpolation_parameter(mid)  # how to get interpolation parameters
        assert model.interpolation_parameter == r_interpolation_parameter
        r_env2 = c.get_region_env(mid)   # how to get back region environment from the model
        assert r_env2 == r_env

        assert c.get_discharge(mid, []).time_axis == TimeAxis()  # remember, no result/state data yet, we have not
        assert c.get_charge(mid, []).time_axis == TimeAxis()  # called run cells yet
        assert c.get_snow_swe(mid, []).time_axis == TimeAxis()
        assert c.get_snow_sca(mid, []).time_axis == TimeAxis()
        assert c.get_temperature(mid, []).time_axis == ta  # but the cell env should have values now
        assert c.get_precipitation(mid, []).time_axis == ta  # as result of the interpolation
        assert c.get_radiation(mid, []).time_axis == ta
        assert c.get_wind_speed(mid, []).time_axis == ta
        assert c.get_rel_hum(mid, []).time_axis == ta

        p0 = stk.parameter_t()
        c.set_region_parameter(mid, p0)  # how to set the remote region parameter
        assert not c.has_catchment_parameter(mid, 1)  # and how to check if there are specific catchm. params
        c.set_catchment_parameter(mid, p0, 1)  # how to set a specific catchment parameter
        assert c.has_catchment_parameter(mid, 1)  # this time, when checking, we expect true

        p0x = c.get_region_parameter(mid)  # how to retrieve region parameter from the remote model
        p0c = c.get_catchment_parameter(mid, 1)  # how to retrieve catchment parameter from the remote model
        c.remove_catchment_parameter(mid, 1)  # how to remove catchment parameter
        assert not c.has_catchment_parameter(mid, 1)  # how to check if the remote model as a catchment specific parameter
        assert p0x == p0
        assert p0c == p0
        assert c.run_cells(mid)
        assert c.is_calculated(mid, model.catchment_ids[0])  #
        assert not c.cancel_calibration(mid)  # how to cancel a calibration, returns false if no calibration was running
        ts_discharge1 = c.get_discharge(mid, [])
        assert ts_discharge1.time_axis == ta and ts_discharge1.size() == ts_size
        ts_snow = c.get_snow_swe(mid, [])
        assert ts_snow.time_axis == ta and ts_snow.size() == ts_size

        # This is how to adjust the rm state to a wanted level
        wanted_state = ts_discharge1.values[0]*0.8
        result = c.adjust_q(mid=mid, indexes=[], wanted_q=wanted_state, start_step=0, scale_range=3.0, scale_eps=0.001, max_iter=300, n_steps=1)
        obtained_state = result.q_r
        assert abs(obtained_state - wanted_state) < 0.01*wanted_state

        ts_discharge2 = c.get_discharge(mid, [])
        assert ts_discharge2.time_axis == ta
        assert ts_discharge2.size() == ts_size
        assert not ts_discharge2.v[0] == ts_discharge1.v[0]

        assert c.set_catchment_calculation_filter(mid, [])  # you can control which catchment to compute
        assert c.fx(mid=mid, fx_args='{"json": "recommended"}')
        #
        # CALIBRATION INTERFACE TESTING
        #

        # 1.0 Create target specification
        target_spec = TargetSpecificationVector([
            TargetSpecificationPts(
                1.3*ts_discharge2, IntVector([1]), 1.0,
                TargetSpecCalcType.KLING_GUPTA, 1.0, 1.0, 1.0,
                CatchmentPropertyType.DISCHARGE,
                "part1"
            )
        ])

        # 1.1 Create parameter range to optimizer, p_min..p_max
        p_min = stk.parameter_t(p0)  # we create a copy of p0, then modify the one to calibrate
        p_min.kirchner.c1 = p_min.kirchner.c1*0.9
        p_min.p_corr.scale_factor = 0.5
        p_max = stk.parameter_t(p0)  # again, copy p0, then set upper range for parameters to find
        p_max.kirchner.c1 = p_min.kirchner.c1*1.9
        p_max.p_corr.scale_factor = 5.0

        # 1.2 Create options for calibration, method and termination criteria
        opt = CalibrationOption.bobyqa(max_iterations=1500, tr_start=0.1, tr_stop=0.001)

        # 1.3 Tweak starting point for bobyqa just give it some work
        p0.kirchner.c1 = p0.kirchner.c1*1.2  # make it a challenge for bobyqa
        p0.p_corr.scale_factor = 3.0  # also tweak up the p_corr

        if c.start_calibration(mid, p0, p_min, p_max, target_spec, opt):  # now calibration is running at the server
            time_limit = time.now() + 10  # allow us to exit after 10 second (this is a test)
            while True:
                sleep(0.01)  # just wait a while
                cs = c.check_calibration(mid)  # fetch,poll the status for the drms calibration
                if not cs.running:  # Ok, if its not running anymore, we are done
                    break
                if time.now() > time_limit:  # just allow us to break out in case everything goes wrong
                    break
            time_used = time.now()-(time_limit-10)
            print(f'calibration took {time_used}')
            assert not cs.running
            assert cs.trace_size > 0  # expect there to be a trace, indicating number of model evaluations
            p_found = cs.result()  # and get out the found parameter set.
            assert p_found  # ensure its not None
            for i in range(cs.trace_size):  # investigate the results in the trace
                assert cs.trace_parameter(i)
                assert math.isfinite(cs.trace_goal_function_value(i))
            #
            # Verify we can cancel ongoing calibration
            assert c.start_calibration(mid, p0, p_min, p_max, target_spec, opt)  # start calibration, and ensure true(its really started)
            assert c.cancel_calibration(mid)  # immediately kill it,  and assert result is true

        else:
            assert False, 'Failed to start calibration test'

        mlist = c.get_model_ids()  # this is how to get the list of 'live' models !
        assert len(mlist) == 1 and mlist[0] == mid
        new_mid = f'{mid}.old'
        c.rename_model(mid, new_mid)  # how to rename a model to a new name
        mlist = c.get_model_ids()
        assert len(mlist) == 1 and mlist[0] == new_mid

        clone_mid = f'{new_mid}.clone'
        assert c.clone_model(new_mid, clone_mid)  # Very easy to remotely create clone (copy without results) of the model
        assert clone_mid in c.get_model_ids()

        copy_mid = f'{new_mid}.copy'
        assert c.copy_model(new_mid, copy_mid)  # A full copy on the remote
        assert copy_mid in c.get_model_ids()
        assert c.get_state(copy_mid)[0].state.kirchner.q == c.get_state(new_mid)[0].state.kirchner.q
        assert c.get_discharge(copy_mid, []) == c.get_discharge(new_mid, [])

        assert c.remove_model(clone_mid)  # this is how to remove a model from the drms server
        assert c.remove_model(copy_mid)
        assert c.remove_model(new_mid), 'remove the model so next round is ready'
        assert len(c.get_model_ids()) == 0

    c.close()
    tc = c.total_clients  # keep this
    del c  # destroy it
    assert tc - DrmClient.total_clients == 1, 'Expect one to be gone by now'
    assert s.alive_connections == 0
    s.stop_server(1000)


# a.duration_minutes*60,a.host_port,a.model_prefix,a.batch_size,a.keep_batches
def run_frag_test(duration_sec: int, host_port: str, model_prefix: str, batch_size: int, keep_batches: int, n_readers: int = 10, start_day: int = 180):
    """
    Prior to the test,
    start up the drms-server using the command
    SHYFT_DRMS_SERVER=7777 cpp/test/hydrology/test_hydrology -tc=drms_server
    You can also use valgrind with massif
    https://valgrind.org/docs/manual/ms-manual.html
    SHYFT_DRMS_SERVER=7777 valgrind --tool=massif cpp/test/hydrology/test_hydrology -tc=drms_server
    then after run, you can use
    ms_print massif.out.12345 (12345=the pid of the last run)
    or use the https://kde.org/applications/en/development/org.kde.massif-visualizer


    Then run this function, using
    SHYFT_DRMS_HOST_PORT=<yourip_or_host>:7777

    """

    def upload_and_run_model(c: DrmClient, mid_base: str, n_x_cells: int = 10, n_y_cells: int = 10, n_steps: int = 24*365):
        stack_models = [(PTGSKModel, RegionModelType.PT_GS_K_OPT)]
        # stack_models= [(PTSTKModel, RegionModelType.PT_ST_K_OPT)]  # currently causes valgrind exit, ok on sanitizer, so most likely valgrind feature
        for stk, st_id in stack_models:
            model = build_model(stk, stk.parameter_t, n_x_cells*n_y_cells)
            gcdv = model.extract_geo_cell_data()
            mid = f'{mid_base}_{st_id}'
            assert c.create_model(mid, st_id, gcdv)

            assert c.set_state_collection(mid, -1, False)
            assert c.set_snow_sca_swe_collection(mid, -1, True)

            s0 = c.get_state(mid)

            assert c.set_state(mid, s0)
            assert c.set_current_state_as_initial_state(mid)  # notice we need this before revert to initial state.
            assert c.revert_to_initial_state(mid)

            ts_size = n_steps
            ta = TimeAxis(Calendar().time(2019, 6, 1), deltahours(1), ts_size)
            r_env = ARegionEnvironment()  # create a grid that somewhat is around the cells
            for x in range(n_x_cells):
                for y in range(n_y_cells):
                    pos = GeoPoint(500 + x*1000, 500 + y*1000, (x + y)*10)
                    r_env.temperature.append(TemperatureSource(pos, TimeSeries(ta, 0.5, POINT_AVERAGE_VALUE)))
                    r_env.precipitation.append(PrecipitationSource(pos, TimeSeries(ta, 0.5, POINT_AVERAGE_VALUE)))
                    r_env.radiation.append(RadiationSource(pos, TimeSeries(ta, 200.0, POINT_AVERAGE_VALUE)))
                    r_env.wind_speed.append(WindSpeedSource(pos, TimeSeries(ta, 3.0, POINT_AVERAGE_VALUE)))
                    r_env.rel_hum.append(RelHumSource(pos, TimeSeries(ta, 3.0, POINT_AVERAGE_VALUE)))

            assert c.run_interpolation(mid, model.interpolation_parameter, ta, r_env, True)

            assert c.get_discharge(mid, []).time_axis == TimeAxis()
            assert c.get_charge(mid, []).time_axis == TimeAxis()
            assert c.get_snow_swe(mid, []).time_axis == TimeAxis()
            assert c.get_snow_sca(mid, []).time_axis == TimeAxis()
            assert c.get_temperature(mid, []).time_axis == ta
            assert c.get_precipitation(mid, []).time_axis == ta
            assert c.get_radiation(mid, []).time_axis == ta
            assert c.get_wind_speed(mid, []).time_axis == ta
            assert c.get_rel_hum(mid, []).time_axis == ta

            p0 = stk.parameter_t()
            c.set_region_parameter(mid, p0)  # how to set the remote region parameter
            c.set_catchment_parameter(mid, p0, 1)  # how to set a specific catchment parameter
            assert c.run_cells(mid)
            assert c.is_calculated(mid, model.catchment_ids[0])

            ts_discharge1 = c.get_discharge(mid, [])
            assert ts_discharge1.time_axis == ta and ts_discharge1.size() == ts_size
            ts_snow = c.get_snow_swe(mid, [])
            assert ts_snow.time_axis == ta and ts_snow.size() == ts_size

            wanted_state = ts_discharge1.values[0]*0.8
            result = c.adjust_q(mid=mid, indexes=[], wanted_q=wanted_state, start_step=0, scale_range=3.0, scale_eps=0.001, max_iter=300, n_steps=1)
            obtained_state = result.q_r
            assert abs(obtained_state - wanted_state) < 0.01*wanted_state

            ts_discharge2 = c.get_discharge(mid, [])
            assert ts_discharge2.time_axis == ta
            assert ts_discharge2.size() == ts_size
            assert not ts_discharge2.v[0] == ts_discharge1.v[0]

            assert c.set_catchment_calculation_filter(mid, [])

            mlist = c.get_model_ids()  # this is how to get the list of 'live' models !
            assert len(mlist) >= 1

    def upload_and_run_batch(c: DrmClient, *, mid_start: int, n_mids: int, model_prefix: str, first_run_steps=365*24, next_run_steps=24*15):
        for mid in range(n_mids):
            n_steps = first_run_steps if mid == 0 else next_run_steps
            upload_and_run_model(c, mid_base=f'{model_prefix}_{mid_start + mid:08d}_', n_steps=n_steps)

    def remove_some_models(c: DrmClient, n_mids: int, model_prefix: str):
        mids = c.get_model_ids()
        mids_list = [m for m in mids if m.startswith(model_prefix)]  # filter out  this client models
        mids_list.sort()
        for i in range(min(n_mids, len(mids_list))):
            c.remove_model(mids_list[i])

    def read_model_results(host_port: str, sleep_min_ms: int, sleep_max_ms: int, n_models_to_read: int, exit_at: time, model_prefix: str):
        success_count = 0
        fail_count = 0
        while utctime_now() < exit_at:
            sleep(randrange(start=sleep_min_ms, stop=sleep_max_ms)/1000.0)
            c = DrmClient(host_port, 1000)
            mids = c.get_model_ids()
            mids_list = [m for m in mids if m.startswith(model_prefix)]
            mids_list.sort()
            mids_list.reverse()
            for i in range(min(len(mids_list), n_models_to_read)):
                mid = mids_list[i]
                try:
                    ts_discharge = c.get_discharge(mid, [])
                    ts_snow = c.get_snow_swe(mid, [])
                    success_count += 1
                finally:
                    fail_count += 1
            del c
        return success_count, fail_count

    def start_readers(n_readers: int, host_port: str, exit_at: time, read_n_models: int, model_prefix: str) -> List[threading.Thread]:
        r: List[threading.Thread] = []
        for i in range(n_readers):
            w = threading.Thread(target=read_model_results, args=(host_port, 20, 1000, read_n_models, exit_at, model_prefix))
            w.start()
            r.append(w)
        return r

    mid_start = 100
    n_batches = keep_batches
    first_steps = start_day*24  # number of hours into hydrolgical year to start., 90 days .. is ok
    print(f'Starting test using  host @ {host_port}, model_prefix={model_prefix} uploading initial batch {n_batches} of size {batch_size}')
    c = DrmClient(host_port, 1000)
    for b in range(n_batches):
        print(f'Initial add new batch model-id {mid_start} +.. {batch_size} historical-days={first_steps/24:0.2f}')
        upload_and_run_batch(c, mid_start=mid_start, n_mids=batch_size, model_prefix=model_prefix, first_run_steps=first_steps)
        mid_start += batch_size
        first_steps += 6  # increment with 6 hour for each step.
        if first_steps > 365*24:  # flip around at year,
            first_steps = 10  # restart at 10 steps
    test_duration = time(duration_sec)
    exit_at = utctime_now() + test_duration
    print(f'Done initial batch, starting readers {n_readers}')

    readers = start_readers(n_readers=n_readers, host_port=host_port, exit_at=exit_at, read_n_models=batch_size, model_prefix=model_prefix)
    print(f'Running upload of incremental new batches for {test_duration}')
    while utctime_now() < exit_at:
        print(f'Incremental add new batch model-id {mid_start} +.. {batch_size} historical-days={first_steps/24:0.2f}')
        upload_and_run_batch(c, mid_start=mid_start, n_mids=batch_size, model_prefix=model_prefix, first_run_steps=first_steps)
        mid_start += batch_size
        print(f'Remove "old" batch +.. {batch_size} from server')
        remove_some_models(c, n_mids=batch_size, model_prefix=model_prefix)
        first_steps += 6
        if first_steps > 365*24:
            first_steps = 1
    print(f'Done testing, joining the readers')
    for w in readers:
        if w.is_alive():
            w.join(timeout=3.0)
            if w.is_alive():
                print(f'reader worker {w.name} still alive')
    print(f'Clean up test models')
    mids = c.get_model_ids()
    for m in mids:
        if m.startswith(model_prefix):
            c.remove_model(m)
    mids = [m for m in c.get_model_ids() if m.startswith(model_prefix)]
    assert len(mids) == 0, 'Expect all models for this test-client to be gone(ensure you are using unique prefixes)'
    c.close()
    print('Done tests')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description=
        """
        Memory test for the DRMS, client side.
        The goal of this test is to prove long-term stability
        in memory usage when loading up the server with 
        models that allocates the major part of the memory for the
        servers.
        
        Recommended setup:
        Run server on a separate computer,
        then run this stress test client on 
        a separate computer.
         
        Prior to the test, start up the drms-server using the command
        
        SHYFT_DRMS_SERVER=7777 ./test_hydrology -tc=drms_server
        
        You can also use valgrind with massif
        https://valgrind.org/docs/manual/ms-manual.html
        SHYFT_DRMS_SERVER=7777 valgrind --tool=massif ./test_hydrology -tc=drms_server
        then after run, you can use
        ms_print massif.out.12345 (12345=the pid of the last run)
        or use the https://kde.org/applications/en/development/org.kde.massif-visualizer
         
        """
    )
    parser.add_argument('--host-port', required=True, type=str, help='Specifies the location, as in  host:port where the drms is running')
    parser.add_argument('--model-prefix', required=True, help='Gives the start of the names created by this test-client, keep this distinct if you run several test-clients at the same time')
    parser.add_argument('--batch-size', default=50, type=int, help='The number of region-models in one batch, typically 10..50')
    parser.add_argument('--keep-batches', default=5, type=int, help='The number of batches to keep in memory of the drms')
    parser.add_argument('--duration-minutes', default=10, type=int, help='test duration in  minutes')
    parser.add_argument('--n-readers', default=10, help='Number of readers that simulate the web-requests')
    parser.add_argument('--start-day', default=180, type=int, help='Initial length of historical simulation, 1..365')
    a = parser.parse_args()
    run_frag_test(a.duration_minutes*60, a.host_port, a.model_prefix, a.batch_size, a.keep_batches, n_readers=a.n_readers, start_day=a.start_day)
