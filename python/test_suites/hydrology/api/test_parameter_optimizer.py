from shyft.hydrology import CalibrationOption, ParameterOptimizer
from shyft.time_series import DoubleVector
from pytest import approx
from math import isfinite
import numpy as np

def test_parameter_optimizer():
    xo=DoubleVector([-5.0,1.0,1.0,1.0])

    def my_goal_function(x: DoubleVector) -> float:
        """
          returns (x-xo)**2, e.g. distance**2 from xo
        """
        # print(x)
        y=0
        for a,b in zip(x,xo):
            y += (a-b)**2
        if not isfinite(y):
            raise ValueError(f"y: {y}, x: {x}")
        return y

    po = ParameterOptimizer(p_min=[-10, 0, 0, 0], p_max=[-4, 2, 2, 2], fx=my_goal_function)

    x = DoubleVector([-9.0, 0.5, 0.9, 0.3])
    assert po.goal_function(x) == my_goal_function(x)  # verify call through the optimizer yields same results as if we call directly

    #print("Start Bobyqa")
    x_opt1 = po.optimize(x, CalibrationOption.bobyqa(500, 0.05, 1.0e-16))
    y1 = po.goal_function(x_opt1)

    #print("Start Global")
    x_opt2 = po.optimize(x, CalibrationOption.global_search(500, 1.0, 0.1e-6))
    y2 = po.goal_function(x_opt2)

    #print("Start Dream")
    x_opt3 = po.optimize(x, CalibrationOption.dream(10000))
    y3 = po.goal_function(x_opt3)

    #print("Start Sceua")
    x_opt4 = po.optimize(x, CalibrationOption.sceua(10000, 0.001, 0.0001))
    y4 = po.goal_function(x_opt4)
    assert np.allclose(x_opt1,xo,atol=0.01)
    assert np.allclose(x_opt2,xo,atol=0.01)
    assert np.allclose(x_opt3,xo,atol=0.5)  # dreams,sceua are less accurate
    assert np.allclose(x_opt4,xo,atol=0.2)
    yo= po.goal_function(xo)  # would give 0.0 if it is just pure distance evaluation
    assert y1 == approx(yo)
    assert y2 == approx(yo)
    assert y3 == approx(yo,abs=0.3)
    assert y3 == approx(yo,abs=0.2)

if __name__ == "__main__":
    test_parameter_optimizer()
