from shyft import hydrology as api
from shyft.time_series import (Calendar, deltahours, TimeAxis, TimeAxisFixedDeltaT, point_interpretation_policy, create_periodic_pattern_ts, TimeSeries)


class GridPPTest:
    """Verify GridPP API to process forecasts from met.no before feeding into shyft.
       Test diverse methods for transforming data sets to grids and vice versa.
       Expose API for IDW and BK from shyft core.
       Calculate bias TimeSeries using a Kalman filter algorithm. 
     """

    def __init__(self):
        self.cal = Calendar()
        self.dt = deltahours(1)
        self.nt = 24*10
        self.t0 = self.cal.time(2016, 1, 1)
        self.ta = TimeAxis(self.t0, self.dt, self.nt)
        self.ta1 = TimeAxisFixedDeltaT(self.t0, self.dt, self.nt)

        self.geo_points = api.GeoPointVector()
        self.geo_points.append(api.GeoPoint(100, 100, 1000))
        self.geo_points.append(api.GeoPoint(5100, 100, 1150))
        self.geo_points.append(api.GeoPoint(100, 5100, 850))

    def _create_obs_set(self, geo_points):
        obs_set = api.TemperatureSourceVector()
        fx = lambda z: [15 for x in range(self.nt)]
        ts = TimeSeries(ta=self.ta, values=fx(self.ta), point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
        for gp in geo_points:
            # Add only one TS per GP, but could be several
            geo_ts = api.TemperatureSource(gp, ts)
            obs_set.append(geo_ts)
        return obs_set

    def _make_fc_from_obs(self, obs_set, bias):
        fc_set = api.TemperatureSourceVector()
        bias_ts = TimeSeries(self.ta, fill_value=bias, point_fx=point_interpretation_policy.POINT_INSTANT_VALUE)
        for obs in obs_set:
            geo_ts = api.TemperatureSource(obs.mid_point(), obs.ts + bias_ts)
            fc_set.append(geo_ts)
        return fc_set

    def _predict_bias(self, obs_set, fc_set):
        # Return a set of bias_ts per observation geo_point
        bias_set = api.TemperatureSourceVector()
        kf = api.KalmanFilter()
        kbp = api.KalmanBiasPredictor(kf)
        kta = TimeAxis(self.t0, deltahours(3), 8)
        for obs in obs_set:
            kbp.update_with_forecast(fc_set, obs.ts, kta)
            pattern = api.KalmanState.get_x(kbp.state)
            # a_ts = TimeSeries(pattern, deltahours(3), self.ta)  # can do using ct of TimeSeries, or:
            b_ts = create_periodic_pattern_ts(pattern, deltahours(3), self.ta.time(0), self.ta)  # function
            bias_set.append(api.TemperatureSource(obs.mid_point(), b_ts))
        return bias_set


def test_calc_bias_should_match_observations():
    c = GridPPTest()
    # Simple test of bias predictor, without Kriging
    # Complete algorithm is described in shyft-doc/notebook
    obs_set = c._create_obs_set(c.geo_points)
    const_bias = 2.0
    fc_set = c._make_fc_from_obs(obs_set, const_bias)
    bias_set = c._predict_bias(obs_set, fc_set)
    assert len(bias_set) == len(obs_set)
    for bias in bias_set:
        for i in range(len(bias.ts)):
            assert bias.ts.value(i) - const_bias < 0.2
