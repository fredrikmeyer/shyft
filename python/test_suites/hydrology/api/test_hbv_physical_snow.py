from shyft.hydrology import HbvPhysicalSnowParameter, HbvPhysicalSnowState, HbvPhysicalSnowCalculator, HbvPhysicalSnowResponse
from shyft.time_series import deltahours
from shyft.time_series import Calendar


def test_hbv_physical_snow_parameter_sig1():
    p = HbvPhysicalSnowParameter(tx=0.0, lw=0.1, cfr=0.5,
                                 wind_scale=3.0, wind_const=1.3,
                                 surface_magnitude=10.2, max_albedo=0.9,
                                 min_albedo=0.5, fast_albedo_decay_rate=4.5,
                                 slow_albedo_decay_rate=3.6,
                                 snowfall_reset_depth=1.3,
                                 calculate_iso_pot_energy=False)

    assert round(abs(p.tx - 0.0), 7) == 0
    assert round(abs(p.lw - 0.1), 7) == 0
    assert round(abs(p.cfr - 0.5), 7) == 0
    assert round(abs(p.wind_scale - 3.0), 7) == 0
    assert round(abs(p.wind_const - 1.3), 7) == 0
    assert round(abs(p.surface_magnitude - 10.2), 7) == 0
    assert round(abs(p.max_albedo - 0.9), 7) == 0
    assert round(abs(p.min_albedo - 0.5), 7) == 0
    assert round(abs(p.fast_albedo_decay_rate - 4.5), 7) == 0
    assert round(abs(p.slow_albedo_decay_rate - 3.6), 7) == 0
    assert round(abs(p.snowfall_reset_depth - 1.3), 7) == 0
    assert p.calculate_iso_pot_energy == False


def test_hbv_physical_snow_parameter_sig2():
    p = HbvPhysicalSnowParameter(snow_redist_factors=[2.0, 2.0, 2.0],
                                 quantiles=[0.0, 0.5, 1.0])
    for (el, comp) in zip(p.s, [1.0, 1.0, 1.0]):  # automatic normalization to 1.0
        assert round(abs(el - comp), 7) == 0

    for (el, comp) in zip(p.intervals, [0.0, 0.5, 1.0]):  # should equal quantiles
        assert round(abs(el - comp), 7) == 0


def test_hbv_physical_snow_state():
    s = HbvPhysicalSnowState([0.3, 0.4, 0.5],
                             [1.3, 2.0, 3.1],
                             3200.0,
                             1000.0,
                             0.7)
    assert round(abs(s.surface_heat - 3200.0), 7) == 0
    assert round(abs(s.swe - 1000.0), 7) == 0
    assert round(abs(s.sca - 0.7), 7) == 0
    for (el, comp) in zip(s.albedo, [0.3, 0.4, 0.5]):
        assert round(abs(el - comp), 7) == 0
    for (el, comp) in zip(s.iso_pot_energy, [1.3, 2.0, 3.1]):
        assert round(abs(el - comp), 7) == 0


def test_hbv_physical_snow_step():
    utc = Calendar()
    s = HbvPhysicalSnowState()
    p = HbvPhysicalSnowParameter()
    r = HbvPhysicalSnowResponse()
    s.distribute(p)
    calc = HbvPhysicalSnowCalculator(p)
    t = utc.time(2016, 10, 1)
    dt = deltahours(1)
    temp = 0.4
    rad = 12.0
    prec_mm_h = 0.3
    wind_speed = 1.3
    rel_hum = 0.4
    # Just check that we don't get an error when stepping
    calc.step(s, r, t, dt, temp, rad, prec_mm_h, wind_speed, rel_hum)
