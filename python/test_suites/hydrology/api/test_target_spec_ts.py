import numpy as np
from shyft.time_series import (Calendar, TimeAxis, UtcTimeVector, point_interpretation_policy, DoubleVector, TimeSeries, TsFixed, deltahours, deltaminutes, IntVector, TsFactory, TimeAxisFixedDeltaT)

from shyft.hydrology import (TsTransform, ABS_DIFF, CELL_CHARGE, TargetSpecificationPts)


def test_calibration_ts_case():
    times = [0, 3600, 3600 + 2*3600]
    ta = TimeAxis(UtcTimeVector(times[0:-1]), times[-1])
    values = DoubleVector([0.0]*(len(times) - 1))
    ts = TimeSeries(ta, values, point_fx=point_interpretation_policy.POINT_AVERAGE_VALUE)
    target = TargetSpecificationPts(ts, IntVector([0]), 1.0, ABS_DIFF, 1.0, 1.0, 1.0, CELL_CHARGE, 'water_balance')
    assert target is not None


def test_ts_transform():
    c = Calendar()
    d = deltahours(1)
    n = 24
    t_start = c.trim(c.time(2018, 1, 1), d)
    ta = TimeAxisFixedDeltaT(t_start, d, n)
    dv = np.arange(ta.size())
    v = DoubleVector.from_numpy(dv)
    t = UtcTimeVector()
    for i in range(ta.size()):
        t.push_back(ta(i).start)
    dt = deltahours(1)
    tax = TimeAxisFixedDeltaT(t_start + deltaminutes(30), dt, ta.size())
    tsf = TsFactory()
    ts1 = tsf.create_point_ts(ta.size(), t_start, d, v)
    ts2 = tsf.create_time_point_ts(ta.total_period(), t, v)
    ts3 = TsFixed(tax, v, point_interpretation_policy.POINT_INSTANT_VALUE)

    tst = TsTransform()
    tt1 = tst.to_average(t_start, dt, tax.size(), ts1)
    tt1i = tst.to_average(int(t_start), int(dt), tax.size(), ts1)
    tt2 = tst.to_average(t_start, dt, tax.size(), ts2)
    tt2i = tst.to_average(int(t_start), int(dt), tax.size(), ts2)
    tt3 = tst.to_average(t_start, dt, tax.size(), ts3)
    tt3i = tst.to_average(int(t_start), int(dt), tax.size(), ts3)
    assert tt1.size() == tax.size()
    assert tt2.size() == tax.size()
    assert tt3.size() == tax.size()
    assert tt1.time_axis == tt1i.time_axis
    assert tt2.time_axis == tt2i.time_axis
    assert tt3.time_axis == tt3i.time_axis
