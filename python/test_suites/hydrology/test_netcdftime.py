from shyft.time_series import (Calendar, deltahours, deltaminutes, TimeAxisFixedDeltaT, YMDhms)
from shyft.hydrology.repository.netcdf.time_conversion import convert_netcdf_time
from shyft.hydrology import parse_cf_time
import numpy as np


def test_extract_conversion_factors_from_string():
    cryptonite='hours since 1970-01-01 00:00:00'
    p=parse_cf_time(cryptonite)
    assert p.timespan() == deltahours(1)
    assert p.start == 0


def test_unit_conversion():
    utc = Calendar()
    t_num = np.arange(-24, 24, 1, dtype=np.float64)  # we use both before and after epoch to ensure sign is ok
    t_converted = convert_netcdf_time('hours since 1970-01-01 00:00:00', t_num)
    t_axis = TimeAxisFixedDeltaT(utc.time(1969, 12, 31, 0, 0, 0), deltahours(1), 2*24)
    for i in range(t_axis.size()):
        assert t_converted[i] == t_axis(i).start
