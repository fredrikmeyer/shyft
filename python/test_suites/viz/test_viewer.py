from shyft.hydrology.viz.data_extractors.shyft_TsVector_data import TsVectorDataExtractor
from shyft.time_series import TsVector, TimeSeries, TimeAxis, time, POINT_AVERAGE_VALUE as stair_case


def test_ts_vector_data_extractor():
    ts_vct_dict = {
        'precipitation': TsVector([TimeSeries(TimeAxis(time(0), time(3600), 2), fill_value=float(i), point_fx=stair_case) for i in range(3)]),
        'temperature': TsVector([TimeSeries(TimeAxis(time(0), time(3600), 2), fill_value=float(i), point_fx=stair_case) for i in range(3)]),
    }
    e = TsVectorDataExtractor(ts_vct_dict=ts_vct_dict, catch_names=['a'], geom=None)
    assert e
    assert e.var_units
    assert e.temporal_vars and len(e.temporal_vars) == 2
    assert e.get_closest_time(time(0)) == time(0)
    assert e.get_closest_time(time(-1000)) == time(0)
    assert e.get_closest_time(time(3600)*5) == time(3600)
