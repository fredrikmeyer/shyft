from time import sleep

import pytest

from shyft.energy_market.stm import DStmClient, DStmServer, ModelState,Unit,UnitGroup,StmSystem,HydroPowerSystem
from shyft.time_series import (point_interpretation_policy as point_fx, TsVector, TimeSeries, TimeAxis, time, deltahours, DtsServer,
                               DtsClient, shyft_url, min as ts_min, Calendar,POINT_AVERAGE_VALUE
                               )
from typing import Tuple,TypeVar,Iterable,Callable,Optional
import numpy as np

shop = pytest.importorskip("shyft.energy_market.stm.shop")

if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)

def show_ts(title: str, ts: TimeSeries, scale: float = 1.0):
    s = ""
    if ts:
        r = TimeSeries(run_ta, fill_value=0.0, point_fx=stair_case)
        xx = ts.use_time_axis_from(r)  # just to resample all values to the run-time-axis
        for v in xx.values:
            if math.isfinite(v):
                s = s + f"{v*scale:5.1f} "
            else:
                s = s + "   .  "
    print(f"{title:20}:{s}")

def create_test_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    r = shop.ShopCommandList()
    if write_files: r.append(
        shop.ShopCommand.log_file(f"shop_log_{run_id}.txt")
    )
    r.extend([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])
    if write_files: r.extend([
        shop.ShopCommand.return_simres(f"shop_results_{run_id}.txt"),
        shop.ShopCommand.save_series(f"shop_series_{run_id}.txt"),
        shop.ShopCommand.save_xmlseries(f"shop_series_{run_id}.xml"),
        shop.ShopCommand.return_simres_gen(f"shop_genres_{run_id}.xml")
    ])
    return r


def test_remote_shop_logging(port_no, web_api_port, system_to_optimize):
    assert port_no != web_api_port
    srv = DStmServer()
    srv.set_listening_ip('127.0.0.1')
    srv.set_listening_port(port_no)
    srv.start_server()
    # Add a
    mid = "optimize"
    srv.do_add_model(mid, system_to_optimize)
    host = "127.0.0.1"
    client = DStmClient(f"{host}:{port_no}", 1000)
    try:
        assert len(client.get_model_ids()) == 1
        # optimize model remotely:
        t_begin = time('2018-10-17T10:00:00Z')
        t_end = time('2018-10-18T10:00:00Z')
        t_step = deltahours(1)
        n_steps = int((t_end - t_begin)/t_step)
        ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps)
        shop_commands = create_test_optimization_commands(1, False)
        assert len(client.get_log(mid)) == 0  # == "Unable to find log."
        assert client.optimize(mid, ta, shop_commands)
        while client.get_state(mid) != ModelState.FINISHED:  # poll until it's done (we resolve this later!)
            sleep(0.2)
        assert client.get_state(mid) == ModelState.FINISHED
        assert client.get_model(mid)  # This will not wait until optimization is complete
        assert len(client.get_log(mid)) > 200
    finally:
        client.close()
        srv.close()

def make_dstm_w_master_dtss(tmp_path, port_no, web_api_port)->Tuple[DStmServer,DStmClient,DStmServer,DtsServer]:
    assert port_no != web_api_port
    dtss = DtsServer()  # start the master dtss keeping the external ts data
    dtss.set_container("test", str(tmp_path/"test"))
    dtss_port = dtss.start_server()

    # the main server
    srv = DStmServer()
    srv.set_listening_port(port_no)
    srv.start_server()

    # the dstm compute node server that does the actual optimization
    cn = DStmServer()
    cn_port = cn.start_server()  # start the compute node(random port), get the port
    cn_host_port = f'localhost:{cn_port}'
    srv.add_compute_node(cn_host_port)  # then register it for usage at the main server
    assert len(srv.compute_node_info()) == 1
    #
    # IMPORTANT: THIS IS HOW YOU CONNECT THE DSTM to the MASTER DTSS
    #
    srv.set_master_slave_mode(ip='localhost',  # set this dstm to use the dtss above for all io
                              port=dtss_port,
                              master_poll_time=0.01,  # maximum time to stay out of sync
                              unsubscribe_threshold=10,  # reduce dstm subscription when it lost interest in more than 10 timeseries
                              unsubscribe_max_delay=1.0  # or maximum this delay (even if you unsubscribe just one, dont wait more than this)
                              )
    host = "127.0.0.1"
    client = DStmClient(f"{host}:{port_no}", 1000)
    m = DtsClient(f'localhost:{dtss_port}')
    return srv,client,cn,cn_host_port,dtss,m

def test_dstm_w_master_dtss(tmp_path, port_no, web_api_port, system_to_optimize):
    srv,client,cn,cn_host_port,dtss,m=make_dstm_w_master_dtss(tmp_path,port_no,web_api_port)
    mid = "optimize"
    mdl_prefix = f'dstm://M{mid}'
    mega = 1e6
    p_min = 10*mega  #
    p_max = 80*mega  #
    p_nom = p_max

    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2018-10-18T10:00:00Z')
    t_step = deltahours(1)
    n_steps = int((t_end - t_begin)/t_step)
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps)
    ww_ref_name = shyft_url('test', 'ww_ref')
    ww_ref_val = TimeSeries(ta,  # clipped values from the expected result.
                            [ 85.34211646,  86.06280654,  86.81629277,  87.60252609,  88.42520234,
                              89.28719041,  90.1916947 ,  91.14230867,  92.14308052,  93.19858737,
                              94.31403747,  95.49538507,  96.74947934,  98.08425096,  99.50894977,
                             101.20321047, 101.45649383, 101.45649383, 101.45649383, 101.45649383,
                             101.45649383, 101.45649383, 101.45649383, 101.45649383],
                            #[84.93987,85.64335,86.37711,87.14468,87.94600,88.78494,
                            # 89.66450,90.58805,91.55937,92.58270,93.66290,94.80545,
                            # 96.01668,97.30387,98.67547,100.16372,100.75776,100.75776,
                            # 100.75776,100.75776,100.75776,100.75776,100.75776,100.75776],
                            #[ 85.5685489, 86.30469309, 87.07370259, 87.87805865, 88.72052023,
                            ##    89.60416626, 90.53244632, 91.50924157, 92.53893999, 93.62652223,
                             #   94.77767977, 95.99894956, 97.2978896, 98.68330076, 100.19395813,
                             #   102.05592665, 102.06649264, 102.06649264, 102.06649264, 102.06649264,
                             #   102.06649264, 102.06649264, 102.06649264, 102.06649264],
                            point_fx.POINT_AVERAGE_VALUE
                            )
    ww_reference = TimeSeries(ww_ref_name, ww_ref_val)  # values below are copied from the result.
    z_name = shyft_url('test', 'zeros')
    z_val = TimeSeries(ta, len(ta)*[0.0], point_fx.POINT_AVERAGE_VALUE)
    z_ref = TimeSeries(z_name, z_val)
    u_name = shyft_url('test', 'u_prod_realised')
    u_val = TimeSeries(ta, len(ta)*[0.5*(p_min + p_max)], point_fx.POINT_AVERAGE_VALUE)
    u_ref = TimeSeries(u_name, u_val)
    f_name = shyft_url('test', 'f_cr_max')
    f_val = TimeSeries(ta, len(ta)*[1.1*p_max], point_fx.POINT_AVERAGE_VALUE)
    f_ref = TimeSeries(f_name, f_val)
    # Arrange the master dtss with the system price
    sys_price_ts_name = shyft_url('test', 'sys_price')
    orig_sys_price = system_to_optimize.market_areas[0].price.value
    tsv = TsVector([TimeSeries(sys_price_ts_name, orig_sys_price), ww_reference, z_ref, u_ref, f_ref])
    m.store_ts(tsv, True, True)
    system_to_optimize.market_areas[0].price.value = TimeSeries(sys_price_ts_name)  # set an unbound ts to the market
    system_to_optimize.market_areas[0].ts['price_max'] = TimeSeries(sys_price_ts_name) + 10  # using user specified unbound series.
    # arrange test case as pr. issue https://gitlab.com/shyft-os/shyft/-/issues/840
    ww = system_to_optimize.hydro_power_systems[0].waterways[1]  # main waterway, updated by shop
    ww.discharge.reference.value = TimeSeries(ww_ref_name).min_max_check_ts_fill(v_min=float('nan'), v_max=float('nan'), dt_max=time.max, cts=TimeSeries(ww.discharge.result.url(mdl_prefix)))  # make this an expression..
    ww.discharge.result.value = z_val  # to avoid empty ts when binding.. TODO: support empty as nans?
    ww.ts['deviation'] = TimeSeries(ww.discharge.result.url(mdl_prefix)) - ww.discharge.reference.value
    u = system_to_optimize.hydro_power_systems[0].find_unit_by_id(1)
    # computational expression for fcr-d
    u.reserve.droop.schedule = TimeSeries(ta, fill_value=8.0, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.reserve.droop.realised = TimeSeries(ta, fill_value=8.0, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.reserve.droop.min = TimeSeries(ta, fill_value=4.0, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.reserve.droop.max = TimeSeries(ta, fill_value=12.0, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.production.static_min.value = TimeSeries(ta, fill_value=p_min, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.production.static_max.value = TimeSeries(ta, fill_value=p_max, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.production.nominal.value = TimeSeries(ta, fill_value=p_nom, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.production.schedule.value = TimeSeries(ta, fill_value=60*mega, point_fx=point_fx.POINT_AVERAGE_VALUE)
    u.reserve.fcr_static_min.value = 0.9*u.production.static_min.value  # allow 10% more for short running fcr calcs
    u.reserve.fcr_static_max.value = 1.0*TimeSeries(f_name)  # 1.1*u.production.static_max.value  # --"--
    u.production.realised.value = TimeSeries(u_name)
    u.production.result.value = z_val  # just need a value here to avoid  empty ts expression for fcr_n.up.result

    # setup fcr expressions, using a mix of model refs ,and dtss refs.

    def ts(x) -> TimeSeries:
        """ @return TimeSeries(x.url(mdl_prefix) """
        return TimeSeries(x.url(mdl_prefix))

    def fcr_up(max_prod: TimeSeries, prod: TimeSeries, droop: TimeSeries) -> TimeSeries:
        """ @return fcr_up expression given max-prod, prod and droop"""
        return ts_min(max_prod - prod, prod*0.2/droop)

    # examples of using expressions that are a combinatio of model relative as well as dtss expressions
    u.reserve.fcr_n.up.realised.value = fcr_up(ts(u.production.static_max), ts(u.production.realised), ts(u.reserve.droop.realised))
    u.reserve.fcr_n.up.schedule.value = fcr_up(ts(u.production.static_max), ts(u.production.schedule), ts(u.reserve.droop.schedule))
    u.ts['fcr_n.up.result'] = fcr_up(ts(u.production.static_max), ts(u.production.result), ts(u.reserve.droop.schedule))
    u.ts['slack.up.result'] = ts(u.production.static_max) - ts(u.production.result)
    u.ts['slack.dn.result'] = ts(u.production.result) - ts(u.production.static_min)

    srv.do_add_model(mid, system_to_optimize)
    try:
        assert len(client.get_model_ids()) == 1

        #
        # IMPORTANT: When you have UNBOUND parameterized models, you need to evaluate those
        #            expressions prior to optimization.
        #
        client.evaluate_model(mid,  # we have to evaluate/bind the model PRIOR to optimize
                              bind_period=ta.total_period(),
                              use_ts_cached_read=True,
                              update_ts_cache=True)
        # optimize model remotely:
        shop_commands = create_test_optimization_commands(1, False)
        assert len(client.get_log(mid)) == 0  # == "Unable to find log."
        assert client.optimize(mid, ta, shop_commands)
        while client.get_state(mid) == ModelState.RUNNING:  # poll until it's done (we resolve this later!)
            sleep(0.2)
        assert client.get_state(mid) == ModelState.FINISHED
        cn_info = client.compute_node_info()
        assert len(cn_info) == 1
        result_model = client.get_model(mid)  # This will not wait until optimization is complete
        assert result_model
        uu = result_model.hydro_power_systems[0].find_unit_by_id(1)
        assert abs((uu.ts['fcr_n.up.result'] - uu.reserve.fcr_n.up.result.value).values.to_numpy().sum()) < 0.001*mega  # allow some slack,
        assert (orig_sys_price - result_model.market_areas[0].price.value).abs().values.to_numpy().sum() < 0.001  # the proof, the dstm talked to the master
        assert (orig_sys_price + 10.0 - result_model.market_areas[0].ts['price_max']).abs().values.to_numpy().sum() < 0.001  # also for user specified
        # ref issue 840, lets check the deviation expression.
        #print(result_model.hydro_power_systems[0].waterways[1].discharge.result.value.values.to_numpy()) # Expected result - clipped into ww_ref_val above!
        assert abs(result_model.hydro_power_systems[0].waterways[1].ts['deviation'].values.to_numpy().sum()) < 0.001  # our setup uses the ref. flow as copied from result
        assert len(client.get_log(mid)) > 200
        summary = client.get_optimization_summary(mid)
        assert summary
        #
        # How to get time-series from a specific dstm model
        #
        ts_urls = system_to_optimize.result_ts_urls(f'dstm://M{mid}')  # this generate all time-series that could have results
        assert len(ts_urls)
        rts = client.get_ts(mid, ts_urls)  # this fetches the time-series back.
        assert len(rts) == len(ts_urls)
        # just verify we can maninpulate the state
        client.set_state(mid, ModelState.IDLE)
        assert client.get_state(mid) == ModelState.IDLE
        client.set_state(mid, ModelState.SETUP)
        assert client.get_state(mid) == ModelState.SETUP
        # verify we can remove compute node
        client.remove_compute_node(cn_host_port)
        assert len(srv.compute_node_info()) == 0
    finally:
        client.close()
        srv.close()
        cn.close()
        dtss.stop_server()


def test_delivery_schedule_ts_expression_dstm(tmp_path, port_no, web_api_port):
    """
    Thanks to Ole Andreas Ramsdal for providing a more complex(aka realistic) evaluation
    example that revealed that we had to take care of interdependent expressions as well.
    """
    srv, dstm_client, cn, cn_host_port, dtss, dts_client = make_dstm_w_master_dtss(tmp_path, port_no, web_api_port)
    T = TypeVar("T")

    def dstm_url(mdl_key: str, attr, attr_template_levels=-1) -> str:
        attr_url = attr.url(f"dstm://M{mdl_key}", template_levels=attr_template_levels)
        return attr_url

    def make_sum_ts_expression(objs: Iterable[T], f: Callable[[T], TimeSeries]) -> TimeSeries:
        return TsVector([f(o) for o in objs]).sum()

    def delivery_schedule_ts_expression(ug: UnitGroup, mdl_key: Optional[str] = None) -> TimeSeries:
        def f_production(m):
            if mdl_key:
                x = TimeSeries(dstm_url(mdl_key, m.unit.production.schedule))
            else:
                x = m.unit.production.schedule.value
            if m.active.exists:
                x *= m.active.value
            return x

        f_map = {
            UnitGroup.PRODUCTION: f_production
        }
        f = f_map.get(ug.group_type, None)
        if not f:
            raise NotImplementedError(f"Delivery schedule expression not implemented for {ug.group_type}")
        return make_sum_ts_expression(ug.members, f)

    def tmpl(o_tag):
        return shyft_url("test", f"{o_tag}.test_schedule")

    def corr_tmpl(o_tag):
        return shyft_url("test", f"{o_tag}.test_correction_schedule")
    try:
        t0 = time("2022-06-01T00:00:00Z")
        dt = Calendar.HOUR
        ta = TimeAxis(t0, dt, 3)
        mdl_key = "sys1"
        u1_sched = TimeSeries(tmpl(911), TimeSeries(ta, 5., POINT_AVERAGE_VALUE))
        u1_sched_corr = TimeSeries(corr_tmpl(911), TimeSeries(ta, float('nan'), POINT_AVERAGE_VALUE))
        u2_sched = TimeSeries(tmpl(912), TimeSeries(ta, 4., POINT_AVERAGE_VALUE))
        tsv = TsVector([u1_sched, u1_sched_corr, u2_sched])
        dts_client.store_ts(tsv, overwrite_on_write=True, cache_on_write=True)
        stm_sys = StmSystem(1, "sys", "")
        hps = HydroPowerSystem(1, "tmp-hps")
        stm_sys.hydro_power_systems.append(hps)
        u1 = hps.create_unit(911, "u1")
        u1.production.schedule.value = TimeSeries(u1_sched.ts_id()).min_max_check_ts_fill(v_min=float('nan'), v_max=float('nan'), dt_max=time.max, cts=TimeSeries(u1_sched_corr.ts_id()))  # TODO not supported in shyft atm. (13.06.2022)
        u1_m_ts = TimeSeries(ta, [1, 1, 0], POINT_AVERAGE_VALUE)
        u2 = hps.create_unit(912, "u2")
        u2.production.schedule.value = TimeSeries(u2_sched.ts_id())
        ug = stm_sys.add_unit_group(1, "ug1", "", UnitGroup.PRODUCTION)
        ug.add_unit(u1, u1_m_ts)
        ug.add_unit(u2, TimeSeries())
        ug.delivery.schedule.value = delivery_schedule_ts_expression(ug, mdl_key)
        dstm_client.add_model(mdl_key, stm_sys)
        eval_result= dstm_client.evaluate_model(mdl_key, ta.total_period())
        stm_sys = dstm_client.get_model(mdl_key)
        ug = stm_sys.unit_groups[0]
        assert np.allclose(ug.delivery.schedule.value.values.to_numpy(), [9., 9., 4.])
    finally:
        dts_client.close()
        dstm_client.close()
        srv.close()
        cn.close()
        dtss.stop_server()

def test_dstm_w_master_dtss_kill_by_signal(tmp_path, port_no, web_api_port, system_to_optimize_long, catch_sig_term):
    srv,client,cn,cn_host_port,dtss,m=make_dstm_w_master_dtss(tmp_path,port_no,web_api_port)
    mid = "optimize"

    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2019-01-17T10:00:00Z')
    t_step = deltahours(1)
    n_steps = int((t_end - t_begin)/t_step)
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps)
    
    srv.do_add_model(mid, system_to_optimize_long) # Use a long running optimization to test kill (~9 sec runtime)
    try:
        assert len(client.get_model_ids()) == 1

        #
        # IMPORTANT: When you have UNBOUND parameterized models, you need to evaluate those
        #            expressions prior to optimization.
        #
        client.evaluate_model(mid,  # we have to evaluate/bind the model PRIOR to optimize
                              bind_period=ta.total_period(),
                              use_ts_cached_read=True,
                              update_ts_cache=True)

        shop_commands = create_test_optimization_commands(1, False)

        kill_handler = catch_sig_term();
        assert kill_handler.sigterm_raised == False

        assert client.kill_optimization(mid) == False # no optimization running, kill not accepted
        assert client.optimize(mid, ta, shop_commands)
        sleep(0.2)
        assert client.kill_optimization(mid) == True # kill accepted

        while client.get_state(mid) == ModelState.RUNNING:
            sleep(0.2)

        # Verify states after kill
        assert client.get_state(mid) == ModelState.FAILED
        assert kill_handler.sigterm_raised
        cn_info = client.compute_node_info()
        assert len(cn_info) == 1
        assert cn_info[0].kill_count == 1

    finally:
        client.close()
        srv.close()
        cn.close()
        dtss.stop_server()

def test_dstm_w_master_dtss_kill_by_timeout(tmp_path, port_no, web_api_port, system_to_optimize_long, catch_sig_term):
    srv,client,cn,cn_host_port,dtss,m=make_dstm_w_master_dtss(tmp_path,port_no,web_api_port)
    mid = "optimize"

    t_begin = time('2018-10-17T10:00:00Z')
    t_end = time('2019-01-17T10:00:00Z')
    t_step = deltahours(1)
    n_steps = int((t_end - t_begin)/t_step)
    ta = TimeAxis(t_begin.seconds, t_step.seconds, n_steps)

    srv.do_add_model(mid, system_to_optimize_long) # Use a long running optimization to test timeout (~9 sec runtime)
    try:
        assert len(client.get_model_ids()) == 1

        #
        # IMPORTANT: When you have UNBOUND parameterized models, you need to evaluate those
        #            expressions prior to optimization.
        #
        client.evaluate_model(mid,  # we have to evaluate/bind the model PRIOR to optimize
                              bind_period=ta.total_period(),
                              use_ts_cached_read=True,
                              update_ts_cache=True)

        shop_commands= shop.ShopCommandList()
        shop_commands.extend([
            shop.ShopCommand.set_method_primal(),
            shop.ShopCommand.set_code_full(),
            shop.ShopCommand.set_timelimit(1), # Use minimal timelimit to trigger hard timeout
            shop.ShopCommand.start_sim(3)
        ])

        kill_handler = catch_sig_term();
        assert kill_handler.sigterm_raised == False

        assert client.optimize(mid, ta, shop_commands)
        sleep(0.2)

        while client.get_state(mid) == ModelState.RUNNING:
            sleep(0.2)

        # Verify states after timeout (which will kill the node)
        client.get_state(mid) == ModelState.FAILED
        cn_info = client.compute_node_info()
        assert len(cn_info) == 1
        assert kill_handler.sigterm_raised
        assert cn_info[0].kill_count == 1

    finally:
        client.close()
        srv.close()
        cn.close()
        dtss.stop_server()