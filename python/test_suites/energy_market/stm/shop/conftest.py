from shyft.time_series import time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE
from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ, TurbineEfficiency, TurbineDescription
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list

import signal
import pytest

shop = pytest.importorskip("shyft.energy_market.stm.shop")


def create_test_hydro_power_system(*, hps_id: int = 1, name: str, duration: int) -> HydroPowerSystem:
    """ helper to create a test system """
    mega = 1000000
    hps = HydroPowerSystem(hps_id, name)
    t0 = time('2000-01-01T00:00:00Z')
    rsv = hps.create_reservoir(1, 'rsv')
    rsv.volume_level_mapping.value = create_t_xy(t0, XyPointCurve(PointList([Point(0.0*mega, 80.0), Point(2.0*mega, 90.0), Point(3.0*mega, 95.0), Point(5.0*mega, 100.0), Point(16.0*mega, 105.0)])))
    rsv.level.regulation_max.value = create_t_double(t0, 100.0)
    rsv.level.regulation_min.value = create_t_double(t0, 80.0)
    rsv.water_value.endpoint_desc.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), duration), fill_value=36.5/mega, point_fx=POINT_AVERAGE_VALUE)
    rsv.level.realised.value = TimeSeries(TimeAxis(time('2018-10-17T09:00:00Z'), time(3600), duration+1), fill_value=90.0, point_fx=POINT_AVERAGE_VALUE)
    rsv.inflow.schedule.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), duration), fill_value=55.0, point_fx=POINT_AVERAGE_VALUE)

    u1 = hps.create_unit(1, 'u1')
    u1.generator_description.value = create_t_xy(t0, XyPointCurve(PointList([Point(20.0*mega, 96.0), Point(40.0*mega, 98.0), Point(60.0*mega, 99.0), Point(80.0*mega, 98.0)])))
    u1.turbine_description.value = create_t_turbine_description(t0,
                                                                [XyPointCurveWithZ(
                                                                    XyPointCurve([Point(20.0, 70.0), Point(40.0, 85.0), Point(60.0, 92.0), Point(80.0, 94.0), Point(100.0, 92.0), Point(110.0, 90.0)]),
                                                                    70.0)
                                                                ])
    plant = hps.create_power_plant(1, 'plant')
    plant.add_unit(u1)
    plant.outlet_level.value = create_t_double(t0, 10.0)

    flood = hps.create_river(1, 'flood')
    flood.discharge.static_max.value = create_t_double(t0, 150.0)
    gt = flood.add_gate(1, "flood_gate", "")
    gt.flow_description.value = create_t_xyz_list(t0, XyPointCurve([Point(100.0, 0.0), Point(101.5, 25.0), Point(103.0, 80.0), Point(104.0, 150.0)]), 0.0)
    main = hps.create_tunnel(2, 'main')
    main.head_loss_coeff.value = create_t_double(t0, 0.00030)

    penstock = hps.create_tunnel(3, 'penstock')
    penstock.head_loss_coeff.value = create_t_double(t0, 0.00005)

    outlet = hps.create_tunnel(4, "outlet")

    tailrace = hps.create_tunnel(5, "tailrace")

    river = hps.create_river(6, "river")

    flood.input_from(rsv, ConnectionRole.flood).output_to(river)
    main.input_from(rsv).output_to(penstock)
    penstock.output_to(u1)
    u1.output_to(outlet)
    outlet.output_to(tailrace)
    tailrace.output_to(river)
    return hps


def create_test_stm_system(idx: int, name: str, duration: int) -> StmSystem:
    """ helper to create a complete system with market and stm hps"""
    mega = 1000000
    a = StmSystem(idx, name, '{}')
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='test hps', duration=duration))
    no_1 = MarketArea(1, 'NO1', '{}', a)
    run_ta = TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), duration)
    no_1.price.value = TimeSeries(run_ta, fill_value=40.0/mega, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(run_ta, fill_value=1300.0*mega, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries(run_ta, fill_value=9999.0*mega, point_fx=POINT_AVERAGE_VALUE)
    no_1.max_sale.value = TimeSeries(run_ta, fill_value=9999.0*mega, point_fx=POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(run_ta, fill_value=5.0*mega, point_fx=POINT_AVERAGE_VALUE)
    a.market_areas.append(no_1)
    return a


@pytest.fixture
def system_to_optimize() -> StmSystem:
    return create_test_stm_system(1, "test stm_system", 240)

@pytest.fixture
def system_to_optimize_long() -> StmSystem:
    return create_test_stm_system(1, "test stm_system", 240*90) # 3 months

class catch_sig_term_helper:
  sigterm_raised = False
  def __init__(self):
    signal.signal(signal.SIGTERM, self.sig_term_handler)

  def sig_term_handler(self, *args):
    self.sigterm_raised = True

@pytest.fixture
def catch_sig_term():
    return catch_sig_term_helper

