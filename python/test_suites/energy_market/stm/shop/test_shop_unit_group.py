import math
from typing import Tuple, Callable, List, Any

import numpy as np
import pytest

from shyft.energy_market.core import ConnectionRole, Point, PointList, XyPointCurve, XyPointCurveWithZ
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, UnitGroup, Unit
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list
from shyft.time_series import min, max, TsVector, time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case, DoubleVector

shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def create_demo_stm_system(run_ta: TimeAxis, id: int, name: str, n_units: int = 3) -> StmSystem:
    """
    create a demo stm system with load obligation first period, then market-access last period

    """
    _i = [id]

    def uid() -> int:
        """ unique id-generator"""
        _i[0] += 1
        return _i[0]

    mega = 1000000
    sys = StmSystem(id, name, '{}')
    n = len(run_ta)
    mv = n//2*[1.0]
    mv.extend(n//2*[0.0])  # make 1-0 masks that we can multiply to mask in/out values in the periods
    load_period_mask = TimeSeries(run_ta, mv, point_fx=stair_case)
    sale_period_mask = 1.0 - load_period_mask

    def wave_(ta: TimeAxis, offset: float, amplitude: float, n_periods: float = 3.0) -> DoubleVector:
        """ ts-generator: f(t) = offset + amplitude*sin( w*t) , w= .. """
        n = len(ta)
        return DoubleVector([offset + amplitude*math.sin(n_periods*2*3.14*i/n) for i in range(n)])

    def create_market_area(id: int, name: str) -> MarketArea:
        """ create a market area with load first period,
            then open for market exchange in second period,
            with a  minor load requirement"""
        ma = MarketArea(id, name, '{}', sys)
        ma.price = TimeSeries(run_ta, values=wave_(run_ta, 36.0/mega, 10.0/mega), point_fx=stair_case)
        ma.max_buy = sale_period_mask*TimeSeries(run_ta, fill_value=99.0*mega, point_fx=stair_case)
        ma.max_sale = sale_period_mask*TimeSeries(run_ta, fill_value=99.0*mega, point_fx=stair_case)
        ma.load = (load_period_mask*TimeSeries(run_ta, values=wave_(run_ta, 90.0*mega, 15*mega), point_fx=stair_case)
                   + sale_period_mask*TimeSeries(run_ta, values=wave_(run_ta, 37.0*mega, 2*mega), point_fx=stair_case))
        return ma

    def create_reserve_groups(sys: StmSystem) -> Tuple[UnitGroup, UnitGroup, UnitGroup, UnitGroup, UnitGroup]:
        cost = TimeSeries(run_ta, fill_value=1500.0/mega, point_fx=stair_case)

        def create_group(name: str, gtype: UnitGroup.unit_group_type, obl: TimeSeries) -> UnitGroup:
            g = sys.add_unit_group(uid(), name, "{}")
            g.group_type = gtype
            g.obligation.schedule = obl
            g.obligation.cost = cost
            return g

        m = sale_period_mask
        fcr_n_up = create_group("fcr-n-up",
                                UnitGroup.unit_group_type.FCR_N_UP,
                                obl=m*TimeSeries(run_ta, fill_value=5.0*mega,
                                                 point_fx=stair_case))
        fcr_n_down = create_group("fcr-n-down",
                                  UnitGroup.unit_group_type.FCR_N_DOWN,
                                  obl=m*TimeSeries(run_ta, fill_value=5.0*mega,
                                                   point_fx=stair_case))
        afrr_up = create_group("afrr-up",
                               UnitGroup.unit_group_type.AFRR_UP,
                               obl=m*TimeSeries(run_ta, fill_value=30*mega,
                                                point_fx=stair_case))
        afrr_down = create_group("afrr-down",
                                 UnitGroup.unit_group_type.AFRR_DOWN,
                                 obl=m*TimeSeries(run_ta, fill_value=30*mega,
                                                  point_fx=stair_case))
        afrr_down2 = create_group("afrr-down2",
                                  UnitGroup.unit_group_type.AFRR_DOWN,
                                  obl=m*TimeSeries(run_ta, fill_value=13*mega,
                                                   point_fx=stair_case))

        return fcr_n_up, fcr_n_down, afrr_up, afrr_down, afrr_down2
    no1=create_market_area(id=uid(), name='NO1')
    sys.market_areas.append(no1)
    fcr_n_up, fcr_n_down, afrr_up, afrr_down, afrr_down2 = create_reserve_groups(sys)

    no1.unit_group=sys.add_unit_group(uid(), name=no1.name,json='{"description": "units delivering production into NO1"}', group_type=UnitGroup.unit_group_type.PRODUCTION)

    hps = HydroPowerSystem(uid(), "hps1")
    t0 = time('2000-01-01T00:00:00Z')  # historical point where this hps was constructed, so time-dependent constant are valid from this time.

    # reservoir ~elevation 80..100 meter, 0..160 Mm3, realised filling is 99 masl
    rsv = hps.create_reservoir(uid(), 'rsv')
    rsv.volume_level_mapping = create_t_xy(t0, XyPointCurve(PointList([Point(0.0*mega, 80.0), Point(20.0*mega, 90.0), Point(30.0*mega, 95.0), Point(50.0*mega, 100.0), Point(160.0*mega, 105.0)])))
    rsv.level.regulation_max = create_t_double(t0, 100.0)
    rsv.level.regulation_min = create_t_double(t0, 80.0)
    rsv.water_value.endpoint_desc = TimeSeries(run_ta, fill_value=36.5/mega, point_fx=stair_case)
    rsv.level.realised = TimeSeries(run_ta, fill_value=99.0, point_fx=stair_case).time_shift(-run_ta.total_period().timespan() + time(3600))  # make it overlap
    rsv.inflow.schedule = TimeSeries(run_ta, values=wave_(run_ta, 55.0, 10.0), point_fx=stair_case)

    # plant, - with indicated outlet level 10 masl, gives 70..90 masl net head for the units.
    plant = hps.create_power_plant(uid(), 'plant')
    plant.outlet_level = create_t_double(t0, 10.0)

    # a flood waterroute, with max 150 m3/s, with a gate attached, 0 m3/s @ 100 masl, 150 m3 @ 104 masl
    flood = hps.create_river(uid(), 'flood')
    flood.discharge.static_max = create_t_double(t0, 150.0)
    gt = flood.add_gate(1, "flood_gate", "")
    gt.flow_description = create_t_xyz_list(t0, XyPointCurve([Point(100.0, 0.0), Point(101.5, 25.0), Point(103.0, 80.0), Point(104.0, 150.0)]), 0.0)

    # the main tunnel from reservoir to the unit's inside the plant
    main = hps.create_tunnel(uid(), 'main')
    main.head_loss_coeff = create_t_double(t0, 0.000030)

    # a common tailrace for the units
    tailrace = hps.create_tunnel(uid(), "tailrace")

    # downstream powerplant, a river, that receives the flow from tailrace and the flood gate
    river = hps.create_river(uid(), "river")

    n_ug_members = 1 + n_units//2
    is_active = TimeSeries(run_ta, fill_value=1.0, point_fx=stair_case)*sale_period_mask
    fcr_schedule = TimeSeries(run_ta.slice(0, 24), fill_value=1.0*mega, point_fx=stair_case)
    zero = TimeSeries(run_ta, fill_value=1.0*mega, point_fx=stair_case)
    no1_active = TimeSeries(run_ta, fill_value=1.0, point_fx=stair_case)  # always active in no1

    afrr_schedule = TimeSeries(run_ta.slice(0, 24), fill_value=2.0*mega, point_fx=stair_case)
    for i in range(n_units):
        u = hps.create_unit(uid(), f'u{i + 1}')
        plant.add_unit(u)
        # add unit to the no1 energy market area
        no1.unit_group.add_unit(u,no1_active)  # always active
        # add unit to reserve markets groups
        # add reserve schedules for the 'load-period'
        if i == 0:  # for the demo we let first unit have different membership
            fcr_n_up.add_unit(u, is_active)
            fcr_n_down.add_unit(u, is_active)
            afrr_up.add_unit(u, is_active)
            mask12 = TimeSeries(run_ta, fill_value=0.0, point_fx=stair_case)
            for i in range(12):
                mask12.set(24 + i, 1.0)
            afrr_down.add_unit(u, is_active*mask12)  # member first 12h of sale period
            afrr_down2.add_unit(u, is_active*(1 - mask12))  # member only last 12 h of last period
            afrr_down2.obligation.schedule = 13*mega*is_active*(1 - mask12)  # make schedule for this unit only
        else:
            fcr_n_up.add_unit(u, is_active)
            fcr_n_down.add_unit(u, is_active)
            afrr_up.add_unit(u, is_active)
            afrr_down.add_unit(u, is_active)

        if i == 0:  # unit 0 only have schedule for the 'sale' period
            u0_schedule = TimeSeries(run_ta.slice(24, 12), fill_value=12.0*mega, point_fx=stair_case)
            u.reserve.afrr.down.schedule = u0_schedule  # TimeSeries(run_ta.slice(24,24),fill_value=12.0*mega,point_fx=stair_case)*sale_period_mask
            u.reserve.afrr.down.max = u0_schedule  #
            u.reserve.afrr.down.min = zero  # *load_period_mask
            # u.reserve.afrr.up.max = afrr_schedule*load_period_mask  #
            # u.reserve.afrr.up.min = zero *load_period_mask
        else:
            u.reserve.fcr_n.up.schedule = fcr_schedule*load_period_mask
            u.reserve.fcr_n.down.schedule = fcr_schedule*load_period_mask
            u.reserve.afrr.down.schedule = afrr_schedule*load_period_mask
            u.reserve.afrr.up.schedule = afrr_schedule*load_period_mask
        # configure the long running min..max and fcr min.. max
        p_min = 10*mega  #
        p_max = 80*mega  #
        p_nom = p_max
        u.production.static_min = TimeSeries(run_ta, fill_value=p_min, point_fx=stair_case)
        u.production.static_max = TimeSeries(run_ta, fill_value=p_max, point_fx=stair_case)
        u.production.nominal = TimeSeries(run_ta, fill_value=p_nom, point_fx=stair_case)
        u.reserve.fcr_static_min = 0.9*u.production.static_min.value  # allow 10% more for short running fcr calcs
        u.reserve.fcr_static_max = 1.1*u.production.static_max.value  # --"--
        # configure the droop setting(schedule), and min..max range
        u.reserve.droop.schedule = load_period_mask*TimeSeries(run_ta, fill_value=8.0, point_fx=stair_case) + sale_period_mask*TimeSeries(run_ta, fill_value=6.0, point_fx=stair_case)
        u.reserve.droop.min = TimeSeries(run_ta, fill_value=4.0, point_fx=stair_case)
        u.reserve.droop.max = TimeSeries(run_ta, fill_value=12.0, point_fx=stair_case)
        u.reserve.droop_steps = create_t_xy(t0, # set discrete steps shop can use for droop
            XyPointCurve(PointList([Point(j+1, 4+2*j) for j in range(i+1) ]))) # each unit different setting
        # generator and turbine-description (efficiency and operating range)
        u.generator_description = create_t_xy(t0,
                                              XyPointCurve(PointList(
                                                  [Point(10.0*mega, 96.0), Point(20.0*mega, 96.0), Point(40.0*mega, 98.0), Point(60.0*mega, 99.0), Point(80.0*mega, 98.0)])))
        u.turbine_description = create_t_turbine_description(t0,
                                                             [XyPointCurveWithZ(
                                                                 XyPointCurve([Point(10.0, 60.0 + i), Point(20.0, 70.0 + i), Point(40.0, 85.0 + i), Point(60.0, 92.0 + i), Point(80.0, 94.0 + i), Point(100.0, 92.0 + i), Point(110.0, 90.0 + i)]),
                                                                 70.0
                                                             ), XyPointCurveWithZ(
                                                                 XyPointCurve([Point(8.0, 60.0 + i), Point(18.0, 70.0 + i), Point(40.0, 86.0 + i), Point(60.0, 93.0 + i), Point(80.0, 95.0 + i), Point(100.0, 93.0 + i), Point(110.0, 89.0 + i)]),
                                                                 90.0
                                                             )]

                                                             )

        penstock = hps.create_tunnel(uid(), f'penstock{i}')
        penstock.head_loss_coeff.value = create_t_double(t0, 0.00005)
        penstock.input_from(main).output_to(u)
        u.output_to(tailrace)

    # connect reservoir->flood -> river
    flood.input_from(rsv, ConnectionRole.flood).output_to(river)
    main.input_from(rsv)  # reservoir -> tunnel
    tailrace.output_to(river)  # tailrace ->  river

    sys.hydro_power_systems.append(hps)
    return sys


def create_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    return shop.ShopCommandList([
        # shop.ShopCommand.set_universal_mip_on(),# takes 4 seconds:
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.set_droop_discretization_limit(6.0),  # new command, require license
        shop.ShopCommand.start_sim(2),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])


def sum_of(collection: List[Any], ts: Callable[[Any], TimeSeries]) -> TimeSeries:
    """
    Utility: given a collection, compute sum of some attribute as returned by the
    callable
    @param collection iterable list of some element
    @param ts a callable, that takes 'some element' as arg, and return TimeSeries
    @returns time-series equal to the sum of the elements.
    """
    return TsVector([ts(e) for e in collection if ts(e)]).sum()


_p_min: float = 0.1  # smallest prod recognized as production
_p_max: float = 1e33  # largest prod recognized as production


def compute_prod_slack_up(unit: Unit) -> TimeSeries:
    """ compute upward running reserve  """
    global _p_max, _p_min
    prod = unit.production.result.value
    prod_max = unit.production.static_max.value  # min(unit.production.static_max,unit.reserve.fcr_static_max)
    slack_up = (prod_max - prod).max(0.0)
    prod_mask = prod.inside(_p_min, _p_max, 0, 1.0, 0.0)
    return prod_mask*slack_up


def compute_prod_slack_down(unit: Unit) -> TimeSeries:
    """ compute downward running reserve  """
    global _p_max, _p_min
    prod = unit.production.result.value
    prod_min = unit.production.static_min
    slack_down = (prod - prod_min).max(0.0)
    prod_mask = prod.inside(_p_min, _p_max, 0, 1.0, 0.0)
    return prod_mask*slack_down


def compute_fcr_n_up(unit: Unit) -> TimeSeries:
    """ Given unit, with production, and droop schedule compute FCR-N up """
    global _p_max, _p_min
    prod = unit.production.result.value
    prod_max = max(unit.production.static_max.value, unit.reserve.fcr_static_max.value)
    slack_up = (prod_max - prod).max(0.0)  # 0.98, 2% of max slack
    droop = unit.reserve.droop.schedule.value
    prod_fcr = 0.1*2.0*unit.production.static_max.value/droop  # max dev is 0.1 hz in nordic
    eff_fcr_up = min(prod_fcr, slack_up)  # clip against upper-fcr-slack
    prod_mask = prod.inside(_p_min, _p_max, 0, 1.0, 0.0)
    return prod_mask*eff_fcr_up


def compute_fcr_n_down(unit: Unit) -> TimeSeries:
    """Given unit, with production, and droop schedule compute FCR-N down """
    global _p_max, _p_min
    prod = unit.production.result.value
    prod_min = min(unit.production.static_min.value, unit.reserve.fcr_static_min.value)
    slack_down = (prod - prod_min).max(0.0)
    droop = unit.reserve.droop.schedule.value
    prod_fcr = 0.1*2.0*unit.production.static_max.value/droop  # nom.. max dev is 0.1 hz in nordic
    eff_fcr_down = min(prod_fcr, slack_down)
    prod_mask = prod.inside(_p_min, _p_max, 0, 1.0, 0.0)
    return prod_mask*eff_fcr_down


def present_demo_results(run_ta: TimeAxis, sys: StmSystem):
    """
    Just print out the input and the results including
    balances for the result.
    """
    MW: float = 1.0/1e6  # just to make reading and scaling easier+consistent

    def show_ts(title: str, ts: TimeSeries, scale: float = 1.0):
        s = ""
        if ts:
            r = TimeSeries(run_ta, fill_value=0.0, point_fx=stair_case)
            xx = ts.use_time_axis_from(r)  # just to resample all values to the run-time-axis
            for v in xx.values:
                if math.isfinite(v):
                    s = s + f"{v*scale:5.1f} "
                else:
                    s = s + "   .  "
        print(f"{title:20}:{s}")

    def ug(ug_type: UnitGroup.unit_group_type) -> List[UnitGroup]:
        r: UnitGroup = []
        for g in sys.unit_groups:
            if g.group_type == ug_type:
                r.append(g)
        if not r:
            raise RuntimeError(f"failed to fined unit group type {ug_type}")
        return r

    sum_prod_mw = sum_of(sys.hydro_power_systems[0].units, lambda u: u.production.result)*MW
    # sum_flow_m3s= sum_of(sys.hydro_power_systems[0].units,lambda u:u.discharge.result)

    print("\nresults\n")
    show_ts('price[x]', sys.market_areas[0].price.value, 1.0/MW)
    show_ts('load [MW]', sys.market_areas[0].load.value, MW)
    show_ts('prod [MW]', sum_prod_mw)
    show_ts('.diff[MW]', sys.market_areas[0].load.value*MW - sum_prod_mw)
    print("--- market")
    show_ts('sale[MW]', sys.market_areas[0].sale.value*MW)
    show_ts('buy [MW]', sys.market_areas[0].buy.value*MW)
    show_ts('mdiff[MW]', sys.market_areas[0].load.value*MW - sum_prod_mw - sys.market_areas[0].sale.value*MW - sys.market_areas[0].buy.value*MW)
    print("--- details ")
    for u in sys.hydro_power_systems[0].units:
        show_ts(f'..{u.name}[MW]', u.production.result.value*MW)

    print("\nReserve groups obligation")
    for gt in [UnitGroup.unit_group_type.FCR_N_UP, UnitGroup.unit_group_type.FCR_N_DOWN, UnitGroup.unit_group_type.AFRR_UP, UnitGroup.unit_group_type.AFRR_DOWN]:
        gs = ug(gt)
        for g in gs:
            show_ts(f'{g.name} ', g.obligation.schedule.value, MW)

    print("\nUnit reserve schedules summary")
    zero = TimeSeries(run_ta, fill_value=0.0, point_fx=stair_case)

    def z_fill(ts: TimeSeries) -> TimeSeries:
        if ts:
            if ts.total_period().contains(zero.total_period()):
                return ts
            return TimeSeries(run_ta, fill_value=0.0, point_fx=stair_case).merge_points(ts)
        else:
            return zero

    fcr_n_up_schedule = sum_of(sys.hydro_power_systems[0].units, lambda u: z_fill(u.reserve.fcr_n.up.schedule.value))
    fcr_n_down_schedule = sum_of(sys.hydro_power_systems[0].units, lambda u: z_fill(u.reserve.fcr_n.down.schedule.value))
    afrr_up_schedule = sum_of(sys.hydro_power_systems[0].units, lambda u: z_fill(u.reserve.afrr.up.schedule.value))
    afrr_down_schedule = sum_of(sys.hydro_power_systems[0].units, lambda u: z_fill(u.reserve.afrr.down.schedule.value))

    ug_fcr_n_up_schedule = sum_of(ug(UnitGroup.unit_group_type.FCR_N_UP), lambda g: g.obligation.schedule.value)
    ug_fcr_n_down_schedule = sum_of(ug(UnitGroup.unit_group_type.FCR_N_DOWN), lambda g: g.obligation.schedule.value)
    ug_afrr_up_schedule = sum_of(ug(UnitGroup.unit_group_type.AFRR_UP), lambda g: g.obligation.schedule.value)
    ug_afrr_down_schedule = sum_of(ug(UnitGroup.unit_group_type.AFRR_DOWN), lambda g: g.obligation.schedule.value)

    fcr_n_up_realised = sum_of(sys.hydro_power_systems[0].units, lambda u: compute_fcr_n_up(u))
    fcr_n_down_realised = sum_of(sys.hydro_power_systems[0].units, lambda u: compute_fcr_n_down(u))
    slack_up_realized = sum_of(sys.hydro_power_systems[0].units, lambda u: compute_prod_slack_up(u))
    slack_down_realized = sum_of(sys.hydro_power_systems[0].units, lambda u: compute_prod_slack_down(u))

    show_ts(f'fcr  + [MW]', fcr_n_up_schedule, MW)
    show_ts(f'fcr  - [MW]', fcr_n_down_schedule, MW)
    show_ts(f'afrr + [MW]', afrr_up_schedule, MW)
    show_ts(f'afrr - [MW]', afrr_down_schedule, MW)

    print("\nReserve group obligation + Unit reserve schedules")
    show_ts(f'fcr  + [MW]', ug_fcr_n_up_schedule + fcr_n_up_schedule, MW)
    show_ts(f'fcr  - [MW]', ug_fcr_n_down_schedule + fcr_n_down_schedule, MW)
    show_ts(f'afrr + [MW]', ug_afrr_up_schedule + afrr_up_schedule, MW)
    show_ts(f'afrr - [MW]', ug_afrr_down_schedule + afrr_down_schedule, MW)

    print("\nRealised reserves sum")
    show_ts('fcr_n +[MW]', fcr_n_up_realised, MW)
    show_ts('fcr_n -[MW]', fcr_n_down_realised, MW)
    show_ts('slack +[MW]', slack_up_realized, MW)
    show_ts('slack -[MW]', slack_down_realized, MW)

    print("\nTotal reserve, realised reserve -(schedule + unit-group)fulfilment view( >0 means ok)")
    show_ts('fcr_n +[MW]', fcr_n_up_realised - (ug_fcr_n_up_schedule + fcr_n_up_schedule), MW)
    show_ts('fcr_n -[MW]', fcr_n_down_realised - (ug_fcr_n_down_schedule + fcr_n_down_schedule), MW)
    show_ts('slack +[MW]', slack_up_realized - (ug_afrr_up_schedule + afrr_up_schedule), MW)
    show_ts('slack -[MW]', slack_down_realized - (ug_afrr_down_schedule + afrr_down_schedule), MW)

    print("\nSHOP reported reserve results on unit-groups")
    for ug in sys.unit_groups:
        show_ts(f'{ug.name} [MW]', ug.obligation.result.value, MW)
        # show_ts(f'{ug.name} [EUR]',ug.obligation.penalty.value,MW)

    print("\nSHOP reported reserve results pr. unit")
    for u in sys.hydro_power_systems[0].units:
        show_ts(f'fcr  + {u.name}[MW]', u.reserve.fcr_n.up.result.value, MW)
        show_ts(f'afrr + {u.name}[MW]', u.reserve.afrr.up.result.value, MW)
        show_ts(f'afrr - {u.name}[MW]', u.reserve.afrr.down.result.value, MW)
        show_ts(f'fcr  - {u.name}[MW]', u.reserve.fcr_n.down.result.value, MW)
        print("---")
        # show_ts(f'droop- {u.name} [%]',u.reserve.droop.result.value) # still empty

    print("\nDetailed: Calc reserve results/schedules based actual prod and min-max ranges")

    for u in sys.hydro_power_systems[0].units:
        show_ts(f'fcr  + {u.name}[MW]', compute_fcr_n_up(u), MW)
        show_ts('.schedule[MW]', u.reserve.fcr_n.up.schedule.value, MW)

        show_ts(f'fcr  - {u.name}[MW]', compute_fcr_n_down(u), MW)
        show_ts('.schedule[MW]', u.reserve.fcr_n.down.schedule.value, MW)

        show_ts(f'slack+ {u.name}[MW]', compute_prod_slack_up(u), MW)
        show_ts('.schedule[MW]', u.reserve.afrr.up.schedule.value, MW)

        show_ts(f'slack- {u.name}[MW]', compute_prod_slack_down(u), MW)
        show_ts('.schedule[MW]', u.reserve.afrr.down.schedule.value, MW)
        print('---')
    print("\nUnit group delivery values on unit-groups")
    for ug in sys.unit_groups:
        show_ts(f'{ug.name:12} result  [MW]', ug.delivery.result.value  , MW)
        show_ts(f'{ug.name:12} schedule[MW]', ug.delivery.schedule.value, MW)
        show_ts(f'{ug.name:12} realised[MW]', ug.delivery.realised.value, MW)


def test_shop_unit_group_with_operational_reserve():
    """ Demo of a simple system
        48 hour simulation period, hourly resolution
        first 24 hours with fixed load and reserve market schedules: SHOP finds the optimal load distribution, no market exchange(no sale/buy option allowed)
        last  24 hours with 99MW sale/buy openings, with load and reserve-group obligations, SHOP finds the most profitable solution that also fulfils(almost) the obligations

        Demonstrates
        * Building a system from scratch, pure-python
        * Running SHOP directly (you can also have the dstm service doing the same!)
        * Unit groups of FCR_N up/down AFRR up/down and unit-group membership that changes over the period
        * Unit groups associated with a market area
        * Unit reserve up/down schedules


             Reservoir ( 90..100 masl, 0..50 Mm3, 36.5 eur/MWh fixed endpoint water-value)
        f  /   |   tunnell
        l |   /|\\   penstocks
        o |  U U U   units 10..80MW
        o |  \\|/  outlet
        d \\   |
             |   downstream river

    """
    run_ta = TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 48)
    sys = create_demo_stm_system(run_ta, 1, 'sys1', n_units=3)
    run_id = 123
    opts = create_optimization_commands(run_id, write_files=False)
    shop_sys = shop.ShopSystem(run_ta)
    shop_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
    shop_sys.emit(sys)
    shop_sys.command(opts)
    shop_sys.collect(sys)
    shop_sys.complete(sys)
    present_demo_results(run_ta, sys)
    # Please note that the expected results below are just valid for the current example and shop-version
    # If we get new shop-versions, or changes internal logic, it will be captured in these asserts,
    # then we would need to do a manual validation, adjust expectation (or fix a bug)
    u_mw = [[
         66.00000019, 71.73749245, 72.08595424, 72.01778187, 71.9624272 , 71.92234579,
         71.89675007, 71.75955829, 66.02388979, 60.28458796, 55.41453508, 52.15440902,
         50.95621652, 52.12699709, 55.36387738, 60.2183905 , 65.95222044, 71.47178928,
         71.39436447, 71.32480973, 71.26828363, 71.22731883, 71.20114382, 71.18605385,
         71.25316865, 40.8895961 , 40.89024119, 40.89097906, 40.89176429, 40.89254427,
         40.89326721, 40.89389001, 50.98672134, 70.76453481, 70.69483228, 70.62697031,
         70.55998966, 70.49277905, 70.42424406, 70.35347357, 70.42970965, 23.06802051,
         23.06921867, 23.0705075 , 23.0718436 , 23.07317651, 23.0744564,  23.07564165
         #66.0, 71.7, 72.0, 72.0, 71.9, 71.9, 71.8, 71.8, 66.0, 60.3, 55.4, 52.2, 51.0, 52.1, 55.4, 60.2, 66.0, 71.4, 71.3, 71.3, 71.2, 71.2, 71.2, 71.1,
        #51.1, 40.9, 40.9, 40.9, 40.9, 40.9, 40.9, 40.9, 51.0, 70.7, 70.6, 70.6, 70.5, 70.4, 70.4, 70.3, 70.4, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1, 23.1
    ], [
         11.63356882, 11.63390507, 11.6342099 , 11.63443025, 11.63453639, 11.63451716,
         11.63438139, 11.63415646, 11.63389538, 11.63363322, 11.63342817, 11.63330644,
         11.63328734, 11.63337259, 11.63356263, 11.63383908, 11.6341776 , 11.63452088,
         11.63483252, 11.63505782, 11.63516705, 11.63514867, 11.63501146, 11.63478298,
         0.,          0.,          0.,          0.,          0.,          0.,
         0.,          0.,          0.,          0.,          0.,          0.,
         0.,          0.,          0.,          0.,          0.,          0.,
         0.,          0.,          0.,          0.,          0.,          0.
         #11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6,
        #0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    ], [
        11.6395133,  11.63984831, 15.46218156, 18.47028216, 19.60014508, 18.57033534,
        15.6468118 , 11.64009192, 11.63982968, 11.63956637, 11.63936015, 11.63923729,
        11.63921707, 11.63930124, 11.63949018, 11.63976548, 11.64010275, 11.82107355,
        16.05620986, 19.1226386 , 20.29361998, 19.2609998 , 16.30820541, 12.14660188,
        11.10057584, 11.09973374, 11.09972232, 11.09970926, 11.09969534, 11.09968151,
        11.09966869, 11.09965763, 11.09990697, 59.23475808, 59.30445306, 59.37230763,
        59.43928091, 59.50648405, 59.57501133, 59.64577377, 40.84894868, 40.83072636,
        40.83213102, 40.83364178, 40.83520773, 40.83676972, 40.83826938, 40.83965795
        #11.6, 11.6, 15.5, 18.5, 19.7, 18.6, 15.7, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.6, 11.9, 16.1, 19.2, 20.4, 19.3, 16.4, 12.2,
        #11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 11.1, 58.4, 59.4, 59.4, 59.5, 59.6, 59.6, 59.7, 40.9, 40.8, 40.8, 40.8, 40.8, 40.8, 40.8, 40.8
    ]]
    for u, prod_mw in zip(sys.hydro_power_systems[0].units, u_mw):
        assert np.allclose(prod_mw, (u.production.result.value/1e6).values, atol=1.0), "Regression values expected for the current shop-version, validate, correct or adjust if needed"
