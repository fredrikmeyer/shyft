import math
import pytest
from typing import List
import numpy as np
from shyft.energy_market.core import Point, PointList, XyPointCurve, XyPointCurveWithZ, ConnectionRole
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea
from shyft.energy_market.stm.utilities import create_t_xy, create_t_double, create_t_turbine_description, create_t_xyz_list
from shyft.time_series import time, TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case, DoubleVector, Calendar

shop = pytest.importorskip("shyft.energy_market.stm.shop")
if not shop.shyft_with_shop:
    pytest.skip('Skip shop-releated test for non-shop build', allow_module_level=True)


def show_ts(title: str, ta: TimeAxis, ts: TimeSeries, scale: float = 1.0, accumulate=False, initial_dev:float=0.0):
    s = ""
    if not isinstance(ts,TimeSeries):
        ts= ts.value if ts.exists and ts.value else None
    if ts:
        r = TimeSeries(ta, fill_value=0.0, point_fx=stair_case)
        if accumulate:
            ta_acc = TimeAxis(ta.time(0), 3600, len(ta) + 1)
            tmp = initial_dev+ts.accumulate(ta_acc).time_shift(-3600)  # just to present it better
            xx = tmp.use_time_axis_from(r)  # just to resample all values to the run-time-axis
        else:
            xx = ts.use_time_axis_from(r)  # just to resample all values to the run-time-axis

        for v in xx.values:
            if math.isfinite(v):
                s = s + f"{v*scale:5.1f} "
            else:
                s = s + "   .  "
        print(f"{title:20}:{s}")
    else:
        print(f"{title:20}: empty")


def create_optimization_commands(run_id: int, write_files: bool) -> shop.ShopCommandList:
    return shop.ShopCommandList([
        shop.ShopCommand.set_method_primal(),
        shop.ShopCommand.set_code_full(),
        shop.ShopCommand.set_universal_mip_on(),
        shop.ShopCommand.start_sim(3),
        shop.ShopCommand.set_code_incremental(),
        shop.ShopCommand.start_sim(3)
    ])


def wave_(ta: TimeAxis, offset: float, amplitude: float, n_periods: float = 3.0) -> DoubleVector:
    """ ts-generator: f(t) = offset + amplitude*sin( w*t) , w= .. """
    n = len(ta)
    return DoubleVector([offset + amplitude*math.sin(n_periods*2*3.14*i/n) for i in range(n)])


def create_const_timeseries(ta, fill_value):
    return TimeSeries(ta, fill_value=fill_value, point_fx=stair_case)


def create_timeseries(ta_points: List, ts_val: List):
    ta = TimeAxis(ta_points)
    ts = TimeSeries(ta, ts_val, stair_case)


def uid_gen():
    i = 0
    while True:
        i += 1
        yield i


def build_simple_stm_system(run_ta: TimeAxis, with_acc_constraints: bool):
    """
    This model is created with the purpose to illustrate/verify
    that constraint  on rivers,
    that imposes limitation on min and max  flow, accumulated flow
    and in this specific case, accumulated deviation up/down from a specific reference flow.
    """
    t_begin = time(int(run_ta.time_points[0]))

    # t_end = run_ta.time_points[-1]
    # dt = run_ta.time_points[1] - run_ta.time_points[0]

    mega = 1000000
    ui = uid_gen()
    stm = StmSystem(next(ui), "stm_system", "")
    # Create power market
    ma = MarketArea(next(ui), "market", '{}', stm)
    ma.price = TimeSeries(run_ta, values=wave_(run_ta, 18.6/mega, 10.0/mega, 2), point_fx=stair_case)  # constructed so that two hours have positive prod values
    ma.max_buy = create_const_timeseries(run_ta, 0*mega)
    ma.max_sale = create_const_timeseries(run_ta, 120*mega)
    # ma.load = TimeSeries(run_ta, values=wave_(run_ta, 50.0 * mega, 20 * mega), point_fx=stair_case)  # no load requirement in this case, for simplicity
    stm.market_areas.append(ma)

    # Create hydro power system
    hps = HydroPowerSystem(next(ui), "hps")

    # Reservoir
    rsv = hps.create_reservoir(next(ui), "rsv")
    volume_mapping = create_t_xy(t_begin, XyPointCurve(PointList(
        [Point(0.0*mega, 80.0), Point(20.0*mega, 90.0),
         Point(30.0*mega, 95.0), Point(50.0*mega, 100.0),
         Point(160.0*mega, 105.0)
         ])))
    rsv.volume_level_mapping = volume_mapping

    rsv.water_value.endpoint_desc = TimeSeries(run_ta, fill_value=28.5/mega, point_fx=stair_case)
    rsv.level.realised = TimeSeries(run_ta, fill_value=100.0, point_fx=stair_case)
    rsv.inflow.schedule = TimeSeries(run_ta, values=wave_(run_ta, 50.0, 15.0, 0.5), point_fx=stair_case)*0.0  # set to 0.0 for simplicity illustration purposes

    # Plant
    # with indicated outlet level 10 masl, gives 70..90 masl net head for the units.
    plant = hps.create_power_plant(next(ui), 'plant')
    plant.outlet_level = create_t_double(t_begin, 10.0)
    plant.mip= TimeSeries(run_ta, fill_value=1.0, point_fx=stair_case)
    # Unit
    unit = hps.create_unit(next(ui), "unit")
    plant.add_unit(unit)
    # generator and turbine-description (efficiency and operating range)
    ge = 90.0  # if set to 80, there is a corresponding drop in efficiency
    unit.generator_description = create_t_xy(t_begin,
                                             XyPointCurve(PointList(
                                                 [Point(10.0*mega, ge + 6.0), Point(20.0*mega, ge + 7.2),
                                                  Point(40.0*mega, ge + 8.0), Point(60.0*mega, ge + 9.0),
                                                  Point(80.0*mega, ge + 8.0)])))
    unit.turbine_description = create_t_turbine_description(t_begin,
                                                            [XyPointCurveWithZ(
                                                                XyPointCurve([Point(10.0, 60.0), Point(20.0, 70.0),
                                                                              Point(40.0, 85.0), Point(60.0, 92.0),
                                                                              Point(80.0, 94.0), Point(100.0, 92.0),
                                                                              Point(110.0, 90.0)]),
                                                                70.0
                                                            ), XyPointCurveWithZ(
                                                                XyPointCurve([Point(8.0, 60.0), Point(18.0, 70.0),
                                                                              Point(40.0, 86.0), Point(60.0, 93.0),
                                                                              Point(80.0, 95.0), Point(100.0, 93.0),
                                                                              Point(110.0, 89.0)]),
                                                                110.0
                                                            )]

                                                            )

    # Waterways

    ## Tunnels
    inlet = hps.create_tunnel(next(ui), "tunnel")
    penstock = hps.create_tunnel(next(ui), "penstock")
    tailrace = hps.create_tunnel(next(ui), "tailrace")

    ### loss coeffecients in tunnels (it marks them as tunnels as well)
    inlet.head_loss_coeff = create_t_double(t_begin, 0.000030)
    penstock.head_loss_coeff = create_t_double(t_begin, 0.00005)
    tailrace.head_loss_coeff = create_t_double(t_begin, 0.00001)

    ## Rivers
    bypass = hps.create_river(next(ui), "bypass")
    flood = hps.create_river(next(ui), "flood")
    river = hps.create_river(next(ui), "river")

    # Set gate and river constraints to trigger discharge group
    gate = bypass.add_gate(next(ui), "bypass_gate", "")
    # bypass.head_loss_coeff = create_t_double(t_begin, 0.00001)  # makes it a tunnel, by def.
    # gate.discharge.schedule = create_const_timeseries(run_ta, 0.0) # stop shop from using the bypass
    # bypass.static_max = create_t_double(t_begin,123.0)

    flood_gate = flood.add_gate(next(ui), "flood_gate", "")
    flood_gate.flow_description = create_t_xyz_list(t_begin, XyPointCurve([Point(100.0, 0.0), Point(105.0, 250.0)]), 0)

    ################ NOTE ####################3
    # Setting discharge reference and contraint.accumulated max, triggers shop_api to add a discharge group
    # for the powerstation and bypass gate upstream. C++ does an iterative search upstreams from river (in this example)
    # until it finds the gate in the bypass, and the powerstation owning the unit. Then adds them to the same discharge group.

    if with_acc_constraints:
        n_historical_steps=3  # we add 3 hours of history before the run timeaxis to provide the history, and intial dev for the accumulator.
        ref_ta = TimeAxis(run_ta.time(0)-n_historical_steps*Calendar.HOUR, Calendar.HOUR, len(run_ta) + n_historical_steps) # one extra timestep for .accumulate(ta)
        hist_ta = TimeAxis(run_ta.time(0)-n_historical_steps*Calendar.HOUR,Calendar.HOUR,n_historical_steps)
        ref_m3s=TimeSeries(
            ref_ta,
            [                                                                                                          # force volume that requires bypass here
            50.0, 50.0,  50.0, # <-- the historical ref                                                                       ++++++++++++
            50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0,50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 50.0, 240.0, 210.0, 30.0, 30.0, 50.0
            ],
            point_fx=stair_case
        )
        river.discharge.realised = TimeSeries(
            hist_ta,
            [                                                                                                          # force volume that requires bypass here
            50.0, 50.0,  49.0  # <-- the historical realised data that along with the historical part of ref accumulates to the intial-deviation calc for shop.
            ],
            point_fx=stair_case
        )

        river.discharge.reference = ref_m3s #*1.0
        if True:
            nan=float('nan')
            unit.production.schedule = mega*TimeSeries(run_ta,  # notice that with nan, the optimizer determines what the optimal plan is
                                [0.0, 100.0, 101.0, 102.0, nan, nan, nan, nan, nan, 99.0, nan,nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan],
                                point_fx=stair_case) # make unit schedule for some period



            acc_dev = mega*TimeSeries(run_ta,
                                      [
                                          1.0,1.0,1.0,1.0,1.0,1.0,
                                          1.0,1.0,0.0,1.0,1.0,1.0,  # notice! the 0.0 acc reference here at the 9th timestep, forcing opt to ensure its on route at this timestep.
                                          1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
                                          nan, 1.0, 1.0, 1.0, 1.0, 0.0, # notice !the nan, seems to reset acc-dev counter to 0., and the last 0.0? it forces opt to reach 0.

                                      ],stair_case)
            river.discharge.constraint.accumulated_min = acc_dev #(refx*0.9999).accumulate(acc_ta)*0.0#.time_shift(-3600.0)
            river.discharge.constraint.accumulated_max = 1.0*acc_dev # (refx*1.001).accumulate(acc_ta)#.time_shift(-3600.0) # float('nan')

    # its possible to specify penalty cost for violating acc up/down like this:
    #river.discharge.penalty.cost.accumulated_min = create_const_timeseries(run_ta, 100.0/mega)
    #river.discharge.penalty.cost.accumulated_max = create_const_timeseries(run_ta, 100.0/mega)

    # Connect hps components
    #
    #  Flood
    #  |                             power_station
    #  |                                  ||
    #  rsv = inlet(wtr) = penstock(wtr) = unit = tailrace(wtr)
    #  |                                                   |
    #  |                                                   | river(wtr)
    #  bypass(wtr) = = = = = = = = = = = = = = = = = = = = |

    inlet.input_from(rsv, ConnectionRole.main).output_to(penstock)
    unit.input_from(penstock).output_to(tailrace)
    river.input_from(tailrace)
    bypass.input_from(rsv, ConnectionRole.bypass).output_to(river)
    flood.input_from(rsv, ConnectionRole.flood).output_to(river)
    stm.hydro_power_systems.append(hps)

    return stm


def test_discharge_group():
    cal = Calendar("Europe/Oslo")
    t_start = cal.time(2021, 1, 1)
    t_end = cal.time(2021, 1, 2)
    dt = cal.HOUR

    n = int((t_end - t_start)//dt)
    time_axis_sim = TimeAxis(t_start, dt, n)
    with_acc_constraints = True  # flip to false to check what the free optimization will give
    stm = build_simple_stm_system(run_ta=time_axis_sim, with_acc_constraints=with_acc_constraints)

    run_id = 123
    opts = create_optimization_commands(run_id, write_files=False)
    shop_sys = shop.ShopSystem(time_axis_sim)
    shop_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
    shop_sys.emit(stm)
    shop_sys.command(opts)
    shop_sys.collect(stm)
    shop_sys.complete(stm)

    hps = stm.hydro_power_systems[0]
    rsv = hps.reservoirs[0]
    bypass = rsv.downstreams[1].target
    river = bypass.downstreams[0].target
    flood = rsv.downstreams[2].target
    unit = hps.units[0]
    print()
    show_ts(title="rsv.volume                   ", ta=time_axis_sim, ts=rsv.volume.result, scale=1/1e6)
    show_ts(title="rsv.level                    ", ta=time_axis_sim, ts=rsv.level.result, scale=1)
    show_ts(title="rsv.inflow   m3/s            ", ta=time_axis_sim, ts=rsv.inflow.schedule, scale=1)
    show_ts(title="price                        ", ta=time_axis_sim, ts=stm.market_areas[0].price, scale=1.e6)
    show_ts(title="unit.production schedul MW   ", ta=time_axis_sim, ts=unit.production.schedule, scale=1/1e6)
    show_ts(title="unit.production MW           ", ta=time_axis_sim, ts=unit.production.result, scale=1/1e6)
    show_ts(title="unit.discharge               ", ta=time_axis_sim, ts=unit.discharge.result, scale=1)

    show_ts(title="flood.discharge              ", ta=time_axis_sim, ts=flood.discharge.result, scale=1)
    show_ts(title="flood.discharge   acc K flux ", ta=time_axis_sim, ts=flood.discharge.result, scale=1/1e3, accumulate=True)
    show_ts(title="bypass.discharge             ", ta=time_axis_sim, ts=bypass.discharge.result, scale=1)
    show_ts(title="bypass.discharge acc Mm3     ", ta=time_axis_sim, ts=bypass.discharge.result, scale=1/1e6, accumulate=True)
    show_ts(title="river.act                    ", ta=time_axis_sim, ts=river.discharge.result, scale=1)
    show_ts(title="river.ref                    ", ta=time_axis_sim, ts=river.discharge.reference, scale=1)

    show_ts(title="river.discharge.acc.up.   Mm3", ta=time_axis_sim, ts=river.discharge.constraint.accumulated_max, scale=1/1e6)
    show_ts(title="river.discharge   acc Mm3    ", ta=time_axis_sim, ts=(river.discharge.result.value-river.discharge.reference.value), scale=1/1e6, accumulate=True, initial_dev=-3600.0)
    show_ts(title="river.discharge.acc.down. Mm3", ta=time_axis_sim, ts=river.discharge.constraint.accumulated_min, scale=1/1e6)
    show_ts(title="river.discharge  pen      max", ta=time_axis_sim, ts=river.discharge.penalty.result.accumulated_max, scale=1)
    show_ts(title="river.discharge  pen      min", ta=time_axis_sim, ts=river.discharge.penalty.result.accumulated_min, scale=1)
    if stm.summary:
        print(f"\n {stm.summary}")
    # note: these values was given using shop 14.4.1.1, if deviations, check for reasonable reasons, and upd.
    if with_acc_constraints:
        expected_river_discharge = np.array(
             [0.0, 131.08627, 132.61641, 134.15450, 53.14282, 0.00000, 0.00000, 0.00000, 0.00000, 130.33510, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 80.00000, 0.00000, 0.00000, 0.00000, 102.37462, 102.53994, 102.70591, 102.87253, 150.50700]
            #[0.0, 131.2, 132.7, 134.3,  52.8,   0.0,   0.0,   0.0,   0.0, 130.4,   0.0,   0.0,   0.0,   0.0,   0.0,  80.0,   0.0,   0.0,   0.0, 102.5, 102.6, 102.8, 103.0, 150.1]
        )
        assert np.allclose(expected_river_discharge, river.discharge.result.value.values.to_numpy(), atol=0.2)
    # switch to simulation mode to verify sim only
    # to avoid intermixing result, create a completely new system, same inputs, but use the plan results from the above
    sim_stm = build_simple_stm_system(run_ta=time_axis_sim, with_acc_constraints=False) # skip river etc. constraints.
    sim_hps = sim_stm.hydro_power_systems[0]
    sim_rsv = sim_hps.reservoirs[0]
    sim_bypass = sim_rsv.downstreams[1].target
    sim_bypass.gates[0].discharge.schedule = bypass.discharge.result
    sim_river = sim_bypass.downstreams[0].target
    sim_flood = sim_rsv.downstreams[2].target
    sim_unit = sim_hps.units[0]
    sim_unit.production.schedule = unit.production.result
    sim_sys = shop.ShopSystem(time_axis_sim)
    sim_sys.set_logging_to_stdstreams(False)  # Set this to true if you want to have a look at shop-printouts during optimization
    sim_sys.emit(sim_stm)
    sim_cmds= shop.ShopCommandList([
        shop.ShopCommand.start_shopsim()
    ])
    sim_sys.command(sim_cmds)
    sim_sys.collect(sim_stm)
    sim_sys.complete(sim_stm)
    print("Sim results, with the unit/gate plans from above as input")
    show_ts(title="rsv.volume                   ", ta=time_axis_sim, ts=sim_rsv.volume.result, scale=1/1e6)
    show_ts(title="rsv.level                    ", ta=time_axis_sim, ts=sim_rsv.level.result, scale=1)
    show_ts(title="rsv.inflow   m3/s            ", ta=time_axis_sim, ts=sim_rsv.inflow.schedule, scale=1)
    show_ts(title="unit.production MW           ", ta=time_axis_sim, ts=sim_unit.production.result, scale=1/1e6)
    show_ts(title="unit.discharge               ", ta=time_axis_sim, ts=sim_unit.discharge.result, scale=1)
    show_ts(title="unit.eff_head m              ", ta=time_axis_sim, ts=sim_unit.effective_head, scale=1)
    show_ts(title="flood.discharge              ", ta=time_axis_sim, ts=sim_flood.discharge.result, scale=1)
    show_ts(title="bypass.discharge             ", ta=time_axis_sim, ts=sim_bypass.discharge.result, scale=1)
    prod_diff = np.abs( (sim_unit.production.result.value - unit.production.result.value).values.to_numpy().sum()/1e6)  #in MW units
    bypass_diff = np.abs((sim_bypass.discharge.result.value - bypass.discharge.result.value).values.to_numpy().sum())
    bypass_diff_except_last = np.abs((sim_bypass.discharge.result.value - bypass.discharge.result.value).values.to_numpy()[:-1].sum())
    print(f" prod diffs are {prod_diff} MW")
    print(f" bypass flow diffs are {prod_diff} m3/s")
    pp = sim_unit.power_plant
    sim_unit.production.realised.value = sim_unit.production.result.value  # assign the .realised from .result as a historical fact, for ease
    pp.production.realised.value = 0.0 + sim_unit.production.realised.value  # just arrange a proxy for the sum, one unit in this case, and note that we have no realised value for unit.
    assert (sim_unit.discharge.realised.value - sim_unit.discharge.result.value).values.to_numpy().mean() < 1e-4, "Verify that unit.discharge.realised attribute is filled in after sim run "
    assert (pp.discharge.realised.value - sim_unit.discharge.result.value).values.to_numpy().mean() < 1e-4, "Verify that power plant .discharge.realised attribute is filled in after sim run "
    assert pp.production.realised.value == sim_unit.production.realised.value  # math equality between ts works, but we must use .value to get through the wrapped proxy to the TimeSeries class
    assert -0.1 < prod_diff < 0.1
    assert -0.1 < bypass_diff < 48.0  # NOTE: Bug in shop?? The flow through the gate in last timestep is different, 0.0 in sim vs 47.1 in opt as of Shop 14.3.2.0
    assert -0.1 < bypass_diff_except_last < 0.1  # Additional test due to above note!
