import pytest
from functools import reduce
from shyft.energy_market.stm import StmSystem, PowerModule, Busbar, Network

def test_power_module_attributes():
    sys = StmSystem(1, "A", "{}")
    pm = PowerModule(2, 'PowerModule #1', '{}', sys)
    # generics, from id_base
    assert hasattr(pm, "id")
    assert hasattr(pm, "name")
    assert hasattr(pm, "json")
    # specifics
    assert hasattr(pm, "power")
    assert hasattr(pm, "connected")
    assert pm.tag == "/P2"

def test_power_module_attributes_flattened():
    sys = StmSystem(1, "A", "{}")
    pm = PowerModule(2, 'PowerModule #1', '{}', sys)
    assert pm.flattened_attributes()
    for (path, val) in pm.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), pm)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()

def test_busbar_association():
    sys = StmSystem(1, "A", "{}")
    pm = PowerModule(2, 'PowerModule #1', '{}', sys)
    sys.power_modules.append(pm)
    n = Network(3, 'Network', '{}', sys)
    sys.networks.append(n)
    # Associate busbar
    b = Busbar(4, 'Busbar #1', '{}', n)
    n.busbars.append(b)
    b.add_to_power_module(pm);
    pms = b.get_power_modules();
    assert len(pms) == 1
    assert pms[0].id == 2

def test_power_module_equal():
    sys = StmSystem(1, "A", "{}")
    a = PowerModule(3, 'PowerModule #1', '{}', sys)
    b = PowerModule(3, 'PowerModule #1', '{}', sys)
    c = PowerModule(4, 'hmm', '{}', sys)
    sys.power_modules.extend([a, b, c])
    assert len(sys.power_modules) == 3
    assert a == a
    assert a == b
    assert a != c
    # Check equality with same associated busbar
    n = Network(3, 'Network', '{}', sys)
    sys.networks.append(n)
    bb = Busbar(4, 'Busbar #1', '{}', n)
    n.busbars.append(bb)
    bb.add_to_power_module(a)
    bb.add_to_power_module(b)
    assert a == b
