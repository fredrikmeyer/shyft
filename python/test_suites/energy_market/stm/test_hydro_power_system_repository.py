import pytest
from pathlib import Path
from .models import create_test_hydro_power_system, create_test_hydro_power_system_for_regression_old_data_test
from shyft.energy_market.stm.model_repository import HydroPowerSystemRepository

from shyft.time_series import utctime_now



def test_create_read_delete(tmpdir):
    """
    Run a test-story where we use all the functions of the repository
    :return:
    """
    mdir = (tmpdir/"hps_crud")
    mdir.mkdir()
    # first check that zero models give zero back
    ms = HydroPowerSystemRepository(Path(mdir))
    model_list = ms.get_model_infos()
    assert model_list is not None
    assert len(model_list) == 0

    # create one model and verify we get it listed
    m = create_test_hydro_power_system(hps_id=1, name='ulla-forre')
    m_id = ms.save_model(m)
    assert m_id > 0
    model_list = ms.get_model_infos()
    assert len(model_list) == 1
    mi = model_list[0]
    assert mi.id == m.id
    assert mi.name == m.name
    assert abs(int(utctime_now()) - mi.created) < 100
    m2 = create_test_hydro_power_system(hps_id=2, name='ulla-forre')
    m_id2 = ms.save_model(m2)  # save another model
    assert m_id2 != m_id  # verify it's different id
    model_list = ms.get_model_infos()
    assert len(model_list) == 2, 'should have two models by now'
    # auto create id, if we pass 0
    m2.id = 0
    m_id3 = ms.save_model(m2)
    assert m_id3 == 3, 'expect 3 here'
    ms.delete_model(m_id3)
    ms.delete_model(m_id2)
    # now read the model and compare with original
    mr = ms.get_model(m_id)
    assert mr.id == mi.id
    assert mr.name == mi.name
    mr_list = ms.get_models([m_id])  # also test reading by list work
    assert len(mr_list) == 1

    # then remove the model and verify it's away
    ms.delete_model(m_id)
    model_list = ms.get_model_infos()
    assert len(model_list) == 0


def test_create_model_service(tmpdir):
    mdirx = (tmpdir/"hps_crudx")
    mdirx.mkdir()
    mdir = Path(mdirx)
    ms = HydroPowerSystemRepository(mdir)  # works on existing directory
    with pytest.raises(RuntimeError):
        x = HydroPowerSystemRepository(Path(r'dir_does_not_exist'))  # fail on non-existing
    with pytest.raises(RuntimeError):
        x = HydroPowerSystemRepository(Path(__file__))  # file should also fail
