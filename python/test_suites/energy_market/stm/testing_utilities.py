import pytest
from itertools import count

from shyft.energy_market.stm import HydroPowerSystem


@pytest.fixture(name='hps_factory')
def fixture_hps_factory():
    counter = count(1)

    def factory(id=None, name=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        return HydroPowerSystem(id, name)

    return factory


@pytest.fixture(name='reservoir_factory')
def fixture_reservoir_factory(hps_factory):
    counter = count(1)

    def factory(id=None, name=None, hps=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        hps = hps or hps_factory()

        return hps.create_reservoir(id, name)

    return factory


@pytest.fixture(name='reservoir_aggregate_factory')
def fixture_reservoir_factory(hps_factory):
    counter = count(1)

    def factory(id=None, name=None, hps=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        hps = hps or hps_factory()

        return hps.create_reservoir_aggregate(id, name)

    return factory


@pytest.fixture(name='unit_factory')
def fixture_unit_factory(hps_factory):
    counter = count(1)

    def factory(id=None, name=None, hps=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        hps = hps or hps_factory()

        return hps.create_unit(id, name)

    return factory


@pytest.fixture(name='waterway_factory')
def fixture_waterway_factory(hps_factory):
    counter = count(1)

    def factory(id=None, name=None, hps=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        hps = hps or hps_factory()

        return hps.create_waterway(id, name)

    return factory


@pytest.fixture(name='power_plant_factory')
def fixture_power_plant_factory(hps_factory):
    counter = count(1)

    def factory(id=None, name=None, hps=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        hps = hps or hps_factory()

        return hps.create_power_plant(id, name)

    return factory


@pytest.fixture(name='gate_factory')
def fixture_gate_factory(hps_factory, waterway_factory):
    counter = count(1)

    def factory(id=None, name=None, hps=None, waterway=None):
        id = id or next(counter)
        name = name or f'test_hps_{id}'

        if waterway is None:
            hps = hps or hps_factory()
            waterway = waterway_factory(hps=hps)

        elif hps is None:
            raise ValueError("Only one of 'hps' and 'waterway' can be provided")

        return waterway.add_gate(id, name)

    return factory





