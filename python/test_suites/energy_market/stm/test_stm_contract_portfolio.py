from shyft.energy_market.stm import StmSystem, Contract, ContractPortfolio
from shyft.time_series import TimeSeries, shyft_url


def test_contract_portolio_ts():
    sys = StmSystem(1, "A", "{}")
    a = ContractPortfolio(2, 'Portfolio A', '{}', sys)
    sys.contract_portfolios.extend([a])
    assert len(a.ts) == 0
    a.ts['price_eur'] = TimeSeries(shyft_url('portfolios', 'a/price_eur'))  # we can assign references to time-series, later to be bound
    a.ts['price_nok'] = 10.5*TimeSeries(a.get_tsm_object('price_eur').url('dstm://Mmdl1'))  # also possible to adress the object model it self
    expr = a.ts['price_nok'].stringify()
    assert len(expr)


def test_contract_portfolio_market_association():
    sys = StmSystem(1, "A", "{}")
    a = ContractPortfolio(2, 'Portfolio A', '{}', sys)
    b = ContractPortfolio(2, 'Portfolio A', '{}', sys)
    c = ContractPortfolio(3, 'Portfolio C', '{}', sys)
    assert a == b
    assert a != c
    sys.contract_portfolios.extend([a, b, c])
    c1 = Contract(3, 'Contract #1', '{}', sys)
    c2 = Contract(3, 'Contract #1', '{}', sys)
    sys.contracts.extend([c1, c2])
    c1.add_to_portfolio(a)
    assert a != b
    assert c1 == c2
    # c2.add_to_portfolio(b), TODO: does not work, because of expose vector<shared_ptr<T>> needs specialisation on compare
    c1.add_to_portfolio(b)
    assert a == b  # works because a and b portfoilos have the exact same object, ref comment above.
