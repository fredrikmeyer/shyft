import pytest
from functools import reduce
from shyft.energy_market.stm import StmSystem, Busbar, Network, TransmissionLine

def test_network_attributes():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    sys.networks.append(n)
    # generics, from id_base
    assert hasattr(n, "id")
    assert hasattr(n, "name")
    assert hasattr(n, "json")
    # specifics
    assert hasattr(n, "busbars")
    assert hasattr(n, "transmission_lines")
    assert n.tag == "/n2"

def test_network_equal():
    sys = StmSystem(1, "A", "{}")
    n = Network(2, 'Network', '{}', sys)
    n_2 = Network(2, 'Network', '{}', sys)
    sys.networks.extend([n, n_2])
    assert n == n_2
    # Compare added transmission lines
    t = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    t_2 = TransmissionLine(3, 'TransmissionLine #1', '{}', n)
    n.transmission_lines.append(t)
    assert n != n_2
    n_2.transmission_lines.append(t_2)
    assert n == n_2
    # Compare added busbars
    b = Busbar(4, 'Busbar #1', '{}', n)
    n.busbars.append(b)
    assert n != n_2
    b_2 = Busbar(4, 'Busbar #1', '{}', n)
    n_2.busbars.append(b_2)
    assert n == n_2
