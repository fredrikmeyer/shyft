import pytest
from tempfile import TemporaryDirectory
from typing import Dict, Optional

websockets = pytest.importorskip("websockets")  # Required for tests in this module

import asyncio
from shyft.energy_market import stm


def uri(port_num):
    return f"ws://127.0.0.1:{port_num}"


def get_response(req: str, port_no: int, auth: str = ""):
    async def wrap(ws_req: str, x_hdr: Optional[Dict[str,str]]):
        async with websockets.connect(uri(port_no), extra_headers=x_hdr) as websocket:
            await websocket.send(ws_req)
            response = await websocket.recv()
            return response
    extra_header = {"Authorization": auth} if auth else None
    return asyncio.get_event_loop().run_until_complete(wrap(req, extra_header))


def test_web_api(simple_stm_system, port_no, web_api_port):
    assert port_no != web_api_port
    import time
    #  ensure there is event loop for this thread
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    srv = stm.DStmServer()
    srv.set_listening_port(port_no)
    with TemporaryDirectory() as doc_root:
        srv.start_server()
        srv.do_add_model("simple", simple_stm_system)
        host = "127.0.0.1"
        srv.start_web_api(f"{host}", web_api_port, doc_root, 1, 1)
        time.sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        qa_tuples = [("""get_model_infos {"request_id": "1"}""",
                      """{"request_id":"1","result":[{"model_key":"simple","id":1,"name":"Test STM system"}]}"""),
                     ("""get_hydro_components {
                                    "request_id": "2",
                                    "model_key": "simple",
                                    "hps_id": 1
                                  }""",
                      """{"request_id":"2","result":{"model_key":"simple","hps_id":1,"reservoirs":[{"id":1,"name":"simple_res"}],"units":[{"id":1,"name":"simple_unit"}],"power_plants":[{"id":2,"name":"simple_pp","units":[1]}],"waterways":[{"id":1,"name":"r->u","upstreams":[{"role":"input","target":"R1"}],"downstreams":[{"role":"main","target":"A1"}]}]}}"""),
                     ("""read_model {"request_id": "1", "model_key": "simple"}""",
                      """{"request_id":"1","result":{"model_key":"simple","id":1,"name":"Test STM system","json":"","hps":[{"id":1,"name":"simple hps"}],"unit_groups":[],"markets":[],"contracts":[],"contract_portfolios":[],"networks":[],"power_modules":[]}}""")]
        try:
            for qa in qa_tuples:
                response = get_response(qa[0], web_api_port)
                assert response == qa[1], 'expected result'
        finally:
            srv.stop_web_api()
            srv.close()


def test_stm_task_update_web_api(port_no, web_api_port):
    assert port_no != web_api_port
    from time import sleep
    with TemporaryDirectory() as tmp:
        srv = stm.StmTaskServer(tmp)
        #  to verify that interface can be setup with auth
        token = "Basic dXNlcjpwd2Q="
        srv.add_auth_tokens([token])  # echo -n 'user:pwd' | base64
        print("Starting webapi")
        socket_port = srv.start_server()
        srv.start_web_api("127.0.0.1",
                          web_api_port,
                          doc_root=tmp,
                          fg_threads=1,
                          bg_threads=1)

        sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        # First store a model
        client = stm.StmTaskClient(f"127.0.0.1:{socket_port}", 9999)
        store_req = """store_model {"request_id": "example-store", 
        "model": {"id": 4, "name": "new stm run","labels": ["example", "wiki"]}}"""
        print("Store model")
        a = get_response(store_req, web_api_port, token)
        print(f"a={a}")
        mdl = client.read_model(4)
        print(mdl.id)

        print("add_case")
        add_case_req = """add_case {"request_id": "add_case",
        "mid": 4, "case": {"id": 2, "name": "added_case", "created": 0, "json": "", "labels": ["test"], "model_refs": [] } }"""
        a = get_response(add_case_req, web_api_port, token)
        print(f"a={a}")
        mdl = client.read_model(4)
        print(mdl.id)
        print(mdl.cases[0].name)

        print()
        print("update_case")
        update_case_req = """update_case {"request_id": "update_case",
        "mid": 4, "case": {"id": 2, "name": "updated_case", "created": 0, "json": "{'spam': 'egg'}", "labels": ["development"], "model_refs": [] } }"""
        a = get_response(update_case_req, web_api_port, token)
        print(f"a={a}")
        sleep(0.5)  # todo: the start web api call is not exactly ready on return, so we need a sleep here.
        mdl = client.read_model(4)
        print(mdl.id)
        print(mdl.cases[0].name)

        # SOMETHING FISHY with ports/mdl.id combo, try different mids

        mdl_update = client.read_model(4)
        assert len(mdl.cases) == 1
        assert mdl_update.cases[0].id == 2  # Id must be identical when updating case
        assert mdl_update.cases[0].name == "updated_case"  # Verify we have updated
        assert mdl_update.cases[0].json == """{'spam': 'egg'}"""  # Verify we have updated
        srv.stop_web_api()


def test_stm_auth():
    with TemporaryDirectory() as tmp:
        s = stm.StmTaskServer(tmp)
        assert not s.auth_needed
        assert len(s.auth_tokens()) == 0
        auth_tokens = ["Basic base64", "Bearer any", "A"]
        s.add_auth_tokens(auth_tokens)
        assert s.auth_needed
        assert len(s.auth_tokens()) == len(auth_tokens)
        assert set(auth_tokens) == set(s.auth_tokens())
        s.remove_auth_tokens(["A", "Bearer any"])
        assert len(s.auth_tokens()) == 1
        assert s.auth_tokens()[0] == "Basic base64"
        assert s.auth_needed
        s.remove_auth_tokens(s.auth_tokens())
        assert not s.auth_needed
