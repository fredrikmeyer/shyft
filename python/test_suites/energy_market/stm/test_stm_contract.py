import pytest
from functools import reduce
from shyft.energy_market.stm import HydroPowerSystem, StmSystem, MarketArea, Contract, ContractPortfolio


def test_contract_attributes():
    a = StmSystem(1, "A", "{}")
    # ma = MarketArea(2, 'NO1', '{}', a)
    ca = Contract(3, 'Contract #1', '{}', a)
    # generics, from id_base
    assert hasattr(ca, "id")
    assert hasattr(ca, "name")
    assert hasattr(ca, "json")
    # specifics
    assert hasattr(ca, "quantity")
    assert hasattr(ca, "price")
    assert hasattr(ca, "fee")
    assert hasattr(ca, "revenue")
    assert hasattr(ca, "parent_id")
    assert hasattr(ca, "buyer")
    assert hasattr(ca, "seller")
    assert hasattr(ca, "active")
    assert hasattr(ca, "validation")
    assert ca.tag == "/c3"


def test_contract_attributes_flattened():
    a = StmSystem(1, "A", "{}")
    # ma = MarketArea(2, 'NO1', '{}', a)
    ca = Contract(3, 'Contract #1', '{}', a)
    assert ca.flattened_attributes()
    for (path, val) in ca.flattened_attributes().items():
        attr = reduce(getattr, path.split('.'), ca)
        assert type(val) is type(attr)
        assert val == attr
        assert val.url() == attr.url()


def test_contract_market_association():
    sys = StmSystem(1, "A", "{}")
    cpf = ContractPortfolio(2, 'Portfolio A', '{}', sys)
    ctr = Contract(3, 'Contract #1', '{}', sys)
    assert len(sys.contract_portfolios) == 0
    assert len(sys.contracts) == 0
    assert len(cpf.contracts) == 0
    assert len(ctr.get_portfolios()) == 0
    sys.contract_portfolios.append(cpf)
    sys.contracts.append(ctr)
    cpf.contracts.append(ctr)
    assert len(sys.contracts) == 1
    assert len(sys.contract_portfolios) == 1
    assert len(cpf.contracts) == 1
    assert len(ctr.get_portfolios()) == 1
    del cpf.contracts[0]
    assert len(cpf.contracts) == 0
    assert len(ctr.get_portfolios()) == 0
    ctr.add_to_market_area(cpf)
    assert len(cpf.contracts) == 1
    assert len(ctr.get_portfolios()) == 1


def test_contract_market_association():
    sys = StmSystem(1, "A", "{}")
    mkt = MarketArea(2, 'NO1', '{}', sys)
    ctr = Contract(3, 'Contract #1', '{}', sys)
    assert hasattr(ctr, 'ts')
    assert len(sys.market_areas) == 0
    assert len(sys.contracts) == 0
    assert len(mkt.contracts) == 0
    assert len(ctr.get_market_areas()) == 0
    sys.market_areas.append(mkt)
    sys.contracts.append(ctr)
    mkt.contracts.append(ctr)
    assert len(sys.contracts) == 1
    assert len(sys.market_areas) == 1
    assert len(mkt.contracts) == 1
    assert len(ctr.get_market_areas()) == 1
    del mkt.contracts[0]
    assert len(mkt.contracts) == 0
    assert len(ctr.get_market_areas()) == 0
    ctr.add_to_market_area(mkt)
    assert len(mkt.contracts) == 1
    assert len(ctr.get_market_areas()) == 1


def test_contract_power_plant_association():
    sys = StmSystem(1, "A", "{}")
    hps = HydroPowerSystem(2, ' test hps')
    plt = hps.create_power_plant(3, 'test plant')
    sys.hydro_power_systems.append(hps)
    ctr = Contract(5, 'Contract #1', '{}', sys)
    assert len(ctr.power_plants) == 0
    ctr.power_plants.append(plt)
    assert len(ctr.power_plants) == 1


def test_contract_equal():
    sys = StmSystem(1, "A", "{}")
    a = Contract(3, 'Contract #1', '{}', sys)
    b = Contract(3, 'Contract #1', '{}', sys)
    c = Contract(4, 'hmm', '{}', sys)
    sys.contracts.extend([a, b, c])
    assert len(sys.contracts) == 3
    assert a == a
    assert a == b
    assert a != c
    a.name = 'Another name'
    assert a != b
