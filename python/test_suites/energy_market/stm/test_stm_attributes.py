import pytest
from functools import reduce

from shyft.energy_market.stm import (Reservoir, HydroPowerSystem, StmSystem, t_xy, StmSystemContext, ModelState,
                                     MessageList, UnitGroup, MarketArea)
from shyft.energy_market.core import Point, XyPointCurve, XyPointCurveWithZ, PointList, StringTimeSeriesDict
from shyft.energy_market.stm.utilities import create_t_turbine_description
from shyft.time_series import (TimeSeries, TimeAxis, time, point_interpretation_policy as pfx)
from .models import create_test_hydro_power_system

# noinspection PyUnresolvedReferences
from test_suites.energy_market.stm.testing_utilities import (
    fixture_reservoir_factory, fixture_unit_factory, fixture_waterway_factory,
    fixture_gate_factory, fixture_power_plant_factory, fixture_hps_factory,
)


def test_reservoir_attributes():
    hps = HydroPowerSystem(1, "Reservoir hps")
    res = hps.create_reservoir(2, "Reservoir one", "")
    assert hasattr(res, "volume_level_mapping")
    assert hasattr(res.water_value, "endpoint_desc")

    assert hasattr(res, "level")
    assert hasattr(res.level, "regulation_min")
    assert hasattr(res.level, "regulation_max")
    assert hasattr(res.level, "realised")
    assert hasattr(res.level, "schedule")
    assert hasattr(res.level, "constraint")
    assert hasattr(res.level, "result")
    assert hasattr(res.level.constraint, "min")
    assert hasattr(res.level.constraint, "max")

    assert hasattr(res, "volume")
    assert hasattr(res.volume, "static_max")
    assert hasattr(res.volume, "result")
    assert hasattr(res.volume, "schedule")
    assert hasattr(res.volume, "constraint")
    assert hasattr(res.volume.constraint, "min")
    assert hasattr(res.volume.constraint, "max")
    assert hasattr(res.volume.constraint, "tactical")
    assert hasattr(res.volume.constraint.tactical, "min")
    assert hasattr(res.volume.constraint.tactical, "max")
    assert hasattr(res.volume, "slack")
    assert hasattr(res.volume.slack, "lower")
    assert hasattr(res.volume.slack, "upper")

    assert hasattr(res, "inflow")
    assert hasattr(res.inflow, "schedule")
    assert hasattr(res.inflow, "realised")
    assert hasattr(res.inflow, "result")

    assert hasattr(res, "ramping")
    assert hasattr(res.ramping, "level_up")
    assert hasattr(res.ramping, "level_down")

    assert hasattr(res, "tag")
    assert res.tag == "/H1/R2"


def test_reservoir_aggregate_attributes():
    hps = HydroPowerSystem(1, "Reservoir Aggregate hps")
    ra = hps.create_reservoir_aggregate(2, "Reservoir Aggregate one", "")

    assert hasattr(ra, "id")
    assert hasattr(ra, "name")
    assert hasattr(ra, "json")
    assert hasattr(ra, "reservoirs")
    assert hasattr(ra, "add_reservoir")
    assert hasattr(ra, "remove_reservoir")
    assert hasattr(ra.inflow, "schedule")
    assert hasattr(ra.inflow, "realised")
    assert hasattr(ra.inflow, "result")
    assert hasattr(ra, "volume")
    assert hasattr(ra.volume, "static_max")
    assert hasattr(ra.volume, "schedule")
    assert hasattr(ra.volume, "realised")
    assert hasattr(ra.volume, "result")

    assert hasattr(ra, "tag")
    assert ra.tag == "/H1/A2"


def test_unit_attributes():
    hps = HydroPowerSystem(1, "Unit hps")
    unit = hps.create_unit(2, "Unit one", "")

    assert hasattr(unit, "generator_description")
    assert hasattr(unit, "turbine_description")
    assert hasattr(unit, "pump_description")
    assert hasattr(unit, "unavailability")
    assert hasattr(unit, "priority")

    assert hasattr(unit, "production")
    assert hasattr(unit.production, "constraint")
    assert hasattr(unit.production.constraint, "min")
    assert hasattr(unit.production.constraint, "max")
    assert hasattr(unit.production, "schedule")
    assert hasattr(unit.production, "result")
    assert hasattr(unit.production, "static_min")
    assert hasattr(unit.production, "static_max")
    assert hasattr(unit.production, "nominal")
    assert hasattr(unit.production, "commitment")

    assert hasattr(unit, "discharge")
    assert hasattr(unit.discharge, "constraint")
    assert hasattr(unit.discharge.constraint, "min")
    assert hasattr(unit.discharge.constraint, "max")
    assert hasattr(unit.discharge.constraint, "max_from_downstream_level")
    assert hasattr(unit.discharge, "schedule")
    assert hasattr(unit.discharge, "result")
    assert hasattr(unit.discharge, "realised")

    assert hasattr(unit, "cost")
    assert hasattr(unit.cost, "start")
    assert hasattr(unit.cost, "stop")

    assert hasattr(unit.reserve, "fcr_static_min")
    assert hasattr(unit.reserve, "fcr_static_max")
    assert hasattr(unit.reserve, "fcr_mip")
    assert hasattr(unit.reserve, "fcr_n")
    assert hasattr(unit.reserve.fcr_n, "up")
    assert hasattr(unit.reserve.fcr_n, "down")

    assert hasattr(unit.reserve, "afrr")
    assert hasattr(unit.reserve.afrr, "up")
    assert hasattr(unit.reserve.afrr, "down")

    assert hasattr(unit.reserve, "mfrr")
    assert hasattr(unit.reserve, "mfrr_static_min")
    assert hasattr(unit.reserve.mfrr, "up")
    assert hasattr(unit.reserve.mfrr, "down")

    assert hasattr(unit.reserve, "rr")
    assert hasattr(unit.reserve.rr, "up")
    assert hasattr(unit.reserve.rr, "down")

    assert hasattr(unit.reserve, "fcr_d")
    assert hasattr(unit.reserve.fcr_d.up, "schedule")
    assert hasattr(unit.reserve.fcr_d.up, "min")
    assert hasattr(unit.reserve.fcr_d.up, "max")
    assert hasattr(unit.reserve.fcr_d.up, "cost")
    assert hasattr(unit.reserve.fcr_d.up, "result")
    assert hasattr(unit.reserve.fcr_d.up, "penalty")
    assert hasattr(unit.reserve.fcr_d.down, "schedule")
    assert hasattr(unit.reserve.fcr_d.down, "min")
    assert hasattr(unit.reserve.fcr_d.down, "max")
    assert hasattr(unit.reserve.fcr_d.down, "cost")
    assert hasattr(unit.reserve.fcr_d.down, "result")
    assert hasattr(unit.reserve.fcr_d.down, "penalty")

    assert hasattr(unit.reserve, "frr")
    assert hasattr(unit.reserve.frr.up, "schedule")
    assert hasattr(unit.reserve.frr.up, "min")
    assert hasattr(unit.reserve.frr.up, "max")
    assert hasattr(unit.reserve.frr.up, "cost")
    assert hasattr(unit.reserve.frr.up, "result")
    assert hasattr(unit.reserve.frr.up, "penalty")

    assert hasattr(unit.reserve.frr.down, "schedule")
    assert hasattr(unit.reserve.frr.down, "min")
    assert hasattr(unit.reserve.frr.down, "max")
    assert hasattr(unit.reserve.frr.down, "cost")
    assert hasattr(unit.reserve.frr.down, "result")
    assert hasattr(unit.reserve.frr.down, "penalty")

    assert hasattr(unit.reserve, "droop")
    assert hasattr(unit.reserve.droop, "schedule")
    assert hasattr(unit.reserve.droop, "cost")
    assert hasattr(unit.reserve.droop, "min")
    assert hasattr(unit.reserve.droop, "max")
    assert hasattr(unit.reserve.droop, "result")

    assert hasattr(unit, "tag")
    assert unit.tag == "/H1/U2"


def test_unit_group_attributes():
    a = StmSystem(1, "A", "{}")
    a.hydro_power_systems.append(create_test_hydro_power_system(hps_id=1, name='ulla-førre'))
    no_1 = MarketArea(1, 'NO1', '{}', a)
    no_1.price.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=3.0, point_fx=pfx.POINT_AVERAGE_VALUE)
    no_1.load.value = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=1300.0, point_fx=pfx.POINT_AVERAGE_VALUE)
    no_1.max_buy.value = TimeSeries('shyft://stm/no_1/max_buy_mw')
    no_1.max_sale.value = TimeSeries('shyft://stm/no_1/max_sale_mw')
    ug = a.add_unit_group(1, "ug1", "{}")
    assert ug
    for u_id, v in zip([1, 165061], [10, 20]):  # assumed to be in the hps
        u = a.hydro_power_systems[0].find_unit_by_id(u_id)
        u.production.result = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=v, point_fx=pfx.POINT_AVERAGE_VALUE)
        u.discharge.result = TimeSeries(TimeAxis(time('2018-10-17T10:00:00Z'), time(3600), 240), fill_value=v/2.0, point_fx=pfx.POINT_AVERAGE_VALUE)
        ug.add_unit(u, TimeSeries())  # just pass empty ts to signal always member
    assert hasattr(ug, "obligation")
    assert hasattr(ug, "production")
    assert hasattr(ug, "flow")
    assert ug.flattened_attributes()
    assert no_1.tag == '/m1'
    assert no_1.load.url('dstm://M1') == 'dstm://M1/m1.load'
    assert no_1.sale.url() == '/m1.sale'


def test_power_station_attributes():
    hps = HydroPowerSystem(1, "Power station hps")
    pwr_plant = hps.create_power_plant(2, "Power station", "")
    assert hasattr(pwr_plant, "outlet_level")
    assert hasattr(pwr_plant, "mip")
    assert hasattr(pwr_plant, "unavailability")

    assert hasattr(pwr_plant, "production")
    assert hasattr(pwr_plant.production, "constraint_min")
    assert hasattr(pwr_plant.production, "constraint_max")
    assert hasattr(pwr_plant.production, "schedule")
    assert hasattr(pwr_plant.production, "realised")
    assert hasattr(pwr_plant.production, "result")

    assert hasattr(pwr_plant.discharge, "ramping_up")
    assert hasattr(pwr_plant.discharge, "ramping_down")

    assert hasattr(pwr_plant, "discharge")
    assert hasattr(pwr_plant.discharge, "constraint_min")
    assert hasattr(pwr_plant.discharge, "constraint_max")
    assert hasattr(pwr_plant.discharge, "schedule")
    assert hasattr(pwr_plant.discharge, "ramping_up")
    assert hasattr(pwr_plant.discharge, "ramping_down")
    assert hasattr(pwr_plant.discharge, "realised")

    assert hasattr(pwr_plant, "tag")
    assert pwr_plant.tag == "/H1/P2"


def test_water_way_attributes():
    hps = HydroPowerSystem(1, "Waterway hps")
    wtr_way = hps.create_waterway(2, "Waterway", "")
    assert hasattr(wtr_way, "head_loss_coeff")
    assert hasattr(wtr_way, "head_loss_func")
    assert hasattr(wtr_way, "delay")

    assert hasattr(wtr_way, "geometry")
    assert hasattr(wtr_way.geometry, "length")
    assert hasattr(wtr_way.geometry, "diameter")
    assert hasattr(wtr_way.geometry, "z0")
    assert hasattr(wtr_way.geometry, "z1")

    assert hasattr(wtr_way, "discharge")
    assert hasattr(wtr_way.discharge, "static_max")
    assert hasattr(wtr_way.discharge, "result")

    assert hasattr(wtr_way, "tag")
    assert wtr_way.tag == "/H1/W2"


def test_gate_attributes():
    hps = HydroPowerSystem(1, "Waterway hps")
    gate = hps.create_waterway(2, "Waterway", "").add_gate(3, "Gate", "")
    assert hasattr(gate, "flow_description")
    assert hasattr(gate, "flow_description_delta_h")

    assert hasattr(gate, "opening")
    assert hasattr(gate.opening, "schedule")

    assert hasattr(gate, "discharge")
    assert hasattr(gate.discharge, "schedule")
    assert hasattr(gate.discharge, "result")
    assert hasattr(gate.discharge, "static_max")

    assert hasattr(gate, "tag")
    assert gate.tag == "/H1/G3"


def test_market_attributes():
    a = StmSystem(1, "A", "{}")
    ma = MarketArea(2, 'NO1', '{}', a)
    # generics, from id_base
    assert hasattr(ma, "id")
    assert hasattr(ma, "name")
    assert hasattr(ma, "json")
    assert hasattr(ma, "ts") # Even this is python exposed on market area!
    # specifics
    assert hasattr(ma, "price")
    assert hasattr(ma, "load")
    assert hasattr(ma, "max_buy")
    assert hasattr(ma, "max_sale")
    assert hasattr(ma, "buy")
    assert hasattr(ma, "sale")
    assert hasattr(ma, "production")
    assert hasattr(ma, "reserve_obligation_penalty")
    assert ma.tag == "/m2"


def test_stm_sys_attributes():
    sys = StmSystem(1, "A", "{misc}")
    assert hasattr(sys, "run_parameters")
    assert hasattr(sys, "unit_groups")
    rp = sys.run_parameters
    assert hasattr(rp, "n_inc_runs")
    assert hasattr(rp, "n_full_runs")
    assert hasattr(rp, "head_opt")
    assert hasattr(rp, "run_time_axis")
    assert hasattr(rp, "fx_log")
    s = sys.summary
    assert hasattr(s, "total")
    s_f = s.flattened_attributes()
    assert len(s_f) == 36
    s_f_expected_keys = ['reservoir.end_value', 'reservoir.sum_ramping_penalty', 'reservoir.sum_limit_penalty', 'reservoir.end_limit_penalty', 'reservoir.hard_limit_penalty',
                         'waterway.vow_in_transit', 'waterway.discharge_group_penalty', 'waterway.discharge_group_ramping_penalty', 'waterway.sum_discharge_fee',
                         'gate.ramping_penalty', 'gate.discharge_cost', 'gate.discharge_constraint_penalty',
                         'spill.cost', 'spill.physical_cost', 'spill.nonphysical_cost', 'spill.physical_volume', 'spill.nonphysical_volume', 'bypass.cost',
                         'ramping.ramping_penalty',
                         'reserve.violation_penalty', 'reserve.sale_buy', 'reserve.obligation_value',
                         'unit.startup_cost', 'unit.schedule_penalty',
                         'plant.production_constraint_penalty', 'plant.discharge_constraint_penalty', 'plant.schedule_penalty', 'plant.ramping_penalty',
                         'market.sum_sale_buy', 'market.load_penalty', 'market.load_value',
                         'total', 'sum_penalties', 'minor_penalties', 'major_penalties', 'grand_total']
    for a in s_f_expected_keys:
        assert a in s_f


def test_t_xy_call():
    """Test that ``t_xy.__call__`` returns the latest value available at the given time."""
    xy = t_xy()
    xy[2] = XyPointCurve([Point(0.0, 0.0)])
    xy[1] = XyPointCurve([Point(1.0, 1.0)])
    xy[3] = XyPointCurve([Point(2.0, 2.0)])

    assert xy(0.5) is None
    assert xy(1.5) == xy[1]
    assert xy(2.5) == xy[2] == xy(2)
    assert xy(3.5) == xy[3]

def test_string_timeseries_dict():
    """Test that the dict string->timeseries is exposed, used by id_base.ts, exposed on market"""
    m = StringTimeSeriesDict()
    assert len(m) == 0
    a = TimeSeries()
    b = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [1, 0, 1], pfx.POINT_AVERAGE_VALUE)
    m['a'] = a
    m['b'] = b
    assert m['a'] == a
    assert m['b'] == b


def test_market_ts():
    a = StmSystem(1, "A", "{}")
    ma = MarketArea(2, 'NO1', '{}', a)
    a = TimeSeries()
    b = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [1, 0, 1], pfx.POINT_AVERAGE_VALUE)
    ma.ts['a'] = a
    ma.ts['b'] = b
    assert ma.ts['a'] == a
    assert ma.ts['b'] == b
    assert not ma.get_tsm_object('a').exists
    assert ma.get_tsm_object('a').url() == '/m2.ts.a'
    assert ma.get_tsm_object('b').exists
    assert ma.get_tsm_object('b').url() == '/m2.ts.b'
    with pytest.raises(RuntimeError, match="Key does not exist"):
        _ = ma.get_tsm_object('x')
    assert ma.demand.bids.url("",0,-1) == ".demand.bids"
    assert ma.demand.usage.realised.url("", 0, -1) == ".demand.usage.realised"
    assert ma.demand.usage.schedule.url("", 0, -1) == ".demand.usage.schedule"
    assert ma.demand.usage.result.url("", 0, -1) == ".demand.usage.result"
    assert ma.supply.bids.url("",0,-1) == ".supply.bids"
    assert ma.supply.usage.realised.url("", 0, -1) == ".supply.usage.realised"
    assert ma.supply.usage.schedule.url("", 0, -1) == ".supply.usage.schedule"
    assert ma.supply.usage.result.url("", 0, -1) == ".supply.usage.result"


def test_proxy_attribute_url():
    """Test the various versions of a proxy-attribute's url-funciton"""
    hps = HydroPowerSystem(1, "url hps")
    rsvx = hps.create_reservoir(2, "reservoir", "")
    rsv = hps.reservoirs[0]
    assert rsv.inflow.schedule.url() == "/H1/R2.inflow.schedule"
    assert hasattr(rsv, "ts")
    assert rsv.inflow.schedule.url("", 0, -1) == ".inflow.schedule"
    assert rsv.inflow.schedule.url("", 0, 0) == ".{attr_id}"
    assert rsv.inflow.schedule.url("", 0, 1) == ".inflow.schedule"

    assert rsv.inflow.schedule.url("", 1) == "/R2.inflow.schedule"
    assert rsv.inflow.schedule.url("", 1, 0) == "/R{o_id}.{attr_id}"
    assert rsv.inflow.schedule.url("", 1, 1) == "/R{o_id}.inflow.schedule"
    assert rsv.inflow.schedule.url("", 1,1,True) == "/R{}.inflow.schedule"
    assert rsv.inflow.schedule.url("", 1, 2) == "/R2.inflow.schedule"

    assert rsv.inflow.schedule.url("", 2) == "/H1/R2.inflow.schedule"
    assert rsv.inflow.schedule.url("", 2, 0) == "/H{parent_id}/R{o_id}.{attr_id}"
    assert rsv.inflow.schedule.url("", 2, 1) == "/H{parent_id}/R{o_id}.inflow.schedule"
    assert rsv.inflow.schedule.url("dstm://{{{case}}}#{{{run}}}", 2, 1, True) == "dstm://{{{case}}}#{{{run}}}/H{}/R{}.inflow.schedule"
    assert rsv.inflow.schedule.url("", 2, 2) == "/H{parent_id}/R2.inflow.schedule"
    assert rsv.inflow.schedule.url("", 2, 3) == "/H1/R2.inflow.schedule"


def test_proxy_attribute_unit_url():
    """Test the various versions of a proxy-attribute's url-funciton"""
    hps = HydroPowerSystem(1, "url hps")
    u = hps.create_unit(2, "unit", "")

    assert u.unavailability.url() == "/H1/U2.unavailability"
    assert hasattr(u, "ts")

    assert u.unavailability.url("", 0) == ".unavailability"
    assert u.unavailability.url("", 0, 0) == ".{attr_id}"
    assert u.unavailability.url("", 0, 1) == ".unavailability"

    assert u.unavailability.url("", 1) == "/U2.unavailability"
    assert u.unavailability.url("", 1, 0) == "/U{o_id}.{attr_id}"
    assert u.unavailability.url("", 1, 1) == "/U{o_id}.unavailability"
    assert u.unavailability.url("", 1, 2) == "/U2.unavailability"

    assert u.unavailability.url("", 2) == "/H1/U2.unavailability"
    assert u.unavailability.url("", 2, 0) == "/H{parent_id}/U{o_id}.{attr_id}"
    assert u.unavailability.url("", 2, 1) == "/H{parent_id}/U{o_id}.unavailability"
    assert u.unavailability.url("", 2, 2) == "/H{parent_id}/U2.unavailability"
    assert u.unavailability.url("", 2, 3) == "/H1/U2.unavailability"


def test_proxy_attribute_unit_fx():
    """Test the various versions of a proxy-attribute's url-function"""
    hps = HydroPowerSystem(1, "url hps")
    u = hps.create_unit(2, "u1", "")

    assert not u.unavailability.exists
    a = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [1, 0, 1], pfx.POINT_AVERAGE_VALUE)
    b = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [1, 0, 1], pfx.POINT_AVERAGE_VALUE)
    c = TimeSeries(TimeAxis(time('2020-12-03T00:00:00Z'), time(3600), 3), [0, 1, 0], pfx.POINT_AVERAGE_VALUE)
    assert a == b
    u.unavailability = a  # assign directly to the proxy (conversion goes magic between proxy<T>->T)
    assert u.unavailability.exists  # verify it's there
    u.unavailability.remove()
    assert not u.unavailability.exists  # verify it's gone
    u.unavailability.value = a  # ensure we can assign to value  by ref
    assert u.unavailability.exists  # verify it's there again
    astr = str(u.unavailability)
    v = hps.create_unit(3, "u2")
    assert v.unavailability != u.unavailability  # ensure it compares not equal by value
    v.unavailability = b
    assert v.unavailability == u.unavailability  # ensure it compares equal by value
    u.unavailability.remove()  # remove it
    assert not u.unavailability.exists  # verify it's not there any more
    v.unavailability.remove()
    assert v.unavailability == u.unavailability  # ensure it compares equal by value(null== null) in this case
    u.unavailability = a
    assert u.unavailability == b  # now compare a ts to a proxy<ts>
    assert u.unavailability != c  # now compare a ts to a proxy<ts>
    # test time-dependent turbine-description at main object.
    td = u.turbine_description
    assert not td.exists
    t0 = time('2020-12-06T00:00:00Z')
    u.turbine_description = create_t_turbine_description(t0,
                                                         [XyPointCurveWithZ(
                                                             XyPointCurve(PointList([Point(10.0, 0.6), Point(15.0, 0.8), Point(20.0, 0.7)])),
                                                             400.0)
                                                         ]
                                                         )
    assert u.turbine_description.exists
    td = u.turbine_description
    u.turbine_description.remove()
    assert not u.turbine_description.exists
    # test TimeSeries attribute in a nested Unit._Production.result
    upr = u.production.result
    assert not upr.exists
    u.production.result = a
    assert u.production.result.exists
    upr_url = u.production.result.url()
    assert upr_url == '/H1/U2.production.result'


def test_stm_run():
    mdl = StmSystem(1, "A", "{misc}")
    ctx = StmSystemContext(ModelState.IDLE, mdl)
    assert not ctx.system.run_parameters.fx_log.exists
    ctx.message("test message")
    assert ctx.system.run_parameters.fx_log.exists
    assert len(ctx.system.run_parameters.fx_log.value) == 1
    assert ctx.system.run_parameters.fx_log.value[0].message == "test message"


def test_stm_gate_idents_unique():
    """Test that adding that gate id uniqueness is enforced"""
    hps = HydroPowerSystem(0, "test")
    ww0 = hps.create_waterway(0, "ww0")
    ww1 = hps.create_waterway(1, "ww1")

    _ = ww0.add_gate(0, "gate_0")
    _ = ww1.add_gate(1, "gate_1")

    with pytest.raises(RuntimeError, match="Gate id must be unique"):
        _ = ww1.add_gate(0, "gate_3")


def test_stm_gate_names_unique():
    """Test that adding that gate name uniqueness is enforced"""
    hps = HydroPowerSystem(0, "test")
    ww0 = hps.create_waterway(0, "ww0")
    ww1 = hps.create_waterway(1, "ww1")

    _ = ww0.add_gate(0, "gate_0")
    g = ww1.add_gate(1, "gate_1")
    assert hasattr(g,"ts")
    with pytest.raises(RuntimeError, match="Gate name must be unique"):
        _ = ww1.add_gate(2, "gate_0")


# noinspection DuplicatedCode
class TestFlattenedAttributes:
    def test_unit_attributes_flattened(self, unit_factory):
        """Test that 'unit.flattened_attributes' has correct paths and attributes."""
        unit = unit_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_reservoir_attributes_flattened(self, reservoir_aggregate_factory):
        """Test that 'reservoir_aggregate.flattened_attributes' has correct paths and attributes."""
        ra = reservoir_aggregate_factory()

        for (path, val) in ra.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), ra)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_waterway_attributes_flattened(self, waterway_factory):
        """Test that 'waterway.flattened_attributes' has correct paths and attributes."""
        unit = waterway_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_gate_attributes_flattened(self, gate_factory):
        """Test that 'gate.flattened_attributes' has correct paths and attributes."""
        unit = gate_factory()

        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()

    def test_power_plant_attributes_flattened(self, power_plant_factory):
        """Test that 'power_plant.flattened_attributes' has correct paths and attributes."""
        unit = power_plant_factory()
        assert hasattr(unit, "ts")
        for (path, val) in unit.flattened_attributes().items():
            attr = reduce(getattr, path.split('.'), unit)
            assert type(val) is type(attr)
            assert val == attr
            assert val.url() == attr.url()
