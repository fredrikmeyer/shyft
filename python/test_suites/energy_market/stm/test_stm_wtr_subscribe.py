from shyft.energy_market import stm
from shyft.energy_market import core
import shyft.time_series as sts
import pytest
import asyncio
import json
import numpy as np
from time import sleep

websockets = pytest.importorskip("websockets")


def create_t_double(t: sts.time, v: float):
    return sts.TimeSeries(sts.TimeAxis([t, sts.max_utctime]), v, sts.POINT_AVERAGE_VALUE)


def create_mock_hps(t0: sts.time, hps_id: int = 1, name: str = "Hydro_power_system") -> stm.HydroPowerSystem:
    """
    Test to learn how to model hydro power system with new stm-model in python
    """
    M = 10**6
    hps = stm.HydroPowerSystem(hps_id, name)

    # ---------- Reservoir ----------
    rsv = hps.create_reservoir(1, "Reservoir")

    # Lowest regulated water level
    lrl = create_t_double(t0, 200.0)
    rsv.level.constraint.min.value = lrl
    # Highest regulated water level
    hrl = create_t_double(t0, 250.0)
    rsv.level.constraint.max.value = hrl

    # ------------- Units -------------
    unit1 = hps.create_unit(1, 'Unit1')

    unit1.production.static_min.value = create_t_double(t0, 30.0)
    unit1.production.static_max.value = create_t_double(t0, 60.0)
    unit1.production.nominal.value = create_t_double(t0, 60.0)

    unit1.cost.start.value = create_t_double(t0, 200.0)
    unit1.cost.stop.value = create_t_double(t0, 200.0)

    # ----------- Power plants -----------
    leirdola_pp = hps.create_power_plant(1, 'Unit1')
    leirdola_pp.add_unit(unit1)

    leirdola_pp.outlet_level.value = create_t_double(t0, 5.5)  # Outlet level might to be moved to waterway

    # ------------- Waterways -------------
    wr1 = hps.create_waterway(2, 'w_Reservoir_to_Unit1')
    pen1 = hps.create_waterway(3, 'Penstock 1 Unit1')
    tr1 = hps.create_waterway(4, 'Tail race for Unit1')

    wr1.input_from(rsv).output_to(pen1)
    pen1.input_from(wr1).output_to(unit1)
    tr1.input_from(unit1)

    wr1.head_loss_coeff.value = create_t_double(t0, 0.01)
    pen1.head_loss_coeff.value = create_t_double(t0, 0.0075)

    wr1.add_gate(2, 'w_Reservoir_to_Unit1_gate', '{}')

    f = hps.create_river(5, 'f_Reservoir_Sea') \
        .input_from(rsv, core.ConnectionRole.flood)

    t1 = t0
    t2 = t1 + sts.Calendar.HOUR
    t3 = t2 + sts.Calendar.HOUR
    t4 = t3 + sts.Calendar.HOUR
    n = 30
    dt = sts.deltahours(1)
    w = sts.DoubleVector([0.05, 0.15, 0.6, 0.15, 0.05])  # weights)
    v = [float(i)*1.0 for i in range(n)]
    # f.discharge.realised = sts.TimeSeries(sts.TimeAxis(t1-dt*n/2, dt, n), v, sts.POINT_AVERAGE_VALUE).convolve_w(w, sts.convolve_policy.CENTER | sts.convolve_policy.USE_NEAREST)
    f.discharge.realised = sts.TimeSeries(sts.shyft_url("test", "W5.flood.discharge.realised")).convolve_w(w, sts.convolve_policy.CENTER | sts.convolve_policy.USE_NEAREST)
    #
    hps.create_river(6, 'b_Reservoir_Sea') \
        .input_from(rsv, core.ConnectionRole.bypass)

    # --------- Spill ---------
    spill = hps.find_waterway_by_name("f_Reservoir_Sea")
    spill_gt = spill.add_gate(1, "Spill")

    spill_desc_xyz = core.XyPointCurveWithZ()
    spill_desc_xyz.xy_point_curve = core.XyPointCurve(core.PointList([
        core.Point(250.0000, 0.0000),
        core.Point(251.0000, 100.0000),
        core.Point(252.0000, 225.0000)]))
    spill_desc_xyz.z = 250.0000
    spill_desc = stm.t_xyz_list()
    spill_desc[t0] = core.XyPointCurveWithZList([spill_desc_xyz])
    spill_gt.flow_description.value = spill_desc

    return hps


def uri(host, port_num):
    return f"ws://{host}:{port_num}"


def get_response(req: str, host: str, port_no: int) -> str:
    async def wrap(wreq):
        async with websockets.connect(uri(host, port_no)) as websocket:
            await websocket.send(wreq)
            response = await websocket.recv()
            return response

    return asyncio.get_event_loop().run_until_complete(wrap(req))


def test_stm_wtr_subscribe(web_api_port, tmpdir):
    """
    Test submitted by Ole-Andreas Ramsdal, to reveal the problem with subscribing to waterway
    Ref issue #804
    It also demonstrates/verifies the function of the request .read_period attribute,
    so that the convolve function get sufficient data to evaluate stable over the requested time-axis.
    """
    dstm_web_port = web_api_port

    host = "127.0.0.1"
    dtss = sts.DtsServer()
    dtss.set_container("test", str(tmpdir/"dtss_test"))
    ip = dtss.get_listening_ip()
    dtss.set_listening_ip(host)
    dtss_port = dtss.start_server()

    dstm_srv = stm.DStmServer()
    dstm_srv.set_master_slave_mode(ip=host, port=dtss_port, master_poll_time=0.1, unsubscribe_threshold=100, unsubscribe_max_delay=0.5)
    dstm_port = dstm_srv.start_server()
    dstm_srv.start_web_api(host, dstm_web_port, str(tmpdir))
    sleep(0.5)  # allow web-api to startup, listening before continue
    mdl_key = 'mdl_key'

    # # Alt. 1
    # store time-series on the dtss server, so that they will be bound/evaluated when we do the request
    # .. with the read-period required to resolve the convolve_w function.
    #
    t0 = sts.time("2000-01-01T00:00:00Z")
    n = 30
    hta = sts.TimeAxis(t0 - sts.deltahours(15), sts.deltahours(1), n)
    values = sts.DoubleVector([float(i) for i in range(n)])
    tsv = sts.TsVector([sts.TimeSeries(sts.shyft_url("test", "W5.flood.discharge.realised"), sts.TimeSeries(hta, values, sts.point_interpretation_policy.POINT_AVERAGE_VALUE))])
    c = sts.DtsClient(f"{host}:{dtss_port}")
    c.store_ts(tsv=tsv, overwrite_on_write=True, cache_on_write=False)

    hps = create_mock_hps(t0)
    stm_sys = stm.StmSystem(1, 'stm-sys', "")
    stm_sys.hydro_power_systems.append(hps)
    dstm_srv.do_add_model(mdl_key, stm_sys)
    read_ta = sts.TimeAxis(t0, sts.Calendar.HOUR, 3)
    read_period = hta.total_period()
    read_req_data = {"request_id": "sample read request",
                     "model_key": mdl_key,
                     "time_axis": {"t0": read_ta.fixed_dt.start.seconds, "dt": read_ta.fixed_dt.delta_t.seconds, "n": read_ta.fixed_dt.n},
                     "read_period": [read_period.start.seconds, read_period.end.seconds],
                     "hps": [{"hps_id": 1,
                              "waterways": [{
                                  "component_id": 5,
                                  "attribute_ids": ["discharge.realised"]
                              }]
                              }],
                     "subscribe": True}
    read_req = f"read_attributes {json.dumps(read_req_data, ensure_ascii=False)}"

    response = get_response(read_req, host, dstm_web_port)
    assert len(response)
    rx=json.loads(response)
    # verify we got the expected result. Since we do read sufficient with data, the convolve fx will make an straight line through
    #  the  requested period.
    assert np.allclose(rx['result']['hps'][0]['waterways'][0]['component_data'][0]['data']['values'],[15.0,16.0,17.0],rtol=0.000001)
    dstm_srv.stop_web_api()
    dstm_srv.close()
    dtss.stop_server()
