from PySide2 import __version__ as PySideVersion
from PySide2.QtCore import __version__ as PySideQtVersion, qVersion, QVersionNumber
from PySide2.QtCore import Qt, QDate, QTime, QDateTime
from PySide2.QtGui import QFont, QIcon
from PySide2.QtWidgets import QApplication, QVBoxLayout, QGridLayout
from PySide2.QtWidgets import QWidget, QLabel, QPushButton, QCheckBox, QRadioButton, QButtonGroup, QFrame, QTreeWidget, QTreeWidgetItem, QTableWidget, QTableWidgetItem, QToolBar, QToolButton, QAction, QMenu
from PySide2.QtCharts import QtCharts
from shyft.energy_market import ui
import random
import json

app = None
def init_qt():
    # Init function to be called at the beginning of each test (that uses PySide2/Qt)
    global app
    if app is None:
        app = QApplication([]) # Create QApplication singleton (python will crash if trying to do this more than once)
    # Print PySide and Qt version numbers so they will be included in captured stdout when a test fails (will normally not be shown)
    print(f"PySide2 version: {PySideVersion}") # PySide2 version
    print(f"PySide2 Qt version: {PySideQtVersion}") # Qt version used to compile PySide2
    print(f"Qt runtime version: {qVersion()}") # Qt version of runtime libraries currently used

def qt_version_at_least(version:str) -> bool:
    # Returns true if current Qt runtime version is not older than specified version
    return QVersionNumber([int(x) for x in qVersion().split(".")]) >= QVersionNumber([int(x) for x in version.split(".")])

def test_simple_widget():
    init_qt()
    window = QWidget()
    window.setObjectName("ObjectName")
    window.setProperty("PropertyName", "PropertyValue")
    window.setToolTip("ToolTip")
    window.setStatusTip("StatusTip")
    window.setWhatsThis("WhatsThis")
    window.setWindowRole("WindowRole")
    window.setWindowTitle('WindowTitle')
    window.setAccessibleName("AccessibleName")
    window.setAccessibleDescription("AccessibleDescription")
    window.setStyleSheet("StyleSheet")
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in ('id', 'properties','toolTip', 'statusTip', 'whatsThis', 'caption', 'role', 'accessibleName', 'accessibleDescription', 'styleSheet', 'type') for key in jwindow)
    assert jwindow.get('id') == 'ObjectName'
    p = jwindow.get('properties')
    assert p
    assert 'PropertyName' in p
    assert p.get('PropertyName') == 'PropertyValue'
    assert jwindow.get('toolTip') == 'ToolTip'
    assert jwindow.get('statusTip') == 'StatusTip'
    assert jwindow.get('whatsThis') == 'WhatsThis'
    assert jwindow.get('caption') == 'WindowTitle'
    assert jwindow.get('role') == 'WindowRole'
    assert jwindow.get('accessibleName') == 'AccessibleName'
    assert jwindow.get('accessibleDescription') == 'AccessibleDescription'
    assert jwindow.get('styleSheet') == 'StyleSheet'

def test_table_widget():
    # First create a 10x3 table, without content
    init_qt()
    window = QWidget()
    table = QTableWidget(window)
    table.setRowCount(10)
    table.setColumnCount(3)
    tableHeader = ["A", "B", "C"]
    table.setHorizontalHeaderLabels(tableHeader)
    table.verticalHeader().setVisible(False)
    table.setShowGrid(False)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtable = jwidgets[0]
    assert all(key in jtable for key in ('type', 'rowCount', 'columnCount'))
    assert jtable.get('type') == 'table'
    assert jtable.get('rowCount') == 10
    assert jtable.get('columnCount') == 3
    # Then fill with some data, but only 2 of 10 rows
    table.setItem(0, 0, QTableWidgetItem("Item 0.0"))
    table.setItem(0, 1, QTableWidgetItem("Item 0.1"))
    table.setItem(0, 2, QTableWidgetItem("Item 0.2"))
    table.setItem(1, 0, QTableWidgetItem("Item 1.0"))
    table.setItem(1, 1, QTableWidgetItem("Item 1.1"))
    table.setItem(1, 2, QTableWidgetItem("Item 1.2"))
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'widgets' in jwindow
    jtable = jwindow.get('widgets')[0]
    assert all(key in jtable for key in ('type', 'rowCount', 'columnCount', 'rows'))
    assert jtable.get('type') == 'table'
    assert jtable.get('rowCount') == 10
    assert jtable.get('columnCount') == 3
    jrows = jtable.get('rows')
    assert len(jrows) == 2 # Note: Only the 2 rows with data emitted, not all 10 given by rowCount!
    jrow = jrows[0]
    assert len(jrow) == 3
    jcell = jrow[0]
    assert all(key in jcell for key in ('value',))
    assert jcell.get('value') == 'Item 0.0'
    jcell = jrow[1]
    assert all(key in jcell for key in ('value',))
    assert jcell.get('value') == 'Item 0.1'
    jcell = jrow[2]
    assert all(key in jcell for key in ('value',))
    assert jcell.get('value') == 'Item 0.2'

def test_chart_widget():
    init_qt()
    fill_data = True
    window = QWidget()
    axis_x = QtCharts.QDateTimeAxis()
    axis_x.setTickCount(10)
    axis_x.setFormat("dd.MM (h:mm)")
    axis_x.setTitleText("This is a datetime axis")
    axis_y = QtCharts.QValueAxis()
    axis_y.setTickCount(10)
    axis_y.setLabelFormat("%.2f")
    axis_y.setTitleText("This is a value axis")
    axis_y.setMin(10)
    axis_y.setMax(100)
    serie1 = QtCharts.QLineSeries()
    serie1.setName("This is a series")
    if fill_data:
        t = QDateTime.currentDateTime()
        t.setTime(QTime())
        for _ in range(24):
            v = random.randrange(10, 100)
            serie1.append(float(t.toMSecsSinceEpoch()), v)
            t = t.addSecs(3600)
    chart = QtCharts.QChart()
    chart.setTitle("This is a chart")
    chart.addAxis(axis_x, Qt.AlignBottom)
    chart.addAxis(axis_y, Qt.AlignLeft)
    chart.addSeries(serie1)
    serie1.attachAxis(axis_x)
    serie1.attachAxis(axis_y)
    chartView = QtCharts.QChartView(chart, window)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtable = jwidgets[0]
    assert all(key in jtable for key in ('type', 'title', 'axes', 'series'))
    assert jtable.get('type') == 'chart'
    assert jtable.get('title') == 'This is a chart'
    jaxes = jtable.get('axes')
    assert len(jaxes) == 2
    jaxis = jaxes[0]
    assert all(key in jaxis for key in ('type', 'title', 'orientation', 'min', 'max', 'tickCount', 'format'))
    assert jaxis.get('type') == 'dateTime'
    assert jaxis.get('orientation') == 'horizontal'
    assert jaxis.get('title') == 'This is a datetime axis'
    jaxis = jaxes[1]
    if qt_version_at_least("5.12"): # TickType (Fixed|Dynamic) was first introduced in Qt 5.12
        assert all(key in jaxis for key in ('type', 'title', 'orientation', 'min', 'max', 'tickType', 'tickCount', 'format'))
    else:
        assert all(key in jaxis for key in ('type', 'title', 'orientation', 'min', 'max', 'tickCount', 'format'))
    assert jaxis.get('type') == 'value'
    assert jaxis.get('orientation') == 'vertical'
    assert jaxis.get('title') == 'This is a value axis'
    jseries = jtable.get('series')
    assert len(jseries) == 1
    jserie = jseries[0]
    assert all(key in jserie for key in ('type', 'name', 'attachedAxes'))
    assert jserie.get('type') == 'line'
    assert jserie.get('name') == 'This is a series'
    jaa = jserie.get('attachedAxes')
    assert len(jaa) == 2
    assert jaa[0] == 0
    assert jaa[1] == 1

def test_tree_widget():
    init_qt()
    window = QWidget()
    tree = QTreeWidget(window)
    #tree.setColumnCount(1)
    tree.setHeaderHidden(True)
    tree.setHeaderLabel("")
    items = []
    for i in range(3):
        p = QTreeWidgetItem([f"Item {i}"])
        children = []
        for j in range(3):
            q = QTreeWidgetItem([f"Item {i}.{j}"])
            grandchildren = []
            for k in range(3):
                r = QTreeWidgetItem([f"Item {i}.{j}.{k}"])
                grandchildren.append(r)
            q.addChildren(grandchildren)
            children.append(q)
        p.addChildren(children)
        items.append(p)
    tree.insertTopLevelItems(0, items)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtree = jwidgets[0]
    assert all(key in jtree for key in ('type', 'columnCount', 'items'))
    assert jtree.get('type') == 'tree'
    assert jtree.get('columnCount') == 1
    jitems = jtree.get('items')
    assert len(jitems) == 3
    jitem = jitems[0]
    assert all(key in jitem for key in ('type', 'columnCount', 'value', 'items'))
    assert jitem.get('columnCount') == 1
    assert len(jitem.get('value')) == 1
    assert jitem.get('value')[0] == 'Item 0'
    assert len(jitem.get('items')) == 1
    jitem = jitem.get('items')[0]
    assert jitem.get('value')[0] == 'Item 0.0'
    assert len(jitem.get('items')) == 1
    jitem = jitem.get('items')[0]
    assert jitem.get('value')[0] == 'Item 0.0.0'
    assert 'items' not in jitem

def test_toolbar_with_action():
    init_qt()
    # Create toolbar, add one action which will be represented by an implicitely created toolbutton
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle('This is a toolbar')
    toolbar.setOrientation(Qt.Vertical)
    a = QAction("A")
    toolbar.addAction(a)
    toolbutton = toolbar.widgetForAction(a)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 1 # 1 button for action A, the default button for toolbar overflow is excluded by export
    assert jbuttons[0].get('toolTip') == 'A'
    jactions = jbuttons[0].get('actions')
    assert jactions
    assert len(jactions) == 1
    assert jactions[0].get('text') == 'A'

def test_toolbar_with_toolbutton():
    init_qt()
    # Create toolbar, add one toolbutton directly as a child widget,
    # add one action to the toolbutton.
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle('This is a toolbar')
    toolbar.setOrientation(Qt.Vertical)
    toolbutton = QToolButton(window)
    toolbutton.setText("This is a button")
    toolbutton.setPopupMode(QToolButton.InstantPopup) # DelayedPopup, MenuButtonPopup, InstantPopup
    a = QAction("A")
    toolbutton.addAction(a)
    wa = toolbar.addWidget(toolbutton)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 1
    assert jbuttons[0].get('text') == 'This is a button' #  Button should have the text set, and not the text from the action!
    assert jbuttons[0].get('type') == 'toolButton'
    jactions = jbuttons[0].get('actions') # Button should return 1 item from actions(), but it is not set as defaultAction()
    assert jactions
    assert len(jactions) == 1
    assert jactions[0].get('text') == 'A'

def test_toolbar_with_toolbutton_with_defaultaction():
    init_qt()
    # Create toolbar, add one toolbutton directly as a child widget,
    # add one action to the toolbutton, and set this action as the
    # button's "default action".
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle('This is a toolbar')
    toolbar.setOrientation(Qt.Vertical)
    toolbutton = QToolButton(window)
    toolbutton.setText("This is a button")
    #toolbutton.setPopupMode(QToolButton.InstantPopup) # DelayedPopup, MenuButtonPopup, InstantPopup
    a = QAction("A")
    #toolbutton.addAction(a) # Does not matter, setDefaultAction will implicitely add it if (and only if) it has not already been added to list of actions
    toolbutton.setDefaultAction(a)
    wa = toolbar.addWidget(toolbutton)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 1
    assert jbuttons[0].get('text') == 'A' # Button should now inherit the text etc from the action
    assert jbuttons[0].get('toolTip') == 'A' # Emitter currently does emit the tooltip, even if it is just the default value fetched from text, due to shared code in implementation..
    assert jbuttons[0].get('type') == 'toolButton'
    jactions = jbuttons[0].get('actions')
    assert jactions
    assert len(jactions) == 1 # Button should return 1 item from actions(), which is the same as from defaultAction()
    assert jactions[0].get('text') == 'A'

def test_toolbar_with_actions():
    init_qt()
    # Create toolbar with three actions, each of them with their own toolbutton
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle('This is a toolbar')
    toolbar.setOrientation(Qt.Vertical)
    a = QAction("A")
    toolbar.addAction(a)
    b = QAction("B")
    toolbar.addAction(b)
    c = QAction("C")
    toolbar.addAction(c)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 3 # 3 buttons for actions A, B and C, the default button for toolbar overflow is excluded by export
    assert jbuttons[0].get('toolTip') == 'A'
    assert jbuttons[1].get('toolTip') == 'B'
    assert jbuttons[2].get('toolTip') == 'C'
    jactions = jbuttons[0].get('actions')
    assert jactions
    assert len(jactions) == 1
    assert jactions[0].get('text') == 'A'

def test_toolbar_with_toolbutton_with_actions():
    init_qt()
    # Create toolbar with one toolbutton, which has multiple actions on it
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle("This is a toolbar")
    toolbar.setOrientation(Qt.Vertical)
    d = QAction("D")
    toolbar.addAction(d) # Adding action D to toolbar, implicitely creating a toolbutton widget for it
    d_toolbutton = toolbar.widgetForAction(d) # Get back the implicitely crated toolbutton widget
    d_toolbutton.setPopupMode(QToolButton.InstantPopup)
    d1 = QAction("D1")
    d_toolbutton.addAction(d1) # Adding action D1 to existing toolbutton widget
    d2 = QAction("D2")
    d_toolbutton.addAction(d2) # Adding action D1 to existing toolbutton widget
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 1 # 1 button for action D, the default button for toolbar overflow is excluded by export
    jbutton = jbuttons[0]
    assert jbutton.get('toolTip') == 'D'
    jactions = jbutton.get('actions')
    assert jactions
    assert len(jactions) == 3
    assert jactions[0].get('text') == 'D'
    assert jactions[1].get('text') == 'D1'
    assert jactions[2].get('text') == 'D2'

def test_toolbar_with_toolbutton_with_action_and_menu():
    init_qt()
    # Create toolbar, add one action which will be represented by an implicitely created toolbutton,
    # and associate a menu containing two additional actions to the toolbutton.
    # This should give a structure of: toolbutton->actions, where one action is the one explicitely
    # created (E) and the other one is one implicitely created for the menu, which in turn has
    # two actions actions associated (action-menu-actions structure).
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle("This is a toolbar")
    toolbar.setOrientation(Qt.Vertical)
    e = QAction("E")
    toolbar.addAction(e) # Adding action E to toolbar, implicitely creating a toolbutton widget for it
    e_toolbutton = toolbar.widgetForAction(e) # Get back the implicitely crated toolbutton widget
    e_toolbutton.setPopupMode(QToolButton.InstantPopup) # DelayedPopup, MenuButtonPopup, InstantPopup
    e_toolmenu = QMenu() # Create a menu widget
    e_toolmenu.addAction(QAction("E1", window)) # Add action to menu
    e_toolmenu.addAction(QAction("E2", window))
    e_toolbutton.setMenu(e_toolmenu) # Associate the menu widget with the toolbutton
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 1 # 1 button for action E, the default button for toolbar overflow is excluded by export
    jbutton = jbuttons[0]
    assert jbutton.get('toolTip') == 'E'
    jactions = jbutton.get('actions')
    assert jactions
    assert len(jactions) == 2
    assert jactions[0].get('text') == 'E'
    jmenu = jactions[1].get('menu')
    assert jmenu
    jactions = jmenu.get('actions')
    assert jactions
    assert len(jactions) == 2
    assert jactions[0].get('text') == 'E1'
    assert jactions[1].get('text') == 'E2'

def test_toolbar_with_toolbutton_with_action_with_menu():
    init_qt()
    # Similar to previous test but with menu associated with action instead of toolbutton.
    # Create toolbar, add one action which will be represented by an implicitely created toolbutton,
    # and associate a menu containing two additional actions to the primary action!
    # This should give a structure of: toolbutton->action(E)->menu->actions(E1,E2)
    window = QWidget()
    toolbar = QToolBar(window)
    toolbar.setWindowTitle("This is a toolbar")
    toolbar.setOrientation(Qt.Vertical)
    e = QAction("E")
    e_menu = QMenu() # Create a menu widget
    e_menu.addAction(QAction("E1", window)) # Add action to menu
    e_menu.addAction(QAction("E2", window))
    e.setMenu(e_menu)
    toolbar.addAction(e) # Adding action E to toolbar, implicitely creating a toolbutton widget for it
    e_toolbutton = toolbar.widgetForAction(e) # Get back the implicitely crated toolbutton widget
    e_toolbutton.setPopupMode(QToolButton.InstantPopup) # DelayedPopup, MenuButtonPopup, InstantPopup
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in jwindow for key in ('widgets',))
    jwidgets = jwindow.get('widgets')
    assert len(jwidgets) == 1
    jtoolbar = jwidgets[0]
    assert jtoolbar
    assert all(key in jtoolbar for key in ('caption', 'type', 'orientation', 'widgets'))
    assert jtoolbar.get('type') == 'toolbar'
    assert jtoolbar.get('caption') == 'This is a toolbar'
    assert jtoolbar.get('orientation') == 'vertical'
    jbuttons  = jtoolbar.get('widgets')
    assert jbuttons
    len(jbuttons) == 1 # 1 button for action E, the default button for toolbar overflow is excluded by export
    jbutton = jbuttons[0]
    assert jbutton.get('toolTip') == 'E'
    jactions = jbutton.get('actions')
    assert jactions
    assert len(jactions) == 1 # One "top level" action
    assert jactions[0].get('text') == 'E'
    jmenu = jactions[0].get('menu') # Menu associated with the action
    assert jmenu
    jactions = jmenu.get('actions')
    assert jactions
    assert len(jactions) == 2 # Two actions associated with the menu
    assert jactions[0].get('text') == 'E1'
    assert jactions[1].get('text') == 'E2'

def test_box_layout():
    init_qt()
    window = QWidget()
    layout = QVBoxLayout()
    layout.addWidget(QLabel('Top'))
    layout.addWidget(QLabel('Bottom'))
    window.setWindowTitle('Hello')
    window.setLayout(layout)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert all(key in ('caption','type','layout') for key in jwindow)
    assert jwindow.get('caption') == 'Hello'
    #assert jwindow.get('type') == 'widget'
    jlayout = jwindow.get('layout')
    assert jlayout
    assert all(key in jlayout for key in ('type', 'direction', 'items'))
    assert jlayout.get('type') == 'box'
    assert jlayout.get('direction') == 'topToBottom'
    assert len(jlayout.get('items')) == 2
    jlayout_item = jlayout.get('items')[0]
    jwidget = jlayout_item.get('widget')
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'Top'
    jlayout_item = jlayout.get('items')[1]
    jwidget = jlayout_item.get('widget')
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'Bottom'

def test_buttons():
    init_qt()
    window = QWidget()
    layout = QVBoxLayout()
    window.setLayout(layout)

    # PushButton
    button = QPushButton(window)
    button.setText("This is a PushButton")
    layout.addWidget(button)

    # Checkbox
    checkbox = QCheckBox(window)
    checkbox.setText("This is a CheckBox")
    layout.addWidget(checkbox)

    # RadioButton
    radiobutton = QRadioButton(window)
    radiobutton.setText("This is a RadioButton")
    layout.addWidget(radiobutton)

    # ButtonGroup with radiobuttons
    buttongroup = QButtonGroup()
    r1 = QRadioButton(window)
    r1.setText("This is grouped RadioButton 1")
    buttongroup.addButton(r1)
    layout.addWidget(r1)
    r2 = QRadioButton(window)
    r2.setText("This is grouped RadioButton 2")
    r2.setChecked(True)
    buttongroup.addButton(r2)
    layout.addWidget(r2)
    r3 = QRadioButton(window)
    r3.setText("This is grouped RadioButton 3")
    buttongroup.addButton(r3)
    layout.addWidget(r3)

    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'layout' in jwindow
    jlayout = jwindow.get('layout')
    assert all(key in jlayout for key in ('type', 'direction', 'items'))
    jitems = jlayout.get('items')
    assert len(jitems) == 6

    jitem = jitems[0]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'pushButton'
    assert jwidget.get('text') == 'This is a PushButton'

    jitem = jitems[1]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text', 'checked'))
    assert jwidget.get('type') == 'checkBox'
    assert jwidget.get('text') == 'This is a CheckBox'
    assert jwidget.get('checked') == False

    jitem = jitems[2]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text', 'checked'))
    assert jwidget.get('type') == 'radioButton'
    assert jwidget.get('text') == 'This is a RadioButton'
    assert jwidget.get('checked') == False

    for i in range(3, 6):
        jitem = jitems[i]
        assert all(key in jitem for key in ('widget',))
        jwidget = jitem.get('widget')
        assert all(key in jwidget for key in ('type', 'text', 'checked', 'group', 'exclusive'))
        assert jwidget.get('type') == 'radioButton'
        assert jwidget.get('text') == f'This is grouped RadioButton {i-2}'
        if i == 4:
            assert jwidget.get('checked') == True
        else:
            assert jwidget.get('checked') == False
        assert jwidget.get('exclusive') == True
        if i == 3:
            group = jwidget.get('group')
            assert group
        else:
            assert jwidget.get('group') == group

def test_pushbutton_menu():
    init_qt()
    window = QWidget()
    layout = QVBoxLayout()
    window.setLayout(layout)

    # Button 1
    button1 = QPushButton(window)
    button1.setText("This is a plain PushButton")
    layout.addWidget(button1)

    # Button 2
    button2 = QPushButton(window)
    button2.setText("This is a PushButton with menu")
    layout.addWidget(button2)

    # Top level menu on button 2
    button2_menu = QMenu()
    button2.setMenu(button2_menu)

    # Top level menu entries
    button2_menu_entry1 = QAction("Meny entry 1", window)
    button2_menu.addAction(button2_menu_entry1)
    button2_menu_entry2 = QAction("Meny entry 2", window)
    button2_menu.addAction(button2_menu_entry2)

    # Sub menu of menu entry 1
    button2_menu_sub = QMenu()
    button2_menu_entry1.setMenu(button2_menu_sub)

    # Sub menu entries
    button2_menu_entry1_sub1 = QAction("Submenu entry A", window)
    button2_menu_sub.addAction(button2_menu_entry1_sub1)
    button2_menu_entry1_sub2 = QAction("Submenu entry B", window)
    button2_menu_sub.addAction(button2_menu_entry1_sub2)
    button2_menu_entry1_sub3 = QAction("Submenu entry C", window)
    button2_menu_sub.addAction(button2_menu_entry1_sub3)

    # Sub-sub menu of sub-menu entry 2
    button2_menu_sub_sub = QMenu()
    button2_menu_entry1_sub2.setMenu(button2_menu_sub_sub)
    button2_menu_entry1_sub2_entry1 = QAction("Subsubmenu entry i", window)
    button2_menu_sub_sub.addAction(button2_menu_entry1_sub2_entry1)

    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'layout' in jwindow
    jlayout = jwindow.get('layout')
    assert all(key in jlayout for key in ('type', 'direction', 'items'))
    jitems = jlayout.get('items')
    assert len(jitems) == 2

    jitem = jitems[0]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'pushButton'
    assert jwidget.get('text') == 'This is a plain PushButton'

    jitem = jitems[1]
    assert all(key in jitem for key in ('widget',))
    jwidget = jitem.get('widget')
    assert all(key in jwidget for key in ('type', 'text', 'actions'))
    assert jwidget.get('type') == 'pushButton'
    assert jwidget.get('text') == 'This is a PushButton with menu'
    jactions = jwidget.get('actions')
    assert len(jactions) == 1
    jaction = jactions[0]
    assert all(key in jaction for key in ('menu',))
    jmenu = jaction.get('menu')
    assert all(key in jmenu for key in ('actions',))
    jactions = jmenu.get('actions')
    assert len(jactions) == 2
    jaction = jactions[1] # Index 1 first, because it is the simplest
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Meny entry 2'
    jaction = jactions[0]
    assert all(key in jaction for key in ('text', 'menu'))
    assert jaction.get('text') == 'Meny entry 1'
    jmenu = jaction.get('menu')
    assert all(key in jmenu for key in ('actions',))
    jactions = jmenu.get('actions')
    jaction = jactions[0]
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Submenu entry A'
    jaction = jactions[1]
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Submenu entry B'
    jaction = jactions[2]
    assert all(key in jaction for key in ('text',))
    assert jaction.get('text') == 'Submenu entry C'

def test_multilevel_grid_layout():
    init_qt()
    # Build Qt ui
    window = QWidget()
    mainLayout = QGridLayout()
    childLayout = QGridLayout()
    childLayout.addWidget(QLabel('A'))
    childLayout.addWidget(QLabel('B'))
    mainLayout.addLayout(childLayout, 0, 0) # Nested layout here
    window.setLayout(mainLayout)
    # Export json
    cfg = ui.export(window)
    assert cfg
    # Build structured json and verify
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'layout' in jwindow
    # Parent layout
    jlayout = jwindow.get('layout')
    assert jlayout
    assert jlayout.get('type') == 'grid'
    assert all(key in jlayout for key in ('type', 'rowCount', 'columnCount', 'items'))
    assert jlayout.get('rowCount') == 1
    assert jlayout.get('columnCount') == 1
    assert len(jlayout.get('items')) == 1
    # Parent layout item
    jlayout_item = jlayout.get('items')[0]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'layout'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert all(jbox.get(key) == 0 for key in ('x', 'y'))
    assert all(jbox.get(key) == 1 for key in ('w', 'h'))
    # Child layout (layout of parent layout item)
    jchild_layout = jlayout_item.get('layout')
    assert jchild_layout
    assert jchild_layout.get('type') == 'grid'
    assert all(key in jlayout for key in ('type', 'rowCount', 'columnCount', 'items'))
    assert jchild_layout.get('rowCount') == 2
    assert jchild_layout.get('columnCount') == 1
    assert len(jchild_layout.get('items')) == 2
    # First child layout item
    jlayout_item = jchild_layout.get('items')[0]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'widget'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert all(jbox.get(key) == 0 for key in ('x', 'y'))
    assert all(jbox.get(key) == 1 for key in ('w', 'h'))
    # Widget (in child layout item)
    assert 'widget' in jlayout_item
    jwidget = jlayout_item.get('widget')
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'A'
    # Second child layout item
    jlayout_item = jchild_layout.get('items')[1]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'widget'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert jbox.get('x') == 0
    assert all(jbox.get(key) == 1 for key in ('y', 'w', 'h'))
    # Widget (in child layout item)
    assert 'widget' in jlayout_item
    jwidget = jlayout_item.get('widget')
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'B'

def test_grid_layout_with_composite_widget():
    init_qt()
    # Build Qt ui
    window = QWidget()
    mainLayout = QGridLayout()
    widget = QWidget()
    widgetLayout = QGridLayout()
    widgetLayout.addWidget(QLabel('A'))
    widgetLayout.addWidget(QLabel('B'))
    widget.setLayout(widgetLayout) # Composite widget using a widget layout
    mainLayout.addWidget(widget, 0, 0) # Single widget inside main layout
    window.setLayout(mainLayout)

    # Export json
    cfg = ui.export(window)
    assert cfg
    # Build structured json and verify
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'layout' in jwindow
    # Parent layout
    jlayout = jwindow.get('layout')
    assert jlayout
    assert jlayout.get('type') == 'grid'
    assert all(key in jlayout for key in ('type', 'rowCount', 'columnCount', 'items'))
    assert jlayout.get('rowCount') == 1
    assert jlayout.get('columnCount') == 1
    assert len(jlayout.get('items')) == 1
    # Parent layout item
    jlayout_item = jlayout.get('items')[0]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'widget'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert all(jbox.get(key) == 0 for key in ('x', 'y'))
    assert all(jbox.get(key) == 1 for key in ('w', 'h'))
    # Widget (layout item)
    jwidget = jlayout_item.get('widget')
    assert jwidget
    # Widget layout
    jwidget_layout = jwidget.get('layout')
    assert jwidget_layout
    assert jwidget_layout.get('type') == 'grid'
    assert all(key in jwidget_layout for key in ('type', 'rowCount', 'columnCount', 'items'))
    assert jwidget_layout.get('rowCount') == 2
    assert jwidget_layout.get('columnCount') == 1
    assert len(jwidget_layout.get('items')) == 2
    # First widget layout item
    jlayout_item = jwidget_layout.get('items')[0]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'widget'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert all(jbox.get(key) == 0 for key in ('x', 'y'))
    assert all(jbox.get(key) == 1 for key in ('w', 'h'))
    # Widget (in child layout item)
    assert 'widget' in jlayout_item
    jwidget = jlayout_item.get('widget')
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'A'
    # Second child layout item
    jlayout_item = jwidget_layout.get('items')[1]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'widget'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert jbox.get('x') == 0
    assert all(jbox.get(key) == 1 for key in ('y', 'w', 'h'))
    # Widget (in child layout item)
    assert 'widget' in jlayout_item
    jwidget = jlayout_item.get('widget')
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'B'

def test_multi_cell_grid_layout():
    init_qt()
    window = QWidget()
    mainGrid = QGridLayout()
    for i in range(3):
        for j in range(3):
            if j == 1 and i == 1:
                subGrid = QGridLayout()
                subGrid.addWidget(QLabel("A"))
                subGrid.addWidget(QLabel("B"))
                # Either:
                #mainGrid.addLayout(subGrid, i, j)
                # Or:
                wm = QWidget()
                wm.setLayout(subGrid)
                mainGrid.addWidget(wm, i,j)
            else:
                mainGrid.addWidget(QLabel(f"B({i},{j})"), i, j)
    window.setLayout(mainGrid)
    cfg = ui.export(window)
    assert cfg
    jwindow = json.loads(cfg)
    #json.dumps(jwindow, indent=2)
    assert jwindow
    assert 'layout' in jwindow
    # Top level layout: {"layout":{"type":"grid","rowCount":3,"columnCount":3,"items":[...
    jlayout = jwindow.get('layout')
    assert jlayout
    assert jlayout.get('type') == 'grid'
    assert all(key in jlayout for key in ('type', 'rowCount', 'columnCount', 'items'))
    assert jlayout.get('rowCount') == 3
    assert jlayout.get('columnCount') == 3
    assert len(jlayout.get('items')) == 9
    # Layout item 4, at position [1,1] in top level grid layout, containing a widget which is composed of two child widgets using a layout
    # {"layout":{"type":"grid","rowCount":2,"columnCount":1,"items":[{"rect":{"x":0,"y":0,"w":1,"h":1},"widget":{"type":"label","text":"A"}},{"rect":{"x":1,"y":0,"w":1,"h":1},"widget":{"type":"label","text":"B"}}]}}}
    jlayout_item = jlayout.get('items')[4]
    assert jlayout_item
    assert all(key in jlayout_item for key in ('rect', 'widget'))
    jbox = jlayout_item.get('rect')
    assert jbox
    assert all(jbox.get(key) == 1 for key in ('x', 'y', 'w', 'h')) # this one happens to be at x=y=w=h=1
    jwidget = jlayout_item.get('widget') # The parent widget
    assert jwidget
    assert 'layout' in jwidget
    jwidget_layout = jwidget.get('layout')
    assert jwidget_layout
    assert all(key in jwidget_layout for key in ('type', 'rowCount', 'columnCount', 'items'))
    assert jwidget_layout.get('rowCount') == 2
    assert jwidget_layout.get('columnCount') == 1
    assert len(jwidget_layout.get('items')) == 2
    jwidget_layout_item = jwidget_layout.get('items')[0]
    jwidget = jwidget_layout_item.get('widget') # The child widget
    assert jwidget
    assert all(key in jwidget for key in ('type', 'text'))
    assert jwidget.get('type') == 'label'
    assert jwidget.get('text') == 'A'

def test_multi_cell_grid_layout_with_chart_and_table():
    init_qt()
    fill_data = True
    window = QWidget()
    mainGrid = QGridLayout()
    counter = 0
    for i in range(3):
        for j in range(3):
            if i == 0 and j == 0:
                subGrid = QGridLayout()
                for k in range(7):
                    subItem = QPushButton(f"Item {counter} [{i},{j}]/[{k}]")
                    subItem.setFlat(True if k % 2 == 0 else False)
                    subGrid.addWidget(subItem)
                subGridWidget = QWidget()
                subGridWidget.setLayout(subGrid)
                mainGrid.addWidget(subGridWidget, i, j)
            elif i == 1 and j == 0:
                table = QTableWidget()
                table.setRowCount(10)
                table.setColumnCount(3)
                tableHeader = ["A", "B", "C"]
                table.setHorizontalHeaderLabels(tableHeader)
                table.verticalHeader().setVisible(False)
                table.setShowGrid(False)
                table.setStyleSheet("QTableView {selection-background-color: red;}")
                if fill_data:
                    table.setItem(0, 0, QTableWidgetItem("Item 0.0"))
                    table.setItem(0, 1, QTableWidgetItem("Item 0.1"))
                    table.setItem(0, 2, QTableWidgetItem("Item 0.2"))
                    table.setItem(1, 0, QTableWidgetItem("Item 1.0"))
                    table.setItem(1, 1, QTableWidgetItem("Item 1.1"))
                    table.setItem(1, 2, QTableWidgetItem("Item 1.2"))
                mainGrid.addWidget(table, i, j)
            elif i == 1 and j == 1:
                axis_x = QtCharts.QDateTimeAxis()
                axis_x.setTickCount(10)
                axis_x.setFormat("dd.MM (h:mm)")
                axis_x.setTitleText("This is a datetime axis")

                axis_y = QtCharts.QValueAxis()
                axis_y.setTickCount(10)
                axis_y.setLabelFormat("%.2f")
                axis_y.setTitleText("This is a value axis")
                axis_y.setMin(10)
                axis_y.setMax(100)

                serie1 = QtCharts.QLineSeries()
                serie1.setName("This is a series")

                if fill_data:
                    t = QDateTime.currentDateTime()
                    t.setTime(QTime())
                    for _ in range(24):
                        v = random.randrange(10, 100)
                        serie1.append(float(t.toMSecsSinceEpoch()), v)
                        t = t.addSecs(3600)

                chart = QtCharts.QChart()
                chart.setTitle("This is a chart")
                chart.addAxis(axis_x, Qt.AlignBottom)
                chart.addAxis(axis_y, Qt.AlignLeft)

                chart.addSeries(serie1)
                serie1.attachAxis(axis_x)
                serie1.attachAxis(axis_y)
                chartView = QtCharts.QChartView(chart)
                mainGrid.addWidget(chartView, i, j)
            else:
                label = QLabel(f"Item {counter} [{i},{j}]")
                label.setFont(QFont("Consolas", 24))
                label.setFrameShape(QFrame.StyledPanel)
                mainGrid.addWidget(label, i, j)
            counter += 1

    # Stretch
    mainGrid.setColumnStretch(1, 1) # Prefer stretching the chart when horizontal resize

    # Spacing
    #mainGrid.setSpacing(1) # Set horizontal and vertical to same value
    #mainGrid.setHorizontalSpacing(2)
    #mainGrid.setVerticalSpacing(3)

    window.setLayout(mainGrid)
    window.setGeometry(200, 200, 1000, 750)
    window.setWindowTitle("This is a window")

    cfg = ui.export(window)
    assert cfg
    assert json.loads(cfg)

    # To show it, for manual inspection:
    #window.show()
    #app.exec_()
