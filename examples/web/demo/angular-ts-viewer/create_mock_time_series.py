from time import sleep
import numpy as np
import os
from shyft.time_series import (point_interpretation_policy as point_fx, TimeAxis, TimeSeries, TsVector, time, Calendar, deltahours,DtsClient,DtsServer,shyft_url,utctime_now)


class DtsService():
  def __init__(self):
    self.s: DtsServer = DtsServer()
    self.s.set_auto_cache(True)
    self.s.set_container("test", os.getcwd() + "/data")
    self.c: DtsClient = None

  def __enter__(self):
    port_no = self.s.start_async()
    print(f"Setting up web API service., raw api at {port_no}")
    self.c = DtsClient(rf"localhost:{port_no}")
    self.s.start_web_api('0.0.0.0', 7777, os.getcwd() + "/dist/angular-ts-viewer")
    return self.c

  def __exit__(self, type, value, traceback):
    self.c.close()
    self.s.stop_web_api()
    self.s.clear()
    del self.c
    del self.s


def shyft_store_url(name: str) -> str:
  return shyft_url("test", name)


def make_ts_fragment(*, x:float,noise:float, ta:TimeAxis)->TimeSeries:
  t=ta.time_points_double[:-1]
  v= np.sin(t*x)+np.random.random([ta.size()])*noise
  return TimeSeries(ta,v,point_fx.POINT_AVERAGE_VALUE)


def store_some_time_series(*, client: DtsClient, ta: TimeAxis):
  n_ts = 20
  tsv = TsVector()  # something we store at server side
  for i in range(n_ts):
    tsv.append(TimeSeries(shyft_store_url(f"ts{i}"),make_ts_fragment(x=3.14/(3600.0*12+i), noise=0.15,ta=ta)))  # generate a bound pts to store
  client.store_ts(tsv=tsv, overwrite_on_write=False, cache_on_write=True)


if __name__ == '__main__':
  with DtsService() as client:
    ta = TimeAxis(time('2020-01-01T00:00:00Z'),time(3600), 24*10)
    store_some_time_series(client=client,ta=ta)
    # Let's verify the results:
    r = client.find(r'shyft://test/ts\d')
    print([it.name for it in r])
    t_next=utctime_now()+time(10.0)
    print("To view app, browse to http://localhost:7777 ")
    while (True):
      sleep(0.2)
      if utctime_now() > t_next:
        store_some_time_series(client=client,ta=ta)
        print('.')
        t_next=utctime_now() + time(5.0)
      pass
