import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TsGraphComponent } from './ts-graph.component';

describe('TsGraphComponent', () => {
  let component: TsGraphComponent;
  let fixture: ComponentFixture<TsGraphComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TsGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
