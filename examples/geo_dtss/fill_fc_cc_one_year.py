from shyft.time_series import (DtsClient, GeoEvalArgs, GeoQuery, GeoGridSpec, GeoTimeSeriesConfiguration, GeoPoint,
                               GeoPointVector, StringVector, time, UtcTimeVector, Calendar, GeoSlice, utctime_now,
                               TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case,
                               time_axis_extract_time_points_as_utctime)


def fill_forecasts(c: DtsClient, ta_t0: TimeAxis, n_variables: int = 5, n_ensembles: int = 1, fc_dt: time = time(3600), fc_steps: int = 24*10):
    geos = c.get_geo_db_ts_info()  # get the configurations from  the server
    gdb = None
    gcc = None
    for geo in geos:
        if geo.name == 'fc':
            gdb = geo
        elif geo.name == 'cc':
            gcc = geo
    assert gdb and gcc, 'db-server must have gdb and gcc setup'

    tp = time_axis_extract_time_points_as_utctime(ta_t0)[:-1]
    g_slice = GeoSlice(v=[i for i in range(n_variables)], e=[i for i in range(n_ensembles)], g=[gix for gix in range(len(gdb.grid.points))], t=tp, ts_dt=gdb.dt)
    fcm = gdb.create_ts_matrix(g_slice)  # create the structure for our forecasts
    print(f'{fcm.shape.n_t0} x {fcm.shape.n_v} x {fcm.shape.n_e} x {fcm.shape.n_g}, xts {fc_steps}')
    # now fill the forecast matrix, with forecast time-series
    # dimensions time, variables, ensembles,geopoints
    for t in range(fcm.shape.n_t0):
        for v in range(fcm.shape.n_v):
            for e in range(fcm.shape.n_e):
                for g in range(fcm.shape.n_g):
                    fc_ts = TimeSeries(TimeAxis(ta_t0.time(t), fc_dt, fc_steps), fill_value=t*10 + v + g/len(gdb.grid.points), point_fx=stair_case)
                    fcm.set_ts(t, v, e, g, fc_ts)  # indicies happens to be correct here
    tx1 = utctime_now()
    #cc_fcm = fcm.concatenate(time(0), ta_t0.period(0).timespan())  # since we do many forecasts in one, concatenate client side
    cc_used = utctime_now() - tx1
    tx1 = utctime_now()
    c.geo_store(gcc.name, fcm, False, True)  # The concat-store them incrementally to the server
    cc_store_used = utctime_now() - tx1
    c.geo_store(gdb.name, fcm, True, True)  # store ordinary forecasts to the server
    store_used = utctime_now() - tx1
    return (cc_used, cc_store_used, store_used)


def fill_one_year_of_data(c:DtsClient):
    utc = Calendar()
    ta_t0_y = TimeAxis(utc.time(2020, 9, 1), time(3600*6), 4*(365))

    for i in range(0, len(ta_t0_y), 1):  # fill 4 forecasts, that is one day
        ta_t0 = TimeAxis(ta_t0_y.time(i), time(3600*6), 1)
        print(f'Fill in forecast {repr(ta_t0)}')
        cc_used, cc_store_used, store_used = fill_forecasts(c, ta_t0)
        print(f'cc={cc_used},cc_store={cc_store_used},store={store_used}')

c = DtsClient(f'localhost:20000')
fill_one_year_of_data(c)  # 4 forecasts, every day (every 6 hours), 365 days,
