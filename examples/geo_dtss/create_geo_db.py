from shyft.time_series import (DtsClient, GeoEvalArgs, GeoQuery, GeoGridSpec, GeoTimeSeriesConfiguration, GeoPoint,
                               GeoPointVector, StringVector, time, UtcTimeVector, Calendar, GeoSlice, utctime_now,
                               TimeSeries, TimeAxis, POINT_AVERAGE_VALUE as stair_case,
                               time_axis_extract_time_points_as_utctime)


def create_geo_db(client: DtsClient, container_name: str = "geo_test",
                  descr: str = 'test geo dtss', n_x: int = 3, n_y: int = 4,
                  n_ensembles: int = 1, n_variables: int = 2, dt: time = Calendar.WEEK):
    variables = StringVector([f'v{i}' for i in range(n_variables)])  # minimalistic variable names
    grid_points = [GeoPoint(x*1000, y*1000, (x + y)*100) for x in range(n_x) for y in range(n_y)]  # (x, y, z) coordinates
    points = GeoPointVector(grid_points)  # Vector of the grid points, length 12
    grid = GeoGridSpec(epsg=32633, points=points)

    gdb = GeoTimeSeriesConfiguration('shyft://', container_name, descr, grid,
                                     UtcTimeVector(), dt, n_ensembles, variables)
    client.add_geo_ts_db(gdb)


def create_concat_geo_db(client: DtsClient, t0_dt=time(3600*6), n_x: int = 10, n_y: int = 20,
                         n_ensembles: int = 1, n_variables: int = 5):
    create_geo_db(client, "fc", 'individual arome-foreast test', n_x, n_y, n_ensembles, n_variables, t0_dt)
    create_geo_db(client, "cc", 'concatenated arome-forecasts', n_x, n_y, n_ensembles, n_variables, Calendar.YEAR*30)


c = DtsClient(f'localhost:20000')
#create_geo_db(c)
create_concat_geo_db(c)
